#version 450

layout(set = 0, binding = 0) uniform sampler2D inputTexture;

layout(location = 0) in vec2 fragUV;

layout(location = 0) out vec4 outFragColor;

void main() {
    outFragColor = texture(inputTexture, vec2(fragUV.x, 1.0 - fragUV.y));
}