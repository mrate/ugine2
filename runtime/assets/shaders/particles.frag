#version 450

#include "common/global.glsl"

#include "common/binding.glsl"

layout(set = MATERIAL_SET, binding = 0) uniform sampler2D inputTexture;

layout(location = 0) in vec2 inFragUV;
layout(location = 1) in vec4 inColor;

layout(location = 0) out vec4 outFragColor;

void main() {
    vec4 color = texture(inputTexture, inFragUV);
    outFragColor = color * inColor;
}