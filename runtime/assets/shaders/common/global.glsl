#define PI 3.14159265359
#define EPSILON 0.0001

struct Light {
    vec3 ambientColor;
    int typeEnabled;
    vec3 color;
    float intensity;
    vec3 position;
    float spotAngleRad;
    vec3 direction;
    float range;
    int shadowIndex;
    int shadowMapIndex;
    int csmMaxLevel;
};

struct Shadow {
    mat4 lightViewProj[UGINE_CSM_LEVELS];
};

struct Camera {
    mat4 viewProj;
    mat4 view;
    mat4 proj;
    mat4 invView;
    mat4 invProj;
    mat4 viewProjPrev;
    vec3 position;
    float pad;
    float zNear;
    float zFar;
    uvec2 resolution;
};

struct Frame {
    float time;
    float timeDiff;
    float windSpeed;
    float windRandomness;

    vec3 windDirection;
    float windWaveSize;

    vec3 sunPosition;
    float volumetricIntensity;

    vec3 csm;
};

struct AABB {
    vec3 mmin;
    vec3 mmax;
};

float CalcSpotCone(Light light, vec3 lightDir) {
    // If the cosine angle of the light's direction
    // vector and the vector from the light source to the point being
    // shaded is less than minCos, then the spotlight contribution will be 0.
    float minCos = cos(light.spotAngleRad);
    // If the cosine angle of the light's direction vector
    // and the vector from the light source to the point being shaded
    // is greater than maxCos, then the spotlight contribution will be 1.
    float maxCos = mix(minCos, 1., 0.5);
    float cosAngle = dot(light.direction, -lightDir);
    // Blend between the minimum and maximum cosine angles.
    return smoothstep(minCos, maxCos, cosAngle);
}

float DoAttenuation(Light light, float distance) {
    //return attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
    return 1.0f - smoothstep(light.range * 0.75, light.range, distance);
}

float Rand(float p) {
    p = fract(p * 230.384);
    p += dot(p, p + 35.13);
    return fract(p * p);
}

float Random(inout float seed, in vec2 uv) {
    float p = fract(sin(seed * dot(uv, vec2(234.34, 435.345))) * 86348.4684);
    seed += 1;
    return p;
}

const mat2 BayerMatrix2 = {
    { 1.0 / 5.0, 3.0 / 5.0 },
    { 4.0 / 5.0, 2.0 / 5.0 },
};

const mat3 BayerMatrix3 = {
    { 3.0 / 10.0, 7.0 / 10.0, 4.0 / 10.0 },
    { 6.0 / 10.0, 1.0 / 10.0, 9.0 / 10.0 },
    { 2.0 / 10.0, 8.0 / 10.0, 5.0 / 10.0 },
};

const mat4 BayerMatrix4 = {
    { 1.0 / 17.0, 9.0 / 17.0, 3.0 / 17.0, 11.0 / 17.0 },
    { 13.0 / 17.0, 5.0 / 17.0, 15.0 / 17.0, 7.0 / 17.0 },
    { 4.0 / 17.0, 12.0 / 17.0, 2.0 / 17.0, 10.0 / 17.0 },
    { 16.0 / 17.0, 8.0 / 17.0, 14.0 / 17.0, 6.0 / 17.0 },
};

const float BayerMatrix8[8][8] = {
    { 1.0 / 65.0, 49.0 / 65.0, 13.0 / 65.0, 61.0 / 65.0, 4.0 / 65.0, 52.0 / 65.0, 16.0 / 65.0, 64.0 / 65.0 },
    { 33.0 / 65.0, 17.0 / 65.0, 45.0 / 65.0, 29.0 / 65.0, 36.0 / 65.0, 20.0 / 65.0, 48.0 / 65.0, 32.0 / 65.0 },
    { 9.0 / 65.0, 57.0 / 65.0, 5.0 / 65.0, 53.0 / 65.0, 12.0 / 65.0, 60.0 / 65.0, 8.0 / 65.0, 56.0 / 65.0 },
    { 41.0 / 65.0, 25.0 / 65.0, 37.0 / 65.0, 21.0 / 65.0, 44.0 / 65.0, 28.0 / 65.0, 40.0 / 65.0, 24.0 / 65.0 },
    { 3.0 / 65.0, 51.0 / 65.0, 15.0 / 65.0, 63.0 / 65.0, 2.0 / 65.0, 50.0 / 65.0, 14.0 / 65.0, 62.0 / 65.0 },
    { 35.0 / 65.0, 19.0 / 65.0, 47.0 / 65.0, 31.0 / 65.0, 34.0 / 65.0, 18.0 / 65.0, 46.0 / 65.0, 30.0 / 65.0 },
    { 11.0 / 65.0, 59.0 / 65.0, 7.0 / 65.0, 55.0 / 65.0, 10.0 / 65.0, 58.0 / 65.0, 6.0 / 65.0, 54.0 / 65.0 },
    { 43.0 / 65.0, 27.0 / 65.0, 39.0 / 65.0, 23.0 / 65.0, 42.0 / 65.0, 26.0 / 65.0, 38.0 / 65.0, 22.0 / 65.0 },
};

float DitherMask2(in ivec2 pixel) {
    return BayerMatrix2[pixel.x % 2][pixel.y % 2];
}

float DitherMask3(in ivec2 pixel) {
    return BayerMatrix3[pixel.x % 3][pixel.y % 3];
}

float DitherMask4(in ivec2 pixel) {
    return BayerMatrix4[pixel.x % 4][pixel.y % 4];
}

float DitherMask8(in ivec2 pixel) {
    return BayerMatrix8[pixel.x % 8][pixel.y % 8];
}

float Dither(in ivec2 pixel) {
    return DitherMask8(pixel);
}

mat4 Rotation(vec3 axis, float angle) {
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;
    
    return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
                0.0,                                0.0,                                0.0,                                1.0);
}

mat4 Translation(vec3 pos) {
    return mat4( 
            1, 0, 0, 0, 
            0, 1, 0, 0, 
            0, 0, 1, 0, 
            pos.x, pos.y, pos.z, 1);
}

bool FrustumCull(vec4 frustumPlanes[6], AABB aabb) {
    vec3 points[8] = {
        vec3( aabb.mmin.x, aabb.mmin.y, aabb.mmin.z ),
        vec3( aabb.mmax.x, aabb.mmin.y, aabb.mmin.z ),
        vec3( aabb.mmin.x, aabb.mmax.y, aabb.mmin.z ),
        vec3( aabb.mmax.x, aabb.mmax.y, aabb.mmin.z ),
        vec3( aabb.mmin.x, aabb.mmin.y, aabb.mmax.z ),
        vec3( aabb.mmax.x, aabb.mmin.y, aabb.mmax.z ),
        vec3( aabb.mmin.x, aabb.mmax.y, aabb.mmax.z ),
        vec3( aabb.mmax.x, aabb.mmax.y, aabb.mmax.z ),
    };

    bool inside = false;
    for (int i = 0; i < 6; ++i) {
        vec4 plane = frustumPlanes[i];

        inside = false;
        for (int j = 0; j < 8; ++j) {
            vec3 point = points[j];

            if (plane.x * point.x + plane.y * point.y + plane.z * point.z + plane.w > 0.0f) {
                inside = true;
                break;
            }
        }

        if (!inside) {
            return false;
        }
    }
    return true;
}

vec3 AABBMin(vec3 a, vec3 b) {
    return vec3(min(a.x, b.x), min(a.y, b.y), min(a.z, b.z));
}

vec3 AABBMax(vec3 a, vec3 b) {
    return vec3(max(a.x, b.x), max(a.y, b.y), max(a.z, b.z));
}

uint CalcLod(float distance) {
    return distance < 5 ? 0 : distance < 10 ? 1 : distance < 20 ? 3 : distance < 40 ? 4 : 5;
}

bool IsSaturated(vec3 v) {
    return v == clamp(v, 0, 1);
}

bool IsSaturated(vec2 v) {
    return v == clamp(v, 0, 1);
}

float RaySphereIntersection(vec3 origin, vec3 direction, vec3 center, float radius) {
    vec3 oc = origin - center;
    float a = dot(direction, direction);
    float b = 2.0 * dot(oc, direction);
    float c = dot(oc, oc) - radius * radius;
    float discriminant = b * b - 4 * a * c;
    return discriminant < 0 ? -1.0 : (-b - sqrt(discriminant)) / (2.0*a);
}

vec3 ViewSpaceFromDepth(vec2 uv, Camera camera, sampler2D depthTexture) {
    // Depth in texture is in 0;1 range, which is also used by Vulkan, no need to normalize it.
    float depth = texture(depthTexture, uv).r;
    vec2 screenSpace = uv * 2.0 - 1.0;
    vec4 positionVS = camera.invProj * vec4(screenSpace, depth, 1.0);
    return positionVS.xyz / positionVS.w;
}

vec3 WorldSpaceFromDepth(vec2 uv, Camera camera, sampler2D depthTexture) {
    vec3 positionVS = ViewSpaceFromDepth(uv, camera, depthTexture);
    return (camera.invView * vec4(positionVS, 1)).xyz;
}

vec3 ViewSpaceFromDepthFloat(vec2 uv, Camera camera, float depth){
    vec2 screenSpace = uv * 2.0 - 1.0;
    vec4 positionVS = camera.invProj * vec4(screenSpace, depth, 1.0);
    return positionVS.xyz / positionVS.w;
}

// Improved reconstruction of normals from depth buffer based on minimal depth discontinuities.
// https://wickedengine.net/2019/09/22/improved-normal-reconstruction-from-depth/
vec3 NormalVSFromDepth(vec2 uv, Camera camera, sampler2D depthTexture) {
    vec2 texelSize = vec2(1) / camera.resolution;
    vec2 offsets[] = {
        uv, // center
        uv - vec2(texelSize.x, 0), // l
        uv + vec2(texelSize.x, 0), // r
        uv - vec2(0, texelSize.y), // t
        uv + vec2(0, texelSize.y)  // b
    };

    float depths[] = {
        texture(depthTexture, offsets[0]).r,
        texture(depthTexture, offsets[1]).r, // l
        texture(depthTexture, offsets[2]).r, // r
        texture(depthTexture, offsets[3]).r, // t
        texture(depthTexture, offsets[4]).r  // b
    };

    int h = abs(depths[1] - depths[0]) < abs(depths[2] - depths[0]) ? 1 : 2;
    int v = abs(depths[3] - depths[0]) < abs(depths[4] - depths[0]) ? 3 : 4;

    vec3 P0 = ViewSpaceFromDepthFloat(offsets[0], camera, depths[0]);
    vec3 P1;
    vec3 P2;

    if ((h == 2 && v == 4) || (h == 1 && v == 3)) {
        P1 = ViewSpaceFromDepthFloat(offsets[h], camera, depths[h]);
        P2 = ViewSpaceFromDepthFloat(offsets[v], camera, depths[v]);
    } else {
        P1 = ViewSpaceFromDepthFloat(offsets[v], camera, depths[v]);
        P2 = ViewSpaceFromDepthFloat(offsets[h], camera, depths[h]);
    }

    return normalize(cross(P2 - P0, P1 - P0));
}

vec3 NormalVSFromDepthClassic(vec2 uv, Camera camera, sampler2D depthTexture) {
    vec2 uv0 = uv; // center
    vec2 uv1 = uv + vec2(1, 0) / camera.resolution; // right 
    vec2 uv2 = uv + vec2(0, 1) / camera.resolution; // top

    vec3 P0 = ViewSpaceFromDepth(uv0, camera, depthTexture);
    vec3 P1 = ViewSpaceFromDepth(uv1, camera, depthTexture);
    vec3 P2 = ViewSpaceFromDepth(uv2, camera, depthTexture);

    return normalize(cross(P2 - P0, P1 - P0));
}
