struct LightingResult {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

float CalcDiff(vec3 lightDir, vec3 normal) {
    return max(dot(normal, lightDir), 0.0);
}

float CalcSpec(vec3 viewDir, vec3 lightDir, vec3 normal, float specularPower) {
    vec3 halfwayDir = normalize(lightDir + viewDir);
    return pow(max(dot(normal, halfwayDir), 0.0), specularPower);
}

LightingResult CalcDirLight(Light light, vec3 normal, vec3 viewDir, float specularPower) {
    vec3 lightDir = light.direction;

    float diff = CalcDiff(lightDir, normal);
    float spec = CalcSpec(viewDir, lightDir, normal, specularPower);

    LightingResult result;
    result.ambient = light.ambientColor.rgb * light.intensity;
    result.diffuse = light.color.rgb * diff * light.intensity;
    result.specular = light.color.rgb * spec * light.intensity;
    return result;
}

LightingResult CalcPointLight(Light light, vec3 normal, vec3 viewDir, vec3 fragPos, float specularPower) {
    vec3 lightDir = normalize(light.position - fragPos);

    float diff = CalcDiff(lightDir, normal);
    float spec = CalcSpec(viewDir, lightDir, normal, specularPower);

    float distance = length(light.position - fragPos);
    float attenuation = DoAttenuation(light, distance);

    LightingResult result;
    result.ambient = light.ambientColor.rgb * attenuation * light.intensity;
    result.diffuse = light.color.rgb * diff * attenuation * light.intensity;
    result.specular = light.color.rgb * spec * attenuation * light.intensity;
    return result;
}

LightingResult CalcSpotLight(Light light, vec3 normal, vec3 viewDir, vec3 fragPos, float specularPower) {
    vec3 lightDir = light.position - fragPos;
    float distance = length(lightDir);
    lightDir = normalize(lightDir);

    float attenuation = DoAttenuation(light, distance);
    float spotIntensity = CalcSpotCone(light, lightDir);

    float diff = CalcDiff(lightDir, normal);
    float spec = CalcSpec(viewDir, lightDir, normal, specularPower);

    LightingResult result;
    result.ambient = light.ambientColor.rgb * attenuation * light.intensity;
    result.diffuse = light.color.rgb * diff * attenuation * spotIntensity * light.intensity;
    result.specular = light.color.rgb * spec * attenuation * spotIntensity * light.intensity;
    return result;
}

LightingResult BlinnPhong(Camera camera, vec3 normal, vec3 viewDir, vec3 fragPos, float specularPower, float shadowStrength) {

    LightingResult result = LightingResult(vec3(0.), vec3(0.), vec3(0.));

    // If this light generates shadow and fragment is not visible by this light then skip it.
    float shadowFactor = 0.0;

    for (int i = 0; i < g_shading.lightCount; ++i) {
        if (!LightEnabled(g_lights[i])) {
            continue;
        }

        LightingResult partialResult = LightingResult(vec3(0.), vec3(0.), vec3(0.));
        switch (LightType(g_lights[i])) {
        case UGINE_LIGHT_DIR:
            if (g_lights[i].shadowIndex >= 0) {
                int shadowIndex = g_lights[i].shadowIndex;
                int shadowMapIndex = g_lights[i].shadowMapIndex;
                int csm = min(CsmIndex(camera.position, fragPos), g_lights[i].csmMaxLevel);
                shadowFactor += CalcShadowFactorPCFDir(shadowMapIndex, g_shading.shadows[shadowIndex].lightViewProj[csm] * vec4(fragPos, 1.0), csm);
            }

            partialResult = CalcDirLight(g_lights[i], normal, viewDir, specularPower);
            break;
        case UGINE_LIGHT_POINT:
            if (g_lights[i].shadowIndex >= 0) {
                int shadowMapIndex = g_lights[i].shadowMapIndex;
                shadowFactor += CalcShadowFactorPoint(shadowMapIndex, fragPos - g_lights[i].position, g_lights[i].range);
            }
            partialResult = CalcPointLight(g_lights[i], normal, viewDir, fragPos, specularPower);
            break;
        case UGINE_LIGHT_SPOT:
            if (g_lights[i].shadowIndex >= 0) {
                int shadowIndex = g_lights[i].shadowIndex;
                int shadowMapIndex = g_lights[i].shadowMapIndex;
                shadowFactor
                    += CalcShadowFactorPCFSpot(shadowMapIndex, g_shading.shadows[shadowIndex].lightViewProj[0] * vec4(fragPos, 1.0), g_lights[i].range);
            }
            partialResult = CalcSpotLight(g_lights[i], normal, viewDir, fragPos, specularPower);
            break;
        }

        result.ambient += partialResult.ambient;
        result.diffuse += partialResult.diffuse;
        result.specular += partialResult.specular;
    }

    shadowFactor = (1.0 - shadowStrength * clamp(shadowFactor, 0.0, 1.0));

    result.diffuse *= shadowFactor;
    result.specular *= shadowFactor;

    // Clamp specular highlights, otherwise it's just too bright and ugly...
    result.specular = clamp(result.specular, 0.0, 1.0);
    return result;
}
