// Sascha Willems examples.
#define parallaxBias -0.02
#define parallaxNumLayers 48.0

//#define Parallax    ParallaxMapping
//#define Parallax    ParallaxMappingSteep
#define Parallax ParallaxMappingOclusion

float HeightMap(vec2 uv, sampler2D normalTexture, sampler2D heightTexture, int normalWithHeight) {
    float height = 0.0;
    if (normalWithHeight != 0.0) {
        float a = texture(normalTexture, uv).a;
        height = normalWithHeight > 0.0 ? a : 1.0 - a;
    } else {
        height = texture(heightTexture, uv).r;
    }
    return height;
}

vec2 ParallaxMapping(vec2 texCoords, vec3 viewDirWS, float heightScale, sampler2D normalTexture, sampler2D heightTexture, int normalWithHeight) {
    float height = HeightMap(texCoords, normalTexture, heightTexture, normalWithHeight);
    vec2 p = viewDirWS.xy * (height * (heightScale * 0.5) + parallaxBias) / viewDirWS.z;
    return texCoords - p;
}

vec2 ParallaxMappingSteep(vec2 uv, vec3 viewDir, float heightScale, sampler2D normalTexture, sampler2D heightTexture, int normalWithHeight) {
    float layerDepth = 1.0 / parallaxNumLayers;
    float currLayerDepth = 0.0;
    vec2 deltaUV = viewDir.xy * heightScale / (viewDir.z * parallaxNumLayers);
    vec2 currUV = uv;
    float height = HeightMap(currUV, normalTexture, heightTexture, normalWithHeight);
    for (int i = 0; i < parallaxNumLayers; i++) {
        currLayerDepth += layerDepth;
        currUV -= deltaUV;
        height = HeightMap(currUV, normalTexture, heightTexture, normalWithHeight);
        if (height < currLayerDepth) {
            break;
        }
    }
    return currUV;
}

vec2 ParallaxMappingOclusion(vec2 uv, vec3 viewDir, float heightScale, sampler2D normalTexture, sampler2D heightTexture, int normalWithHeight) {
    float layerDepth = 1.0 / parallaxNumLayers;
    float currLayerDepth = 0.0;
    vec2 deltaUV = viewDir.xy * heightScale / (viewDir.z * parallaxNumLayers);
    vec2 currUV = uv;
    float height = HeightMap(currUV, normalTexture, heightTexture, normalWithHeight);
    for (int i = 0; i < parallaxNumLayers; i++) {
        currLayerDepth += layerDepth;
        currUV -= deltaUV;
        height = HeightMap(currUV, normalTexture, heightTexture, normalWithHeight);
        if (height < currLayerDepth) {
            break;
        }
    }
    vec2 prevUV = currUV + deltaUV;
    float nextDepth = height - currLayerDepth;
    float prevDepth = HeightMap(prevUV, normalTexture, heightTexture, normalWithHeight) - currLayerDepth + layerDepth;
    return mix(currUV, prevUV, nextDepth / (nextDepth - prevDepth));
}