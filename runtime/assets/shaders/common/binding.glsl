#ifndef NO_GLOBAL_FRAME
layout(set = FRAME_SET, binding = FRAME_BINDING) uniform FrameCB {
    Frame g_frame;
};
#endif

#ifndef NO_GLOBAL_CAMERA
layout(set = CAMERA_SET, binding = CAMERA_BINDING) uniform CameraCB {
    Camera g_camera;
};
#endif

#ifndef NO_GLOBAL_SHADING
layout(set = GLOBAL_SET, binding = SHADING_BINDING) uniform ShadingCB {
    int lightCount;
    int shadowCount;
    int hasIbl;
    float shadowStrength;
    Shadow shadows[UGINE_MAX_SHADOWS];
}
g_shading;

layout(std140, set = GLOBAL_SET, binding = LIGHTS_BINDING) readonly buffer Lights {
    Light g_lights[];
};

layout(set = GLOBAL_SET, binding = DIR_SHADOW_MAP_BINDING) uniform sampler2DArray g_dirShadowMap[UGINE_MAX_DIR_SHADOWS];
layout(set = GLOBAL_SET, binding = SPOT_SHADOW_MAP_BINDING) uniform sampler2D g_spotShadowMap[UGINE_MAX_SPOT_SHADOWS];
layout(set = GLOBAL_SET, binding = POINT_SHADOW_MAP_BINDING) uniform samplerCube g_pointShadowMap[UGINE_MAX_POINT_SHADOWS];
layout(set = GLOBAL_SET, binding = BRDF_LUT_BINDING) uniform sampler2D g_brdfLutTexture;
layout(set = GLOBAL_SET, binding = IRRADIANCE_BINDING) uniform samplerCube g_irradianceTexture;
layout(set = GLOBAL_SET, binding = PREFILTERED_CUBEMAP_BINDING) uniform samplerCube g_prefilteredTexture;

layout(set = GLOBAL_SET, binding = AO_BINDING) uniform sampler2D g_ao;
layout(set = GLOBAL_SET, binding = DEPTH_MAP_BINDING) uniform sampler2D g_depth;
layout(set = GLOBAL_SET, binding = SCENE_BINDING) uniform sampler2D g_scene;

bool LightEnabled(in Light light) {
    return (light.typeEnabled & 0x1) > 0;
}

int LightType(in Light light) {
    return (light.typeEnabled & 0xfe) >> 1;
}

#endif

#ifndef NO_GLOBAL_FRAME
int CsmIndex(vec3 cameraPos, vec3 positionWS) {
    float distance = length(cameraPos - positionWS);
    if (distance < g_frame.csm.x) {
        return 0;
    } else if (distance < g_frame.csm.y) {
        return 1;
    } else {
        return 2;
    }
}
#endif