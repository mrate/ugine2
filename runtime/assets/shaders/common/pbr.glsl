
float DistributionGGX(vec3 N, vec3 H, float roughness) {
    float a = roughness * roughness;
    float a2 = a * a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH * NdotH;

    float num = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return num / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness) {
    float r = (roughness + 1.0);
    float k = (r * r) / 8.0;

    float num = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return num / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness) {
    float NdotL = max(dot(N, L), 0.0);
    float NdotV = max(dot(N, V), 0.0);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);

    return ggx1 * ggx2;
}

vec3 FresnelSchlick(float cosTheta, vec3 F0, float roughness) {
    return F0 + (max(vec3(1.0f - roughness), F0) - F0) * pow(1.0f - cosTheta, 5.0f);
}

vec3 DoLight(vec3 L, float attenuation, vec3 lightColor, vec3 viewDir, vec3 normal, vec3 F0, vec3 albedo, float metallic, float roughness) {
    vec3 H = normalize(viewDir + L);

    vec3 radiance = lightColor * attenuation;

    vec3 F = FresnelSchlick(max(dot(H, viewDir), 0.0), F0, roughness);

    // Cook-Torrance BRDF
    float NDF = DistributionGGX(normal, H, roughness);
    float G = GeometrySmith(normal, viewDir, L, roughness);

    vec3 numerator = NDF * G * F;
    float denominator = 4.0 * max(dot(normal, viewDir), 0.0) * max(dot(normal, L), 0.0);
    vec3 specular = numerator / max(denominator, 0.001);

    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS;
    kD *= 1.0 - metallic;

    float NdotL = max(dot(normal, L), 0.0);
    return (kD * albedo / PI + specular) * radiance * NdotL;
}

vec3 PBR(Camera camera, vec3 normal, vec3 viewDir, vec3 fragPos, vec3 F0, vec3 albedo, float metallic, float roughness, float shadowStrength) {
    vec3 Lo = vec3(0.0);

    float distance = 0;
    float attenuation = 0;
    vec3 lightDir = vec3(0.0);

    float shadowFactor = 0.0;

    for (int i = 0; i < g_shading.lightCount; ++i) {
        if (!LightEnabled(g_lights[i])) {
            continue;
        }

        // If this light generates shadow and fragment is not visible from this light then skip it.

        vec3 L = vec3(0.0);
        switch (LightType(g_lights[i])) {
        case UGINE_LIGHT_DIR:
            if (g_lights[i].shadowIndex >= 0) {
                int shadowIndex = g_lights[i].shadowIndex;
                int shadowMapIndex = g_lights[i].shadowMapIndex;
                int csm = min(CsmIndex(camera.position, fragPos), g_lights[i].csmMaxLevel);
                shadowFactor += CalcShadowFactorPCFDir(shadowMapIndex, g_shading.shadows[shadowIndex].lightViewProj[csm] * vec4(fragPos, 1.0), csm);
            }

            L = DoLight(g_lights[i].direction, 1.0, g_lights[i].color.rgb, viewDir, normal, F0, albedo, metallic, roughness);
            break;
        case UGINE_LIGHT_POINT:
            if (g_lights[i].shadowIndex >= 0) {
                int shadowMapIndex = g_lights[i].shadowMapIndex;
                shadowFactor += CalcShadowFactorPoint(shadowMapIndex, fragPos - g_lights[i].position, g_lights[i].range);
            }

            lightDir = normalize(g_lights[i].position.xyz - fragPos);
            distance = length(g_lights[i].position.xyz - fragPos);
            attenuation = 1.0 / (distance * distance);
            L = DoLight(lightDir, attenuation, g_lights[i].color.rgb, viewDir, normal, F0, albedo, metallic, roughness);
            break;
        case UGINE_LIGHT_SPOT:
            if (g_lights[i].shadowIndex >= 0) {
                int shadowIndex = g_lights[i].shadowIndex;
                int shadowMapIndex = g_lights[i].shadowMapIndex;
                shadowFactor
                    += CalcShadowFactorPCFSpot(shadowMapIndex, g_shading.shadows[shadowIndex].lightViewProj[0] * vec4(fragPos, 1.0), g_lights[i].range);
            }

            lightDir = normalize(g_lights[i].position.xyz - fragPos);
            distance = length(g_lights[i].position.xyz - fragPos);
            attenuation = 1.0 / (distance * distance);
            attenuation *= CalcSpotCone(g_lights[i], lightDir);
            L = DoLight(lightDir, attenuation, g_lights[i].color.rgb, viewDir, normal, F0, albedo, metallic, roughness);
            break;
        }

        Lo += L * g_lights[i].intensity;
    }

    shadowFactor = (1.0 - shadowStrength * clamp(shadowFactor, 0.0, 1.0));

    return Lo * shadowFactor;
}
