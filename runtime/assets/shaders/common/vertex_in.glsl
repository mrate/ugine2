// Attribs.
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inTangent;
layout(location = 3) in vec3 inBitangent;
layout(location = 4) in vec2 inUV;

#ifndef NO_INSTANCE

// Instance.
layout(location = 5) in vec4 inModel0;
layout(location = 6) in vec4 inModel1;
layout(location = 7) in vec4 inModel2;
layout(location = 8) in vec4 inModel3;

mat4 Model() {
    return mat4(inModel0, inModel1, inModel2, inModel3);
}

mat4 Normal() {
    return transpose(inverse(Model()));
}

#endif // NO_INSTANCE