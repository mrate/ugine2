#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "common/global.glsl"

#include "common/binding.glsl"

#include "common/vertex_in.glsl"

layout(set = MATERIAL_SET, binding = 0) uniform Buffer {
    float heightScale;
}
params;

layout(location = 0) out vec2 fragUV;
layout(location = 1) out vec3 positionVS;
layout(location = 2) out vec3 tangentWS;
layout(location = 3) out vec3 bitangentWS;
layout(location = 4) out vec3 normalWS;
layout(location = 5) out vec3 positionWS;
layout(location = 6) out vec4 positionM;

void main() {
    mat4 model = Model();
    mat4 normal = Normal();

    vec4 position = vec4(inPosition, 1.0);
    position.y *= params.heightScale;

    vec4 modelPos = model * position;

    gl_Position = g_camera.viewProj * modelPos;

    fragUV = inUV;

    positionVS = (g_camera.view * modelPos).xyz;
    positionWS = modelPos.xyz;
    positionM = position;

    tangentWS = normalize(vec3(normal * vec4(inTangent, 1.0)).xyz);
    bitangentWS = normalize(vec3(normal * vec4(inBitangent, 1.0)).xyz);
    normalWS = normalize(vec3(normal * vec4(inNormal, 1.0)).xyz);
}
