#version 450

#include "common/global.glsl"

#include "common/binding.glsl"

#include "common/parallax.glsl"

#include "common/shadows.glsl"

#include "common/blinnphong.glsl"

layout(set = MATERIAL_SET, binding = 1) uniform UniformBufferObject {
    vec2 uvScale;
}
params;

layout(set = MATERIAL_SET, binding = 2) uniform sampler2D heightMap;
layout(set = MATERIAL_SET, binding = 3) uniform sampler2DArray layerTextures;
layout(set = MATERIAL_SET, binding = 4) uniform sampler2D splatMap;

layout(location = 0) in vec2 fragUV;
layout(location = 1) in vec3 positionVS;
layout(location = 2) in vec3 tangentWS;
layout(location = 3) in vec3 bitangentWS;
layout(location = 4) in vec3 normalWS;
layout(location = 5) in vec3 positionWS;
layout(location = 6) in vec4 positionM;

layout(location = 0) out vec4 outColor;

void main() {
    //outColor = vec4(normalWS, 1);
    //return;

    vec4 splat = texture(splatMap, fragUV);
    //outColor = splat;
    //return;

    vec3 viewDirWS = normalize(g_camera.position - positionWS);
    vec3 normalWS = normalize(normalWS);

    vec4 diffuse = vec4(0);
    for (int i = 0; i < 4; ++i) {
        diffuse += splat[i] * texture(layerTextures, vec3(params.uvScale * fragUV, i));
    }

    LightingResult lit = BlinnPhong(g_camera, normalWS, viewDirWS, positionWS, 0, g_shading.shadowStrength);

    diffuse *= vec4(lit.diffuse, 1.);

    outColor = vec4(diffuse.rgb, 1);
}