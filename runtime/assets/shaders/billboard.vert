#version 450

#include "common/global.glsl"

#include "common/binding.glsl"

#include "common/vertex.glsl"

layout(location = 0) out vec2 outFragUV;

void main() {
    mat4 model = Model();
    model[0] = vec4(1, 0, 0, 0);
    model[1] = vec4(0, 1, 0, 0);
    model[2] = vec4(0, 0, 1, 0);

    mat4 modelView = g_camera.view * model;
    modelView[0] = vec4(1, 0, 0, modelView[0][3]);
    modelView[1] = vec4(0, 1, 0, modelView[1][3]);
    modelView[2] = vec4(0, 0, 1, modelView[2][3]);

    gl_Position = g_camera.proj * modelView * vec4(inPosition, 1.0);
    outFragUV = inUV;
}