#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(push_constant) uniform Params {
    mat4 mvp;
}
params;

layout(location = 0) out vec4 outPosition;

void main() {
    vec3 position = vec3(0.5);

    switch (gl_VertexIndex) {
    case 0:
    case 7:
    case 16:
        position.z *= -1;
        break;
    case 1:
    case 2:
    case 18:
        position.z *= -1;
        position.x *= -1;
        break;
    case 3:
    case 4:
    case 20:
        position.z *= -1;
        position.x *= -1;
        position.y *= -1;
        break;
    case 5:
    case 6:
    case 22:
        position.z *= -1;
        position.y *= -1;
        break;
    case 8:
    case 15:
    case 17:
        break;
    case 9:
    case 10:
    case 19:
        position.x *= -1;
        break;
    case 11:
    case 12:
    case 21:
        position.x *= -1;
        position.y *= -1;
        break;
    case 13:
    case 14:
    case 23:
        position.y *= -1;
        break;
    }

    gl_Position = params.mvp * vec4(position, 1.0);
    outPosition = gl_Position;
}
