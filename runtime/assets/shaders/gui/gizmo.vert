#version 450
#extension GL_ARB_separate_shader_objects : enable

#define NO_INSTANCE
#include "../common/vertex_in.glsl"

layout(push_constant) uniform Params {
    layout(offset = 0) vec4 mvp;
}
params;

layout(location = 0) out vec4 outPosition;

void main() {
    gl_Position = params.mvp * vec4(inPosition, 1.0);
    outPosition = gl_Position;
}
