#version 450

layout(push_constant) uniform Params {
    layout(offset = 112) vec4 color;
}
params;

layout(location = 0) in vec4 inPosition;
layout(location = 0) out vec4 outColor;

void main() {
    outColor = params.color;
}