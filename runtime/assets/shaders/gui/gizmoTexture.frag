#version 450

layout(set = 0, binding = 0) uniform sampler2D inputTexture;

layout(location = 0) in vec2 inFragUV;
layout(location = 1) in vec4 inColor;

layout(location = 0) out vec4 outFragColor;

void main() {
    vec4 color = texture(inputTexture, inFragUV);
    outFragColor = color * inColor;
}