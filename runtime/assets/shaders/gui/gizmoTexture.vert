#version 450

int index[6] = int[](0, 1, 2, 1, 3, 2);

vec2 positions[4] = vec2[](vec2(-1.0, -1.0), vec2(1.0, -1.0), vec2(-1.0, 1.0), vec2(1.0, 1.0));
vec2 coords[4] = vec2[](vec2(0.0, 0.0), vec2(1.0, 0.0), vec2(0.0, 1.0), vec2(1.0, 1.0));

layout(location = 0) out vec2 outFragUV;
layout(location = 1) out vec4 outColor;

layout(push_constant) uniform Params {
    mat4 mvp;
    vec4 color;
    vec2 size;
}
params;

void main() {
    int i = index[gl_VertexIndex];

    vec4 pos = vec4(params.size * positions[i], 0.0, 1.0);

    gl_Position = params.mvp * pos;
    outFragUV = coords[i];
    outColor = params.color;
}