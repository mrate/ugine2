#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(push_constant) uniform Params {
    mat4 mvp;
}
params;

layout(location = 0) out vec4 outPosition;

void main() {
    float x = -1 + (gl_VertexIndex & 0x2);
    float y = -1 + 0.5 * (gl_VertexIndex & 0x4);
    float z = 1 * (gl_VertexIndex & 0x1);

    vec3 position = vec3(0.1 * x, 0.1 * y, 0.5 * z);

    gl_Position = params.mvp * vec4(position, 1.0);
    outPosition = gl_Position;
}
