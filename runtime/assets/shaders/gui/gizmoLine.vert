#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(push_constant) uniform Line {
    mat4 viewProj;
    vec3 from;
    vec3 to;
}
params;

layout(location = 0) out vec4 outPosition;

void main() {
    gl_Position = params.viewProj * vec4(gl_VertexIndex == 0 ? params.from : params.to, 1.0);
    outPosition = gl_Position;
}
