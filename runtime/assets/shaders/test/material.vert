#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform Buffer {
    mat4 ugineModelView;
    mat4 ugineModelViewProjection;
}
MVP;

layout(set = 1, binding = 0) uniform BufferParam {
    float displace;
    mat4 ugineModel;
    mat4 ugineNormal;
}
vParams;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec2 inUV;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec3 inTangent;
layout(location = 4) in vec3 inBitangent;

layout(location = 0) out vec2 fragUV;
layout(location = 1) out vec3 positionVS;
layout(location = 2) out vec3 tangentWS;
layout(location = 3) out vec3 bitangentWS;
layout(location = 4) out vec3 normalWS;
layout(location = 5) out vec3 positionWS;
layout(location = 6) out vec4 positionM;

void main() {
    vec3 position = inPosition * sin(vParams.displace);
    //vec3 position = inPosition;

    gl_Position = MVP.ugineModelViewProjection * vec4(position, 1.0);

    fragUV = inUV;
    positionVS = (MVP.ugineModelView * vec4(position, 1.0)).xyz;
    tangentWS = mat3(vParams.ugineModel) * inTangent;
    bitangentWS = mat3(vParams.ugineModel) * inBitangent;
    normalWS = normalize(mat3(vParams.ugineNormal) * inNormal);
    positionWS = (vParams.ugineModel * vec4(position, 1.0)).xyz;
    positionM = vec4(position, 1.0);
}
