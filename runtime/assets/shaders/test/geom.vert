#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "../common/global.glsl"

#include "../common/binding.glsl"

#include "../common/vertex_in.glsl"

layout(location = 0) out vec3 normalWS;
layout(location = 1) out mat4 model;

void main() {
    vec3 position = inPosition;

    gl_Position = vec4(position, 1.0);

    mat4 normal = Normal();
    normalWS = normalize(mat3(normal) * inNormal);

    model = Model();
}
