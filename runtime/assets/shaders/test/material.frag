#version 450

layout(set = 2, binding = 0) uniform UniformBufferObject {
    vec4 thisCouldBeSomeColor;
    vec4 param;
}
params;

layout(set = 2, binding = 1) uniform sampler2D inTex;

layout(location = 0) in vec2 fragUV;
layout(location = 1) in vec3 positionVS;
layout(location = 2) in vec3 tangentWS;
layout(location = 3) in vec3 bitangentWS;
layout(location = 4) in vec3 normalWS;
layout(location = 5) in vec3 positionWS;
layout(location = 6) in vec4 positionM;

layout(location = 0) out vec4 outColor;

void main() {
    vec4 tex = texture(inTex, fragUV);
    outColor = params.param * params.thisCouldBeSomeColor * tex;
}