#version 450
#extension GL_ARB_separate_shader_objects : enable

#define CSM_LEVELS 3

#define MAX_LIGHTS 32

#define MAX_DIR_SHADOWS 1
#define MAX_POINT_SHADOWS 1
#define MAX_SPOT_SHADOWS 1

#define MAX_SHADOWS (MAX_DIR_SHADOWS + MAX_POINT_SHADOWS + MAX_SPOT_SHADOWS)

#define TYPE_DIRECTIONAL 0
#define TYPE_POINT 1
#define TYPE_SPOT 2

#define SHADOW_FACTOR 0.75

struct Light {
    vec4 ambientColor;
    vec4 color;
    vec3 position;
    int type;
    vec3 direction;
    int enabled;
    float intensity;
    float spotAngleRad;
    float range;
    int shadowIndex;
    int shadowMapIndex;
    int csmMaxLevel;
};

struct Shadow {
    mat4 lightViewProj[CSM_LEVELS];
};

layout(set = 2, binding = 0) uniform Lights {
    vec3 eyePoseWS;
    int lightCount;
    int shadowCount;
    int hasIbl;
    Light g_lights[MAX_LIGHTS];
    Shadow shadows[MAX_SHADOWS];
}
lights;

layout(set = 2, binding = 1) uniform sampler2DArray g_dirShadowMap[MAX_DIR_SHADOWS];
layout(set = 2, binding = 2) uniform sampler2D g_spotShadowMap[MAX_SPOT_SHADOWS];
layout(set = 2, binding = 3) uniform samplerCube g_pointShadowMap[MAX_POINT_SHADOWS];

// Material.
layout(set = 1, binding = 0) uniform Material {
    vec4 ambientColor;
    vec4 emissiveColor;
    vec4 diffuseColor;
    vec4 specularColor;
    int hasAmbientTexture;
    int hasEmissiveTexture;
    int hasDiffuseTexture;
    int hasSpecularTexture;
    int hasNormalTexture;
    int hasHeightTexture;
    int hasNormalWithHeight;
    float specularPower;
    float opacity;
    float heightScale;
    vec2 uvTiling;
}
material;

layout(set = 1, binding = 1) uniform sampler2D ambientTexture;
layout(set = 1, binding = 2) uniform sampler2D emissiveTexture;
layout(set = 1, binding = 3) uniform sampler2D diffuseTexture;
layout(set = 1, binding = 4) uniform sampler2D specularTexture;
layout(set = 1, binding = 5) uniform sampler2D normalTexture;
layout(set = 1, binding = 6) uniform sampler2D heightTexture;

// Input.
layout(location = 0) in vec2 inFragUV;
layout(location = 1) in vec3 inPositionVS;
layout(location = 2) in vec3 inTangentWS;
layout(location = 3) in vec3 inBitangentWS;
layout(location = 4) in vec3 inNormalWS;
layout(location = 5) in vec3 inPositionWS;
layout(location = 6) in vec4 inPositionM;

layout(location = 0) out vec4 outColor;
layout(location = 1) out vec3 outPosition;
layout(location = 2) out vec3 outNormal;
layout(location = 3) out vec4 outSpecular;

struct LightingResult {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

// Pridejte, nebojte se...
#define EPSILON_D 0.0001

float CalcShadowFactorDir(int shadowIndex, vec4 lightSpacePos, vec2 off, int csm) {
    float shadow = 1.0;

    vec3 shadowCoords = lightSpacePos.xyz / lightSpacePos.w;
    if (shadowCoords.z > -1.0 && shadowCoords.z < 1.0) {
        vec2 normCoords = 0.5 * shadowCoords.xy + 0.5;
        vec2 uv = normCoords + off;
        float depth = texture(g_dirShadowMap[shadowIndex], vec3(uv, csm)).r;

        if (depth + EPSILON_D < shadowCoords.z) {
            shadow = 0.0;
        }
    }

    return shadow;
}

float CalcShadowFactorPCFDir(int shadowIndex, vec4 lightSpacePos, int csm) {
    ivec2 texDim = textureSize(g_dirShadowMap[shadowIndex], 0).xy;
    float scale = 1.0; // 1.5
    float dx = scale * 1.0 / float(texDim.x);
    float dy = scale * 1.0 / float(texDim.y);

    float shadowFactor = 0.0;
    int count = 0;
    int range = 1;

    for (int x = -range; x <= range; x++) {
        for (int y = -range; y <= range; y++) {
            shadowFactor += CalcShadowFactorDir(shadowIndex, lightSpacePos, vec2(dx * x, dy * y), csm);
            count++;
        }
    }
    return shadowFactor / count;
}

float CalcShadowFactorPoint(int shadowIndex, vec3 lightToFragDir, float range) {
    float shadow = 1.0;

    float sampledDepth = texture(g_pointShadowMap[shadowIndex], lightToFragDir).r;
    float distance = length(lightToFragDir);

    if (sampledDepth + 0.02 <= distance) {
        shadow = smoothstep(range * 0.5, 0.0, distance);
    }

    return shadow;
}

float CalcShadowFactor(int shadowIndex, vec4 lightSpacePos, vec2 off) {
    float shadow = 1.0;

    vec3 shadowCoords = lightSpacePos.xyz / lightSpacePos.w;
    if (shadowCoords.z > -1.0 && shadowCoords.z < 1.0) {
        vec2 normCoords = 0.5 * shadowCoords.xy + 0.5;
        vec2 uv = normCoords + off;
        float depth = texture(g_spotShadowMap[shadowIndex], uv).r;

        if (depth + EPSILON_D < shadowCoords.z) {
            shadow = 0.0;
        }
    }

    return shadow;
}

float CalcShadowFactorPCF(int shadowIndex, vec4 lightSpacePos) {
    ivec2 texDim = textureSize(g_spotShadowMap[shadowIndex], 0);
    float scale = 1.5;
    float dx = scale * 1.0 / float(texDim.x);
    float dy = scale * 1.0 / float(texDim.y);

    float shadowFactor = 0.0;
    int count = 0;
    int range = 1;

    for (int x = -range; x <= range; x++) {
        for (int y = -range; y <= range; y++) {
            shadowFactor += CalcShadowFactor(shadowIndex, lightSpacePos, vec2(dx * x, dy * y));
            count++;
        }
    }
    return shadowFactor / count;
}

float CalcDiff(vec3 lightDir, vec3 normal) {
    return max(dot(normal, lightDir), 0.0);
}

float CalcSpec(vec3 viewDir, vec3 lightDir, vec3 normal) {
    vec3 halfwayDir = normalize(lightDir + viewDir);
    return pow(max(dot(normal, halfwayDir), 0.0), material.specularPower);
}

float CalcSpotCone(Light light, vec3 lightDir) {
    // If the cosine angle of the light's direction
    // vector and the vector from the light source to the point being
    // shaded is less than minCos, then the spotlight contribution will be 0.
    float minCos = cos(light.spotAngleRad);
    // If the cosine angle of the light's direction vector
    // and the vector from the light source to the point being shaded
    // is greater than maxCos, then the spotlight contribution will be 1.
    float maxCos = mix(minCos, 1., 0.5);
    float cosAngle = dot(light.direction, -lightDir);
    // Blend between the minimum and maximum cosine angles.
    return smoothstep(minCos, maxCos, cosAngle);
}

float DoAttenuation(Light light, float distance) {
    //return attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
    return 1.0f - smoothstep(light.range * 0.75, light.range, distance);
}

LightingResult CalcDirLight(Light light, vec3 normal, vec3 viewDir) {
    vec3 lightDir = light.direction;

    float diff = CalcDiff(lightDir, normal);
    float spec = CalcSpec(viewDir, lightDir, normal);

    LightingResult result;
    result.ambient = light.ambientColor.rgb * light.intensity;
    result.diffuse = light.color.rgb * diff * light.intensity;
    result.specular = light.color.rgb * spec * light.intensity;
    return result;
}

LightingResult CalcPointLight(Light light, vec3 normal, vec3 viewDir, vec3 fragPos) {
    vec3 lightDir = normalize(light.position - fragPos);

    float diff = CalcDiff(lightDir, normal);
    float spec = CalcSpec(viewDir, lightDir, normal);

    float distance = length(light.position - fragPos);
    float attenuation = DoAttenuation(light, distance);

    LightingResult result;
    result.ambient = light.ambientColor.rgb * attenuation * light.intensity;
    result.diffuse = light.color.rgb * diff * attenuation * light.intensity;
    result.specular = light.color.rgb * spec * attenuation * light.intensity;
    return result;
}

LightingResult CalcSpotLight(Light light, vec3 normal, vec3 viewDir, vec3 fragPos) {
    vec3 lightDir = light.position - fragPos;
    float distance = length(lightDir);
    lightDir = normalize(lightDir);

    float attenuation = DoAttenuation(light, distance);
    float spotIntensity = CalcSpotCone(light, lightDir);

    float diff = CalcDiff(lightDir, normal);
    float spec = CalcSpec(viewDir, lightDir, normal);

    LightingResult result;
    result.ambient = light.ambientColor.rgb * attenuation * light.intensity;
    result.diffuse = light.color.rgb * diff * attenuation * spotIntensity * light.intensity;
    result.specular = light.color.rgb * spec * attenuation * spotIntensity * light.intensity;
    return result;
}

int CsmIndex(vec3 positionWS) {
    float distance = length(lights.eyePoseWS - positionWS);
    if (distance < 10) {
        return 0;
    } else if (distance < 20) {
        return 1;
    } else {
        return 2;
    }
}

LightingResult DoLighting(vec3 normal, vec3 viewDir, vec3 fragPos) {

    LightingResult result = LightingResult(vec3(0.), vec3(0.), vec3(0.));

    for (int i = 0; i < lights.lightCount; ++i) {
        if (lights.g_lights[i].enabled < 1) {
            continue;
        }

        // If this light generates shadow and fragment is not visible by this light then skip it.
        float shadowFactor = 1.0;

        LightingResult partialResult = LightingResult(vec3(0.), vec3(0.), vec3(0.));
        switch (lights.g_lights[i].type) {
        case TYPE_DIRECTIONAL:
            if (lights.g_lights[i].shadowIndex >= 0) {
                int shadowIndex = lights.g_lights[i].shadowIndex;
                int shadowMapIndex = lights.g_lights[i].shadowMapIndex;
                int csm = min(CsmIndex(fragPos), lights.g_lights[i].csmMaxLevel);
                shadowFactor = CalcShadowFactorPCFDir(shadowMapIndex, lights.shadows[shadowIndex].lightViewProj[csm] * vec4(fragPos, 1.0), csm);
            }

            partialResult = CalcDirLight(lights.g_lights[i], normal, viewDir);
            break;
        case TYPE_POINT:
            if (lights.g_lights[i].shadowIndex >= 0) {
                int shadowMapIndex = lights.g_lights[i].shadowMapIndex;
                shadowFactor = CalcShadowFactorPoint(shadowMapIndex, fragPos - lights.g_lights[i].position, lights.g_lights[i].range);
            }
            partialResult = CalcPointLight(lights.g_lights[i], normal, viewDir, fragPos);
            break;
        case TYPE_SPOT:
            if (lights.g_lights[i].shadowIndex >= 0) {
                int shadowIndex = lights.g_lights[i].shadowIndex;
                int shadowMapIndex = lights.g_lights[i].shadowMapIndex;
                shadowFactor = CalcShadowFactorPCF(shadowMapIndex, lights.shadows[shadowIndex].lightViewProj[0] * vec4(fragPos, 1.0));
            }
            partialResult = CalcSpotLight(lights.g_lights[i], normal, viewDir, fragPos);
            break;
        }

        shadowFactor = shadowFactor + (1 - shadowFactor) * SHADOW_FACTOR;

        result.ambient += partialResult.ambient;
        result.diffuse += shadowFactor * partialResult.diffuse;
        result.specular += shadowFactor * partialResult.specular;
    }

    // Clamp specular highlights, otherwise it's just too bright and ugly...
    result.specular = clamp(result.specular, 0.0, 1.0);
    return result;
}

// Sascha Willems examples.
#define parallaxBias -0.02
#define parallaxNumLayers 48.0

//#define Parallax    ParallaxMapping
//#define Parallax    ParallaxMappingSteep
#define Parallax ParallaxMappingOclusion

float HeightMap(vec2 uv) {
    float height = 0.0;
    if (material.hasNormalWithHeight != 0.0) {
        float a = texture(normalTexture, uv).a;
        height = material.hasNormalWithHeight > 0.0 ? a : 1.0 - a;
    } else {
        height = texture(heightTexture, uv).r;
    }
    return height;
}

vec2 ParallaxMapping(vec2 texCoords, vec3 viewDirWS) {
    float height = HeightMap(texCoords);
    vec2 p = viewDirWS.xy * (height * (material.heightScale * 0.5) + parallaxBias) / viewDirWS.z;
    return texCoords - p;
}

vec2 ParallaxMappingSteep(vec2 uv, vec3 viewDir) {
    float layerDepth = 1.0 / parallaxNumLayers;
    float currLayerDepth = 0.0;
    vec2 deltaUV = viewDir.xy * material.heightScale / (viewDir.z * parallaxNumLayers);
    vec2 currUV = uv;
    float height = HeightMap(currUV);
    for (int i = 0; i < parallaxNumLayers; i++) {
        currLayerDepth += layerDepth;
        currUV -= deltaUV;
        height = HeightMap(currUV);
        if (height < currLayerDepth) {
            break;
        }
    }
    return currUV;
}

vec2 ParallaxMappingOclusion(vec2 uv, vec3 viewDir) {
    float layerDepth = 1.0 / parallaxNumLayers;
    float currLayerDepth = 0.0;
    vec2 deltaUV = viewDir.xy * material.heightScale / (viewDir.z * parallaxNumLayers);
    vec2 currUV = uv;
    float height = HeightMap(currUV);
    for (int i = 0; i < parallaxNumLayers; i++) {
        currLayerDepth += layerDepth;
        currUV -= deltaUV;
        height = HeightMap(currUV);
        if (height < currLayerDepth) {
            break;
        }
    }
    vec2 prevUV = currUV + deltaUV;
    float nextDepth = height - currLayerDepth;
    float prevDepth = HeightMap(prevUV) - currLayerDepth + layerDepth;
    return mix(currUV, prevUV, nextDepth / (nextDepth - prevDepth));
}

void main() {
    outPosition = inPositionVS;

    vec3 viewDirWS = normalize(lights.eyePoseWS - inPositionWS);
    vec3 normalWS = normalize(inNormalWS);

    vec2 texCoords = inFragUV * material.uvTiling;
    if (material.hasHeightTexture > 0 || material.hasNormalWithHeight != 0) {
        mat3 TBN = transpose(mat3(inTangentWS, inBitangentWS, inNormalWS));
        vec3 tangentView = TBN * lights.eyePoseWS;
        vec3 tangentFragPos = TBN * inPositionWS;

        vec3 tangentViewDir = normalize(tangentView - tangentFragPos);

        texCoords = Parallax(texCoords, tangentViewDir);

        if (texCoords.x > 1.0 || texCoords.y > 1.0 || texCoords.x < 0.0 || texCoords.y < 0.0) {
            discard;
        }
    }

    vec4 ambient = material.ambientColor * (material.hasAmbientTexture > 0 ? texture(ambientTexture, texCoords) : vec4(1.0));
    vec4 emissive = material.emissiveColor + (material.hasEmissiveTexture > 0 ? texture(emissiveTexture, texCoords) : vec4(0.0));
    vec4 diffuse = material.diffuseColor * (material.hasDiffuseTexture > 0 ? texture(diffuseTexture, texCoords) : vec4(1.0));

    if (material.hasNormalTexture > 0) {
        vec3 sampledNormal = (texture(normalTexture, texCoords).rgb - 0.5) * 2.0;
        mat3 TBN = mat3(inTangentWS, inBitangentWS, inNormalWS);
        normalWS = TBN * sampledNormal;
    }

    outNormal = normalWS;

    LightingResult lit = DoLighting(normalWS, viewDirWS, inPositionWS);

    ambient *= vec4(lit.ambient, 1.);
    diffuse *= vec4(lit.diffuse, 1.);

    vec4 specular = vec4(0.0);
    if (material.specularPower > 1.0f) {
        // If specular power is too low, don't use it.
        vec4 specularColor = material.specularColor * (material.hasSpecularTexture > 0 ? texture(specularTexture, texCoords) : vec4(1.0));
        specular = specularColor * vec4(lit.specular, 1.);
    }

    outSpecular = specular;

    vec4 color = (ambient + emissive + (diffuse + specular));
    outColor = vec4(color.rgb, color.a * material.opacity);

    // Debug CSM
    //int csm = CsmIndex(inPositionWS);
    //vec4 csmColor = csm < 1 ? vec4(1.0, 0.0, 0.0, 1.0) : csm < 2 ? vec4(0.0, 1.0, 0.0, 1.0) : vec4(0.0, 0.0, 1.0, 1.0);
    // outColor += 0.2 * csmColor;
}