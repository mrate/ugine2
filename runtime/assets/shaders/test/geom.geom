#version 450

#include "../common/global.glsl"

#include "../common/binding.glsl"

layout (triangles) in;
layout (line_strip, max_vertices = 6) out;

layout (location = 0) in vec3 inNormal[];
layout (location = 1) in mat4 model[];

layout (location = 0) out vec3 outColor;

void main(void)
{	
	float normalLength = 0.5;
	for(int i=0; i<gl_in.length(); i++)
	{
		vec3 pos = gl_in[i].gl_Position.xyz;
		vec3 normal = inNormal[i].xyz;

		gl_Position = gl_in[i].gl_Position;
		outColor = vec3(1.0, 0.0, 0.0);
		EmitVertex();

		gl_Position = g_camera.proj * (model[i] * vec4(pos + normal * normalLength, 1.0));
		outColor = vec3(0.0, 0.0, 1.0);
		EmitVertex();

		EndPrimitive();
	}
}