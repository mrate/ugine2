#version 450

#include "../common/global.glsl"

#include "../common/binding.glsl"

layout(location = 0) in vec3 inColor;

layout(location = 0) out vec4 outColor;

void main() {
    outColor = vec4(inColor, 1);
}