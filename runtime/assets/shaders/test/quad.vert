#version 450

layout(set = 0, binding = 0) uniform Buffer {
    mat4 model;
    mat4 modelView;
    mat4 modelViewProj;
    mat4 normalMatrix;
}
MVP;

layout(set = 1, binding = 0) uniform BufferParam {
    float displace;
}
vParams;

vec2 positions[6] = vec2[](vec2(-0.5, -0.5), vec2(0.5, -0.5), vec2(-0.5, 0.5), vec2(0.5, -0.5), vec2(0.5, 0.5), vec2(-0.5, 0.5));

layout(location = 0) out vec2 fragUV;

void main() {
    vec2 p = positions[gl_VertexIndex];
    gl_Position = vec4(p, 0.0, 1.0);

    fragUV = p * 0.5 + 0.5;
}