#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform Buffer {
    mat4 ugineModel;
    mat4 ugineModelView;
    mat4 ugineModelViewProjection;
    mat4 ugineNormal;
}
mvp;

layout(set = 1, binding = 1) readonly buffer InstanceMatrices {
    vec4 offsetSpeed[];
}
offsets;

layout(set = 1, binding = 0) uniform Time {
    float displacement;
    float time;
}
params;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec2 inUV;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec3 inTangent;
layout(location = 4) in vec3 inBitangent;

layout(location = 0) out vec2 fragUV;
layout(location = 1) out vec3 positionVS;
layout(location = 2) out vec3 tangentWS;
layout(location = 3) out vec3 bitangentWS;
layout(location = 4) out vec3 normalWS;
layout(location = 5) out vec3 positionWS;
layout(location = 6) out vec4 positionM;

mat4 rotateX(float angle) {
    float cs = cos(angle);
    float sn = sin(angle);

    return mat4(1.0, 0.0, 0.0, 0.0, // 1. column
        0.0, cs, sn, 0.0, // 2. column
        0.0, -sn, cs, 0.0, // 3. column
        0.0, 0.0, 0.0, 1.0); // 4. column
}

mat4 translate(vec3 off) {
    return mat4(1.0, 0.0, 0.0, 0.0, // 1. column
        0.0, 1.0, 0.0, 0.0, // 2. column
        0.0, 0.0, 1.0, 0.0, // 3. column
        off.x, off.y, off.z, 1.0); // 4. column
}

void main() {
    vec4 offSpeed = offsets.offsetSpeed[gl_InstanceIndex];

    float angle = params.time * 0.02f * offSpeed.w;
    mat4 trans = translate(vec3(offSpeed.xy, sin(params.time * 0.001f * offSpeed.w))) * rotateX(angle);

    //mat4 rot = mat4(1.0, 0.0, 0.0, 0.0, // 1. column
    //    0.0, 1.0, 0.0, 0.0, // 2. column
    //    0.0, 0.0, 1.0, 0.0, // 3. column
    //    0.0, 0.0, 0.0, 1.0); // 4. column

    mat4 model = mvp.ugineModel * trans;
    mat4 normalMatrix = inverse(model);

    gl_Position = mvp.ugineViewProjection * model * vec4(inPosition, 1.0);

    fragUV = inUV;
    positionVS = (mvp.ugineModelView * model * vec4(inPosition, 1.0)).xyz;
    positionWS = (model * vec4(inPosition, 1.0)).xyz;
    positionM = vec4(inPosition, 1.0);

    tangentWS = normalize(vec3(normalMatrix * vec4(inTangent, 1.0)).xyz);
    bitangentWS = normalize(vec3(normalMatrix * vec4(inBitangent, 1.0)).xyz);
    normalWS = normalize(vec3(normalMatrix * vec4(inNormal, 1.0)).xyz);
}
