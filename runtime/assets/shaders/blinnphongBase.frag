#include "common/global.glsl"

#include "common/binding.glsl"

#include "common/parallax.glsl"
#include "common/shadows.glsl"

#include "common/blinnphong.glsl"

// Material.
layout(set = MATERIAL_SET, binding = 0) uniform Material {
    vec4 ambientColor;
    vec4 emissiveColor;
    vec4 diffuseColor;
    vec4 specularColor;
    int hasAmbientTexture;
    int hasEmissiveTexture;
    int hasDiffuseTexture;
    int hasSpecularTexture;
    int hasNormalTexture;
    int hasHeightTexture;
    int hasNormalWithHeight;
    float specularPower;
    float opacity;
    float heightScale;
    vec2 uvTiling;
    float emissiveMultiplier;
    float refractIndex;
}
material;

layout(set = MATERIAL_SET, binding = 1) uniform sampler2D ambientTexture;
layout(set = MATERIAL_SET, binding = 2) uniform sampler2D emissiveTexture;
layout(set = MATERIAL_SET, binding = 3) uniform sampler2D diffuseTexture;
layout(set = MATERIAL_SET, binding = 4) uniform sampler2D specularTexture;
layout(set = MATERIAL_SET, binding = 5) uniform sampler2D normalTexture;
layout(set = MATERIAL_SET, binding = 6) uniform sampler2D heightTexture;

// Input.
layout(location = 0) in vec2 inFragUV;
layout(location = 1) in vec3 inPositionVS;

#ifndef BLINNPHONG_DEPTH
layout(location = 2) in vec3 inTangentWS;
layout(location = 3) in vec3 inBitangentWS;
layout(location = 4) in vec3 inNormalWS;
layout(location = 5) in vec3 inPositionWS;
layout(location = 6) in vec4 inPositionM;

layout(location = 0) out vec4 outColor;
#endif // BLINNPHONG_DEPTH

void main() {
    //outPosition = inPositionVS;
    vec2 texCoords = inFragUV * material.uvTiling;

#ifndef BLINNPHONG_DEPTH
    vec3 viewDirWS = normalize(g_camera.position - inPositionWS);
    vec3 normalWS = normalize(inNormalWS);

    if (material.hasHeightTexture > 0 || material.hasNormalWithHeight != 0) {
        mat3 TBN = transpose(mat3(inTangentWS, inBitangentWS, inNormalWS));
        vec3 tangentView = TBN * g_camera.position;
        vec3 tangentFragPos = TBN * inPositionWS;

        vec3 tangentViewDir = normalize(tangentView - tangentFragPos);

        texCoords = Parallax(texCoords, tangentViewDir, material.heightScale, normalTexture, heightTexture, material.hasNormalWithHeight);

        if (texCoords.x > 1.0 || texCoords.y > 1.0 || texCoords.x < 0.0 || texCoords.y < 0.0) {
            discard;
        }
    }
#endif
    
    vec4 diffuse = material.diffuseColor * (material.hasDiffuseTexture > 0 ? texture(diffuseTexture, texCoords) : vec4(1.0));

#ifdef BLINNPHONG_DEPTH
    if (diffuse.a < 0.5) {
        discard;
    }
#else

#ifndef BLINNPHONG_TRANSPARENT
    if (diffuse.a < 0.5) {
        discard;
    }
#endif

    vec2 screenSpace = gl_FragCoord.xy / g_camera.resolution;
    float ao = texture(g_ao, screenSpace).r;
    vec4 ambient = material.ambientColor * (material.hasAmbientTexture > 0 ? texture(ambientTexture, texCoords) : vec4(1.0));

    vec4 emissive = material.emissiveColor + (material.hasEmissiveTexture > 0 ? texture(emissiveTexture, texCoords) : vec4(0.0));
    emissive *= material.emissiveMultiplier;

    if (material.hasNormalTexture > 0) {
        vec3 sampledNormal = (texture(normalTexture, texCoords).rgb - 0.5) * 2.0;
        mat3 TBN = mat3(inTangentWS, inBitangentWS, inNormalWS);
        normalWS = TBN * sampledNormal;
    }

    LightingResult lit = BlinnPhong(g_camera, normalWS, viewDirWS, inPositionWS, material.specularPower, g_shading.shadowStrength);

    ambient *= vec4(lit.ambient, 1.);
    diffuse *= vec4(lit.diffuse, 1.);

    vec4 specular = vec4(0.0);
    if (material.specularPower > 1.0f) {
        // If specular power is too low, don't use it.
        vec4 specularColor = material.specularColor * (material.hasSpecularTexture > 0 ? texture(specularTexture, texCoords) : vec4(1.0));
        specular = specularColor * vec4(lit.specular, 1.);
    }

    vec4 color = (ambient + emissive + (diffuse + specular));
    outColor = vec4(ao * color.rgb, color.a * material.opacity);

    // Debug CSM
    //int csm = CsmIndex(inPositionWS);
    //vec4 csmColor = csm < 1 ? vec4(1.0, 0.0, 0.0, 1.0) : csm < 2 ? vec4(0.0, 1.0, 0.0, 1.0) : vec4(0.0, 0.0, 1.0, 1.0);
    // outColor += 0.2 * csmColor;
#endif // BLINNPHONG_DEPTH
}