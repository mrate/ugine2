#extension GL_ARB_separate_shader_objects : enable

#include "common/global.glsl"

#include "common/binding.glsl"

#include "common/vertex_in.glsl"

invariant gl_Position;

layout(location = 0) out vec2 fragUV;
layout(location = 1) out vec3 positionVS;

#ifndef DEPTH
layout(location = 2) out vec3 tangentWS;
layout(location = 3) out vec3 bitangentWS;
layout(location = 4) out vec3 normalWS;
layout(location = 5) out vec3 positionWS;
layout(location = 6) out vec4 positionM;
#endif

void main() {
    mat4 model = Model();
    mat4 normal = Normal();

    vec4 position = vec4(inPosition, 1.0);

#ifdef VS_USE_WIND
    float windweight = 1.0;
    float waveoffset = dot(position.xyz, g_frame.windDirection) * g_frame.windWaveSize + (position.x + position.y + position.z) * g_frame.windRandomness;
    vec3 wavedir = g_frame.windDirection * windweight * position.y * 0.1;
    vec3 wind = sin(g_frame.time * g_frame.windSpeed + waveoffset) * wavedir;
    position.xyz += wind;
#endif

    vec4 modelPos = model * position;
    vec4 projViewModel = g_camera.viewProj * modelPos;

    gl_Position = projViewModel;

    fragUV = inUV;

    vec4 viewModel = g_camera.view * modelPos;
    positionVS = viewModel.xyz;
#ifndef DEPTH
    positionWS = modelPos.xyz;
    positionM = vec4(inPosition, 1.0);

    tangentWS = normalize(vec3(normal * vec4(inTangent, 1.0)).xyz);
    bitangentWS = normalize(vec3(normal * vec4(inBitangent, 1.0)).xyz);
    normalWS = normalize(vec3(normal * vec4(inNormal, 1.0)).xyz);
#endif
}
