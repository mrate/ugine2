#version 450

#include "common/global.glsl"

#include "common/binding.glsl"

int index[6] = int[](0, 1, 2, 2, 1, 3);
vec2 positions[4] = vec2[](vec2(-1.0, -1.0), vec2(1.0, -1.0), vec2(-1.0, 1.0), vec2(1.0, 1.0));
vec2 coords[4] = vec2[](vec2(0.0, 0.0), vec2(1.0, 0.0), vec2(0.0, 1.0), vec2(1.0, 1.0));

layout(location = 0) out vec2 outFragUV;
layout(location = 1) out vec4 outColor;

struct Particle {
    vec4 color;
    vec3 position;
    vec3 speed;
    vec3 life;
    vec2 size;
};

layout(set = MATERIAL_SET, binding = 1) buffer Particles {
    Particle particles[];
}
data;

layout(set = MATERIAL_SET, binding = 2) buffer Live {
    uint index[];
}
live;

void main() {
    int vIndex = index[gl_VertexIndex];

    uint particleIndex = live.index[gl_InstanceIndex];
    Particle particle = data.particles[particleIndex];

    mat4 model;
    model[0] = vec4(1, 0, 0, 0);
    model[1] = vec4(0, 1, 0, 0);
    model[2] = vec4(0, 0, 1, 0);
    model[3] = vec4(particle.position, 1);

    mat4 modelView = g_camera.view * model;
    modelView[0] = vec4(1, 0, 0, modelView[0][3]);
    modelView[1] = vec4(0, 1, 0, modelView[1][3]);
    modelView[2] = vec4(0, 0, 1, modelView[2][3]);

    gl_Position = g_camera.proj * modelView * vec4(particle.size * positions[vIndex], 0.0, 1.0);
    outFragUV = coords[vIndex];

    outColor = particle.life.z * particle.color;
}