#version 450

#include "common/global.glsl"

#include "common/binding.glsl"

layout(set = MATERIAL_SET, binding = 0) uniform UniformBufferObject {
    vec4 color;
}
params;

layout(location = 0) out vec4 outColor;

void main() {
    outColor = params.color;
}