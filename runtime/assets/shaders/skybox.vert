#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "common/global.glsl"

#include "common/binding.glsl"

#define NO_INSTANCE
#include "common/vertex_in.glsl"

layout(location = 0) out vec3 fragUVW;
layout(location = 1) out vec3 positionVS;

void main() {
    fragUVW = inPosition;

    mat4 modelView = g_camera.view;
    modelView[3] = vec4(0, 0, 0, modelView[3][3]);

    vec4 pos = g_camera.proj * modelView * vec4(inPosition, 1.0);
    // ww trick: https://learnopengl.com/PBR/IBL/Diffuse-irradiance
    gl_Position = pos.xyww;
}
