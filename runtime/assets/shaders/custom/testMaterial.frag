#version 450

#include "../common/global.glsl"

#include "../common/binding.glsl"

layout(set = MATERIAL_SET, binding = 0) uniform UniformBufferObject {
    vec4 thisCouldBeSomeColor;
    vec4 param;
}
params;

layout(set = MATERIAL_SET, binding = 1) uniform sampler2D inTex;

layout(location = 0) in vec2 fragUV;

layout(location = 0) out vec4 outColor;

void main() {
    vec4 tex = texture(inTex, fragUV);
    outColor = params.thisCouldBeSomeColor * tex * vec4(1, 1, 1, 1);
}