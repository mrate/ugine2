#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "../common/global.glsl"

#include "../common/binding.glsl"

#include "../common/vertex_in.glsl"

layout(location = 0) out vec2 fragUV;

void main() {
    vec3 position = inPosition;

    gl_Position = g_camera.viewProj * Model() * vec4(position, 1.0);

    fragUV = inUV;
}
