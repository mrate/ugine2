#include "../common/global.glsl"

#include "../common/binding.glsl"

layout(set = MATERIAL_SET, binding = 0) uniform UniformBufferObject {
    float threshold;
    float range;
}
params;

layout(set = MATERIAL_SET, binding = 1) uniform sampler2D sourceTexture;
layout(set = MATERIAL_SET, binding = 2) uniform sampler2D noiseTexture;

layout(location = 0) in vec2 fragUV;

#ifndef DEPTH
layout(location = 0) out vec4 outColor;
#endif

void main() {
    float noise = texture(noiseTexture, fragUV).r;

    float tr = 1 + sin(0.5 * g_frame.time);
    //float tr = params.threshold;
    float tMin = tr - 0.5 * params.range;
    float tMax = tr + 0.5 * params.range;

    if (noise < tMin) {
        discard;
    }

#ifndef DEPTH
    vec3 burn1 = vec3(0.99, 0.58, 0.114);
    vec3 burn2 = vec3(0.9, 0.5, 0.08);
    vec3 burn3 = vec3(0.9, 0.392, 0.078);

    vec4 srcColor = texture(sourceTexture, fragUV); 
    float burnVal = smoothstep(0, params.range, abs(noise - tr) / params.range);
    vec3 burnColor = mix(burn1, burn3, burnVal);

    outColor = vec4(mix(burnColor, srcColor.rgb, smoothstep(tMin, tMax, noise)), srcColor.a);
#endif // DEPTH
}