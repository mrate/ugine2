#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform Buffer {
    mat4 ugineModel;
    mat4 ugineNormal;
    mat4 ugineModelView;
    mat4 ugineModelViewProjection;
    vec3 wind;
}
params;

layout(set = 0, binding = 1) buffer Positions {
    vec4 positions[];
}
pos;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inTangent;
layout(location = 3) in vec3 inBitangent;
layout(location = 4) in vec2 inUV;

layout(location = 0) out vec2 fragUV;
layout(location = 1) out vec3 positionVS;
layout(location = 2) out vec3 tangentWS;
layout(location = 3) out vec3 bitangentWS;
layout(location = 4) out vec3 normalWS;
layout(location = 5) out vec3 positionWS;
layout(location = 6) out vec4 positionM;

void main() {
    //offset += vec3(0.125 * gl_InstanceIndex / 10, 0, 0.125 * mod(gl_InstanceIndex, 10) + 0.2 * (gl_InstanceIndex & 1));

    vec3 offset = pos.positions[gl_InstanceIndex].xyz;
    offset += inPosition.y * params.wind;

    vec4 position = vec4(inPosition + offset, 1.0);

    //
    gl_Position = params.ugineModelViewProjection * position;
    fragUV = inUV;

    positionVS = (params.ugineModelView * position).xyz;
    positionWS = (params.ugineModel * vec4(inPosition, 1.0)).xyz;
    positionM = vec4(inPosition, 1.0);

    tangentWS = normalize(vec3(params.ugineNormal * vec4(inTangent, 1.0)).xyz);
    bitangentWS = normalize(vec3(params.ugineNormal * vec4(inBitangent, 1.0)).xyz);
    normalWS = normalize(vec3(params.ugineNormal * vec4(inNormal, 1.0)).xyz);
}
