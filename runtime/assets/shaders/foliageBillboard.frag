#version 450

#include "common/global.glsl"

#include "common/binding.glsl"

layout(set = MATERIAL_SET, binding = 0) uniform sampler2D diffuseTexture;

layout(location = 0) in vec2 fragUV;

layout(location = 0) out vec4 outFragColor;

void main() {
    vec4 color = texture(diffuseTexture, fragUV);
    if (color.a < 0.5) {
        discard;
    }

    outFragColor = color;
}