#version 450

layout(push_constant) uniform Params {
    vec3 color;
}
params;

layout(set = 0, binding = 0) uniform sampler2D inputTexture;
layout(set = 0, binding = 1) uniform usampler2D inputStencil;

layout(location = 0) in vec2 fragUV;

layout(location = 0) out vec4 outFragColor;

#define KERNEL_SIZE 1

void main() {
    vec3 inputColor = texture(inputTexture, fragUV).rgb;
    ivec2 size = textureSize(inputStencil, 0);
    
    ivec2 posStencil = ivec2(size * fragUV);

    float acc = 0;
    float samples = 0; 
    for (int x = -KERNEL_SIZE; x <= KERNEL_SIZE; ++x) {
        for (int y = -KERNEL_SIZE; y <= KERNEL_SIZE; ++y) {
            samples += 1;
            acc += (texelFetch(inputStencil, posStencil + ivec2(x, y), 0).r & UGINE_OUTLINE_STENCIL) > 0 ? 1 : 0;
        }
    }

    outFragColor = vec4(acc > 0 && acc < samples ? params.color : inputColor, 1);
}