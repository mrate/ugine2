#version 450

layout(set = 2, binding = 0) uniform UniformBufferObject {
    vec4 color;
}
params;

layout(location = 0) out vec4 outColor;

void main() {
    outColor = params.color;
}