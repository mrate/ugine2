#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 fragUV;
layout(location = 1) in vec3 positionVS;

layout(location = 0) out float outColor;

void main() {
    // We are rendering from the light POV, so the distance to fragment is it's position in VS.
    vec3 lightVec = positionVS;
    outColor = length(lightVec);
}