#version 450

layout(push_constant) uniform Params {
    vec2 nearFar;
}
params;

layout(set = 0, binding = 0) uniform sampler2D inputTexture;

layout(location = 0) in vec2 fragUV;

layout(location = 0) out vec4 outFragColor;

float LinearizeDepth(float depth) {
    float n = params.nearFar.x;
    float f = params.nearFar.y;
    float z = depth;
    return (2.0 * n) / (f + n - z * (f - n));
}

void main() {
    float depth = texture(inputTexture, fragUV).r;
    outFragColor = vec4(vec3(1.0 - LinearizeDepth(depth)), 1.0);
    //outFragColor = depth == 1.0 ? vec4(1.0, 0.0, 0.0, 1.0) : vec4(0.0, 1.0, 0.0, 1.0);
}