#version 450

layout(set = 1, binding = 0) uniform sampler2D grass;

layout(location = 0) in vec2 fragUV;
layout(location = 1) in vec3 positionVS;
layout(location = 2) in vec3 tangentWS;
layout(location = 3) in vec3 bitangentWS;
layout(location = 4) in vec3 normalWS;
layout(location = 5) in vec3 positionWS;
layout(location = 6) in vec4 positionM;

layout(location = 0) out vec4 outColor;

void main() {
    vec4 c = texture(grass, fragUV);
    if (c.a < 0.5) {
        discard;
    }
    outColor = c;

    //outColor = texture(grass, fragUV);
    //outColor = vec4(1, 0, 0, 1);
}