#include "common/global.glsl"

#include "common/binding.glsl"

#include "common/parallax.glsl"
#include "common/shadows.glsl"

#include "common/pbr.glsl"

// Material.
layout(set = MATERIAL_SET, binding = 0) uniform Material {
    vec4 albedo;
    vec4 emissive;
    vec2 uvTiling;
    float opacity;
    float heightScale;
    float metallic;
    float roughness;
    float ao;
    float emissiveMultiplier;
    int hasAlbedoTexture;
    int hasMetallicTexture;
    int hasRoughnessTexture;
    int hasAoTexture;
    int hasEmissiveTexture;
    int hasNormalTexture;
    int hasHeightTexture;
    int hasNormalWithHeight;
}
material;

layout(set = MATERIAL_SET, binding = 1) uniform sampler2D albedoTexture;
layout(set = MATERIAL_SET, binding = 2) uniform sampler2D metallicTexture;
layout(set = MATERIAL_SET, binding = 3) uniform sampler2D roughnessTexture;
layout(set = MATERIAL_SET, binding = 4) uniform sampler2D aoTexture;
layout(set = MATERIAL_SET, binding = 5) uniform sampler2D emissiveTexture;
layout(set = MATERIAL_SET, binding = 6) uniform sampler2D normalTexture;
layout(set = MATERIAL_SET, binding = 7) uniform sampler2D heightTexture;

// Input.
layout(location = 0) in vec2 inFragUV;
layout(location = 1) in vec3 inPositionVS;

#ifndef PBR_DEPTH
layout(location = 2) in vec3 inTangentWS;
layout(location = 3) in vec3 inBitangentWS;
layout(location = 4) in vec3 inNormalWS;
layout(location = 5) in vec3 inPositionWS;
layout(location = 6) in vec4 inPositionM;

layout(location = 0) out vec4 outColor;
#endif // PBR_DEPTH

void main() {
    //outPosition = inPositionVS;
    vec2 texCoords = inFragUV * material.uvTiling;

#ifndef PBR_DEPTH
    vec3 viewDirWS = normalize(g_camera.position - inPositionWS);
    vec3 normalWS = normalize(inNormalWS);

    // Parallax mapping
    if (material.hasHeightTexture > 0 || material.hasNormalWithHeight != 0) {
        mat3 TBN = transpose(mat3(inTangentWS, inBitangentWS, inNormalWS));
        vec3 tangentView = TBN * g_camera.position;
        vec3 tangentFragPos = TBN * inPositionWS;

        vec3 tangentViewDir = normalize(tangentView - tangentFragPos);

        texCoords = Parallax(texCoords, tangentViewDir, material.heightScale, normalTexture, heightTexture, material.hasNormalWithHeight);

        if (texCoords.x > 1.0 || texCoords.y > 1.0 || texCoords.x < 0.0 || texCoords.y < 0.0) {
            discard;
        }
    }
#endif // PBR_DEPTH

    // TODO: Skip testing. If texture is not present then black texture is bound anyway.
    vec4 albedoFull = material.albedo * (material.hasAlbedoTexture > 0 ? texture(albedoTexture, texCoords) : vec4(1.0));

#ifdef PBR_DEPTH
    if (albedoFull.a < 0.5) {
        discard;
    }
#else

#ifndef PBR_TRANSPARENT
    if (albedoFull.a < 0.5) {
        discard;
    }
#endif

    vec2 screenSpace = gl_FragCoord.xy / g_camera.resolution;
    float gAo = texture(g_ao, screenSpace).r;

    vec3 emissive = material.emissive.rgb + (material.hasEmissiveTexture > 0 ? texture(emissiveTexture, texCoords).rgb : vec3(0.0));
    vec3 albedo = albedoFull.rgb;
    float metallic = material.metallic * (material.hasMetallicTexture > 0 ? texture(metallicTexture, texCoords).b : 1.0);
    float roughness = material.roughness
        * (material.hasRoughnessTexture > 0 ? texture(roughnessTexture, texCoords).r
                                            : material.hasMetallicTexture > 0 ? texture(metallicTexture, texCoords).g : 1.0);
    float ao = material.ao * (material.hasAoTexture > 0 ? texture(aoTexture, texCoords).r : 1.0);
    emissive *= material.emissiveMultiplier;

    if (material.hasNormalTexture > 0) {
        vec3 sampledNormal = (texture(normalTexture, texCoords).rgb - 0.5) * 2.0;
        mat3 TBN = mat3(inTangentWS, inBitangentWS, inNormalWS);
        normalWS = normalize(TBN * sampledNormal);
    }

    //outNormal = normalWS;

    vec3 F0 = mix(vec3(0.04), albedo, metallic);
    vec3 F = FresnelSchlick(max(dot(normalWS, viewDirWS), 0.0), F0, roughness);

    vec3 Lo = PBR(g_camera, normalWS, viewDirWS, inPositionWS, F0, albedo, metallic, roughness, g_shading.shadowStrength);

    vec3 ks = F;
    vec3 kd = vec3(1.0f) - ks;
    kd *= 1.0f - metallic;

    vec3 diffuse = vec3(0.0);
    vec3 specular = vec3(0.0);

    if (g_shading.hasIbl > 0) {
        // https://learnopengl.com/PBR/IBL/Diffuse-irradiance
        // https://learnopengl.com/PBR/IBL/Specular-IBL
        // https://www.indiedb.com/features/using-image-based-lighting-ibl
        // https://chetanjags.wordpress.com/2015/08/26/image-based-lighting/

        // Prefiltered texture is generated with mips equal to: numMips = floor(log2(dimension)) + 1;
        // This gives us 10 for dimension 512.
        //const float MAX_REFLECTION_LOD = 10.0f;
        const float MAX_REFLECTION_LOD = 8.0f;

        vec3 R = reflect(-viewDirWS, normalWS);
        vec3 prefilteredColor = textureLod(g_prefilteredTexture, R, roughness * MAX_REFLECTION_LOD).rgb;
        vec2 envBRDF = texture(g_brdfLutTexture, vec2(max(dot(normalWS, viewDirWS), 0.0), roughness)).rg;

        specular = prefilteredColor * (F * envBRDF.x + envBRDF.y);
        diffuse = texture(g_irradianceTexture, normalWS).rgb * albedo;
    } else {
        vec3 ambientBrightness = vec3(0.03);

        diffuse = ambientBrightness * albedo;
    }

    vec3 ambient = (kd * diffuse + specular) * ao;
    vec3 color = ambient + Lo + emissive;
    outColor = vec4(gAo * color, material.opacity);

    // TODO:
    //outSpecular = vec4(0.0);

#endif // PBR_DEPTH
}