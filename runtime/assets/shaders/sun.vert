#version 450

#include "common/global.glsl"

#include "common/binding.glsl"

const vec4 position[] = { { -1, -1, 0, 1 }, { 1, -1, 0, 1 }, { -1, 1, 0, 1 }, { 1, -1, 0, 1 }, { 1, 1, 0, 1 }, { -1, 1, 0, 1 } };
const vec2 frag[] = { { 0, 0 }, { 1, 0 }, { 0, 1 }, { 1, 0 }, { 1, 1 }, { 0, 1 } };

layout(push_constant) uniform Params {
    mat4 mvp;
}
params;

layout(location = 0) out vec2 outFragUV;

void main() {
    gl_Position = params.mvp * position[gl_VertexIndex];
    outFragUV = frag[gl_VertexIndex];
}