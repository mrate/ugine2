#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "../common/global.glsl"

#include "../common/binding.glsl"


layout(location = 0) in vec2 inFragUV;

layout(location = 0) out vec4 outColor;

layout(set = MATERIAL_SET, binding = 0) uniform sampler2D image;
layout(set = MATERIAL_SET, binding = 1) uniform sampler2D lensFlareTex;

void main() {
      //vec4 lensMod = texture(lensDirtTex, vTexcoord);
      vec4 lensMod = vec4(1.0);
      vec4 lensFlare = texture(lensFlareTex, inFragUV) * lensMod;
      outColor = texture(image, inFragUV) + lensFlare;
}