#version 450

#include "../common/global.glsl"

#include "../common/binding.glsl"

layout(set = MATERIAL_SET, binding = 0) uniform Params {
    vec2 size;
	float fade;
}
params;

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

layout (location = 0) out vec2 outUv;
layout (location = 1) out float outOpacity;

void main(void)
{	
	vec3 pos = gl_in[0].gl_Position.xyz / gl_in[0].gl_Position.w;

	vec2 tStep = vec2(params.fade);
	vec2 range = 10.5 * tStep;

	vec2 uv = pos.xy * vec2(0.5, 0.5) + 0.5;

	float lightDepth = pos.z;

	float samples = 0;
	float accu = 0;

	for (float y = -range.y; y <= range.y; y += tStep.y) {
		for (float x = -range.x; x <= range.x; x += tStep.x) {
			samples += 1;
			accu += texture(g_depth, uv + vec2(x, y)).r <= lightDepth ? 0 : 1;
		}
	}

	float op = accu / samples;

	gl_Position = vec4(pos.x + params.size.x, pos.y - params.size.y, 0, 1);
	outUv = vec2(1, 0);
	outOpacity = op;
	EmitVertex();

	gl_Position = vec4(pos.x - params.size.x, pos.y - params.size.y, 0, 1);
	outUv = vec2(0, 0);
	outOpacity = op;
	EmitVertex();

	gl_Position = vec4(pos.x + params.size.x, pos.y + params.size.y, 0, 1);
	outUv = vec2(1, 1);
	outOpacity = op;
	EmitVertex();

	gl_Position = vec4(pos.x - params.size.x, pos.y + params.size.y, 0, 1);
	outUv = vec2(0, 1);
	outOpacity = op;
	EmitVertex();

	EndPrimitive();
}