#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "../common/global.glsl"

#include "../common/binding.glsl"

layout(location = 0) in vec2 inFragUV;

layout(location = 0) out vec4 outColor;

layout(set = MATERIAL_SET, binding = 0) uniform sampler2D image;
layout(set = MATERIAL_SET, binding = 1) uniform Params {
    int horizontal;
}
params;

const float weight[5] = float[](0.227027, 0.1945946, 0.1216216, 0.054054, 0.016216);

void main() {
    vec2 size = textureSize(image, 0);

    vec2 tex_offset = 1.0 / textureSize(image, 0); // gets size of single texel
    vec3 result = texture(image, inFragUV).rgb * weight[0]; // current fragment's contribution
    if (params.horizontal > 0) {
        for (int i = 1; i < 5; ++i) {
            float xOff = tex_offset.x * i;
            result += texture(image, inFragUV + vec2(xOff, 0.0)).rgb * weight[i];
            result += texture(image, inFragUV - vec2(xOff, 0.0)).rgb * weight[i];
        }
    } else {
        for (int i = 1; i < 5; ++i) {
            float yOff = tex_offset.y * i;
            result += texture(image, inFragUV + vec2(0.0, yOff)).rgb * weight[i];
            result += texture(image, inFragUV - vec2(0.0, yOff)).rgb * weight[i];
        }
    }

    outColor = vec4(result, 1.0);
}
