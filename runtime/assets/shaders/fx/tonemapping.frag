#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "../common/global.glsl"

#include "../common/binding.glsl"

layout(location = 0) in vec2 inFragUV;

layout(location = 0) out vec4 outColor;

layout(set = MATERIAL_SET, binding = 0) uniform sampler2D image;
layout(set = MATERIAL_SET, binding = 1) uniform Params {
    float gamma;
    float exposure;
    int algorithm;
}
params;

vec3 Reinhard2(vec3 x) {
    const float L_white = 4.0;

    return (x * (1.0 + x / (L_white * L_white))) / (1.0 + x);
}

float Reinhard2(float x) {
    const float L_white = 4.0;

    return (x * (1.0 + x / (L_white * L_white))) / (1.0 + x);
}

vec3 Uncharted2Tonemap(vec3 x) {
    float A = 0.15;
    float B = 0.50;
    float C = 0.10;
    float D = 0.20;
    float E = 0.02;
    float F = 0.30;
    float W = 11.2;
    return ((x * (A * x + C * B) + D * E) / (x * (A * x + B) + D * F)) - E / F;
}

float uncharted2Tonemap(float x) {
    float A = 0.15;
    float B = 0.50;
    float C = 0.10;
    float D = 0.20;
    float E = 0.02;
    float F = 0.30;
    float W = 11.2;
    return ((x * (A * x + C * B) + D * E) / (x * (A * x + B) + D * F)) - E / F;
}

float uncharted2(float color) {
    const float W = 11.2;
    //const float exposureBias = 2.0;
    float exposureBias = params.exposure;
    float curr = uncharted2Tonemap(exposureBias * color);
    float whiteScale = 1.0 / uncharted2Tonemap(W);
    return curr * whiteScale;
}

void main() {
    vec3 hdrColor = texture(image, inFragUV).rgb;

    // exposure tone mapping
    vec3 mapped = vec3(0.0);
    switch (params.algorithm) {
    case 0:
        mapped = vec3(1.0) - exp(-hdrColor * params.exposure);
        break;
    case 1:
        mapped = Uncharted2Tonemap(hdrColor);
        mapped = mapped * (1.0f / Uncharted2Tonemap(vec3(11.2f)));
        break;
    }

    // Gamma correction
    mapped = pow(mapped, vec3(1.0 / params.gamma));

    outColor = vec4(mapped, 1.0);
}