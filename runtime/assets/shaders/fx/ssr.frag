#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 inFragUV;

layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 0) uniform sampler2D position;
layout(set = 0, binding = 1) uniform sampler2D normal;
layout(set = 0, binding = 2) uniform sampler2D specular;

layout(set = 1, binding = 0) uniform sampler2D image;

layout(set = 1, binding = 1) uniform Params {
    mat4 invView;
    mat4 projection;
    mat4 invprojection;
    mat4 view;
    float step;
    float minRayStep;
    float maxSteps;
    int numBinarySearchSteps;
    float reflectionSpecularFalloffExponent;
}
params;

//http://imanolfotia.com/blog/update/2017/03/11/ScreenSpaceReflections.html
float Metallic;

#define Scale vec3(.8, .8, .8)
#define K 19.19

vec3 BinarySearch(inout vec3 dir, inout vec3 hitCoord, inout float dDepth) {
    float depth;

    vec4 projectedCoord;

    for (int i = 0; i < params.numBinarySearchSteps; i++) {

        projectedCoord = params.projection * vec4(hitCoord, 1.0);
        projectedCoord.xy /= projectedCoord.w;
        projectedCoord.xy = projectedCoord.xy * 0.5 + 0.5;

        depth = textureLod(position, projectedCoord.xy, 2).z;

        dDepth = hitCoord.z - depth;

        dir *= 0.5;
        if (dDepth > 0.0)
            hitCoord += dir;
        else
            hitCoord -= dir;
    }

    projectedCoord = params.projection * vec4(hitCoord, 1.0);
    projectedCoord.xy /= projectedCoord.w;
    projectedCoord.xy = projectedCoord.xy * 0.5 + 0.5;

    return vec3(projectedCoord.xy, depth);
}

vec3 fresnelSchlick(float cosTheta, vec3 F0) {
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 hash(vec3 a) {
    a = fract(a * Scale);
    a += dot(a, a.yxz + K);
    return fract((a.xxy + a.yxx) * a.zyx);
}

vec4 RayMarch(in vec3 dir, inout vec3 hitCoord, out float dDepth) {
    dir *= params.step;

    float depth;
    int steps;
    vec4 projectedCoord;

    for (int i = 0; i < params.maxSteps; i++) {
        hitCoord += dir;

        projectedCoord = params.projection * vec4(hitCoord, 1.0);
        projectedCoord.xy /= projectedCoord.w;
        projectedCoord.xy = projectedCoord.xy * 0.5 + 0.5;

        depth = textureLod(position, projectedCoord.xy, 2).z;
        if (depth > 1000.0)
            continue;

        dDepth = hitCoord.z - depth;

        if ((dir.z - dDepth) < 1.2) {
            if (dDepth <= 0.0) {
                vec4 Result;
                Result = vec4(BinarySearch(dir, hitCoord, dDepth), 1.0);

                return Result;
            }
        }

        steps++;
    }

    return vec4(projectedCoord.xy, depth, 0.0);
}

void main() {
    float Metallic = 0.5;
    if (Metallic < 0.01) {
        outColor = texture(image, inFragUV);
        return;
    }

    vec3 viewNormal = vec3(texture(normal, inFragUV) * params.invView);
    vec3 viewPos = textureLod(position, inFragUV, 2).xyz;
    vec3 albedo = texture(image, inFragUV).rgb;

    float spec = texture(specular, inFragUV).w;

    vec3 F0 = vec3(0.04);
    F0 = mix(F0, albedo, Metallic);
    vec3 Fresnel = fresnelSchlick(max(dot(normalize(viewNormal), normalize(viewPos)), 0.0), F0);

    // Reflection vector
    vec3 reflected = normalize(reflect(normalize(viewPos), normalize(viewNormal)));

    vec3 hitPos = viewPos;
    float dDepth;

    vec3 wp = vec3(vec4(viewPos, 1.0) * params.invView);
    vec3 jitt = mix(vec3(0.0), vec3(hash(wp)), spec);
    vec3 dir = vec3(jitt) + reflected * max(params.minRayStep, -viewPos.z);

    vec4 coords = RayMarch(dir, hitPos, dDepth);

    vec2 dCoords = smoothstep(0.2, 0.6, abs(vec2(0.5, 0.5) - coords.xy));

    float screenEdgefactor = clamp(1.0 - (dCoords.x + dCoords.y), 0.0, 1.0);

    float ReflectionMultiplier = pow(Metallic, params.reflectionSpecularFalloffExponent) * screenEdgefactor * -reflected.z;

    // Get color
    vec3 SSR = textureLod(image, coords.xy, 0).rgb * clamp(ReflectionMultiplier, 0.0, 0.9) * Fresnel;

    outColor = vec4(mix(albedo.rgb, SSR.rgb, clamp(Metallic, 0.0, 1.0)), 1.0);
}
