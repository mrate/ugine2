#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "../common/global.glsl"

#include "../common/binding.glsl"


layout(location = 0) in vec2 inFragUV;

layout(location = 0) out vec4 outColor;

layout(set = MATERIAL_SET, binding = 0) uniform sampler2D inputTexture;

layout(set = MATERIAL_SET, binding = 1) uniform Params {
    vec2 center;
    float radius;
    float amount;
}
params;

#define NUM_SAMPLES 32

void main() {
    vec4 col = vec4(0);
    vec2 dist = inFragUV - params.center;
    float val = clamp(length(dist) / params.radius, 0, 1);
    for(int j = 0; j < NUM_SAMPLES; ++j) {
        float scale = 1.0 - params.amount * (float(j) / float(NUM_SAMPLES - 1)) * val;
        col += texture(inputTexture, dist * scale + params.center);
    }
    col /= float(NUM_SAMPLES);
    outColor = col;
}
