#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "../common/global.glsl"

#include "../common/binding.glsl"

layout(location = 0) in vec2 inFragUV;

layout(location = 0) out vec4 outColor;

layout(set = MATERIAL_SET, binding = 0) uniform sampler2D image;
layout(set = MATERIAL_SET, binding = 1) uniform Params {
    vec4 color;
    vec4 sunColor;
    vec2 nearFar;
    vec2 minMax;
    int enabled;
}
params;

void main() {
    outColor = texture(image, inFragUV);

    if (params.enabled < 1) {
        return;
    }

    float near = params.nearFar.x;
    float far = params.nearFar.y;

    vec3 positionWS = WorldSpaceFromDepth(inFragUV, g_camera, g_depth);

    //outColor = vec4(vec3(depth), 1.0);
    //return ;

    //float distance = 2.0 * zNear * zFar / (zFar + zNear - depth * (zFar - zNear));
    float distance = length(g_camera.position - positionWS);
    float near2 = near * near;
    float far2 = far * far;

    vec4 fogColor = vec4(0.0);
    float intensity = clamp((distance - near) / (far - near), params.minMax.x, params.minMax.y);

    if (params.sunColor.a > 0.0) {
        float sunAmount = max(dot(normalize(positionWS.xyz), g_frame.sunPosition), 0.0);
        fogColor = mix(params.color, params.sunColor, pow(sunAmount, 8.0));
    } else {
        fogColor = params.color;
    }

    outColor = vec4(mix(outColor, fogColor, intensity).rgb, 1.0);
}