#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "../common/global.glsl"

#include "../common/binding.glsl"

layout(location = 0) in vec2 inFragUV;

layout(location = 0) out vec4 outColor;

layout(set = MATERIAL_SET, binding = 0) uniform sampler2D image;

layout(set = MATERIAL_SET, binding = 1) uniform sampler2D outOfFocus;

layout(set = MATERIAL_SET, binding = 2) uniform Params {
    int enabled;
    float distance;
    float range;
}
params;

void main() {
    vec4 focusColor = texture(image, inFragUV);
    outColor = focusColor;

    if (params.enabled < 1) {
        return;
    }

    vec3 fragPosition = ViewSpaceFromDepth(inFragUV, g_camera, g_depth);
    float cameraDistance = -fragPosition.z;

    vec4 outOfFocusColor = texture(outOfFocus, inFragUV);
    float blur = smoothstep(params.range * 0.5, params.range, abs(params.distance - cameraDistance));

    outColor = mix(focusColor, outOfFocusColor, blur);
}