#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "../common/global.glsl"

#include "../common/binding.glsl"


layout(location = 0) in vec2 inFragUV;

layout(location = 0) out vec4 outColor;

layout(set = MATERIAL_SET, binding = 0) uniform sampler2D image;

layout(set = MATERIAL_SET, binding = 1) uniform Params {
    int ghost;
    float ghostDispersal;
    float distortion;
    float haloWidth;
}
params;

vec3 textureDistorted(
      in sampler2D tex,
      in vec2 texcoord,
      in vec2 direction, // direction of distortion
      in vec3 distortion // per-channel distortion factor
   ) {
    return vec3(
        texture(tex, texcoord + direction * distortion.r).r,
        texture(tex, texcoord + direction * distortion.g).g,
        texture(tex, texcoord + direction * distortion.b).b
    );
}

void main() {
    vec2 texcoord = vec2(1.0) - inFragUV;
    vec2 texelSize = 1.0 / vec2(textureSize(image, 0));

    // Ghost vector to image centre:
    vec2 ghostVec = (vec2(0.5) - texcoord) * params.ghostDispersal;

    vec2 direction = normalize(ghostVec);
    vec3 distortion = vec3(-texelSize.x * params.distortion, 0.0, texelSize.x * params.distortion);

    // Sample ghosts:  
    vec3 result = vec3(0.0);
    for (int i = 0; i < params.ghost; ++i) { 
        vec2 offset = fract(texcoord + ghostVec * float(i));

        float weight = length(vec2(0.5) - offset) / length(vec2(0.5));
        weight = pow(1.0 - weight, 10.0);
        result += textureDistorted(image, offset, direction, distortion) * weight;
    }

    // sample halo:
    vec2 haloVec = normalize(ghostVec) * params.haloWidth;
    float weight = length(vec2(0.5) - fract(texcoord + haloVec)) / length(vec2(0.5));
    weight = pow(1.0 - weight, 5.0);
    result += textureDistorted(image, texcoord + haloVec, direction, distortion) * weight;

    //result *= texture(lensColorTexture, length(vec2(0.5) - texcoord) / length(vec2(0.5)));

    outColor = vec4(result, 1.0f);
}