#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "../common/global.glsl"

#include "../common/binding.glsl"

#include "../common/vertex_in.glsl"

void main() {
    vec4 position = g_camera.viewProj * Model() * vec4(0, 0, 0, 1);

    gl_Position = position;
}
