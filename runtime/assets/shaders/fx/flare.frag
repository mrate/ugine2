#version 450

#include "../common/global.glsl"

#include "../common/binding.glsl"

layout(set = MATERIAL_SET, binding = 1) uniform sampler2D flareTex;

layout(location = 0) in vec2 inUv;
layout(location = 1) in float inOpacity;

layout(location = 0) out vec4 outColor;

void main() {
    vec3 color = texture(flareTex, inUv).rgb;

    outColor = vec4(inOpacity * color, 1);
}