#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "../common/global.glsl"

#include "../common/binding.glsl"

layout(location = 0) in vec2 inFragUV;

layout(location = 0) out vec4 outColor;

#define KERNEL_SIZE 64
#define BIAS 0.000025

layout(set = MATERIAL_SET, binding = 0) uniform sampler2D image;
layout(set = MATERIAL_SET, binding = 1) uniform Params {
    int debug;
    float strength;
    float radius;
    vec3 samples[KERNEL_SIZE];
}
params;

layout(set = MATERIAL_SET, binding = 2) uniform sampler2D randomVectors;

void main() {
    vec3 inColor = texture(image, inFragUV).rgb;

    vec3 positionVS = ViewSpaceFromDepth(inFragUV, g_camera, g_depth);
    vec3 normalVS = NormalVSFromDepth(inFragUV, g_camera, g_depth);

    // TODO: Random vector rotation.
    //vec3 randomVec = vec3(1, 1, 0);
    vec3 randomVec = vec3(texture(randomVectors, inFragUV * vec2(g_camera.resolution / 4)).rg, 0);

    vec3 tangent   = normalize(randomVec - normalVS * dot(randomVec, normalVS));
    vec3 bitangent = cross(normalVS, tangent);
    mat3 TBN       = mat3(tangent, bitangent, normalVS);

    float ao = 0.0;
    for (int i = 0; i < KERNEL_SIZE; ++i) {
        // Sample position - fragment position offset by radnom hemisphere vector * sample radius.
        vec3 samplePosVS = TBN * params.samples[i]; // Tangent to VS.
        samplePosVS = positionVS + samplePosVS * params.radius;

        // VS to clip space.
        vec4 offset = vec4(samplePosVS, 1.0);
        offset      = g_camera.proj * offset;    // from view to clip-space
        offset.xyz /= offset.w;               // perspective divide
        offset.xyz  = offset.xyz * 0.5 + 0.5; // transform to range 0.0 - 1.0  

        // TODO: Sample linear depth texture.
        float sampleDepth = -ViewSpaceFromDepth(offset.xy, g_camera, g_depth).z;
        float rangeCheck = smoothstep(0.0, 1.0, params.radius / abs(-positionVS.z - sampleDepth));

        ao += (sampleDepth <= -samplePosVS.z + BIAS ? 1 : 0) * rangeCheck;
    }
    ao = 1 - (ao / float(KERNEL_SIZE));

    outColor = params.debug > 0 ? vec4(vec3(ao), 1) : vec4(inColor * mix(1, ao, params.strength), 1);
}