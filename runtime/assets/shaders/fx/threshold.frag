#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "../common/global.glsl"

#include "../common/binding.glsl"


layout(location = 0) in vec2 inFragUV;

layout(location = 0) out vec4 outColor;

layout(set = MATERIAL_SET, binding = 0) uniform sampler2D image;

layout(set = MATERIAL_SET, binding = 1) uniform Params {
    float threshold;
    float scale;
}
params;

void main() {
    vec3 color = texture(image, inFragUV).rgb;
    //color = color / (color + 1);

    float brightness = dot(color.rgb, vec3(0.2126, 0.7152, 0.0722));
    outColor = brightness > params.threshold ? vec4(params.scale * color.rgb, 1.0) : vec4(0.0);
}