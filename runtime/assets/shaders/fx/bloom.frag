#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "../common/global.glsl"

#include "../common/binding.glsl"


layout(location = 0) in vec2 inFragUV;

layout(location = 0) out vec4 outColor;

layout(set = MATERIAL_SET, binding = 0) uniform sampler2D image;
layout(set = MATERIAL_SET, binding = 1) uniform sampler2D bloom;

layout(set = MATERIAL_SET, binding = 2) uniform Params {
    float amount;
    float debug;
}
params;

void main() {
    vec4 bloomColor = texture(bloom, inFragUV) * params.amount;
    if (params.debug > 0.0) {
        outColor = bloomColor;
    } else {
        outColor = texture(image, inFragUV) + bloomColor;
    }
}