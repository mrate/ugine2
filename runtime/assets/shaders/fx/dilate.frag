#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "../common/global.glsl"

#include "../common/binding.glsl"

layout(location = 0) in vec2 inFragUV;

layout(location = 0) out vec4 outColor;

layout(set = MATERIAL_SET, binding = 0) uniform sampler2D image;

layout(set = MATERIAL_SET, binding = 1) uniform Params {
    int enabled;
    int size;
    float separation;
    float minThreshold;
    float maxThreshold;
}
params;

void main() {
    vec2 texSize = textureSize(image, 0).xy;
    vec2 fragCoord = gl_FragCoord.xy;

    outColor = texture(image, fragCoord / texSize);

    if (params.enabled < 1 || params.size <= 0) {
        return;
    }

    float mx = 0.0;
    vec4 cmx = outColor;

    for (int i = -params.size; i <= params.size; ++i) {
        for (int j = -params.size; j <= params.size; ++j) {
            if (!(distance(vec2(i, j), vec2(0, 0)) <= params.size)) {
                continue;
            }

            vec4 c = texture(image, (gl_FragCoord.xy + (vec2(i, j) * params.separation)) / texSize);

            float mxt = dot(c.rgb, vec3(0.3, 0.59, 0.11));
            if (mxt > mx) {
                mx = mxt;
                cmx = c;
            }
        }
    }

    outColor = vec4(mix(outColor.rgb, cmx.rgb, smoothstep(params.minThreshold, params.maxThreshold, mx)), 1.0);
}