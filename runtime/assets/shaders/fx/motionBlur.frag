#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "../common/global.glsl"

#include "../common/binding.glsl"


layout(location = 0) in vec2 inFragUV;

layout(location = 0) out vec4 outColor;

layout(set = MATERIAL_SET, binding = 0) uniform sampler2D image;

layout(set = MATERIAL_SET, binding = 1) uniform Params {
    float strength;
}
params;

#define SAMPLES     16

void main() {
    vec3 pos = WorldSpaceFromDepth(inFragUV, g_camera, g_depth);
    
    // TODO: Current to prev matrix.
    // Prev position.
    vec4 prevPos = g_camera.viewProjPrev * vec4(pos, 1.0);
    prevPos.xyz /= prevPos.w;
    prevPos.xy = prevPos.xy * 0.5 + 0.5;

    vec2 blurVec = prevPos.xy - inFragUV;

    vec3 color = texture(image, inFragUV).rgb;
    vec3 blur = color;
    for (int i = 1; i < SAMPLES; ++i) {
        vec2 offset = blurVec * (float(i) / float(SAMPLES - 1) - 0.5);
        blur += texture(image, inFragUV + offset).rgb;
    }
    blur /= float(SAMPLES);
    color = mix(color, blur, params.strength);

    outColor = vec4(color.rgb, 1.0);
}