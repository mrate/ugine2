#version 450

#include "common/global.glsl"

#include "common/binding.glsl"

layout(location = 0) in vec2 inFragUV;

layout(location = 0) out vec4 outColor;

void main() {
    float dist = smoothstep(0.5, 0, length(vec2(0.5) - inFragUV));
    outColor = vec4(vec3(dist), 1);
}