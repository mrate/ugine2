#version 450

#include "../common/global.glsl"

#include "../common/binding.glsl"

layout(set = MATERIAL_SET, binding = 0) uniform sampler2D inputTexture;

layout(location = 0) in vec2 fragUV;

layout(location = 0) out vec4 outFragColor;

void main() {
    outFragColor = g_frame.volumetricIntensity * texture(inputTexture, fragUV);
}