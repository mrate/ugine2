#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "../common/global.glsl"

#include "../common/binding.glsl"

#include "../common/shadows.glsl"

layout(push_constant) uniform Params {
    uint lightIndex;
}
params;

// Input.
layout(location = 0) in vec2 inFragUV;

layout(location = 0) out vec4 outColor;

void main() {
    Light light = g_lights[params.lightIndex];
    if (light.shadowIndex < 0) {
        outColor = vec4(0);
        return;
    }

    float depth = texture(g_depth, inFragUV).r;
    vec2 screenSpace = inFragUV * 2.0 - 1.0;
    vec4 positionVS = g_camera.invProj * vec4(screenSpace, depth, 1.0);
    positionVS = vec4(positionVS.xyz / positionVS.w, 1);
    vec4 positionWS = g_camera.invView * positionVS;

    vec3 rayStart = positionWS.xyz;
    vec3 rayDir = g_camera.position - rayStart;
    
    float cameraDistance = length(rayDir);
    rayDir /= cameraDistance;

    float sampleCount = 32;
    float stepSize = cameraDistance / sampleCount;
    vec3 step = rayDir * stepSize;

    vec3 accumulation = vec3(0);
    //float marchedDistance = 0;

    rayStart += step * Dither(ivec2(inFragUV * 2048));

    for (int i = 0; i < sampleCount; ++i) {
        int csm = min(CsmIndex(g_camera.position, rayStart), light.csmMaxLevel);
        vec3 shPos = (g_shading.shadows[light.shadowIndex].lightViewProj[csm] * vec4(rayStart, 1.0)).xyz;
        vec3 shTex = shPos * vec3(0.5) + 0.5;

        if (IsSaturated(shTex)) {
            float att = 1 - CalcShadowFactorPCFDir(light.shadowMapIndex, vec4(shPos, 1.0), csm);

            accumulation += vec3(att);
            //marchedDistance += stepSize;
            rayStart += step;
        } else {
            // TODO: ?
            // accumulation += sampleCount - i;
            break;
        }
    }

    accumulation /= sampleCount;
    outColor = max(vec4(0), vec4(vec3(accumulation * light.color.rgb * light.intensity), 1.0));
}