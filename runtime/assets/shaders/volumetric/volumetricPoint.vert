#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "../common/global.glsl"

#include "../common/binding.glsl"

#include "../shapes/icosphere.glsl"

layout(push_constant) uniform Params {
    mat4 mvp;
}
params;

layout(location = 0) out vec4 outPosition;

void main() {
    gl_Position = outPosition = params.mvp * ICOSPHERE[gl_VertexIndex];
}