#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "../common/global.glsl"

#include "../common/binding.glsl"

#include "../common/shadows.glsl"

layout(push_constant) uniform Params {
    layout(offset = 64) uint lightIndex;
}
params;

// Input.
layout(location = 0) in vec4 inPosition;

layout(location = 0) out vec4 outColor;

void main() {
    Light light = g_lights[params.lightIndex];
    if (light.shadowIndex < 0) {
        outColor = vec4(0);
        return;
    }

    vec2 uv = (inPosition.xy / inPosition.w) * 0.5f + 0.5f;

    float depth = texture(g_depth, uv).r;
    vec2 screenSpace = uv * 2.0 - 1.0;
    vec4 positionVS = g_camera.invProj * vec4(screenSpace, depth, 1.0);
    positionVS = vec4(positionVS.xyz / positionVS.w, 1);
    vec4 positionWS = g_camera.invView * positionVS;

    vec3 rayStart = positionWS.xyz;
    vec3 rayEnd = g_camera.position;
    vec3 rayDir = rayEnd - rayStart;

    float cameraDistance = length(rayDir);
    rayDir /= cameraDistance;

    // Start tracing ray at the edge of the sphere
    if (length(rayEnd - light.position) > light.range) {
        float t = RaySphereIntersection(rayStart, normalize(rayDir), light.position, light.range);
        if (t < 0) {
            outColor = vec4(0);
            return;
        }
        rayEnd = rayEnd - t * rayDir;
    }

    float sampleCount = 64;
    float stepSize = cameraDistance / sampleCount;
    vec3 step = rayDir * stepSize;

    float accumulation = 0;
    //float marchedDistance = 0;

    rayStart += step * Dither(ivec2(uv * 2048));

    for (int i = 0; i < sampleCount; ++i) {
        vec3 lightDir = rayStart - light.position;
        float att = DoAttenuation(light, length(lightDir));
        att *= 1 - CalcShadowFactorPoint(light.shadowMapIndex, lightDir, light.range);

        //att *= FogAmount(cameraDistance - marchedDistance);

        accumulation += att;
        //marchedDistance += stepSize;
        rayStart += step;
    }    

    accumulation /= sampleCount;
    outColor = max(vec4(0), vec4(vec3(accumulation * light.color.rgb * light.intensity), 1.0));
}