#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "common/global.glsl"

#include "common/binding.glsl"

#include "common/vertex_in.glsl"

void main() {
    gl_Position = g_camera.viewProj * Model() * vec4(inPosition, 1.0);
}
