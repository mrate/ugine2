#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(push_constant) uniform ViewProj {
    mat4 modelViewProj;
}
viewProj;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec2 inUV;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec3 inTangent;
layout(location = 4) in vec3 inBitangent;

layout(location = 0) out vec3 fragUVW;

void main() {
    fragUVW = inPosition;

    vec4 pos = viewProj.modelViewProj * vec4(inPosition, 1.0);
    gl_Position = pos;
}
