// Generates an irradiance cube from an environment map using convolution

#version 450

layout(location = 0) in vec3 inPos;
layout(location = 0) out vec4 outColor;
layout(binding = 0) uniform samplerCube samplerEnv;

layout(push_constant) uniform PushConsts {
    layout(offset = 64) float deltaPhi;
    layout(offset = 68) float deltaTheta;
}
consts;

#define PI 3.1415926535897932384626433832795
#define HALF_PI 1.57079632679489661923132169163975
#define TWO_PI 6.283185307179586476925286766559

vec3 Uncharted2Tonemap(vec3 color) {
    float A = 0.15;
    float B = 0.50;
    float C = 0.10;
    float D = 0.20;
    float E = 0.02;
    float F = 0.30;
    float W = 11.2;
    return ((color * (A * color + C * B) + D * E) / (color * (A * color + B) + D * F)) - E / F;
}

vec3 srcColor(vec3 sampleVector) {
    vec3 color = texture(samplerEnv, sampleVector).rgb;

    // Tone mapping
    color = Uncharted2Tonemap(color);
    color = color * (1.0f / Uncharted2Tonemap(vec3(11.2f)));
    // Gamma correction
    //color = pow(color, vec3(1.0f / 2.2));
    return color;
}

void main() {
    vec3 N = normalize(inPos);
    vec3 up = vec3(0.0, 1.0, 0.0);
    vec3 right = normalize(cross(up, N));
    up = cross(N, right);

    vec3 color = vec3(0.0);
    uint sampleCount = 0u;
    for (float phi = 0.0; phi < TWO_PI; phi += consts.deltaPhi) {
        for (float theta = 0.0; theta < HALF_PI; theta += consts.deltaTheta) {
            vec3 tempVec = cos(phi) * right + sin(phi) * up;
            vec3 sampleVector = cos(theta) * N + sin(theta) * tempVec;
            color += srcColor(sampleVector).rgb * cos(theta) * sin(theta);
            sampleCount++;
        }
    }
    outColor = vec4(PI * color / float(sampleCount), 1.0);
}
