#extension GL_EXT_shader_16bit_storage : require

#include "../common/global.glsl"

#define PARTICLES_WITH_EMITTER
#include "particle.glsl"

layout(local_size_x = PARTICLE_THREAD_COUNT) in;

void main() {
    uint emitIndex = gl_GlobalInvocationID.x;

    if (emitIndex < counter.emitCount) {
        uint deadIndex = atomicAdd(counter.deadCount, -1);
        uint particleIndex = dead.index[deadIndex - 1];

        uint liveIndex = atomicAdd(counter.liveCurCount, 1);
        liveCur.index[liveIndex] = particleIndex;

        vec2 uv = vec2(emitParams.randomSeed, float(emitIndex) / float(PARTICLE_THREAD_COUNT));
        float seed = 1.3456;
        
        float life = emitParams.life.x + emitParams.life.y * Random(seed, uv);

        Particle newParticle;
        newParticle.life = vec3(life, life, 0);
        newParticle.size = emitParams.size;
        newParticle.color = emitParams.color;

#ifdef PARTICLES_EMIT_MESH
        uint vIndex = emitParams.meshIndexStart + uint(Random(seed, uv) * (emitParams.meshIndexCount / 3));
        
        uint vIndex0 = emitParams.meshVertexStart + uint(meshI.index[vIndex * 3]);
        uint vIndex1 = emitParams.meshVertexStart + uint(meshI.index[vIndex * 3 + 1]);
        uint vIndex2 = emitParams.meshVertexStart + uint(meshI.index[vIndex * 3 + 2]);

        vec3 p0 = meshV.vertex[vIndex0].pos;
        vec3 n0 = meshV.vertex[vIndex0].nor;
        vec3 p1 = meshV.vertex[vIndex1].pos;
        vec3 n1 = meshV.vertex[vIndex1].nor;
        vec3 p2 = meshV.vertex[vIndex2].pos;
        vec3 n2 = meshV.vertex[vIndex2].nor;

        float f = Random(seed, uv);
        float g = Random(seed, uv);
        if (f + g > 1) {
            f = 1 - f;
            g = 1 - g;
        }

        vec3 position = p0 + f * (p1 - p0) + g * (p2 - p0);
        vec3 normal = n0 + f * (n1 - n0) + g * (n2 - n0);
        
        newParticle.speed = normal;
        newParticle.position = (emitParams.modelMatrix * vec4(position, 1)).xyz;
#else
        newParticle.speed = emitParams.startSpeed - 0.5 * emitParams.speedOffset + vec3(Random(seed, uv), Random(seed, uv), Random(seed, uv)) * emitParams.speedOffset;
        newParticle.position = (emitParams.modelMatrix * vec4(Random(seed, uv), Random(seed, uv), Random(seed, uv), 1.0)).xyz;
#endif

        particle[particleIndex] = newParticle;
    }
}
