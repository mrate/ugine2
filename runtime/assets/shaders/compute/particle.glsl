#include "../common/indirect.glsl"

#define PARTICLE_DATASET 0

#define PARTICLE_THREAD_COUNT 256

struct Particle {
    vec4 color;
    vec3 position;
    vec3 speed;
    vec3 life; // actual, full, fade
    vec2 size;
};

layout(std140, set = PARTICLE_DATASET, binding = 0) buffer Particles {
    Particle particle[];
};

layout(set = PARTICLE_DATASET, binding = 1) buffer Counter {
    uint deadCount;
    uint liveCurCount;
    uint liveNewCount;
    uint emitCount;
}
counter;

layout(std430, set = PARTICLE_DATASET, binding = 2) buffer DeadIndex {
    uint index[];
}
dead;

layout(std430, set = PARTICLE_DATASET, binding = 3) buffer LiveIndex1 {
    uint index[];
}
liveCur;

layout(std430, set = PARTICLE_DATASET, binding = 4) buffer LiveIndex2 {
    uint index[];
}
liveNew;

#ifdef PARTICLES_WITH_EMITTER
layout(set = PARTICLE_DATASET, binding = 5) uniform EmitterCB {
    mat4 modelMatrix;
    vec3 gravity;
    float timeDelta;
    vec3 startSpeed;
    float randomSeed;
    vec3 positionOffset;
    uint particleCount;
    vec3 speedOffset;
    uint emitCount;
    vec4 color;
    vec2 size;
    vec2 life;
    uint meshIndexCount;
    uint meshIndexStart;
    uint meshVertexStart;
}
emitParams;
#endif

#ifdef PARTICLES_INIT
layout(std430, set = PARTICLE_DATASET, binding = 6) buffer ParticleIndirectEmit {
    uvec3 params;
}
indirectEmit;

layout(std430, set = PARTICLE_DATASET, binding = 7) buffer ParticleIndirectSimulation {
    uvec3 params;
}
indirectSimulation;
#endif

#ifdef PARTICLES_EMIT_MESH
struct MeshVertex {
    vec3 pos;
    vec3 nor;
    vec3 tan;
    vec3 bitan;
    vec2 uv;
};

layout(std430, set = PARTICLE_DATASET, binding = 6) readonly buffer MeshVertices {
    MeshVertex vertex[];
} meshV;

layout(std430, set = PARTICLE_DATASET, binding = 7) readonly buffer MeshIndices {
    uint16_t index[];
} meshI;
#endif

#ifdef PARTICLES_WITH_INDIRECT_DRAW
layout(set = PARTICLE_DATASET, binding = 5) buffer ParticleIndirectDraw {
    IndirectIndexedDraw indirectDraw[];
};
#endif

#ifdef PARTICLES_FINISH
layout(set = PARTICLE_DATASET, binding = 6) uniform ParticleSubmeshes {
    uint count;
}
finish;
#endif