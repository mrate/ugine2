// Copyright (c) 2021 Vrgineers Inc. All rights reserved.

#pragma once

#define VRG_API extern "C" __declspec(dllimport)

/// <summary>Initialization structure to use as a graphics device for vrgAttachGraphicsDevice() call when using Vulkan as a graphics API.</summary>
/// <seealso cref="vrgAttachGraphicsDevice" />
typedef struct {
    /// <summary>Pointer to VkInstance.</summary>
    VkInstance* instance;
    /// <summary>Pointer to VkPhysicalDevice.</summary>
    VkPhysicalDevice* physicalDevice;
    /// <summary>Pointer to VkDevice.</summary>
    VkDevice* device;
} VrgVulkanDeviceDescriptor;

/// <summary>Wrapper structure used as a texture for vrgSubmitFrameLayer() call when using Vulkan as a graphics API.</summary>
/// <seealso cref="vrgSubmitFrameLayer" />
typedef struct {
    /// <summary>Pointer to VkImage.</summary>
    VkImage* texture;
    /// <summary>Texture width.</summary>
    uint16_t width;
    /// <summary>Texture height.</summary>
    uint16_t height;
    /// <summary>Texture format, integer representation of VkFormat.</summary>
    uint32_t format;
} VrgVulkanTextureDescriptor;

/// <summary>Synchronization object to use when using swap chain and Vulkan together.</summary>
/// <seealso cref="vrgSwapChainCurrentIndexEx" />
typedef struct {
    VkDeviceMemory acquireSync;
    uint64_t acquireKey;
    uint32_t acquireTimeout;
    VkDeviceMemory releaseSync;
    uint64_t releaseKey;
} VrgVulkanSwapchainSync;

