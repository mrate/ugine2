﻿#pragma once

#include <ugine/gfx/Color.h>
#include <ugine/gfx/Storages.h>
#include <ugine/system/Platform.h>
#include <ugine/utils/FpsCounter.h>

namespace ugine::common {

class EngineBase : public ugine::system::EngineImpl {
public:
    EngineBase(const std::wstring& title, int windowWidth, int windowHeight, bool srgb, bool useGui);

protected:
    void OnResize(unsigned int width, unsigned int height) override;

    void InitGui();

    vk::Viewport Viewport(bool flip = true) const {
        return flip ? vk::Viewport{ 0, static_cast<float>(Height()), static_cast<float>(Width()), -static_cast<float>(Height()), 0, 1 }
                    : vk::Viewport{ 0, 0, static_cast<float>(Width()), static_cast<float>(Height()), 0, 1 };
    }

    vk::Rect2D Scissor() const {
        return vk::Rect2D{ { 0, 0 }, vk::Extent2D{ Width(), Height() } };
    }

    bool Fps(float& fps) {
        return fpsCounter_.Fps(fps);
    }

    virtual vk::RenderPass GuiRenderPass() const;
    virtual vk::Extent2D GuiExtent() const;

private:
    utils::FpsCounter fpsCounter_;
    std::vector<gfx::TextureViewRef> depthBuffer_;
};

} // namespace ugine::common
