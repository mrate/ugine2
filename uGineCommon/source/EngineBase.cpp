﻿#include "EngineBase.h"

#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/core/Device.h>
#include <ugine/gfx/tools/Initializers.h>
#include <ugine/gui/Gui.h>

namespace ugine::common {

EngineBase::EngineBase(const std::wstring& title, int windowWidth, int windowHeight, bool srgb, bool useGui)
    : ugine::system::EngineImpl{ title, windowWidth, windowHeight, srgb, useGui } {}

void EngineBase::OnResize(unsigned int width, unsigned int height) {
    Graphics().WaitIdle();

    if (HasGui()) {
        InitGui();
    }
}

void EngineBase::InitGui() {
    Gui().Init(GuiRenderPass(), GuiExtent());
}

vk::RenderPass EngineBase::GuiRenderPass() const {
    return gfx::GraphicsService::Device().PresentRenderpass();
}

vk::Extent2D EngineBase::GuiExtent() const {
    return Presenter().SwapChainExtent();
}

} // namespace ugine::common
