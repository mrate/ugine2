﻿#ifdef _WIN32

#include "Win32Window.h"

#include <ugine/input/InputService.h>
#include <ugine/input/Win32/DirectInput.h>
#include <ugine/utils/File.h>

#include <iostream>

#include <windowsx.h>

#include <backends/imgui_impl_win32.h>

extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

namespace ugine::system::win32 {

std::map<HWND, Win32Window*> Win32Window::WIN_MAP;

Win32Window::Win32Window() {}

Win32Window::~Win32Window() {
    Close();
}

bool Win32Window::Init(HINSTANCE hInstance, const std::wstring& title, const std::wstring& name, LPCWSTR iconName, int width, int height, bool resizable) {
    m_width = width;
    m_height = height;

    // Register class
    WNDCLASSEX wcex;
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(hInstance, iconName);
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = nullptr;
    wcex.lpszClassName = title.c_str();
    wcex.hIconSm = LoadIcon(wcex.hInstance, iconName);
    if (!RegisterClassEx(&wcex)) {
        return false;
    }

    // Create window
    m_hInst = hInstance;
    RECT rc = { 0, 0, width, height };
    AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
    m_hWnd = CreateWindow(title.c_str(), name.c_str(), WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | (resizable ? WS_SIZEBOX : 0), CW_USEDEFAULT,
        CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, nullptr, nullptr, hInstance, nullptr);

    if (!m_hWnd) {
        return false;
    }

    WIN_MAP.insert(std::make_pair(m_hWnd, this));

    return true;
}

vk::UniqueSurfaceKHR Win32Window::CreateSurface(vk::Instance instance) const {
    vk::Win32SurfaceCreateInfoKHR createInfo{};
    createInfo.hinstance = Instance();
    createInfo.hwnd = Handle();
    createInfo.flags = {};

    return instance.createWin32SurfaceKHRUnique(createInfo);
}

void Win32Window::Show() {
    Show(SW_SHOW);
}

void Win32Window::Show(int nCmdShow) {
    ShowWindow(m_hWnd, nCmdShow);
}

void Win32Window::Close() {
    if (m_hWnd) {
        WIN_MAP.erase(m_hWnd);
        CloseWindow(m_hWnd);
        m_hWnd = nullptr;
    }
}

void Win32Window::SetTitle(const std::string_view& title) {
    SetWindowTextA(m_hWnd, title.data());
}

void Win32Window::InitInput() {
    auto directInput{ std::make_shared<input::win32::DirectInput>() };
    for (auto i : directInput->AttachedControllers()) {
        input::SharedController controller{ directInput->CreateController(i.id) };
        input::InputService::RegisterController(controller);
    }
}

void Win32Window::InitGui() {
    ImGui_ImplWin32_Init(m_hWnd);

    m_customWndProc = [&](HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) { return CustomWndProcHandler(hWnd, message, wParam, lParam); };
}

void Win32Window::DestroyGui() {
    ImGui_ImplWin32_Shutdown();
}

void Win32Window::NewFrame() {
    ImGui_ImplWin32_NewFrame();
}

bool Win32Window::CustomWndProcHandler(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
    return ImGui_ImplWin32_WndProcHandler(hWnd, message, wParam, lParam);
}

std::pair<int, int> Win32Window::GetSize() const {
    RECT rc;

    GetClientRect(m_hWnd, &rc);
    return { rc.right - rc.left, rc.bottom - rc.top };
}

bool Win32Window::Closed() const {
    return false;
}

void Win32Window::SetMouseCapture(bool capture) {
    if (capture) {
        SetCapture(m_hWnd);
    } else {
        ReleaseCapture();
    }
}

LRESULT CALLBACK Win32Window::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
    PAINTSTRUCT ps;
    HDC hdc;

    Win32Window* win = nullptr;
    auto it = WIN_MAP.find(hWnd);
    if (it != WIN_MAP.end()) {
        win = it->second;
    }

    if (win && win->m_customWndProc) {
        if (win->m_customWndProc(hWnd, message, wParam, lParam)) {
            return true;
        }
    }

    switch (message) {
    case WM_PAINT:
        hdc = BeginPaint(hWnd, &ps);
        EndPaint(hWnd, &ps);
        break;

    case WM_DESTROY:
        PostQuitMessage(0);
        break;

    case WM_MOUSEWHEEL:
        if (win) {
            auto x = GET_X_LPARAM(lParam);
            auto y = GET_Y_LPARAM(lParam);
            auto zDelta = GET_WHEEL_DELTA_WPARAM(wParam);

            win->HandleMouseWheel(x, y, zDelta);
        }
        break;

    case WM_MOUSEMOVE:
        if (win) {
            auto x = GET_X_LPARAM(lParam);
            auto y = GET_Y_LPARAM(lParam);

            win->HandleMouseMove(x, y);
        }
        break;

    case WM_LBUTTONDOWN:
        if (win) {
            auto x = GET_X_LPARAM(lParam);
            auto y = GET_Y_LPARAM(lParam);

            win->HandleMouseDown(0, x, y);
        }
        break;

    case WM_LBUTTONUP:
        if (win) {
            auto x = GET_X_LPARAM(lParam);
            auto y = GET_Y_LPARAM(lParam);

            win->HandleMouseUp(0, x, y);
        }
        break;

    case WM_RBUTTONDOWN:
        if (win) {
            auto x = GET_X_LPARAM(lParam);
            auto y = GET_Y_LPARAM(lParam);

            win->HandleMouseDown(1, x, y);
        }
        break;

    case WM_RBUTTONUP:
        if (win) {
            auto x = GET_X_LPARAM(lParam);
            auto y = GET_Y_LPARAM(lParam);

            win->HandleMouseUp(1, x, y);
        }
        break;

    case WM_KEYUP:
        if (win) {
            win->HandleKeyUp(wParam);
        }
        break;

    case WM_KEYDOWN:
        if (win) {
            win->HandleKeyDown(wParam);
        }

        if (wParam == VK_ESCAPE) {
            PostQuitMessage(0);
        }
        break;

    case WM_SIZE:
        if (win) {
            if (wParam == SIZE_MAXIMIZED || wParam == SIZE_RESTORED || wParam == SIZE_MINIMIZED) {
                if (wParam == SIZE_MINIMIZED) {
                    win->HandleShowWindow(false);
                } else {
                    win->HandleShowWindow(true);
                    UINT width = LOWORD(lParam);
                    UINT height = HIWORD(lParam);
                    win->HandleResize(width, height);
                }
            }
        }
        break;

    case WM_ENTERSIZEMOVE:
        if (win) {
            win->SetResizing(true);
        }
        break;

    case WM_EXITSIZEMOVE:
        if (win) {
            win->SetResizing(false);
        }
        break;

    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }

    return FALSE;
}

void Win32Window::HandleKeyDown(WPARAM param) {
    if (m_keyDown) {
        m_keyDown(static_cast<unsigned long>(param));
    }
}

void Win32Window::HandleKeyUp(WPARAM param) {
    if (m_keyUp) {
        m_keyUp(static_cast<unsigned long>(param));
    }
}

void Win32Window::HandleMouseMove(int x, int y) {
    if (m_mouseMove) {
        m_mouseMove(x, y);
    }
}

void Win32Window::HandleMouseDown(int button, int x, int y) {
    if (m_mouseDown) {
        m_mouseDown(button, x, y);
    }
}

void Win32Window::HandleMouseUp(int button, int x, int y) {
    if (m_mouseUp) {
        m_mouseUp(button, x, y);
    }
}
void Win32Window::HandleMouseWheel(int x, int y, int zDelta) {
    if (m_mouseWheel) {
        m_mouseWheel(x, y, zDelta);
    }
}

void Win32Window::HandleResize(unsigned int width, unsigned int height) {
    m_newWidth = width;
    m_newHeight = height;

    if (!m_resizing) {
        UpdateSize();
    }
}

void Win32Window::HandleShowWindow(bool shown) {
    if (shown != m_visible) {
        m_visible = shown;
        if (m_show) {
            m_show(shown);
        }
    }
}

void Win32Window::SetResizing(bool resizing) {
    m_resizing = resizing;

    if (!m_resizing) {
        UpdateSize();
    }
}

void Win32Window::UpdateSize() {
    if (m_newWidth != m_width || m_newHeight != m_height) {
        m_width = m_newWidth;
        m_height = m_newHeight;

        if (m_resize) {
            m_resize(m_width, m_height);
        }
    }
}

std::vector<std::filesystem::path> Win32Window::OpenFileDialog(const std::vector<std::wstring>& filter, bool multipleFiles) const {
    return OpenFileDialog(Handle(), filter, multipleFiles);
}

std::vector<std::filesystem::path> Win32Window::OpenFileDialog(HWND hwnd, const std::vector<std::wstring>& filter, bool multipleFiles) const {
    static auto lastDir{ utils::CurrentWorkingDir() };

    wchar_t szFilter[255];
    wchar_t* out = szFilter;
    for (const auto& f : filter) {
        wcscpy_s(out, 255 - (out - szFilter), f.c_str());
        out += f.size() + 1;
    }
    *out = '\0';

    OPENFILENAMEW ofn;
    WCHAR szFile[MAX_PATH + 1] = { 0 };
    ZeroMemory(&ofn, sizeof(OPENFILENAME));
    ofn.lStructSize = sizeof(OPENFILENAME);
    ofn.hwndOwner = /*hwnd*/ nullptr;
    ofn.lpstrFile = szFile;
    ofn.nMaxFile = MAX_PATH;
    ofn.lpstrInitialDir = lastDir.c_str();
    ofn.lpstrFilter = szFilter;
    ofn.nFilterIndex = 1;
    ofn.Flags = OFN_EXPLORER | OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR;

    // TODO: Multiple files.
    // https://docs.microsoft.com/en-us/windows/win32/api/commdlg/ns-commdlg-openfilenamea
    if (multipleFiles) {
        ofn.Flags |= OFN_ALLOWMULTISELECT;
    }

    if (GetOpenFileName(&ofn) != TRUE) {
        return {};
    }

    std::vector<std::filesystem::path> result;
    if (multipleFiles) {
        const wchar_t* file = ofn.lpstrFile;
        std::filesystem::path directory{ file };
        file += wcslen(file) + 1;
        while (file[0] != '\0') {
            result.push_back(directory / file);
            file += wcslen(file) + 1;
        }

        if (result.empty()) {
            result.push_back(directory);
        }

    } else {
        //std::string file{ ofn.lpstrFile };
        result.push_back(ofn.lpstrFile);
    }

    return result;
}

} // namespace ugine::system::win32

#endif // _WIN32
