﻿#ifdef _WIN32

#include "Win32Engine.h"
#include "Win32Window.h"

#include <ugine/core/Error.h>
#include <ugine/gfx/VulkanApi.h>
#include <ugine/gui/Gui.h>

namespace ugine::system::win32 {

Win32Engine::Win32Engine(const std::wstring& title, int windowWidth, int windowHeight, bool srgb, bool useGui) {
    auto window{ CreateEngineWindow(title, windowWidth, windowHeight) };
    Init(srgb, useGui, window);
}

Win32Engine::~Win32Engine() {}

std::shared_ptr<Window> Win32Engine::CreateEngineWindow(const std::wstring& title, int windowWidth, int windowHeight) const {
    HINSTANCE hInstance = GetModuleHandle(nullptr);
    auto window{ std::make_shared<Win32Window>() };
    if (!window->Init(hInstance, title, title, nullptr, windowWidth, windowHeight, true)) {
        UGINE_THROW(core::Error, "Failed to create window.");
    }

    return window;
}

} // namespace ugine::system::win32

#endif // _WIN32
