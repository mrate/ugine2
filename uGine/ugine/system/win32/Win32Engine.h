﻿#pragma once

#ifdef _WIN32

#include "../Engine.h"

#include <Windows.h>

namespace ugine::system::win32 {

class Win32Engine : public Engine {
public:
    Win32Engine(const std::wstring& title, int windowWidth, int windowHeight, bool srgb, bool useGui);
    ~Win32Engine();

private:
    std::shared_ptr<Window> CreateEngineWindow(const std::wstring& title, int windowWidth, int windowHeight) const;
};

} // namespace ugine::system::win32

#endif // _WIN32
