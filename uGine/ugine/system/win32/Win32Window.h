﻿#pragma once

#ifdef _WIN32

#include <ugine/system/Window.h>

#include <Windows.h>

#include <functional>
#include <map>
#include <string>
#include <string_view>
#include <utility>

namespace ugine::system::win32 {

class Win32Window : public Window {
public:
    using CustomWndProc = std::function<LRESULT(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)>;

    Win32Window();
    ~Win32Window();

    std::vector<std::filesystem::path> OpenFileDialog(const std::vector<std::wstring>& filter, bool multipleFiles) const override;

    vk::UniqueSurfaceKHR CreateSurface(vk::Instance instance) const override;
    void InitInput() override;

    bool Init(HINSTANCE hInstance, const std::wstring& title, const std::wstring& name, LPCWSTR iconName, int width, int height, bool resizable = false);

    void Show() override;
    void Close() override;

    void SetTitle(const std::string_view& title) override;

    void InitGui() override;
    void DestroyGui() override;
    void NewFrame() override;

    void Show(int nCmdShow);

    std::pair<int, int> GetSize() const override;
    bool Closed() const;

    HWND Handle() const {
        return m_hWnd;
    }

    HINSTANCE Instance() const {
        return m_hInst;
    }

    void SetMouseCapture(bool capture) override;

private:
    static std::map<HWND, Win32Window*> WIN_MAP;
    static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
    std::vector<std::filesystem::path> OpenFileDialog(HWND hwnd, const std::vector<std::wstring>& filter, bool multipleFiles) const;

    bool CustomWndProcHandler(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

    void HandleKeyDown(WPARAM param);
    void HandleKeyUp(WPARAM param);
    void HandleMouseMove(int x, int y);
    void HandleMouseDown(int button, int x, int y);
    void HandleMouseUp(int button, int x, int y);
    void HandleMouseWheel(int x, int y, int zDelta);
    void HandleResize(unsigned int width, unsigned int height);
    void HandleShowWindow(bool shown);
    void SetResizing(bool resizing);
    void UpdateSize();

    HINSTANCE m_hInst = nullptr;
    HWND m_hWnd = nullptr;
    CustomWndProc m_customWndProc = nullptr;

    unsigned int m_width = 0;
    unsigned int m_height = 0;

    unsigned int m_newWidth = 0;
    unsigned int m_newHeight = 0;

    bool m_visible{ false };
    bool m_resizing{ false };
};

} // namespace ugine::system::win32

#endif // _WIN32
