﻿#pragma once

#ifdef _WIN32

#include "win32/Win32Engine.h"

#ifdef SetWindowText
#undef SetWindowText
#endif

namespace ugine::system {

using EngineImpl = win32::Win32Engine;

} // namespace ugine::system

#endif // _WIN32
