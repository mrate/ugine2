﻿#pragma once

#include <vulkan/vulkan.hpp>

#include <filesystem>
#include <functional>
#include <vector>

namespace ugine::system {

class Window {
public:
    using KeyCallback = std::function<void(unsigned long)>;
    using MouseButtonCallback = std::function<void(int, int, int)>;
    using MouseMoveCallback = std::function<void(int, int)>;
    using MouseWheelCallback = std::function<void(int, int, int)>;
    using ResizeCallback = std::function<void(unsigned int, unsigned int)>;
    using ShowCallback = std::function<void(bool)>;

    virtual ~Window() {}

    virtual vk::UniqueSurfaceKHR CreateSurface(vk::Instance instance) const = 0;
    virtual std::pair<int, int> GetSize() const = 0;

    virtual std::vector<std::filesystem::path> OpenFileDialog(const std::vector<std::wstring>& filter, bool multipleFiles) const = 0;

    virtual void SetMouseCapture(bool capture) = 0;

    virtual void SetTitle(const std::string_view& title) = 0;

    virtual void Show() = 0;
    virtual void Close() = 0;

    virtual void InitInput() = 0;

    virtual void InitGui() = 0;
    virtual void DestroyGui() = 0;
    virtual void NewFrame() = 0;

    const std::wstring& Title() const {
        return m_title;
    }

    void OnKeyDown(KeyCallback callback) {
        m_keyDown = callback;
    }
    void OnKeyUp(KeyCallback callback) {
        m_keyUp = callback;
    }
    void OnMouseMove(MouseMoveCallback callback) {
        m_mouseMove = callback;
    }
    void OnMouseDown(MouseButtonCallback callback) {
        m_mouseDown = callback;
    }
    void OnMouseUp(MouseButtonCallback callback) {
        m_mouseUp = callback;
    }
    void OnMouseWheel(MouseWheelCallback callback) {
        m_mouseWheel = callback;
    }
    void OnResize(ResizeCallback callback) {
        m_resize = callback;
    }
    void OnShow(ShowCallback callback) {
        m_show = callback;
    }

protected:
    // Callbacks
    KeyCallback m_keyDown;
    KeyCallback m_keyUp;
    MouseButtonCallback m_mouseUp;
    MouseButtonCallback m_mouseDown;
    MouseMoveCallback m_mouseMove;
    MouseWheelCallback m_mouseWheel;
    ResizeCallback m_resize;
    ShowCallback m_show;
    std::wstring m_title;
};

} // namespace ugine::system
