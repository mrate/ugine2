﻿#include "Engine.h"

#include "win32/Win32Window.h"

//#include <ugine/core/ConfigService.h>

#include <ugine/assets/AssetService.h>

#include <ugine/core/CoreService.h>
#include <ugine/core/Error.h>
#include <ugine/core/Scheduler.h>

#include <ugine/input/InputService.h>

#include <ugine/gfx/Graphics.h>
#include <ugine/gfx/GraphicsService.h>
#include <ugine/gfx/Presenter.h>
#include <ugine/gfx/VulkanApi.h>

#include <ugine/gui/Gui.h>

#include <ugine/utils/Log.h>
#include <ugine/utils/Profile.h>
#include <ugine/utils/Strings.h>

#include <chrono>

namespace {

ugine::input::InputId ButtonToInput(int button) {
    switch (button) {
    case 0:
        return ugine::input::INPUT_MOUSE_LBUTTON;
    case 1:
        return ugine::input::INPUT_MOUSE_RBUTTON;
    default:
        return ugine::input::INPUT_MOUSE_MBUTTON;
    }
}

ugine::system::Event::MouseButton ButtonToMouseButton(int button) {
    using namespace ugine::system;

    switch (button) {
    case 0:
        return Event::MouseButton::Left;
    case 1:
        return Event::MouseButton::Right;
    case 2:
        return Event::MouseButton::Middle;
    default:
        return Event::MouseButton::None;
    }
}

} // namespace

namespace ugine::system {

Engine::Engine()
    : startTime_{ Clock::now() } {}

Engine::~Engine() {
    gui_ = nullptr;

    presenter_ = nullptr;
    graphics_ = nullptr;
    vulkanApi_ = nullptr;

    window_->Close();
}

void Engine::Init(bool srgb, bool useGui, const std::shared_ptr<Window>& window) {
    PROFILE_THREAD("Main thread");

    window_ = std::move(window);

    utils::InitLogger();

    InitConfig();
    InitServices();
    InitGraphics(srgb);
    InitWindow();

    if (useGui) {
        InitGui();
    }
}

void Engine::SetMultithread(bool multithread) {
    multithread_ = multithread;
}

void Engine::InitConfig() {
    // TODO: Init config service.
    //core::ConfigService::Load("config.json");
}

void Engine::InitGraphics(bool srgb) {
    vulkanApi_ = std::make_unique<gfx::VulkanApi>(utils::ToString(window_->Title()), core::CoreService::IsDebug());
    auto surface{ window_->CreateSurface(vulkanApi_->Instance()) };
    graphics_ = vulkanApi_->CreateFirstSuitableDevice(*surface);
    gfx::GraphicsService::SetGraphics(graphics_);

    const auto [windowWidth, windowHeight] = window_->GetSize();
    presenter_ = std::make_shared<gfx::Presenter>(srgb);
    presenter_->RecreateSwapchain(std::move(surface), windowWidth, windowHeight);
    gfx::GraphicsService::SetPresenter(presenter_);
    gfx::GraphicsService::SetDebug(vulkanApi_->SupportsDebug());

    vulkanApi_->LoadProcAddr(graphics_->Device());

    gfx::GraphicsService::Init();
    assets::AssetService::Init();
}

void Engine::DestroyServices() {
    core::CoreService::Destroy();
    core::Scheduler::Destroy();
}

void Engine::DestroyGraphics() {
    assets::AssetService::Destroy();
    gfx::GraphicsService::Destroy();

    presenter_ = nullptr;
    graphics_ = nullptr;
}

void Engine::InitServices() {
    using namespace std::chrono;

    core::Scheduler::Init();
    core::CoreService::Init(1000ms);
    input::InputService::SetupDefaultMapping();
}

void Engine::InitWindow() {
    window_->InitInput();
    window_->Show();

    window_->OnKeyDown([&](unsigned long key) {
        if (!gui_ || !gui_->CaptureKeyboard()) {
            input::InputService::SetKeyDown(key);
            OnEvent(Event(Event::Type::KeyDown, key));
        }
    });
    window_->OnKeyUp([&](unsigned long key) {
        if (!gui_ || !gui_->CaptureKeyboard()) {
            input::InputService::SetKeyUp(key);
            OnEvent(Event(Event::Type::KeyUp, key));
        }
    });
    window_->OnMouseMove([&](int x, int y) {
        if (!gui_ || !gui_->CaptureMouse()) {
            input::InputService::SetInput(input::INPUT_MOUSE_X, static_cast<float>(x));
            input::InputService::SetInput(input::INPUT_MOUSE_Y, static_cast<float>(y));

            if (mouse_.rightDown) {
                input::InputService::AddInput(input::INPUT_MOUSE_DRAG_X, static_cast<float>(x - mouse_.lastPosX));
                input::InputService::AddInput(input::INPUT_MOUSE_DRAG_Y, static_cast<float>(y - mouse_.lastPosY));

                mouse_.lastPosX = x;
                mouse_.lastPosY = y;
            }

            OnEvent(Event(Event::Type::MouseMove, Event::MouseButton::None, x, y));
        }
    });
    window_->OnMouseUp([&](int button, int x, int y) {
        if (!gui_ || !gui_->CaptureMouse()) {
            input::InputService::SetInput(ButtonToInput(button), 0.0f);
            input::InputService::SetInput(input::INPUT_MOUSE_X, static_cast<float>(x));
            input::InputService::SetInput(input::INPUT_MOUSE_Y, static_cast<float>(y));

            //if (button == 1) {
            //    input::InputService::AddInput(input::INPUT_MOUSE_DRAG_X, 0.0f);
            //    input::InputService::AddInput(input::INPUT_MOUSE_DRAG_Y, 0.0f);

            //    mouse_.lastPosX = x;
            //    mouse_.lastPosY = y;
            //    mouse_.rightDown = false;
            //}

            OnEvent(Event(Event::Type::MouseUp, ButtonToMouseButton(button), x, y));
        }
    });
    window_->OnMouseDown([&](int button, int x, int y) {
        if (!gui_ || !gui_->CaptureMouse()) {
            input::InputService::SetInput(ButtonToInput(button), 1.0f);
            input::InputService::SetInput(input::INPUT_MOUSE_X, static_cast<float>(x));
            input::InputService::SetInput(input::INPUT_MOUSE_Y, static_cast<float>(y));

            //if (button == 1) {
            //    mouse_.lastPosX = x;
            //    mouse_.lastPosY = y;
            //    mouse_.rightDown = true;
            //}

            OnEvent(Event(Event::Type::MouseDown, ButtonToMouseButton(button), x, y));
        }
    });
    window_->OnMouseWheel([&](int x, int y, int delta) {
        if (!gui_ || !gui_->CaptureMouse()) {
            input::InputService::SetInput(input::INPUT_MOUSE_Z, static_cast<float>(delta));
            input::InputService::SetInput(input::INPUT_MOUSE_X, static_cast<float>(x));
            input::InputService::SetInput(input::INPUT_MOUSE_Y, static_cast<float>(y));

            OnEvent(Event(Event::Type::MouseWheel, Event::MouseButton::None, x, y, delta));
        }
    });
    window_->OnResize([&](unsigned int width, unsigned int height) {
        RecreateSwapchain();
        OnResize(width, height);
    });
    window_->OnShow([&](bool shown) {
        windowVisible_ = shown;
        OnShow(shown);
    });
}

void Engine::InitGui() {
    gui_ = std::make_unique<gui::Gui>(window_);
}

void Engine::DestroyGui() {
    gui_ = nullptr;
}

void Engine::RecreateSwapchain() {
    const auto [width, height] = window_->GetSize();

    auto surface{ window_->CreateSurface(vulkanApi_->Instance()) };
    presenter_->RecreateSwapchain(std::move(surface), width, height);
}

void Engine::BeginFrame() {
    // Begin frame.
    presenter_->BeginFrame();
    core::CoreService::BeginFrame();
    gfx::GraphicsService::BeginFrame();
}

int Engine::Run() {
    using namespace std::chrono;

    UGINE_DEBUG("Engine startup time: {}", utils::FormatTime(Clock::now() - startTime_));

    {
        auto userStartupTime{ Clock::now() };

        PROFILE_EVENT("OnStart");

        OnStart();

        UGINE_DEBUG("User startup time: {}", utils::FormatTime(Clock::now() - userStartupTime));
    }

    UGINE_INFO("Startup time: {}", utils::FormatTime(Clock::now() - startTime_));

    MSG msg = { 0 };
    running_ = true;

    while (WM_QUIT != msg.message && running_) {
        PROFILE_EVENT("Event loop");
        if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
            continue;
        }

        if (!windowVisible_) {
            continue;
        }

        PROFILE_FRAME("Engine::Run");

        // Update input.
        input::InputService::UpdateControllers();
        keys_.shift[0] = GetAsyncKeyState(VK_LSHIFT) != 0;
        keys_.shift[1] = GetAsyncKeyState(VK_RSHIFT) != 0;
        keys_.ctrl[0] = GetAsyncKeyState(VK_LCONTROL) != 0;
        keys_.ctrl[1] = GetAsyncKeyState(VK_RCONTROL) != 0;
        keys_.alt[0] = GetAsyncKeyState(VK_LMENU) != 0;
        keys_.alt[1] = GetAsyncKeyState(VK_RMENU) != 0;

        if (!multithread_) {
            BeginFrame();
        }

        {
            PROFILE_EVENT("OnUpdate");

            OnUpdate(core::CoreService::TimeDiffMS());
        }

        if (!multithread_) {
            PROFILE_EVENT("OnFrame");

            OnRender();
        }

        // Reset input.
        input::InputService::SetInput(input::INPUT_MOUSE_DRAG_X, 0.0f);
        input::InputService::SetInput(input::INPUT_MOUSE_DRAG_Y, 0.0f);
        input::InputService::SetInput(input::INPUT_MOUSE_Z, 0.0f);
    }

    graphics_->WaitIdle();

    {
        PROFILE_EVENT("OnEnd");

        OnEnd();
    }

    graphics_->WaitIdle();

    DestroyGui();
    DestroyGraphics();
    DestroyServices();

    return 0;
}

void Engine::Close() {
    running_ = false;
}

void Engine::SetMouseCapture(bool capture) {
    mouseCaptured_ = capture;
    window_->SetMouseCapture(capture);
}

bool Engine::IsShiftPressed(bool left) const {
    return keys_.shift[left ? 0 : 1];
}

bool Engine::IsCtrlPressed(bool left) const {
    return keys_.ctrl[left ? 0 : 1];
}

bool Engine::IsAltPressed(bool left) const {
    return keys_.alt[left ? 0 : 1];
}

uint32_t Engine::Width() const {
    return window_->GetSize().first;
}

uint32_t Engine::Height() const {
    return window_->GetSize().second;
}

void Engine::SetWindowTitle(const std::string_view& title) {
    window_->SetTitle(title);
}

} // namespace ugine::system
