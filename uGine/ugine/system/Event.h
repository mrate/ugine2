﻿#pragma once

namespace ugine::system {

struct Event {
    enum class Type { KeyDown, KeyUp, MouseDown, MouseUp, MouseMove, MouseWheel };

    enum class MouseButton { None, Left, Right, Middle };

    Type type;
    MouseButton mouseButton{ MouseButton::None };
    int x{};
    int y{};
    int wheel{};
    int key{};

    Event(Type type, int key)
        : type{ type }
        , key{ key } {}
    Event(Type type, MouseButton button, int x, int y, int wheel = 0)
        : type{ type }
        , mouseButton{ button }
        , x{ x }
        , y{ y }
        , wheel{ wheel } {}
};

} // namespace ugine::system
