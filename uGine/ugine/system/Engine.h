﻿#pragma once

#include "Event.h"
#include "Window.h"

#include <memory>
#include <string>

namespace ugine::gui {
class Gui;
}

namespace ugine::gfx {
class GraphicsContext;
class Graphics;
class Presenter;
class VulkanApi;
} // namespace ugine::gfx

namespace ugine::system {

class Engine {
public:
    Engine();
    virtual ~Engine();

    int Run();
    void Close();

protected:
    gui::Gui& Gui() {
        return *gui_;
    }

    bool HasGui() const {
        return static_cast<bool>(gui_);
    }

    gfx::Graphics& Graphics() {
        return *graphics_;
    }

    gfx::Presenter& Presenter() {
        return *presenter_;
    }

    const gfx::Graphics& Graphics() const {
        return *graphics_;
    }

    const gfx::Presenter& Presenter() const {
        return *presenter_;
    }

    uint32_t Width() const;
    uint32_t Height() const;

    void SetWindowTitle(const std::string_view& title);

    void SetMouseCapture(bool capture);
    bool IsMouseCaptured() const {
        return mouseCaptured_;
    }

    bool IsShiftPressed(bool left) const;
    bool IsCtrlPressed(bool left) const;
    bool IsAltPressed(bool left) const;

    virtual void OnStart() {}
    virtual void OnEnd() {}
    virtual void OnShow(bool shown) {}

    virtual void OnUpdate(float deltaMS) {}
    virtual void OnRender() {}
    virtual void OnEvent(const Event& event) {}
    virtual void OnResize(uint32_t /*width*/, uint32_t /*height*/) {}

protected:
    void Init(bool srgb, bool useGui, const std::shared_ptr<Window>& window);
    void SetMultithread(bool multithread);
    void BeginFrame();

private:
    using Clock = std::chrono::high_resolution_clock;

    void InitConfig();
    void InitGraphics(bool srgb);
    void InitServices();
    void InitWindow();
    void InitGui();

    void DestroyServices();
    void DestroyGraphics();
    void DestroyGui();

    void RecreateSwapchain();

    std::shared_ptr<Window> window_;
    std::unique_ptr<gfx::VulkanApi> vulkanApi_;
    std::shared_ptr<gfx::Graphics> graphics_;
    std::shared_ptr<gfx::Presenter> presenter_;
    std::unique_ptr<gui::Gui> gui_;

    Clock::time_point startTime_;

    bool running_{ false };
    bool mouseCaptured_{ false };
    bool windowVisible_{ true };
    bool multithread_{ false };

    struct Mouse {
        int lastPosX{ 0 };
        int lastPosY{ 0 };
        bool leftDown{ false };
        bool rightDown{ false };
    } mouse_;

    struct Keys {
        bool shift[2];
        bool ctrl[2];
        bool alt[2];
    } keys_;
};

} // namespace ugine::system
