﻿#pragma once

#include <ugine/core/systems/System.h>

#include <memory>

namespace ugine::physics {

struct PhysicsImplData;
struct PhysicsEngine;

class PhysicsSystem : public core::System {
public:
    ~PhysicsSystem();

    void Init(entt::registry& reg) override;
    void Destroy(entt::registry& reg) override;

    void Update(entt::registry& reg) override;

private:
    void AddObject(entt::registry& reg, entt::entity ent);
    void RemoveObject(entt::registry& reg, entt::entity ent);
    void RemoveImplData(entt::registry& reg, entt::entity ent);
    void UpdateObject(entt::registry& reg, entt::entity ent);
    void MoveObject(entt::registry& reg, entt::entity ent);

    bool SceneEmpty() const;
    void RecreateActor(PhysicsImplData& physicsData, entt::registry& reg, entt::entity ent);

    std::shared_ptr<PhysicsEngine> engine_;
    bool updating_{};
};

} // namespace ugine::physics
