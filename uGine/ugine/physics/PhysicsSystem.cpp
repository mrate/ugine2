﻿#include "PhysicsSystem.h"
#include "PhysXUtils.h"

#include <ugine/core/Component.h>
#include <ugine/core/Scene.h>
#include <ugine/utils/Assert.h>
#include <ugine/utils/Log.h>
#include <ugine/utils/Utils.h>

#include <PxPhysicsAPI.h>

#pragma comment(lib, "PhysX_64.lib")
#pragma comment(lib, "PhysXFoundation_64.lib")
#pragma comment(lib, "PhysXCommon_64.lib")
#pragma comment(lib, "PhysXPvdSDK_static_64.lib")
#pragma comment(lib, "PhysXExtensions_static_64.lib")

using namespace physx;

namespace {
#define PX_RELEASE(x)                                                                                                                                          \
    if (x) {                                                                                                                                                   \
        x->release();                                                                                                                                          \
        x = nullptr;                                                                                                                                           \
    }
} // namespace

namespace ugine::physics {

class UserErrorCallback : public PxErrorCallback {
public:
    void reportError(PxErrorCode::Enum code, const char* message, const char* file, int line) override {
        UGINE_ERROR("PhysX error {}: {} ({}, {})", code, message, file, line);
    }
};

struct PhysicsEngine {
    PxDefaultAllocator allocator;
    PxFoundation* foundation{};
    PxPhysics* physics{};
    PxDefaultCpuDispatcher* dispatcher{};
    PxScene* scene{};
    UserErrorCallback errorCallback;

    // TODO:
    PxMaterial* material{};

    PhysicsEngine() {
        foundation = PxCreateFoundation(PX_PHYSICS_VERSION, allocator, errorCallback);
        UGINE_ASSERT(foundation);

        physics = PxCreatePhysics(PX_PHYSICS_VERSION, *foundation, PxTolerancesScale(), true, nullptr);

        PxSceneDesc sceneDesc(physics->getTolerancesScale());
        sceneDesc.gravity = PxVec3(0.0f, -9.81f, 0.0f);

        dispatcher = PxDefaultCpuDispatcherCreate(2);
        sceneDesc.cpuDispatcher = dispatcher;
        sceneDesc.filterShader = PxDefaultSimulationFilterShader;
        scene = physics->createScene(sceneDesc);
        scene->setFlag(PxSceneFlag::eENABLE_ACTIVE_ACTORS, true);
        scene->setFlag(PxSceneFlag::eEXCLUDE_KINEMATICS_FROM_ACTIVE_ACTORS, true);

        // TODO:
        material = physics->createMaterial(0.5f, 0.5f, 0.6f);
    }

    ~PhysicsEngine() {
        PX_RELEASE(scene);
        PX_RELEASE(dispatcher);
        PX_RELEASE(physics);
        PX_RELEASE(foundation);
    }

    void StepPhysics(float s) {
        scene->simulate(s);
        scene->fetchResults(true);
    }
};

struct PhysicsImplData {
    PxActor* actor{};
    core::RigidBodyComponent prevState;
};

// TODO:
struct ActorData {
    entt::entity ent{};
};

PxActor* AddActor(PhysicsEngine& engine, entt::registry& reg, entt::entity ent) {
    auto& scene{ core::Scene::FromRegistry(reg) };
    auto transform{ scene.Get(ent).GlobalTransformation() };
    auto& rigidBody{ reg.get<core::RigidBodyComponent>(ent) };

    transform.position += transform.rotation * rigidBody.offset;

    PxBoxGeometry boxG{};
    PxSphereGeometry sphereG{};

    PxGeometry* geometry{};

    PxActor* actor{};
    auto trans{ ToPhysX(transform) };

    switch (rigidBody.shape) {
    case core::RigidBodyComponent::Shape::Box:
        boxG = PxBoxGeometry{ ToPhysX(0.5f * rigidBody.scale) };
        geometry = &boxG;
        break;
    case core::RigidBodyComponent::Shape::Sphere:
        sphereG = PxSphereGeometry{ 0.5f * rigidBody.scale.x };
        geometry = &sphereG;
        break;
    case core::RigidBodyComponent::Shape::Plane:
        //geometry = PxPlane{ ToPhysX(0.5f * transform.scale) };
        break;
    }

    if (rigidBody.isDynamic) {
        // TODO: Density + material.
        PxRigidDynamic* dynamic{ PxCreateDynamic(*engine.physics, trans, *geometry, *engine.material, 10.0f) };
        // TODO: Angular damping.
        dynamic->setAngularDamping(0.5f);
        dynamic->setLinearVelocity(ToPhysX(rigidBody.linearVelocity));
        dynamic->setAngularVelocity(ToPhysX(rigidBody.angularVelocity));
        dynamic->setRigidBodyFlag(PxRigidBodyFlag::eKINEMATIC, rigidBody.isKinematic);

        actor = dynamic;
    } else {
        // TODO: Material.
        PxRigidStatic* st{ PxCreateStatic(*engine.physics, trans, *geometry, *engine.material) };

        actor = st;
    }

    engine.scene->addActor(*actor);

    return actor;
}

PhysicsSystem::~PhysicsSystem() {}

void PhysicsSystem::Init(entt::registry& reg) {
    reg.on_construct<core::RigidBodyComponent>().connect<&PhysicsSystem::AddObject>(this);
    reg.on_destroy<core::RigidBodyComponent>().connect<&PhysicsSystem::RemoveObject>(this);
    reg.on_destroy<PhysicsImplData>().connect<&PhysicsSystem::RemoveImplData>(this);
    reg.on_update<core::RigidBodyComponent>().connect<&PhysicsSystem::UpdateObject>(this);
    reg.on_update<core::TransformationComponent>().connect<&PhysicsSystem::MoveObject>(this);
}

void PhysicsSystem::Destroy(entt::registry& reg) {
    reg.on_construct<core::RigidBodyComponent>().disconnect<&PhysicsSystem::AddObject>(this);
    reg.on_destroy<core::RigidBodyComponent>().disconnect<&PhysicsSystem::RemoveObject>(this);
    reg.on_destroy<PhysicsImplData>().disconnect<&PhysicsSystem::RemoveImplData>(this);
    reg.on_update<core::RigidBodyComponent>().disconnect<&PhysicsSystem::UpdateObject>(this);
    reg.on_update<core::TransformationComponent>().disconnect<&PhysicsSystem::MoveObject>(this);
}

void PhysicsSystem::Update(entt::registry& reg) {
    if (engine_ && core::CoreService::PhysicsActive()) {
        utils::ScopedValue u{ updating_, true };

        auto& scene{ core::Scene::FromRegistry(reg) };
        for (auto [ent, phys] : reg.view<PhysicsImplData>().each()) {
            if (phys.actor) {
                if (phys.prevState.isDynamic) {
                    auto* body{ static_cast<PxRigidBody*>(phys.actor) };
                    if (phys.prevState.isKinematic) {
                        body->setGlobalPose(ToPhysX(scene.Get(ent).GlobalTransformation()));
                    }
                }
            }
        }

        // TODO: Limit simulation time.
        engine_->StepPhysics(std::min(core::CoreService::TimeDiffS(), 1.0f / 60.0f));

        PxU32 activeCount{};
        PxActor** activeActors{ engine_->scene->getActiveActors(activeCount) };

        for (PxU32 i = 0; i < activeCount; ++i) {
            if (activeActors[i]->userData != nullptr) {
                PxRigidActor* actor{ static_cast<PxRigidActor*>(activeActors[i]) };
                auto t{ actor->getGlobalPose() };
                auto* data{ static_cast<ActorData*>(actor->userData) };

                auto tr{ FromPhysX(t) };
                auto offset{ reg.get<core::RigidBodyComponent>(data->ent).offset };

                core::Transformation newTrans;
                newTrans.position = tr.position - tr.rotation * offset;
                newTrans.rotation = tr.rotation;
                scene.Get(data->ent).SetGlobalTransformation(newTrans);
            }
        }
    }
}

void PhysicsSystem::MoveObject(entt::registry& reg, entt::entity ent) {
    if (updating_ || !reg.has<core::RigidBodyComponent>(ent)) {
        return;
    }

    auto scene{ reg.ctx<core::Scene*>() };
    auto& rigidBody{ reg.get<core::RigidBodyComponent>(ent) };
    auto& physicsData{ reg.get<PhysicsImplData>(ent) };
    auto transform{ scene->Get(ent).GlobalTransformation() };
    transform.position += transform.rotation * rigidBody.offset;

    if (rigidBody.isDynamic) {
        if (rigidBody.isKinematic) {
            auto* body{ static_cast<PxRigidDynamic*>(physicsData.actor) };
            body->setKinematicTarget(ToPhysX(transform));
        }
    } else {
        auto* body{ static_cast<PxRigidStatic*>(physicsData.actor) };
        body->setGlobalPose(ToPhysX(transform));
    }
}

void PhysicsSystem::AddObject(entt::registry& reg, entt::entity ent) {
    if (!engine_) {
        UGINE_INFO("Initializing physics engine.");

        engine_ = std::make_unique<PhysicsEngine>();
    }

    auto& rigidBody{ reg.get<core::RigidBodyComponent>(ent) };

    auto* actor{ AddActor(*engine_, reg, ent) };
    reg.emplace<PhysicsImplData>(ent, actor, rigidBody);

    // TODO:
    actor->userData = new ActorData{ ent };
}

void PhysicsSystem::RemoveObject(entt::registry& reg, entt::entity ent) {
    auto& rigidBody{ reg.get<core::RigidBodyComponent>(ent) };

    if (reg.has<PhysicsImplData>(ent)) {
        auto& physicsData{ reg.get<PhysicsImplData>(ent) };

        engine_->scene->removeActor(*physicsData.actor);
        reg.remove<PhysicsImplData>(ent);

        // TODO: If there are no active actors, destroy engine.
        if (SceneEmpty()) {
            engine_ = nullptr;
        }
    }
}

void PhysicsSystem::RemoveImplData(entt::registry& reg, entt::entity ent) {
    auto& physicsData{ reg.get<PhysicsImplData>(ent) };

    // TODO:
    if (physicsData.actor->userData) {
        delete physicsData.actor->userData;
        physicsData.actor->userData = nullptr;
    }
}

void PhysicsSystem::RecreateActor(PhysicsImplData& physicsData, entt::registry& reg, entt::entity ent) {
    delete physicsData.actor->userData;
    engine_->scene->removeActor(*physicsData.actor);
    physicsData.actor = AddActor(*engine_, reg, ent);
    physicsData.actor->userData = new ActorData{ ent };
}

void PhysicsSystem::UpdateObject(entt::registry& reg, entt::entity ent) {
    auto scene{ reg.ctx<core::Scene*>() };
    auto& rigidBody{ reg.get<core::RigidBodyComponent>(ent) };
    auto& physicsData{ reg.get<PhysicsImplData>(ent) };

    // TODO: Static vs dynamic.
    if (rigidBody.isDynamic != physicsData.prevState.isDynamic || rigidBody.shape != physicsData.prevState.shape
        || rigidBody.scale != physicsData.prevState.scale) {
        RecreateActor(physicsData, reg, ent);
    } else if (rigidBody.isDynamic) {

        // Dynamic changed.
        auto* body{ static_cast<PxRigidDynamic*>(physicsData.actor) };

        UGINE_ASSERT(body);

        if (rigidBody.offset != physicsData.prevState.offset) {
            auto transform{ scene->Get(ent).GlobalTransformation() };
            transform.position += transform.rotation * rigidBody.offset;
            body->setGlobalPose(ToPhysX(transform));
        }

        // Kinematic change.
        if (rigidBody.isKinematic != physicsData.prevState.isKinematic) {
            body->setRigidBodyFlag(PxRigidBodyFlag::eKINEMATIC, rigidBody.isKinematic);
        }

        // Velocity.
        if (rigidBody.isKinematic) {
            auto transform{ ToPhysX(scene->Get(ent).GlobalTransformation()) };

            body->setKinematicTarget(transform);
        } else {
            if (rigidBody.linearVelocity != physicsData.prevState.linearVelocity) {
                body->setLinearVelocity(ToPhysX(rigidBody.linearVelocity));
            }

            if (rigidBody.angularVelocity != physicsData.prevState.angularVelocity) {
                body->setAngularVelocity(ToPhysX(rigidBody.angularVelocity));
            }
        }

        // Forces.
        bool applyForce{ rigidBody.force.x || rigidBody.force.y || rigidBody.force.z };
        bool applyTorque{ rigidBody.torque.x || rigidBody.torque.y || rigidBody.torque.z };

        if (applyForce) {
            body->addForce(ToPhysX(rigidBody.force));
        }

        if (applyTorque) {
            body->addTorque(ToPhysX(rigidBody.torque));
        }

        rigidBody.force = {};
        rigidBody.torque = {};
    } else {
        // Static changed.
        auto* body{ static_cast<PxRigidStatic*>(physicsData.actor) };

        if (rigidBody.offset != physicsData.prevState.offset) {
            auto transform{ scene->Get(ent).GlobalTransformation() };
            transform.position += transform.rotation * rigidBody.offset;
            body->setGlobalPose(ToPhysX(transform));
        }
    }

    physicsData.prevState = rigidBody;
}

bool PhysicsSystem::SceneEmpty() const {
    return engine_->scene->getNbActors(PxActorTypeFlag::eRIGID_DYNAMIC | PxActorTypeFlag::eRIGID_STATIC) == 0;
}

} // namespace ugine::physics
