#include "PhysXUtils.h"

using namespace physx;

namespace ugine::physics {

PxVec3 ToPhysX(const glm::vec3& g) {
    return PxVec3{ g.x, g.y, g.z };
}

PxQuat ToPhysX(const glm::fquat& g) {
    return PxQuat{ g.x, g.y, g.z, g.w };
}

PxTransform ToPhysX(const ugine::core::Transformation& t) {
    return PxTransform{ ToPhysX(t.position), ToPhysX(t.rotation) };
}

glm::vec3 FromPhysX(const PxVec3& v) {
    return glm::vec3{ v.x, v.y, v.z };
}

glm::fquat FromPhysX(const PxQuat& v) {
    return glm::fquat{ v.w, v.x, v.y, v.z };
}

ugine::core::Transformation FromPhysX(const PxTransform& t) {
    return ugine::core::Transformation{ FromPhysX(t.p), FromPhysX(t.q), glm::vec3{ 1.0f } };
}

} // namespace ugine::physics