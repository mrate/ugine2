#pragma once

#include <ugine/core/Transformation.h>

#include <PxPhysicsAPI.h>

#include <glm/glm.hpp>

namespace ugine::physics {

physx::PxVec3 ToPhysX(const glm::vec3& g);
physx::PxQuat ToPhysX(const glm::fquat& g);
physx::PxTransform ToPhysX(const core::Transformation& t);

glm::vec3 FromPhysX(const physx::PxVec3& v);
glm::fquat FromPhysX(const physx::PxQuat& v);
core::Transformation FromPhysX(const physx::PxTransform& t);

} // namespace ugine::physics