﻿#pragma once

#include <ugine/core/systems/System.h>

namespace ugine::script {

class NativeScriptSystem : public core::System {
public:
    void Init(entt::registry& registry) override;
    void Destroy(entt::registry& registry) override;

    void Update(entt::registry& registry) override;
    void PostRender(entt::registry& registry, gfx::GPUCommandList command) override;

    void ScriptAdd(entt::registry& registry, entt::entity ent);
    void ScriptRemove(entt::registry& registry, entt::entity ent);

private:
};

} // namespace ugine::script
