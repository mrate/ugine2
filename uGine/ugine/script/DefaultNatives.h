﻿#pragma once

#include "NativeScripts.h"
#include "natives/CameraController.h"
#include "natives/DebugTracer.h"
#include "natives/DummyScript.h"
#include "natives/Shaker.h"

namespace ugine::script {

namespace {
    NativeScripts::Register<defaults::CameraController> __cameraController("Camera controller");
    NativeScripts::Register<defaults::DummyScript> __dummy("Dummy");
    NativeScripts::Register<defaults::DebugTracer> __debugTracer("Debug tracer");
    NativeScripts::Register<defaults::Shaker> __shaker("Shaker");
} // namespace

} // namespace ugine::script
