﻿#include "NativeScript.h"

namespace ugine::script {

core::Transformation NativeScript::Transformation() const {
    UGINE_ASSERT(owner_.IsValid());

    return owner_.Has<core::TransformationComponent>() ? owner_.Component<core::TransformationComponent>().transformation : core::Transformation{};
}

void NativeScript::SetTransformation(const core::Transformation& trans) {
    UGINE_ASSERT(owner_.IsValid());

    if (!owner_.Has<core::TransformationComponent>()) {
        owner_.CreateComponent<core::TransformationComponent>(trans);
    } else {
        owner_.Patch<core::TransformationComponent>([&](auto& c) { c.transformation = trans; });
    }
}

void NativeScript::LookAt(const glm::vec3& to) {
    UGINE_ASSERT(owner_.IsValid());
    UGINE_THROW(core::Error, "Not implemented");
}

void NativeScript::LookAt(const core::GameObject& go) {
    UGINE_ASSERT(owner_.IsValid());
    UGINE_THROW(core::Error, "Not implemented");
}

} // namespace ugine::script
