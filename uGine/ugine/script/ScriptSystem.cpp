﻿#include "ScriptSystem.h"
#include "NativeScript.h"

#include <ugine/core/GameObject.h>
#include <ugine/core/Scene.h>

namespace ugine::script {

void NativeScriptSystem::Init(entt::registry& registry) {
    registry.on_construct<core::NativeScriptComponent>().connect<&NativeScriptSystem::ScriptAdd>(this);
    registry.on_destroy<core::NativeScriptComponent>().connect<&NativeScriptSystem::ScriptRemove>(this);
}

void NativeScriptSystem::Destroy(entt::registry& registry) {
    registry.on_construct<core::NativeScriptComponent>().disconnect<&NativeScriptSystem::ScriptAdd>(this);
    registry.on_destroy<core::NativeScriptComponent>().disconnect<&NativeScriptSystem::ScriptRemove>(this);
}

void NativeScriptSystem::Update(entt::registry& registry) {
    for (const auto& [entt, scripts] : registry.view<core::NativeScriptComponent>().each()) {
        for (auto& script : scripts.Scripts()) {
            script->Update();
        }
    }
}

void NativeScriptSystem::PostRender(entt::registry& registry, gfx::GPUCommandList command) {
    for (const auto& [entt, scripts] : registry.view<core::NativeScriptComponent>().each()) {
        for (auto& script : scripts.Scripts()) {
            script->PostRender();
        }
    }
}

void NativeScriptSystem::ScriptAdd(entt::registry& registry, entt::entity ent) {
    auto& scene{ core::Scene::FromRegistry(registry) };

    for (auto& script : registry.get<core::NativeScriptComponent>(ent).Scripts()) {
        script->OnAttach(scene.Get(ent));
    }
}

void NativeScriptSystem::ScriptRemove(entt::registry& registry, entt::entity ent) {
    auto& scene{ core::Scene::FromRegistry(registry) };

    for (auto& script : registry.get<core::NativeScriptComponent>(ent).Scripts()) {
        script->OnDetach(scene.Get(ent));
    }
}

} // namespace ugine::script
