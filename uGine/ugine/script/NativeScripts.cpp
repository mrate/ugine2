#include "NativeScripts.h"

#include <ugine/utils/Log.h>

namespace ugine::script {

std::shared_ptr<NativeScript> NativeScripts::Create(const std::string_view& name) {
    auto it{ Instance().registered_.find(name.data()) };
    if (it == Instance().registered_.end()) {
        UGINE_ERROR("Native script '{}' not registered.", name);
        return {};
    }

    return it->second->Create();
}

void NativeScripts::AddRegistrator(std::unique_ptr<Registrator> registrator) {
    bool registered{ registered_.insert(std::make_pair(registrator->Name(), std::move(registrator))).second };
    if (!registered) {
        UGINE_ERROR("Native script '{}' already registered.", registrator->Name());
    }
}

void NativeScripts::Destroy() {
    Instance().registered_.clear();
}

} // namespace ugine::script