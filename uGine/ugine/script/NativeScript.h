﻿#pragma once

#include <ugine/core/GameObject.h>

namespace ugine::script {

class NativeScript {
public:
    virtual ~NativeScript() {}

    virtual const char* Name() const = 0;
    virtual void OnAttach() {}
    virtual void OnDetach() {}
    virtual void Update() {}
    virtual void PostRender() {}

    core::GameObject& Owner() {
        return owner_;
    }

    const core::GameObject& Owner() const {
        return owner_;
    }

    core::Scene& Scene() {
        auto scene{ Owner().Scene() };
        UGINE_ASSERT(scene);
        return *scene;
    }

    template <typename T> T* Component() {
        UGINE_ASSERT(Owner());
        return Owner()->template Component<T>();
    }

    core::Transformation Transformation() const;
    void SetTransformation(const core::Transformation& trans);
    void LookAt(const glm::vec3& to);
    void LookAt(const core::GameObject& go);
    void OnAttach(const core::GameObject& go) {
        if (owner_ != go) {
            owner_ = go;
            OnAttach();
        }
    }
    void OnDetach(const core::GameObject& go) {
        OnDetach();
        owner_ = {};
    }

private:
    core::GameObject owner_{};
};

} // namespace ugine::script
