﻿#pragma once

#include <ugine/script/NativeScript.h>

#include <chrono>

namespace ugine::script::defaults {

class DummyScript : public NativeScript {
public:
    const char* Name() const {
        return "Dummy";
    };

    void OnAttach() override;
    void OnDetach() override;
    void Update() override;

private:
    std::chrono::system_clock::time_point time_;
};

} // namespace ugine::script::defaults
