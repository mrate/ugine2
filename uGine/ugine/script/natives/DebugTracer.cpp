#include "DebugTracer.h"

#include <ugine/script/NativeScripts.h>
#include <ugine/utils/Log.h>

using namespace ugine::script;

namespace ugine::script::defaults {

void DebugTracer::OnAttach() {
    UGINE_DEBUG("DebugTracer::OnAttach [{}]", Owner().Name());

    time_ = std::chrono::system_clock::now();
    lastTransform_ = Owner().Component<core::TransformationComponent>().transformation;
}

void DebugTracer::OnDetach() {
    UGINE_DEBUG("[{}] DebugTracer::OnDetach", Owner().Name());
}

void DebugTracer::Update() {
    auto now{ std::chrono::system_clock::now() };
    auto trans{ Owner().Component<core::TransformationComponent>().transformation };

    if (trans != lastTransform_) {
        UGINE_DEBUG("[{}] DebugTracer::Transformed: [{}, {}, {}] / [{}, {}, {}, {}] / [{}, {}, {}] ", Owner().Name(), trans.position.x, trans.position.y,
            trans.position.z, trans.rotation.x, trans.rotation.y, trans.rotation.z, trans.rotation.w, trans.scale.x, trans.scale.y, trans.scale.z);

        time_ = now;
        lastTransform_ = trans;
    }

    if (now - time_ >= std::chrono::milliseconds(periodMS_)) {
        time_ = now;
        UGINE_DEBUG("[{}] DebugTracer::Active", Owner().Name());
    }
}

} // namespace ugine::script::defaults
