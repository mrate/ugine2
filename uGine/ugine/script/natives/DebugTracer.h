#pragma once

#include <ugine/core/Transformation.h>

#include <ugine/script/NativeScript.h>

#include <chrono>

namespace ugine::script::defaults {

class DebugTracer : public NativeScript {
public:
    const char* Name() const {
        return "Debug tracer";
    };

    void OnAttach() override;
    void OnDetach() override;
    void Update() override;

private:
    std::chrono::system_clock::time_point time_;
    int periodMS_{ 1000 };
    core::Transformation lastTransform_;
};

} // namespace ugine::script::defaults
