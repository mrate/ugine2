﻿#include "DummyScript.h"

#include <ugine/script/NativeScripts.h>
#include <ugine/utils/Log.h>

using namespace ugine::script;

namespace ugine::script::defaults {

void DummyScript::OnAttach() {
    UGINE_INFO("DummyScript::OnAttach");

    time_ = std::chrono::system_clock::now();
}

void DummyScript::OnDetach() {
    UGINE_INFO("DummyScript::OnDetach");
}

void DummyScript::Update() {
    auto now{ std::chrono::system_clock::now() };

    if (now - time_ >= std::chrono::seconds(1)) {
        time_ = now;
        UGINE_INFO("DummyScript::Update");
    }
}

} // namespace ugine::script::defaults
