﻿#pragma once

#include <ugine/script/NativeScript.h>

namespace ugine::script::defaults {

class CameraController : public NativeScript {
public:
    const char* Name() const override {
        return "Camera controller";
    }
    void OnAttach() override;
    void Update() override;

private:
    bool firstUpdate_{ true };
    float yaw_{};
    float pitch_{};
};

} // namespace ugine::script::defaults
