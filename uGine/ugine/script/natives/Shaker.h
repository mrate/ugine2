﻿#pragma once

#include <ugine/core/Transformation.h>

#include <ugine/script/NativeScript.h>

#include <chrono>

namespace ugine::script::defaults {

class Shaker : public NativeScript {
public:
    const char* Name() const {
        return "Shaker";
    };

    void OnAttach() override;
    void OnDetach() override;
    void Update() override;

    const glm::vec3& Radius() const {
        radius_;
    }

    void SetRadius(const glm::vec3& v) {
        radius_ = v;
    }

private:
    using Clock = std::chrono::high_resolution_clock;

    glm::vec3 position_{};
    glm::vec3 radius_{ 0.5f };

    glm::vec3 prevPosition_{};
    glm::vec3 targetPosition_{};
    float time_{ 250.0f };
    Clock::time_point lastTime_;
};

} // namespace ugine::script::defaults
