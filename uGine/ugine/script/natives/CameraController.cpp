﻿#include "CameraController.h"

#include <ugine/core/CoreService.h>
#include <ugine/input/InputService.h>

namespace ugine::script::defaults {

void CameraController::OnAttach() {
    firstUpdate_ = true;
}

void CameraController::Update() {
    using namespace ugine::core;
    using namespace ugine::input;

    const float time{ std::min(0.02f, CoreService::TimeDiffS()) };

    const float speed{ 4.0f * time * (InputService::KeyDown(VK_SHIFT) ? 2.0f : 1.0f) + std::abs(InputService::GetAction(ACTION_POINTER_Z) * 0.001f) };
    const float rotSpeed{ 0.25f * time };

    float analogX{ InputService::GetAction(ACTION_LEFT_ANALOG_X) };
    float analogY{ InputService::GetAction(ACTION_LEFT_ANALOG_Y) };

    bool forward{ InputService::KeyDown('W') || InputService::GetAction(ACTION_POINTER_Z) > 0 || InputService::GetAction(ACTION_RIGHT_ANALOG_Y) < 0.0f };
    bool backward{ InputService::KeyDown('S') || InputService::GetAction(ACTION_POINTER_Z) < 0 || InputService::GetAction(ACTION_RIGHT_ANALOG_Y) > 0.0f };
    bool left{ InputService::KeyDown('A') || InputService::GetAction(ACTION_RIGHT_ANALOG_X) < 0.0f };
    bool right{ InputService::KeyDown('D') || InputService::GetAction(ACTION_RIGHT_ANALOG_X) > 0.0f };
    bool up{ InputService::KeyDown('E') };
    bool down{ InputService::KeyDown('Q') };

    if (!(forward || backward || left || right || up || down || analogX != 0.0f || analogY != 0.0f)) {
        return;
    }

    if (firstUpdate_) {
        firstUpdate_ = false;

        auto euler{ glm::eulerAngles(Transformation().rotation) };
        pitch_ = euler.x;
        yaw_ = euler.y;
    }

    //Owner()->Follow(nullptr);

    yaw_ -= fmod(analogX * rotSpeed * glm::pi<float>(), glm::pi<float>() * 2.0f);
    pitch_ -= analogY * rotSpeed * glm::pi<float>();
    pitch_ = std::max(std::min(pitch_, glm::pi<float>() * 0.499f), -glm::pi<float>() * 0.499f);

    const auto heading{ glm::fquat(glm::vec3{ 0.0f, yaw_, 0.0f }) };
    const auto rotation{ glm::fquat(glm::vec3{ pitch_, yaw_, 0.0f }) };

    glm::vec3 forwardOffset{ rotation * ugine::core::FORWARD };
    glm::vec3 sideOffset{ glm::cross(heading * ugine::core::FORWARD, ugine::core::UP) };
    glm::vec3 upOffset{ ugine::core::UP };

    if (left || right) {
        sideOffset *= (left ? -1 : 1) * speed;
    } else {
        sideOffset = glm::vec3(0.0f);
    }

    if (up || down) {
        upOffset *= (down ? -1 : 1) * speed;
    } else {
        upOffset = glm::vec3(0.0f);
    }

    if (forward || backward) {
        forwardOffset *= (forward ? 1 : -1) * speed;
    } else {
        forwardOffset = glm::vec3(0.0f);
    }

    auto trans{ Transformation() };
    trans.position = trans.position + forwardOffset + sideOffset + upOffset;
    trans.rotation = rotation;
    SetTransformation(trans);
}

} // namespace ugine::script::defaults
