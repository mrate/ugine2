﻿#include "Shaker.h"

#include <ugine/math/Math.h>
#include <ugine/script/NativeScripts.h>
#include <ugine/utils/Log.h>

using namespace ugine::script;

namespace ugine::script::defaults {

void Shaker::OnAttach() {
    position_ = Owner().Component<core::TransformationComponent>().transformation.position;
    lastTime_ = Clock::now();

    prevPosition_ = glm::vec3{ 0.0f };
    targetPosition_ = radius_ * math::RandomUniformVector();
}

void Shaker::OnDetach() {}

void Shaker::Update() {
    auto now{ Clock::now() };
    if (std::chrono::duration_cast<std::chrono::milliseconds>(now - lastTime_).count() > time_) {
        lastTime_ = now;
        prevPosition_ = targetPosition_;
        targetPosition_ = radius_ * math::RandomUniformVector();
    }

    float prog{ static_cast<float>(std::chrono::duration_cast<std::chrono::milliseconds>(now - lastTime_).count()) / time_ };
    Owner().MoveTo(position_ + glm::mix(prevPosition_, targetPosition_, prog));
}

} // namespace ugine::script::defaults
