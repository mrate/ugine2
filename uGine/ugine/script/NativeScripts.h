#pragma once

#include <ugine/script/NativeScript.h>
#include <ugine/utils/Singleton.h>

#include <map>
#include <memory>
#include <string>

namespace ugine::script {

class NativeScripts : public utils::Singleton<NativeScripts> {
public:
    struct Registrator {
        virtual ~Registrator() {}

        virtual const std::string& Name() const = 0;
        virtual std::shared_ptr<NativeScript> Create() const = 0;
    };

    template <typename T> struct RegistratorImpl : public Registrator {
        RegistratorImpl(const std::string& name)
            : name_(name) {}

        const std::string& Name() const override {
            return name_;
        }

        std::shared_ptr<NativeScript> Create() const override {
            return std::make_shared<T>();
        }

    private:
        std::string name_;
    };

    template <typename T> struct Register {
        Register(const std::string& name) {
            NativeScripts::Instance().AddRegistrator(std::make_unique<NativeScripts::RegistratorImpl<T>>(name));
        }
    };

    static std::shared_ptr<NativeScript> Create(const std::string_view& name);

    static void Init() {}
    static void Destroy();

    void AddRegistrator(std::unique_ptr<Registrator> registrator);

    // TODO:
    const std::map<std::string, std::unique_ptr<Registrator>>& Registered() const {
        return registered_;
    }

private:
    std::map<std::string, std::unique_ptr<Registrator>> registered_;
};

} // namespace ugine::script