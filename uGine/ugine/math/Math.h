﻿#pragma once

#include <ugine/core/Camera.h>
#include <ugine/core/Core.h>

#include <glm/glm.hpp>

#include <array>
#include <vector>

namespace ugine::math {

constexpr float PI{ 3.1415926535897932f };
constexpr float TWO_PI{ 6.28318530718f };
constexpr float PI_INV{ 0.31830988618f };
constexpr float PI_OVER_2{ 1.57079632679f };
constexpr float PI_OVER_4{ 0.78539816339f };
constexpr float PI_HALF{ PI_OVER_2 };

enum class Axis {
    XPos,
    XNeg,
    YPos,
    YNeg,
    ZPos,
    ZNeg,
};

float Max(const glm::vec3& vec);

void TurnVector(glm::vec3& vec, Axis up);

glm::fquat LookAt(const glm::vec3& forward, const glm::vec3& up = core::UP);

glm::vec3 QuatToEuler(const glm::fquat& q);
glm::fquat EulerToQuat(const glm::vec3& v);

inline glm::vec3 Interpolate(const glm::vec3& a, const glm::vec3& b, float p) {
    return glm::mix(a, b, p);
}

inline glm::fquat Interpolate(const glm::fquat& a, const glm::fquat& b, float p) {
    return glm::slerp(a, b, p);
}

glm::vec3 RandomUniformVector();
uint32_t Random(uint32_t from, uint32_t to);
float RandomFloat();

} // namespace ugine::math
