﻿#pragma once

#include <glm/glm.hpp>

namespace ugine::math {

class AABB;
struct Frustum;

float PointPlaneDistance(const glm::vec3& planeNormal, const glm::vec3& point);
float PointAABBDistanceSquared(const AABB& aabb, const glm::vec3& point);

bool InFrustum(const Frustum& frustum, const AABB& aabb);

int CalcLod(const glm::vec3& cameraPosition, const glm::vec3& position);
int CalcLod(float distance);

} // namespace ugine::math
