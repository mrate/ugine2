﻿#pragma once

#include <glm/glm.hpp>

#include <array>

namespace ugine::math {

class Plane {
public:
    Plane() {}
    Plane(const glm::vec3& orig, const glm::vec3& normal)
        : normal{ normal }
        , d{ -glm::dot(normal, orig) } {}

    Plane(const glm::vec3& v1, const glm::vec3& v2, const glm::vec3& v3) {
        normal = glm::normalize(glm::cross(v2 - v1, v3 - v1));
        d = -glm::dot(normal, v1);
    }

    glm::vec3 normal;
    float d{};
};

struct Frustum {
    std::array<glm::vec4, 6> planes;
};

} // namespace ugine::math
