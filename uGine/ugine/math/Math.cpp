﻿#include "Math.h"

#include <glm/gtc/quaternion.hpp>
#include <ugine/core/Core.h>

#include <list>
#include <random>

namespace {

float angle(const glm::vec2& a, const glm::vec2& b) {
    auto dot = a.x * b.x + a.y * b.y;
    auto det = a.x * b.y - a.y * b.x;
    return atan2(det, dot);
}

} // namespace

namespace ugine::math {

float Max(const glm::vec3& vec) {
    return std::max(std::max(vec.x, vec.y), vec.z);
}

void TurnVector(glm::vec3& vec, Axis up) {
    switch (up) {
    case Axis::XPos:
        std::swap(vec.x, vec.y);
        vec.x *= -1;
        break;
    case Axis::XNeg:
        std::swap(vec.x, vec.y);
        vec.y *= -1;
        break;
    case Axis::YPos:
        // No-op.
        break;
    case Axis::YNeg:
        vec.x *= -1;
        vec.y *= -1;
        break;
    case Axis::ZPos:
        std::swap(vec.y, vec.z);
        vec.z *= -1;
        break;
    case Axis::ZNeg:
        std::swap(vec.y, vec.z);
        vec.y *= -1;
        break;
    }
}

glm::fquat LookAt(const glm::vec3& forward, const glm::vec3& up) {
    return glm::quatLookAtRH(glm::normalize(forward), up);
}

float UnwindDegrees(float a) {
    while (a > 180.f) {
        a -= 360.f;
    }
    while (a < -180.f) {
        a += 360.f;
    }
    return a;
}

void UnwindDegrees(glm::vec3& a) {
    a.x = UnwindDegrees(a.x);
    a.y = UnwindDegrees(a.y);
    a.z = UnwindDegrees(a.z);
}

glm::vec3 QuatToEuler(const glm::fquat& q) {
    glm::vec3 result;
    const float sqw = q.w * q.w;
    const float sqx = q.x * q.x;
    const float sqy = q.y * q.y;
    const float sqz = q.z * q.z;
    const float unit = sqx + sqy + sqz + sqw; // if normalised is one, otherwise is correction factor
    const float test = q.x * q.w - q.y * q.z;

    if (test > 0.499995f * unit) {
        // singularity at north pole

        // yaw pitch roll
        result.y = 2.0f * atan2f(q.y, q.x);
        result.x = PI_OVER_2;
        result.z = 0;
    } else if (test < -0.499995f * unit) {
        // singularity at south pole

        // yaw pitch roll
        result.y = -2.0f * atan2f(q.y, q.x);
        result.x = -PI_OVER_2;
        result.z = 0;
    } else {
        // yaw pitch roll
        const glm::fquat tq{ q.w, q.z, q.x, q.y };
        result.y = atan2f(2.0f * tq.x * tq.w + 2.0f * tq.y * tq.z, 1 - 2.0f * (tq.z * tq.z + tq.w * tq.w));
        result.x = asinf(2.0f * (tq.x * tq.z - tq.w * tq.y));
        result.z = atan2f(2.0f * tq.x * tq.y + 2.0f * tq.z * tq.w, 1 - 2.0f * (tq.y * tq.y + tq.z * tq.z));
    }

    result = glm::degrees(result);
    UnwindDegrees(result);
    return result;
}

glm::fquat EulerToQuat(const glm::vec3& v) {
    return glm::fquat(v);
}

glm::vec3 RandomUniformVector() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> dis(0.0, 1.0);
    return glm::vec3{ dis(gen), dis(gen), dis(gen) };
}

uint32_t Random(uint32_t from, uint32_t to) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int<uint32_t> dis(from, to);
    return dis(gen);
}

float RandomFloat() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> dis(0.0, 1.0);
    return dis(gen);
}

} // namespace ugine::math
