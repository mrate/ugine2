#pragma once

#include "Math.h"

#include <vector>

namespace ugine::math {

void FastPoissonDisk2D(float maxX, float maxY, float minDistance, std::vector<glm::vec2>& output, uint32_t maxCount = 0);

}