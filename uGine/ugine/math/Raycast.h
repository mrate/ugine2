﻿#pragma once

#include "Aabb.h"

namespace ugine::math {

struct Ray {
    glm::vec3 origin;
    glm::vec3 dir;
};

bool Intersect(const Ray& ray, const AABB& aabb, float* distance = nullptr, glm::vec3* hit = nullptr);

Ray RayFromCamera(const glm::mat4& projectionMatrix, const glm::mat4& viewMatrix, float x, float y);

} // namespace ugine::math
