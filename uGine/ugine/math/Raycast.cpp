﻿#include "Raycast.h"

namespace ugine::math {

bool Intersect(const Ray& ray, const AABB& aabb, float* distance, glm::vec3* hit) {
    glm::vec3 invdir{ 1.0f / ray.dir };
    int sign[3] = { invdir.x < 0 ? 1 : 0, invdir.y < 0 ? 1 : 0, invdir.z < 0 ? 1 : 0 };

    glm::vec3 bounds[2] = { aabb.Min(), aabb.Max() };

    float tmin = (bounds[sign[0]].x - ray.origin.x) * invdir.x;
    float tmax = (bounds[1 - sign[0]].x - ray.origin.x) * invdir.x;
    float tymin = (bounds[sign[1]].y - ray.origin.y) * invdir.y;
    float tymax = (bounds[1 - sign[1]].y - ray.origin.y) * invdir.y;

    if ((tmin > tymax) || (tymin > tmax)) {
        return false;
    }
    if (tymin > tmin) {
        tmin = tymin;
    }
    if (tymax < tmax) {
        tmax = tymax;
    }

    float tzmin = (bounds[sign[2]].z - ray.origin.z) * invdir.z;
    float tzmax = (bounds[1 - sign[2]].z - ray.origin.z) * invdir.z;

    if ((tmin > tzmax) || (tzmin > tmax)) {
        return false;
    }
    if (tzmin > tmin) {
        tmin = tzmin;
    }
    if (tzmax < tmax) {
        tmax = tzmax;
    }

    if (distance) {
        *distance = tmin;
    }

    return true;
}

//bool Intersect(const Ray& ray, const AABB& aabb, glm::vec3* hit) {
//    enum Quadrant { Left = 0, Right, Middle };
//
//    Quadrant quadrant[3];
//
//    bool inside{ true };
//    int whichPlane{};
//    glm::vec3 maxT;
//    glm::vec3 candidatePlane;
//
//    /* Find candidate planes; this loop can be avoided if
//   	rays cast all from the eye(assume perpsective view) */
//    for (int i = 0; i < 3; ++i) {
//        if (ray.origin[i] < aabb.Min()[i]) {
//            quadrant[i] = Left;
//            candidatePlane[i] = aabb.Min()[i];
//            inside = true;
//        } else if (ray.origin[i] > aabb.Max()[i]) {
//            quadrant[i] = Right;
//            candidatePlane[i] = aabb.Max()[i];
//            inside = false;
//        } else {
//            quadrant[i] = Middle;
//        }
//    }
//
//    /* Ray origin inside bounding box */
//    if (inside) {
//        if (hit) {
//            *hit = ray.origin;
//        }
//        return true;
//    }
//
//    /* Calculate T distances to candidate planes */
//    for (int i = 0; i < 3; ++i) {
//        if (quadrant[i] != Middle && ray.dir[i] != 0.0f) {
//            maxT[i] = (candidatePlane[i] - ray.origin[i]) / ray.dir[i];
//        } else {
//            maxT[i] = -1.0f;
//        }
//    }
//
//    /* Get largest of the maxT's for final choice of intersection */
//    whichPlane = 0;
//    for (int i = 1; i < 3; i++) {
//        if (maxT[whichPlane] < maxT[i]) {
//            whichPlane = i;
//        }
//    }
//
//    /* Check final candidate actually inside box */
//    if (maxT[whichPlane] < 0.0f) {
//        return false;
//    }
//
//    glm::vec3 coord;
//    for (int i = 0; i < 3; i++) {
//        if (whichPlane != i) {
//            coord[i] = ray.origin[i] + maxT[whichPlane] * ray.dir[i];
//            if (coord[i] < aabb.Min()[i] || coord[i] > aabb.Max()[i]) {
//                return true;
//            }
//        } else {
//            coord[i] = candidatePlane[i];
//        }
//    }
//
//    if (hit) {
//        *hit = coord;
//    }
//
//    return true;
//}

Ray RayFromCamera(const glm::mat4& projectionMatrix, const glm::mat4& viewMatrix, float x, float y) {
    glm::vec3 rayNds{ 2.0f * x - 1.0f, 1.0f - 2.0f * y, 1.0f };
    glm::vec4 rayClip{ rayNds.x, rayNds.y, -1.0f, 1.0f };
    glm::vec4 rayEye = glm::inverse(projectionMatrix) * rayClip;
    rayEye = glm::vec4(rayEye.x, rayEye.y, -1.0f, 0.0f);
    glm::vec3 rayWor{ glm::inverse(viewMatrix) * rayEye };
    return Ray{ glm::vec3{ 0.0f }, glm::normalize(rayWor) };
}

} // namespace ugine::math
