﻿#include "AssetService.h"

#include <ugine/gfx/GraphicsService.h>
//#include <ugine/gfx/import/MeshLoader.h>

#include <ugine/utils/Strings.h>

namespace ugine::assets {

namespace {
    int CubeFileIndex(const std::filesystem::path& file) {
        using namespace utils;

        auto n{ file.filename().string() };

        if (Contains(n, "001") || Contains(n, "posx", true)) {
            return 0;
        } else if (Contains(n, "002") || Contains(n, "negx", true)) {
            return 1;
        } else if (Contains(n, "003") || Contains(n, "posy", true)) {
            return 2;
        } else if (Contains(n, "004") || Contains(n, "negy", true)) {
            return 3;
        } else if (Contains(n, "005") || Contains(n, "posz", true)) {
            return 4;
        } else if (Contains(n, "006") || Contains(n, "negz", true)) {
            return 5;
        }

        return 0;
    }
} // namespace

void SortCubeMapFiles(std::vector<std::filesystem::path>& files) {
    std::sort(files.begin(), files.end(), [](const auto& f1, const auto& f2) { return CubeFileIndex(f1) < CubeFileIndex(f2); });
}

AssetService& AssetService::Instance() {
    static AssetService i;
    return i;
}

void AssetService::Init() {}

void AssetService::Destroy() {
    auto& i{ Instance() };
    i.textureCache_.clear();
    i.textureCubeCache_.clear();
}

gfx::TextureRef AssetService::LoadTexture(const std::filesystem::path& name, bool withMips) {
    auto key{ std::make_pair(name, withMips) };
    auto it{ Instance().textureCache_.find(key) };
    if (it != Instance().textureCache_.end()) {
        return it->second;
    }

    auto texture{ gfx::TEXTURE_ADD(gfx::Texture::FromFile(name, vk::ImageUsageFlagBits::eSampled, withMips)) };
    texture->SetFileName(name.filename().string());

    DEBUG_ATTACH(texture, "TextureFile {}", name.string());

    Instance().textureCache_.insert(std::make_pair(key, texture));
    return texture;
}

gfx::TextureRef AssetService::LoadCubeTexture(const std::filesystem::path& name) {
    auto it{ Instance().textureCubeCache_.find(name) };
    if (it != Instance().textureCubeCache_.end()) {
        return it->second;
    }

    auto texture{ gfx::TEXTURE_ADD(gfx::Texture::FromCubeMapFile(name, vk::ImageUsageFlagBits::eSampled)) };
    texture->SetFileName(name.filename().string());

    DEBUG_ATTACH(texture, "TextureCubeFile {}", name.string());

    Instance().textureCubeCache_.insert(std::make_pair(name, texture));
    return texture;
}

gfx::TextureRef AssetService::LoadCubeTexture(const std::vector<std::filesystem::path>& names) {
    std::stringstream str;
    for (const auto& p : names) {
        str << p;
    }
    auto name{ str.str() };

    auto it{ Instance().textureCubeCache_.find(name) };
    if (it != Instance().textureCubeCache_.end()) {
        return it->second;
    }

    auto texture{ gfx::TEXTURE_ADD(gfx::Texture::FromCubeMapFiles(names, vk::ImageUsageFlagBits::eSampled)) };
    texture->SetFileName(names[0].filename().string());

    DEBUG_ATTACH(texture, "TextureCubeFiles {}", name);

    Instance().textureCubeCache_.insert(std::make_pair(name, texture));
    return texture;
}

//std::shared_ptr<gfx::MeshData> AssetService::LoadSkinnedMesh(const std::filesystem::path& name, bool pbr) {
//    return Instance().skinnedMeshCache_.Get(name, pbr);
//}
//
//std::shared_ptr<std::vector<gfx::MeshData>> AssetService::LoadMesh(const std::filesystem::path& name, bool pbr) {
//    return Instance().meshCache_.Get(name, pbr);
//}
//
//std::shared_ptr<gfx::AnimationData> AssetService::LoadAnimation(const std::filesystem::path& name) {
//    return Instance().animationCache_.Get(name);
//}

AssetService::AssetService()
//, skinnedMeshCache_([](const std::filesystem::path& file, bool pbr) {
//    ugine::gfx::MeshLoader loader;
//    return loader.LoadMesh(gfx::GraphicsService::Graphics(), file, pbr);
//})
//, meshCache_([](const std::filesystem::path& file, bool pbr) {
//    ugine::gfx::MeshLoader loader;
//    return loader.LoadMesh(gfx::GraphicsService::Graphics(), file, pbr);
//})
//, animationCache_([](const std::filesystem::path& file) {
//    ugine::gfx::MeshLoader loader;
//    return std::make_shared<gfx::AnimationData>(loader.LoadAnimation(file));
//})
{}

} // namespace ugine::assets
