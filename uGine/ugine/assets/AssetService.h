﻿#pragma once

#include <ugine/gfx/Graphics.h>
#include <ugine/gfx/Storages.h>
//#include "../graphics/data/MeshData.h"

#include <ugine/utils/Cache.h>
#include <ugine/utils/Singleton.h>

#include <filesystem>
#include <memory>

namespace ugine::assets {

void SortCubeMapFiles(std::vector<std::filesystem::path>& files);

class AssetService : public utils::Singleton<AssetService> {
public:
    static AssetService& Instance();

    static void Init();
    static void Destroy();

    static gfx::TextureRef LoadTexture(const std::filesystem::path& name, bool withMips = false);

    static gfx::TextureRef LoadCubeTexture(const std::filesystem::path& name);
    static gfx::TextureRef LoadCubeTexture(const std::vector<std::filesystem::path>& name);

    //static std::shared_ptr<gfx::MeshData> LoadSkinnedMesh(const std::filesystem::path& name, bool pbr = false);

    //static std::shared_ptr<std::vector<gfx::MeshData>> LoadMesh(const std::filesystem::path& name, bool pbr = false);

    //static std::shared_ptr<graphics::AnimationData> LoadAnimation(const std::filesystem::path& name);

private:
    AssetService();

    // TODO: Owner
    std::map<std::pair<std::filesystem::path, bool>, gfx::TextureRef> textureCache_;
    std::map<std::filesystem::path, gfx::TextureRef> textureCubeCache_;

    //utils::Cache<gfx::MeshData, std::filesystem::path, bool> skinnedMeshCache_;
    //utils::Cache<gfx::AnimationData, std::filesystem::path> animationCache_;
    //utils::Cache<std::vector<graphics::MeshData>, std::filesystem::path, bool> meshCache_;
};

} // namespace ugine::assets
