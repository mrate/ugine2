﻿#include "Presenter.h"

#include "Error.h"
#include "Graphics.h"
#include "GraphicsService.h"
#include "VulkanApi.h"

#include <ugine/utils/Assert.h>
#include <ugine/utils/Profile.h>

namespace ugine::gfx {

Presenter::Presenter(bool srgb)
    : srgb_(srgb) {

    CreateSyncs();
}

Presenter::~Presenter() {
    DestroySwapChain();
}

bool Presenter::RecreateSwapchain(vk::UniqueSurfaceKHR surface, uint32_t width, uint32_t height) {
    PROFILE_EVENT();

    if (surface_) {
        GraphicsService::Graphics().WaitIdle();

        DestroySwapChain();

        if (!GraphicsService::Graphics().IsSutableSurface(*surface)) {
            return false;
        }
    }

    surface_ = std::move(surface);

    CreateSwapChain(width, height);

    return true;
}

bool Presenter::AcquireNextImage(vk::Semaphore semaphore, uint32_t& imageIndex) {
    PROFILE_EVENT();

    try {
        if (!swapChain_) {
            return false;
        }

        auto result{ GraphicsService::Graphics().Device().acquireNextImageKHR(*swapChain_, UINT64_MAX, semaphore, {}) };
        if (result.result != vk::Result::eSuccess) {
            return false;
        }

        imageIndex = result.value;
    } catch (const vk::OutOfDateKHRError&) {
        return false;
    }

    return true;
}

bool Presenter::Present(vk::Semaphore signalSemaphore, uint32_t imageIndex) {
    PROFILE_EVENT();

    if (!swapChain_) {
        return false;
    }

    vk::PresentInfoKHR presentInfo{};
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = &signalSemaphore;
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = &swapChain_.get();
    presentInfo.pImageIndices = &imageIndex;
    presentInfo.pResults = nullptr;

    try {
        auto result{ GraphicsService::Graphics().PresentQueue().presentKHR(presentInfo) };

        currentFrame_ = (currentFrame_ + 1) % MAX_FRAMES_IN_FLIGHT;

        if (result != vk::Result::eSuccess) {
            return false;
        }
    } catch (vk::OutOfDateKHRError&) {
        return false;
    }
    return true;
}

void Presenter::CreateSwapChain(uint32_t width, uint32_t height) {
    auto& gfx{ GraphicsService::Graphics() };

    auto swapChainSupport{ VulkanApi::QuerySwapChainSupport(gfx.PhysicalDevice(), *surface_) };
    auto surfaceFormat{ VulkanApi::ChooseSwapSurfaceFormat(swapChainSupport.formats, srgb_) };
    auto presentMode{ VulkanApi::ChooseSwapPresentMode(swapChainSupport.presentModes) };
    auto extent{ VulkanApi::ChooseSwapExtent(swapChainSupport.capabilities, width, height) };

    uint32_t imageCount{ swapChainSupport.capabilities.minImageCount + 1 };
    if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount) {
        imageCount = swapChainSupport.capabilities.maxImageCount;
    }

    vk::SwapchainCreateInfoKHR swapChainCI({}, *surface_, imageCount);
    swapChainCI.surface = *surface_;
    swapChainCI.minImageCount = imageCount;
    swapChainCI.imageFormat = surfaceFormat.format;
    swapChainCI.imageColorSpace = surfaceFormat.colorSpace;
    swapChainCI.imageExtent = extent;
    swapChainCI.imageArrayLayers = 1;
    swapChainCI.imageUsage = vk::ImageUsageFlagBits::eColorAttachment;

    if (gfx.Queues().graphics != gfx.Queues().surfacePresent) {
        uint32_t queueFamilyIndices[] = { gfx.Queues().graphics, gfx.Queues().surfacePresent };

        swapChainCI.imageSharingMode = vk::SharingMode::eConcurrent;
        swapChainCI.queueFamilyIndexCount = 2;
        swapChainCI.pQueueFamilyIndices = queueFamilyIndices;
    } else {
        swapChainCI.imageSharingMode = vk::SharingMode::eExclusive;
    }

    swapChainCI.preTransform = swapChainSupport.capabilities.currentTransform;
    swapChainCI.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
    swapChainCI.presentMode = presentMode;
    swapChainCI.clipped = VK_TRUE;

    swapChain_ = gfx.Device().createSwapchainKHRUnique(swapChainCI);
    swapChainImages_ = gfx.Device().getSwapchainImagesKHR(*swapChain_);

    swapChainImageFormat_ = surfaceFormat.format;
    swapChainExtent_ = extent;

    for (const auto& image : swapChainImages_) {
        vk::ImageViewCreateInfo imageViewCI;
        imageViewCI.image = image;
        imageViewCI.format = swapChainImageFormat_;
        imageViewCI.viewType = vk::ImageViewType::e2D;
        imageViewCI.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
        imageViewCI.subresourceRange.baseMipLevel = 0;
        imageViewCI.subresourceRange.levelCount = 1;
        imageViewCI.subresourceRange.baseArrayLayer = 0;
        imageViewCI.subresourceRange.layerCount = 1;

        swapChainViews_.push_back(gfx.Device().createImageViewUnique(imageViewCI));
    }

    imagesInFlight_.resize(SwapChainImageCount(), {});
}

void Presenter::DestroySwapChain() {
    swapChainViews_.clear();
    swapChainImages_.clear();

    swapChain_.reset();
}

bool Presenter::BeginFrame() {
    auto& gfx{ GraphicsService::Graphics() };

    PROFILE_EVENT();

    {
        PROFILE_EVENT("BeginFrame::WairForFences");

        auto result{ gfx.Device().waitForFences(*inFlightFences_[currentFrame_], VK_TRUE, UINT64_MAX) };
        if (result != vk::Result::eSuccess) {
            UGINE_THROW(GfxError, "Failed to wait on fences.", result);
        }
    }

    {
        PROFILE_EVENT("BeginFrame::AcquireNextImage");
        if (!AcquireNextImage(*imageAvailableSemaphores_[currentFrame_], frameIndex_)) {
            //Destroy();
            return false;
        }
    }

    if (imagesInFlight_[frameIndex_] != VK_NULL_HANDLE) {
        PROFILE_EVENT("BeginFrame::WaitForFences2");

        auto result{ gfx.Device().waitForFences(imagesInFlight_[frameIndex_], VK_TRUE, UINT64_MAX) };
        if (result != vk::Result::eSuccess) {
            UGINE_THROW(GfxError, "Failed to wait on fences.", result);
        }
    }
    imagesInFlight_[frameIndex_] = *inFlightFences_[currentFrame_];

    return true;
}

void Presenter::Present() {
    Present(*renderFinishedSemaphores_[currentFrame_], frameIndex_);
}

void Presenter::Present(vk::CommandBuffer command, const utils::Vector<vk::Semaphore>& waitSemaphores) {
    PROFILE_EVENT();

    utils::Vector<vk::Semaphore> toWait;
    toWait.push_back(WaitSemaphore());
    std::copy(waitSemaphores.begin(), waitSemaphores.end(), std::back_inserter(toWait));

    GraphicsService::Graphics().SubmitGfxCommand(command, toWait, { SignalSemaphore() }, Fence());
    Present();
}

vk::Semaphore Presenter::WaitSemaphore() const {
    return *imageAvailableSemaphores_[currentFrame_];
}

vk::Semaphore Presenter::SignalSemaphore() const {
    return *renderFinishedSemaphores_[currentFrame_];
}

vk::Fence Presenter::Fence() const {
    return *inFlightFences_[currentFrame_];
}

void Presenter::CreateSyncs() {
    auto& gfx{ GraphicsService::Graphics() };

    // Sync.
    for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i) {
        imageAvailableSemaphores_.push_back(gfx.Device().createSemaphoreUnique({}));
        renderFinishedSemaphores_.push_back(gfx.Device().createSemaphoreUnique({}));
        //computeFinishedSemaphores_.push_back(gfx.Device().createSemaphoreUnique({}));

        vk::FenceCreateInfo fenceCI{};
        fenceCI.flags = vk::FenceCreateFlagBits::eSignaled;
        inFlightFences_.push_back(gfx.Device().createFenceUnique(fenceCI));
    }

    //hasCompute_ = std::vector<bool>(MAX_FRAMES_IN_FLIGHT, false);
}

} // namespace ugine::gfx
