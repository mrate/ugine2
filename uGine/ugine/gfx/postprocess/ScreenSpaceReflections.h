﻿#pragma once

#include "BasicPostprocess.h"

#include "../vulkan/ShaderModule.h"
#include "../vulkan/TypedBuffer.h"

namespace ugine::graphics {

class ScreenSpaceReflections : public BasicPostprocess {
public:
    ScreenSpaceReflections();
    ~ScreenSpaceReflections();

    void SetViewMatrix(const glm::mat4& view);
    void SetProjectionMatrix(const glm::mat4& projection);

    float Step() const {
        return params_.step;
    }
    float MinRayStep() const {
        return params_.minRayStep;
    }

    float MaxSteps() const {
        return params_.maxSteps;
    }

    int NumBinarySearchSteps() const {
        return params_.numBinarySearchSteps;
    }

    float ReflectionSpecFallof() const {
        return params_.reflectionSpecFallof;
    }

    void SetStep(float val) {
        params_.step = val;
    }

    void SetMinRayStep(float val) {
        params_.minRayStep = val;
    }

    void SetMaxSteps(float val) {
        params_.maxSteps = val;
    }

    void SetNumBinarySearchSteps(int val) {
        params_.numBinarySearchSteps = val;
    }

    void SetReflectionSpecFallof(float val) {
        params_.reflectionSpecFallof = val;
    }

protected:
    void UpdateBuffers(uint32_t index) override;
    void DescriptorBindings(std::vector<vk::DescriptorSetLayoutBinding>& bindings) const override;
    void DescriptorSizes(std::vector<vk::DescriptorPoolSize>& sizes, uint32_t count) const override;
    void UpdateDescriptors(uint32_t index, std::vector<vk::WriteDescriptorSet>& descriptorWrite) const override;
    vulkan::SharedShaderModule CreateFragmentShader() const override;

private:
    struct alignas(16) Params {
        glm::mat4 invView;
        glm::mat4 projection;
        glm::mat4 invprojection;
        glm::mat4 view;
        float step{ 0.1f };
        float minRayStep{ 0.1f };
        float maxSteps{ 30.0f };
        int numBinarySearchSteps{ 5 };
        float reflectionSpecFallof{ 3.0f };
    };

    Params params_;

    std::vector<vulkan::TypedBuffer<Params>> ubo_;
};

} // namespace ugine::graphics
