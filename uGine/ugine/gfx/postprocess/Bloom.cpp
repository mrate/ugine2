﻿#include "Bloom.h"

#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/data/PipelineCache.h>
#include <ugine/gfx/data/SamplerCache.h>
#include <ugine/gfx/tools/Initializers.h>
#include <ugine/utils/Profile.h>

namespace ugine::gfx {

void Bloom::Init(uint32_t width, uint32_t height) {
    InitStage(threshold_, L"assets/materials/fx/threshold.mat", float(width), float(height), ThresholdDownsample, &thresholdParams_, sizeof(ThresholdParams),
        "BloomThreshold RT");
    InitStage(blurV_, L"assets/materials/fx/blur.mat", float(width), float(height), BlurDownsample, &blurParamsV_, sizeof(BlurParams), "BloomBlurV RT");
    InitStage(blurH_, L"assets/materials/fx/blur.mat", float(width), float(height), BlurDownsample, &blurParamsH_, sizeof(BlurParams), "BloomBlurH RT");
    InitStage(bloom_, L"assets/materials/fx/bloom.mat", float(width), float(height), 1.0f, &bloomParams_, sizeof(BloomParams), "Boom RT", true);

    ClearColor(clear_, Color{});

    blurParamsV_.horizontal = 0;
    blurParamsH_.horizontal = 1;

    {
        auto& pass{ blurV_.material->Pass(RenderPassType::Postprocess) };
        pass.UpdateUniform(1, 0, blurV_.params, blurV_.size);
    }
    {
        auto& pass{ blurH_.material->Pass(RenderPassType::Postprocess) };
        pass.UpdateUniform(1, 0, blurH_.params, blurH_.size);
    }
}

void Bloom::Resize(uint32_t width, uint32_t height) {
    ResizeStage(threshold_, float(width), float(height));
    ResizeStage(blurV_, float(width), float(height));
    ResizeStage(blurH_, float(width), float(height));
}

void Bloom::Render(vk::RenderPass renderPass, RenderContext& context, const TextureViewRef& input, const gfx::RenderTarget& output) {
    PROFILE_EVENT("Bloom");
    GPUDebugLabel label(context.command, "Bloom");

    UpdateThresholdParams(context, input);
    RenderStage(threshold_, renderPass, context, threshold_.renderTarget, "Threshold");

    auto blurIn{ threshold_.renderTarget };
    for (int i = 0; i < blurCount_; ++i) {
        UpdateBlurParams(context, blurV_, blurIn.ViewRef());
        RenderStage(blurV_, renderPass, context, blurV_.renderTarget, "BlurV");

        UpdateBlurParams(context, blurH_, blurV_.renderTarget.ViewRef());
        RenderStage(blurH_, renderPass, context, blurH_.renderTarget, "BlurH");

        blurIn = blurH_.renderTarget;
    }

    UpdateBloomParams(context, input, blurH_.renderTarget.ViewRef());
    RenderStage(bloom_, renderPass, context, output, "Bloom");
}

void Bloom::UpdateThresholdParams(RenderContext& context, const TextureViewRef& input) {
    auto& pass{ threshold_.material->Pass(RenderPassType::Postprocess) };
    pass.UpdateTexture(0, input);
    pass.UpdateUniform(1, 0, threshold_.params, threshold_.size);
}

void Bloom::UpdateBlurParams(RenderContext& context, Stage& stage, const TextureViewRef& input) {
    auto& pass{ stage.material->Pass(RenderPassType::Postprocess) };
    pass.UpdateTexture(0, input);
    pass.UpdateUniform(1, 0, stage.params, stage.size);
}

void Bloom::UpdateBloomParams(RenderContext& context, const TextureViewRef& input1, const TextureViewRef& input2) {
    auto& pass{ bloom_.material->Pass(RenderPassType::Postprocess) };
    pass.UpdateTexture(0, input1);
    pass.UpdateTexture(1, input2);
    pass.UpdateUniform(2, 0, bloom_.params, bloom_.size);
}

} // namespace ugine::gfx
