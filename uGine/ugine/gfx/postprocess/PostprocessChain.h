﻿#pragma once

#include <ugine/gfx/Gfx.h>

#include "Postprocess.h"

#include <vector>

namespace ugine::gfx {

class PostprocessChain {
public:
    PostprocessChain();

    void Init(uint32_t width, uint32_t height, const std::vector<std::shared_ptr<Postprocess>>& postprocesses);
    void Resize(uint32_t width, uint32_t height);

    size_t Size() const {
        return psChain_.size();
    }

    bool Empty() const {
        return psChain_.empty();
    }

    void Render(RenderContext& context, const TextureViewRef& input, const RenderTarget& output);

    const RenderTarget& Input() const;
    TextureViewRef ResultView() const;

    const std::vector<std::shared_ptr<Postprocess>>& Postprocesses() const {
        return psChain_;
    }

    uint32_t Width() const {
        return width_;
    }

    uint32_t Height() const {
        return height_;
    }

private:
    RenderTarget CreateRenderTarget(uint32_t width, uint32_t height) const;

    std::vector<std::shared_ptr<Postprocess>> psChain_;
    std::array<RenderTarget, 2> renderTargets_;

    uint32_t width_{};
    uint32_t height_{};
};

} // namespace ugine::gfx
