﻿#include "DepthOfField.h"

#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/data/PipelineCache.h>
#include <ugine/gfx/data/SamplerCache.h>
#include <ugine/gfx/tools/Initializers.h>
#include <ugine/utils/Profile.h>

namespace ugine::gfx {

void DepthOfField::Init(uint32_t width, uint32_t height) {
    ClearColor(clear_, Color{});

    InitStage(dilate_, L"assets/materials/fx/dilate.mat", float(width), float(height), 1, &dilateParams_, sizeof(DilationParams), "DofDialte RT");
    InitStage(dof_, L"assets/materials/fx/dof.mat", float(width), float(height), 1, &dofParams_, sizeof(DofParams), "Dof RT");
}

void DepthOfField::Resize(uint32_t width, uint32_t height) {
    ResizeStage(dilate_, float(width), float(height));
    ResizeStage(dof_, float(width), float(height));
}

void DepthOfField::Render(vk::RenderPass renderPass, RenderContext& context, const TextureViewRef& input, const gfx::RenderTarget& output) {
    PROFILE_EVENT("DepthOfField");
    GPUDebugLabel label(context.command, "DepthOfField");

    auto& device{ GraphicsService::Device() };

    {
        auto& pass{ dilate_.material->Pass(RenderPassType::Postprocess) };
        pass.UpdateTexture(0, input);
        pass.UpdateUniform(1, 0, dilate_.params, dilate_.size);

        RenderStage(dilate_, renderPass, context, dilate_.renderTarget, "Dilate");
    }

    {
        auto& pass{ dof_.material->Pass(RenderPassType::Postprocess) };
        pass.UpdateTexture(0, input);
        pass.UpdateTexture(1, dilate_.renderTarget.ViewRef());
        pass.UpdateUniform(2, 0, dof_.params, dof_.size);

        RenderStage(dof_, renderPass, context, output, "DoF");
    }
}

} // namespace ugine::gfx
