﻿#include "SSAO.h"

#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/data/SamplerCache.h>
#include <ugine/utils/Profile.h>

#include <random>

namespace ugine::gfx {

void SSAO::GenerateRandomKernel(glm::vec4* array, uint32_t kernelSize) {
    std::uniform_real_distribution<float> randomFloats(0.0, 1.0); // random floats between [0.0, 1.0]
    std::default_random_engine generator;

    for (uint32_t i = 0; i < kernelSize; ++i) {
        glm::vec3 sample{ //
            randomFloats(generator) * 2.0 - 1.0, //
            randomFloats(generator) * 2.0 - 1.0, //
            -randomFloats(generator)
        };
        sample = glm::normalize(sample);
        sample *= randomFloats(generator);

        auto scale = float(i) / float(kernelSize);
        scale = glm::mix(0.1f, 1.0f, scale * scale);
        sample *= scale;
        array[i] = glm::vec4{ sample, 0.0f };
    }
}

SSAO::SSAO() {}

SSAO::~SSAO() {}

void SSAO::Init(uint32_t /*width*/, uint32_t /*height*/) {
    material_ = MaterialService::InstantiateByPath(L"assets/materials/fx/ssao.mat");

    GenerateKernels();

    material_->SetTexture("randomVectors", randomVecTexture_);
}

void SSAO::Resize(uint32_t /*width*/, uint32_t /*height*/) {}

void SSAO::Update(GPUCommandList command, const TextureViewRef& input) {
    PROFILE_EVENT("SSAO Update");

    auto& pass{ material_->Pass(RenderPassType::Postprocess) };
    pass.UpdateTexture(0, input);

    //if (dirty_) {
    //pass.UpdateUniform(1, 0, &params_, sizeof(Params));
    //}
}

void SSAO::Bind(RenderContext& context) {
    // TODO:
    auto allocation{ GraphicsService::Device().AllocateGPU(context.command, sizeof(Params)) };
    memcpy(allocation.memory, &params_, sizeof(Params));
    GraphicsService::Device().BindUniform(context.command, MATERIAL_DATASET, 1, allocation.buffer, allocation.offset, allocation.size);
}

void SSAO::GenerateKernels() {

    GenerateRandomKernel(params_.kernel, limits::SSAO_KERNEL_SIZE);

    std::uniform_real_distribution<float> randomFloats(0.0, 1.0); // random floats between [0.0, 1.0]
    std::default_random_engine generator;
    std::vector<glm::vec2> ssaoNoise;
    for (unsigned int i = 0; i < 16; i++) {
        glm::vec2 noise{ randomFloats(generator) * 2.0 - 1.0, randomFloats(generator) * 2.0 - 1.0 };
        ssaoNoise.push_back(noise);
    }

    auto texture{ TEXTURE_ADD(
        Texture::FromData(ssaoNoise.data(), sizeof(ssaoNoise[0]) * ssaoNoise.size(), vk::Extent3D{ 4, 4, 1 }, vk::Format::eR16G16Sfloat)) };
    randomVecTexture_ = TEXTURE_VIEW_ADD(texture, TextureView::CreateSampled(*texture, SamplerCache::Sampler(SamplerCache::Type::NearestRepeat)));
    randomVecTexture_->SetName("SSAO random vectors");
}

} // namespace ugine::gfx
