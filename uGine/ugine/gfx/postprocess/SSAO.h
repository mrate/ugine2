﻿#pragma once

#include "Postprocess.h"

#include <ugine/core/Transformation.h>
#include <ugine/gfx/rendering/MaterialInstance.h>

#include <glm/glm.hpp>

namespace ugine::gfx {

class SSAO : public SinglePassPostprocess {
public:
    static void GenerateRandomKernel(glm::vec4* array, uint32_t kernelSize);

    SSAO();
    ~SSAO();

    const char* Name() const override {
        return "SSAO";
    }

    void Init(uint32_t width, uint32_t height) override;
    void Resize(uint32_t width, uint32_t height) override;

    void Update(GPUCommandList command, const TextureViewRef& input) override;

    void SetStrength(float v) {
        params_.strength = v;
    }

    float Strength() const {
        return params_.strength;
    }

    void SetRadius(float v) {
        params_.radius = v;
    }

    float Radius() const {
        return params_.radius;
    }

    void SetDebug(bool v) {
        params_.debug = v ? 1 : 0;
    }

    bool Debug() const {
        return params_.debug > 0;
    }

protected:
    const MaterialInstanceRef& Material() const {
        return material_;
    }

    void Bind(RenderContext& context) override;

private:
    struct Params {
        int debug{ 0 };
        float strength{ 1.0f };
        float radius{ 0.5f };
        float __padding;
        // vec4 for padding.
        glm::vec4 kernel[limits::SSAO_KERNEL_SIZE];
    };

    void GenerateKernels();

    Params params_;
    MaterialInstanceRef material_;
    TextureViewRef randomVecTexture_;
};

} // namespace ugine::gfx
