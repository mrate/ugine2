﻿#include "Postprocess.h"

#include <ugine/gfx/Color.h>
#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/data/PipelineCache.h>
#include <ugine/gfx/data/ShaderBindings.h>
#include <ugine/gfx/tools/Initializers.h>
#include <ugine/utils/Profile.h>

namespace ugine::gfx {

void SinglePassPostprocess::Render(vk::RenderPass renderPass, RenderContext& context, const TextureViewRef& input, const gfx::RenderTarget& output) {
    PROFILE_EVENT("SinglePassRender");
    GPUDebugLabel label(context.command, Name());

    vk::ClearValue clear;
    ClearColor(clear, Color{});

    Update(context.command, input);

    context.SetRenderTarget(0, output.View());
    context.BeginRenderPass(renderPass, clear);

    auto pipeline{ PipelineCache::Instance().GetPipeline(context, *Material()) };
    GraphicsService::Device().BindPipeline(context.command, pipeline);
    Material()->Bind(RenderPassType::Postprocess, context.command);
    Bind(context);

    context.DrawFullscreen();

    context.EndRenderPass();
    context.ClearRenderTargets();

    GraphicsService::Device().ResetDescriptor(context.command, MATERIAL_DATASET);
}

} // namespace ugine::gfx
