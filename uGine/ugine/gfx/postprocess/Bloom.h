﻿#pragma once

#include "MultiStagePostprocess.h"

#include <ugine/gfx/rendering/MaterialInstance.h>

namespace ugine::gfx {

class Bloom : public MultiStagePostprocess {
public:
    static inline const float ThresholdDownsample{ 0.25f };
    static inline const float BlurDownsample{ 0.0625f };

    Bloom() = default;

    void Init(uint32_t width, uint32_t height) override;
    void Resize(uint32_t width, uint32_t height) override;

    void Render(vk::RenderPass renderPass, RenderContext& context, const TextureViewRef& input, const gfx::RenderTarget& output) override;

    float Threshold() const {
        return thresholdParams_.threshold;
    }

    void SetThreshold(float threshold) {
        thresholdParams_.threshold = threshold;
    }

    float ThresholdScale() const {
        return thresholdParams_.scale;
    }

    void SetThresholdScale(float v) {
        thresholdParams_.scale = v;
    }

    float Amount() const {
        return bloomParams_.amount;
    }

    void SetAmount(float amount) {
        bloomParams_.amount = amount;
    }

    float Sigma() const {
        //return blurParamsH_.sigma;
        return 0.0f;
    }

    void SetSigma(float v) {
        //blurParamsH_.sigma = v;
        //blurParamsV_.sigma = v;
    }

    int Size() const {
        //return blurParamsH_.size;
        return 0;
    }

    void SetSize(int size) {
        //blurParamsH_.size = size;
        //blurParamsV_.size = size;
    }

    void SetBlurCount(int v) {
        blurCount_ = v;
    }

    int BlurCount() const {
        return blurCount_;
    }

    float Debug() const {
        return bloomParams_.debug;
    }

    void SetDebug(float amount) {
        bloomParams_.debug = amount;
    }

private:
    struct ThresholdParams {
        float threshold{ 1.0f };
        float scale{ 1.0f };
    };

    struct BlurParams {
        int horizontal{ 0 };
    };

    struct BlurParams2 {
        glm::vec2 texOffset{ 1.0f, 1.0f };
        int size{ 5 };
        int horizontal{ 0 };
        float sigma{ 3.5f };
    };

    struct BloomParams {
        float amount{ 0.3f };
        float debug{ 0.0f };
    };

    void UpdateThresholdParams(RenderContext& context, const TextureViewRef& input);
    void UpdateBlurParams(RenderContext& context, Stage& stage, const TextureViewRef& input);
    void UpdateBloomParams(RenderContext& context, const TextureViewRef& input1, const TextureViewRef& input2);

    ThresholdParams thresholdParams_;
    BlurParams blurParamsV_;
    BlurParams blurParamsH_;
    BloomParams bloomParams_;

    Stage threshold_;
    Stage blurV_;
    Stage blurH_;
    Stage bloom_;

    int blurCount_{ 2 };
};

} // namespace ugine::gfx
