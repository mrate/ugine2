﻿#include "ScreenSpaceReflections.h"

#include "../GraphicsService.h"

#include "../vulkan/Functions.h"
#include "../vulkan/Presenter.h"

#include "../../system/AssetService.h"

namespace ugine::graphics {

ScreenSpaceReflections::ScreenSpaceReflections() {
    const auto count{ GraphicsService::Presenter().SwapChainImageCount() };
    for (uint32_t i = 0; i < count; ++i) {
        vulkan::TypedBuffer<Params> buffer{ GraphicsService::Graphics() };
        buffer.Map(GraphicsService::Graphics());
        ubo_.push_back(std::move(buffer));
    }
}

ScreenSpaceReflections::~ScreenSpaceReflections() {
    for (auto& ubo : ubo_) {
        ubo.Unmap(GraphicsService::Graphics());
    }
}

void ScreenSpaceReflections::UpdateBuffers(uint32_t index) {
    ubo_[index].Update(GraphicsService::Graphics(), params_);
}

void ScreenSpaceReflections::SetViewMatrix(const glm::mat4& view) {
    params_.view = view;
    params_.invView = glm::inverse(view);
}

void ScreenSpaceReflections::SetProjectionMatrix(const glm::mat4& projection) {
    params_.projection = projection;
    params_.invprojection = glm::inverse(projection);
}

void ScreenSpaceReflections::UpdateDescriptors(uint32_t index, std::vector<vk::WriteDescriptorSet>& descriptorWrite) const {
    vk::DescriptorBufferInfo bufferInfo{ ubo_[index].Descriptor() };
    descriptorWrite.push_back(vulkan::DescriptorBuffer(Descriptor(index), vk::DescriptorType::eUniformBuffer, &bufferInfo, 1));

    FinishDescriptorSets(descriptorWrite);
}

vulkan::SharedShaderModule ScreenSpaceReflections::CreateFragmentShader() const {
    return system::AssetService::LoadShader(vk::ShaderStageFlagBits::eFragment, "assets/shaders/fx/ssr.frag", "main");
}

void ScreenSpaceReflections::DescriptorBindings(std::vector<vk::DescriptorSetLayoutBinding>& bindings) const {
    bindings.push_back(vulkan::Binding(vk::ShaderStageFlagBits::eFragment, vk::DescriptorType::eUniformBuffer, 1));
}

void ScreenSpaceReflections::DescriptorSizes(std::vector<vk::DescriptorPoolSize>& sizes, uint32_t count) const {
    sizes.push_back(vk::DescriptorPoolSize(vk::DescriptorType::eUniformBuffer, count));
}

} // namespace ugine::graphics
