﻿#pragma once

#include "Postprocess.h"

#include <ugine/core/Transformation.h>
#include <ugine/gfx/rendering/MaterialInstance.h>

#include <glm/glm.hpp>

namespace ugine::gfx {

class Fog : public SinglePassPostprocess {
public:
    Fog();
    ~Fog();

    const char* Name() const override {
        return "Fog";
    }

    void Init(uint32_t width, uint32_t height) override;
    void Resize(uint32_t width, uint32_t height) override;

    void Update(GPUCommandList command, const TextureViewRef& input) override;

    bool Enabled() const {
        return params_.enabled > 0;
    }

    void SetEnabled(bool enabled) {
        params_.enabled = enabled ? 1 : 0;
    }

    void SetColor(const glm::vec4& color) {
        params_.color = color;
    }

    const glm::vec4& Color() const {
        return params_.color;
    }

    void SetSunColor(const glm::vec4& color) {
        params_.sunColor = color;
    }

    const glm::vec4& SunColor() const {
        return params_.sunColor;
    }

    void SetNearFar(const glm::vec2& nearFar) {
        params_.nearFar = nearFar;
    }

    const glm::vec2& NearFar() const {
        return params_.nearFar;
    }

    void SetMinMax(const glm::vec2& minMax) {
        params_.minMax = minMax;
    }

    const glm::vec2& MinMax() const {
        return params_.minMax;
    }

protected:
    const MaterialInstanceRef& Material() const {
        return material_;
    }

private:
    struct Params {
        glm::vec4 color{ 0.2f, 0.4f, 0.6f, 1.0f };
        glm::vec4 sunColor{ 0.8f, 0.7f, 0.2f, 1.0f };
        glm::vec2 nearFar{ 0.0f, 10.0f };
        glm::vec2 minMax{ 0.0f, 0.97f };
        int enabled{ 0 };
    };

    Params params_;
    MaterialInstanceRef material_;
};

} // namespace ugine::gfx
