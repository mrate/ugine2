#pragma once

#include "Postprocess.h"

namespace ugine::gfx {

class MultiStagePostprocess : public Postprocess {
protected:
    struct Stage {
        MaterialInstanceRef material;
        RenderTarget renderTarget;
        const void* params{};
        size_t size{};
        std::string name;
        float scale{ 1.0f };
        bool isLast{ false };
    };

    void ResizeStage(Stage& stage, float width, float height) const;
    void InitStage(Stage& stage, const std::filesystem::path& materialFile, float width, float height, float scale, const void* params, size_t paramSize,
        const std::string_view& name, bool isLast = false) const;

    void RenderStage(Stage& stage, vk::RenderPass renderPass, RenderContext& context, const gfx::RenderTarget& output, const char* name) const;

    vk::ClearValue clear_;
};

} // namespace ugine::gfx