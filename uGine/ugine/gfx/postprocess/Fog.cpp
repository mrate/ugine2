﻿#include "Fog.h"

#include <ugine/gfx/MaterialService.h>
#include <ugine/utils/Profile.h>

namespace ugine::gfx {

Fog::Fog() {}

Fog::~Fog() {}

void Fog::Init(uint32_t /*width*/, uint32_t /*height*/) {
    material_ = MaterialService::InstantiateByPath(L"assets/materials/fx/fog.mat");
}

void Fog::Resize(uint32_t /*width*/, uint32_t /*height*/) {}

void Fog::Update(GPUCommandList command, const TextureViewRef& input) {
    PROFILE_EVENT("FogUpdate");

    auto& pass{ material_->Pass(RenderPassType::Postprocess) };
    pass.UpdateTexture(0, input);

    //if (dirty_) {
    pass.UpdateUniform(1, 0, &params_, sizeof(Params));
    //}
}

} // namespace ugine::gfx
