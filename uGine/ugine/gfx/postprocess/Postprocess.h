﻿#pragma once

#include <ugine/core/Camera.h>
#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/core/Device.h>
#include <ugine/gfx/core/RenderTarget.h>
#include <ugine/gfx/rendering/MaterialInstance.h>

#include <vector>

namespace ugine::gfx {

class PostprocessChain;

class Postprocess {
public:
    virtual ~Postprocess() {}

    virtual void Init(uint32_t width, uint32_t height) = 0;
    virtual void Resize(uint32_t width, uint32_t height) = 0;

    virtual void Render(vk::RenderPass renderPass, RenderContext& context, const TextureViewRef& input, const gfx::RenderTarget& output) = 0;
};

class SinglePassPostprocess : public Postprocess {
public:
    void Render(vk::RenderPass renderPass, RenderContext& context, const TextureViewRef& input, const gfx::RenderTarget& output) override;

    virtual const char* Name() const = 0;
    virtual void Update(GPUCommandList command, const TextureViewRef& input) = 0;

protected:
    virtual void Bind(RenderContext& context) {}
    virtual const MaterialInstanceRef& Material() const = 0;
};

} // namespace ugine::gfx
