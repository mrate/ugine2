#include "MotionBlur.h"

#include <ugine/gfx/MaterialService.h>
#include <ugine/utils/Profile.h>

namespace ugine::gfx {

MotionBlur::MotionBlur() {}

MotionBlur::~MotionBlur() {}

void MotionBlur::Init(uint32_t /*width*/, uint32_t /*height*/) {
    material_ = MaterialService::InstantiateByPath(L"assets/materials/fx/motionBlur.mat");
}

void MotionBlur::Resize(uint32_t /*width*/, uint32_t /*height*/) {}

void MotionBlur::Update(GPUCommandList command, const TextureViewRef& input) {
    PROFILE_EVENT("MotionBlurUpdate");

    auto& pass{ material_->Pass(RenderPassType::Postprocess) };
    pass.UpdateTexture(0, input);

    //if (dirty_) {
    pass.UpdateUniform(1, 0, &params_, sizeof(Params));
    //}
}

} // namespace ugine::gfx
