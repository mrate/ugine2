﻿#include "ToneMapping.h"

#include <ugine/gfx/MaterialService.h>
#include <ugine/utils/Profile.h>

namespace ugine::gfx {

void ToneMapping::Init(uint32_t /*width*/, uint32_t /*height*/) {
    material_ = MaterialService::InstantiateByPath(L"assets/materials/fx/tonemapping.mat");
}

void ToneMapping::Resize(uint32_t /*width*/, uint32_t /*height*/) {}

void ToneMapping::Update(GPUCommandList command, const TextureViewRef& input) {
    PROFILE_EVENT("ToneMappingUpdate");

    auto& pass{ material_->Pass(RenderPassType::Postprocess) };
    pass.UpdateTexture(0, input);

    if (dirty_) {
        pass.UpdateUniform(1, 0, &params_, sizeof(Params));
    }
}

} // namespace ugine::gfx
