﻿#pragma once

#include "Postprocess.h"

#include "../rendering/QuadRenderer.h"

namespace ugine::graphics {

class DepthOfField : public Postprocess {
public:
    void Setup(uint32_t count, vk::DescriptorSetLayout psLayout) override;
    void SetInputs(const std::vector<vk::ImageView>& images) override;
    void Render(vk::CommandBuffer command, uint32_t index, vk::DescriptorSet commonDescriptorSet) override;
    vk::ImageView ResultView(uint32_t index) const override;

    float Distance() const {
        return dofParams_.distance;
    }

    void SetDistance(float distance) {
        dofParams_.distance = distance;
    }

    bool Enabled() const {
        return dofParams_.enabled > 0;
    }

    void SetEnabled(bool enabled) {
        dilationParams_.enabled = dofParams_.enabled = enabled ? 1 : 0;
    }

    float Range() const {
        return dofParams_.range;
    }

    void SetRange(float v) {
        dofParams_.range = v;
    }

    //float Near() const {
    //    return dofParams_.zNear;
    //}

    //void SetNear(float v) {
    //    dofParams_.zNear = v;
    //}

    //float Far() const {
    //    return dofParams_.zFar;
    //}

    //void SetFar(float v) {
    //    dofParams_.zFar = v;
    //}

    int Size() const {
        return dilationParams_.size;
    }

    void SetSize(int v) {
        dilationParams_.size = v;
    }

    float Separation() const {
        return dilationParams_.separation;
    }

    void SetSeparation(float v) {
        dilationParams_.separation = v;
    }

    float MinThreshold() const {
        return dilationParams_.minThreshold;
    }

    void SetMinThreshold(float v) {
        dilationParams_.minThreshold = v;
    }

    float MaxThreshold() const {
        return dilationParams_.maxThreshold;
    }

    void SetMaxThreshold(float v) {
        dilationParams_.maxThreshold = v;
    }

private:
    void CreateDescriptorSetLayout();
    void CreateDescriptorSets(uint32_t count);

    struct Descriptors {
        //std::vector<vk::DescriptorSet> blurVertical;
        //std::vector<vk::DescriptorSet> blurHorizontal;
        std::vector<vk::DescriptorSet> dilation;
        std::vector<vk::DescriptorSet> dof;
    };

    struct DilationParams {
        int enabled{ 0 };
        int size{ 5 };
        float separation{ 0.3f };
        float minThreshold{ 0.25f };
        float maxThreshold{ 0.5f };
    };

    struct BlurParams {
        int horizontal{ 0 };
    };

    struct DofParams {
        int enabled{ 0 };
        float distance{ 5.0f };
        float range{ 5.0f };
    };

    DilationParams dilationParams_{};
    DofParams dofParams_{};

    std::vector<vk::ImageView> inputs_;

    vk::UniqueDescriptorSetLayout singleImageDsLayout_;
    vk::UniqueDescriptorSetLayout doubleImageDsLayout_;

    vk::UniqueDescriptorPool descriptorSetPool_;
    Descriptors descriptors_;

    vk::UniqueSampler sampler_;

    //std::unique_ptr<QuadRenderer> blurVerticalRenderer_;
    //std::unique_ptr<QuadRenderer> blurHorizontalRenderer_;
    std::unique_ptr<QuadRenderer> dilationRenderer_;
    std::unique_ptr<QuadRenderer> dofRenderer_;
};

} // namespace ugine::graphics
