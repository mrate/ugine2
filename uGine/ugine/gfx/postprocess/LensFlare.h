﻿#pragma once

#include "MultiStagePostprocess.h"

#include <ugine/gfx/rendering/MaterialInstance.h>

namespace ugine::gfx {

class LensFlare : public MultiStagePostprocess {
public:
    static inline const float ThresholdDownsample{ 0.25f };

    void Init(uint32_t width, uint32_t height) override;
    void Resize(uint32_t width, uint32_t height) override;

    void Render(vk::RenderPass renderPass, RenderContext& context, const TextureViewRef& input, const gfx::RenderTarget& output) override;

    float Threshold() const {
        return thresholdParams_.threshold;
    }

    void SetThreshold(float threshold) {
        thresholdParams_.threshold = threshold;
    }

    float ThresholdScale() const {
        return thresholdParams_.scale;
    }

    void SetThresholdScale(float v) {
        thresholdParams_.scale = v;
    }

    int Ghost() const {
        return ghostFeatureParams_.ghost;
    }

    void SetGhost(int v) {
        ghostFeatureParams_.ghost = v;
    }

    float GhostDispersal() const {
        return ghostFeatureParams_.ghostDispersal;
    }

    void SetGhostDispersal(float v) {
        ghostFeatureParams_.ghostDispersal = v;
    }

    float GhostDistortion() const {
        return ghostFeatureParams_.distortion;
    }

    void SetGhostDistortion(float v) {
        ghostFeatureParams_.distortion = v;
    }

    float GhostHaloWidth() const {
        return ghostFeatureParams_.haloWidth;
    }

    void SetGhostHaloWidth(float v) {
        ghostFeatureParams_.haloWidth = v;
    }

private:
    struct ThresholdParams {
        float threshold{ 1.234f };
        float scale{ 0.05f };
    };

    struct GhostFeatureParams {
        int ghost{ 3 };
        float ghostDispersal{ 0.494f };
        float distortion{ 1.6f };
        float haloWidth{ 0.494f };
    };

    struct BlurParams {
        int horizontal{ 0 };
    };

    ThresholdParams thresholdParams_;
    GhostFeatureParams ghostFeatureParams_;
    BlurParams blurParamsV_;
    BlurParams blurParamsH_;

    Stage threshold_;
    Stage ghostFeatures_;
    Stage blurV_;
    Stage blurH_;
    Stage lensFlare_;
};

} // namespace ugine::gfx
