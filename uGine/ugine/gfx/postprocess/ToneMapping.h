﻿#pragma once

#include "Postprocess.h"

#include <ugine/gfx/rendering/MaterialInstance.h>

namespace ugine::gfx {

class ToneMapping : public SinglePassPostprocess {
public:
    ToneMapping() = default;

    const char* Name() const override {
        return "ToneMapping";
    }

    void Init(uint32_t width, uint32_t height) override;
    void Resize(uint32_t width, uint32_t height) override;
    void Update(GPUCommandList command, const TextureViewRef& input) override;

    void SetGamma(float gamma) {
        params_.gamma = gamma;
        dirty_ = true;
    }

    float Gamma() const {
        return params_.gamma;
    }

    void SetExposure(float exp) {
        params_.exposure = exp;
        dirty_ = true;
    }

    float Exposure() const {
        return params_.exposure;
    }

    void SetAlgorithm(int alg) {
        params_.algorithm = alg;
        dirty_ = true;
    }

    int Algorithm() const {
        return params_.algorithm;
    }

protected:
    const MaterialInstanceRef& Material() const {
        return material_;
    }

private:
    struct Params {
        float gamma{ 1.0f }; // 2.2 for SRGB.
        float exposure{ 1.0f };
        int algorithm{ 0 };
    };

    bool dirty_{ true };
    Params params_;
    MaterialInstanceRef material_;
};

} // namespace ugine::gfx
