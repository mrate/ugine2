﻿#include "PostprocessChain.h"

#include "Postprocess.h"

#include <ugine/gfx/Color.h>
#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/GraphicsService.h>
#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/core/GpuProfiler.h>
#include <ugine/gfx/data/RenderPassCache.h>
#include <ugine/gfx/data/SamplerCache.h>
#include <ugine/gfx/tools/Initializers.h>
#include <ugine/utils/Profile.h>

namespace ugine::gfx {

PostprocessChain::PostprocessChain() {}

void PostprocessChain::Init(uint32_t width, uint32_t height, const std::vector<std::shared_ptr<Postprocess>>& postprocesses) {
    width_ = width;
    height_ = height;

    psChain_.clear();

    for (auto& fx : postprocesses) {
        fx->Init(width, height);
        psChain_.push_back(fx);
    }

    for (int i = 0; i < 2; ++i) {
        renderTargets_[i] = CreateRenderTarget(width, height);
    }
}

void PostprocessChain::Resize(uint32_t width, uint32_t height) {
    width_ = width;
    height_ = height;

    for (auto& fx : psChain_) {
        fx->Resize(width, height);
    }

    for (int i = 0; i < 2; ++i) {
        renderTargets_[i] = CreateRenderTarget(width, height);
    }
}

void PostprocessChain::Render(RenderContext& context, const TextureViewRef& input, const gfx::RenderTarget& output) {
    PROFILE_EVENT("PostProcess");
    GpuEvent event{ context.command, "Postprocess", QUERY_DEFAULT };

    auto renderPass{ RenderPassCache::Postprocess(config::PostprocessFormat).RenderPass() };

    context.samples = vk::SampleCountFlagBits::e1;
    context.SetViewportAndScrissor(width_, height_);

    UGINE_ASSERT(Textures::Get(TextureViews::TextureID(input.id())).Samples() == vk::SampleCountFlagBits::e1);
    UGINE_ASSERT(output.Texture().Samples() == vk::SampleCountFlagBits::e1);
    UGINE_ASSERT(context.samples == vk::SampleCountFlagBits::e1);

    if (psChain_.size() == 1) {
        psChain_[0]->Render(renderPass, context, input, output);
    } else {
        // TODO: Copy.
        auto in{ input };

        int target{ 1 };
        for (size_t i = 0; i < psChain_.size() - 1; ++i) {
            psChain_[i]->Render(renderPass, context, in, renderTargets_[target]);
            in = renderTargets_[target].ViewRef();
            target = 1 - target;
        }

        psChain_.back()->Render(renderPass, context, in, output);
    }
}

const RenderTarget& PostprocessChain::Input() const {
    return renderTargets_[0];
}

TextureViewRef PostprocessChain::ResultView() const {
    UGINE_ASSERT(!psChain_.empty());
    return renderTargets_.back().ViewRef();
}

RenderTarget PostprocessChain::CreateRenderTarget(uint32_t width, uint32_t height) const {
    auto sampler{ SamplerCache::Sampler(SamplerCache::Type::LinearClampToEdge) };
    RenderTarget rt{ width, height, config::PostprocessFormat, vk::SampleCountFlagBits::e1,
        vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eTransferSrc, vk::ImageAspectFlagBits::eColor,
        sampler };

    rt.SetName(fmt::format("PostprocessRenderTarget[{}x{}]", width, height));
    return rt;
}

} // namespace ugine::gfx
