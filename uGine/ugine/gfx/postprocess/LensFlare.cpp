﻿#include "LensFlare.h"

#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/data/PipelineCache.h>
#include <ugine/gfx/data/SamplerCache.h>
#include <ugine/gfx/tools/Initializers.h>
#include <ugine/utils/Profile.h>

namespace ugine::gfx {

void LensFlare::Init(uint32_t width, uint32_t height) {
    ClearColor(clear_, Color{});

    InitStage(threshold_, L"assets/materials/fx/threshold.mat", float(width), float(height), ThresholdDownsample, &thresholdParams_, sizeof(ThresholdParams),
        "LensFlareThreshold RT");
    InitStage(ghostFeatures_, L"assets/materials/fx/lensFlareFeatures.mat", float(width), float(height), ThresholdDownsample, &ghostFeatureParams_,
        sizeof(GhostFeatureParams), "LensFlareFeatures RT");
    InitStage(
        blurV_, L"assets/materials/fx/blur.mat", float(width), float(height), ThresholdDownsample, &blurParamsV_, sizeof(BlurParams), "LensFlareBlurV RT");
    InitStage(
        blurH_, L"assets/materials/fx/blur.mat", float(width), float(height), ThresholdDownsample, &blurParamsH_, sizeof(BlurParams), "LensFlareBlurH RT");
    InitStage(lensFlare_, L"assets/materials/fx/lensFlare.mat", float(width), float(height), 1, nullptr, 0, "LensFlare RT", true);

    {
        blurParamsV_.horizontal = 0;
        auto& pass{ blurV_.material->Pass(RenderPassType::Postprocess) };
        pass.UpdateUniform(1, 0, blurV_.params, blurV_.size);
    }

    {
        blurParamsH_.horizontal = 1;
        auto& pass{ blurH_.material->Pass(RenderPassType::Postprocess) };
        pass.UpdateUniform(1, 0, blurH_.params, blurH_.size);
    }
}

void LensFlare::Resize(uint32_t width, uint32_t height) {
    ResizeStage(threshold_, float(width), float(height));
    ResizeStage(ghostFeatures_, float(width), float(height));
    ResizeStage(blurV_, float(width), float(height));
    ResizeStage(blurH_, float(width), float(height));
}

void LensFlare::Render(vk::RenderPass renderPass, RenderContext& context, const TextureViewRef& input, const gfx::RenderTarget& output) {
    PROFILE_EVENT("LensFlare");
    GPUDebugLabel label(context.command, "LensFlare");

    auto& device{ GraphicsService::Device() };

    { // Threshold.
        auto& pass{ threshold_.material->Pass(RenderPassType::Postprocess) };
        pass.UpdateTexture(0, input);
        pass.UpdateUniform(1, 0, threshold_.params, threshold_.size);

        RenderStage(threshold_, renderPass, context, threshold_.renderTarget, "Threshold");
    }

    { // Generate features.
        auto& pass{ ghostFeatures_.material->Pass(RenderPassType::Postprocess) };
        pass.UpdateTexture(0, threshold_.renderTarget.ViewRef());
        pass.UpdateUniform(1, 0, ghostFeatures_.params, ghostFeatures_.size);

        RenderStage(ghostFeatures_, renderPass, context, ghostFeatures_.renderTarget, "Features");
    }

    { // Blur vertically.
        auto& pass{ blurV_.material->Pass(RenderPassType::Postprocess) };
        pass.UpdateTexture(0, ghostFeatures_.renderTarget.ViewRef());
        pass.UpdateUniform(1, 0, blurV_.params, blurV_.size);
        RenderStage(blurV_, renderPass, context, blurV_.renderTarget, "BlurV");
    }

    { // Blur horizontally.
        auto& pass{ blurH_.material->Pass(RenderPassType::Postprocess) };
        pass.UpdateTexture(0, blurV_.renderTarget.ViewRef());
        pass.UpdateUniform(1, 0, blurH_.params, blurH_.size);
        RenderStage(blurH_, renderPass, context, blurH_.renderTarget, "BlurH");
    }

    { // Blend.
        auto& pass{ lensFlare_.material->Pass(RenderPassType::Postprocess) };
        pass.UpdateTexture(0, input);
        pass.UpdateTexture(1, blurH_.renderTarget.ViewRef());

        RenderStage(lensFlare_, renderPass, context, output, "LensFlare");
    }
}

} // namespace ugine::gfx
