﻿#pragma once

#include "Postprocess.h"

#include <ugine/core/Transformation.h>
#include <ugine/gfx/rendering/MaterialInstance.h>

#include <glm/glm.hpp>

namespace ugine::gfx {

class MotionBlur : public SinglePassPostprocess {
public:
    MotionBlur();
    ~MotionBlur();

    const char* Name() const override {
        return "Motion Blur";
    }

    void Init(uint32_t width, uint32_t height) override;
    void Resize(uint32_t width, uint32_t height) override;

    void Update(GPUCommandList command, const TextureViewRef& input) override;

    float Strength() const {
        return params_.strength;
    }

    void SetStrength(float v) {
        params_.strength = v;
    }

protected:
    const MaterialInstanceRef& Material() const {
        return material_;
    }

private:
    struct Params {
        float strength{ 1.0f };
    };

    Params params_;
    MaterialInstanceRef material_;
};

} // namespace ugine::gfx
