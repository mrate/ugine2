#include "MultiStagePostprocess.h"

#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/data/PipelineCache.h>
#include <ugine/gfx/data/SamplerCache.h>

namespace ugine::gfx {

void MultiStagePostprocess::ResizeStage(Stage& stage, float width, float height) const {
    auto sampler{ SamplerCache::Sampler(SamplerCache::Type::LinearClampToBorderBlack) };

    width = std::max(16.0f, stage.scale * width);
    height = std::max(16.0f, stage.scale * height);

    stage.renderTarget = RenderTarget(static_cast<uint32_t>(width), static_cast<uint32_t>(height), config::PostprocessFormat, vk::SampleCountFlagBits::e1,
        vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eColorAttachment, vk::ImageAspectFlagBits::eColor, sampler);
    stage.renderTarget.SetName(stage.name);
}

void MultiStagePostprocess::InitStage(Stage& stage, const std::filesystem::path& materialFile, float width, float height, float scale, const void* params,
    size_t paramSize, const std::string_view& name, bool isLast) const {
    stage.material = MaterialService::InstantiateByPath(materialFile);
    stage.params = params;
    stage.size = paramSize;
    stage.name = name;
    stage.scale = scale;
    stage.isLast = isLast;

    if (!stage.isLast) {
        ResizeStage(stage, width, height);
    }
}

void MultiStagePostprocess::RenderStage(
    Stage& stage, vk::RenderPass renderPass, RenderContext& context, const gfx::RenderTarget& output, const char* name) const {
    GPUDebugLabel label(context.command, name);

    context.SetViewportAndScrissor(output.Extent().width, output.Extent().height);
    context.SetRenderTarget(0, output.View());
    context.BeginRenderPass(renderPass, clear_);

    auto pipeline{ PipelineCache::Instance().GetPipeline(context, *stage.material) };

    GraphicsService::Device().BindPipeline(context.command, pipeline);
    stage.material->Bind(RenderPassType::Postprocess, context.command);
    context.DrawFullscreen();

    context.EndRenderPass();
    context.ClearRenderTargets();

    GraphicsService::Device().ResetDescriptor(context.command, MATERIAL_DATASET);
}

} // namespace ugine::gfx