﻿#pragma once

#include "MultiStagePostprocess.h"

namespace ugine::gfx {

class DepthOfField : public MultiStagePostprocess {
public:
    static inline const float ThresholdDownsample{ 1.0f };
    static inline const float BlurDownsample{ 0.0625f };

    void Init(uint32_t width, uint32_t height) override;
    void Resize(uint32_t width, uint32_t height) override;

    void Render(vk::RenderPass renderPass, RenderContext& context, const TextureViewRef& input, const gfx::RenderTarget& output) override;

    float Distance() const {
        return dofParams_.distance;
    }

    void SetDistance(float distance) {
        dofParams_.distance = distance;
    }

    bool Enabled() const {
        return dofParams_.enabled > 0;
    }

    void SetEnabled(bool enabled) {
        dilateParams_.enabled = dofParams_.enabled = enabled ? 1 : 0;
    }

    float Range() const {
        return dofParams_.range;
    }

    void SetRange(float v) {
        dofParams_.range = v;
    }

    int Size() const {
        return dilateParams_.size;
    }

    void SetSize(int v) {
        dilateParams_.size = v;
    }

    float Separation() const {
        return dilateParams_.separation;
    }

    void SetSeparation(float v) {
        dilateParams_.separation = v;
    }

    float MinThreshold() const {
        return dilateParams_.minThreshold;
    }

    void SetMinThreshold(float v) {
        dilateParams_.minThreshold = v;
    }

    float MaxThreshold() const {
        return dilateParams_.maxThreshold;
    }

    void SetMaxThreshold(float v) {
        dilateParams_.maxThreshold = v;
    }

private:
    struct DilationParams {
        int enabled{ 1 };
        int size{ 1 };
        float separation{ 1.0f };
        float minThreshold{ 0.5f };
        float maxThreshold{ 1.0f };
    };

    struct DofParams {
        int enabled{ 1 };
        float distance{ 3.0f };
        float range{ 1.0f };
    };

    Stage dilate_;
    Stage dof_;

    DilationParams dilateParams_;
    DofParams dofParams_;
};

} // namespace ugine::gfx
