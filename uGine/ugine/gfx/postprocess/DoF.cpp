﻿#include "DoF.h"

#include "../../system/AssetService.h"

#include "../GraphicsService.h"

#include "../vulkan/Functions.h"
#include "../vulkan/Presenter.h"

namespace ugine::graphics {

void DepthOfField::Setup(uint32_t count, vk::DescriptorSetLayout psLayout) {
    CreateDescriptorSetLayout();
    CreateDescriptorSets(count);

    auto extent{ GraphicsService::Presenter().SwapChainExtent() };
    vk::Extent2D extentDownscaled{ extent.width / 2, extent.height / 2 };

    auto format{ GraphicsService::FramebufferFormat() };
    std::vector<vk::DescriptorSetLayout> singleImageLayouts = { psLayout, *singleImageDsLayout_ };
    std::vector<vk::DescriptorSetLayout> doubleImageLayouts = { psLayout, *doubleImageDsLayout_ };

    //std::vector<vk::PushConstantRange> blurConstants = { { vk::ShaderStageFlagBits::eFragment, 0, sizeof(BlurParams) } };
    //blurVerticalRenderer_ = std::make_unique<QuadRenderer>(extentDownscaled, format, singleImageLayouts, blurConstants,
    //    system::AssetService::LoadShader(vk::ShaderStageFlagBits::eFragment, "assets/shaders/fx/blur.frag", "main"));

    //blurHorizontalRenderer_ = std::make_unique<QuadRenderer>(extentDownscaled, format, singleImageLayouts, blurConstants,
    //    system::AssetService::LoadShader(vk::ShaderStageFlagBits::eFragment, "assets/shaders/fx/blur.frag", "main"));

    std::vector<vk::PushConstantRange> dilationConstants = { { vk::ShaderStageFlagBits::eFragment, 0, sizeof(DilationParams) } };
    dilationRenderer_ = std::make_unique<QuadRenderer>(extent, format, singleImageLayouts, dilationConstants,
        system::AssetService::LoadShader(vk::ShaderStageFlagBits::eFragment, "assets/shaders/fx/dilation.frag", "main"));

    std::vector<vk::PushConstantRange> bloomConstants = { { vk::ShaderStageFlagBits::eFragment, 0, sizeof(DofParams) } };
    dofRenderer_ = std::make_unique<QuadRenderer>(extent, format, doubleImageLayouts, bloomConstants,
        system::AssetService::LoadShader(vk::ShaderStageFlagBits::eFragment, "assets/shaders/fx/dof.frag", "main"));

    sampler_ = GraphicsService::CreateSampler(1);
}

void DepthOfField::SetInputs(const std::vector<vk::ImageView>& images) {
    inputs_ = images;

    auto& gfx{ GraphicsService::Graphics() };
    for (uint32_t i = 0; i < images.size(); ++i) {
        { // Blur vertical.
            std::vector<vk::WriteDescriptorSet> descriptorWrite;
            vk::DescriptorImageInfo imageInfo{ *sampler_, inputs_[i], vk::ImageLayout::eShaderReadOnlyOptimal };
            descriptorWrite.push_back(vulkan::DescriptorImage(descriptors_.dilation[i], vk::DescriptorType::eCombinedImageSampler, &imageInfo, 0));
            gfx.Device().updateDescriptorSets(descriptorWrite, {});
        }

        //{ // Blur vertical.
        //    std::vector<vk::WriteDescriptorSet> descriptorWrite;
        //    vk::DescriptorImageInfo imageInfo{ GraphicsService::CacheSampler(1), inputs_[i], vk::ImageLayout::eShaderReadOnlyOptimal };
        //    descriptorWrite.push_back(vulkan::DescriptorImage(descriptors_.blurVertical[i], vk::DescriptorType::eCombinedImageSampler, &imageInfo, 0));
        //    gfx.Device().updateDescriptorSets(descriptorWrite, {});
        //}

        //{ // Blur horizontal.
        //    std::vector<vk::WriteDescriptorSet> descriptorWrite;
        //    vk::DescriptorImageInfo imageInfo{ GraphicsService::CacheSampler(1), blurVerticalRenderer_->Result(i), vk::ImageLayout::eShaderReadOnlyOptimal };
        //    descriptorWrite.push_back(vulkan::DescriptorImage(descriptors_.blurHorizontal[i], vk::DescriptorType::eCombinedImageSampler, &imageInfo, 0));
        //    gfx.Device().updateDescriptorSets(descriptorWrite, {});
        //}

        { // Merge.
            std::vector<vk::WriteDescriptorSet> descriptorWrite;
            vk::DescriptorImageInfo input1Info{ *sampler_, inputs_[i], vk::ImageLayout::eShaderReadOnlyOptimal };
            vk::DescriptorImageInfo input2Info{ *sampler_, dilationRenderer_->Result(i), vk::ImageLayout::eShaderReadOnlyOptimal };
            descriptorWrite.push_back(vulkan::DescriptorImage(descriptors_.dof[i], vk::DescriptorType::eCombinedImageSampler, &input1Info, 0));
            descriptorWrite.push_back(vulkan::DescriptorImage(descriptors_.dof[i], vk::DescriptorType::eCombinedImageSampler, &input2Info, 1));
            gfx.Device().updateDescriptorSets(descriptorWrite, {});
        }
    }
}

void DepthOfField::Render(vk::CommandBuffer command, uint32_t index, vk::DescriptorSet commonDescriptorSet) {
    //{ // Vertical blur.
    //    BlurParams blurParams;
    //    blurParams.horizontal = 0;

    //    std::vector<QuadRenderer::PushConstant> constants = { QuadRenderer::PushConstant{ 0, sizeof(BlurParams), &blurParams } };
    //    std::vector<vk::DescriptorSet> descriptors{ commonDescriptorSet, descriptors_.blurVertical[index] };
    //    blurVerticalRenderer_->Render(command, index, descriptors, constants);
    //}

    //{ // Horizontal blur.
    //    BlurParams blurParams;
    //    blurParams.horizontal = 1;

    //    std::vector<QuadRenderer::PushConstant> constants = { QuadRenderer::PushConstant{ 0, sizeof(BlurParams), &blurParams } };
    //    std::vector<vk::DescriptorSet> descriptors{ commonDescriptorSet, descriptors_.blurHorizontal[index] };
    //    blurHorizontalRenderer_->Render(command, index, descriptors, constants);
    //}

    { // Dilation.
        std::vector<QuadRenderer::PushConstant> constants = { QuadRenderer::PushConstant{ 0, sizeof(DilationParams), &dilationParams_ } };
        std::vector<vk::DescriptorSet> descriptors{ commonDescriptorSet, descriptors_.dilation[index] };
        dilationRenderer_->Render(command, index, descriptors, constants);
    }

    { // Bloom.
        std::vector<QuadRenderer::PushConstant> constants = { QuadRenderer::PushConstant{ 0, sizeof(DofParams), &dofParams_ } };
        std::vector<vk::DescriptorSet> descriptors{ commonDescriptorSet, descriptors_.dof[index] };
        dofRenderer_->Render(command, index, descriptors, constants);
    }
}

vk::ImageView DepthOfField::ResultView(uint32_t index) const {
    return dofRenderer_->Result(index);
}

void DepthOfField::CreateDescriptorSetLayout() {
    {
        std::vector<vk::DescriptorSetLayoutBinding> bindings;
        bindings.push_back(vulkan::Binding(vk::ShaderStageFlagBits::eFragment, vk::DescriptorType::eCombinedImageSampler, 0));

        vk::DescriptorSetLayoutCreateInfo layoutInfo{};
        layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
        layoutInfo.pBindings = bindings.data();

        auto& gfx{ GraphicsService::Graphics() };
        singleImageDsLayout_ = gfx.Device().createDescriptorSetLayoutUnique(layoutInfo);
    }

    {
        std::vector<vk::DescriptorSetLayoutBinding> bindings;
        bindings.push_back(vulkan::Binding(vk::ShaderStageFlagBits::eFragment, vk::DescriptorType::eCombinedImageSampler, 0));
        bindings.push_back(vulkan::Binding(vk::ShaderStageFlagBits::eFragment, vk::DescriptorType::eCombinedImageSampler, 1));

        vk::DescriptorSetLayoutCreateInfo layoutInfo{};
        layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
        layoutInfo.pBindings = bindings.data();

        auto& gfx{ GraphicsService::Graphics() };
        doubleImageDsLayout_ = gfx.Device().createDescriptorSetLayoutUnique(layoutInfo);
    }
}

void DepthOfField::CreateDescriptorSets(uint32_t count) {
    auto& gfx{ GraphicsService::Graphics() };

    std::vector<vk::DescriptorPoolSize> sizes{
        vk::DescriptorPoolSize(vk::DescriptorType::eCombinedImageSampler, count * 2),
    };

    vk::DescriptorPoolCreateInfo poolInfo{};
    poolInfo.pPoolSizes = sizes.data();
    poolInfo.poolSizeCount = static_cast<uint32_t>(sizes.size());
    poolInfo.maxSets = count * 4; // Threshold + 2x blur + Bloom.
    descriptorSetPool_ = gfx.Device().createDescriptorPoolUnique(poolInfo);

    {
        std::vector<vk::DescriptorSetLayout> layouts(count, *singleImageDsLayout_);

        vk::DescriptorSetAllocateInfo allocInfo;
        allocInfo.descriptorPool = *descriptorSetPool_;
        allocInfo.descriptorSetCount = count;
        allocInfo.pSetLayouts = layouts.data();

        //descriptors_.blurVertical = gfx.Device().allocateDescriptorSets(allocInfo);
        //descriptors_.blurHorizontal = gfx.Device().allocateDescriptorSets(allocInfo);
        descriptors_.dilation = gfx.Device().allocateDescriptorSets(allocInfo);
    }

    {
        std::vector<vk::DescriptorSetLayout> layouts(count, *doubleImageDsLayout_);

        vk::DescriptorSetAllocateInfo allocInfo;
        allocInfo.descriptorPool = *descriptorSetPool_;
        allocInfo.descriptorSetCount = count;
        allocInfo.pSetLayouts = layouts.data();
        descriptors_.dof = gfx.Device().allocateDescriptorSets(allocInfo);
    }
}

} // namespace ugine::graphics
