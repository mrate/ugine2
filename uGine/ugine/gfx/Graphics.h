﻿#pragma once

#include "VulkanApi.h"

#include <ugine/utils/Vector.h>
#include <ugine/utils/VmaWrapper.h>

namespace ugine::gfx {

class Graphics {
public:
    Graphics(vk::Instance instance, vk::PhysicalDevice physicalDevice, const Queues& queues);
    ~Graphics();

    bool IsSutableSurface(vk::SurfaceKHR surface);

    void SubmitGfxCommand(vk::CommandBuffer command, vk::Fence fence = {});
    void SubmitGfxCommand(vk::CommandBuffer command, vk::Semaphore waitSemaphore, vk::Semaphore signalSemaphore, vk::Fence fence);
    void SubmitGfxCommand(
        vk::CommandBuffer command, const utils::Vector<vk::Semaphore>& waitSemaphore, const utils::Vector<vk::Semaphore>& signalSemaphore, vk::Fence fence);
    void SubmitGfxCommands(const std::vector<vk::CommandBuffer>& commands, const utils::Vector<vk::Semaphore>& waitSemaphore,
        const utils::Vector<vk::Semaphore>& signalSemaphore, vk::Fence fence);

    void SubmitComputeCommand(
        vk::CommandBuffer command, const utils::Vector<vk::Semaphore>& waitSemaphore, const utils::Vector<vk::Semaphore>& signalSemaphore, vk::Fence fence);
    void SubmitComputeCommands(const std::vector<vk::CommandBuffer>& commands, const utils::Vector<vk::Semaphore>& waitSemaphore,
        const utils::Vector<vk::Semaphore>& signalSemaphore, vk::Fence fence);

    uint32_t FindMemoryType(uint32_t typeFilter, vk::MemoryPropertyFlags properties) const;

    void WaitIdle();

    vk::Device Device() const {
        return *device_;
    }

    vk::PhysicalDeviceProperties Properties() const {
        return properties_;
    }

    vk::PhysicalDevice PhysicalDevice() const {
        return physicalDevice_;
    }

    uint32_t GraphicsQueueFamily() const {
        return queues_.graphics;
    }

    uint32_t ComputeQueueFamily() const {
        return queues_.compute;
    }

    vk::Queue GraphicsQueue() {
        return graphicsQueue_;
    }

    vk::Queue ComputeQueue() {
        return computeQueue_;
    }

    vk::Queue PresentQueue() {
        return presentQueue_;
    }

    vk::SampleCountFlagBits AvailableSampleCounts() const {
        return availableSampleCount_;
    }

    vk::Format DepthFormat() const {
        return depthFormat_;
    }

    vk::Format DepthStencilFormat() const {
        return depthStencilFormat_;
    }

    const Queues& Queues() const {
        return queues_;
    }

    vma::Allocator& Allocator() {
        return allocator_;
    }

    vk::Instance Instance() const {
        return instance_;
    }

    bool HasComputeQueue() const {
        return queues_.compute != queues_.graphics;
    }

    vk::DeviceSize StorageBufferOffsetAlignment() const {
        return properties_.limits.minStorageBufferOffsetAlignment;
    }

    vk::DeviceSize UniformBufferOffsetAlignment() const {
        return properties_.limits.minUniformBufferOffsetAlignment;
    }

private:
    void InitCapabilities();
    vk::Format FindSupportedFormat(const std::vector<vk::Format>& candidates, vk::ImageTiling tiling, vk::FormatFeatureFlags features) const;

    vk::Instance instance_;
    vk::PhysicalDevice physicalDevice_;
    vk::UniqueDevice device_;
    gfx::Queues queues_;

    vk::Queue graphicsQueue_;
    vk::Queue computeQueue_;
    vk::Queue presentQueue_;

    // Capabilities.
    vk::Format depthFormat_{};
    vk::Format depthStencilFormat_{};
    vk::SampleCountFlagBits availableSampleCount_{};

    vk::PhysicalDeviceProperties properties_;

    vma::Allocator allocator_;
};

} // namespace ugine::gfx
