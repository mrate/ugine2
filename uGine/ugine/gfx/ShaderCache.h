﻿#pragma once

#include "Gfx.h"

#include <ugine/gfx/tools/SpirvParser.h>
#include <ugine/utils/Singleton.h>

#include <condition_variable>
#include <filesystem>

namespace ugine::gfx {

struct ShaderBinding {
    uint32_t set{};
    uint32_t binding{};
    uint32_t size{};
    uint32_t count{};
    vk::ShaderStageFlags stage{};
    vk::DescriptorType type{};
    vk::ImageViewType imgType{};
};

struct Shader {
    vk::UniqueShaderModule module;
    std::string entryPoint;
    vk::ShaderStageFlagBits stage;
    std::vector<uint8_t> binary;
    std::vector<ShaderBinding> bindings;
    uint32_t inputs{};
    std::vector<GPUPushConstant> pushConstants;

    // TODO:
    ShaderParsedData parsedData;

    vk::PipelineShaderStageCreateInfo CreateInfo() const {
        return vk::PipelineShaderStageCreateInfo{ {}, stage, *module, entryPoint.c_str() };
    }
};

class ShaderProgram {
public:
    void AddShader(const Shader& shader);
    void Link(std::vector<vk::UniqueDescriptorSetLayout>& descriptorSetLayouts);

    const std::vector<ShaderBinding>& Bindings() const {
        return bindings_;
    }

    const std::vector<GPUPushConstant>& PushConstants() const {
        return pushConstants_;
    }

    const std::vector<vk::PipelineShaderStageCreateInfo>& Stages() const {
        return shaderStages_;
    }

    uint32_t Inputs(vk::ShaderStageFlagBits stage) const;

private:
    std::vector<ShaderBinding> bindings_;
    std::vector<GPUPushConstant> pushConstants_;
    std::vector<vk::PipelineShaderStageCreateInfo> shaderStages_;
    std::map<vk::ShaderStageFlagBits, uint32_t> inputs_;
};

class ShaderCache : public utils::Singleton<ShaderCache> {
public:
    static void Destroy();

    static std::set<std::filesystem::path> ShaderIncludes(const std::filesystem::path& file);
    static const Shader& GetShader(vk::ShaderStageFlagBits stage, const std::filesystem::path& file);

    // Debug info.
    inline static std::atomic_ullong WaitTime{};
    inline static std::atomic_ullong CompileTime{};
    inline static std::atomic_ullong ParseTime{};

private:
    using Key = std::pair<vk::ShaderStageFlags, std::filesystem::path>;

    static std::vector<uint8_t> LoadShader(vk::ShaderStageFlags stage, const std::filesystem::path& path, const std::string& entryPoint);
    static void ParseBindings(Shader& shader);

    const Shader& GetShaderImpl(vk::ShaderStageFlagBits stage, const std::filesystem::path& file);

    std::map<Key, Shader> shaders_;
    std::mutex shaderLock_;

    std::condition_variable loadedCV_;
    std::set<Key> loadingShaders_;
};

} // namespace ugine::gfx
