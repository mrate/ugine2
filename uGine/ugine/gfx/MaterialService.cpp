﻿#include "MaterialService.h"

#include <ugine/core/Scheduler.h>
#include <ugine/gfx/rendering/Material.h>
#include <ugine/gfx/rendering/MaterialHelpers.h>
#include <ugine/gfx/rendering/MaterialInstance.h>
#include <ugine/utils/File.h>
#include <ugine/utils/FileWatcher.h>
#include <ugine/utils/Log.h>
#include <ugine/utils/Profile.h>

#include <nlohmann/json.hpp>

#include <algorithm>
#include <fstream>

namespace ugine::gfx {

MaterialService& MaterialService::Instance() {
    static MaterialService i;
    return i;
}

void MaterialService::Init() {
    Instance().fileWatcher_ = std::make_unique<utils::FileWatcher>();

    utils::ScopeTimer timer("Common materials init");

    std::vector<std::filesystem::path> files = {
        L"assets/materials/pbr.mat",
        L"assets/materials/pbrTransparent.mat",
        L"assets/materials/blinnphong.mat",
        L"assets/materials/blinnphongTransparent.mat",
        L"assets/materials/blinnphongDoubleSided.mat",
        L"assets/materials/skybox.mat",
        L"assets/materials/terrain.mat",
        L"assets/materials/flare.mat",
    };

    auto refs{ Register(files) };

    Instance().defaultMaterial_ = refs[0];
    Instance().defaultMaterialTransparent_ = refs[1];
}

void MaterialService::Destroy() {
    Instance().fileWatcher_ = nullptr;
    Instance().defaultMaterial_ = {};
    Instance().defaultMaterialTransparent_ = {};

    Instance().instances_.clear();

    Instance().materials_.clear();
    Instance().materialsByPath_.clear();
}

std::vector<MaterialRef> MaterialService::Register(const std::vector<std::filesystem::path>& materialFiles) {
    std::vector<Material> materials(materialFiles.size());
    std::vector<uint32_t> ids(materialFiles.size());
    std::vector<MaterialRef> materialRefs(materialFiles.size());

    core::Scheduler::Group grp;

    int counter{};
    for (auto& file : materialFiles) {
        auto existingMaterial{ Instance().materialsByPath_.find(file) };

        if (existingMaterial == Instance().materialsByPath_.end()) {
            uint32_t id{ Instance().nextId_++ };
            core::Scheduler::Schedule(grp, [&, id, counter]() { materials[counter] = Material(id, file); });
        } else {
            materialRefs[counter] = existingMaterial->second;
        }
        ++counter;
    }

    core::Scheduler::Wait(grp);

    for (int i = 0; i < counter; ++i) {
        if (!materialRefs[i]) {
            materialRefs[i] = MATERIAL_ADD(std::move(materials[i]));
            Instance().Register(materialRefs[i], materialFiles[i]);
        }
    }

    return materialRefs;
}

std::vector<MaterialInstanceRef> MaterialService::Instantiate(const std::vector<std::filesystem::path>& materialFiles) {
    Register(materialFiles);

    std::vector<MaterialInstance> instances(materialFiles.size());
    std::vector<MaterialRef> materials(materialFiles.size());

    size_t i{};
    for (const auto& file : materialFiles) {
        auto it{ Instance().materialsByPath_.find(file) };
        if (it != Instance().materialsByPath_.end()) {
            materials[i] = it->second;
        }
        ++i;
    }

    core::Scheduler::Group grp;

    for (size_t i = 0; i < materials.size(); ++i) {
        if (materials[i]) {
            core::Scheduler::Schedule(grp, [&, i]() { instances[i] = MaterialInstance(std::move(materials[i])); });
        }
    }

    core::Scheduler::Wait(grp);

    std::vector<MaterialInstanceRef> instanceRefs(materialFiles.size());
    for (size_t i = 0; i < instanceRefs.size(); ++i) {
        instanceRefs[i] = Instantiate(std::move(instances[i]));
    }

    return instanceRefs;
}

void MaterialService::Register(MaterialRef& material, const std::filesystem::path& materialFile) {
    auto inserted{ materialsByPath_.insert(std::make_pair(materialFile, material)).second };
    if (!inserted) {
        UGINE_WARN("Material with name '{}' already registered.", material->Name());
    } else {
        materials_.push_back(material);
        materialsByPath_.insert(std::make_pair(materialFile, material));

        std::set<std::filesystem::path> shadersWithIncludes;
        for (const auto& s : material->Shaders()) {
            shadersWithIncludes.insert(s);
            for (const auto& i : ShaderCache::ShaderIncludes(s)) {
                shadersWithIncludes.insert(i);
            }
        }

        RegisterFileWatch(material.id(), shadersWithIncludes);
    }
}

void MaterialService::RegisterFileWatch(MaterialID materialId, const std::set<std::filesystem::path>& files) {
    std::scoped_lock lock{ updatedFilesMutex_ };
    for (const auto& s : files) {
        auto it{ shaderMaterialMap_.find(s) };
        if (it == shaderMaterialMap_.end()) {
            it = shaderMaterialMap_.insert(std::make_pair(s, std::set<MaterialID>{})).first;
            fileWatcher_->Add(s, [](const std::filesystem::path& path) { Instance().ShaderChanged(path); });
        }

        it->second.insert(materialId);
    }
}

void MaterialService::ShaderChanged(const std::filesystem::path& shader) {
    std::scoped_lock lock{ updatedFilesMutex_ };

    auto it{ shaderMaterialMap_.find(shader) };
    if (it != shaderMaterialMap_.end()) {
        for (auto matID : it->second) {
            updatedMaterials_.insert(matID);
        }
    }
}

std::string MaterialService::GenerateName(const MaterialRef& material) {
    return fmt::format("{} instance #{}", material->Name(), Instance().counter_++);
}

MaterialInstanceRef MaterialService::Instantiate(const MaterialRef& material, const std::string_view& name) {
    auto ref{ MATERIAL_INSTANCE_ADD(material) };
    ref->SetName(name.empty() ? GenerateName(material) : name.data());
    Instance().instances_.push_back(ref);
    return ref;
}

MaterialInstanceRef MaterialService::Instantiate(const MaterialInstance& instance, const std::string_view& name) {
    auto newInstance{ Instantiate(instance.MaterialRef()) };
    TransferMaterialParams(instance, *newInstance);
    if (name.empty()) {
        newInstance->SetName(fmt::format("{} copy #{}", instance.Name(), Instance().counter_++));
    } else {
        newInstance->SetName(name.data());
    }
    return newInstance;
}

MaterialRef MaterialService::Register(const std::filesystem::path& materialFile) {
    UGINE_DEBUG("Loading material: {}", materialFile.string());

    auto it{ Instance().materialsByPath_.find(materialFile) };
    if (it != Instance().materialsByPath_.end()) {
        return it->second;
    }
    auto material{ MATERIAL_ADD(Instance().nextId_++, materialFile) };
    Instance().Register(material, materialFile);
    return material;
}

MaterialRef MaterialService::FindMaterial(const std::string_view& material) {
    auto it{ std::find_if(Instance().materials_.begin(), Instance().materials_.end(), [&material](const auto& mat) { return mat->Name() == material; }) };

    return it == Instance().materials_.end() ? MaterialRef{} : *it;
}

MaterialRef MaterialService::DefaultMaterial(bool transparent) {
    return transparent ? Instance().defaultMaterialTransparent_ : Instance().defaultMaterial_;
}

void MaterialService::Reload(const MaterialRef& material) {
    UGINE_ASSERT(material);

    Reload(material.id());
}

void MaterialService::Reload(MaterialID id) {
    utils::ScopeTimer timer("Load");

    auto& material{ Materials::Get(id) };
    UGINE_DEBUG("Reloading material {}...", material.Name());

    try {
        Material newMaterial{ static_cast<uint32_t>(id), material.Path() };
        Materials::Replace(id, std::move(newMaterial));
    } catch (const std::exception& ex) {
        UGINE_ERROR("Failed to reload material {}: {}", material.Name(), ex.what());
    }
}

bool MaterialService::ReloadChagned() {
    std::set<MaterialID> toReload;
    {
        std::scoped_lock lock{ Instance().updatedFilesMutex_ };
        std::swap(toReload, Instance().updatedMaterials_);
    }

    for (auto matID : toReload) {
        Reload(matID);
    }

    return !toReload.empty();
}

std::vector<MaterialInstanceRef> MaterialService::Instances(const std::string_view& category) {
    std::vector<MaterialInstanceRef> result;
    std::copy_if(Instance().instances_.begin(), Instance().instances_.end(), std::back_inserter(result),
        [&category](const auto& instRef) { return instRef->Material().Category() == category; });
    return result;
}

std::string MaterialService::UniqueName(const std::string& name) {
    // TODO:
    return name + std::string(" ") + std::to_string(Instance().instances_.size());
}

MaterialRef MaterialService::Find(const std::string_view& name) {
    for (auto& ref : Instance().materials_) {
        if (ref->Name() == name) {
            return ref;
        }
    }

    return {};
}

MaterialInstanceRef MaterialService::InstantiateDefault(bool transparent) {
    return transparent ? Instantiate(Instance().defaultMaterialTransparent_) : Instantiate(Instance().defaultMaterial_);
}

MaterialInstanceRef MaterialService::Instantiate(const std::string_view& material, const std::string_view& name) {
    auto mat{ Instance().Find(material.data()) };
    if (!mat) {
        UGINE_THROW(GfxError, "Unknown material");
    }

    return Instantiate(mat, name);
}

MaterialInstanceRef MaterialService::InstantiateByPath(const std::filesystem::path& name) {
    return Instantiate(Register(name));
}

MaterialInstanceRef MaterialService::InstanceFromFile(const std::filesystem::path& file) {
    try {
        using namespace nlohmann;

        std::ifstream in(file);
        if (!in.good()) {
            throw std::exception("File not found");
        }

        json j;
        in >> j;

        auto materialName = j.value<std::string>("material", "");
        if (materialName.empty()) {
            UGINE_THROW(GfxError, "Missing material name.");
        }

        auto material{ MaterialService::Instance().Find(materialName) };
        if (!material) {
            UGINE_THROW(GfxError, "Material not found");
        }

        MaterialParamValues values{};
        ParseParamValues(j["values"], material->DefaultPass().Params(), values);

        for (auto& [name, param] : values.values) {
            if (param.type == MaterialParam::Type::Texture2D || param.type == MaterialParam::Type::Texture3D
                || param.type == MaterialParam::Type::TextureCube) {
                auto textureFile{ entt::any_cast<std::string>(param.texture) };
                if (!textureFile.empty()) {
                    param.texture = utils::MakeRelative(file.parent_path(), textureFile).string();
                }
            }
        }

        auto newInstance{ Instance().Instantiate(material) };
        FillParamsToInstance(values, *newInstance);
        return newInstance;

    } catch (const std::exception& ex) {
        UGINE_ERROR("Failed to load material instance file {}: {}", file.string(), ex.what());
        UGINE_THROW(GfxError, "Failed to load material instance file.");
    }
}

void MaterialService::Remove(const MaterialRef& ref) {
    for (auto [path, mat] : Instance().materialsByPath_) {
        if (mat == ref) {
            Instance().materialsByPath_.erase(path);
            break;
        }
    }

    Instance().materials_.erase(std::remove(Instance().materials_.begin(), Instance().materials_.end(), ref), Instance().materials_.end());
}

void MaterialService::RemoveInstance(const MaterialInstanceRef& ref) {
    Instance().instances_.erase(std::remove(Instance().instances_.begin(), Instance().instances_.end(), ref), Instance().instances_.end());
}

std::vector<MaterialRef> MaterialService::Materials(const std::string_view& category) {
    std::vector<MaterialRef> result;
    std::copy_if(Instance().materials_.begin(), Instance().materials_.end(), std::back_inserter(result),
        [&category](const auto& instRef) { return instRef->Category() == category; });
    return result;
}

} // namespace ugine::gfx
