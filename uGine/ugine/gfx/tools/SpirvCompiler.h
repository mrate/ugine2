﻿#pragma once

#include <vulkan/vulkan.hpp>

#include <filesystem>
#include <memory>
#include <set>
#include <string>

namespace ugine::gfx {

class SpirvCompilerImpl;

class SpirvCompiler {
public:
    SpirvCompiler();
    ~SpirvCompiler();

    SpirvCompiler(SpirvCompiler& other) = delete;
    SpirvCompiler& operator=(SpirvCompiler& other) = delete;

    void SetWarningsAsErrors();

    bool Compile(vk::ShaderStageFlags stage, const std::string& source, const std::string& entryPoint, const std::filesystem::path& fileName,
        bool preprocess = true, std::set<std::filesystem::path>* includes = nullptr);

    const std::string& Error() const;

    const std::vector<uint8_t>& Data() const;

private:
    std::unique_ptr<SpirvCompilerImpl> impl_;
};

} // namespace ugine::gfx
