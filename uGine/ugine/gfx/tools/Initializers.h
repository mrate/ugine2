﻿#pragma once

#include <ugine/gfx/Gfx.h>

namespace ugine::gfx {

struct Color;

vk::DescriptorSetLayoutBinding ShaderLayoutBinding(vk::ShaderStageFlags stage, vk::DescriptorType type, uint32_t binding, uint32_t count = 1);

vk::WriteDescriptorSet DescriptorBuffer(vk::DescriptorSet descriptorSet, vk::DescriptorType type, const vk::DescriptorBufferInfo* info, uint32_t binding,
    uint32_t count = 1, uint32_t arrayElement = 0);

vk::WriteDescriptorSet DescriptorImage(vk::DescriptorSet descriptorSet, vk::DescriptorType type, const vk::DescriptorImageInfo* info, uint32_t binding,
    uint32_t count = 1, uint32_t arrayElement = 0);

const char* ToString(vk::SampleCountFlagBits samples);

void ClearColor(vk::ClearValue& value, float r, float g, float b, float a);
void ClearColor(vk::ClearValue& value, const Color& color);
void ClearDepthStencil(vk::ClearValue& value, float depth, uint32_t stencil);

void EnsureCommand(GPUCommandList& cmd);

} // namespace ugine::gfx
