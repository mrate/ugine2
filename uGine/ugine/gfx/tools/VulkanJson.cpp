﻿#include "VulkanJson.h"

namespace ugine::gfx {

const std::map<std::string, vk::ShaderStageFlagBits> ShaderStages = {
    { "vertex", vk::ShaderStageFlagBits::eVertex },
    { "tessellationControl", vk::ShaderStageFlagBits::eTessellationControl },
    { "tessellationEvaluation", vk::ShaderStageFlagBits::eTessellationEvaluation },
    { "geometry", vk::ShaderStageFlagBits::eGeometry },
    { "fragment", vk::ShaderStageFlagBits::eFragment },
};

void ParsePipelineDesc(const nlohmann::json& pipeline, PipelineDesc& pipelineDesc) {
    if (pipeline.contains("blending")) {
        const auto& blending{ pipeline["blending"] };

        pipelineDesc.blending.enabled = blending.value<bool>("enable", pipelineDesc.blending.enabled);
        pipelineDesc.blending.colorBlendOp = blending.value<vk::BlendOp>("colorBlendOp", pipelineDesc.blending.colorBlendOp);
        pipelineDesc.blending.srcColorBlendFactor = blending.value<vk::BlendFactor>("srcColorBlendFactor", pipelineDesc.blending.srcColorBlendFactor);
        pipelineDesc.blending.dstColorBlendFactor = blending.value<vk::BlendFactor>("dstColorBlendFactor", pipelineDesc.blending.dstColorBlendFactor);
        pipelineDesc.blending.alphaBlendOp = blending.value<vk::BlendOp>("alphaBlendOp", pipelineDesc.blending.alphaBlendOp);
        pipelineDesc.blending.srcAlphaBlendFactor = blending.value<vk::BlendFactor>("srcAlphaBlendFactor", pipelineDesc.blending.srcAlphaBlendFactor);
        pipelineDesc.blending.dstAlphaBlendFactor = blending.value<vk::BlendFactor>("dstAlphaBlendFactor", pipelineDesc.blending.dstAlphaBlendFactor);

        if (blending.contains("colorWriteMask")) {
            auto strVal{ blending.value<std::string>("colorWriteMask", "rgba") };

            pipelineDesc.blending.colorWriteMask = {};
            for (auto& c : strVal) {
                switch (c) {
                case 'r':
                    pipelineDesc.blending.colorWriteMask |= vk::ColorComponentFlagBits::eR;
                    break;
                case 'g':
                    pipelineDesc.blending.colorWriteMask |= vk::ColorComponentFlagBits::eG;
                    break;
                case 'b':
                    pipelineDesc.blending.colorWriteMask |= vk::ColorComponentFlagBits::eB;
                    break;
                case 'a':
                    pipelineDesc.blending.colorWriteMask |= vk::ColorComponentFlagBits::eA;
                    break;
                }
            }
        }
    }

    if (pipeline.contains("depth")) {
        const auto& depth{ pipeline["depth"] };

        pipelineDesc.depth.testEnable = depth.value<bool>("test", pipelineDesc.depth.testEnable);
        pipelineDesc.depth.writeEnable = depth.value<bool>("write", pipelineDesc.depth.writeEnable);
        pipelineDesc.depth.compareOp = depth.value<vk::CompareOp>("compareOp", pipelineDesc.depth.compareOp);
    }

    if (pipeline.contains("stencil")) {
        const auto& stencil{ pipeline["stencil"] };

        pipelineDesc.stencil.testEnable = stencil.value<bool>("test", pipelineDesc.stencil.testEnable);
        pipelineDesc.stencil.compareOp = stencil.value<vk::CompareOp>("compareOp", pipelineDesc.stencil.compareOp);
        pipelineDesc.stencil.depthFailOp = stencil.value<vk::StencilOp>("depthFailOp", pipelineDesc.stencil.depthFailOp);
        pipelineDesc.stencil.failOp = stencil.value<vk::StencilOp>("failOp", pipelineDesc.stencil.failOp);
        pipelineDesc.stencil.passOp = stencil.value<vk::StencilOp>("passOp", pipelineDesc.stencil.passOp);
        pipelineDesc.stencil.compareMask = stencil.value<uint32_t>("compareMask", pipelineDesc.stencil.compareMask);
        pipelineDesc.stencil.writeMask = stencil.value<uint32_t>("writeMask", pipelineDesc.stencil.writeMask);
        pipelineDesc.stencil.reference = stencil.value<uint32_t>("reference", pipelineDesc.stencil.reference);
    }

    pipelineDesc.polygonMode = pipeline.value<vk::PolygonMode>("polygonMode", pipelineDesc.polygonMode);
    pipelineDesc.cullMode = pipeline.value<vk::CullModeFlagBits>("cullMode", vk::CullModeFlagBits::eBack); // TODO: Default value.
    pipelineDesc.topology = pipeline.value<vk::PrimitiveTopology>("topology", pipelineDesc.topology);
}

} // namespace ugine::gfx
