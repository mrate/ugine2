﻿#include "SpirvPreprocessor.h"

#include <ugine/gfx/Limits.h>
#include <ugine/gfx/data/ShaderBindings.h>
#include <ugine/gfx/rendering/LightShadingData.h>
#include <ugine/utils/Assert.h>
#include <ugine/utils/File.h>

#include <regex>
#include <sstream>

namespace ugine::gfx {

void SpirvPreprocessor::IncludePaths(const std::filesystem::path& file, std::vector<std::filesystem::path>& result) {
    const auto directory{ file.parent_path() };
    const auto src{ utils::ReadFile(file) };

    std::regex reInclude{ "^#include \"(.*)\"$" };

    for (std::sregex_iterator i{ src.begin(), src.end(), reInclude }, end{}; i != end; ++i) {
        std::smatch matches{ *i };

        const auto file{ directory / matches.str(1) };
        result.push_back(file);

        IncludePaths(file, result);
    }
}

const std::vector<std::pair<std::string, std::string>>& SpirvPreprocessor::BuiltIns() {
    static std::vector<std::pair<std::string, std::string>> vars{
        { "UGINE_CSM_LEVELS", std::to_string(limits::CSM_LEVELS) },
        { "UGINE_MAX_LIGHTS", std::to_string(limits::MAX_LIGHTS) },
        { "UGINE_MAX_DIR_SHADOWS", std::to_string(limits::MAX_DIR_SHADOWS) },
        { "UGINE_MAX_POINT_SHADOWS", std::to_string(limits::MAX_POINT_SHADOWS) },
        { "UGINE_MAX_SPOT_SHADOWS", std::to_string(limits::MAX_SPOT_SHADOWS) },
        { "UGINE_MAX_SHADOWS", std::to_string(limits::MAX_SHADOWS) },
        { "UGINE_LIGHT_DIR", std::to_string(LightEntry::Type::Directional) },
        { "UGINE_SSAO_KERNEL_SIZE", std::to_string(limits::SSAO_KERNEL_SIZE) },
        { "UGINE_LIGHT_POINT", std::to_string(LightEntry::Type::Point) },
        { "UGINE_LIGHT_SPOT", std::to_string(LightEntry::Type::Spot) },
        { "GLOBAL_SET", std::to_string(GLOBAL_SHADING_DATASET) },
        { "FRAME_SET", std::to_string(FRAME_DATASET) },
        { "CAMERA_SET", std::to_string(CAMERA_DATASET) },
        { "MATERIAL_SET", std::to_string(MATERIAL_DATASET) },
        { "COMPUTE_SET", std::to_string(COMPUTE_DATASET) },
        { "SHADING_BINDING", std::to_string(SHADING_BINDING) },
        { "LIGHTS_BINDING", std::to_string(LIGHTS_BINDING) },
        { "DIR_SHADOW_MAP_BINDING", std::to_string(DIR_SHADOW_MAP_BINDING) },
        { "SPOT_SHADOW_MAP_BINDING", std::to_string(SPOT_SHADOW_MAP_BINDING) },
        { "POINT_SHADOW_MAP_BINDING", std::to_string(POINT_SHADOW_MAP_BINDING) },
        { "BRDF_LUT_BINDING", std::to_string(BRDF_LUT_BINDING) },
        { "IRRADIANCE_BINDING", std::to_string(IRRADIANCE_BINDING) },
        { "PREFILTERED_CUBEMAP_BINDING", std::to_string(PREFILTERED_CUBEMAP_BINDING) },
        { "DEPTH_MAP_BINDING", std::to_string(DEPTH_MAP_BINDING) },
        { "AO_BINDING", std::to_string(AO_BINDING) },
        { "SCENE_BINDING", std::to_string(SCENE_BINDING) },
        { "FRAME_BINDING", std::to_string(FRAME_BINDING) },
        { "CAMERA_BINDING", std::to_string(CAMERA_BINDING) },
        { "POSTPROCESS_THREAD_COUNT", std::to_string(POSTPROCESS_THREAD_COUNT) },
        { "UGINE_OUTLINE_STENCIL", std::to_string(OUTLINE_STENCIL) },
        { "UGINE_OPAQUE_STENCIL", std::to_string(OPAQUE_STENCIL) },
    };
    return vars;
}

bool SpirvPreprocessor::Process(const std::filesystem::path& file, std::string& src, std::set<std::filesystem::path>* includes) {
    if (!HandleIncludes(file, src, includes)) {
        return false;
    }

    return true;
}

bool SpirvPreprocessor::HandleIncludes(const std::filesystem::path& file, std::string& src, std::set<std::filesystem::path>* includes) {
    const auto directory{ file.parent_path() };

    std::regex reInclude{ "^#include \"(.*)\"$" };

    std::sregex_iterator begin(src.begin(), src.end(), reInclude);
    std::sregex_iterator end;

    uint64_t pos{};
    std::stringstream out;
    for (auto i = begin; i != end; ++i) {
        std::smatch matches{ *i };

        out << src.substr(pos, matches.position(0) - pos);

        auto file{ directory / matches.str(1) };
        if (includes) {
            includes->insert(file);
        }

        if (!std::filesystem::exists(file)) {
            error_ = "Failed to open file '" + file.string() + "'";
            return false;
        }
        IncludeFile(out, file, includes);

        pos = matches.position(0) + matches.length(0);
    }

    if (pos > 0) {
        out << src.substr(pos, src.size() - pos);
        src = out.str();
    }

    return true;
}

void SpirvPreprocessor::IncludeFile(std::ostream& out, const std::filesystem::path& path, std::set<std::filesystem::path>* includes) {
    auto fileContent{ utils::ReadFile(path) };

    Process(path, fileContent, includes);

    out << fileContent;
}

} // namespace ugine::gfx
