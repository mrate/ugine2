﻿#include "Equirect.h"

#include <ugine/assets/AssetService.h>
#include <ugine/gfx/Graphics.h>
#include <ugine/gfx/GraphicsService.h>
#include <ugine/gfx/ShaderCache.h>
#include <ugine/gfx/tools/Initializers.h>

namespace ugine::gfx {

Equirect::Equirect() {
    CreateEqui2Cube();
}

TextureRef Equirect::LoadHdrCubeMap(const std::filesystem::path& path, vk::Extent2D size, vk::ImageUsageFlags usage) {
    // Src texture.
    auto srcTexture{ Texture::FromFile(path, vk::ImageUsageFlagBits::eSampled, false, vk::ImageLayout::eShaderReadOnlyOptimal) };

    // Dst texture.
    vk::ImageCreateInfo imageCI{};
    imageCI.imageType = vk::ImageType::e2D;
    imageCI.extent = vk::Extent3D{ size.width, size.height, 1 };
    imageCI.mipLevels = 2;
    imageCI.arrayLayers = 6;
    imageCI.format = vk::Format::eR32G32B32A32Sfloat;
    imageCI.tiling = vk::ImageTiling::eOptimal;
    imageCI.initialLayout = vk::ImageLayout::eUndefined;
    imageCI.usage = usage | vk::ImageUsageFlagBits::eStorage | vk::ImageUsageFlagBits::eTransferSrc | vk::ImageUsageFlagBits::eTransferDst;
    imageCI.samples = vk::SampleCountFlagBits::e1;
    imageCI.sharingMode = vk::SharingMode::eExclusive;
    imageCI.flags = vk::ImageCreateFlagBits::eCubeCompatible;

    auto dstTexture{ TEXTURE_ADD(imageCI) };
    dstTexture->SetFileName(path.filename().string());

    {
        //auto command{ GraphicsService::Device().BeginCommandList() };
        //dstTexture->Transition(command, vk::ImageLayout::eUndefined, vk::ImageLayout::eGeneral);
        //GraphicsService::Device().SubmitCommandLists();
    }

    // Prepare buffer.
    auto srcView{ TextureView::CreateSampled(srcTexture, vk::ImageViewType::e2D, vk::ImageAspectFlagBits::eColor) };
    auto dstView{ TextureView::CreateSampled(*dstTexture, vk::ImageViewType::e2DArray, vk::ImageAspectFlagBits::eColor, 0, 6) };

    // TODO: Verify.

    auto& device{ GraphicsService::Device() };

    //vk::DescriptorImageInfo srcDesc{ srcView.Descriptor() };
    //vk::DescriptorImageInfo dstDesc{ dstView.Descriptor() };

    //std::vector<vk::WriteDescriptorSet> descriptorWrites = {
    //    DescriptorImage(descriptors_[0], vk::DescriptorType::eCombinedImageSampler, &srcDesc, 0),
    //    DescriptorImage(descriptors_[0], vk::DescriptorType::eStorageImage, &dstDesc, 1),
    //};

    //GraphicsService::Graphics().Device().updateDescriptorSets(descriptorWrites, {});

    // Run compute.
    {
        auto command{ device.BeginCommandList(Device::CommandType::Compute) };
        device.BindImageSampler(command, 0, 0, srcView.Descriptor());
        device.BindImageSampler(command, 0, 1, dstView.Descriptor());
        device.BindPipeline(command, &pipeline_);
        device.CommandBuffer(command).bindDescriptorSets(
            vk::PipelineBindPoint::eCompute, pipeline_.Layout(), 0, static_cast<uint32_t>(descriptors_.size()), descriptors_.data(), 0, 0);
        device.Dispatch(command, 1 + size.width / 32, 1 + size.height / 32, 6); // TODO: Ceil sizes.
        dstTexture->GenerateMips(command, vk::ImageLayout::eUndefined, vk::ImageLayout::eShaderReadOnlyOptimal);
        device.SubmitCommandLists();
    }

    return dstTexture;
}

void Equirect::CreateEqui2Cube() {
    std::vector<vk::DescriptorSetLayoutBinding> bindings = {
        ShaderLayoutBinding(vk::ShaderStageFlagBits::eCompute, vk::DescriptorType::eCombinedImageSampler, 0),
        ShaderLayoutBinding(vk::ShaderStageFlagBits::eCompute, vk::DescriptorType::eStorageImage, 1),
    };

    // Layout.
    vk::DescriptorSetLayoutCreateInfo descriptorSetLayoutCI;
    descriptorSetLayoutCI.bindingCount = static_cast<uint32_t>(bindings.size());
    descriptorSetLayoutCI.pBindings = bindings.data();
    descriptorSetLayout_ = GraphicsService::Graphics().Device().createDescriptorSetLayoutUnique(descriptorSetLayoutCI);

    // Descriptors.
    vk::DescriptorPoolSize poolSize(vk::DescriptorType::eStorageImage, 2);

    vk::DescriptorPoolCreateInfo poolInfo{};
    poolInfo.pPoolSizes = &poolSize;
    poolInfo.poolSizeCount = 1;
    poolInfo.maxSets = 1;

    pool_ = GraphicsService::Graphics().Device().createDescriptorPoolUnique(poolInfo);

    vk::DescriptorSetAllocateInfo allocInfo;
    allocInfo.descriptorPool = *pool_;
    allocInfo.descriptorSetCount = 1;
    allocInfo.pSetLayouts = &(*descriptorSetLayout_);

    descriptors_ = GraphicsService::Graphics().Device().allocateDescriptorSets(allocInfo);

    // CS.
    const auto& shader{ ShaderCache::GetShader(vk::ShaderStageFlagBits::eCompute, L"assets/shaders/compute/equirect2cube.comp") };

    std::vector<vk::DescriptorSetLayout> layouts{ *descriptorSetLayout_ };
    pipeline_ = ComputePipeline(layouts, shader);
}

} // namespace ugine::gfx
