﻿#include "Initializers.h"

#include <ugine/gfx/Color.h>

namespace ugine::gfx {

vk::DescriptorSetLayoutBinding ShaderLayoutBinding(vk::ShaderStageFlags stage, vk::DescriptorType type, uint32_t binding, uint32_t count) {
    vk::DescriptorSetLayoutBinding dsBinding{};
    dsBinding.stageFlags = stage;
    dsBinding.descriptorType = type;
    dsBinding.binding = binding;
    dsBinding.descriptorCount = count;
    return dsBinding;
}

vk::WriteDescriptorSet DescriptorBuffer(
    vk::DescriptorSet descriptorSet, vk::DescriptorType type, const vk::DescriptorBufferInfo* info, uint32_t binding, uint32_t count, uint32_t arrayElement) {
    vk::WriteDescriptorSet descriptorWrite{};

    descriptorWrite.dstSet = descriptorSet;
    descriptorWrite.dstBinding = binding;
    descriptorWrite.dstArrayElement = arrayElement;
    descriptorWrite.descriptorType = type;
    descriptorWrite.descriptorCount = count;
    descriptorWrite.pBufferInfo = info;

    return descriptorWrite;
}

vk::WriteDescriptorSet DescriptorBuffer(vk::DescriptorSet descriptorSet, vk::DescriptorType type, const std::vector<vk::DescriptorBufferInfo>& info,
    uint32_t binding, uint32_t count, uint32_t arrayElement) {
    vk::WriteDescriptorSet descriptorWrite{};

    descriptorWrite.dstSet = descriptorSet;
    descriptorWrite.dstBinding = binding;
    descriptorWrite.dstArrayElement = arrayElement;
    descriptorWrite.descriptorType = type;
    descriptorWrite.descriptorCount = count;
    descriptorWrite.pBufferInfo = info.data();

    return descriptorWrite;
}

vk::WriteDescriptorSet DescriptorImage(
    vk::DescriptorSet descriptorSet, vk::DescriptorType type, const vk::DescriptorImageInfo* info, uint32_t binding, uint32_t count, uint32_t arrayElement) {
    vk::WriteDescriptorSet descriptorWrite{};

    descriptorWrite.dstSet = descriptorSet;
    descriptorWrite.dstBinding = binding;
    descriptorWrite.dstArrayElement = arrayElement;
    descriptorWrite.descriptorType = type;
    descriptorWrite.descriptorCount = count;
    descriptorWrite.pImageInfo = info;

    return descriptorWrite;
}

vk::WriteDescriptorSet DescriptorImage(vk::DescriptorSet descriptorSet, vk::DescriptorType type, const std::vector<vk::DescriptorImageInfo>& info,
    uint32_t binding, uint32_t arrayElement, size_t size) {
    vk::WriteDescriptorSet descriptorWrite{};

    descriptorWrite.dstSet = descriptorSet;
    descriptorWrite.dstBinding = binding;
    descriptorWrite.dstArrayElement = arrayElement;
    descriptorWrite.descriptorType = type;
    descriptorWrite.descriptorCount = static_cast<uint32_t>(size > 0 ? size : info.size());
    descriptorWrite.pImageInfo = info.data();

    return descriptorWrite;
}

const char* ToString(vk::SampleCountFlagBits samples) {
    switch (samples) {
    case vk::SampleCountFlagBits::e1:
        return "1x";
    case vk::SampleCountFlagBits::e2:
        return "2x";
    case vk::SampleCountFlagBits::e4:
        return "4x";
    case vk::SampleCountFlagBits::e8:
        return "8x";
    case vk::SampleCountFlagBits::e16:
        return "16x";
    case vk::SampleCountFlagBits::e32:
        return "32x";
    case vk::SampleCountFlagBits::e64:
        return "64x";
    default:
        UGINE_ASSERT(false);
        return "???";
    }
}

void ClearColor(vk::ClearValue& value, float r, float g, float b, float a) {
    value.color = vk::ClearColorValue(std::array<float, 4>{ r, g, b, a });
}

void ClearDepthStencil(vk::ClearValue& value, float depth, uint32_t stencil) {
    value.depthStencil = vk::ClearDepthStencilValue(depth, stencil);
}

void EnsureCommand(GPUCommandList& cmd) {
    if (cmd == gfx::NullCommandList) {
        cmd = gfx::GraphicsService::Device().BeginCommandList();
    }
}

void ClearColor(vk::ClearValue& value, const Color& color) {
    value.color = vk::ClearColorValue(std::array<float, 4>{ color.r(), color.g(), color.b(), color.a() });
}

} // namespace ugine::gfx
