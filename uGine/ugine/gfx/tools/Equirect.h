﻿#pragma once

#include <ugine/gfx/Storages.h>
#include <ugine/gfx/core/ComputePipeline.h>

namespace ugine::gfx {

class Equirect {
public:
    Equirect();

    TextureRef LoadHdrCubeMap(const std::filesystem::path& path, vk::Extent2D size, vk::ImageUsageFlags usage);

private:
    void CreateEqui2Cube();

    vk::UniqueDescriptorPool pool_;
    vk::UniqueDescriptorSetLayout descriptorSetLayout_;
    std::vector<vk::DescriptorSet> descriptors_;

    ComputePipeline pipeline_;
};

} // namespace ugine::gfx
