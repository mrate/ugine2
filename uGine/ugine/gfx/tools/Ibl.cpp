﻿#include "Ibl.h"

#include <ugine/assets/AssetService.h>
#include <ugine/gfx/ShaderCache.h>
#include <ugine/gfx/core/Framebuffer.h>
#include <ugine/gfx/core/Vertex.h>
#include <ugine/gfx/core/VertexBuffer.h>
#include <ugine/gfx/data/SamplerCache.h>
#include <ugine/gfx/data/Shapes.h>
#include <ugine/gfx/tools/Initializers.h>
#include <ugine/utils/StbImageWrapper.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

namespace {

const std::vector<glm::mat4> CubeMapProjViewMat = {
    // POSITIVE_X
    glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
    // NEGATIVE_X
    glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
    // POSITIVE_Y
    glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
    // NEGATIVE_Y
    glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
    // POSITIVE_Z
    glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
    // NEGATIVE_Z
    glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f)),
};
}

namespace ugine::gfx {

Ibl::Ibl() {
    CreateBrdfLut();
    CreateCommonDescriptorSetLayout();
    CreateIrradianceMap();
    CreatePrefilteredCube();
}

TextureRef Ibl::GenerateBrdfLut() {
    // Generate a BRDF integration map used as a look-up-table (stores roughness / NdotV)
    const vk::Extent2D extent{ 512, 512 };

    auto& gfx{ GraphicsService::Graphics() };
    auto& device{ GraphicsService::Device() };

    auto texture{ TEXTURE_ADD(
        vk::Extent3D{ extent, 1 }, BrdfLut::Format, vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eSampled) };

    auto view{ TextureView::CreateSampled(*texture, SamplerCache::Sampler(SamplerCache::Type::LinearClampToEdge)) };
    auto pView{ view.View() };

    vk::FramebufferCreateInfo framebufferCI{};
    framebufferCI.renderPass = *brdfLut_.renderPass;
    framebufferCI.attachmentCount = 1;
    framebufferCI.pAttachments = &pView;
    framebufferCI.width = extent.width;
    framebufferCI.height = extent.height;
    framebufferCI.layers = 1;

    auto framebuffer{ gfx.Device().createFramebufferUnique(framebufferCI) };

    ScopedDescriptor descriptorSet{ *brdfLut_.pool };

    // Render
    std::array<vk::ClearValue, 1> clearValues;
    clearValues[0].color = vk::ClearColorValue(std::array<float, 4>{ 0.0f, 0.0f, 0.0f, 1.0f });

    auto command{ device.BeginCommandList() };

    vk::Rect2D area{ vk::Offset2D{ 0, 0 }, extent };
    vk::RenderPassBeginInfo renderPassInfo(*brdfLut_.renderPass, *framebuffer, area, static_cast<uint32_t>(clearValues.size()), clearValues.data());
    vk::Viewport viewport{ 0.0f, 0.0f, static_cast<float>(extent.width), static_cast<float>(extent.height), 0.0f, 1.0f };
    vk::Rect2D scissor{ vk::Offset2D{ 0, 0 }, extent };

    device.CommandBuffer(command).beginRenderPass(&renderPassInfo, vk::SubpassContents::eInline);
    device.CommandBuffer(command).setViewport(0, viewport);
    device.CommandBuffer(command).setScissor(0, scissor);
    device.CommandBuffer(command).bindPipeline(vk::PipelineBindPoint::eGraphics, *brdfLut_.pipeline);
    device.CommandBuffer(command).draw(3, 1, 0, 0);
    device.CommandBuffer(command).endRenderPass();

    device.SubmitCommandLists();
    device.WaitGpu();

    return texture;
}

TextureRef Ibl::GenerateIrradianceMap(const TextureViewRef& sourceTexture) {
    // TODO: Cleanup.

    const vk::Extent2D extent{ 64, 64 };
    const auto numMips{ static_cast<uint32_t>(floor(log2(extent.width))) + 1 };

    // Pre-filtered cube map
    // Image
    auto& gfx{ GraphicsService::Graphics() };
    auto& device{ GraphicsService::Device() };

    auto irradianceView{ CreateCubeTexture(extent, irradianceMap_.Format, numMips, "Irradiance") };
    irradianceView->SetSampler(SamplerCache::Sampler(SamplerCache::Type::LinearClampToEdge, numMips));

    // Framebuffer
    FramebufferAttachments attachments;
    attachments.CreateAttachment(vk::Extent3D{ extent, 1 }, IrradianceMap::Format, vk::ImageAspectFlagBits::eColor, vk::SampleCountFlagBits::e1,
        vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eTransferSrc);

    Framebuffer fb{ *irradianceMap_.renderPass, extent, std::move(attachments) };

    vk::FramebufferCreateInfo fbufCreateInfo{};
    fbufCreateInfo.renderPass = *irradianceMap_.renderPass;
    fbufCreateInfo.attachmentCount = static_cast<uint32_t>(fb.Attachments().Attachments().size());
    fbufCreateInfo.pAttachments = fb.Attachments().Attachments().data();
    fbufCreateInfo.width = extent.width;
    fbufCreateInfo.height = extent.height;
    fbufCreateInfo.layers = 1;
    auto offscreenFb = gfx.Device().createFramebufferUnique(fbufCreateInfo);

    // Descriptor sets
    ScopedDescriptor descriptorSet{ *cisPool_ };

    auto imageInfo{ sourceTexture->Descriptor() };
    auto writeDescriptor{ DescriptorImage(descriptorSet.Descriptor(), vk::DescriptorType::eCombinedImageSampler, &imageInfo, 0) };
    gfx.Device().updateDescriptorSets(writeDescriptor, {});

    // Render
    std::array<vk::ClearValue, 1> clearValues;
    clearValues[0].color = vk::ClearColorValue(std::array<float, 4>{ 0.0f, 0.0f, 0.2f, 0.0f });

    vk::Rect2D area{ vk::Offset2D{ 0, 0 }, extent };
    vk::RenderPassBeginInfo renderPassInfo(*irradianceMap_.renderPass, *offscreenFb, area, static_cast<uint32_t>(clearValues.size()), clearValues.data());
    vk::Viewport viewport{ 0.0f, 0.0f, static_cast<float>(extent.width), static_cast<float>(extent.height), 0.0f, 1.0f };
    vk::Rect2D scissor{ vk::Offset2D{ 0, 0 }, extent };

    auto command{ device.BeginCommandList() };

    //fb.Attachments().Texture(0).Transition(command, vk::ImageLayout::eColorAttachmentOptimal);

    device.SetScissor(command, scissor);

    // Change image layout for all cubemap faces to transfer destination
    auto irradianceTexture{ TextureViews::TextureRef(irradianceView) };
    irradianceTexture->Transition(command, vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal);

    IrradianceMap::PushBlock pushBlock{};
    pushBlock.deltaPhi = (2.0f * glm::pi<float>()) / 180.0f;
    pushBlock.deltaTheta = (0.5f * glm::pi<float>()) / 64.0f;

    GraphicsService::VertexBuffer()->Bind(command);

    for (uint32_t m = 0; m < numMips; m++) {
        for (uint32_t f = 0; f < 6; f++) {
            viewport.width = static_cast<float>(extent.width * std::pow(0.5f, m));
            viewport.height = static_cast<float>(extent.height * std::pow(0.5f, m));
            device.SetViewport(command, viewport);

            // Render scene from cube face's point of view
            auto cmd{ device.CommandBuffer(command) };

            cmd.beginRenderPass(&renderPassInfo, vk::SubpassContents::eInline);

            // Update shader push constant block
            pushBlock.mvp = glm::perspective(glm::pi<float>() * 0.5f, 1.0f, 0.1f, 512.0f) * CubeMapProjViewMat[f];

            cmd.pushConstants(irradianceMap_.pipeline.Layout(), vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, 0,
                sizeof(IrradianceMap::PushBlock), &pushBlock);

            cmd.bindPipeline(vk::PipelineBindPoint::eGraphics, *irradianceMap_.pipeline);
            cmd.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, irradianceMap_.pipeline.Layout(), 0, 1, descriptorSet.PDescriptor(), 0, nullptr);

            // Draw skybox
            const auto& drawCall{ Shapes::Cube()->DrawCall() };
            device.DrawIndexed(command, drawCall.indexCount, drawCall.instanceCount, drawCall.indexStart, drawCall.vertexOffset, drawCall.firstInstance);

            cmd.endRenderPass();

            // Copy region for transfer from framebuffer to cube face
            vk::ImageCopy copyRegion = {};

            copyRegion.srcSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
            copyRegion.srcSubresource.baseArrayLayer = 0;
            copyRegion.srcSubresource.mipLevel = 0;
            copyRegion.srcSubresource.layerCount = 1;
            copyRegion.srcOffset = vk::Offset3D{ 0, 0, 0 };

            copyRegion.dstSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
            copyRegion.dstSubresource.baseArrayLayer = f;
            copyRegion.dstSubresource.mipLevel = m;
            copyRegion.dstSubresource.layerCount = 1;
            copyRegion.dstOffset = vk::Offset3D{ 0, 0, 0 };

            copyRegion.extent.width = static_cast<uint32_t>(viewport.width);
            copyRegion.extent.height = static_cast<uint32_t>(viewport.height);
            copyRegion.extent.depth = 1;

            cmd.copyImage(fb.Attachments().Texture(0).Image(), vk::ImageLayout::eTransferSrcOptimal, irradianceTexture->Image(),
                vk::ImageLayout::eTransferDstOptimal, 1, &copyRegion);

            // Transform framebuffer color attachment back
            fb.Attachments().Texture(0).Transition(command, vk::ImageLayout::eTransferSrcOptimal, vk::ImageLayout::eColorAttachmentOptimal);
        }
    }

    irradianceTexture->Transition(command, vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eShaderReadOnlyOptimal);

    device.SubmitCommandLists();
    device.WaitGpu();

    return irradianceTexture;
}

// Prefilter environment cubemap
// See https://placeholderart.wordpress.com/2015/07/28/implementation-notes-runtime-environment-map-filtering-for-image-based-lighting/
TextureRef Ibl::GeneratePrefilteredCube(const TextureViewRef& sourceTexture) {
    // TODO: Cleanup.

    const vk::Extent2D extent{ 256, 256 };
    const uint32_t numMips = static_cast<uint32_t>(floor(log2(extent.width))) + 1;

    auto& gfx{ GraphicsService::Graphics() };
    auto& device{ GraphicsService::Device() };

    auto prefilteredView{ CreateCubeTexture(extent, PrefilteredCube::Format, numMips, "PrefilteredCube") };

    prefilteredView->SetSampler(SamplerCache::Sampler(SamplerCache::Type::LinearClampToEdge, numMips));

    // Framebuffer.
    FramebufferAttachments attachments;
    attachments.CreateAttachment(vk::Extent3D{ extent, 1 }, PrefilteredCube::Format, vk::ImageAspectFlagBits::eColor, vk::SampleCountFlagBits::e1,
        vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eTransferSrc);

    Framebuffer fb{ *prefilteredCube_.renderPass, extent, std::move(attachments) };

    vk::FramebufferCreateInfo fbufCreateInfo{};
    fbufCreateInfo.renderPass = *prefilteredCube_.renderPass;
    fbufCreateInfo.attachmentCount = static_cast<uint32_t>(fb.Attachments().Attachments().size());
    fbufCreateInfo.pAttachments = fb.Attachments().Attachments().data();
    fbufCreateInfo.width = extent.width;
    fbufCreateInfo.height = extent.height;
    fbufCreateInfo.layers = 1;
    auto framebuffer{ gfx.Device().createFramebufferUnique(fbufCreateInfo) };

    // Descriptor sets
    ScopedDescriptor descriptorSet{ *cisPool_ };

    auto imageInfo{ sourceTexture->Descriptor() };
    auto writeDescriptor{ DescriptorImage(descriptorSet.Descriptor(), vk::DescriptorType::eCombinedImageSampler, &imageInfo, 0) };
    gfx.Device().updateDescriptorSets(writeDescriptor, {});

    // Render
    std::array<vk::ClearValue, 1> clearValues;
    clearValues[0].color = vk::ClearColorValue(std::array<float, 4>{ 0.0f, 0.0f, 0.2f, 0.0f });

    vk::Rect2D area{ vk::Offset2D{ 0, 0 }, extent };
    vk::RenderPassBeginInfo renderPassInfo(*prefilteredCube_.renderPass, *framebuffer, area, static_cast<uint32_t>(clearValues.size()), clearValues.data());
    vk::Viewport viewport{ 0.0f, 0.0f, static_cast<float>(extent.width), static_cast<float>(extent.height), 0.0f, 1.0f };
    vk::Rect2D scissor{ vk::Offset2D{ 0, 0 }, extent };

    auto command{ device.BeginCommandList() };

    device.SetScissor(command, scissor);

    vk::ImageSubresourceRange subresourceRange{};
    subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
    subresourceRange.baseMipLevel = 0;
    subresourceRange.levelCount = numMips;
    subresourceRange.layerCount = 6;

    // Change image layout for all cubemap faces to transfer destination
    auto prefilteredTexture{ TextureViews::TextureRef(prefilteredView) };
    prefilteredTexture->Transition(command, vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal);

    PrefilteredCube::PushBlock pushBlock{};

    GraphicsService::VertexBuffer()->Bind(command);

    for (uint32_t m = 0; m < numMips; m++) {
        pushBlock.roughness = static_cast<float>(m) / static_cast<float>(numMips - 1);

        for (uint32_t f = 0; f < 6; f++) {
            viewport.width = static_cast<float>(extent.width * std::pow(0.5f, m));
            viewport.height = static_cast<float>(extent.height * std::pow(0.5f, m));

            device.SetViewport(command, viewport);
            auto cmd{ device.CommandBuffer(command) };

            cmd.beginRenderPass(&renderPassInfo, vk::SubpassContents::eInline);

            // Update shader push constant block
            pushBlock.mvp = glm::perspective(glm::pi<float>() / 2.0f, 1.0f, 0.1f, 512.0f) * CubeMapProjViewMat[f];

            cmd.pushConstants(prefilteredCube_.pipeline.Layout(), vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, 0,
                sizeof(PrefilteredCube::PushBlock), &pushBlock);

            cmd.bindPipeline(vk::PipelineBindPoint::eGraphics, *prefilteredCube_.pipeline);
            cmd.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, prefilteredCube_.pipeline.Layout(), 0, 1, descriptorSet.PDescriptor(), 0, nullptr);

            const auto& drawCall{ Shapes::Cube()->DrawCall() };
            device.DrawIndexed(command, drawCall.indexCount, drawCall.instanceCount, drawCall.indexStart, drawCall.vertexOffset, drawCall.firstInstance);

            cmd.endRenderPass();

            // Copy region for transfer from framebuffer to cube face
            vk::ImageCopy copyRegion = {};

            copyRegion.srcSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
            copyRegion.srcSubresource.baseArrayLayer = 0;
            copyRegion.srcSubresource.mipLevel = 0;
            copyRegion.srcSubresource.layerCount = 1;
            copyRegion.srcOffset = vk::Offset3D{ 0, 0, 0 };

            copyRegion.dstSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
            copyRegion.dstSubresource.baseArrayLayer = f;
            copyRegion.dstSubresource.mipLevel = m;
            copyRegion.dstSubresource.layerCount = 1;
            copyRegion.dstOffset = vk::Offset3D{ 0, 0, 0 };

            copyRegion.extent.width = static_cast<uint32_t>(viewport.width);
            copyRegion.extent.height = static_cast<uint32_t>(viewport.height);
            copyRegion.extent.depth = 1;

            cmd.copyImage(fb.Attachments().Texture(0).Image(), vk::ImageLayout::eTransferSrcOptimal, prefilteredTexture->Image(),
                vk::ImageLayout::eTransferDstOptimal, copyRegion);

            // Transform framebuffer color attachment back
            fb.Attachments().Texture(0).Transition(command, vk::ImageLayout::eTransferSrcOptimal, vk::ImageLayout::eColorAttachmentOptimal);
        }
    }

    prefilteredTexture->Transition(command, vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eShaderReadOnlyOptimal);
    device.SubmitCommandLists();
    device.WaitGpu();

    return prefilteredTexture;
}

void Ibl::CreateBrdfLut() {
    vk::AttachmentDescription attDesc = {};
    // Color attachment
    attDesc.format = BrdfLut::Format;
    attDesc.samples = vk::SampleCountFlagBits::e1;
    attDesc.loadOp = vk::AttachmentLoadOp::eClear;
    attDesc.storeOp = vk::AttachmentStoreOp::eStore;
    attDesc.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
    attDesc.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
    attDesc.initialLayout = vk::ImageLayout::eUndefined;
    attDesc.finalLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
    vk::AttachmentReference colorReference = { 0, vk::ImageLayout::eColorAttachmentOptimal };

    vk::SubpassDescription subpassDescription = {};
    subpassDescription.pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
    subpassDescription.colorAttachmentCount = 1;
    subpassDescription.pColorAttachments = &colorReference;

    // Use subpass dependencies for layout transitions
    std::array<vk::SubpassDependency, 2> dependencies;
    dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
    dependencies[0].dstSubpass = 0;
    dependencies[0].srcStageMask = vk::PipelineStageFlagBits::eBottomOfPipe;
    dependencies[0].dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
    dependencies[0].srcAccessMask = vk::AccessFlagBits::eMemoryRead;
    dependencies[0].dstAccessMask = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;
    dependencies[0].dependencyFlags = vk::DependencyFlagBits::eByRegion;
    dependencies[1].srcSubpass = 0;
    dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
    dependencies[1].srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
    dependencies[1].dstStageMask = vk::PipelineStageFlagBits::eBottomOfPipe;
    dependencies[1].srcAccessMask = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;
    dependencies[1].dstAccessMask = vk::AccessFlagBits::eMemoryRead;
    dependencies[1].dependencyFlags = vk::DependencyFlagBits::eByRegion;

    // Create the actual renderpass
    vk::RenderPassCreateInfo renderPassCI{};
    renderPassCI.attachmentCount = 1;
    renderPassCI.pAttachments = &attDesc;
    renderPassCI.subpassCount = 1;
    renderPassCI.pSubpasses = &subpassDescription;
    renderPassCI.dependencyCount = 2;
    renderPassCI.pDependencies = dependencies.data();

    brdfLut_.renderPass = GraphicsService::Graphics().Device().createRenderPassUnique(renderPassCI);

    // Descriptorset
    std::vector<vk::DescriptorSetLayoutBinding> bindings{};
    vk::DescriptorSetLayoutCreateInfo descriptorSetLayoutCI;
    descriptorSetLayoutCI.bindingCount = static_cast<uint32_t>(bindings.size());
    descriptorSetLayoutCI.pBindings = bindings.data();

    brdfLut_.descriptorSetLayout = GraphicsService::Graphics().Device().createDescriptorSetLayoutUnique(descriptorSetLayoutCI);

    std::vector<vk::DescriptorPoolSize> sizes = { { vk::DescriptorType::eCombinedImageSampler, 1 } };
    brdfLut_.pool = std::make_unique<DescriptorSetPool>(*brdfLut_.descriptorSetLayout, sizes, 10);

    // Pipeline.
    vk::PipelineLayoutCreateInfo pipelineLayoutCI;
    pipelineLayoutCI.pSetLayouts = &(*brdfLut_.descriptorSetLayout);
    pipelineLayoutCI.setLayoutCount = 1;
    brdfLut_.pipelineLayout = GraphicsService::Graphics().Device().createPipelineLayoutUnique(pipelineLayoutCI);

    // Pipeline
    vk::PipelineInputAssemblyStateCreateInfo inputAssemblyState{ {}, vk::PrimitiveTopology::eTriangleList };
    vk::PipelineRasterizationStateCreateInfo rasterizationState{ {}, {}, {}, vk::PolygonMode::eFill, vk::CullModeFlagBits::eNone,
        vk::FrontFace::eCounterClockwise };
    rasterizationState.lineWidth = 1.0f;
    vk::PipelineColorBlendAttachmentState blendAttachmentState{};
    blendAttachmentState.blendEnable = VK_FALSE;
    blendAttachmentState.colorWriteMask
        = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;

    vk::PipelineColorBlendStateCreateInfo colorBlendState{};
    colorBlendState.attachmentCount = 1;
    colorBlendState.pAttachments = &blendAttachmentState;

    vk::PipelineDepthStencilStateCreateInfo depthStencilState{ {}, {}, {}, vk::CompareOp::eLessOrEqual };

    vk::Viewport viewPort{};
    vk::Rect2D scissor{};

    vk::PipelineViewportStateCreateInfo viewportState;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewPort;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;

    vk::PipelineMultisampleStateCreateInfo multisampleState{ {}, vk::SampleCountFlagBits::e1 };

    std::array<vk::DynamicState, 2> dynamicStateEnables = { vk::DynamicState::eScissor, vk::DynamicState::eViewport };

    vk::PipelineDynamicStateCreateInfo dynamicState{ {}, static_cast<uint32_t>(dynamicStateEnables.size()), dynamicStateEnables.data() };
    vk::PipelineVertexInputStateCreateInfo emptyInputState{};

    const auto& vertexShader{ ShaderCache::GetShader(vk::ShaderStageFlagBits::eVertex, L"assets/shaders/pbr/genbrdflut.vert") };
    const auto& fragmentShader{ ShaderCache::GetShader(vk::ShaderStageFlagBits::eFragment, L"assets/shaders/pbr/genbrdflut.frag") };

    std::array<vk::PipelineShaderStageCreateInfo, 2> shaderStages = { vertexShader.CreateInfo(), fragmentShader.CreateInfo() };

    vk::GraphicsPipelineCreateInfo pipelineCI{};
    pipelineCI.pInputAssemblyState = &inputAssemblyState;
    pipelineCI.pRasterizationState = &rasterizationState;
    pipelineCI.pColorBlendState = &colorBlendState;
    pipelineCI.pMultisampleState = &multisampleState;
    pipelineCI.pViewportState = &viewportState;
    pipelineCI.pDepthStencilState = &depthStencilState;
    pipelineCI.pDynamicState = &dynamicState;
    pipelineCI.stageCount = 2;
    pipelineCI.pStages = shaderStages.data();
    pipelineCI.pVertexInputState = &emptyInputState;
    pipelineCI.layout = *brdfLut_.pipelineLayout;
    pipelineCI.renderPass = *brdfLut_.renderPass;

    // Look-up-table (from BRDF) pipeline
    brdfLut_.pipeline = std::move(GraphicsService::Graphics().Device().createGraphicsPipelineUnique({}, pipelineCI).value);
}

void Ibl::CreateIrradianceMap() {
    auto& gfx{ GraphicsService::Graphics() };

    irradianceMap_.renderPass = CreateRenderPass(IrradianceMap::Format);

    // Pipeline
    vk::PushConstantRange pushConstantRange = { vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, 0, sizeof(IrradianceMap::PushBlock) };

    std::vector<vk::DescriptorSetLayout> dsLayouts{ *cisDescriptorSetLayout_ };

    vk::PipelineLayoutCreateInfo pipelineLayoutCI;
    pipelineLayoutCI.pushConstantRangeCount = 1;
    pipelineLayoutCI.pPushConstantRanges = &pushConstantRange;
    pipelineLayoutCI.pSetLayouts = dsLayouts.data();
    pipelineLayoutCI.setLayoutCount = static_cast<uint32_t>(dsLayouts.size());

    auto layout{ gfx.Device().createPipelineLayoutUnique(pipelineLayoutCI) };

    // Pipeline
    const auto& fragmentShader{ ShaderCache::GetShader(vk::ShaderStageFlagBits::eFragment, L"assets/shaders/pbr/irradiancecube.frag") };

    auto pipeline{ CreatePipeline(fragmentShader, *layout, *irradianceMap_.renderPass) };

    irradianceMap_.pipeline = Pipeline(vk::PipelineBindPoint::eGraphics, std::move(layout), std::move(pipeline), dsLayouts);
}

void Ibl::CreatePrefilteredCube() {
    auto& gfx{ GraphicsService::Graphics() };

    prefilteredCube_.renderPass = CreateRenderPass(PrefilteredCube::Format);

    // Pipeline
    vk::PushConstantRange pushConstantRange = { vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, 0, sizeof(PrefilteredCube::PushBlock) };

    std::vector<vk::DescriptorSetLayout> dsLayouts{ *cisDescriptorSetLayout_ };

    vk::PipelineLayoutCreateInfo pipelineLayoutCI;
    pipelineLayoutCI.pushConstantRangeCount = 1;
    pipelineLayoutCI.pPushConstantRanges = &pushConstantRange;
    pipelineLayoutCI.pSetLayouts = dsLayouts.data();
    pipelineLayoutCI.setLayoutCount = static_cast<uint32_t>(dsLayouts.size());

    auto layout{ gfx.Device().createPipelineLayoutUnique(pipelineLayoutCI) };

    // Pipeline
    const auto& fragmentShader{ ShaderCache::GetShader(vk::ShaderStageFlagBits::eFragment, L"assets/shaders/pbr/prefilterenvmap.frag") };

    auto pipeline{ CreatePipeline(fragmentShader, *layout, *prefilteredCube_.renderPass) };

    prefilteredCube_.pipeline = Pipeline(vk::PipelineBindPoint::eGraphics, std::move(layout), std::move(pipeline), dsLayouts);
}

void Ibl::CreateCommonDescriptorSetLayout() {
    // Layout.
    vk::DescriptorSetLayoutBinding setLayoutBindings
        = { ShaderLayoutBinding(vk::ShaderStageFlagBits::eFragment, vk::DescriptorType::eCombinedImageSampler, 0) };
    vk::DescriptorSetLayoutCreateInfo descriptorsetlayoutCI;
    descriptorsetlayoutCI.bindingCount = 1;
    descriptorsetlayoutCI.pBindings = &setLayoutBindings;

    cisDescriptorSetLayout_ = GraphicsService::Graphics().Device().createDescriptorSetLayoutUnique(descriptorsetlayoutCI);

    // Pool.
    std::vector<vk::DescriptorPoolSize> poolSizes = { { vk::DescriptorType::eCombinedImageSampler, 1 } };
    cisPool_ = std::make_unique<DescriptorSetPool>(*cisDescriptorSetLayout_, poolSizes, 16);
}

vk::UniquePipeline Ibl::CreatePipeline(const Shader& fragmentShader, vk::PipelineLayout pipelineLayout, vk::RenderPass renderPass) const {
    const auto& vertexShader{ ShaderCache::GetShader(vk::ShaderStageFlagBits::eVertex, L"assets/shaders/pbr/skybox.vert") };

    std::array<vk::PipelineShaderStageCreateInfo, 2> shaderStages = { vertexShader.CreateInfo(), fragmentShader.CreateInfo() };

    vk::PipelineInputAssemblyStateCreateInfo inputAssemblyState{ {}, vk::PrimitiveTopology::eTriangleList };
    vk::PipelineRasterizationStateCreateInfo rasterizationState{ {}, {}, {}, vk::PolygonMode::eFill, vk::CullModeFlagBits::eNone,
        vk::FrontFace::eCounterClockwise };
    rasterizationState.lineWidth = 1.0f;
    vk::PipelineColorBlendAttachmentState blendAttachmentState{};
    blendAttachmentState.blendEnable = VK_FALSE;
    blendAttachmentState.colorWriteMask
        = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;

    vk::PipelineColorBlendStateCreateInfo colorBlendState{};
    colorBlendState.attachmentCount = 1;
    colorBlendState.pAttachments = &blendAttachmentState;

    vk::PipelineDepthStencilStateCreateInfo depthStencilState{ {}, {}, {}, vk::CompareOp::eLessOrEqual };

    vk::Viewport viewPort{};
    vk::Rect2D scissor{};

    vk::PipelineViewportStateCreateInfo viewportState;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewPort;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;

    vk::PipelineMultisampleStateCreateInfo multisampleState{ {}, vk::SampleCountFlagBits::e1 };

    std::array<vk::DynamicState, 2> dynamicStateEnables = { vk::DynamicState::eScissor, vk::DynamicState::eViewport };

    vk::PipelineDynamicStateCreateInfo dynamicState{ {}, static_cast<uint32_t>(dynamicStateEnables.size()), dynamicStateEnables.data() };

    // TODO: Instances.
    auto bindingDescription{ VertexBindingDescription(false) };
    auto attributeDescriptions{ VertexAttributeDescriptions(false) };

    vk::PipelineVertexInputStateCreateInfo vertexInputInfo;
    vertexInputInfo.vertexBindingDescriptionCount = static_cast<uint32_t>(bindingDescription.size());
    vertexInputInfo.pVertexBindingDescriptions = bindingDescription.data();
    vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
    vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

    vk::GraphicsPipelineCreateInfo pipelineCI{};
    pipelineCI.pInputAssemblyState = &inputAssemblyState;
    pipelineCI.pRasterizationState = &rasterizationState;
    pipelineCI.pColorBlendState = &colorBlendState;
    pipelineCI.pMultisampleState = &multisampleState;
    pipelineCI.pViewportState = &viewportState;
    pipelineCI.pDepthStencilState = &depthStencilState;
    pipelineCI.pDynamicState = &dynamicState;
    pipelineCI.stageCount = 2;
    pipelineCI.pStages = shaderStages.data();
    pipelineCI.pVertexInputState = &vertexInputInfo;
    pipelineCI.layout = pipelineLayout;
    pipelineCI.renderPass = renderPass;

    return std::move(GraphicsService::Graphics().Device().createGraphicsPipelineUnique({}, pipelineCI).value);
}

TextureViewRef Ibl::CreateCubeTexture(vk::Extent2D extent, vk::Format format, uint32_t numMips, const std::string_view& name) const {
    vk::ImageCreateInfo imageCI{};
    imageCI.imageType = vk::ImageType::e2D;
    imageCI.format = format;
    imageCI.extent = vk::Extent3D{ extent, 1 };
    imageCI.mipLevels = numMips;
    imageCI.arrayLayers = 6;
    imageCI.samples = vk::SampleCountFlagBits::e1;
    imageCI.tiling = vk::ImageTiling::eOptimal;
    imageCI.usage = vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eTransferDst;
    imageCI.flags = vk::ImageCreateFlagBits::eCubeCompatible;

    auto cubeTexture{ TEXTURE_ADD(imageCI) };
    cubeTexture->SetName(name);

    auto cubeView{ TEXTURE_VIEW_ADD(cubeTexture, TextureView::CreateSampled(*cubeTexture, vk::ImageViewType::eCube, vk::ImageAspectFlagBits::eColor, 0, 6)) };

    return cubeView;
}

vk::UniqueRenderPass Ibl::CreateRenderPass(vk::Format format) const {
    vk::AttachmentDescription attDesc = {};
    // Color attachment
    attDesc.format = format;
    attDesc.samples = vk::SampleCountFlagBits::e1;
    attDesc.loadOp = vk::AttachmentLoadOp::eClear;
    attDesc.storeOp = vk::AttachmentStoreOp::eStore;
    attDesc.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
    attDesc.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
    attDesc.initialLayout = vk::ImageLayout::eUndefined;
    attDesc.finalLayout = vk::ImageLayout::eTransferSrcOptimal;
    vk::AttachmentReference colorReference = { 0, vk::ImageLayout::eColorAttachmentOptimal };

    vk::SubpassDescription subpassDescription = {};
    subpassDescription.pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
    subpassDescription.colorAttachmentCount = 1;
    subpassDescription.pColorAttachments = &colorReference;

    // Use subpass dependencies for layout transitions
    std::array<vk::SubpassDependency, 2> dependencies;
    dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
    dependencies[0].dstSubpass = 0;
    dependencies[0].srcStageMask = vk::PipelineStageFlagBits::eBottomOfPipe;
    dependencies[0].dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
    dependencies[0].srcAccessMask = vk::AccessFlagBits::eMemoryRead;
    dependencies[0].dstAccessMask = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;
    dependencies[0].dependencyFlags = vk::DependencyFlagBits::eByRegion;
    dependencies[1].srcSubpass = 0;
    dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
    dependencies[1].srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
    dependencies[1].dstStageMask = vk::PipelineStageFlagBits::eBottomOfPipe;
    dependencies[1].srcAccessMask = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;
    dependencies[1].dstAccessMask = vk::AccessFlagBits::eMemoryRead;
    dependencies[1].dependencyFlags = vk::DependencyFlagBits::eByRegion;

    // Create the actual renderpass
    vk::RenderPassCreateInfo renderPassCI{};
    renderPassCI.attachmentCount = 1;
    renderPassCI.pAttachments = &attDesc;
    renderPassCI.subpassCount = 1;
    renderPassCI.pSubpasses = &subpassDescription;
    renderPassCI.dependencyCount = 2;
    renderPassCI.pDependencies = dependencies.data();

    return GraphicsService::Graphics().Device().createRenderPassUnique(renderPassCI);
}

} // namespace ugine::gfx
