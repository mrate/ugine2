﻿#include "Helpers.h"

#include <ugine/gfx/core/Vertex.h>

namespace ugine::gfx {

float CalcCullRadius(const std::vector<Vertex>& vertices) {
    float maxDistance{};

    float distance{};
    for (const auto& vertex : vertices) {
        distance = vertex.pos.x * vertex.pos.x + vertex.pos.y * vertex.pos.y + vertex.pos.z * vertex.pos.z;
        maxDistance = std::max(distance, maxDistance);
    }

    return sqrtf(maxDistance);
}

math::AABB CalcAABB(const std::vector<Vertex>& vertices) {
    glm::vec3 min{}, max{};
    for (const auto& v : vertices) {
        min.x = std::min(min.x, v.pos.x);
        min.y = std::min(min.y, v.pos.y);
        min.z = std::min(min.z, v.pos.z);

        max.x = std::max(max.x, v.pos.x);
        max.y = std::max(max.y, v.pos.y);
        max.z = std::max(max.z, v.pos.z);
    }
    return math::AABB(min, max);
}

} // namespace ugine::gfx
