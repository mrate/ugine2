﻿#pragma once

#include <vulkan/vulkan.hpp>

#include <filesystem>

#include <stdint.h>

namespace ugine::gfx {

struct ShaderParsedData {
    struct Member {
        enum class Type {
            Float,
            Float2,
            Float3,
            Float4,
            Matrix3x3,
            Matrix4x4,
            Int,
            Int2,
            Int3,
            Int4,
            Bool,
        };

        Type type;
        uint32_t offset;
        uint32_t size;
        std::string name;
    };

    struct Binding {
        enum class TexDim {
            Tex2D,
            Tex3D,
            TexCube,
        };

        uint32_t binding{};
        vk::DescriptorType type{};
        std::string name;
        std::string typeName;
        std::vector<Member> members;
        uint32_t size{};
        TexDim dim{};
        uint32_t count{};
        bool isArray{};
    };

    struct DescriptorSet {
        uint32_t set;
        std::vector<Binding> bindings;
    };

    struct PushConstant {
        uint32_t offset{};
        uint32_t size{};
    };

    struct InterfaceVariable {
        std::string name;
        uint32_t location{};
    };

    std::vector<DescriptorSet> descriptorSets;
    std::vector<PushConstant> pushConstants;
    std::vector<InterfaceVariable> inputs;
};

class SpirvParser {
public:
    SpirvParser(vk::ShaderStageFlags stage, const std::filesystem::path& file, bool binary);
    SpirvParser(const void* data, size_t size);

    const ShaderParsedData& ParsedData() const {
        return data_;
    }

private:
    void Parse(const void* data, size_t size);

    ShaderParsedData data_;
};

} // namespace ugine::gfx
