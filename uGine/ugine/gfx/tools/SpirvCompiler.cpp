﻿#include "SpirvCompiler.h"
#include "SpirvPreprocessor.h"

#include <ugine/gfx/Error.h>
#include <ugine/gfx/VulkanApi.h>
#include <ugine/utils/Log.h>

#include <shaderc/shaderc.hpp>

#include <fstream>

#pragma comment(lib, "shaderc_shared.lib")

namespace {

shaderc_shader_kind FromStage(vk::ShaderStageFlags stage) {
    if (stage == vk::ShaderStageFlagBits::eVertex) {
        return shaderc_glsl_vertex_shader;
    } else if (stage == vk::ShaderStageFlagBits::eGeometry) {
        return shaderc_glsl_geometry_shader;
    } else if (stage == vk::ShaderStageFlagBits::eTessellationControl) {
        return shaderc_glsl_tess_control_shader;
    } else if (stage == vk::ShaderStageFlagBits::eTessellationEvaluation) {
        return shaderc_glsl_tess_evaluation_shader;
    } else if (stage == vk::ShaderStageFlagBits::eFragment) {
        return shaderc_glsl_fragment_shader;
    } else if (stage == vk::ShaderStageFlagBits::eCompute) {
        return shaderc_glsl_compute_shader;
    } else {
        UGINE_THROW(ugine::gfx::GfxError, "Invalid shader stage passed to compiler.");
    }
}

} // namespace

namespace ugine::gfx {

class SpirvCompilerImpl {
public:
    SpirvCompilerImpl() {
        compiler_ = shaderc_compiler_initialize();
        options_ = shaderc_compile_options_initialize();

        // TODO: Use macro instead of search/replace in preprocessor.
        for (auto [name, val] : SpirvPreprocessor::BuiltIns()) {
            shaderc_compile_options_add_macro_definition(options_, name.data(), name.size(), val.data(), val.size());
        }

#ifdef _DEBUG
        shaderc_compile_options_set_generate_debug_info(options_);
        shaderc_compile_options_set_optimization_level(options_, shaderc_optimization_level_zero);
#else
        //shaderc_compile_options_set_optimization_level(options_, shaderc_optimization_level_performance);
#endif
    }

    ~SpirvCompilerImpl() {
        if (result_) {
            shaderc_result_release(result_);
        }

        shaderc_compile_options_release(options_);
        shaderc_compiler_release(compiler_);
    }

    bool Compile(shaderc_shader_kind kind, const std::string& source, const std::string& entryPoint, const std::filesystem::path& fileName, bool preprocess,
        std::set<std::filesystem::path>* includes) {
        //UGINE_PROFILE("ShaderCompile");

        if (result_) {
            shaderc_result_release(result_);
        }

        std::string preprocessed{ source };

        if (preprocess) {
            SpirvPreprocessor prep;
            if (!prep.Process(fileName, preprocessed, includes)) {
                error_ = prep.Error();
                return false;
            }
        }

        result_ = shaderc_compile_into_spv(compiler_, preprocessed.c_str(), preprocessed.size(), kind, fileName.string().c_str(), entryPoint.c_str(), options_);

        if (shaderc_result_get_num_errors(result_)) {
            error_ = shaderc_result_get_error_message(result_);

            int errLine{};
            std::string errFile{};
            ParseError(error_, errFile, errLine);

            UGINE_DEBUG("Failed to compile shader {}:", fileName.string());
            UGINE_DEBUG(error_.c_str());
            std::istringstream in(preprocessed);
            int lineNo{ 1 };
            for (std::string line; std::getline(in, line); lineNo++) {
                if (lineNo > errLine - 3 && lineNo < errLine + 3) {
                    if (errLine == lineNo) {
                        UGINE_ERROR(">{:4}: {}", lineNo, line);
                    } else {
                        UGINE_ERROR(" {:4}: {}", lineNo, line);
                    }
                } else if (lineNo >= errLine + 3) {
                    break;
                }
                //} else {
                //    UGINE_DEBUG(" {:4}: {}", lineNo, line);
                //}
            }

            return false;
        }

        if (shaderc_result_get_num_warnings(result_)) {
            error_ = shaderc_result_get_error_message(result_);
            UGINE_WARN("Shader '{}' warnings: {}", error_);
        }

        data_.resize(shaderc_result_get_length(result_));
        memcpy(data_.data(), shaderc_result_get_bytes(result_), data_.size());

#ifdef _DEBUG
        {
            std::ofstream str(std::filesystem::path("tmp") / fileName.filename());
            str << preprocessed;
        }
#endif

        return true;
    }

    void ParseError(const std::string& error, std::string& file, int& line) const {
        auto pos{ error.find(':') };
        if (pos != std::string::npos) {
            file = error.substr(0, pos);

            line = 0;
            while (++pos < error.size() && error[pos] >= '0' && error[pos] <= '9') {
                line = line * 10 + (error[pos] - '0');
            }
        }
    }

    shaderc_compile_options_t options_{};
    shaderc_compiler_t compiler_{};
    shaderc_compilation_result_t result_{};
    std::string error_;
    std::vector<uint8_t> data_;
};

SpirvCompiler::SpirvCompiler()
    : impl_(std::make_unique<SpirvCompilerImpl>()) {}

SpirvCompiler::~SpirvCompiler() {}

void SpirvCompiler::SetWarningsAsErrors() {
    shaderc_compile_options_set_warnings_as_errors(impl_->options_);
}

bool SpirvCompiler::Compile(vk::ShaderStageFlags stage, const std::string& source, const std::string& entryPoint, const std::filesystem::path& fileName,
    bool preprocess, std::set<std::filesystem::path>* includes) {
    return impl_->Compile(FromStage(stage), source, entryPoint, fileName, preprocess, includes);
}

const std::string& SpirvCompiler::Error() const {
    return impl_->error_;
}

const std::vector<uint8_t>& SpirvCompiler::Data() const {
    return impl_->data_;
}

} // namespace ugine::gfx
