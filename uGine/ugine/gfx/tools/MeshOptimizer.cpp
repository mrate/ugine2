﻿#include "MeshOptimizer.h"

#include <meshoptimizer.h>

#pragma comment(lib, "meshoptimizer.lib")

namespace ugine::gfx {

MeshOptimizer::MeshOptimizer(const std::vector<Vertex>& vertices, const std::vector<IndexType>& indices, bool baseOptimization)
    : vertices_{ vertices }
    , indices_{ indices } {

    meshopt_optimizeVertexCache(indices_.data(), indices_.data(), indices_.size(), vertices_.size());
    meshopt_optimizeOverdraw(indices_.data(), indices_.data(), indices_.size(), &vertices_[0].pos.x, vertices_.size(), sizeof(Vertex), 1.05f);
    if (baseOptimization) {
        meshopt_optimizeVertexFetch(vertices_.data(), indices_.data(), indices_.size(), vertices_.data(), vertices_.size(), sizeof(Vertex));
    }
}

void MeshOptimizer::CreateLod(float lod, std::vector<IndexType>& indices) const {
    auto targetIndexCount{ size_t(indices_.size() * lod) };
    float targetError{ 1e-2f };

    float lodError{};
    indices.resize(indices_.size());
    indices.resize(meshopt_simplify(
        indices.data(), indices_.data(), indices_.size(), &vertices_[0].pos.x, vertices_.size(), sizeof(Vertex), targetIndexCount, targetError, &lodError));
}

} // namespace ugine::gfx
