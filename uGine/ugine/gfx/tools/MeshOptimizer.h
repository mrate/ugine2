﻿#pragma once

#include <ugine/gfx/import/MeshData.h>

namespace ugine::gfx {

class MeshOptimizer {
public:
    MeshOptimizer(const std::vector<Vertex>& vertices, const std::vector<IndexType>& indices, bool baseOptimization = true);

    void CreateLod(float lod, std::vector<IndexType>& indices) const;

    const std::vector<Vertex>& Vertices() const {
        return vertices_;
    }

    const std::vector<IndexType>& Indices() const {
        return indices_;
    }

private:
    std::vector<Vertex> vertices_;
    std::vector<IndexType> indices_;
};

} // namespace ugine::gfx
