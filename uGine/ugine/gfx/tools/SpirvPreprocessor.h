﻿#pragma once

#include <filesystem>
#include <map>
#include <set>
#include <string>

namespace ugine::gfx {

class SpirvPreprocessor {
public:
    static void IncludePaths(const std::filesystem::path& file, std::vector<std::filesystem::path>& result);

    static const std::vector<std::pair<std::string, std::string>>& BuiltIns();

    bool Process(const std::filesystem::path& file, std::string& src, std::set<std::filesystem::path>* includes = nullptr);

    const std::string& Error() const {
        return error_;
    }

private:
    bool HandleIncludes(const std::filesystem::path& file, std::string& src, std::set<std::filesystem::path>* includes);
    void IncludeFile(std::ostream& out, const std::filesystem::path& path, std::set<std::filesystem::path>* includes);

    std::string error_;
};

} // namespace ugine::gfx
