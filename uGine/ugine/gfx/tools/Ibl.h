﻿#pragma once

#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/ShaderCache.h>
#include <ugine/gfx/Storages.h>
#include <ugine/gfx/core/DescriptorSetPool.h>

#include <glm/glm.hpp>

#include <filesystem>
#include <memory>

namespace ugine::gfx {

class Ibl {
public:
    Ibl();

    TextureRef GenerateBrdfLut();
    TextureRef GenerateIrradianceMap(const TextureViewRef& sourceTexture);
    TextureRef GeneratePrefilteredCube(const TextureViewRef& sourceTexture);

private:
    void CreateBrdfLut();
    void CreateIrradianceMap();
    void CreatePrefilteredCube();
    void CreateCommonDescriptorSetLayout();

    vk::UniquePipeline CreatePipeline(const Shader& fragmentShader, vk::PipelineLayout pipelineLayout, vk::RenderPass renderPass) const;

    TextureViewRef CreateCubeTexture(vk::Extent2D extent, vk::Format format, uint32_t numMips, const std::string_view& name) const;

    vk::UniqueRenderPass CreateRenderPass(vk::Format format) const;

    struct BrdfLut {
        static const vk::Format Format{ vk::Format::eR16G16Sfloat };
        vk::UniqueRenderPass renderPass;
        vk::UniqueDescriptorSetLayout descriptorSetLayout;
        std::unique_ptr<DescriptorSetPool> pool;
        vk::UniquePipelineLayout pipelineLayout;
        vk::UniquePipeline pipeline;
    };

    struct IrradianceMap {
        static const vk::Format Format{ vk::Format::eR32G32B32A32Sfloat };

        struct PushBlock {
            glm::mat4 mvp;
            float deltaPhi;
            float deltaTheta;
        };

        vk::UniqueRenderPass renderPass;
        Pipeline pipeline;
    };

    struct PrefilteredCube {
        static const vk::Format Format{ vk::Format::eR16G16B16A16Sfloat };

        struct PushBlock {
            glm::mat4 mvp;
            float roughness{};
            uint32_t numSamples{ 128 };
        };

        vk::UniqueRenderPass renderPass;
        Pipeline pipeline;
    };

    vk::UniqueDescriptorPool pool_;

    BrdfLut brdfLut_;
    IrradianceMap irradianceMap_;
    PrefilteredCube prefilteredCube_;

    vk::UniqueDescriptorSetLayout cisDescriptorSetLayout_;
    std::unique_ptr<DescriptorSetPool> cisPool_;
};

} // namespace ugine::gfx
