﻿#include "GizmoRenderer.h"

#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/data/Shapes.h>
#include <ugine/gfx/rendering/Mesh.h>
#include <ugine/utils/Profile.h>

#include <glm/gtc/matrix_transform.hpp>

namespace ugine::gfx {

GizmoRenderer::GizmoRenderer(vk::RenderPass renderPass) {
    utils::ScopeTimer timer("Gizmo materials init");

    std::vector<std::filesystem::path> files = {
        L"assets/materials/gui/gizmoWireframe.mat",
        L"assets/materials/gui/gizmoWireframeFrustum.mat",
        L"assets/materials/gui/gizmoWireframeCube.mat",
        L"assets/materials/gui/gizmoWireframeDir.mat",
        L"assets/materials/gui/gizmoWireframeLine.mat",
    };

    //auto refs{ MaterialService::Instantiate(files) };

    for (int i = 0; i < PipelineCount; ++i) {
        pipelines_[i].material = MaterialService::InstantiateByPath(files[i]);

        //pipelines_[i].material = refs[i];
        pipelines_[i].pipeline = pipelines_[i].material->Material().DefaultPass().CreatePipeline(renderPass);
    }
}

void GizmoRenderer::RenderMesh(GPUCommandList command, const Mesh& mesh, const glm::vec4& color, const glm::mat4& mvp) {
    auto& device{ GraphicsService::Device() };

    device.BindPipeline(command, &pipelines_[PipelineMesh].pipeline);
    device.PushConstants(command, GPUPushConstant::Data(vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, mvp));
    device.PushConstants(command, GPUPushConstant::Data(vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, color, 112));
    device.BindVertexIndexBuffer(command, *mesh.VertexBuffer());

    const auto& drawCall{ mesh.DrawCall(0) };
    device.DrawIndexed(command, drawCall.indexCount, drawCall.instanceCount, drawCall.indexStart, drawCall.vertexOffset, drawCall.firstInstance);
}

void GizmoRenderer::RenderCube(GPUCommandList command, const glm::vec4& color, const glm::mat4& mvp) {
    auto& device{ GraphicsService::Device() };

    device.BindPipeline(command, &pipelines_[PipelineCube].pipeline);
    device.PushConstants(command, GPUPushConstant::Data(vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, mvp));
    device.PushConstants(command, GPUPushConstant::Data(vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, color, 112));
    device.Draw(command, 24, 1, 0, 0);
}

void GizmoRenderer::RenderAABB(GPUCommandList command, const math::AABB& aabb, const glm::vec4& color, const glm::mat4& mvp) {
    RenderCube(command, color, mvp * glm::translate(glm::mat4{ 1.0f }, aabb.CenterPoint()) * glm::scale(glm::mat4{ 1.0f }, aabb.Size()));
}

void GizmoRenderer::RenderFrustum(
    GPUCommandList command, float vFov, float aspectRatio, float zNear, float zFar, const glm::vec4& color, const glm::mat4& mvp) {
    auto& device{ GraphicsService::Device() };

    Frustum frustum{};
    frustum.mvp = mvp;
    frustum.zNear = zNear;
    frustum.zFar = std::min(zFar, 10.0f);
    frustum.vfovTanHalf = tan(0.5f * glm::radians(vFov));
    frustum.hfovTanHalf = frustum.vfovTanHalf * aspectRatio;

    device.BindPipeline(command, &pipelines_[PipelineFrustum].pipeline);
    device.PushConstants(command, GPUPushConstant::Data(vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, frustum));
    device.PushConstants(command, GPUPushConstant::Data(vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, color, 112));
    device.Draw(command, 24, 1, 0, 0);
}

void GizmoRenderer::RenderDirection(GPUCommandList command, const glm::vec4& color, const glm::mat4& mvp) {
    auto& device{ GraphicsService::Device() };

    device.BindPipeline(command, &pipelines_[PipelineDir].pipeline);
    device.PushConstants(command, GPUPushConstant::Data(vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, mvp));
    device.PushConstants(command, GPUPushConstant::Data(vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, color, 112));
    device.Draw(command, 8, 1, 0, 0);
}

void GizmoRenderer::RenderLine(GPUCommandList command, const glm::vec4& color, const glm::mat4& viewProj, const glm::vec3& from, const glm::vec3& to) {
    Line line{};
    line.viewProj = viewProj;
    line.from = from;
    line.to = to;

    auto& device{ GraphicsService::Device() };

    device.BindPipeline(command, &pipelines_[PipelineLine].pipeline);
    device.PushConstants(command, GPUPushConstant::Data(vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, line));
    device.PushConstants(command, GPUPushConstant::Data(vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, color, 112));
    device.Draw(command, 2, 1, 0, 0);
}

} // namespace ugine::gfx
