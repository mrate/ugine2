﻿#pragma once

#include <ugine/math/Aabb.h>

#include <vector>

namespace ugine::gfx {

struct Vertex;

float CalcCullRadius(const std::vector<Vertex>& vertices);
math::AABB CalcAABB(const std::vector<Vertex>& vertices);

} // namespace ugine::gfx
