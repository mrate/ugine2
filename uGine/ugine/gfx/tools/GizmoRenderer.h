﻿#pragma once

#include <ugine/gfx/core/Device.h>
#include <ugine/gfx/core/Pipeline.h>
#include <ugine/gfx/rendering/MaterialInstance.h>
#include <ugine/math/Aabb.h>

namespace ugine::gfx {

class Mesh;

class GizmoRenderer {
public:
    GizmoRenderer() = default;
    GizmoRenderer(vk::RenderPass renderPass);

    void RenderMesh(GPUCommandList command, const Mesh& mesh, const glm::vec4& color, const glm::mat4& mvp);
    void RenderCube(GPUCommandList command, const glm::vec4& color, const glm::mat4& mvp);
    void RenderAABB(GPUCommandList command, const math::AABB& aabb, const glm::vec4& color, const glm::mat4& mvp);
    void RenderFrustum(GPUCommandList command, float vFov, float aspectRatio, float zNear, float zFar, const glm::vec4& color, const glm::mat4& mvp);
    void RenderDirection(GPUCommandList command, const glm::vec4& color, const glm::mat4& mvp);
    void RenderLine(GPUCommandList command, const glm::vec4& color, const glm::mat4& viewProj, const glm::vec3& from, const glm::vec3& to);

private:
    enum Pipelines {
        PipelineMesh = 0,
        PipelineFrustum,
        PipelineCube,
        PipelineDir,
        PipelineLine,
        PipelineCount,
    };

    struct alignas(16) Frustum {
        glm::mat4 mvp;
        float vfovTanHalf;
        float hfovTanHalf;
        float zNear;
        float zFar;
    };

    struct Line {
        glm::mat4 viewProj;
        alignas(16) glm::vec3 from;
        alignas(16) glm::vec3 to;
    };

    struct PipelineDesc {
        Pipeline pipeline;
        MaterialInstanceRef material;
    };

    std::array<PipelineDesc, PipelineCount> pipelines_;
};

} // namespace ugine::gfx
