﻿#pragma once

#include <ugine/core/Error.h>

#include <vulkan/vulkan.hpp>

#include <memory>
#include <optional>
#include <string>
#include <vector>

namespace ugine::gfx {

class Graphics;

struct Queues {
    uint32_t graphics;
    uint32_t compute;
    uint32_t transfer;
    uint32_t surfacePresent;
};

struct SwapChainSupportDetails {
    vk::SurfaceCapabilitiesKHR capabilities;
    std::vector<vk::SurfaceFormatKHR> formats;
    std::vector<vk::PresentModeKHR> presentModes;
};

class VulkanApi {
public:
    static const std::vector<const char*> DEVICE_EXTENSIONS;
    static const std::vector<const char*> VALIDATION_LAYERS;

    static vk::SampleCountFlagBits GetMaxUsableSampleCount(vk::PhysicalDevice& physicalDevice);
    static SwapChainSupportDetails QuerySwapChainSupport(vk::PhysicalDevice device, vk::SurfaceKHR surface);
    static vk::SurfaceFormatKHR ChooseSwapSurfaceFormat(const std::vector<vk::SurfaceFormatKHR>& availableFormats, bool srgb);
    static vk::PresentModeKHR ChooseSwapPresentMode(const std::vector<vk::PresentModeKHR>& availablePresentModes);
    static vk::Extent2D ChooseSwapExtent(const vk::SurfaceCapabilitiesKHR& capabilities, uint32_t width, uint32_t height);
    static bool HasStencilComponent(VkFormat format);

    VulkanApi(const std::string& appName, bool enableValidationLayers);
    ~VulkanApi();

    vk::Instance Instance() {
        return instance_.get();
    }

    std::unique_ptr<Graphics> CreateFirstSuitableDevice(vk::SurfaceKHR surface);

    bool SupportsDebug() const {
        return supportsDebug_;
    }

    void LoadProcAddr(vk::Device device);

private:
    bool CheckDebugSupport(std::vector<const char*>& extensions);
    bool CheckValidationLayerSupport() const;
    std::vector<const char*> GetRequiredExtensions(bool enableValidationLayers);
    void SetupDebugMessenger();

    bool enableValidationLayers_{ false };
    vk::UniqueInstance instance_;
    VkDebugUtilsMessengerEXT debugMessenger_;
    bool supportsDebug_{};
};

} // namespace ugine::gfx
