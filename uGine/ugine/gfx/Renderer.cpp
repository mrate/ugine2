﻿#include "Renderer.h"

#include <ugine/core/systems/LightingDataCollector.h>

#include <ugine/core/Component.h>
#include <ugine/core/ConfigService.h>
#include <ugine/core/Scene.h>
#include <ugine/core/Scheduler.h>
#include <ugine/core/systems/AnimationSystem.h>
#include <ugine/gfx/FrameStats.h>
#include <ugine/gfx/Graphics.h>
#include <ugine/gfx/GraphicsService.h>
#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/core/GpuProfiler.h>
#include <ugine/gfx/data/LensFlare.h>
#include <ugine/gfx/data/PipelineCache.h>
#include <ugine/gfx/data/RenderPassCache.h>
#include <ugine/gfx/data/Shapes.h>
#include <ugine/gfx/postprocess/SSAO.h>
#include <ugine/gfx/rendering/Foliage.h>
#include <ugine/gfx/rendering/MeshMaterial.h>
#include <ugine/gfx/rendering/ShadowMaps.h>
#include <ugine/gfx/rendering/Terrain.h>
#include <ugine/gfx/tools/Ibl.h>
#include <ugine/utils/Log.h>
#include <ugine/utils/Profile.h>

#include <ugine/utils/ResourceManagement.h>

#include <algorithm>
#include <execution>

namespace ugine::gfx {

namespace {

    inline uint32_t StencilValue(const core::GameObject& go) {
        return (go.Component<core::Tag>().flags & core::Tag::Flags::Outline) ? 0x1 : 0x0;
    }

    inline uint32_t StencilValue(RenderContext& context, entt::entity ent) {
        return (context.scene->Registry().get<core::Tag>(ent).flags & core::Tag::Flags::Outline) ? 0x1 : 0x0;
    }

    inline void PrepareStencil(RenderContext& context, entt::entity ent, uint32_t flags = 0) {
        const auto write{ StencilValue(context, ent) };
        GraphicsService::Device().SetStencilWriteCompareMask(context.command, vk::StencilFaceFlagBits::eFrontAndBack, flags | write, 0x0);
    }

    inline void PrepareStencil(RenderContext& context, const core::GameObject& go, uint32_t flags = 0) {
        const auto write{ StencilValue(go) };
        GraphicsService::Device().SetStencilWriteCompareMask(context.command, vk::StencilFaceFlagBits::eFrontAndBack, flags | write, 0x0);
    }

} // namespace

Renderer::Renderer() {
    auto brdfTexture{ GraphicsService::Ibl().GenerateBrdfLut() };
    brdfLut_ = TEXTURE_VIEW_ADD(brdfTexture, TextureView::CreateSampled(*brdfTexture));

    // Volumetrics.
    volumetricMat_ = MaterialService::Register(L"assets/materials/volumetric.mat");
    dirLightVolMaterial_ = gfx::MaterialService::Register(L"assets/materials/volumetricDir.mat");
    pointLightVolMaterial_ = gfx::MaterialService::Register(L"assets/materials/volumetricPoint.mat");
    spotLightVolMaterial_ = gfx::MaterialService::Register(L"assets/materials/volumetricSpot.mat");

    // Light shafts.
    sunMaterial_ = gfx::MaterialService::Register(L"assets/materials/sun.mat");
    lightShaftsMaterial_ = gfx::MaterialService::InstantiateByPath(L"assets/materials/fx/lightShafts.mat");

    // AO
    ssaoCB_ = CreateCB(sizeof(SsaoParams));
    SSAO::GenerateRandomKernel(ssaoParams_.kernel, limits::SSAO_KERNEL_SIZE);
    ssaoMaterial_ = gfx::MaterialService::Register(L"assets/materials/fx/ssaoFx.mat");

    frameCB_ = CreateCB(sizeof(FrameCB));

    shadowMaps_ = std::make_unique<gfx::ShadowMaps>(RenderPassCache::Shadow().RenderPass());

    // Compute.
    bilateralUpscale_ = LoadComputeShader(L"assets/shaders/compute/bilateralUpscale.comp");
    lightShafts_ = LoadComputeShader(L"assets/shaders/compute/lightShafts.comp");
    blur_ = LoadComputeShader(L"assets/shaders/compute/blur.comp");
}

Renderer::~Renderer() {}

ComputePipeline Renderer::LoadComputeShader(const std::filesystem::path& path) const {
    ShaderProgram shaderProgram;
    std::vector<vk::UniqueDescriptorSetLayout> layouts;

    const auto& computeShader{ ShaderCache::GetShader(vk::ShaderStageFlagBits::eCompute, path) };
    shaderProgram.AddShader(computeShader);
    shaderProgram.Link(layouts);

    return ComputePipeline{ std::move(layouts), computeShader };
}

void Renderer::CollectLights(RenderContext& context) {
    PROFILE_EVENT("CollectLights");

    core::LightingDataCollector col{ lightShading_.Data(), lightShading_.Lights() };
    col.Collect(context);
    lightShading_.SetShadowStrength(core::ConfigService::ShadowIntensity());
    lightShading_.Update(context.command);
}

void Renderer::BindCommonCB(GPUCommandList cmd, const RenderContext& context, const TextureView& ao, const TextureView& depth, const TextureView& scene) {
    PROFILE_EVENT("BindCommonCB");

    lightShading_.Bind(cmd, *shadowMaps_);

    auto& device{ GraphicsService::Device() };

    device.BindUniform(cmd, FRAME_DATASET, 0, frameCB_);

    auto allocation{ GraphicsService::Device().AllocateGPU(cmd, sizeof(CameraCB)) };
    SetupCameraCB(*reinterpret_cast<CameraCB*>(allocation.memory), *context.camera, context.viewProjPrev);
    device.BindUniform(cmd, CAMERA_DATASET, 0, vk::DescriptorBufferInfo{ allocation.buffer, allocation.offset, sizeof(CameraCB) });

    const auto nullTextureBlack{ GraphicsService::NullTextureBlack()->Descriptor() };
    const auto nullTextureWhite{ GraphicsService::NullTextureWhite()->Descriptor() };
    device.BindImageSampler(
        cmd, GLOBAL_SHADING_DATASET, AO_BINDING, (context.renderFlags & RenderFlags::AmbientOcclusion) && ao ? ao.Descriptor() : nullTextureWhite);
    device.BindImageSampler(cmd, GLOBAL_SHADING_DATASET, DEPTH_MAP_BINDING, depth ? depth.Descriptor() : nullTextureWhite);
    device.BindImageSampler(cmd, GLOBAL_SHADING_DATASET, SCENE_BINDING, scene ? scene.Descriptor() : nullTextureBlack);
}

void Renderer::UpdateSkyBox(const core::Scene& scene) {
    skyBox_ = scene.SkyBox();

    if (skyBox_) {
        if (!skyBoxMaterial_) {
            skyBoxMaterial_ = MaterialService::Instantiate("Skybox");
        }
    }

    auto irradianceTexture{ GraphicsService::Ibl().GenerateIrradianceMap(skyBox_) };
    skyBoxIrradiance_
        = TEXTURE_VIEW_ADD(irradianceTexture, TextureView::CreateSampled(*irradianceTexture, vk::ImageViewType::eCube, vk::ImageAspectFlagBits::eColor, 0, 6));

    auto prefilteredTexture{ GraphicsService::Ibl().GeneratePrefilteredCube(skyBox_) };
    skyBoxPrefiltered_ = TEXTURE_VIEW_ADD(
        prefilteredTexture, TextureView::CreateSampled(*prefilteredTexture, vk::ImageViewType::eCube, vk::ImageAspectFlagBits::eColor, 0, 6));

    skyBoxMaterial_->SetTexture("skyboxTexture", skyBox_);

    lightShading_.SetIbl(brdfLut_, skyBoxIrradiance_, skyBoxPrefiltered_);
}

glm::fquat Renderer::SunDirection(const RenderContext& context) const {
    const auto& sunLight{ context.scene->SunLight() };
    if (sunLight && sunLight.Has<core::LightComponent>()) {
        const auto& light{ sunLight.Component<core::LightComponent>() };
        if (light.light.type == Light::Type::Directional) {
            return sunLight.GlobalTransformation().rotation;
        }
    }

    return glm::fquat{};
}

void Renderer::RenderOpaque(RenderContext& context) {
    PROFILE_EVENT("Render opaque");

    UGINE_ASSERT(context.scene);
    UGINE_ASSERT(context.camera);
    UGINE_ASSERT(context.visibility);

    context.stencil = OPAQUE_STENCIL;
    {
        PROFILE_EVENT("Render opaque");

        auto& device{ GraphicsService::Device() };

        // Opaque meshes.
        if (!context.opaqueQueue.Empty() && (context.renderFlags & RenderFlags::Opaque)) {
            RenderMeshes(context, context.opaqueQueue);
        }

        // Foliage.
        if ((context.renderFlags & RenderFlags::Foliage)) {
            for (auto [ent, component, foliageArray] : context.scene->Registry().view<core::FoliageComponent, FoliageArray>().each()) {
                UGINE_ASSERT(component.layerCount == foliageArray.layers.size());

                for (uint32_t i = 0; i < component.layerCount; ++i) {
                    const auto& foliage{ foliageArray.layers[i] };
                    const auto& foliageComp{ component.layers[i] };

                    if (foliage && (foliageComp.shadows || !IsShadowRenderPass(context.pass))) {
                        PrepareStencil(context, ent, OPAQUE_STENCIL);
                        foliage.Render(context);
                    }
                }
            }
        }

        // Skybox.
        if ((context.renderFlags & RenderFlags::SkyBox) && (dynamicSky_ || (skyBoxMaterial_ && skyBoxMaterial_->HasPass(context.pass)))) {
            const auto& mesh{ Shapes::Cube() };
            const auto& material{ dynamicSky_ ? *dynamicSkyMaterial_ : *skyBoxMaterial_ };

            auto pipeline{ PipelineCache::Instance().GetPipeline(context, material) };

            device.SetStencilWriteCompareMask(context.command, vk::StencilFaceFlagBits::eFrontAndBack, 0x0, 0x0);
            device.BindPipeline(context.command, pipeline);
            device.BindVertexIndexBuffer(context.command, *mesh->VertexBuffer());
            material.Pass(context.pass).Bind(context.command);
            device.Draw(context.command, mesh->DrawCall());
            device.ResetDescriptor(context.command, MATERIAL_DATASET);
        }

        device.ResetDescriptors(context.command);
    }
}

void Renderer::RenderMeshes(RenderContext& context, const RenderQueue& renderQueue) {
    UGINE_ASSERT(!renderQueue.batches.empty());

    PROFILE_EVENT("Render meshes");
    GPUDebugLabel label(context.command, "Render meshes");

    auto& device{ GraphicsService::Device() };

    // Allocate gpu instance meshes.
    const uint32_t numInstances{ static_cast<uint32_t>(renderQueue.batches.size()) };

    auto gpuAllocation{ device.AllocateGPU(context.command, sizeof(VertexInstance) * numInstances) };
    VertexInstance* instanceMatrices{ reinterpret_cast<VertexInstance*>(gpuAllocation.memory) };

    utils::Vector<RenderInstance> instances;

    {
        PROFILE_EVENT("Merge instances");

        uint32_t instanceCounter{};

        RenderInstance* prevInstance{};
        instances.reserve(numInstances);
        for (const auto& batch : renderQueue.batches) {
            UGINE_ASSERT(batch.entity != entt::null);

            auto go{ context.scene->Get(batch.entity) };
            SetupVertexInstance(instanceMatrices[instanceCounter], go.GlobalTransformation().Matrix());

            RenderInstance instance;
            instance.vboID = MeshMaterials::Get(batch.meshMaterialId).mesh->VertexBufferId();
            instance.instanceCount = 1;
            instance.meshMaterialId = batch.meshMaterialId;
            instance.subMeshId = batch.submeshId;
            instance.lod = batch.lod;
            instance.stencilWrite = StencilValue(context, batch.entity);

            if (prevInstance && prevInstance->CanMerge(instance)) {
                prevInstance->instanceCount += instance.instanceCount;
            } else {
                instances.push_back(instance);
                prevInstance = &instances.back();
            }

            ++instanceCounter;
        }
    }

    {
        PROFILE_EVENT("Draw calls");

        // For each instance:
        uint32_t instanceCounter{};
        for (const auto& instance : instances) {
            // TODO:
            const auto& mesh{ MeshMaterials::Get(instance.meshMaterialId) };
            const auto& material{ MaterialInstances::Get(mesh.submeshes[instance.subMeshId].material.id()) };

            if (!material.HasPass(context.pass)) {
                continue;
            }

            // bind vertex + instance + index buffers.
            const auto& vbo{ VertexBuffers::Get(instance.vboID) };

            vk::Buffer vboBuffers[2] = {
                vbo.VertexBuf().GetBuffer(),
                gpuAllocation.buffer,
            };

            vk::DeviceSize offsets[2] = {
                vbo.VertexBuf().Offset(),
                gpuAllocation.offset + instanceCounter * sizeof(VertexInstance),
            };

            device.BindVertexBuffers(context.command, 2, vboBuffers, offsets);
            device.BindIndexBuffer(context.command, vbo.IndexBuf().GetBuffer(), 0, vbo.IndexType());

            auto pipeline{ PipelineCache::Instance().GetPipeline(context, material) };

            {
                PROFILE_EVENT("BindDraw");

                device.BindPipeline(context.command, pipeline);
                material.Bind(context.pass, context.command);

                device.SetStencilWriteMask(context.command, vk::StencilFaceFlagBits::eFrontAndBack, instance.stencilWrite | context.stencil);

                // TODO: Make common func.
                auto dc{ mesh.mesh->DrawCall(instance.lod) };
                dc.instanceCount = instance.instanceCount;
                dc.indexStart += mesh.submeshes[instance.subMeshId].indexOffset;
                dc.indexCount = mesh.submeshes[instance.subMeshId].indexCount;
                dc.vertexOffset += mesh.submeshes[instance.subMeshId].vertexOffset;

                device.Draw(context.command, dc);
            }

            instanceCounter += instance.instanceCount;

            device.ResetDescriptor(context.command, MATERIAL_DATASET);
        }
    }

    //device.Free(context.command, gpuAllocation);
}

void Renderer::BilateralUpscale(
    RenderContext& context, const TextureViewRef& src, vk::ImageLayout srcLayout, const TextureViewRef& dst, vk::ImageLayout dstLayout) const {

    auto& device{ GraphicsService::Device() };

    // Source to general layout.
    auto& srcTexture{ Textures::Get(TextureViews::TextureID(src.id())) };
    if (srcLayout != vk::ImageLayout::eGeneral) {
        GPUBarrier barrier{ GPUBarrier::Image(srcTexture, vk::AccessFlagBits::eShaderRead, vk::AccessFlagBits::eShaderWrite, srcLayout,
            vk::ImageLayout::eGeneral, vk::ImageAspectFlagBits::eColor) };
        device.Barrier(context.command, barrier);
    }

    // Target to general layout.
    auto& dstTexture{ Textures::Get(TextureViews::TextureID(dst.id())) };
    if (dstLayout != vk::ImageLayout::eGeneral) {
        GPUBarrier barrier{ GPUBarrier::Image(dstTexture, vk::AccessFlagBits::eShaderRead, vk::AccessFlagBits::eShaderWrite, dstLayout,
            vk::ImageLayout::eGeneral, vk::ImageAspectFlagBits::eColor) };
        device.Barrier(context.command, barrier);
    }

    // dispatch.
    auto srcDesc{ src->Descriptor() };
    srcDesc.imageLayout = vk::ImageLayout::eGeneral;

    auto dstDesc{ dst->Descriptor() };
    dstDesc.imageLayout = vk::ImageLayout::eGeneral;

    device.BindPipeline(context.command, &bilateralUpscale_);
    device.BindStorageImage(context.command, COMPUTE_DATASET, 0, srcDesc);
    device.BindStorageImage(context.command, COMPUTE_DATASET, 1, dstDesc);
    device.Dispatch(context.command, uint32_t(std::ceil(dst->Extent().width / float(POSTPROCESS_THREAD_COUNT))),
        uint32_t(std::ceil(dst->Extent().height / float(POSTPROCESS_THREAD_COUNT))), 1);
    device.ResetDescriptor(context.command, COMPUTE_DATASET);

    // Final layout barriers.
    if (srcLayout != vk::ImageLayout::eGeneral) {
        GPUBarrier barrier{ GPUBarrier::Image(srcTexture, vk::AccessFlagBits::eShaderRead, vk::AccessFlagBits::eShaderWrite, vk::ImageLayout::eGeneral,
            srcLayout, vk::ImageAspectFlagBits::eColor) };
        device.Barrier(context.command, barrier);
    }

    if (dstLayout != vk::ImageLayout::eGeneral) {
        GPUBarrier barrier{ GPUBarrier::Image(dstTexture, vk::AccessFlagBits::eShaderWrite, vk::AccessFlagBits::eShaderRead, vk::ImageLayout::eGeneral,
            dstLayout, vk::ImageAspectFlagBits::eColor) };
        device.Barrier(context.command, barrier);
    }

    device.FlushBarriers(context.command);
}

void Renderer::RenderTransparent(RenderContext& context, const RenderTarget& output, const TextureView& volumetrics, const TextureView& sun) {
    PROFILE_EVENT("Render transparent");
    GpuEvent event{ context.command, "Transparent", QUERY_DEFAULT };

    auto& reg{ context.scene->Registry() };

    auto flares{ reg.view<gfx::LensFlare>() };
    auto volumes{ reg.view<gfx::VolumetricFlag>() };

    // TODO: Use barrier instread of render pass if nothing has to be rendered.
    /*
    if ((flares.empty() || (context.renderFlags & RenderFlags::LensFlare) == 0) && (volumes.empty() || (context.renderFlags & RenderFlags::VolumetricLights) == 0)) {
        GPUBarrier barrier{ GPUBarrier::Image(output.Texture(), vk::AccessFlagBits::eColorAttachmentWrite, vk::AccessFlagBits::eShaderRead,
            vk::ImageLayout::eColorAttachmentOptimal, vk::ImageLayout::eShaderReadOnlyOptimal, vk::ImageAspectFlagBits::eColor) };

        GraphicsService::Device().Barrier(context.command, barrier);
        return;
    }
    */

    auto& device{ GraphicsService::Device() };

    GPUDebugLabel label(context.command, "Transparent");

    auto& pass{ RenderPassCache::Transparent(output.Format(), output.Samples()) };
    pass.Begin(context, output);

    // Particles.
    if (context.renderFlags & RenderFlags::Particles) {
        GpuEvent event{ context.command, "Particles render", QUERY_DEFAULT };

        for (auto ent : context.visibility->Particles()) {
            auto go{ context.scene->Get(ent) };
            const auto& particle{ go.Component<Particles>() };

            PrepareStencil(context, go);
            particle.Render(context);
        }
        //device.SetStencilWriteCompareMask(context.command, vk::StencilFaceFlagBits::eFrontAndBack, 0x0, 0x0);
    }

    // VolumetricLights.
    if (!volumes.empty() && (context.renderFlags & RenderFlags::VolumetricLights) != 0) {
        GPUDebugLabel label(context.command, "Volumetrics");

        auto pipeline{ PipelineCache::Instance().GetPipeline(context, *volumetricMat_) };
        device.BindPipeline(context.command, pipeline);
        device.BindImageSampler(context.command, MATERIAL_DATASET, 0, volumetrics.Descriptor());
        context.DrawFullscreen();

        device.ResetDescriptor(context.command, MATERIAL_DATASET);
    }

    // Light shafts.
    if (context.renderFlags & RenderFlags::LightShafts) {
        GPUDebugLabel label(context.command, "LightsShafts");

        auto pipeline{ PipelineCache::Instance().GetPipeline(context, *lightShaftsMaterial_) };
        device.BindPipeline(context.command, pipeline);
        lightShaftsMaterial_->Bind(context.pass, context.command);
        context.DrawFullscreen();

        device.ResetDescriptor(context.command, MATERIAL_DATASET);
    }

    // Lens flares.
    if (!flares.empty() && (context.renderFlags & RenderFlags::LensFlare) != 0) {
        GPUDebugLabel label(context.command, "Flares");
        GpuEvent event{ context.command, "Lens flares", QUERY_DEFAULT };

        auto gpuAllocation{ device.AllocateGPU(context.command, sizeof(VertexInstance) * flares.size()) };
        VertexInstance* instanceMatrices{ reinterpret_cast<VertexInstance*>(gpuAllocation.memory) };

        uint32_t index{};
        for (auto [ent, flare] : flares.each()) {
            const auto& light{ context.scene->Get(ent).Component<core::LightComponent>() };
            UGINE_ASSERT(light.flare);

            auto pipeline{ PipelineCache::Instance().GetPipeline(context, *flare.material) };

            device.BindPipeline(context.command, pipeline);
            flare.material->Bind(context.pass, context.command);

            vk::Buffer vboBuffers[2] = {
                GraphicsService::NullBuffer()->GetBuffer(),
                gpuAllocation.buffer,
            };

            vk::DeviceSize offsets[2] = {
                0,
                gpuAllocation.offset + index * sizeof(VertexInstance),
            };

            device.BindVertexBuffers(context.command, 2, vboBuffers, offsets);

            auto go{ context.scene->Get(ent) };
            if (light.light.type == Light::Type::Directional) {
                auto transform{ go.GlobalTransformation().rotation * glm::vec3{ 0, 0, -context.camera->Far() } };

                SetupVertexInstance(instanceMatrices[index++], glm::translate(glm::mat4{ 1.0f }, context.camera->Position() + transform));
            } else {
                SetupVertexInstance(instanceMatrices[index++], go.GlobalTransformation().Matrix());
            }

            device.Draw(context.command, 3, 1, 0, 0);
        }
    }

    // Transparent.
    if (!context.transparentQueue.Empty() && (context.renderFlags & RenderFlags::Transparent)) {
        RenderMeshes(context, context.transparentQueue);
    }

    pass.End(context);

    device.ResetDescriptor(context.command, MATERIAL_DATASET);
}

void Renderer::RenderDepthPrePass(RenderContext& context, const RenderTarget& depth) {
    PROFILE_EVENT("Depth Z-Pass");
    GpuEvent event{ context.command, "Z-Pass", QUERY_DEFAULT };

    GPUDebugLabel label(context.command, "Depth Z-Pass");
    context.pass = RenderPassType::Depth;
    context.renderFlags = MaskRenderFlags(RenderFlags::Opaque);
    context.stencil = OPAQUE_STENCIL;

    auto& pass{ RenderPassCache::Depth(depth.Samples()) };

    pass.Begin(context, depth);
    BindCommonCB(context.command, context);
    RenderOpaque(context);
    pass.End(context);
}

void Renderer::RenderVolumetricSources(RenderContext& context, const RenderTarget& output, const TextureView& inputDepth) {
    PROFILE_EVENT("Volumetrcs");

    auto& device{ GraphicsService::Device() };

    GPUDebugLabel label(context.command, "Volumetric pass");

    BindCommonCB(context.command, context, {}, inputDepth, {});

    auto& pass{ RenderPassCache::Volumetric() };
    pass.Begin(context, output);

    for (auto [ent, light] : context.scene->Registry().view<core::LightComponent, VolumetricFlag>().each()) {
        auto go{ context.scene->Get(ent) };

        switch (light.light.type) {
        case gfx::Light::Type::Directional: {
            auto pipeline{ PipelineCache::Instance().GetPipeline(context, *dirLightVolMaterial_) };
            device.BindPipeline(context.command, pipeline);
            device.PushConstants(context.command, GPUPushConstant::Data(vk::ShaderStageFlagBits::eFragment, light.lightId));
            device.Draw(context.command, FULLSCREEN_VS_COUNT, 1, 0, 0);
        } break;
        case gfx::Light::Type::Point: {
            auto trans{ go.GlobalTransformation() };
            trans.scale = glm::vec3{ light.light.range };
            const auto mvp{ context.camera->ViewProjectionMatrix() * trans.Matrix() };

            auto pipeline{ PipelineCache::Instance().GetPipeline(context, *pointLightVolMaterial_) };
            device.BindPipeline(context.command, pipeline);
            device.PushConstants(context.command, GPUPushConstant::Data(vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, mvp));
            device.PushConstants(
                context.command, GPUPushConstant::Data(vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, light.lightId, 64));
            device.Draw(context.command, ICOSPHERE_VS_COUNT, 1, 0, 0);
        } break;
        case gfx::Light::Type::Spot: {
            const auto size{ light.light.range * 2.0f * std::tan(glm::radians(light.light.spotAngleDeg)) };
            auto trans{ go.GlobalTransformation() };
            trans.scale = glm::vec3{ size, light.light.range, size };
            trans.rotation *= glm::fquat(glm::vec3{ 0.5f * glm::pi<float>(), 0, 0 }); // TODO: Rotation.
            const auto mvp{ context.camera->ViewProjectionMatrix() * trans.Matrix() };

            auto pipeline{ PipelineCache::Instance().GetPipeline(context, *spotLightVolMaterial_) };
            device.BindPipeline(context.command, pipeline);
            device.PushConstants(context.command, GPUPushConstant::Data(vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, mvp));
            device.PushConstants(
                context.command, GPUPushConstant::Data(vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, light.lightId, 64));
            device.Draw(context.command, CONE_VS_COUNT, 1, 0, 0);
        } break;
        }
    }

    pass.End(context);
}

void Renderer::RenderSun(RenderContext& context, const RenderTarget& intOutput, const RenderTarget& output, const RenderTarget& inputDepth) {
    const auto sunRotation{ SunDirection(context) };
    // TODO: Check.
    //UGINE_DEBUG("Dot: {}", glm::dot(sunRotation, context.camera->Rotation()));
    //if (glm::dot(sunRotation, context.camera->Rotation()) < 0) {
    //    return;
    //}

    PROFILE_EVENT("Sun");

    // TODO: Sun size
    glm::mat4 lightMVP{ context.camera->ViewMatrix()
        * glm::translate(glm::mat4{ 1 }, context.camera->Position() + sunRotation * core::FORWARD * core::ConfigService::LightShaftsSunSize()) };
    lightMVP[0] = glm::vec4{ 1, 0, 0, lightMVP[0][3] };
    lightMVP[1] = glm::vec4{ 0, 1, 0, lightMVP[1][3] };
    lightMVP[2] = glm::vec4{ 0, 0, 1, lightMVP[2][3] };
    lightMVP = context.camera->ProjectionMatrix() * lightMVP;

    auto& device{ GraphicsService::Device() };

    GPUDebugLabel label(context.command, "Sun pass");
    BindCommonCB(context.command, context);

    {
        auto& pass{ RenderPassCache::LightShafts() };
        pass.Begin(context, intOutput, inputDepth);

        device.SetStencilWriteCompareMask(context.command, vk::StencilFaceFlagBits::eFrontAndBack, 0x0, OPAQUE_STENCIL);

        auto pipeline{ PipelineCache::Instance().GetPipeline(context, *sunMaterial_) };
        device.BindPipeline(context.command, pipeline);
        device.PushConstants(context.command, GPUPushConstant::Data(vk::ShaderStageFlagBits::eVertex, lightMVP));
        device.Draw(context.command, 6, 1, 0, 0);

        pass.End(context);
    }

    {
        device.Barrier(context.command,
            GPUBarrier::Image(intOutput.Texture(), //
                vk::AccessFlagBits::eColorAttachmentWrite, //
                vk::AccessFlagBits::eShaderRead, //
                vk::ImageLayout::eGeneral, //
                vk::ImageLayout::eGeneral, //
                vk::ImageAspectFlagBits::eColor));

        device.Barrier(context.command,
            GPUBarrier::Image(output.Texture(), //
                vk::AccessFlagBits::eShaderRead, //
                vk::AccessFlagBits::eShaderWrite, //
                vk::ImageLayout::eShaderReadOnlyOptimal, //
                vk::ImageLayout::eGeneral, //
                vk::ImageAspectFlagBits::eColor));

        const auto light2D{ lightMVP * glm::vec4{ 0, 0, 0, 1 } };
        glm::vec2 lightPos{ light2D.x, light2D.y };
        lightPos = (lightPos / light2D.w) * 0.5f + 0.5f;

        auto mem{ device.AllocateGPU(context.command, sizeof(LightShaftsParams)) };
        //memcpy(mem.memory, &params, sizeof(LightShaftsParams));

        //LightShaftsParams params;
        auto params{ reinterpret_cast<LightShaftsParams*>(mem.memory) };
        params->lightPos = lightPos;
        params->size = core::ConfigService::LightShaftsSize();
        params->weight = core::ConfigService::LightShaftsWeight();
        params->decayFallof = core::ConfigService::LightShaftsDecayFallof();
        params->exposure = core::ConfigService::LightShaftsExposure();

        auto srcDesc{ intOutput.View().Descriptor() };
        srcDesc.imageLayout = vk::ImageLayout::eGeneral;

        auto dstDesc{ output.View().Descriptor() };
        dstDesc.imageLayout = vk::ImageLayout::eGeneral;

        device.BindPipeline(context.command, &lightShafts_);
        device.BindImageSampler(context.command, COMPUTE_DATASET, 0, srcDesc);
        device.BindStorageImage(context.command, COMPUTE_DATASET, 1, dstDesc);
        device.BindUniform(context.command, COMPUTE_DATASET, 2, mem.buffer, mem.offset, sizeof(LightShaftsParams));

        device.Dispatch(context.command, uint32_t(std::ceil(output.Extent().width / float(POSTPROCESS_THREAD_COUNT))),
            uint32_t(std::ceil(output.Extent().height / float(POSTPROCESS_THREAD_COUNT))), 1);

        device.ResetDescriptor(context.command, COMPUTE_DATASET);

        device.Barrier(context.command,
            GPUBarrier::Image(output.Texture(), //
                vk::AccessFlagBits::eShaderWrite, //
                vk::AccessFlagBits::eShaderRead, //
                vk::ImageLayout::eGeneral, //
                vk::ImageLayout::eShaderReadOnlyOptimal, //
                vk::ImageAspectFlagBits::eColor));

        device.FlushBarriers(context.command);

        lightShaftsMaterial_->SetUniformVal("center", lightPos);
        lightShaftsMaterial_->SetUniformVal("radius", core::ConfigService::LightShaftsBlurRadius());
        lightShaftsMaterial_->SetUniformVal("amount", core::ConfigService::LightShaftsBlurAmount());
    }
}

void Renderer::RenderAO(RenderContext& context, const RenderTarget& intOutput, const TextureView& inputDepth, const RenderTarget& output) {
    PROFILE_EVENT("Ambient Occlusion");
    GpuEvent event{ context.command, "SSAO", QUERY_DEFAULT };

    auto& device{ GraphicsService::Device() };

    GPUDebugLabel label(context.command, "SSAO pass");
    BindCommonCB(context.command, context, {}, inputDepth, {});

    // TODO: Make compute?
    {
        ssaoParams_.radius = core::ConfigService::SsaoRadius();
        ssaoParams_.bias = core::ConfigService::SsaoBias();
        memcpy(ssaoCB_.Mapped(), &ssaoParams_, sizeof(SsaoParams));

        auto& pass{ RenderPassCache::Ssao() };
        pass.Begin(context, intOutput);

        auto pipeline{ PipelineCache::Instance().GetPipeline(context, *ssaoMaterial_) };
        device.BindPipeline(context.command, pipeline);
        device.BindUniform(context.command, MATERIAL_DATASET, 0, ssaoCB_.Descriptor());
        device.Draw(context.command, 6, 1, 0, 0);

        pass.End(context);
    }

    {
        device.Barrier(context.command,
            GPUBarrier::Image(intOutput.Texture(), //
                vk::AccessFlagBits::eColorAttachmentWrite, //
                vk::AccessFlagBits::eShaderRead, //
                vk::ImageLayout::eGeneral, //
                vk::ImageLayout::eGeneral, //
                vk::ImageAspectFlagBits::eColor));

        device.Barrier(context.command,
            GPUBarrier::Image(output.Texture(), //
                vk::AccessFlagBits::eShaderRead, //
                vk::AccessFlagBits::eShaderWrite, //
                vk::ImageLayout::eShaderReadOnlyOptimal, //
                vk::ImageLayout::eGeneral, //
                vk::ImageAspectFlagBits::eColor));

        auto srcDesc{ intOutput.View().Descriptor() };
        srcDesc.imageLayout = vk::ImageLayout::eGeneral;

        auto dstDesc{ output.View().Descriptor() };
        dstDesc.imageLayout = vk::ImageLayout::eGeneral;

        device.BindPipeline(context.command, &blur_);
        device.BindStorageImage(context.command, COMPUTE_DATASET, 0, srcDesc);
        device.BindStorageImage(context.command, COMPUTE_DATASET, 1, dstDesc);

        device.Dispatch(context.command, uint32_t(std::ceil(output.Extent().width / float(POSTPROCESS_THREAD_COUNT))),
            uint32_t(std::ceil(output.Extent().height / float(POSTPROCESS_THREAD_COUNT))), 1);

        device.ResetDescriptor(context.command, COMPUTE_DATASET);

        device.Barrier(context.command,
            GPUBarrier::Image(output.Texture(), //
                vk::AccessFlagBits::eShaderWrite, //
                vk::AccessFlagBits::eShaderRead, //
                vk::ImageLayout::eGeneral, //
                vk::ImageLayout::eShaderReadOnlyOptimal, //
                vk::ImageAspectFlagBits::eColor));

        device.FlushBarriers(context.command);
    }
}

void Renderer::CalcVisibility(core::Scene& scene, const core::Camera& camera, RenderPassType pass, Visibility& visibility) const {
    PROFILE_EVENT("Visibility");

    visibility.SetMeshes(scene.Registry().size<core::MeshComponent>());
    visibility.SetSkinnedMeshes(scene.Registry().size<core::SkinnedMeshComponent>());
    visibility.SetParticles(scene.Registry().size<core::ParticleComponent>());
    visibility.SetTerrains(scene.Registry().size<core::TerrainComponent>());

    auto terrains{ scene.Registry().view<core::TerrainComponent>() };
    auto meshes{ scene.Registry().view<core::MeshComponent>() };
    auto skinnedMeshes{ scene.Registry().view<core::SkinnedMeshComponent>() };
    auto particles{ scene.Registry().view<core::ParticleComponent>() };

    enki::TaskSet terrainsTask(static_cast<uint32_t>(terrains.size()), [&](enki::TaskSetPartition range, uint32_t threadnum) {
        //PROFILE_EVENT("Terrain visibility");

        for (uint32_t i = range.start; i < range.end; ++i) {
            auto go{ scene.Get(terrains[i]) };
            auto& terrain{ go.Component<gfx::Terrain>() };

            if (go.IsEnabled() && terrain.Material().HasPass(pass)) {
                visibility.CheckTerrain(go.Component<core::TerrainComponent>().aabb, terrains[i]);
            }
        }
    });

    core::Scheduler::Schedule(&terrainsTask);

    enki::TaskSet meshTask(static_cast<uint32_t>(meshes.size()), [&](enki::TaskSetPartition range, uint32_t threadnum) {
        //PROFILE_EVENT("Mesh visibility");

        for (uint32_t i = range.start; i < range.end; ++i) {
            auto ent{ meshes[i] };
            auto go{ scene.Get(ent) };
            const auto& mesh{ go.Component<core::MeshComponent>() };

            // TODO: Check submeshes.
            if (go.IsEnabled() && mesh.render && mesh.meshMaterial && mesh.meshMaterial->submeshes[0].HasPass(pass)
                && (!IsShadowRenderPass(pass) || mesh.castsShadows)) {
                visibility.CheckMesh(mesh.aabb, ent, static_cast<uint32_t>(mesh.meshMaterial->submeshes.size()));
            }
        }
    });

    core::Scheduler::Schedule(&meshTask);

    enki::TaskSet skinMeshTask(static_cast<uint32_t>(skinnedMeshes.size()), [&](enki::TaskSetPartition range, uint32_t threadnum) {
        //PROFILE_EVENT("Skin mesh visibility");

        for (uint32_t i = range.start; i < range.end; ++i) {
            auto ent{ skinnedMeshes[i] };
            auto go{ scene.Get(ent) };
            const auto& mesh{ go.Component<core::SkinnedMeshComponent>() };

            if (go.IsEnabled() && mesh.render && mesh.mesh && mesh.mesh.MeshCount() > 0 && (!IsShadowRenderPass(pass) || mesh.castsShadows)) {
                visibility.CheckSkinnedMesh(mesh.aabb, ent, mesh.mesh.MeshCount());
            }
        }
    });

    core::Scheduler::Schedule(&skinMeshTask);

    enki::TaskSet particleTask(static_cast<uint32_t>(particles.size()), [&](enki::TaskSetPartition range, uint32_t threadnum) {
        //PROFILE_EVENT("Particle visibility");

        for (uint32_t i = range.start; i < range.end; ++i) {
            auto ent{ particles[i] };
            auto go{ scene.Get(ent) };
            const auto& particle{ go.Component<core::ParticleComponent>() };

            if (go.IsEnabled()) {
                visibility.CheckParticles(particle.aabb, ent);
            }
        }
    });

    core::Scheduler::Schedule(&particleTask);

    core::Scheduler::WaitFor(&terrainsTask);
    core::Scheduler::WaitFor(&meshTask);
    core::Scheduler::WaitFor(&skinMeshTask);
    core::Scheduler::WaitFor(&particleTask);

    visibility.Finalize();
}

void Renderer::PopulateRenderQueue(const RenderContext& context, RenderQueue& opaqueQueue, RenderQueue& transparentQueue) const {
    PROFILE_EVENT("Fill queue");

    std::atomic_uint32_t opaqueCnt{};
    std::atomic_uint32_t transparentCnt{};

    const auto meshCount{ static_cast<uint32_t>(context.visibility->Meshes().size()) };
    const auto meshSubmeshCount{ context.visibility->MeshesSubmeshSize() };

    const auto skinMeshCount{ static_cast<uint32_t>(context.visibility->SkinnedMeshes().size()) };
    const auto skinMeshSubmeshCount{ context.visibility->SkinnedMeshesSubmeshSize() };
    const auto terrainCount{ static_cast<uint32_t>(context.visibility->Terrains().size()) };

    const auto count{ terrainCount + meshCount + skinMeshCount };
    const auto submeshCount{ terrainCount + meshSubmeshCount + skinMeshSubmeshCount };

    opaqueQueue.batches.resize(submeshCount);
    transparentQueue.batches.resize(submeshCount);

    enki::TaskSet task{ count,
        [&](enki::TaskSetPartition range, uint32_t threadnum) {
            PROFILE_EVENT("Collect meshes");

            for (uint32_t i = range.start; i < range.end; ++i) {
                if (i < terrainCount) {
                    // Terrain.
                    const auto ent{ context.visibility->Terrains()[i] };
                    auto go{ context.scene->Get(ent) };

                    const auto& terrainComp{ go.Component<core::TerrainComponent>() };
                    const auto& terrain{ go.Component<gfx::Terrain>() };
                    float distance{ glm::length(go.GlobalTransformation().position - context.camera->Position()) };

                    auto index{ opaqueCnt.fetch_add(1) };
                    opaqueQueue.batches[index] = RenderBatch::Create(ent, terrain.MeshMaterialID(), 0, distance, 0);
                } else if (i < terrainCount + meshCount) {
                    // Mesh.
                    const auto& sm{ context.visibility->Meshes()[i - terrainCount] };
                    auto go{ context.scene->Get(sm.ent) };

                    const auto& meshComp{ go.Component<core::MeshComponent>() };
                    const auto distance{ glm::length(go.GlobalTransformation().position - context.camera->Position()) };
                    const auto lod{ std::min(meshComp.lod < 0 ? math::CalcLod(distance) : meshComp.lod, /*meshComp.mesh->LodLevels() - 1*/ 1) }; // TODO:

                    uint32_t index{};
                    RenderQueue* queue{};

                    const uint32_t count{ meshComp.debugSubmesh < 0 ? sm.subMeshCount : 1 };

                    if (meshComp.meshMaterial->Transparent()) {
                        if ((context.renderFlags & RenderFlags::Transparent) == 0) {
                            continue;
                        }

                        index = transparentCnt.fetch_add(count);
                        queue = &transparentQueue;
                    } else {
                        if ((context.renderFlags & RenderFlags::Opaque) == 0) {
                            continue;
                        }

                        index = opaqueCnt.fetch_add(count);
                        queue = &opaqueQueue;
                    }

                    for (uint32_t j{ 0 }; j < count; ++j) {
                        const auto submesh{ meshComp.debugSubmesh < 0 ? j : std::min<int>(meshComp.debugSubmesh, sm.subMeshCount - 1) };
                        queue->batches[index + j] = RenderBatch::Create(sm.ent, meshComp.meshMaterial.id(), submesh, distance, lod);
                    }
                } else {
                    // Skinned mesh.
                    const auto idx{ i - meshCount - terrainCount };

                    const auto& sm{ context.visibility->SkinnedMeshes()[idx] };
                    auto go{ context.scene->Get(sm.ent) };

                    const auto& meshComp{ go.Component<core::SkinnedMeshComponent>() };
                    const auto distance{ glm::length(go.GlobalTransformation().position - context.camera->Position()) };
                    const auto lod{ std::min(meshComp.lod < 0 ? math::CalcLod(distance) : meshComp.lod, meshComp.mesh.LodLevels() - 1) };

                    uint32_t index{};
                    RenderQueue* queue{};

                    if (meshComp.mesh.MeshMaterial().Transparent()) {
                        if ((context.renderFlags & RenderFlags::Transparent) == 0) {
                            continue;
                        }

                        index = transparentCnt.fetch_add(sm.subMeshCount);
                        queue = &transparentQueue;
                    } else {
                        if ((context.renderFlags & RenderFlags::Opaque) == 0) {
                            continue;
                        }

                        index = opaqueCnt.fetch_add(sm.subMeshCount);
                        queue = &opaqueQueue;
                    }

                    for (uint32_t j{ 0 }; j < sm.subMeshCount; ++j) {
                        queue->batches[index + j] = RenderBatch::Create(sm.ent, meshComp.mesh.MeshMaterialID(), j, distance, lod);
                    }
                }
            }
        } };

    core::Scheduler::Schedule(&task);
    core::Scheduler::WaitFor(&task);

    opaqueQueue.batches.resize(opaqueCnt);
    transparentQueue.batches.resize(transparentCnt);

    {
        PROFILE_EVENT("Sort queues");
        if (context.renderFlags & RenderFlags::Opaque) {
            opaqueQueue.Sort(RenderQueue::SortBy::FrontToBack);
        }

        if (context.renderFlags & RenderFlags::Transparent) {
            transparentQueue.Sort(RenderQueue::SortBy::BackToFront);
        }
    }
}

void Renderer::RenderOpaque(RenderContext& context, const RenderTarget& output, const RenderTarget& depth, const TextureView& ao) {
    PROFILE_EVENT("ForwardOpaque");
    GPUDebugLabel label(context.command, "Opaque pass");

    auto& pass{ RenderPassCache::Opaque(output.Format(), output.Samples()) };

    context.pass = RenderPassType::Main;
    context.renderFlags = MaskRenderFlags(RenderFlags::All);

    // TODO: Move to visibility?
    auto foliageArray{ context.scene->Registry().view<FoliageArray>() };

    BindCommonCB(context.command, context, ao);

    if (!foliageArray.empty()) {
        PROFILE_EVENT("Foliage update");
        GpuEvent event{ context.command, "Foliage update", QUERY_DEFAULT };

        for (auto [ent, foliageA] : foliageArray.each()) {
            for (auto& foliage : foliageA.layers) {
                if (foliage) {
                    foliage.Update(context, context.scene->Get(ent).GlobalTransformation().Matrix());
                }
            }
        }
    }

    {
        GpuEvent event{ context.command, "Opaque", QUERY_DEFAULT };

        pass.Begin(context, output, depth);
        RenderOpaque(context);
        pass.End(context);
    }
}

void Renderer::RenderShadows(RenderContext& context, core::Scheduler::Group& grp) {
    for (const auto& entry : context.lightCounter.LightShadows()) {
        auto go{ context.scene->Get(entry.ent) };
        auto& light{ go.Component<core::LightComponent>() };

        UGINE_ASSERT(light.generatesShadows);

        switch (light.light.type) {
        case Light::Type::Directional:
            for (int i = 0; i < (light.isCsm ? limits::CSM_LEVELS : 1); ++i) {
                auto cmd{ GraphicsService::Device().BeginCommandList() };

                core::Scheduler::Schedule(grp, [this, context, cmd, light, entry, i]() {
                    RenderContext ctx{ context };
                    ctx.pass = RenderPassType::Shadows;
                    ctx.command = cmd;
                    ctx.renderFlags = MaskRenderFlags(RenderFlags::Opaque | RenderFlags::Foliage);
                    ctx.SetCamera(&light.shadowMapCamera[i]);
                    RenderShadowDir(ctx, entry, i);
                });
            }
            break;

        case Light::Type::Spot: {
            auto cmd{ GraphicsService::Device().BeginCommandList() };

            core::Scheduler::Schedule(grp, [this, context, cmd, light, entry]() {
                RenderContext ctx{ context };
                ctx.pass = RenderPassType::Shadows;
                ctx.command = cmd;
                ctx.renderFlags = MaskRenderFlags(RenderFlags::Opaque | RenderFlags::Foliage);
                ctx.SetCamera(&light.shadowMapCamera[0]);
                RenderShadowSpot(ctx, entry);
            });
        } break;

        case Light::Type::Point: {
            auto cmd{ GraphicsService::Device().BeginCommandList() };

            core::Scheduler::Schedule(grp, [this, context, cmd, go, light, entry]() {
                RenderContext ctx{ context };
                ctx.pass = RenderPassType::ShadowsCube;
                ctx.command = cmd;
                ctx.renderFlags = MaskRenderFlags(RenderFlags::Opaque | RenderFlags::Foliage);

                RenderShadowPoint(ctx, go, light, entry);
            });
        } break;
        }
    }
}

void Renderer::RenderShadowDir(RenderContext& context, const LightCounter::LightShadow& entry, int csm) {
    PROFILE_EVENT("Dir ShadowPass");
    GPUDebugLabel label(context.command, "DirLight Shadow pass");
    GpuEvent event{ context.command, "Shadow (Dir)", QUERY_DEFAULT };

    Visibility visibility;
    visibility.SetCamera(*context.camera, false);
    context.visibility = &visibility;

    CalcVisibility(*context.scene, *context.camera, context.pass, visibility);
    PopulateRenderQueue(context, context.opaqueQueue, context.transparentQueue);

    auto& shadowPass{ RenderPassCache::Shadow() };

    shadowPass.Begin(context, shadowMaps_->DirShadowMapCsm(entry.shadowIndex, csm));
    BindCommonCB(context.command, context);
    RenderOpaque(context);
    shadowPass.End(context);
}

void Renderer::RenderShadowSpot(RenderContext& context, const LightCounter::LightShadow& entry) {
    PROFILE_EVENT("Spot ShadowPass");
    GpuEvent event{ context.command, "Shadow (Spot)", QUERY_DEFAULT };

    GPUDebugLabel label(context.command, "SpotLight Shadow pass");

    Visibility visibility;
    visibility.SetCamera(*context.camera, false);
    context.visibility = &visibility;

    CalcVisibility(*context.scene, *context.camera, context.pass, visibility);
    PopulateRenderQueue(context, context.opaqueQueue, context.transparentQueue);

    auto& shadowPass{ RenderPassCache::Shadow() };

    shadowPass.Begin(context, shadowMaps_->SpotShadowMap(entry.shadowIndex));
    BindCommonCB(context.command, context);
    RenderOpaque(context);
    shadowPass.End(context);
}

void Renderer::RenderShadowPoint(
    RenderContext& context, const core::GameObject& go, const core::LightComponent& light, const LightCounter::LightShadow& entry) {
    PROFILE_EVENT("Point ShadowPass");
    GPUDebugLabel label(context.command, "PointLight Shadow pass");
    GpuEvent event{ context.command, "Shadow (Point)", QUERY_DEFAULT };

    auto cameraPos{ go.GlobalTransformation().position };
    auto& shadowPass{ RenderPassCache::Shadow() };

    for (uint32_t face = 0; face < 6; ++face) {
        glm::mat4 viewMatrix{};
        switch (face) {
        case 0: // posx
            viewMatrix = glm::lookAt(cameraPos, cameraPos + glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f));
            break;
        case 1: // negx
            viewMatrix = glm::lookAt(cameraPos, cameraPos + glm::vec3(-1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f));
            break;
        case 2: // posy
            viewMatrix = glm::lookAt(cameraPos, cameraPos + glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
            break;
        case 3: // negy
            viewMatrix = glm::lookAt(cameraPos, cameraPos + glm::vec3(0.0f, -1.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f));
            break;
        case 4: // posz
            viewMatrix = glm::lookAt(cameraPos, cameraPos + glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, -1.0f, 0.0f));
            break;
        case 5: // negz
            viewMatrix = glm::lookAt(cameraPos, cameraPos + glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, -1.0f, 0.0f));
            break;
        }

        auto camera{ light.shadowMapCamera[0] };
        camera.SetViewMatrix(viewMatrix);
        context.SetCamera(&camera);

        Visibility visibility;
        visibility.SetCamera(*context.camera, true, light.light.range);
        context.visibility = &visibility;

        CalcVisibility(*context.scene, *context.camera, context.pass, visibility);
        PopulateRenderQueue(context, context.opaqueQueue, context.transparentQueue);

        shadowPass.BeginPoint(context, shadowMaps_->PointShadowMapFace(entry.shadowIndex, face), shadowMaps_->PointShadowMapDepth(entry.shadowIndex));
        BindCommonCB(context.command, context);
        RenderOpaque(context);
        shadowPass.End(context);
    }
}

void Renderer::RenderCamera(RenderContext& context, core::CameraComponent& camera, core::Scheduler::Group& grp) {
    if (!camera.renderTarget) {
        return;
    }

    PROFILE_EVENT("Camera render");

    context.SetCamera(&camera.camera, camera.viewProjectionPrev);

    Visibility visibility;
    visibility.SetCamera(camera.camera, false);
    CalcVisibility(*context.scene, *context.camera, RenderPassType::Main, visibility);
    context.visibility = &visibility;

    PopulateRenderQueue(context, context.opaqueQueue, context.transparentQueue);

    // TODO:
    if (context.renderFlags & RenderFlags::LightShafts) {
        lightShaftsMaterial_->Default().SetTexture("inputTexture", camera.sunRToutput.ViewRef());
    }

    context.pass = RenderPassType::Depth;

    { // Depth prepass.
        auto cmd{ GraphicsService::Device().BeginCommandList() };

        core::Scheduler::Schedule(grp, [&, context, cmd]() {
            RenderContext ctx{ context };
            ctx.command = cmd;

            RenderDepthPrePass(ctx, camera.depthBuffer);

            // Copy depth
            // TODO: Generate linear depth while you're at it.
            //auto depthCopyTextureID{ TextureViews::TextureID(camera.depthCopy.id()) };
            //auto& depthTextureCopy{ Textures::Get(depthCopyTextureID) };
            //camera.depthBuffer.Texture().CopyTo(context.command, depthTextureCopy, vk::ImageLayout::eTransferSrcOptimal,
            //    vk::ImageLayout::eShaderReadOnlyOptimal, vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil, vk::Filter::eNearest);

            //GPUBarrier barrier{ GPUBarrier::Image(camera.depthBuffer.Texture(), vk::AccessFlagBits::eTransferRead,
            //    vk::AccessFlagBits::eDepthStencilAttachmentRead | vk::AccessFlagBits::eDepthStencilAttachmentWrite, vk::ImageLayout::eTransferSrcOptimal,
            //    vk::ImageLayout::eDepthStencilAttachmentOptimal, vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil) };
            //GraphicsService::Device().Barrier(context.command, barrier);
            //GraphicsService::Device().FlushBarriers(context.command);
        });
    }

    context.pass = RenderPassType::Main;

    // TODO: Custom pas?
    if (context.renderFlags & RenderFlags::AmbientOcclusion) {
        // Screen-space effects that are needed before main pass.
        auto cmd{ GraphicsService::Device().BeginCommandList() };

        core::Scheduler::Schedule(grp, [&, context, cmd]() {
            RenderContext ctx{ context };
            ctx.command = cmd;

            RenderAO(ctx, camera.aoRTtmp, camera.depthBuffer.View(), camera.aoRT);
        });
    }

    { // Opaque rendering.
        auto cmd{ GraphicsService::Device().BeginCommandList() };
        core::Scheduler::Schedule(grp, [this, &camera, context, cmd]() {
            RenderContext ctx{ context };
            ctx.command = cmd;

            RenderOpaque(ctx, camera.renderTarget, camera.depthBuffer, camera.aoRT.View());

            // TODO:
            // Scene copy.
            //auto& texture{ Textures::Get(TextureViews::TextureID(camera.sceneCopy.id())) };
            //output.Texture().CopyTo(ctx.command, texture, vk::ImageLayout::eColorAttachmentOptimal, vk::ImageLayout::eShaderReadOnlyOptimal,
            //    vk::ImageAspectFlagBits::eColor, vk::Filter::eNearest);
        });
    }

    { // Volumetric light scattering + light shafts.
        auto cmd{ GraphicsService::Device().BeginCommandList() };

        core::Scheduler::Schedule(grp, [&, context, cmd]() {
            RenderContext ctx{ context };
            ctx.command = cmd;

            if ((context.renderFlags & RenderFlags::VolumetricLights) && !context.scene->Registry().view<gfx::VolumetricFlag>().empty()) {
                GpuEvent event{ context.command, "Volumetrics", QUERY_DEFAULT };

                RenderVolumetricSources(ctx, camera.volumetricLightRT, camera.depthBuffer.View());

                BilateralUpscale(
                    ctx, camera.volumetricLightRT.ViewRef(), vk::ImageLayout::eGeneral, camera.volumetricUpscale, vk::ImageLayout::eShaderReadOnlyOptimal);
            }

            if (context.renderFlags & RenderFlags::LightShafts) {
                GpuEvent event{ context.command, "Light shafts", QUERY_DEFAULT };

                RenderSun(ctx, camera.sunRTstencil, camera.sunRToutput, camera.depthBuffer);
            }
        });
    }

    { // Transparent + FX + Post process.
        auto cmd{ GraphicsService::Device().BeginCommandList() };
        BindCommonCB(cmd, context, camera.aoRT.View(), camera.depthBuffer.View(), *camera.sceneCopy);

        RenderContext ctx{ context };
        ctx.command = cmd;
        ctx.pass = RenderPassType::Main;

        RenderTransparent(ctx, //
            camera.renderTarget, //
            *camera.volumetricUpscale, //
            camera.sunRToutput.View());

        if (!camera.postprocess.Empty()) {
            ctx.pass = RenderPassType::Postprocess;

            camera.postprocess.Render(ctx, camera.renderTarget.ViewRef(), camera.renderTarget);
        }

        gfx::GpuProfiler::Instance().EndQuery(ctx.command, frameQuery_);
        //});
    }

    {
        PROFILE_EVENT("Wait group");

        core::Scheduler::Wait(grp);
    }
}

void Renderer::Render(core::Scene& scene, GPUCommandList command) {
    PROFILE_EVENT("Render");

    gfx::GpuProfiler::Instance().BeginFrame(command);

    // TODO:
    frameQuery_ = gfx::GpuProfiler::Instance().BeginQuery(command, "Frame", QUERY_FRAME);

    auto beginDrawCalls{ FrameStats::Instance().drawCallsIssued.load() };

    auto frameStartTime{ Clock::now() };

    if (scene.SkyBox() != skyBox_ || scene.DynamicSky() != dynamicSky_) {
        dynamicSky_ = scene.DynamicSky();
        if (scene.DynamicSky()) {
            if (!dynamicSkyMaterial_) {
                dynamicSkyMaterial_ = MaterialService::InstantiateByPath("assets/materials/sky.mat");
            }

            // TODO: Dynamic sky IBL.
            lightShading_.ResetIbl();
        } else if (scene.SkyBox()) {
            UpdateSkyBox(scene);

            // TODO:
            command = GraphicsService::Device().BeginCommandList();
        }
    }

    RenderContext context;
    context.scene = &scene;
    context.command = command;
    context.renderFlags = renderFlags_;

    {
        FrameCB cb{};
        SetupFrameCB(cb);
        cb.windDirection = glm::vec3{ 1.0f, 0.0f, 0.0f };
        cb.windRandomness = 1; //math::RandomFloat();
        cb.windSpeed = 0.5f;
        cb.windWaveSize = 0.1f;
        cb.sunPosition = glm::normalize(SunDirection(context) * core::FORWARD);

        memcpy(frameCB_.Mapped(), &cb, std::min(frameCB_.Size(), sizeof(FrameCB)));
    }

    // TODO: Update only if neccessary.
    CollectLights(context);

    auto mainCam{ scene.Camera() };

    core::Scheduler::Group grp;

    // Shadows.
    if (lightShading_.Data().shadowCount > 0) {
        RenderShadows(context, grp);
    }

    // Cameras.
    core::CameraComponent* mainCamera{};
    for (const auto& [cameraEnt, camera] : context.scene->Registry().view<core::CameraComponent>().each()) {
        if (camera.renderTarget) {
            if (camera.samples != camera.renderTarget.Samples()) {
                UGINE_WARN("Mismatch between samples of camera and render target.");

                camera.samples = camera.renderTarget.Samples();
            }

            if (camera.camera.Width() != static_cast<float>(camera.renderTarget.Width())
                || camera.camera.Height() != static_cast<float>(camera.renderTarget.Height())) {
                camera.camera.SetSize(static_cast<float>(camera.renderTarget.Width()), static_cast<float>(camera.renderTarget.Height()));
            }
        }

        if (camera.isMain) {
            // Render main camera last.
            mainCamera = &camera;
        } else {
            RenderCamera(context, camera, grp);
        }
    }

    // Main Camera.
    if (mainCamera) {
        RenderCamera(context, *mainCamera, grp);
    }

    core::Scheduler::Wait(grp);

    FrameStats::Instance().drawCallsScene = FrameStats::Instance().drawCallsIssued.load() - beginDrawCalls;
    FrameStats::Instance().cpuRenderTimeMS
        = static_cast<float>(std::chrono::duration_cast<std::chrono::microseconds>(Clock::now() - frameStartTime).count()) / 1000.0f;
}

} // namespace ugine::gfx
