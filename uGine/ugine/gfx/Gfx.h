﻿#pragma once

// TODO: ??
#include <optional>

#include "DrawCall.h"
#include "Graphics.h"
#include "GraphicsService.h"
#include "Limits.h"
#include "Presenter.h"

namespace ugine::gfx {

namespace config {
    static inline constexpr vk::Format DefaultFormat{ vk::Format::eR32G32B32A32Sfloat };
    //static inline constexpr vk::Format SunFormat{ vk::Format::eR8G8B8A8Unorm };
    static inline constexpr vk::Format SunFormat{ vk::Format::eR32G32B32A32Sfloat };
    static inline constexpr vk::Format SsaoFormat{ vk::Format::eR32Sfloat };
    static inline constexpr vk::Format PointShadowMapFormat{ vk::Format::eR32Sfloat };
    static inline constexpr vk::Format PostprocessFormat{ vk::Format::eR32G32B32A32Sfloat };
    static inline constexpr vk::Format VolumetricLightFormat{ vk::Format::eR16G16B16A16Sfloat };
    static inline constexpr float VolumentricLightDownsample{ 0.25f };
} // namespace config

class RenderContext;
} // namespace ugine::gfx
