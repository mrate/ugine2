﻿#pragma once

#include <vulkan/vulkan.hpp>

#include <memory>

namespace ugine::gfx {

class Texture;

} // namespace ugine::gfx
