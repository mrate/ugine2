﻿#pragma once

#include <ugine/gfx/core/Buffer.h>
#include <ugine/gfx/core/Texture.h>
#include <ugine/gfx/core/VertexBuffer.h>
#include <ugine/utils/Storage.h>

namespace ugine::gfx {

using Textures = utils::SafeDeleteStorage<Texture>;
using TextureRef = Textures::Ref;
using TextureID = Textures::Id;

class TextureViews : public utils::Storage<TextureView, TextureViews> {
public:
    using Base = utils::Storage<TextureView, TextureViews>;

    static Ref Add(const char* file, int line, const TextureRef& texture, TextureView&& view) {
        auto ref{ Base::Add(std::move(view), file, line) };

        static_cast<TextureViews&>(Instance()).storage_.emplace<gfx::TextureRef>(ref.id(), texture);

        return Ref{ std::move(ref) };
    }

    static gfx::TextureRef TextureRef(Id view) {
        UGINE_ASSERT(static_cast<TextureViews&>(Instance()).storage_.has<gfx::TextureRef>(view));

        return static_cast<TextureViews&>(Instance()).storage_.get<gfx::TextureRef>(view);
    }

    static TextureID TextureID(Id view) {
        UGINE_ASSERT(static_cast<TextureViews&>(Instance()).storage_.has<gfx::TextureRef>(view));

        return static_cast<TextureViews&>(Instance()).storage_.get<gfx::TextureRef>(view).id();
    }

    static gfx::TextureRef TextureRef(const Ref& view) {
        return TextureRef(view.id());
    }

    void PreDestroy(Id id) override {
        auto view{ std::move(static_cast<TextureViews&>(Instance()).storage_.get<TextureView>(id)) };
        SAFE_DELETE(view);
    }
};

using TextureViewRef = TextureViews::Ref;
using TextureViewID = TextureViews::Id;

using Buffers = utils::SafeDeleteStorage<Buffer>;
using BufferRef = Buffers::Ref;
using BufferID = Buffers::Id;

using VertexBuffers = utils::SafeDeleteStorage<VertexBuffer>;
using VertexBufferRef = VertexBuffers::Ref;
using VertexBufferID = VertexBuffers::Id;

#define TEXTURE_ADD(...) STORAGE_ADD(Textures, __VA_ARGS__)
#define TEXTURE_VIEW_ADD(...) STORAGE_ADD(TextureViews, __VA_ARGS__)

#define BUFFER_ADD(...) STORAGE_ADD(Buffers, __VA_ARGS__)
#define VERTEX_BUFFER_ADD(...) STORAGE_ADD(VertexBuffers, __VA_ARGS__)

} // namespace ugine::gfx
