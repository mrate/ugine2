﻿#include "ShaderBindings.h"

#include <ugine/core/Camera.h>
#include <ugine/core/ConfigService.h>

namespace ugine::gfx {

void SetupFrameCB(FrameCB& cb) {
    cb.time = float(core::CoreService::Time()) / 1000.0f;
    cb.timeDiff = core::CoreService::TimeDiffS();
    cb.csm.x = core::ConfigService::DirLightCsmSize(0);
    cb.csm.y = core::ConfigService::DirLightCsmSize(1);
    cb.csm.z = core::ConfigService::DirLightCsmSize(2);
    cb.volumetricIntensity = core::ConfigService::VolumetricIntensity();
}

void SetupFrameCB(Buffer& buffer) {
    FrameCB cb;
    SetupFrameCB(cb);

    memcpy(buffer.Mapped(), &cb, std::min(buffer.Size(), sizeof(FrameCB)));
}

void SetupCameraCB(CameraCB& cb, const core::Camera& camera, const glm::mat4& viewProjPrev) {
    cb.viewProj = camera.ViewProjectionMatrix();
    cb.view = camera.ViewMatrix();
    cb.proj = camera.ProjectionMatrix();
    cb.invView = glm::inverse(cb.view);
    cb.invProj = glm::inverse(cb.proj);
    cb.viewProjPrev = viewProjPrev;
    cb.cameraPos = camera.Position();
    cb.cameraNear = camera.Near();
    cb.cameraFar = camera.Far();
    cb.resolution.x = static_cast<uint32_t>(camera.Width());
    cb.resolution.y = static_cast<uint32_t>(camera.Height());
}

void SetupCameraCB(Buffer& buffer, const core::Camera& camera, const glm::mat4& viewProjPrev) {
    CameraCB cb;
    SetupCameraCB(cb, camera, viewProjPrev);

    memcpy(buffer.Mapped(), &cb, std::min(buffer.Size(), sizeof(CameraCB)));
}

Buffer CreateCB(size_t size) {
    return Buffer(size, vk::BufferUsageFlagBits::eUniformBuffer | vk::BufferUsageFlagBits::eTransferDst,
        vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCached);
}

} // namespace ugine::gfx
