﻿#include "FramebufferCache.h"

#include <ugine/utils/Hash.h>

namespace ugine::gfx {

vk::Framebuffer FramebufferCache::GetOrCreateFramebuffer(const FramebufferKey& key) {
    auto it{ framebuffers_.find(key) };
    if (it == framebuffers_.end()) {
        vk::FramebufferCreateInfo framebufferCI{};
        framebufferCI.renderPass = key.renderpass;
        framebufferCI.attachmentCount = key.attachmentCount;
        framebufferCI.pAttachments = key.attachments.data();
        framebufferCI.width = key.width;
        framebufferCI.height = key.height;
        framebufferCI.layers = 1;

        auto fb{ GraphicsService::Graphics().Device().createFramebufferUnique(framebufferCI) };
        it = framebuffers_.insert(std::make_pair(key, std::move(fb))).first;
    }

    return *it->second;
}

void FramebufferCache::Init() {}

void FramebufferCache::Destroy() {
    Instance().framebuffers_.clear();
}

} // namespace ugine::gfx
