#pragma once

#include <ugine/gfx/rendering/MaterialInstance.h>

namespace ugine::gfx {

struct LensFlare {
    MaterialInstanceRef material;
};

struct VolumetricFlag {};

struct ShadowFlag {};

} // namespace ugine::gfx
