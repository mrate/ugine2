﻿#pragma once

#include <ugine/utils/Singleton.h>

#include <ugine/gfx/pass/DepthPass.h>
#include <ugine/gfx/pass/OpaquePass.h>
#include <ugine/gfx/pass/PostprocessPass.h>
#include <ugine/gfx/pass/ShadowPass.h>
#include <ugine/gfx/pass/TransparentPass.h>
#include <ugine/gfx/pass/VolumetricLightPass.h>

#include <map>
#include <tuple>

namespace ugine::gfx {

class RenderPassCache : public utils::Singleton<RenderPassCache> {
public:
    static void Init();
    static void Destroy();

    static const OpaquePass& Opaque(vk::Format format, vk::SampleCountFlagBits samples = vk::SampleCountFlagBits::e1) {
        return Instance().Get(Instance().opaque_, samples, format);
    }

    static const DepthPass& Depth(vk::SampleCountFlagBits samples) {
        return Instance().Get(Instance().depth_, samples);
    }

    static const TransparentPass& Transparent(vk::Format format, vk::SampleCountFlagBits samples = vk::SampleCountFlagBits::e1) {
        return Instance().Get(Instance().transparent_, samples, format);
    }

    static const ShadowPass& Shadow() {
        return Instance().Get(Instance().shadow_);
    }

    static const PostprocessPass& Postprocess(vk::Format format) {
        return Instance().Get(Instance().postprocess_, format);
    }

    static const VolumetricLightPass& Volumetric() {
        return Instance().Get(Instance().volumetric_);
    }

    static const LightShaftsPass& LightShafts() {
        return Instance().Get(Instance().lightShafts_);
    }

    static const SsaoPass& Ssao() {
        return Instance().Get(Instance().ssao_);
    }

private:
    using SamplesFormat = std::tuple<vk::SampleCountFlagBits, vk::Format>;
    using Samples = vk::SampleCountFlagBits;

    template <typename Val, typename Key> const Val& Get(std::map<Key, Val>& valMap, Key key) {
        auto it{ valMap.find(key) };
        if (it == valMap.end()) {
            it = valMap.insert(std::make_pair(key, Val{ key })).first;
        }
        return it->second;
    }

    template <typename Val, typename... Key> const Val& Get(std::map<std::tuple<Key...>, Val>& valMap, Key... keyVals) {
        auto key{ std::make_tuple(std::forward<Key>(keyVals)...) };
        auto it{ valMap.find(key) };
        if (it == valMap.end()) {
            it = valMap.insert(std::make_pair(key, Val{ std::forward<Key>(keyVals)... })).first;
        }
        return it->second;
    }

    template <typename Val> const Val& Get(std::optional<Val>& val) {
        if (!val.has_value()) {
            val = Val{};
        }
        return *val;
    }

    std::map<Samples, DepthPass> depth_;
    std::map<SamplesFormat, TransparentPass> transparent_;
    std::map<SamplesFormat, OpaquePass> opaque_;
    std::map<vk::Format, PostprocessPass> postprocess_;
    std::optional<ShadowPass> shadow_;
    std::optional<VolumetricLightPass> volumetric_;
    std::optional<LightShaftsPass> lightShafts_;
    std::optional<SsaoPass> ssao_;
};

} // namespace ugine::gfx
