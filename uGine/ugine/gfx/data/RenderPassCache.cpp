﻿#include "RenderPassCache.h"

namespace ugine::gfx {

void RenderPassCache::Init() {}

void RenderPassCache::Destroy() {
    Instance().depth_.clear();
    Instance().opaque_.clear();
    Instance().transparent_.clear();
    Instance().postprocess_.clear();
    Instance().shadow_.reset();
    Instance().volumetric_.reset();
    Instance().lightShafts_.reset();
    Instance().ssao_.reset();
}

} // namespace ugine::gfx
