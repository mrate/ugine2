﻿#pragma once

#include <ugine/gfx/core/Pipeline.h>
#include <ugine/utils/Singleton.h>

namespace ugine::gfx {

class Material;
class MaterialInstance;
class RenderContext;

class PipelineCache : public utils::Singleton<PipelineCache> {
public:
    static void Init();
    static void Destroy();

    static size_t Size() {
        return Instance().pipelineCache_.size();
    }

    Pipeline* GetPipeline(const RenderContext& context, const MaterialInstance& material);
    Pipeline* GetPipeline(const RenderContext& context, const Material& material);

    void Invalidate(const Material& material);
    void Invalidate();

public:
    using CacheKey = std::tuple<const void*, vk::RenderPass, vk::SampleCountFlagBits>;
    std::map<CacheKey, Pipeline> pipelineCache_;
};

} // namespace ugine::gfx
