﻿#pragma once

#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/Limits.h>
#include <ugine/utils/Hash.h>
#include <ugine/utils/Singleton.h>

#include <array>
#include <unordered_map>

namespace ugine::gfx {

struct FramebufferKey {
    vk::RenderPass renderpass;
    uint32_t attachmentCount{};
    std::array<vk::ImageView, limits::MAX_ATTACHMENTS> attachments;
    uint32_t width{};
    uint32_t height{};
};

inline bool operator==(const FramebufferKey& l, const FramebufferKey& r) {
    if (l.renderpass != r.renderpass || l.attachmentCount != r.attachmentCount || l.width != r.width || l.height != r.height) {
        return false;
    }

    for (uint32_t i = 0; i < l.attachmentCount; ++i) {
        if (l.attachments[i] != r.attachments[i]) {
            return false;
        }
    }

    return true;
}

} // namespace ugine::gfx

namespace std {
template <> struct hash<ugine::gfx::FramebufferKey> {
    std::size_t operator()(const ugine::gfx::FramebufferKey& k) const noexcept {
        std::size_t seed{ std::hash<uint32_t>{}(k.attachmentCount) };
        ugine::utils::HashCombine(seed, k.renderpass);
        for (uint32_t i = 0; i < k.attachmentCount; ++i) {
            ugine::utils::HashCombine(seed, k.attachments[i]);
        }
        return seed;
    }
};
} // namespace std

namespace ugine::gfx {
class FramebufferCache : public utils::Singleton<FramebufferCache> {
public:
    vk::Framebuffer GetOrCreateFramebuffer(const FramebufferKey& key);

    static size_t Size() {
        return Instance().framebuffers_.size();
    }

    static void Init();
    static void Destroy();

private:
    std::unordered_map<FramebufferKey, vk::UniqueFramebuffer> framebuffers_;
};

} // namespace ugine::gfx
