﻿#include "PipelineCache.h"

#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/GraphicsService.h>
#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/rendering/MaterialInstance.h>
#include <ugine/utils/Log.h>

namespace ugine::gfx {

void PipelineCache::Init() {}

void PipelineCache::Destroy() {
    Instance().pipelineCache_.clear();
}

Pipeline* PipelineCache::GetPipeline(const RenderContext& context, const MaterialInstance& material) {
    return GetPipeline(context, material.Material());
}

Pipeline* PipelineCache::GetPipeline(const RenderContext& context, const Material& material) {
    CacheKey key{ &material, context.renderPass, context.samples };

    auto it{ pipelineCache_.find(key) };
    if (it == pipelineCache_.end()) {
        UGINE_TRACE("Creating new pipline.");

        it = pipelineCache_.insert(std::make_pair(key, material.GetPass(context.pass).CreatePipeline(context.renderPass, context.samples))).first;
    }

    return &it->second;
}

void PipelineCache::Invalidate(const Material& material) {
    // TODO:
    Invalidate();
}

void PipelineCache::Invalidate() {
    UGINE_DEBUG("PipelineCache invalidated.");

    GraphicsService::Device().WaitGpu();
    pipelineCache_.clear();
}

} // namespace ugine::gfx
