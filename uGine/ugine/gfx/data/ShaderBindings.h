﻿#pragma once

#include <ugine/gfx/core/Buffer.h>

#include <glm/glm.hpp>

namespace ugine::core {
class Camera;
}

namespace ugine::gfx {

static constexpr uint32_t GLOBAL_SHADING_DATASET{ 0 };
static constexpr uint32_t SHADING_BINDING{ 0 };
static constexpr uint32_t LIGHTS_BINDING{ 1 };
static constexpr uint32_t DIR_SHADOW_MAP_BINDING{ 2 };
static constexpr uint32_t SPOT_SHADOW_MAP_BINDING{ 3 };
static constexpr uint32_t POINT_SHADOW_MAP_BINDING{ 4 };
static constexpr uint32_t BRDF_LUT_BINDING{ 5 };
static constexpr uint32_t IRRADIANCE_BINDING{ 6 };
static constexpr uint32_t PREFILTERED_CUBEMAP_BINDING{ 7 };
static constexpr uint32_t AO_BINDING{ 8 };
static constexpr uint32_t DEPTH_MAP_BINDING{ 9 };
static constexpr uint32_t SCENE_BINDING{ 10 };

static constexpr uint32_t FRAME_DATASET{ 1 };
static constexpr uint32_t FRAME_BINDING{ 0 };

static constexpr uint32_t CAMERA_DATASET{ 2 };
static constexpr uint32_t CAMERA_BINDING{ 0 };

static constexpr uint32_t MATERIAL_DATASET{ 3 };
static constexpr uint32_t COMPUTE_DATASET{ 3 };

static constexpr uint32_t POSTPROCESS_THREAD_COUNT{ 32 };

static constexpr uint32_t FULLSCREEN_VS_COUNT{ 3 };
static constexpr uint32_t CUBE_VS_COUNT{ 32 };
static constexpr uint32_t ICOSPHERE_VS_COUNT{ 240 };
static constexpr uint32_t UVSPHERE_VS_COUNT{ 2880 };
static constexpr uint32_t CYLINDER_VS_COUNT{ 384 };
static constexpr uint32_t CONE_VS_COUNT{ 192 };

static constexpr uint32_t OUTLINE_STENCIL{ 1 << 0 };
static constexpr uint32_t OPAQUE_STENCIL{ 1 << 1 };

struct FrameCB {
    float time;
    float timeDiff;
    float windSpeed;
    float windRandomness;

    glm::vec3 windDirection;
    float windWaveSize;

    glm::vec3 sunPosition;
    float volumetricIntensity{};

    glm::vec3 csm;
};

struct CameraCB {
    glm::mat4 viewProj;
    glm::mat4 view;
    glm::mat4 proj;
    glm::mat4 invView;
    glm::mat4 invProj;
    glm::mat4 viewProjPrev;
    glm::vec3 cameraPos;
    float pad;
    float cameraNear;
    float cameraFar;
    glm::uvec2 resolution;
};

struct IndirectDispatchCB {
    uint32_t dx;
    uint32_t dy;
    uint32_t dz;
};

struct IndirectDrawCB {
    uint32_t vertexCount;
    uint32_t instanceCount;
    uint32_t firstVertex;
    uint32_t firstInstance;
};

struct IndirectIndexedDrawCB {
    uint32_t indexCount;
    uint32_t instanceCount;
    uint32_t firstIndex;
    int32_t vertexOffset;
    uint32_t firstInstance;
};

void SetupFrameCB(FrameCB& cb);
void SetupFrameCB(Buffer& buffer);

void SetupCameraCB(CameraCB& cb, const core::Camera& camera, const glm::mat4& viewProjPrev);
void SetupCameraCB(Buffer& buffer, const core::Camera& camera, const glm::mat4& viewProjPrev);

Buffer CreateCB(size_t size);

} // namespace ugine::gfx
