﻿#include "Shapes.h"

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <random>

namespace ugine::gfx {

Mesh CreateCubeMesh(float size) {
    const auto halfSize{ size * 0.5f };

    const std::vector<Vertex> vertices = { {
        // Front
        { { -halfSize, -halfSize, halfSize }, { 0, 0, 1.0f }, { -1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 0.0f } },
        { { halfSize, -halfSize, halfSize }, { 0, 0, 1.0f }, { -1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 0.0f } },
        { { halfSize, halfSize, halfSize }, { 0, 0, 1.0f }, { -1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 1.0f } },
        { { -halfSize, halfSize, halfSize }, { 0, 0, 1.0f }, { -1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 1.0f } },

        // Back
        { { halfSize, -halfSize, -halfSize }, { 0, 0, -1.0f }, { 1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 0.0f } },
        { { -halfSize, -halfSize, -halfSize }, { 0, 0, -1.0f }, { 1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 0.0f } },
        { { -halfSize, halfSize, -halfSize }, { 0, 0, -1.0f }, { 1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 1.0f } },
        { { halfSize, halfSize, -halfSize }, { 0, 0, -1.0f }, { 1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 1.0f } },

        // Right
        { { halfSize, -halfSize, halfSize }, { 1.0f, 0, 0 }, { 0.0f, 0.0f, 1.0f }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 0.0f } },
        { { halfSize, -halfSize, -halfSize }, { 1.0f, 0, 0 }, { 0.0f, 0.0f, 1.0f }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 0.0f } },
        { { halfSize, halfSize, -halfSize }, { 1.0f, 0, 0 }, { 0.0f, 0.0f, 1.0f }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 1.0f } },
        { { halfSize, halfSize, halfSize }, { 1.0f, 0, 0 }, { 0.0f, 0.0f, 1.0f }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 1.0f } },

        // Left
        { { -halfSize, -halfSize, -halfSize }, { -1.0f, 0, 0 }, { 0.0f, 0.0f, -1.0f }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 0.0f } },
        { { -halfSize, -halfSize, halfSize }, { -1.0f, 0, 0 }, { 0.0f, 0.0f, -1.0f }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 0.0f } },
        { { -halfSize, halfSize, halfSize }, { -1.0f, 0, 0 }, { 0.0f, 0.0f, -1.0f }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 1.0f } },
        { { -halfSize, halfSize, -halfSize }, { -1.0f, 0, 0 }, { 0.0f, 0.0f, -1.0f }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 1.0f } },

        // Bottom
        { { -halfSize, -halfSize, -halfSize }, { 0, -1.0f, 0 }, { -1.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, 1.0f }, { 1.0f, 0.0f } },
        { { halfSize, -halfSize, -halfSize }, { 0, -1.0f, 0 }, { -1.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, 1.0f }, { 0.0f, 0.0f } },
        { { halfSize, -halfSize, halfSize }, { 0, -1.0f, 0 }, { -1.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, 1.0f }, { 0.0f, 1.0f } },
        { { -halfSize, -halfSize, halfSize }, { 0, -1.0f, 0 }, { -1.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, 1.0f }, { 1.0f, 1.0f } },

        // Top
        { { -halfSize, halfSize, halfSize }, { 0, 1.0f, 0 }, { -1.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, -1.0f }, { 1.0f, 0.0f } },
        { { halfSize, halfSize, halfSize }, { 0, 1.0f, 0 }, { -1.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, -1.0f }, { 0.0f, 0.0f } },
        { { halfSize, halfSize, -halfSize }, { 0, 1.0f, 0 }, { -1.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, -1.0f }, { 0.0f, 1.0f } },
        { { -halfSize, halfSize, -halfSize }, { 0, 1.0f, 0 }, { -1.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, -1.0f }, { 1.0f, 1.0f } },
    } };

    const std::vector<IndexType> indices = {
        0, 1, 2, 2, 3, 0, // Front
        4, 5, 6, 6, 7, 4, // Back
        8, 9, 10, 10, 11, 8, // Right
        12, 13, 14, 14, 15, 12, // Left
        16, 17, 18, 18, 19, 16, // Bottom
        20, 21, 22, 22, 23, 20, // Top
    };

    return Mesh(3, vertices, indices, false, "Cube");
}

Mesh CreatePlaneMesh(float width, float height) {
    const auto halfWidth{ width * 0.5f };
    const auto halfHeight{ height * 0.5f };

    const std::vector<Vertex> vertices = { {
        // Front
        { { -halfWidth, -halfHeight, 0.0f }, { 0.0f, 0.0f, 1.0f }, { 1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 0.0f } },
        { { halfWidth, -halfHeight, 0.0f }, { 0.0f, 0.0f, 1.0f }, { 1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 0.0f } },
        { { -halfWidth, halfHeight, 0.0f }, { 0.0f, 0.0f, 1.0f }, { 1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 1.0f } },
        { { halfWidth, halfHeight, 0.0f }, { 0.0f, 0.0f, 1.0f }, { 1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 1.0f } },
    } };

    const std::vector<IndexType> indices = { 0, 1, 2, 1, 3, 2 };

    return Mesh(1, vertices, indices, false, "Plane");
}

Mesh CreateSphereMesh(float diameter, float sectorCount, float stackCount) {
    // http://www.songho.ca/opengl/gl_sphere.html

    float radius{ diameter * 0.5f };

    float x, y, z, xy; // vertex position
    float nx, ny, nz, lengthInv = 1.0f / radius; // vertex normal
    float s, t; // vertex texCoord

    float sectorStep = 2 * glm::pi<float>() / sectorCount;
    float stackStep = glm::pi<float>() / stackCount;
    float sectorAngle, stackAngle;

    std::vector<Vertex> vertices;
    for (int i = 0; i <= stackCount; ++i) {
        stackAngle = glm::pi<float>() / 2 - i * stackStep; // starting from pi/2 to -pi/2
        xy = radius * cosf(stackAngle); // r * cos(u)
        z = radius * sinf(stackAngle); // r * sin(u)

        // add (sectorCount+1) vertices per stack
        // the first and last vertices have same position and normal, but different tex coords
        for (int j = 0; j <= sectorCount; ++j) {
            sectorAngle = j * sectorStep; // starting from 0 to 2pi

            // vertex position (x, y, z)
            x = xy * cosf(sectorAngle); // r * cos(u) * cos(v)
            y = xy * sinf(sectorAngle); // r * cos(u) * sin(v)

            // normalized vertex normal (nx, ny, nz)
            nx = x * lengthInv;
            ny = y * lengthInv;
            nz = z * lengthInv;

            // vertex tex coord (s, t) range between [0, 1]
            s = (float)j / sectorCount;
            t = (float)i / stackCount;

            vertices.push_back({ { x, y, z }, { nx, ny, nz }, { 0, 0, 0 }, { 0, 0, 0 }, { s, t } });
        }
    }

    // generate CCW index list of sphere triangles
    std::vector<IndexType> indices;
    int k1, k2;
    for (int i = 0; i < stackCount; ++i) {
        k1 = static_cast<int>(i * (sectorCount + 1)); // beginning of current stack
        k2 = static_cast<int>(k1 + sectorCount + 1); // beginning of next stack

        for (int j = 0; j < sectorCount; ++j, ++k1, ++k2) {
            // 2 triangles per sector excluding first and last stacks
            // k1 => k2 => k1+1
            if (i != 0) {
                indices.push_back(k1);
                indices.push_back(k2);
                indices.push_back(k1 + 1);
            }

            // k1+1 => k2 => k2+1
            if (i != (stackCount - 1)) {
                indices.push_back(k1 + 1);
                indices.push_back(k2);
                indices.push_back(k2 + 1);
            }
        }
    }

    return Mesh(3, vertices, indices, false, "Sphere");
}

Mesh CreateGrassCluster(float dist) {
    std::vector<Vertex> vertices;
    std::vector<IndexType> indices;

    glm::mat4 rot{ 1.0f };
    glm::mat4 trans{ glm::translate(glm::mat4{ 1.0f }, { 0, 0, dist }) };

    uint32_t index{};
    for (int i = 0; i < 3; ++i) {
        glm::mat t{ rot * trans };

        vertices.push_back({ glm::vec3{ t * glm::vec4{ -0.25f, 0, 0, 0.5f } }, { 0, 1, 0 }, {}, {}, { 0, 1 } });
        vertices.push_back({ glm::vec3{ t * glm::vec4{ -0.25f, 0.5f, 0, 0.5f } }, { 0, 1, 0 }, {}, {}, { 0, 0 } });
        vertices.push_back({ glm::vec3{ t * glm::vec4{ 0.25f, 0.5f, 0, 0.5f } }, { 0, 1, 0 }, {}, {}, { 1, 0 } });
        vertices.push_back({ glm::vec3{ t * glm::vec4{ 0.25f, 0, 0, 0.5f } }, { 0, 1, 0 }, {}, {}, { 1, 1 } });

        rot *= glm::rotate(glm::mat4{ 1.0f }, 2.0f * 1.047198f, glm::vec3{ 0, 1, 0 });

        indices.push_back(index);
        indices.push_back(index + 1);
        indices.push_back(index + 3);
        indices.push_back(index + 2);
        indices.push_back(index + 3);
        indices.push_back(index + 1);

        index += 4;
    }

    return Mesh(1, vertices, indices, false, "Grass cluster");
}

void GenerateGridMesh(uint32_t width, uint32_t height, std::vector<Vertex>& vertices, std::vector<IndexType>& indices) {
    float sx{ 1.0f / static_cast<float>(width - 1) };
    float sy{ 1.0f / static_cast<float>(height - 1) };

    float j{ 0.1f };

    std::random_device rd; //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<float> disX(-sx * j, sx * j);
    std::uniform_real_distribution<float> disY(-sy * j, sy * j);

    for (uint32_t z = 0; z < height; ++z) {
        for (uint32_t x = 0; x < width; ++x) {
            float offsetX{ x == 0 || x == width - 1 ? 0 : disX(gen) };
            float offsetY{ z == 0 || z == height - 1 ? 0 : disY(gen) };

            auto u{ static_cast<float>(x) * sx + offsetX };
            auto v{ static_cast<float>(z) * sy + offsetY };

            vertices.push_back({ { x, 0, z }, { 0, 1, 0 }, { -1, 0, 0 }, { 0, 0, -1 }, { u, v } });
        }
    }

    for (uint32_t z = 0; z < height - 1; ++z) {
        IndexType index{ static_cast<IndexType>(z * width) };

        for (uint32_t x = 0; x < width - 1; ++x) {
            indices.push_back(index);
            indices.push_back(index + width);
            indices.push_back(index + 1);

            indices.push_back(index + width);
            indices.push_back(index + width + 1);
            indices.push_back(index + 1);

            ++index;
        }
    }
}

Mesh CreateGridMesh(uint32_t width, uint32_t height, int lodLevels, bool isMutable) {
    std::vector<Vertex> vertices;
    std::vector<IndexType> indices;

    GenerateGridMesh(width, height, vertices, indices);

    return Mesh(lodLevels, vertices, indices, isMutable, "Grid mesh");
}

void Shapes::Init() {
    Instance().cube_ = MESH_ADD(CreateCubeMesh(1.0f));
    Instance().plane_ = MESH_ADD(CreatePlaneMesh(1.0f, 1.0f));
    Instance().sphere_ = MESH_ADD(CreateSphereMesh(1.0f));

    Instance().grassClusterA_ = MESH_ADD(CreateGrassCluster(0.0625f));
    Instance().grassClusterStar_ = MESH_ADD(CreateGrassCluster(0.0f));
    Instance().gridMesh_ = MESH_ADD(CreateGridMesh(64, 64));
}

void Shapes::Destroy() {
    Instance().cube_ = {};
    Instance().plane_ = {};
    Instance().sphere_ = {};

    Instance().grassClusterA_ = {};
    Instance().grassClusterStar_ = {};
    Instance().gridMesh_ = {};
}

const MeshRef& Shapes::Cube() {
    return Instance().cube_;
}

const MeshRef& Shapes::Plane() {
    return Instance().plane_;
}

const MeshRef& Shapes::Sphere() {
    return Instance().sphere_;
}

const MeshRef& Shapes::GrassClusterA() {
    return Instance().grassClusterA_;
}

const MeshRef& Shapes::GrassClusterStar() {
    return Instance().grassClusterStar_;
}

const MeshRef& Shapes::TerrainMesh() {
    return Instance().gridMesh_;
}

} // namespace ugine::gfx
