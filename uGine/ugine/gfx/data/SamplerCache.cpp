﻿#include "SamplerCache.h"

#include <ugine/core/ConfigService.h>
#include <ugine/gfx/Graphics.h>
#include <ugine/gfx/GraphicsService.h>
#include <ugine/utils/Assert.h>

namespace ugine::gfx {

void SamplerCache::Init() {}

void SamplerCache::Destroy() {
    Instance().samplers_.clear();
}

SamplerRef SamplerCache::Sampler(Type type, uint32_t mipLevels) {
    auto it{ Instance().samplers_.find(std::make_pair(type, mipLevels)) };
    if (it == Instance().samplers_.end()) {
        auto sampler{ Instance().CreateSampler(type, mipLevels) };
        it = Instance().samplers_.insert(std::make_pair(std::make_pair(type, mipLevels), std::move(sampler))).first;
    }
    return it->second;
}

SamplerRef SamplerCache::CreateSampler(Type type, uint32_t mipLevels) {
    vk::SamplerCreateInfo samplerInfo{};
    switch (type) {
    case Type::LinearClampToBorderWhite:
    case Type::LinearClampToBorderBlack:
        samplerInfo.maxAnisotropy = 1.0f;
        samplerInfo.magFilter = vk::Filter::eLinear;
        samplerInfo.minFilter = vk::Filter::eLinear;
        samplerInfo.mipmapMode = vk::SamplerMipmapMode::eLinear;
        samplerInfo.addressModeU = vk::SamplerAddressMode::eClampToBorder;
        samplerInfo.addressModeV = vk::SamplerAddressMode::eClampToBorder;
        samplerInfo.addressModeW = vk::SamplerAddressMode::eClampToBorder;
        samplerInfo.mipLodBias = 0.0f;
        samplerInfo.maxAnisotropy = 1.0f;
        samplerInfo.minLod = 0.0f;
        samplerInfo.maxLod = 1.0f;
        samplerInfo.borderColor = type == Type::LinearClampToBorderWhite ? vk::BorderColor::eFloatOpaqueWhite : vk::BorderColor::eFloatOpaqueBlack;
        samplerInfo.compareOp = vk::CompareOp::eNever;
        break;

    case Type::LinearRepeatBorderWhite:
    case Type::LinearRepeatBorderBlack:
        samplerInfo.magFilter = vk::Filter::eLinear;
        samplerInfo.minFilter = vk::Filter::eLinear;
        samplerInfo.mipmapMode = vk::SamplerMipmapMode::eLinear;
        samplerInfo.addressModeU = vk::SamplerAddressMode::eRepeat;
        samplerInfo.addressModeV = vk::SamplerAddressMode::eRepeat;
        samplerInfo.addressModeW = vk::SamplerAddressMode::eRepeat;
        samplerInfo.borderColor = type == Type::LinearRepeatBorderWhite ? vk::BorderColor::eFloatOpaqueWhite : vk::BorderColor::eFloatOpaqueBlack;
        samplerInfo.compareOp = vk::CompareOp::eNever;
        samplerInfo.mipLodBias = 0.0f;
        samplerInfo.minLod = 0.0f;
        samplerInfo.maxLod = mipLevels > 1 ? static_cast<float>(mipLevels) : 0.0f;
        samplerInfo.maxAnisotropy = mipLevels > 1 ? core::ConfigService::AnisotropicFilteringLevels() : 1.0f;
        samplerInfo.anisotropyEnable = mipLevels > 1 ? VK_TRUE : VK_FALSE;
        break;

    case Type::LinearClampToEdge:
        samplerInfo.magFilter = vk::Filter::eLinear;
        samplerInfo.minFilter = vk::Filter::eLinear;
        samplerInfo.mipmapMode = vk::SamplerMipmapMode::eLinear;
        samplerInfo.addressModeU = vk::SamplerAddressMode::eClampToEdge;
        samplerInfo.addressModeV = vk::SamplerAddressMode::eClampToEdge;
        samplerInfo.addressModeW = vk::SamplerAddressMode::eClampToEdge;
        samplerInfo.minLod = 0.0f;
        samplerInfo.maxLod = mipLevels > 1 ? static_cast<float>(mipLevels) : 1.0f;
        samplerInfo.borderColor = vk::BorderColor::eFloatOpaqueWhite;
        break;

    case Type::NearestRepeat:
        samplerInfo.magFilter = vk::Filter::eNearest;
        samplerInfo.minFilter = vk::Filter::eNearest;
        samplerInfo.mipmapMode = vk::SamplerMipmapMode::eNearest;
        samplerInfo.addressModeU = vk::SamplerAddressMode::eRepeat;
        samplerInfo.addressModeV = vk::SamplerAddressMode::eRepeat;
        samplerInfo.addressModeW = vk::SamplerAddressMode::eRepeat;
        samplerInfo.minLod = 0.0f;
        samplerInfo.maxLod = mipLevels > 1 ? static_cast<float>(mipLevels) : 1.0f;
        samplerInfo.borderColor = vk::BorderColor::eFloatOpaqueWhite;
        break;

    default:
        UGINE_ASSERT("Invalid sampler type");
    }

    return SAMPLER_ADD(GraphicsService::Graphics().Device().createSamplerUnique(samplerInfo));
}

} // namespace ugine::gfx
