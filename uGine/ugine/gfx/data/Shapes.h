﻿#pragma once

#include <ugine/gfx/rendering/Mesh.h>
#include <ugine/utils/Singleton.h>

namespace ugine::gfx {

Mesh CreateCubeMesh(float size);
Mesh CreatePlaneMesh(float width, float height);
Mesh CreateSphereMesh(float diameter, float sectorCount = 16.0f, float stackCount = 16.0f);

Mesh CreateGrassCluster(float dist);
Mesh CreateGridMesh(uint32_t width, uint32_t height, int lodLevels = 1, bool isMutable = false);
void GenerateGridMesh(uint32_t width, uint32_t height, std::vector<Vertex>& vertices, std::vector<IndexType>& indices);

class Shapes : public utils::Singleton<Shapes> {
public:
    static void Init();
    static void Destroy();

    static const MeshRef& Cube();
    static const MeshRef& Plane();
    static const MeshRef& Sphere();

    static const MeshRef& GrassClusterA();
    static const MeshRef& GrassClusterStar();
    static const MeshRef& TerrainMesh();

private:
    MeshRef cube_;
    MeshRef plane_;
    MeshRef sphere_;

    MeshRef grassClusterA_;
    MeshRef grassClusterStar_;
    MeshRef gridMesh_;
};

} // namespace ugine::gfx
