﻿#pragma once

#include <ugine/gfx/Sampler.h>
#include <ugine/utils/Singleton.h>

#include <map>

namespace ugine::gfx {

class SamplerCache : public utils::Singleton<SamplerCache> {
public:
    enum class Type {
        LinearClampToBorderWhite,
        LinearClampToBorderBlack,
        LinearRepeatBorderWhite,
        LinearRepeatBorderBlack,
        LinearClampToEdge,
        NearestRepeat,
    };

    static void Init();
    static void Destroy();

    static SamplerRef Sampler(Type type, uint32_t mipLevels = 0);

private:
    SamplerRef CreateSampler(Type type, uint32_t mipLevels);

    std::map<std::pair<Type, uint32_t>, SamplerRef> samplers_;
};

} // namespace ugine::gfx
