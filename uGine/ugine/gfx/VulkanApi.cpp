﻿#include "VulkanApi.h"

#include "Error.h"
#include "Gfx.h"
#include "Graphics.h"

#include <ugine/utils/Log.h>

#include <iostream>
#include <set>
#include <stdexcept>
#include <string>

#pragma comment(lib, "vulkan-1.lib")

using namespace ugine::gfx;

VKAPI_ATTR void VKAPI_CALL vkCmdSetDepthTestEnableEXT(VkCommandBuffer commandBuffer, VkBool32 depthTestEnable) {}
VKAPI_ATTR void VKAPI_CALL vkCmdSetStencilOpEXT(
    VkCommandBuffer commandBuffer, VkStencilFaceFlags faceMask, VkStencilOp failOp, VkStencilOp passOp, VkStencilOp depthFailOp, VkCompareOp compareOp) {}

PFN_vkCmdBeginDebugUtilsLabelEXT vkCmdBeginDebugUtilsLabelEXT_Impl;
PFN_vkCmdEndDebugUtilsLabelEXT vkCmdEndDebugUtilsLabelEXT_Impl;
PFN_vkSetDebugUtilsObjectNameEXT vkSetDebugUtilsObjectNameEXT_Impl;

VKAPI_ATTR void VKAPI_CALL vkCmdBeginDebugUtilsLabelEXT(VkCommandBuffer commandBuffer, const VkDebugUtilsLabelEXT* pLabelInfo) {
    vkCmdBeginDebugUtilsLabelEXT_Impl(commandBuffer, pLabelInfo);
}
VKAPI_ATTR void VKAPI_CALL vkCmdEndDebugUtilsLabelEXT(VkCommandBuffer commandBuffer) {
    vkCmdEndDebugUtilsLabelEXT_Impl(commandBuffer);
}
VKAPI_ATTR VkResult VKAPI_CALL vkSetDebugUtilsObjectNameEXT(VkDevice device, const VkDebugUtilsObjectNameInfoEXT* ext) {
    return vkSetDebugUtilsObjectNameEXT_Impl(device, ext);
}

namespace {

struct DeviceDescriptor {
    vk::PhysicalDevice device;
    vk::SurfaceKHR surface;
    Queues queues;
};

bool CheckDeviceExtensionSupport(vk::PhysicalDevice device) {
    auto availableExtensions{ device.enumerateDeviceExtensionProperties() };
    std::set<std::string> requiredExtensions(VulkanApi::DEVICE_EXTENSIONS.begin(), VulkanApi::DEVICE_EXTENSIONS.end());

    for (const auto& extension : availableExtensions) {
        requiredExtensions.erase(extension.extensionName);
    }

    return requiredExtensions.empty();
}

VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData) {

    switch (messageSeverity) {
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
        UGINE_TRACE(pCallbackData->pMessage);
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
        UGINE_INFO(pCallbackData->pMessage);
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
        UGINE_WARN(pCallbackData->pMessage);
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
        UGINE_ERROR(pCallbackData->pMessage);
        break;
    }

    return VK_FALSE;
}

vk::Result CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, VkDebugUtilsMessengerEXT* pDebugMessenger) {
    auto func{ (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT") };
    if (func) {
        func(instance, pCreateInfo, nullptr, pDebugMessenger);
        return vk::Result::eSuccess;
    }

    return vk::Result::eErrorExtensionNotPresent;
}

void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger) {
    auto func{ (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT") };
    if (func) {
        func(instance, debugMessenger, nullptr);
    }
}

void PopulateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo) {
    createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    createInfo.messageSeverity
        = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    createInfo.messageType
        = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    createInfo.pfnUserCallback = DebugCallback;
}

SwapChainSupportDetails QuerySwapChainSupport(vk::PhysicalDevice device, vk::SurfaceKHR surface) {
    SwapChainSupportDetails details{};

    details.capabilities = device.getSurfaceCapabilitiesKHR(surface);
    details.formats = device.getSurfaceFormatsKHR(surface);
    details.presentModes = device.getSurfacePresentModesKHR(surface);

    return details;
}

bool IsDeviceSuitable(vk::PhysicalDevice device, vk::SurfaceKHR& surface, DeviceDescriptor& outputDesciptor) {
    auto deviceProperties{ device.getProperties() };
    auto deviceFeatures{ device.getFeatures() };
    std::vector<vk::QueueFamilyProperties> queueFamilies = device.getQueueFamilyProperties();

    bool hasGfxCmdQueue{ false };
    bool hasComputeCmdQueue{ false };
    bool hasTransferCmdQueue{ false };
    bool supportSurfacePresent{ false };

    outputDesciptor.device = device;
    outputDesciptor.surface = surface;

    int i{};
    for (const auto& family : queueFamilies) {
        if (family.queueFlags & vk::QueueFlagBits::eGraphics) {
            hasGfxCmdQueue = true;
            outputDesciptor.queues.graphics = i;
        }

        if ((family.queueFlags & vk::QueueFlagBits::eCompute) && !(family.queueFlags & vk::QueueFlagBits::eGraphics)) {
            hasComputeCmdQueue = true;
            outputDesciptor.queues.compute = i;
        }

        if ((family.queueFlags & vk::QueueFlagBits::eTransfer) && !(family.queueFlags & vk::QueueFlagBits::eGraphics)
            && !(family.queueFlags & vk::QueueFlagBits::eCompute)) {
            hasTransferCmdQueue = true;
            outputDesciptor.queues.transfer = i;
        }

        vk::Bool32 presentSupport{ false };
        auto result{ device.getSurfaceSupportKHR(i, surface, &presentSupport) };

        if (result == vk::Result::eSuccess && presentSupport) {
            supportSurfacePresent = true;
            outputDesciptor.queues.surfacePresent = i;
        }

        ++i;

        if (hasGfxCmdQueue && supportSurfacePresent && hasComputeCmdQueue && hasTransferCmdQueue) {
            break;
        }
    }

    if (!hasTransferCmdQueue) {
        outputDesciptor.queues.transfer = outputDesciptor.queues.graphics;
    }

    if (!hasComputeCmdQueue) {
        outputDesciptor.queues.compute = outputDesciptor.queues.graphics;
    }

    bool extensionsSupported{ CheckDeviceExtensionSupport(device) };

    bool swapChainAdequate{ false };
    if (extensionsSupported) {
        SwapChainSupportDetails swapChainSupport{ QuerySwapChainSupport(device, surface) };
        swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
    }

    return deviceProperties.deviceType == vk::PhysicalDeviceType::eDiscreteGpu // Is discrete GPU.
        && deviceFeatures.geometryShader // Supports geometry shader.
        && hasGfxCmdQueue // Has graphics command queue.
        && supportSurfacePresent // Has surface presentation queue.
        && extensionsSupported // Supports all required extensions.
        && swapChainAdequate; // Swap chain has all required capabilities.
}

} // namespace

namespace ugine::gfx {

const std::vector<const char*> VulkanApi::DEVICE_EXTENSIONS = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME,
    VK_KHR_16BIT_STORAGE_EXTENSION_NAME,
};

// VR:
// VK_KHR_EXTERNAL_MEMORY_WIN32_EXTENSION_NAME,
// VK_KHR_WIN32_KEYED_MUTEX_EXTENSION_NAME,

const std::vector<const char*> VulkanApi::VALIDATION_LAYERS = { "VK_LAYER_KHRONOS_validation" };

VulkanApi::VulkanApi(const std::string& appName, bool enableValidationLayers)
    : enableValidationLayers_(enableValidationLayers) {
    vk::ApplicationInfo appInfo{};
    appInfo.pApplicationName = appName.c_str();
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "uGine";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_2;

    vk::InstanceCreateInfo createInfo{};
    createInfo.pApplicationInfo = &appInfo;
    createInfo.enabledExtensionCount = 0;
    createInfo.ppEnabledExtensionNames = nullptr;

    vk::DebugUtilsMessengerCreateInfoEXT debugCreateInfo;
    if (enableValidationLayers) {
        if (!CheckValidationLayerSupport()) {
            UGINE_THROW(GfxError, "Validation layers not available!");
        }

        createInfo.enabledLayerCount = static_cast<uint32_t>(VALIDATION_LAYERS.size());
        createInfo.ppEnabledLayerNames = VALIDATION_LAYERS.data();

        PopulateDebugMessengerCreateInfo(debugCreateInfo);
        createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT*)&debugCreateInfo;
    } else {
        createInfo.enabledLayerCount = 0;
        createInfo.pNext = nullptr;
    }

    auto extensions{ GetRequiredExtensions(enableValidationLayers) };
    CheckDebugSupport(extensions);

    createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
    createInfo.ppEnabledExtensionNames = extensions.data();

    instance_ = vk::createInstanceUnique(createInfo);

    if (enableValidationLayers) {
        SetupDebugMessenger();
    }
}

VulkanApi::~VulkanApi() {
    if (debugMessenger_) {
        DestroyDebugUtilsMessengerEXT(*instance_, debugMessenger_);
    }
}

std::unique_ptr<Graphics> VulkanApi::CreateFirstSuitableDevice(vk::SurfaceKHR surface) {
    auto devices{ instance_->enumeratePhysicalDevices() };

    if (devices.empty()) {
        UGINE_THROW(GfxError, "No suitable GPU device found.");
    }

    DeviceDescriptor deviceDesc;
    for (const auto& device : devices) {
        if (IsDeviceSuitable(device, surface, deviceDesc)) {
            return std::make_unique<Graphics>(*instance_, deviceDesc.device, deviceDesc.queues);
        }
    }

    UGINE_THROW(GfxError, "No suitable GPU device found.");
}

void VulkanApi::LoadProcAddr(vk::Device device) {
    if (supportsDebug_) {
        vkCmdBeginDebugUtilsLabelEXT_Impl = PFN_vkCmdBeginDebugUtilsLabelEXT(vkGetDeviceProcAddr(device, "vkCmdBeginDebugUtilsLabelEXT"));
        vkCmdEndDebugUtilsLabelEXT_Impl = PFN_vkCmdEndDebugUtilsLabelEXT(vkGetDeviceProcAddr(device, "vkCmdEndDebugUtilsLabelEXT"));
        vkSetDebugUtilsObjectNameEXT_Impl = PFN_vkSetDebugUtilsObjectNameEXT(vkGetDeviceProcAddr(device, "vkSetDebugUtilsObjectNameEXT"));
    }
}

bool VulkanApi::CheckDebugSupport(std::vector<const char*>& extensions) {
    uint32_t instance_extension_count;
    if (vkEnumerateInstanceExtensionProperties(nullptr, &instance_extension_count, nullptr) == VK_SUCCESS) {

        std::vector<VkExtensionProperties> available_instance_extensions(instance_extension_count);
        if (vkEnumerateInstanceExtensionProperties(nullptr, &instance_extension_count, available_instance_extensions.data()) == VK_SUCCESS) {

            for (auto& available_extension : available_instance_extensions) {
                if (strcmp(available_extension.extensionName, VK_EXT_DEBUG_UTILS_EXTENSION_NAME) == 0) {
                    supportsDebug_ = true;
                    extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
                }
            }
        }
    }

    return supportsDebug_;
}

bool VulkanApi::CheckValidationLayerSupport() const {
    auto availableLayers{ vk::enumerateInstanceLayerProperties() };
    for (const char* layerName : VALIDATION_LAYERS) {
        auto layerFound{ false };

        for (const auto& layerProperties : availableLayers) {
            if (strcmp(layerName, layerProperties.layerName) == 0) {
                layerFound = true;
                break;
            }
        }

        if (!layerFound) {
            return false;
        }
    }

    return true;
}

std::vector<const char*> VulkanApi::GetRequiredExtensions(bool enableValidationLayers) {
    std::vector<const char*> extensions;

    if (enableValidationLayers) {
        extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    }

    extensions.push_back(VK_KHR_SURFACE_EXTENSION_NAME);

#ifdef WIN32
    extensions.push_back(VK_KHR_WIN32_SURFACE_EXTENSION_NAME);
#endif

    return extensions;
}

void VulkanApi::SetupDebugMessenger() {
    VkDebugUtilsMessengerCreateInfoEXT createInfo = {};
    PopulateDebugMessengerCreateInfo(createInfo);

    auto result{ CreateDebugUtilsMessengerEXT(*instance_, &createInfo, &debugMessenger_) };
    if (result != vk::Result::eSuccess) {
        UGINE_THROW(GfxError, "Failed to create debug messenger.", result);
    }
}

vk::SampleCountFlagBits VulkanApi::GetMaxUsableSampleCount(vk::PhysicalDevice& physicalDevice) {
    vk::PhysicalDeviceProperties physicalDeviceProperties = physicalDevice.getProperties();

    vk::SampleCountFlags counts = physicalDeviceProperties.limits.framebufferColorSampleCounts & physicalDeviceProperties.limits.framebufferDepthSampleCounts;
    if (counts & vk::SampleCountFlagBits::e64) {
        return vk::SampleCountFlagBits::e64;
    }
    if (counts & vk::SampleCountFlagBits::e32) {
        return vk::SampleCountFlagBits::e32;
    }
    if (counts & vk::SampleCountFlagBits::e16) {
        return vk::SampleCountFlagBits::e16;
    }
    if (counts & vk::SampleCountFlagBits::e8) {
        return vk::SampleCountFlagBits::e8;
    }
    if (counts & vk::SampleCountFlagBits::e4) {
        return vk::SampleCountFlagBits::e4;
    }
    if (counts & vk::SampleCountFlagBits::e2) {
        return vk::SampleCountFlagBits::e2;
    }

    return vk::SampleCountFlagBits::e1;
}

SwapChainSupportDetails VulkanApi::QuerySwapChainSupport(vk::PhysicalDevice device, vk::SurfaceKHR surface) {
    SwapChainSupportDetails details;

    details.capabilities = device.getSurfaceCapabilitiesKHR(surface);
    details.formats = device.getSurfaceFormatsKHR(surface);
    details.presentModes = device.getSurfacePresentModesKHR(surface);

    return details;
}

vk::SurfaceFormatKHR VulkanApi::ChooseSwapSurfaceFormat(const std::vector<vk::SurfaceFormatKHR>& availableFormats, bool srgb) {
    for (const auto& availableFormat : availableFormats) {
        if (srgb && (availableFormat.format == vk::Format::eB8G8R8A8Srgb && availableFormat.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)) {
            return availableFormat;
        }

        if (!srgb && availableFormat.format == vk::Format::eB8G8R8A8Unorm) {
            return availableFormat;
        }
    }

    return availableFormats[0];
}

vk::PresentModeKHR VulkanApi::ChooseSwapPresentMode(const std::vector<vk::PresentModeKHR>& availablePresentModes) {
    for (const auto& availablePresentMode : availablePresentModes) {
        if (availablePresentMode == vk::PresentModeKHR::eMailbox) {
            return availablePresentMode;
        }
    }

    return vk::PresentModeKHR::eFifo;
}

vk::Extent2D VulkanApi::ChooseSwapExtent(const vk::SurfaceCapabilitiesKHR& capabilities, uint32_t width, uint32_t height) {
    if (capabilities.currentExtent.width != UINT32_MAX) {
        return capabilities.currentExtent;
    } else {
        vk::Extent2D actualExtent = { width, height };

        actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
        actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

        return actualExtent;
    }
}

bool VulkanApi::HasStencilComponent(VkFormat format) {
    return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}

} // namespace ugine::gfx
