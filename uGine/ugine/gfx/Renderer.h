﻿#pragma once

#include <ugine/core/Component.h>
#include <ugine/core/Scheduler.h>
#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/RenderQueue.h>
#include <ugine/gfx/Visibility.h>
#include <ugine/gfx/core/Query.h>
#include <ugine/gfx/core/Texture.h>
#include <ugine/gfx/postprocess/PostprocessChain.h>
#include <ugine/gfx/rendering/LightShadingData.h>

#include <entt/entt.hpp>

namespace ugine::core {
class Scene;
}

namespace ugine::gfx {

class Renderer {
public:
    Renderer();
    ~Renderer();

    void Render(core::Scene& scene, GPUCommandList command);

    void SetRenderFlags(RenderFlags flags) {
        renderFlags_ = flags;
    }

    RenderFlags RenderFlags() const {
        return renderFlags_;
    }

private:
    using Clock = std::chrono::high_resolution_clock;

    struct LightShaftsParams {
        glm::vec2 lightPos;
        float size;
        float weight;
        float decayFallof;
        float exposure;
    };

    struct SsaoParams {
        float radius{ 0.5f };
        float bias{ 0.000025f };
        float __pad1;
        float __pad2;
        // vec4 for padding.
        glm::vec4 kernel[limits::SSAO_KERNEL_SIZE];
    };

    gfx::RenderFlags MaskRenderFlags(gfx::RenderFlags flags) const {
        return gfx::RenderFlags{ flags & renderFlags_ };
    }

    ComputePipeline LoadComputeShader(const std::filesystem::path& path) const;

    void CollectLights(RenderContext& context);
    void CalcVisibility(core::Scene& scene, const core::Camera& camera, RenderPassType pass, Visibility& visibility) const;
    void PopulateRenderQueue(const RenderContext& context, RenderQueue& opauqeQueue, RenderQueue& transparentQueue) const;

    // Render.
    void RenderCamera(RenderContext& context, core::CameraComponent& camera, core::Scheduler::Group& grp);
    void RenderDepthPrePass(RenderContext& context, const RenderTarget& depth);
    void RenderVolumetricSources(RenderContext& context, const RenderTarget& output, const TextureView& inputDepth);
    void RenderSun(RenderContext& context, const RenderTarget& intOutput, const RenderTarget& output, const RenderTarget& inputDepth);
    void RenderAO(RenderContext& context, const RenderTarget& intOutput, const TextureView& inputDepth, const RenderTarget& output);

    void RenderShadows(RenderContext& context, core::Scheduler::Group& grp);
    void RenderShadowDir(RenderContext& context, const LightCounter::LightShadow& entry, int csm);
    void RenderShadowSpot(RenderContext& context, const LightCounter::LightShadow& entry);
    void RenderShadowPoint(RenderContext& context, const core::GameObject& go, const core::LightComponent& light, const LightCounter::LightShadow& entry);

    void RenderOpaque(RenderContext& context, const RenderTarget& output, const RenderTarget& depth, const TextureView& ao);
    void RenderOpaque(RenderContext& context);
    void RenderTransparent(RenderContext& context, const RenderTarget& output, const TextureView& volumetrics, const TextureView& sun);

    void RenderMeshes(RenderContext& context, const RenderQueue& renderQueue);

    // Compute.
    void BilateralUpscale(
        RenderContext& context, const TextureViewRef& src, vk::ImageLayout srcLayout, const TextureViewRef& dst, vk::ImageLayout dstLayout) const;

    // Bind.
    void BindCommonCB(
        GPUCommandList cmd, const RenderContext& context, const TextureView& ao = {}, const TextureView& depth = {}, const TextureView& scene = {});

    void UpdateSkyBox(const core::Scene& scene);
    glm::fquat SunDirection(const RenderContext& context) const;

    gfx::RenderFlags renderFlags_{ RenderFlags::All };

    RenderTarget volLightRT_;

    LightShadingDataBinder lightShading_;

    uint32_t frameQuery_{};

    bool dynamicSky_{};
    TextureViewRef skyBox_;
    TextureViewRef brdfLut_;
    TextureViewRef skyBoxIrradiance_;
    TextureViewRef skyBoxPrefiltered_;

    MaterialInstanceRef skyBoxMaterial_;
    MaterialInstanceRef dynamicSkyMaterial_;

    Buffer frameCB_;

    Buffer ssaoCB_;
    SsaoParams ssaoParams_;

    std::unique_ptr<gfx::ShadowMaps> shadowMaps_;

    // TODO: Volumetric
    MaterialRef volumetricMat_;
    MaterialRef dirLightVolMaterial_;
    MaterialRef pointLightVolMaterial_;
    MaterialRef spotLightVolMaterial_;

    MaterialRef sunMaterial_;
    MaterialInstanceRef lightShaftsMaterial_;

    MaterialRef ssaoMaterial_;

    // Computes.
    ComputePipeline bilateralUpscale_;
    ComputePipeline lightShafts_;
    ComputePipeline blur_;
};

} // namespace ugine::gfx
