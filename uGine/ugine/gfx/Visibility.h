﻿#pragma once

#include <ugine/core/Camera.h>
#include <ugine/math/Culling.h>
#include <ugine/math/Frustum.h>
#include <ugine/utils/Vector.h>

#include <entt/entt.hpp>

#include <unordered_map>
#include <vector>

namespace ugine::gfx {

class Visibility {
public:
    enum Flags {
        None = 0,
    };

    struct SubMesh {
        entt::entity ent;
        uint32_t subMeshCount;
    };

    Visibility(const Visibility&) = default;
    Visibility& operator=(const Visibility&) = default;

    Visibility() = default;
    void SetCamera(const core::Camera& camera, bool omniDirectional = false, float omniDistance = 0.0f);

    void SetFlags(uint32_t flags) {
        flags_ = flags;
    }

    uint32_t Flags() const {
        return flags_;
    }

    void SetMeshes(size_t size);
    void SetSkinnedMeshes(size_t size);
    void SetParticles(size_t size);
    void SetTerrains(size_t size);
    void Finalize();

    const std::vector<SubMesh>& Meshes() const {
        return meshes_.Entries();
    }

    uint32_t MeshesSubmeshSize() const {
        return meshesSize_;
    }

    const std::vector<SubMesh>& SkinnedMeshes() const {
        return skinnedMeshes_.Entries();
    }

    uint32_t SkinnedMeshesSubmeshSize() const {
        return skinnedMeshesSize_;
    }

    const std::vector<entt::entity>& Particles() const {
        return particles_.Entries();
    }

    const std::vector<entt::entity>& Terrains() const {
        return terrains_.Entries();
    }

    // Thread-safe.
    void CheckMesh(const math::AABB& aabb, entt::entity ent, uint32_t submeshes);
    void CheckSkinnedMesh(const math::AABB& aabb, entt::entity ent, uint32_t submeshes);
    void CheckParticles(const math::AABB& aabb, entt::entity ent);
    void CheckTerrain(const math::AABB& aabb, entt::entity ent);

private:
    bool Pass(const math::AABB& aabb) const;

    glm::vec3 cameraPos_;
    math::Frustum frustum_;
    bool omniDirectional_{};
    float omniDistanceSquared_{};

    uint32_t flags_;
    utils::TSVector<SubMesh> meshes_;
    std::atomic_uint32_t meshesSize_;
    utils::TSVector<SubMesh> skinnedMeshes_;
    std::atomic_uint32_t skinnedMeshesSize_;
    utils::TSVector<entt::entity> particles_;
    utils::TSVector<entt::entity> terrains_;
};

class LightCounter {
public:
    struct LightShadow {
        entt::entity ent;
        uint32_t shadowIndex;
    };

    void SetLightShadows(size_t size);

    const std::vector<LightShadow>& LightShadows() const {
        return lightShadows_.Entries();
    }

    void FinalizeLightShadows();

    // Thread-safe.
    void AddLightShadow(entt::entity ent, uint32_t shadowIndex);

private:
    utils::TSVector<LightShadow> lightShadows_;
};

} // namespace ugine::gfx
