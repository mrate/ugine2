﻿#pragma once

#include <ugine/core/Error.h>

#include <optional>

#include <vulkan/vulkan.hpp>

namespace ugine::gfx {

class GfxError : public core::Error {
public:
    GfxError(const char* msg, const char* name, const char* file, int line, const char* function, std::optional<vk::Result> result = {}) noexcept
        : core::Error(msg, name, file, line, function)
        , result_(result) {}

    std::optional<vk::Result> Result() const noexcept {
        return result_;
    }

private:
    std::optional<vk::Result> result_;
};

} // namespace ugine::gfx
