﻿#pragma once

#include <ugine/utils/ResourceManagement.h>
#include <ugine/utils/Storage.h>

#include <vulkan/vulkan.hpp>

namespace ugine::gfx {

using Samplers = utils::SafeDeleteStorage<vk::UniqueSampler>;
using SamplerRef = Samplers::Ref;

#define SAMPLER_ADD(...) STORAGE_ADD(Samplers, __VA_ARGS__)

} // namespace ugine::gfx
