﻿#include "RenderPassFactory.h"

#include <ugine/gfx/Graphics.h>
#include <ugine/gfx/GraphicsService.h>
#include <ugine/gfx/core/Device.h>

namespace ugine::gfx {

void RenderPassFactory::AddAttachment(vk::SampleCountFlagBits samples, vk::Format format, vk::AttachmentLoadOp loadOp, vk::AttachmentStoreOp storeOp,
    vk::ImageLayout initLayout, vk::ImageLayout finalLayout) {
    vk::AttachmentDescription2 attachment;
    attachment.format = format;
    attachment.samples = samples;
    attachment.loadOp = loadOp;
    attachment.storeOp = storeOp;
    attachment.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
    attachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
    attachment.initialLayout = initLayout;
    attachment.finalLayout = finalLayout;

    attchmentDescriptions_.push_back(attachment);

    colorReferences_.push_back(
        { static_cast<uint32_t>(attchmentDescriptions_.size() - 1), vk::ImageLayout::eColorAttachmentOptimal, vk::ImageAspectFlagBits::eColor });
}

void RenderPassFactory::AddDepthAttachment(
    vk::SampleCountFlagBits samples, vk::AttachmentLoadOp loadOp, vk::AttachmentStoreOp storeOp, vk::ImageLayout initLayout, vk::ImageLayout finalLayout) {
    vk::AttachmentDescription2 attachment;
    attachment.format = GraphicsService::Graphics().DepthFormat();
    attachment.samples = samples;
    attachment.loadOp = loadOp;
    attachment.storeOp = storeOp;
    attachment.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
    attachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
    attachment.initialLayout = initLayout;
    attachment.finalLayout = finalLayout;
    attchmentDescriptions_.push_back(attachment);

    depthReferences_.push_back({ static_cast<uint32_t>(attchmentDescriptions_.size() - 1), vk::ImageLayout::eDepthStencilAttachmentOptimal,
        vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil });
}

void RenderPassFactory::AddDepthStencilAttachment(vk::SampleCountFlagBits samples, vk::AttachmentLoadOp loadOp, vk::AttachmentStoreOp storeOp,
    vk::AttachmentLoadOp stencilLoadOp, vk::AttachmentStoreOp stencilStoreOp, vk::ImageLayout initLayout, vk::ImageLayout finalLayout) {
    vk::AttachmentDescription2 attachment;
    attachment.format = GraphicsService::Graphics().DepthStencilFormat();
    attachment.samples = samples;
    attachment.loadOp = loadOp;
    attachment.storeOp = storeOp;
    attachment.stencilLoadOp = stencilLoadOp;
    attachment.stencilStoreOp = stencilStoreOp;
    attachment.initialLayout = initLayout;
    attachment.finalLayout = finalLayout;
    attchmentDescriptions_.push_back(attachment);

    depthReferences_.push_back({ static_cast<uint32_t>(attchmentDescriptions_.size() - 1), vk::ImageLayout::eDepthStencilAttachmentOptimal,
        vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil });
}

void RenderPassFactory::AddResolve(vk::Format format, vk::ImageLayout initialLayout, vk::ImageLayout finalLayout) {
    vk::AttachmentDescription2 attachment;
    attachment.samples = vk::SampleCountFlagBits::e1;
    attachment.loadOp = vk::AttachmentLoadOp::eDontCare;
    attachment.storeOp = vk::AttachmentStoreOp::eDontCare;
    attachment.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
    attachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
    attachment.initialLayout = initialLayout;
    attachment.finalLayout = finalLayout;

    // Color attachment.
    attachment.format = format;
    attchmentDescriptions_.push_back(attachment);

    resolveReferences_.push_back(
        { static_cast<uint32_t>(attchmentDescriptions_.size() - 1), vk::ImageLayout::eColorAttachmentOptimal, vk::ImageAspectFlagBits::eColor });
}

void RenderPassFactory::AddDepthResolve(vk::ImageLayout initialLayout, vk::ImageLayout finalLayout) {
    vk::AttachmentDescription2 attachment;
    attachment.samples = vk::SampleCountFlagBits::e1;
    attachment.loadOp = vk::AttachmentLoadOp::eDontCare;
    attachment.storeOp = vk::AttachmentStoreOp::eStore;
    attachment.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
    attachment.stencilStoreOp = vk::AttachmentStoreOp::eStore;
    attachment.initialLayout = initialLayout;
    attachment.finalLayout = finalLayout;

    // Depth attachment.
    attachment.format = GraphicsService::Graphics().DepthStencilFormat();
    attchmentDescriptions_.push_back(attachment);

    depthResolveReferences_.push_back({ static_cast<uint32_t>(attchmentDescriptions_.size() - 1), vk::ImageLayout::eDepthStencilAttachmentOptimal,
        vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil });

    vk::SubpassDescriptionDepthStencilResolve resolveDepth;
    // TODO: Resolve mode.
    resolveDepth.pDepthStencilResolveAttachment = depthResolveReferences_.data();
    resolveDepth.depthResolveMode = vk::ResolveModeFlagBits::eAverage;
    resolveDepth.stencilResolveMode = vk::ResolveModeFlagBits::eNone;

    resolveDepth_.push_back(resolveDepth);
}

void RenderPassFactory::AddSubpassDependency(uint32_t srcSubpass, uint32_t dstSubpass, vk::PipelineStageFlags srcStage, vk::AccessFlags srcAccessMask,
    vk::PipelineStageFlags dstStage, vk::AccessFlags dstAccessMask, vk::DependencyFlags flags) {
    vk::SubpassDependency2 dependency{};
    dependency.srcSubpass = srcSubpass;
    dependency.dstSubpass = dstSubpass;
    dependency.srcStageMask = srcStage;
    dependency.srcAccessMask = srcAccessMask;
    dependency.dstStageMask = dstStage;
    dependency.dstAccessMask = dstAccessMask;
    dependency.dependencyFlags = flags;
    dependencies_.push_back(dependency);
}

vk::UniqueRenderPass RenderPassFactory::Create(const std::string_view& name) {
    std::array<vk::SubpassDescription2, 1> subpassDescriptions;
    subpassDescriptions[0].pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
    subpassDescriptions[0].colorAttachmentCount = static_cast<uint32_t>(colorReferences_.size());
    subpassDescriptions[0].pColorAttachments = colorReferences_.data();
    subpassDescriptions[0].pDepthStencilAttachment = depthReferences_.data();

    if (!resolveReferences_.empty()) {
        subpassDescriptions[0].pResolveAttachments = resolveReferences_.data();
    }

    if (!resolveDepth_.empty()) {
        subpassDescriptions[0].pNext = resolveDepth_.data();
    }

    // Create the actual renderpass
    vk::RenderPassCreateInfo2 renderPassCI;
    renderPassCI.attachmentCount = static_cast<uint32_t>(attchmentDescriptions_.size());
    renderPassCI.pAttachments = attchmentDescriptions_.data();
    renderPassCI.subpassCount = static_cast<uint32_t>(subpassDescriptions.size());
    renderPassCI.pSubpasses = subpassDescriptions.data();
    renderPassCI.dependencyCount = static_cast<uint32_t>(dependencies_.size());
    renderPassCI.pDependencies = dependencies_.data();

    auto renderPass{ GraphicsService::Graphics().Device().createRenderPass2Unique(renderPassCI) };
    GraphicsService::Device().SetObjectDebugName(*renderPass, name);
    return renderPass;
}

} // namespace ugine::gfx
