﻿#include "Image.h"

#include <ugine/utils/Assert.h>
#include <ugine/utils/KtxRead.h>
#include <ugine/utils/StbImageWrapper.h>

namespace ugine::gfx {

namespace detail {
    class KtxImage : public ImageImpl {
    public:
        KtxImage(const std::filesystem::path& path) {
            ktxResult result{ ktxTexture_CreateFromNamedFile(path.string().c_str(), KTX_TEXTURE_CREATE_LOAD_IMAGE_DATA_BIT, &texture_) };
            if (result != KTX_SUCCESS) {
                throw std::exception("Invalid ktx texture.");
            }

            data_ = ktxTexture_GetData(texture_);
            pixelSize_ = static_cast<uint32_t>(texture_->dataSize / texture_->baseWidth / texture_->baseHeight / texture_->baseDepth);

            UGINE_ASSERT(pixelSize_ > 0 && pixelSize_ <= 4);
        }

        ~KtxImage() {
            ktxTexture_Destroy(texture_);
        }

        uint32_t At(uint32_t x, uint32_t y) const override {
            auto ptr{ &data_[(y * texture_->baseHeight + x) * pixelSize_] };
            uint32_t pixel{};
            if (pixelSize_ > 3) {
                pixel |= (*ptr & 0xff) << 24;
            }
            if (pixelSize_ > 2) {
                pixel |= (*ptr & 0xff) << 16;
            }
            if (pixelSize_ > 1) {
                pixel |= (*ptr & 0xff) << 8;
            }

            pixel |= (*ptr & 0xff) << 0;
            return pixel;
        }

        uint32_t Width() const override {
            return texture_->baseWidth;
        }
        uint32_t Height() const override {
            return texture_->baseHeight;
        }
        uint32_t Levels() const override {
            return texture_->numLevels;
        }
        uint32_t Layers() const override {
            return texture_->numLayers;
        }

    private:
        ktxTexture* texture_{};
        ktx_uint8_t* data_{};
        uint32_t pixelSize_{};
    };

    class StbImage : public ImageImpl {
    public:
        StbImage(const std::filesystem::path& file) {
            int texChannels{};

            if (stbi_is_hdr(file.string().c_str())) {
                data_ = reinterpret_cast<unsigned char*>(stbi_loadf(file.string().c_str(), &width_, &width_, &texChannels, STBI_rgb_alpha));
            } else {
                data_ = stbi_load(file.string().c_str(), &width_, &height_, &texChannels, STBI_rgb_alpha);
            }
        }

        ~StbImage() {
            stbi_image_free(data_);
        }

        uint32_t At(uint32_t x, uint32_t y) const override {
            auto ptr{ &data_[(y * width_ + x) * 4] };
            uint32_t pixel{};
            pixel |= (*ptr & 0xff) << 24;
            pixel |= (*ptr & 0xff) << 16;
            pixel |= (*ptr & 0xff) << 8;
            pixel |= (*ptr & 0xff) << 0;
            return pixel;
        }

        uint32_t Width() const override {
            return width_;
        }

        uint32_t Height() const override {
            return height_;
        }

        uint32_t Levels() const override {
            return 1;
        }

        uint32_t Layers() const override {
            return 1;
        }

    private:
        unsigned char* data_{};
        int width_{};
        int height_{};
    };

} // namespace detail

Image::Image() {}

Image::Image(const std::filesystem::path& path)
    : path_{ path } {
    if (path.extension() == ".ktx" || path.extension() == ".ktx2") {
        image_ = std::make_unique<detail::KtxImage>(path);
    } else {
        image_ = std::make_unique<detail::StbImage>(path);
    }
}

Image::~Image() {}

uint32_t Image::At(float u, float v) const {
    return At(static_cast<uint32_t>(u * Width()), static_cast<uint32_t>(v * Height()));
}

uint32_t Image::At(uint32_t x, uint32_t y) const {
    return image_->At(x, y);
}

uint32_t Image::Width() const {
    return image_->Width();
}

uint32_t Image::Height() const {
    return image_->Height();
}

uint32_t Image::Levels() const {
    return image_->Levels();
}

uint32_t Image::Layers() const {
    return image_->Layers();
}

} // namespace ugine::gfx
