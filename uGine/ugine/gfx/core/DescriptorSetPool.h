﻿#pragma once

#include <ugine/utils/StlMemAllocator.h>

#include <vulkan/vulkan.hpp>

#include <deque>

namespace ugine::gfx {

class DescriptorSetPool {
public:
    DescriptorSetPool(vk::DescriptorSetLayout layout, const std::vector<vk::DescriptorPoolSize>& sizes, uint32_t singlePoolSize = 512u);

    [[nodiscard]] vk::DescriptorSet Allocate();
    void Release(vk::DescriptorSet ds, bool immediately = false);

private:
    bool TryPendingSets();
    void CreateNewPool();
    void CreateDescriptorSets(vk::DescriptorPool pool);

    const vk::DescriptorSetLayout descriptorSetLayout_;
    const std::vector<vk::DescriptorPoolSize> sizes_;
    const uint32_t singlePoolSize_{};

    uint32_t index_{ 0 };
    std::vector<vk::UniqueDescriptorPool> pools_;

    using PendingDescriptor = std::pair<uint64_t, vk::DescriptorSet>;
    std::vector<vk::DescriptorSet, utils::StlMemAllocator<vk::DescriptorSet>> availableSets_;
    std::deque<PendingDescriptor, utils::StlMemAllocator<PendingDescriptor>> pendingSets_;
};

class ScopedDescriptor {
public:
    ScopedDescriptor(DescriptorSetPool& pool)
        : pool_(pool)
        , descriptor_(pool.Allocate()) {}

    ~ScopedDescriptor() {
        pool_.Release(descriptor_);
    }

    ScopedDescriptor(const ScopedDescriptor&) = delete;
    ScopedDescriptor& operator=(const ScopedDescriptor&) = delete;
    ScopedDescriptor(ScopedDescriptor&&) = delete;
    ScopedDescriptor& operator=(ScopedDescriptor&&) = delete;

    vk::DescriptorSet Descriptor() const {
        return descriptor_;
    }

    vk::DescriptorSet* PDescriptor() {
        return &descriptor_;
    }

private:
    DescriptorSetPool& pool_;
    vk::DescriptorSet descriptor_;
};

// New interface.
class DescriptorSetPoolNew {
public:
    DescriptorSetPoolNew(const std::vector<vk::DescriptorPoolSize>& sizes, uint32_t singlePoolSize = 512u);
    ~DescriptorSetPoolNew();

    [[nodiscard]] vk::DescriptorSet Allocate(vk::DescriptorSetLayout layout);
    void Reset();

private:
    void CreatePool();
    void DestroyPool();

    vk::UniqueDescriptorPool pool_;
    const std::vector<vk::DescriptorPoolSize> sizes_;
    const uint32_t singlePoolSize_{};
};

} // namespace ugine::gfx
