﻿#pragma once

#include <ugine/gfx/Fwd.h>
#include <ugine/utils/Utils.h>

#include <vulkan/vulkan.hpp>

#include <algorithm>
#include <iterator>
#include <map>
#include <vector>

namespace ugine::gfx {

struct PipelineDesc {
    struct Blending {
        bool enabled{ false };
        vk::BlendOp colorBlendOp{ vk::BlendOp::eAdd };
        vk::BlendFactor srcColorBlendFactor{ vk::BlendFactor::eSrcAlpha };
        vk::BlendFactor dstColorBlendFactor{ vk::BlendFactor::eOneMinusSrcAlpha };
        vk::BlendOp alphaBlendOp{ vk::BlendOp::eAdd };
        vk::BlendFactor srcAlphaBlendFactor{ vk::BlendFactor::eOne };
        vk::BlendFactor dstAlphaBlendFactor{ vk::BlendFactor::eZero };
        vk::ColorComponentFlags colorWriteMask{ vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB
            | vk::ColorComponentFlagBits::eA };
    } blending;

    struct DepthTest {
        bool testEnable{ true };
        bool writeEnable{ true };
        vk::CompareOp compareOp{ vk::CompareOp::eLessOrEqual };
    } depth;

    struct StencilTest {
        bool testEnable{ true };
        vk::CompareOp compareOp{ vk::CompareOp::eAlways };
        vk::StencilOp depthFailOp{ vk::StencilOp::eReplace };
        vk::StencilOp failOp{ vk::StencilOp::eReplace };
        vk::StencilOp passOp{ vk::StencilOp::eReplace };
        uint32_t compareMask{ 0xff };
        uint32_t writeMask{ 0xff };
        uint32_t reference{ 0xff };
    } stencil;

    vk::PolygonMode polygonMode{ vk::PolygonMode::eFill };
    vk::CullModeFlags cullMode{ vk::CullModeFlagBits::eBack };
    vk::PrimitiveTopology topology{ vk::PrimitiveTopology::eTriangleList };
};

class Pipeline {
public:
    UGINE_MOVABLE_ONLY(Pipeline);

    Pipeline() = default;
    Pipeline(
        vk::PipelineBindPoint bindPoint, vk::UniquePipelineLayout layout, vk::UniquePipeline pipeline, const std::vector<vk::DescriptorSetLayout>& dsLayouts)
        : bindPoint_{ bindPoint }
        , layout_{ std::move(layout) }
        , pipeline_{ std::move(pipeline) }
        , dsLayouts_{ dsLayouts } {}
    Pipeline(
        vk::PipelineBindPoint bindPoint, vk::UniquePipelineLayout layout, vk::UniquePipeline pipeline, std::vector<vk::UniqueDescriptorSetLayout>&& udsLayouts)
        : bindPoint_{ bindPoint }
        , layout_{ std::move(layout) }
        , pipeline_{ std::move(pipeline) }
        , udsLayouts_{ std::move(udsLayouts) } {
        std::transform(udsLayouts_.begin(), udsLayouts_.end(), std::back_inserter(dsLayouts_), [](auto& uds) { return *uds; });
    }

    vk::PipelineLayout Layout() const {
        return *layout_;
    }

    vk::Pipeline operator*() const {
        return *pipeline_;
    }

    vk::PipelineBindPoint BindPoint() const {
        return bindPoint_;
    }

    const std::vector<vk::DescriptorSetLayout>& DescriptorLayouts() const {
        return dsLayouts_;
    }

protected:
    vk::PipelineBindPoint bindPoint_;
    vk::UniquePipelineLayout layout_;
    vk::UniquePipeline pipeline_;

    std::vector<vk::UniqueDescriptorSetLayout> udsLayouts_;
    std::vector<vk::DescriptorSetLayout> dsLayouts_;
};

} // namespace ugine::gfx
