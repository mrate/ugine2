﻿#pragma once

#include "DescriptorSetPool.h"
#include "Pipeline.h"

#include <ugine/gfx/Color.h>
#include <ugine/gfx/core/GpuAllocation.h>
#include <ugine/utils/Vector.h>

#include <vulkan/vulkan.hpp>

#include <array>
#include <stdint.h>

#ifdef MemoryBarrier
#undef MemoryBarrier
#endif

namespace ugine::gfx {

class Buffer;
class Graphics;
class Pipeline;
class TextureView;
class VertexBuffer;

struct DrawCall;

using GPUCommandList = uint32_t;

static GPUCommandList NullCommandList = static_cast<uint32_t>(-1);

struct GPUSemaphore {
    std::vector<vk::UniqueSemaphore> semaphore;

    operator bool() const {
        return !semaphore.empty();
    }

    vk::Semaphore Get() const;
};

struct GPUBarrier {
    enum class Type {
        Buffer,
        Image,
        Memory,
    };

    Type type;
    vk::MemoryBarrier memory;
    vk::BufferMemoryBarrier buffer;
    vk::ImageMemoryBarrier image;

    static GPUBarrier Memory();
    static GPUBarrier Buffer(vk::Buffer buffer, vk::DeviceSize offset, vk::DeviceSize size, vk::AccessFlags srcMask, vk::AccessFlags dstMask);
    static GPUBarrier Buffer(const gfx::Buffer& buffer, vk::AccessFlags srcMask, vk::AccessFlags dstMask);
    static GPUBarrier Image(const gfx::Texture& image, vk::AccessFlags srcMask, vk::AccessFlags dstMask, vk::ImageLayout srcLayout, vk::ImageLayout dstLayout,
        const vk::ImageSubresourceRange& range);
    static GPUBarrier Image(const gfx::Texture& image, vk::AccessFlags srcMask, vk::AccessFlags dstMask, vk::ImageLayout srcLayout, vk::ImageLayout dstLayout,
        vk::ImageAspectFlags aspect);
};

struct GPUDebugLabel {
    GPUDebugLabel(GPUCommandList cmd, const std::string_view& name, const gfx::Color& color);
    GPUDebugLabel(GPUCommandList cmd, const std::string_view& name);
    ~GPUDebugLabel();

private:
    GPUCommandList cmd_;
};

struct GPUPushConstant {
    vk::ShaderStageFlags stage{};
    uint32_t offset{};
    uint32_t size{};
    const void* data{};

    GPUPushConstant() = default;
    GPUPushConstant(vk::ShaderStageFlags stage, uint32_t offset, uint32_t size)
        : stage{ stage }
        , offset{ offset }
        , size{ size } {}

    template <typename T> static GPUPushConstant Data(vk::ShaderStageFlags stage, const T& data, uint32_t offset = 0) {
        GPUPushConstant result{ stage, offset, sizeof(T) };
        result.data = &data;
        return result;
    }
};

class Device {
public:
    enum class CommandType {
        Graphics = 0,
        Compute,
    };

    explicit Device(Graphics& gfx);

    // Command list.
    GPUCommandList BeginCommandList(CommandType type = CommandType::Graphics, bool gfxQueue = false);
    void SubmitCommandLists(const GPUSemaphore* waitSemaphore = nullptr, const GPUSemaphore* signalSemaphore = nullptr);

    void WaitGpu();

    //
    GpuAllocation AllocateGPU(GPUCommandList cmd, size_t size);
    void Free(GPUCommandList cmd, const GpuAllocation& alloc);

    void FlushAllocations(GPUCommandList cmd);

    vk::CommandBuffer CommandBuffer(GPUCommandList cmd);

    void PresentBegin(GPUCommandList cmd, const Color& color = {});
    void PresentEnd(GPUCommandList cmd, const GPUSemaphore* semaphore = nullptr);

    void InitSemaphore(GPUSemaphore& semaphore);
    void DestroySemaphore(GPUSemaphore& semaphore);

    vk::PipelineLayout PipelineLayout(GPUCommandList cmd) const;
    vk::RenderPass PresentRenderpass() const;

    void Barrier(GPUCommandList cmd, const GPUBarrier& barrier);
    void FlushBarriers(GPUCommandList cmd);

    void BeginDebugLabel(GPUCommandList cmd, const std::string_view& name, const Color& color);
    void EndDebugLabel(GPUCommandList cmd);

    void SetObjectDebugName(uint64_t handle, vk::ObjectType type, const std::string_view& name);

    template <typename T> void SetObjectDebugName(T t, const std::string_view& name) {
        SetObjectDebugName(reinterpret_cast<uint64_t>(static_cast<T::CType>(t)), t.objectType, name);
    }

    void SetStencilWriteCompareMask(GPUCommandList cmd, vk::StencilFaceFlags face, uint32_t write, uint32_t compare);

    // Commands.
    void Draw(GPUCommandList cmd, uint32_t vertexCount, uint32_t instanceCount, uint32_t vertexStart, uint32_t firstInstance);
    void Draw(GPUCommandList cmd, const DrawCall& drawCall);
    void DrawIndexed(GPUCommandList cmd, uint32_t indexCount, uint32_t instanceCount, uint32_t indexStart, uint32_t vertexStart, uint32_t firstInstance);
    void DrawIndirect(GPUCommandList cmd, vk::Buffer buffer, vk::DeviceSize offset, uint32_t drawCount, uint32_t stride);
    void DrawIndexedIndirect(GPUCommandList cmd, vk::Buffer buffer, vk::DeviceSize offset, uint32_t drawCount, uint32_t stride);

    void Dispatch(GPUCommandList cmd, uint32_t x, uint32_t y, uint32_t z);
    void DispatchIndirect(GPUCommandList cmd, vk::Buffer buffer, vk::DeviceSize offset);

    void UpdateBuffer(GPUCommandList cmd, vk::Buffer buffer, vk::DeviceSize offset, vk::DeviceSize size, const void* data);
    void BeginRenderPass(GPUCommandList cmd, vk::RenderPass renderpass, vk::Framebuffer framebuffer, const vk::Rect2D scissor, uint32_t clearColorCount,
        const vk::ClearValue* clearColor);
    void EndRenderPass(GPUCommandList cmd);

    void SetViewport(GPUCommandList cmd, const vk::Viewport& viewport);
    void SetScissor(GPUCommandList cmd, const vk::Rect2D& scissor);

    void SetStencilWriteMask(GPUCommandList cmd, vk::StencilFaceFlags flags, uint32_t value);
    void SetStencilCompareMask(GPUCommandList cmd, vk::StencilFaceFlags flags, uint32_t value);

    void BindVertexIndexBuffer(GPUCommandList cmd, const VertexBuffer& buffer);
    void BindVertexBuffer(GPUCommandList cmd, vk::Buffer buffer, vk::DeviceSize offset);
    void BindVertexBuffers(GPUCommandList cmd, uint32_t count, const vk::Buffer* buffers, const vk::DeviceSize* offsets);
    void BindIndexBuffer(GPUCommandList cmd, vk::Buffer buffer, vk::DeviceSize offset, vk::IndexType indexType);

    void PushConstants(GPUCommandList command, const GPUPushConstant& constants);

    void CopyBuffer(GPUCommandList command, vk::Buffer src, vk::Buffer dst, const vk::BufferCopy& range);

    // Bindings.
    void ResetDescriptors(GPUCommandList cmd);
    void ResetDescriptor(GPUCommandList cmd, uint32_t set);
    void ResetBinding(GPUCommandList cmd, uint32_t set, uint32_t binding);
    void ResetBinding(GPUCommandList cmd, uint32_t set, uint32_t binding, uint32_t count);

    void BindPipeline(GPUCommandList cmd, const Pipeline* pipeline);

    void BindDescriptor(GPUCommandList cmd, uint32_t set, vk::DescriptorSet descriptor);
    void BindUniform(GPUCommandList cmd, uint32_t set, uint32_t binding, const Buffer& buffer);
    void BindUniform(GPUCommandList cmd, uint32_t set, uint32_t binding, vk::Buffer buffer, vk::DeviceSize offset, vk::DeviceSize size);
    void BindUniform(GPUCommandList cmd, uint32_t set, uint32_t binding, const vk::DescriptorBufferInfo& buffer);
    void BindImageSampler(GPUCommandList cmd, uint32_t set, uint32_t binding, const TextureView& image);
    void BindImageSampler(GPUCommandList cmd, uint32_t set, uint32_t binding, const vk::DescriptorImageInfo& image);
    void BindImageSampler(GPUCommandList cmd, uint32_t set, uint32_t binding, const std::vector<vk::DescriptorImageInfo>& image);
    void BindStorage(GPUCommandList cmd, uint32_t set, uint32_t binding, const Buffer& buffer);
    void BindStorage(GPUCommandList cmd, uint32_t set, uint32_t binding, const vk::DescriptorBufferInfo& buffer);
    void BindStorage(GPUCommandList cmd, uint32_t set, uint32_t binding, vk::Buffer buffer, vk::DeviceSize offset, vk::DeviceSize size);
    void BindStorageImage(GPUCommandList cmd, uint32_t set, uint32_t binding, const vk::DescriptorImageInfo& image);

private:
    static const uint32_t COMMANDLIST_COUNT{ 32 };
    static const uint32_t MAX_BINDINGS{ 16 };
    static const uint32_t MAX_DATASETS{ 4 };

    struct Dataset {
        bool dirty{};
        vk::DescriptorSet descriptor;

        uint32_t bindings{};
        uint32_t bufArrayCnt[MAX_BINDINGS];
        uint32_t imgArrayCnt[MAX_BINDINGS];

        vk::DescriptorType TYPES[MAX_BINDINGS];
        std::vector<vk::DescriptorBufferInfo> BUF[MAX_BINDINGS];
        std::vector<vk::DescriptorImageInfo> IMG[MAX_BINDINGS];
    };

    struct DescriptorSetManager {
        std::unique_ptr<DescriptorSetPoolNew> pool;

        Dataset dataset[MAX_DATASETS];
        uint32_t sets{};

        bool dirty{};
    };

    struct CommandData {
        vk::UniqueCommandPool gfxCommandPool;
        vk::UniqueCommandBuffer gfxCommandBuffer;

        vk::UniqueCommandPool computeCommandPool;
        vk::UniqueCommandBuffer computeCommandBuffer;

        vk::UniqueCommandPool copyCommandPool;
        vk::UniqueCommandBuffer copyCommandBuffer;

        vk::UniqueCommandPool transitionCommandPool;
        vk::UniqueCommandBuffer transitionCommandBuffer;

        DescriptorSetManager descriptor;

        const Pipeline* pipeline{};
        vk::RenderPass activeRenderPass{};
        CommandType type{ CommandType::Graphics };
        vk::CommandBuffer cmd{};

        std::vector<vk::MemoryBarrier> memoryBarriers;
        std::vector<vk::ImageMemoryBarrier> imageBarriers;
        std::vector<vk::BufferMemoryBarrier> bufferBarriers;

        GpuAllocator allocator;
    };

    struct FrameData {
        CommandData command[COMMANDLIST_COUNT];
    };

    vk::DescriptorSet AllocateDescriptor(GPUCommandList cmd, vk::DescriptorSetLayout layout);

    void CreatePresentRenderpass();

    FrameData& Frame();
    const FrameData& Frame() const;

    CommandData& Command(GPUCommandList cmd);
    const CommandData& Command(GPUCommandList cmd) const;

    DescriptorSetManager& Descriptor(GPUCommandList cmd);

    void BindDescriptors(GPUCommandList cmd);

    bool IsGraphics(GPUCommandList cmd) const;
    bool IsCompute(GPUCommandList cmd) const;

    void SubmitCommandLists(const utils::Vector<vk::Semaphore>& waitSemaphore, const utils::Vector<vk::Semaphore>& signalSemaphore, vk::Fence fence = {});

    Graphics& gfx_;
    std::vector<FrameData> frameData_;

    std::atomic<GPUCommandList> cmdIndex_;
    std::atomic<GPUCommandList> cmdStart_;

    vk::UniqueRenderPass presentRenderpass_;
};

} // namespace ugine::gfx
