﻿#include "GpuAllocation.h"

#include <ugine/gfx/Graphics.h>
#include <ugine/gfx/GraphicsService.h>

#include <ugine/utils/Align.h>

namespace {

struct AllocationDeleter {
    vma::Allocation allocation{};
    vk::Buffer buffer{};

    operator bool() const {
        return allocation || buffer;
    }

    AllocationDeleter(vma::Allocation a, vk::Buffer b)
        : allocation{ a }
        , buffer{ b } {}

    AllocationDeleter(const AllocationDeleter&) = delete;
    AllocationDeleter& operator=(const AllocationDeleter&) = delete;

    AllocationDeleter(AllocationDeleter&& other) {
        allocation = other.allocation;
        buffer = other.buffer;

        other.allocation = nullptr;
        other.buffer = nullptr;
    }
    AllocationDeleter& operator=(AllocationDeleter&& other) {
        allocation = other.allocation;
        buffer = other.buffer;

        other.allocation = nullptr;
        other.buffer = nullptr;
    }

    ~AllocationDeleter() {
        if (allocation) {
            ugine::gfx::GraphicsService::Graphics().Allocator().destroyBuffer(buffer, allocation);
            allocation = nullptr;
            buffer = nullptr;
        }
    }
};

} // namespace

namespace ugine::gfx {

GpuAllocation GpuAllocator::Allocate(size_t size, size_t alignment) {
    current_ = reinterpret_cast<uint8_t*>(utils::AlignTo(reinterpret_cast<size_t>(current_), alignment));

    if (current_ + size > end_) {
        Resize(std::max(size_ * 2, size_ + size * 2));
    }

    GpuAllocation allocation{};
    allocation.buffer = buffer_;
    allocation.memory = current_;
    allocation.offset = static_cast<uint32_t>(current_ - reinterpret_cast<uint8_t*>(mapped_));
    allocation.size = static_cast<uint32_t>(size);

    current_ += size;

    return allocation;
}

GpuAllocator::~GpuAllocator() {
    Free();
}

void GpuAllocator::Reset() {
    current_ = reinterpret_cast<uint8_t*>(mapped_);
    end_ = current_ + size_;
}

void GpuAllocator::Free(const GpuAllocation& a) {
    current_ -= a.size;

    UGINE_ASSERT(current_ >= mapped_);
}

void GpuAllocator::Free() {
    if (allocation_) {
        AllocationDeleter del{ allocation_, buffer_ };
        SAFE_DELETE(del);

        allocation_ = nullptr;
        buffer_ = nullptr;
    }
}

void GpuAllocator::Resize(size_t size) {
    UGINE_TRACE("Resizing GPU allocator from {} to {}", size_, size);

    Free();

    size_ = size;

    vk::BufferCreateInfo bufferCI{};
    bufferCI.size = size;
    bufferCI.usage = vk::BufferUsageFlagBits::eTransferSrc;
    bufferCI.usage |= vk::BufferUsageFlagBits::eVertexBuffer;
    bufferCI.usage |= vk::BufferUsageFlagBits::eIndexBuffer;
    bufferCI.usage |= vk::BufferUsageFlagBits::eUniformBuffer;
    bufferCI.usage |= vk::BufferUsageFlagBits::eStorageBuffer;
    bufferCI.usage |= vk::BufferUsageFlagBits::eIndirectBuffer;

    vma::AllocationCreateInfo allocCI{};
    allocCI.usage = vma::MemoryUsage::eCpuToGpu;
    allocCI.flags = vma::AllocationCreateFlagBits::eMapped;

    vma::AllocationInfo allocInfo;
    auto result{ GraphicsService::Graphics().Allocator().createBuffer(bufferCI, allocCI, &allocInfo) };

    buffer_ = result.first;
    allocation_ = result.second;

    mapped_ = allocInfo.pMappedData;
    memory_ = allocInfo.deviceMemory;

    current_ = reinterpret_cast<uint8_t*>(mapped_);
    end_ = current_ + size_;
}

void GpuAllocator::Flush(uint32_t offset, uint32_t size) {
    GraphicsService::Graphics().Allocator().flushAllocation(allocation_, offset, size);
}

void GpuAllocator::Flush() {
    if (current_ != mapped_) {
        auto size{ static_cast<uint32_t>(reinterpret_cast<uint64_t>(current_) - reinterpret_cast<uint64_t>(mapped_)) };
        Flush(0, size);
    }
}

} // namespace ugine::gfx
