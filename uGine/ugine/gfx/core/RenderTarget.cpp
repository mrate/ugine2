﻿#include "RenderTarget.h"

#include <ugine/utils/Log.h>

namespace ugine::gfx {

RenderTarget::~RenderTarget() {}

RenderTarget::RenderTarget(uint32_t width, uint32_t height, vk::Format format, vk::SampleCountFlagBits samples, vk::ImageUsageFlags usage,
    vk::ImageAspectFlags aspect, const SamplerRef& sampler, bool withStencils)
    : aspect_{ aspect } {
    UGINE_ASSERT(width > 0 && height > 0);

    for (uint32_t i = 0; i < GraphicsService::FramesInFlight(); ++i) {
        auto texture{ TEXTURE_ADD(vk::Extent3D{ width, height, 1 }, format, vk::ImageTiling::eOptimal, usage) };
        DEBUG_ATTACH(texture, "RenderTarget [{}x{}]", width, height);
        texture->SetName(fmt::format("RenderTarget [{}x{}]", width, height));

        auto view{ TEXTURE_VIEW_ADD(texture, TextureView::CreateSampled(*texture, sampler, vk::ImageViewType::e2D, aspect)) };
        textures_.push_back(view);

        if (withStencils) {
            auto stencilView{ TEXTURE_VIEW_ADD(texture, TextureView::CreateSampled(*texture, vk::ImageAspectFlagBits::eStencil)) };
            stencilView->SetName(fmt::format("RenderTarget stencil view [{}x{}]", width, height));
            stencils_.push_back(stencilView);
        }

        if (samples != vk::SampleCountFlagBits::e1) {
            auto multisampleTexture{ TEXTURE_ADD(vk::Extent3D{ width, height, 1 }, format, vk::ImageTiling::eOptimal, usage, samples) };
            DEBUG_ATTACH(multisampleTexture, "RenderTarget multisample [{}x{}]", width, height);
            multisampleTexture->SetName(fmt::format("RenderTarget multisample [{}x{}]", width, height));

            // TODO: Sampler.
            auto multisampleView{ TEXTURE_VIEW_ADD(
                multisampleTexture, TextureView::CreateSampled(*multisampleTexture, sampler, vk::ImageViewType::e2D, aspect)) };

            multisamples_.push_back(multisampleView);
        }
    }
}

void RenderTarget::Transition(GPUCommandList cmd, vk::ImageLayout initLayout, vk::ImageLayout newLayout) {
    for (auto& view : textures_) {
        auto& texture{ Textures::Get(TextureViews::TextureID(view.id())) };
        texture.Transition(cmd, initLayout, newLayout, aspect_);
    }
    for (auto& view : multisamples_) {
        auto& texture{ Textures::Get(TextureViews::TextureID(view.id())) };
        texture.Transition(cmd, initLayout, newLayout, aspect_);
    }
}

void RenderTarget::TransitionStencil(GPUCommandList cmd, vk::ImageLayout initLayout, vk::ImageLayout newLayout) {
    for (auto& view : stencils_) {
        auto& texture{ Textures::Get(TextureViews::TextureID(view.id())) };
        texture.Transition(cmd, initLayout, newLayout, vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil);
    }
}

const Texture& RenderTarget::Texture() const {
    auto textureId{ TextureViews::TextureID(textures_[GraphicsService::ActiveFrameInFlight()].id()) };
    return Textures::Get(textureId);
}

TextureRef RenderTarget::TextureRef() const {
    return TextureViews::TextureRef(ViewRef());
}

const TextureView& RenderTarget::ViewForFrame(uint32_t frame) const {
    return TextureViews::Get(textures_[frame].id());
}

const TextureViewRef& RenderTarget::ViewRefForFrame(uint32_t frame) const {
    return textures_[frame];
}

const TextureView& RenderTarget::View() const {
    return TextureViews::Get(textures_[GraphicsService::ActiveFrameInFlight()].id());
}

const TextureViewRef& RenderTarget::ViewRef() const {
    return textures_[GraphicsService::ActiveFrameInFlight()];
}

const TextureView& RenderTarget::Multisample() const {
    return TextureViews::Get(multisamples_[GraphicsService::ActiveFrameInFlight()].id());
}

const TextureViewRef& RenderTarget::MultisampleRef() const {
    UGINE_ASSERT(!multisamples_.empty());

    return multisamples_[GraphicsService::ActiveFrameInFlight()];
}

const TextureView& RenderTarget::Stencil() const {
    return TextureViews::Get(stencils_[GraphicsService::ActiveFrameInFlight()].id());
}

const TextureViewRef& RenderTarget::StencilRef() const {
    return stencils_[GraphicsService::ActiveFrameInFlight()];
}

vk::Extent3D RenderTarget::Extent() const {
    UGINE_ASSERT(!textures_.empty());

    return textures_[0]->Extent();
}

uint32_t RenderTarget::Width() const {
    UGINE_ASSERT(!textures_.empty());

    return textures_[0]->Extent().width;
}

uint32_t RenderTarget::Height() const {
    UGINE_ASSERT(!textures_.empty());

    return textures_[0]->Extent().height;
}

vk::Format RenderTarget::Format() const {
    UGINE_ASSERT(!textures_.empty());

    return textures_[0]->Format();
}

vk::SampleCountFlagBits RenderTarget::Samples() const {
    return multisamples_.empty() ? vk::SampleCountFlagBits::e1 : Textures::Get(TextureViews::TextureID(multisamples_[0].id())).Samples();
}

void RenderTarget::SetName(const std::string_view& name) {
    for (auto& t : textures_) {
        t->SetName(name);
        TextureViews::TextureRef(t)->SetName(name);
    }

    for (auto& t : multisamples_) {
        t->SetName(name);
        TextureViews::TextureRef(t)->SetName(fmt::format("{} [msaa]", name));
    }

    for (auto& t : stencils_) {
        t->SetName(name);
        TextureViews::TextureRef(t)->SetName(fmt::format("{} [stencil]", name));
    }
}

} // namespace ugine::gfx
