﻿#pragma once

#include <ugine/gfx/Gfx.h>
#include <ugine/utils/Utils.h>

#include <atomic>

namespace ugine::gfx {

class QueryPool {
public:
    UGINE_MOVABLE_ONLY(QueryPool);

    QueryPool() = default;
    QueryPool(vk::QueryType type, uint32_t queryCount);

    void Reset(vk::CommandBuffer cmd);
    uint32_t Timestamp(vk::CommandBuffer cmd, vk::PipelineStageFlagBits stage);
    void FetchResults();

    uint64_t Result(uint32_t query) const {
        return values_[query];
    }

    uint32_t Count() const {
        return count_;
    }

private:
    vk::UniqueQueryPool pool_;
    uint32_t count_{};
    std::atomic_uint32_t frameCount_{};
    std::vector<uint64_t> values_;
};

} // namespace ugine::gfx
