﻿#pragma once

#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/ShaderCache.h>

#include "Pipeline.h"

#include <filesystem>

namespace ugine::gfx {

class ComputePipeline : public Pipeline {
public:
    static ComputePipeline Load(const std::filesystem::path& csShader);

    ComputePipeline() = default;
    ComputePipeline(vk::UniquePipelineLayout pipelineLayout, vk::UniquePipeline pipeline, const std::vector<vk::DescriptorSetLayout>& dsLayouts);
    ComputePipeline(const std::vector<vk::DescriptorSetLayout>& dsLayouts, const Shader& shader);
    ComputePipeline(std::vector<vk::UniqueDescriptorSetLayout>&& udsLayouts, const Shader& shader);

private:
    void Init(const Shader& shader);
};

} // namespace ugine::gfx
