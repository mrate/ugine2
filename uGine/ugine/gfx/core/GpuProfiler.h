﻿#pragma once

#include <ugine/gfx/core/Query.h>
#include <ugine/utils/Singleton.h>

namespace ugine::gfx {

static constexpr uint32_t QUERY_DEFAULT{ 0 };
static constexpr uint32_t QUERY_FRAME{ 1 };

class GpuProfiler : public utils::Singleton<GpuProfiler> {
public:
    struct QueryResult {
        std::string name;
        uint32_t tag{};
        double timeDiffUS{};
    };

    static void Init(uint32_t queryCount = 64);
    static void Destroy();

    void BeginFrame(GPUCommandList cmd);

    uint32_t BeginQuery(
        GPUCommandList cmd, const std::string_view& name, uint32_t tag = 0, vk::PipelineStageFlagBits stage = vk::PipelineStageFlagBits::eTopOfPipe);
    void EndQuery(GPUCommandList cmd, uint32_t beginQuery, vk::PipelineStageFlagBits stage = vk::PipelineStageFlagBits::eBottomOfPipe);

    const std::vector<QueryResult>& Results() const {
        return results_;
    }

    uint32_t ResultsCount() const {
        return resultsCount_;
    }

private:
    // Begin/End queries of single probe.
    struct Query {
        uint32_t beginQuery;
        uint32_t endQuery;
        std::string name;
        uint32_t tag;
    };

    struct FrameQueries {
        std::unique_ptr<QueryPool> pool; // TODO:
        std::vector<Query> queries;
        uint32_t count;
    };

    uint32_t frame_;
    std::atomic_uint32_t frameCount_;

    std::vector<FrameQueries> queries_;
    std::vector<QueryResult> results_;
    uint32_t resultsCount_{};
};

class GpuEvent {
public:
    GpuEvent(GPUCommandList cmd, const std::string_view& name, uint32_t tag, vk::PipelineStageFlagBits beginStage = vk::PipelineStageFlagBits::eTopOfPipe,
        vk::PipelineStageFlagBits endStage = vk::PipelineStageFlagBits::eBottomOfPipe)
        : cmd_{ cmd }
        , endStage_{ endStage } {
        beginQuery_ = GpuProfiler::Instance().BeginQuery(cmd, name, tag, beginStage);
    }

    ~GpuEvent() {
        GpuProfiler::Instance().EndQuery(cmd_, beginQuery_, endStage_);
    }

    GpuEvent() = delete;
    GpuEvent(const GpuEvent&) = delete;
    GpuEvent(GpuEvent&&) = delete;
    GpuEvent& operator=(const GpuEvent&) = delete;
    GpuEvent& operator=(GpuEvent&&) = delete;

private:
    GPUCommandList cmd_{};
    vk::PipelineStageFlagBits endStage_;
    uint32_t beginQuery_{};
};

} // namespace ugine::gfx
