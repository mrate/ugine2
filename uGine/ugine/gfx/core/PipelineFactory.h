﻿#pragma once

#include <vulkan/vulkan.hpp>

#include <vector>

namespace ugine::gfx {

class PipelineFactory {
public:
    PipelineFactory();
    vk::UniquePipeline Create(vk::PipelineLayout layout, vk::RenderPass renderPass);
    vk::UniquePipeline Create(vk::PipelineLayout layout, vk::RenderPass renderPass, uint32_t attachmentCount);

    void SetVertexInput(const vk::VertexInputBindingDescription* binding, const vk::VertexInputAttributeDescription* attribute);
    void SetVertexInput(
        uint32_t bindingCount, const vk::VertexInputBindingDescription* binding, uint32_t attributeCount, const vk::VertexInputAttributeDescription* attribute);

    void SetShaderStages(uint32_t count, const vk::PipelineShaderStageCreateInfo* stages);
    void SetShaderStages(const std::vector<vk::PipelineShaderStageCreateInfo>& stages);
    void SetDynamicStates(uint32_t count, const vk::DynamicState* states);

    vk::PipelineVertexInputStateCreateInfo vertexInputInfo{};
    vk::PipelineInputAssemblyStateCreateInfo inputAssembly{};
    vk::Viewport viewPort{};
    vk::Rect2D scissor{};
    vk::PipelineColorBlendAttachmentState colorBlendAttachment{};
    vk::PipelineViewportStateCreateInfo viewportState{};
    vk::PipelineRasterizationStateCreateInfo rasterizer{};
    vk::PipelineMultisampleStateCreateInfo multisampling{};
    vk::PipelineColorBlendStateCreateInfo colorBlending{};
    vk::PipelineDepthStencilStateCreateInfo depthStencil{};
    vk::PipelineDynamicStateCreateInfo dynamicState{};
    vk::GraphicsPipelineCreateInfo pipelineInfo{};
};

} // namespace ugine::gfx
