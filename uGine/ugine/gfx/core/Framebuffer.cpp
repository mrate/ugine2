﻿#include "Framebuffer.h"

#include <ugine/gfx/Gfx.h>
#include <ugine/utils/Assert.h>

namespace ugine::gfx {

FramebufferAttachments::FramebufferAttachments() {}

TextureViewRef FramebufferAttachments::CreateAttachment(
    vk::Extent3D extent, vk::Format format, vk::ImageAspectFlags aspect, vk::SampleCountFlagBits samples, vk::ImageUsageFlags usage) {

    auto texture{ TEXTURE_ADD(extent, format, vk::ImageTiling::eOptimal, usage, samples) };
    auto view{ TEXTURE_VIEW_ADD(texture, TextureView::Create(*texture, aspect)) };

    attachments_.emplace_back(view);
    attachmentViews_.emplace_back(attachments_.back()->View());

    UGINE_ASSERT(attachments_.size() == attachmentViews_.size());

    return attachments_.back();
}

TextureViewRef FramebufferAttachments::CreateSampledAttachment(vk::Extent3D extent, vk::Format format, vk::ImageAspectFlags aspect,
    vk::SampleCountFlagBits samples, vk::ImageUsageFlags usage, vk::UniqueSampler sampler) {

    auto texture{ TEXTURE_ADD(extent, format, vk::ImageTiling::eOptimal, usage, samples) };
    auto view{ TEXTURE_VIEW_ADD(texture, TextureView::CreateSampled(*texture, vk::ImageViewType::e2D, aspect)) };

    attachments_.emplace_back(view);
    attachmentViews_.emplace_back(attachments_.back()->View());

    UGINE_ASSERT(attachments_.size() == attachmentViews_.size());

    return attachments_.back();
}

Framebuffer::Framebuffer(vk::RenderPass renderPass, const vk::Extent2D& extent, FramebufferAttachments&& attachments)
    : attachmens_(std::move(attachments)) {
    vk::FramebufferCreateInfo frameBufferCI;
    frameBufferCI.attachmentCount = static_cast<uint32_t>(attachmens_.Attachments().size());
    frameBufferCI.pAttachments = attachmens_.Attachments().data();
    frameBufferCI.width = extent.width;
    frameBufferCI.height = extent.height;
    frameBufferCI.renderPass = renderPass;
    frameBufferCI.layers = 1;
    framebuffer_ = GraphicsService::Graphics().Device().createFramebufferUnique(frameBufferCI);
}

void f() {
    std::vector<Framebuffer> vec;
    std::vector<std::vector<Framebuffer>> vecVec;

    for (auto& i : vecVec) {
        for (auto& j : i) {
            auto x = j.Attachments().Size();
        }
    }
}

} // namespace ugine::gfx
