﻿#pragma once

#include <ugine/gfx/Sampler.h>
#include <ugine/gfx/core/Device.h>
#include <ugine/utils/Utils.h>
#include <ugine/utils/VmaWrapper.h>

#include <filesystem>
#include <memory>

namespace ugine::gfx {

class Texture;

class TextureView {
public:
    UGINE_MOVABLE_ONLY(TextureView);

    static TextureView CreateSampled(const Texture& texture, const SamplerRef& sampler, vk::ImageViewType type = vk::ImageViewType::e2D,
        vk::ImageAspectFlags imageAspect = vk::ImageAspectFlagBits::eColor, uint32_t baseLayer = 0, uint32_t layerCount = 0);
    static TextureView CreateSampled(const Texture& texture, vk::ImageViewType type, vk::ImageAspectFlags imageAspect = vk::ImageAspectFlagBits::eColor,
        uint32_t baseLayer = 0, uint32_t layerCount = 0);
    static TextureView CreateSampled(const Texture& texture, vk::ImageAspectFlags imageAspect = vk::ImageAspectFlagBits::eColor);
    static TextureView Create(const Texture& texture, vk::ImageViewType type, vk::ImageAspectFlags imageAspect = vk::ImageAspectFlagBits::eColor,
        uint32_t baseLayer = 0, uint32_t layerCount = 0);
    static TextureView Create(const Texture& texture, vk::ImageAspectFlags imageAspect = vk::ImageAspectFlagBits::eColor);

    TextureView() {}
    TextureView(vk::UniqueImageView view, const SamplerRef& sampler, const vk::Extent3D& extent, vk::Format format)
        : view_(std::move(view))
        , extent_(extent)
        , format_(format) {}

    ~TextureView() {}

    operator bool() const {
        return static_cast<bool>(view_);
    }

    void SetName(const std::string_view& name);

    vk::ImageView View() const {
        return *view_;
    }

    const vk::Extent3D& Extent() const {
        return extent_;
    }

    vk::Format Format() const {
        return format_;
    }

    vk::Sampler Sampler() const {
        return *(*sampler_);
    }

    void SetSampler(const SamplerRef& sampler) {
        sampler_ = sampler;
        descriptor_.sampler = sampler_ ? *(*sampler_) : nullptr;
    }

    const vk::DescriptorImageInfo& Descriptor() const {
        UGINE_ASSERT(descriptor_.sampler);

        return descriptor_;
    }

protected:
    void InitDescriptor(vk::ImageLayout layout);

    vk::UniqueImageView view_;
    SamplerRef sampler_;
    vk::Extent3D extent_{};
    vk::Format format_{};
    vk::DescriptorImageInfo descriptor_{};
};

class Texture {
public:
    struct Data {
        const void* data;
        size_t size;
    };

    static uint32_t CalculateMipLevels(const vk::Extent3D& extent);
    static Texture FromFile(const std::filesystem::path& file, vk::ImageUsageFlags usage = vk::ImageUsageFlagBits::eSampled, bool generateMips = false,
        vk::ImageLayout finalLayout = vk::ImageLayout::eShaderReadOnlyOptimal);
    static Texture FromData(const void* data, size_t size, const vk::Extent3D& extent, vk::Format format, vk::ImageTiling tiling = vk::ImageTiling::eOptimal,
        vk::ImageUsageFlags usage = vk::ImageUsageFlagBits::eSampled, bool generateMips = false,
        vk::ImageLayout finalLayout = vk::ImageLayout::eShaderReadOnlyOptimal);
    static Texture FromData(const std::vector<Data>& data, vk::ImageCreateInfo createInfo, bool generateMips = false,
        vk::ImageLayout finalLayout = vk::ImageLayout::eShaderReadOnlyOptimal);
    static Texture FromCubeMapFile(const std::filesystem::path& file, vk::ImageUsageFlags usage = vk::ImageUsageFlagBits::eSampled, bool hdr = false);
    static Texture FromCubeMapFiles(const std::vector<std::filesystem::path>& files, vk::ImageUsageFlags usage = vk::ImageUsageFlagBits::eSampled);

    Texture() = default;
    ~Texture();

    Texture(Texture&&);
    Texture& operator=(Texture&&);

    Texture(const vk::Extent3D& extent, vk::Format format, vk::ImageTiling tiling, vk::ImageUsageFlags usage,
        vk::SampleCountFlagBits sampleCount = vk::SampleCountFlagBits::e1, uint32_t mipLevels = 1);
    Texture(const vk::ImageCreateInfo& createInfo);

    operator bool() const {
        return image_;
    }

    void GenerateMips(GPUCommandList command, vk::ImageLayout initialLayout, vk::ImageLayout finalLayout);

    void CopyTo(GPUCommandList command, Texture& target, vk::ImageLayout srcLayout, vk::ImageLayout dstLayout,
        vk::ImageAspectFlags aspect = vk::ImageAspectFlagBits::eColor, vk::Filter filter = vk::Filter::eCubicIMG) const;

    void Transition(
        GPUCommandList command, vk::ImageLayout oldLayout, vk::ImageLayout newLayout, vk::ImageAspectFlags aspect = vk::ImageAspectFlagBits::eColor) const;

    vk::Extent3D Extent() const {
        return extent_;
    }

    vk::Format Format() const {
        return format_;
    }

    vk::SampleCountFlagBits Samples() const {
        return samples_;
    }

    vk::Image Image() const {
        return image_;
    }

    bool IsCube() const {
        return isCubeMap_;
    }

    void SetName(const std::string_view& name);
    const std::string& DebugName() const {
        return debugName_;
    }

    void SetFileName(const std::string& fileName) {
        fileName_ = fileName;
        SetName(fileName_);
    }

    const std::string& FileName() const {
        return fileName_;
    }

    uint32_t MipLevels() const {
        return mipLevels_;
    }

    uint32_t Layers() const {
        return layers_;
    }

private:
    Texture(const Texture&) = delete;
    Texture& operator=(const Texture&) = default;

    void CreateImage(const vk::ImageCreateInfo& imageCI);
    void SetData(const std::vector<Data>& data, vk::ImageLayout initialLayout, vk::ImageLayout finalLayout = vk::ImageLayout::eShaderReadOnlyOptimal);
    void GenerateMipMaps(
        GPUCommandList command, uint32_t mipLevels, vk::ImageLayout initialLayout, vk::ImageLayout finalLayout = vk::ImageLayout::eShaderReadOnlyOptimal);

    void Free();

    vk::Image image_{};
    vk::Extent3D extent_{};
    vk::Format format_{};
    vk::SampleCountFlagBits samples_{};
    bool isCubeMap_{ false };
    uint32_t mipLevels_{};
    uint32_t layers_{};

    vma::Allocation allocation_{};

    std::string fileName_{ "<none>" };
    std::string debugName_{};

    static int TotalAllocation;
};

} // namespace ugine::gfx
