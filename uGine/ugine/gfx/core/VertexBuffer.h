﻿#pragma once

#include "Buffer.h"
#include "Device.h"
#include "Vertex.h"

#include <ugine/utils/Utils.h>

#include <vector>

namespace ugine::gfx {

class VertexBuffer {
public:
    UGINE_MOVABLE_ONLY(VertexBuffer);

    VertexBuffer() = default;
    VertexBuffer(uint32_t initVertexSize, uint32_t initIndexSize);
    ~VertexBuffer();

    operator bool() const {
        return buffers_[0].buffer && buffers_[1].buffer;
    }

    uint32_t Add(const std::vector<Vertex>& vertices, size_t alignTo = 0);
    uint32_t Add(const std::vector<gfx::IndexType>& indices, size_t alignTo = 0);

    void Update(uint32_t vertexStart, const std::vector<Vertex>& vertices);
    void Update(uint32_t indexStart, const std::vector<gfx::IndexType>& indices);

    uint32_t VertexBufferOffset() const;
    vk::DescriptorBufferInfo VertexBufferDescriptor() const;

    uint32_t IndexBufferOffset() const;

    void Bind(GPUCommandList cmd);

    const Buffer& VertexBuf() const;
    Buffer& VertexBuf();

    const Buffer& IndexBuf() const;
    Buffer& IndexBuf();

    vk::IndexType IndexType() const;

    uint32_t VertexBufferAllocated() const;
    uint32_t VertexBufferFilled() const;

    uint32_t IndexBufferAllocated() const;
    uint32_t IndexBufferFilled() const;

private:
    enum {
        VERTEX = 0,
        INDEX = 1,
    };

    struct BuffDesc {
        Buffer buffer;
        uint32_t fillSize{};
        uint32_t offset{};
        vk::BufferUsageFlags usage;
    };

    void ReallocateBuffer(BuffDesc& buffer, vk::DeviceSize newSize) const;
    uint32_t Add(BuffDesc& buffer, const void* data, size_t singleElementSize, size_t count, size_t alignTo) const;

    BuffDesc buffers_[2];
};

} // namespace ugine::gfx
