﻿#include "Device.h"

#include <ugine/gfx/FrameStats.h>
#include <ugine/gfx/Graphics.h>
#include <ugine/gfx/GraphicsService.h>
#include <ugine/gfx/Presenter.h>
#include <ugine/gfx/core/Buffer.h>
#include <ugine/gfx/core/Texture.h>
#include <ugine/gfx/data/FramebufferCache.h>
#include <ugine/gfx/tools/Initializers.h>
#include <ugine/utils/Profile.h>
#include <ugine/utils/Utils.h>

#include <ugine/math/Math.h>
#include <ugine/utils/Log.h>

namespace ugine::gfx {

vk::Semaphore GPUSemaphore::Get() const {
    UGINE_ASSERT(!semaphore.empty());
    return *semaphore[GraphicsService::ActiveFrameInFlight()];
}

GPUDebugLabel::GPUDebugLabel(GPUCommandList cmd, const std::string_view& name, const gfx::Color& color)
    : cmd_{ cmd } {
    GraphicsService::Device().BeginDebugLabel(cmd, name, color);
}

ugine::gfx::GPUDebugLabel::GPUDebugLabel(GPUCommandList cmd, const std::string_view& name)
    : cmd_{ cmd } {
    Color color{
        math::RandomFloat(),
        math::RandomFloat(),
        math::RandomFloat(),
        1,
    };
    GraphicsService::Device().BeginDebugLabel(cmd, name, color);
}

GPUDebugLabel::~GPUDebugLabel() {
    GraphicsService::Device().EndDebugLabel(cmd_);
}

GPUBarrier GPUBarrier::Memory() {
    GPUBarrier barrier{};
    barrier.type = Type::Memory;
    barrier.memory.srcAccessMask = vk::AccessFlagBits::eMemoryWrite | vk::AccessFlagBits::eShaderWrite;
    barrier.memory.dstAccessMask = vk::AccessFlagBits::eMemoryRead | vk::AccessFlagBits::eShaderRead;
    return barrier;
}

GPUBarrier GPUBarrier::Buffer(vk::Buffer buffer, vk::DeviceSize offset, vk::DeviceSize size, vk::AccessFlags srcMask, vk::AccessFlags dstMask) {
    GPUBarrier barrier{};
    barrier.type = Type::Buffer;
    barrier.buffer.buffer = buffer;
    barrier.buffer.offset = offset;
    barrier.buffer.size = size;
    barrier.buffer.srcAccessMask = srcMask;
    barrier.buffer.dstAccessMask = dstMask;
    barrier.buffer.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.buffer.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    return barrier;
}

GPUBarrier GPUBarrier::Buffer(const gfx::Buffer& buffer, vk::AccessFlags srcMask, vk::AccessFlags dstMask) {
    GPUBarrier barrier{};
    barrier.type = Type::Buffer;
    barrier.buffer.buffer = buffer.GetBuffer();
    barrier.buffer.offset = buffer.Offset();
    barrier.buffer.size = buffer.Size();
    barrier.buffer.srcAccessMask = srcMask;
    barrier.buffer.dstAccessMask = dstMask;
    barrier.buffer.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.buffer.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    return barrier;
}

GPUBarrier GPUBarrier::Image(const gfx::Texture& image, vk::AccessFlags srcMask, vk::AccessFlags dstMask, vk::ImageLayout srcLayout, vk::ImageLayout dstLayout,
    const vk::ImageSubresourceRange& range) {
    GPUBarrier barrier{};
    barrier.type = Type::Image;
    barrier.image.image = image.Image();
    barrier.image.oldLayout = srcLayout;
    barrier.image.newLayout = dstLayout;
    barrier.image.subresourceRange = range;
    barrier.image.srcAccessMask = srcMask;
    barrier.image.dstAccessMask = dstMask;
    barrier.image.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    return barrier;
}

GPUBarrier GPUBarrier::Image(const gfx::Texture& image, vk::AccessFlags srcMask, vk::AccessFlags dstMask, vk::ImageLayout srcLayout, vk::ImageLayout dstLayout,
    vk::ImageAspectFlags aspect) {
    vk::ImageSubresourceRange range;
    range.aspectMask = aspect;
    range.baseArrayLayer = 0;
    range.baseMipLevel = 0;
    range.layerCount = 1;
    range.levelCount = 1;
    return Image(image, srcMask, dstMask, srcLayout, dstLayout, range);
}

Device::Device(Graphics& gfx)
    : gfx_{ gfx } {

    const std::vector<vk::DescriptorPoolSize> PoolSizes = {
        { vk::DescriptorType::eUniformBuffer, 8 },
        { vk::DescriptorType::eCombinedImageSampler, 16 },
        { vk::DescriptorType::eStorageBuffer, 8 },
        { vk::DescriptorType::eStorageImage, 8 },
    };

    frameData_.resize(GraphicsService::FramesInFlight());
    int frameIndex{};
    for (auto& frame : frameData_) {
        // Per-frame.
        for (uint32_t i = 0; i < COMMANDLIST_COUNT; ++i) {
            // Per-thread.
            auto& cmd{ frame.command[i] };

            {
                vk::CommandPoolCreateInfo ci{};
                ci.queueFamilyIndex = gfx.GraphicsQueueFamily();
                cmd.gfxCommandPool = gfx.Device().createCommandPoolUnique(ci);
                cmd.gfxCommandBuffer = std::move(gfx.Device().allocateCommandBuffersUnique({ *cmd.gfxCommandPool, vk::CommandBufferLevel::ePrimary, 1 })[0]);
                SetObjectDebugName(*cmd.gfxCommandBuffer, fmt::format("GFX Command {} [{}]", i, frameIndex));
            }

            {
                vk::CommandPoolCreateInfo ci{};
                ci.queueFamilyIndex = gfx.ComputeQueueFamily();
                cmd.computeCommandPool = gfx.Device().createCommandPoolUnique(ci);
                cmd.computeCommandBuffer
                    = std::move(gfx.Device().allocateCommandBuffersUnique({ *cmd.computeCommandPool, vk::CommandBufferLevel::ePrimary, 1 })[0]);
                SetObjectDebugName(*cmd.computeCommandBuffer, fmt::format("Compute Command {} [{}]", i, frameIndex));
            }

            uint32_t size{ 256 };
            std::vector<vk::DescriptorPoolSize> poolSizes;
            for (auto& p : PoolSizes) {
                poolSizes.emplace_back(p.type, p.descriptorCount * size);
            }

            cmd.descriptor.pool = std::make_unique<DescriptorSetPoolNew>(poolSizes, size);
            cmd.allocator.Resize(1024 * 1024);
        }

        ++frameIndex;
    }

    CreatePresentRenderpass();
}

GpuAllocation Device::AllocateGPU(GPUCommandList cmd, size_t size) {
    size_t alignment{ std::max(gfx_.Properties().limits.minUniformBufferOffsetAlignment, gfx_.Properties().limits.minStorageBufferOffsetAlignment) };

    return Command(cmd).allocator.Allocate(size, alignment);
}

void Device::Free(GPUCommandList cmd, const GpuAllocation& alloc) {
    Command(cmd).allocator.Free(alloc);
}

void Device::FlushAllocations(GPUCommandList cmd) {
    Command(cmd).allocator.Flush();
}

GPUCommandList Device::BeginCommandList(CommandType type, bool gfxQueue) {
    auto cmd{ cmdIndex_.fetch_add(1) };

    UGINE_ASSERT(cmd < COMMANDLIST_COUNT);

    // Prepare commands.
    auto& command{ Command(cmd) };

    vk::CommandBuffer vkCmd;
    vk::CommandPool vkPool;

    if (type == CommandType::Graphics || gfxQueue) {
        vkCmd = *command.gfxCommandBuffer;
        vkPool = *command.gfxCommandPool;
    } else {
        vkCmd = *command.computeCommandBuffer;
        vkPool = *command.computeCommandPool;
    }

    gfx_.Device().resetCommandPool(vkPool, {});
    command.type = type;
    command.cmd = vkCmd;
    command.cmd.begin(vk::CommandBufferBeginInfo{});

    // Prepare descriptors.
    ResetDescriptors(cmd);
    command.descriptor.pool->Reset();
    command.pipeline = nullptr;
    command.activeRenderPass = nullptr;
    command.allocator.Reset();

    // Dynamic states.
    if (type == CommandType::Graphics) {
        SetStencilWriteCompareMask(cmd, vk::StencilFaceFlagBits::eFrontAndBack, 0x0, 0x0);
    }

    return cmd;
}

void Device::SubmitCommandLists(const GPUSemaphore* waitSemaphore, const GPUSemaphore* signalSemaphore) {
    utils::Vector<vk::Semaphore> waitSemaphores;
    utils::Vector<vk::Semaphore> signalSemaphores;

    if (waitSemaphore) {
        waitSemaphores.push_back(waitSemaphore->Get());
    }

    if (signalSemaphore) {
        signalSemaphores.push_back(signalSemaphore->Get());
    }

    SubmitCommandLists(waitSemaphores, signalSemaphores);
}

void Device::SubmitCommandLists(const utils::Vector<vk::Semaphore>& waitSemaphore, const utils::Vector<vk::Semaphore>& signalSemaphore, vk::Fence fence) {
    PROFILE_EVENT("Submit commands");

    auto start{ cmdStart_.load() };
    auto count{ cmdIndex_.load() };
    cmdStart_ = count;

    std::vector<vk::CommandBuffer> gfxCommands;
    std::vector<vk::CommandBuffer> computeCommands;

    for (uint32_t i = start; i < count; ++i) {
        auto& command{ Command(i) };
        command.cmd.end();

        switch (command.type) {
        case CommandType::Graphics:
            gfxCommands.push_back(command.cmd);
            break;
        case CommandType::Compute:
            computeCommands.push_back(command.cmd);
            break;
        }
    }

    if (!computeCommands.empty() && !gfxCommands.empty()) {
        UGINE_WARN("Compute command and gfx commands in single command list.");
    }

    if (!computeCommands.empty()) {
        gfx_.SubmitComputeCommands(computeCommands, waitSemaphore, signalSemaphore, fence);
    }

    if (!gfxCommands.empty()) {
        gfx_.SubmitGfxCommands(gfxCommands, waitSemaphore, signalSemaphore, fence);
    }
}

void Device::BindDescriptors(GPUCommandList cmd) {
    auto& ds{ Descriptor(cmd) };

    if (!ds.dirty) {
        return;
    }

    ds.dirty = false;

    if (ds.sets == 0) {
        return;
    }

    auto& command{ Command(cmd) };
    UGINE_ASSERT(command.pipeline);

    const auto& dsLayouts{ command.pipeline->DescriptorLayouts() };
    UGINE_ASSERT(dsLayouts.size() == ds.sets);

    // Prepare descriptors.
    utils::Vector<vk::DescriptorSet> descriptors;
    for (uint32_t i = 0; i < ds.sets; ++i) {
        if (!ds.dataset[i].descriptor) {
            ds.dataset[i].descriptor = AllocateDescriptor(cmd, dsLayouts[i]);
        }

        UGINE_ASSERT(ds.dataset[i].descriptor);

        descriptors.push_back(ds.dataset[i].descriptor);
    }

    // Update writes.
    utils::Vector<vk::WriteDescriptorSet> descriptorWrites;
    descriptorWrites.reserve(256);

    for (uint32_t i = 0; i < ds.sets; ++i) {
        auto& dataset{ ds.dataset[i] };
        if (dataset.dirty) {
            dataset.dirty = false;

            for (uint32_t j = 0; j < dataset.bindings; ++j) {
                switch (dataset.TYPES[j]) {
                case vk::DescriptorType::eUniformBuffer:
                case vk::DescriptorType::eStorageBuffer:
                    descriptorWrites.push_back(DescriptorBuffer(dataset.descriptor, dataset.TYPES[j], dataset.BUF[j].data(), j, dataset.bufArrayCnt[j]));
                    break;
                case vk::DescriptorType::eCombinedImageSampler:
                case vk::DescriptorType::eStorageImage:
                    descriptorWrites.push_back(DescriptorImage(dataset.descriptor, dataset.TYPES[j], dataset.IMG[j].data(), j, dataset.imgArrayCnt[j]));
                    break;
                default:
                    UGINE_ASSERT(false && "Invalid binding type");
                    break;
                }
            }
        }
    }

    if (!descriptorWrites.empty()) {
        gfx_.Device().updateDescriptorSets(static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
    }

    // Bind.
    command.cmd.bindDescriptorSets(
        command.pipeline->BindPoint(), command.pipeline->Layout(), 0, static_cast<uint32_t>(descriptors.size()), descriptors.data(), 0, nullptr);
}

void Device::FlushBarriers(GPUCommandList cmd) {
    auto& command{ Command(cmd) };

    if (!command.memoryBarriers.empty() || !command.imageBarriers.empty() || !command.bufferBarriers.empty()) {
        command.cmd.pipelineBarrier(vk::PipelineStageFlagBits::eAllCommands, vk::PipelineStageFlagBits::eAllCommands, {}, command.memoryBarriers,
            command.bufferBarriers, command.imageBarriers);

        command.memoryBarriers.clear();
        command.imageBarriers.clear();
        command.bufferBarriers.clear();
    }
}

void Device::BeginDebugLabel(GPUCommandList cmd, const std::string_view& name, const Color& color) {
    if (GraphicsService::SupportsDebug()) {
        vk::DebugUtilsLabelEXT info{};
        info.color[0] = color.r();
        info.color[1] = color.g();
        info.color[2] = color.b();
        info.color[3] = color.a();
        info.pLabelName = name.data();

        Command(cmd).cmd.beginDebugUtilsLabelEXT(info);
    }
}

void Device::EndDebugLabel(GPUCommandList cmd) {
    if (GraphicsService::SupportsDebug()) {
        Command(cmd).cmd.endDebugUtilsLabelEXT();
    }
}

void Device::SetObjectDebugName(uint64_t handle, vk::ObjectType type, const std::string_view& name) {
    if (GraphicsService::SupportsDebug()) {
        vk::DebugUtilsObjectNameInfoEXT nameInfo{};
        nameInfo.objectType = type;
        nameInfo.objectHandle = handle;
        nameInfo.pObjectName = name.data();
        if (GraphicsService::Graphics().Device().setDebugUtilsObjectNameEXT(&nameInfo) != vk::Result::eSuccess) {
            UGINE_WARN("Failed to set debug name '{}'", name);
        }
    }
}

void Device::WaitGpu() {
    GraphicsService::Graphics().GraphicsQueue().waitIdle();
    GraphicsService::Graphics().ComputeQueue().waitIdle();
    cmdStart_ = 0;
    cmdIndex_ = 0;
}

vk::CommandBuffer Device::CommandBuffer(GPUCommandList cmd) {
    return Command(cmd).cmd;
}

void Device::InitSemaphore(GPUSemaphore& semaphore) {
    semaphore.semaphore.resize(GraphicsService::FramesInFlight());
    for (auto& s : semaphore.semaphore) {
        s = gfx_.Device().createSemaphoreUnique({});
    }
}

void Device::DestroySemaphore(GPUSemaphore& semaphore) {
    semaphore.semaphore.clear();
}

vk::PipelineLayout Device::PipelineLayout(GPUCommandList cmd) const {
    UGINE_ASSERT(Command(cmd).pipeline);

    return Command(cmd).pipeline->Layout();
}

vk::RenderPass Device::PresentRenderpass() const {
    return *presentRenderpass_;
}

void Device::Barrier(GPUCommandList cmd, const GPUBarrier& barrier) {
    auto& command{ Command(cmd) };

    switch (barrier.type) {
    case GPUBarrier::Type::Memory:
        command.memoryBarriers.push_back(barrier.memory);
        break;
    case GPUBarrier::Type::Image:
        command.imageBarriers.push_back(barrier.image);
        break;
    case GPUBarrier::Type::Buffer:
        command.bufferBarriers.push_back(barrier.buffer);
        break;
    }
}

void Device::PresentBegin(GPUCommandList cmd, const Color& color) {
    auto extent{ GraphicsService::Presenter().SwapChainExtent() };

    vk::Rect2D scissor{ { 0, 0 }, extent };

    FramebufferKey key{};
    key.renderpass = *presentRenderpass_;
    key.width = extent.width;
    key.height = extent.height;
    key.attachmentCount = 1;
    key.attachments[0] = *GraphicsService::Presenter().SwapChainViews()[GraphicsService::ActiveFrameInFlight()];

    auto framebuffer{ FramebufferCache::Instance().GetOrCreateFramebuffer(key) };

    vk::ClearValue clearValue{};
    ClearColor(clearValue, color);
    BeginRenderPass(cmd, *presentRenderpass_, framebuffer, scissor, 1, &clearValue);
}

void Device::PresentEnd(GPUCommandList cmd, const GPUSemaphore* semaphore) {
    PROFILE_EVENT("Present End");

    EndRenderPass(cmd);

    //Command(cmd).cmd.end();
    utils::Vector<vk::Semaphore> wait;
    utils::Vector<vk::Semaphore> signal;

    if (semaphore) {
        wait.push_back(semaphore->Get());
    }

    wait.push_back(GraphicsService::Presenter().WaitSemaphore());
    signal.push_back(GraphicsService::Presenter().SignalSemaphore());

    SubmitCommandLists(wait, signal, GraphicsService::Presenter().Fence());

    //utils::Vector<vk::Semaphore> wait;
    //if (semaphore) {
    //    wait.push_back(semaphore->Get());
    //}

    //GraphicsService::Presenter().Present(CommandBuffer(cmd), wait);
    GraphicsService::Presenter().Present();

    cmdIndex_ = 0;
    cmdStart_ = 0;
}

vk::DescriptorSet Device::AllocateDescriptor(GPUCommandList cmd, vk::DescriptorSetLayout layout) {
    return Descriptor(cmd).pool->Allocate(layout);
}

bool Device::IsGraphics(GPUCommandList cmd) const {
    return Command(cmd).type == CommandType::Graphics;
}

bool Device::IsCompute(GPUCommandList cmd) const {
    return Command(cmd).type == CommandType::Compute;
}

void Device::BindPipeline(GPUCommandList cmd, const Pipeline* pipeline) {
    auto& command{ Command(cmd) };

    if (command.pipeline != pipeline) {
        command.pipeline = pipeline;
        command.cmd.bindPipeline(command.pipeline->BindPoint(), *(*command.pipeline));

        auto& ds{ Descriptor(cmd) };
        ds.dirty = true;
        for (uint32_t i = 0; i < ds.sets; ++i) {
            // Rebind datasets as stages of newly bound pipeline could have change.
            ds.dataset[i].descriptor = nullptr;
            ds.dataset[i].dirty = true;
        }
    }
}

void Device::Draw(GPUCommandList cmd, uint32_t vertexCount, uint32_t instanceCount, uint32_t vertexStart, uint32_t firstInstance) {
    //UGINE_ASSERT(IsGraphics(cmd));

    //FlushBarriers(cmd);
    BindDescriptors(cmd);
    Command(cmd).cmd.draw(vertexCount, instanceCount, vertexStart, firstInstance);
    ++FrameStats::Instance().drawCallsIssued;
}

void Device::DrawIndexed(GPUCommandList cmd, uint32_t indexCount, uint32_t instanceCount, uint32_t indexStart, uint32_t vertexStart, uint32_t firstInstance) {
    //UGINE_ASSERT(IsGraphics(cmd));

    //FlushBarriers(cmd);
    BindDescriptors(cmd);
    Command(cmd).cmd.drawIndexed(indexCount, instanceCount, indexStart, vertexStart, firstInstance);
    ++FrameStats::Instance().drawCallsIssued;
}

void Device::DrawIndirect(GPUCommandList cmd, vk::Buffer buffer, vk::DeviceSize offset, uint32_t drawCount, uint32_t stride) {
    //UGINE_ASSERT(IsGraphics(cmd));

    //FlushBarriers(cmd);
    BindDescriptors(cmd);
    Command(cmd).cmd.drawIndirect(buffer, offset, drawCount, stride);
    ++FrameStats::Instance().drawCallsIndirect;
}

void Device::DrawIndexedIndirect(GPUCommandList cmd, vk::Buffer buffer, vk::DeviceSize offset, uint32_t drawCount, uint32_t stride) {
    //UGINE_ASSERT(IsGraphics(cmd));

    //FlushBarriers(cmd);
    BindDescriptors(cmd);
    Command(cmd).cmd.drawIndexedIndirect(buffer, offset, drawCount, stride);
    ++FrameStats::Instance().drawCallsIndirect;
}

void Device::Draw(GPUCommandList cmd, const DrawCall& drawCall) {
    if (drawCall.indexCount > 0) {
        DrawIndexed(cmd, drawCall.indexCount, drawCall.instanceCount, drawCall.indexStart, drawCall.vertexOffset, drawCall.firstInstance);
    } else {
        Draw(cmd, drawCall.vertexCount, drawCall.instanceCount, drawCall.vertexOffset, drawCall.firstInstance);
    }
}

void Device::Dispatch(GPUCommandList cmd, uint32_t x, uint32_t y, uint32_t z) {
    //UGINE_ASSERT(IsCompute(cmd));
    UGINE_ASSERT(x > 0 && y > 0 && z > 0);

    FlushBarriers(cmd);
    BindDescriptors(cmd);
    Command(cmd).cmd.dispatch(x, y, z);
    ++FrameStats::Instance().compDispatches;
}

void Device::DispatchIndirect(GPUCommandList cmd, vk::Buffer buffer, vk::DeviceSize offset) {
    //UGINE_ASSERT(IsCompute(cmd));

    FlushBarriers(cmd);
    BindDescriptors(cmd);
    Command(cmd).cmd.dispatchIndirect(buffer, offset);
    ++FrameStats::Instance().passes;
}

void Device::UpdateBuffer(GPUCommandList cmd, vk::Buffer buffer, vk::DeviceSize offset, vk::DeviceSize size, const void* data) {
    UGINE_ASSERT(!Command(cmd).activeRenderPass);

    Command(cmd).cmd.updateBuffer(buffer, offset, size, data);
}

void Device::BeginRenderPass(GPUCommandList cmd, vk::RenderPass renderpass, vk::Framebuffer framebuffer, const vk::Rect2D scissor, uint32_t clearColorCount,
    const vk::ClearValue* clearColor) {
    vk::RenderPassBeginInfo renderPassInfo{ renderpass, framebuffer, scissor, clearColorCount, clearColor };

    ++FrameStats::Instance().passes;

    FlushBarriers(cmd);

    Command(cmd).cmd.beginRenderPass(&renderPassInfo, vk::SubpassContents::eInline);
    Command(cmd).activeRenderPass = renderpass;
}

void Device::EndRenderPass(GPUCommandList cmd) {
    Command(cmd).cmd.endRenderPass();
    Command(cmd).activeRenderPass = nullptr;
}

void Device::SetScissor(GPUCommandList cmd, const vk::Rect2D& scissor) {
    Command(cmd).cmd.setScissor(0, scissor);
}

void Device::SetStencilWriteCompareMask(GPUCommandList cmd, vk::StencilFaceFlags face, uint32_t write, uint32_t compare) {
    auto& command(Command(cmd).cmd);

    command.setStencilWriteMask(face, write);
    command.setStencilCompareMask(face, compare);
}

void Device::SetStencilWriteMask(GPUCommandList cmd, vk::StencilFaceFlags flags, uint32_t value) {
    Command(cmd).cmd.setStencilWriteMask(flags, value);
}

void Device::SetStencilCompareMask(GPUCommandList cmd, vk::StencilFaceFlags flags, uint32_t value) {
    Command(cmd).cmd.setStencilCompareMask(flags, value);
}

void Device::BindVertexIndexBuffer(GPUCommandList cmd, const VertexBuffer& buffer) {
    Command(cmd).cmd.bindVertexBuffers(0, buffer.VertexBuf().GetBuffer(), buffer.VertexBuf().Offset());
    Command(cmd).cmd.bindIndexBuffer(buffer.IndexBuf().GetBuffer(), buffer.IndexBuf().Offset(), buffer.IndexType());
}

void Device::BindVertexBuffer(GPUCommandList cmd, vk::Buffer buffer, const vk::DeviceSize offset) {
    Command(cmd).cmd.bindVertexBuffers(0, buffer, offset);
}

void Device::BindVertexBuffers(GPUCommandList cmd, uint32_t count, const vk::Buffer* buffers, const vk::DeviceSize* offsets) {
    Command(cmd).cmd.bindVertexBuffers(0, count, buffers, offsets);
}

void Device::BindIndexBuffer(GPUCommandList cmd, vk::Buffer buffer, vk::DeviceSize offset, vk::IndexType indexType) {
    Command(cmd).cmd.bindIndexBuffer(buffer, offset, indexType);
}

void Device::PushConstants(GPUCommandList cmd, const GPUPushConstant& constants) {
    auto& command{ Command(cmd) };
    UGINE_ASSERT(command.pipeline);

    command.cmd.pushConstants(command.pipeline->Layout(), constants.stage, constants.offset, constants.size, constants.data);
}

void Device::CopyBuffer(GPUCommandList cmd, vk::Buffer src, vk::Buffer dst, const vk::BufferCopy& range) {
    auto& command{ Command(cmd) };

    command.cmd.copyBuffer(src, dst, range);
}

void Device::SetViewport(GPUCommandList cmd, const vk::Viewport& viewport) {
    Command(cmd).cmd.setViewport(0, viewport);
}

void Device::BindDescriptor(GPUCommandList cmd, uint32_t set, vk::DescriptorSet descriptor) {
    auto& ds{ Descriptor(cmd) };

    UGINE_ASSERT(set < MAX_DATASETS);

    if (ds.dataset[set].descriptor != descriptor) {
        ds.dataset[set].descriptor = descriptor;
        ds.sets = std::max(ds.sets, set + 1);
        ds.dirty = true;
    }
}

void Device::BindUniform(GPUCommandList cmd, uint32_t set, uint32_t binding, const Buffer& buffer) {
    BindUniform(cmd, set, binding, buffer.GetBuffer(), buffer.Offset(), buffer.Size());
}

void Device::BindUniform(GPUCommandList cmd, uint32_t set, uint32_t binding, const vk::DescriptorBufferInfo& buffer) {
    BindUniform(cmd, set, binding, buffer.buffer, buffer.offset, buffer.range);
}

void Device::BindUniform(GPUCommandList cmd, uint32_t set, uint32_t binding, vk::Buffer buffer, vk::DeviceSize offset, vk::DeviceSize size) {
    auto& ds{ Descriptor(cmd) };

    UGINE_ASSERT(set < MAX_DATASETS);
    UGINE_ASSERT(binding < MAX_BINDINGS);

    utils::EnsureSize(ds.dataset[set].BUF[binding], 1);

    //if (ds.dataset[set].CBO[binding] != &buffer) {
    ds.dataset[set].descriptor = nullptr; // Reset descriptor, will issue new allocation.
    ds.dataset[set].bindings = std::max(ds.dataset[set].bindings, binding + 1);
    ds.dataset[set].TYPES[binding] = vk::DescriptorType::eUniformBuffer;
    ds.dataset[set].BUF[binding][0] = vk::DescriptorBufferInfo{ buffer, offset, size };
    ds.dataset[set].bufArrayCnt[binding] = 1;
    ds.dataset[set].dirty = true;
    ds.sets = std::max(ds.sets, set + 1);
    ds.dirty = true;
    //}
}

void Device::BindImageSampler(GPUCommandList cmd, uint32_t set, uint32_t binding, const TextureView& image) {
    BindImageSampler(cmd, set, binding, image.Descriptor());
}

void Device::BindImageSampler(GPUCommandList cmd, uint32_t set, uint32_t binding, const vk::DescriptorImageInfo& image) {
    auto& ds{ Descriptor(cmd) };

    UGINE_ASSERT(set < MAX_DATASETS);
    UGINE_ASSERT(binding < MAX_BINDINGS);

    utils::EnsureSize(ds.dataset[set].IMG[binding], 1);

    //if (ds.dataset[set].IMG[binding] != &image) {
    ds.dataset[set].descriptor = nullptr; // Reset descriptor, will issue new allocation.
    ds.dataset[set].bindings = std::max(ds.dataset[set].bindings, binding + 1);
    ds.dataset[set].TYPES[binding] = vk::DescriptorType::eCombinedImageSampler;
    ds.dataset[set].IMG[binding][0] = image;
    ds.dataset[set].imgArrayCnt[binding] = 1;
    ds.dataset[set].dirty = true;
    ds.sets = std::max(ds.sets, set + 1);
    ds.dirty = true;
    //}
}

void Device::BindImageSampler(GPUCommandList cmd, uint32_t set, uint32_t binding, const std::vector<vk::DescriptorImageInfo>& image) {
    auto& ds{ Descriptor(cmd) };

    UGINE_ASSERT(set < MAX_DATASETS);
    UGINE_ASSERT(binding < MAX_BINDINGS);

    //if (ds.dataset[set].IMG[binding] != &image) {
    utils::EnsureSize(ds.dataset[set].IMG[binding], image.size());

    ds.dataset[set].descriptor = nullptr; // Reset descriptor, will issue new allocation.
    ds.dataset[set].bindings = std::max(ds.dataset[set].bindings, binding + 1);
    ds.dataset[set].TYPES[binding] = vk::DescriptorType::eCombinedImageSampler;
    for (size_t i = 0; i < image.size(); ++i) {
        ds.dataset[set].IMG[binding][i] = image[i];
    }
    ds.dataset[set].imgArrayCnt[binding] = static_cast<uint32_t>(image.size());
    ds.dataset[set].dirty = true;
    ds.sets = std::max(ds.sets, set + 1);
    ds.dirty = true;
    //}
}

void Device::BindStorage(GPUCommandList cmd, uint32_t set, uint32_t binding, const Buffer& buffer) {
    BindStorage(cmd, set, binding, buffer.GetBuffer(), buffer.Offset(), buffer.Size());
}

void Device::BindStorage(GPUCommandList cmd, uint32_t set, uint32_t binding, const vk::DescriptorBufferInfo& buffer) {
    BindStorage(cmd, set, binding, buffer.buffer, buffer.offset, buffer.range);
}

void Device::BindStorage(GPUCommandList cmd, uint32_t set, uint32_t binding, vk::Buffer buffer, vk::DeviceSize offset, vk::DeviceSize size) {
    auto& ds{ Descriptor(cmd) };

    UGINE_ASSERT(set < MAX_DATASETS);
    UGINE_ASSERT(binding < MAX_BINDINGS);

    utils::EnsureSize(ds.dataset[set].BUF[binding], 1);

    //if (ds.dataset[set].SBO[binding] != &buffer) {
    ds.dataset[set].descriptor = nullptr; // Reset descriptor, will issue new allocation.
    ds.dataset[set].bindings = std::max(ds.dataset[set].bindings, binding + 1);
    ds.dataset[set].TYPES[binding] = vk::DescriptorType::eStorageBuffer;
    ds.dataset[set].BUF[binding][0] = vk::DescriptorBufferInfo{ buffer, offset, size };
    ds.dataset[set].bufArrayCnt[binding] = 1;
    ds.dataset[set].dirty = true;
    ds.sets = std::max(ds.sets, set + 1);
    ds.dirty = true;
    //}
}

void Device::BindStorageImage(GPUCommandList cmd, uint32_t set, uint32_t binding, const vk::DescriptorImageInfo& image) {
    auto& ds{ Descriptor(cmd) };

    UGINE_ASSERT(set < MAX_DATASETS);
    UGINE_ASSERT(binding < MAX_BINDINGS);

    utils::EnsureSize(ds.dataset[set].IMG[binding], 1);

    //if (ds.dataset[set].IMG[binding] != &image) {
    ds.dataset[set].descriptor = nullptr; // Reset descriptor, will issue new allocation.
    ds.dataset[set].bindings = std::max(ds.dataset[set].bindings, binding + 1);
    ds.dataset[set].TYPES[binding] = vk::DescriptorType::eStorageImage;
    ds.dataset[set].IMG[binding][0] = image;
    ds.dataset[set].imgArrayCnt[binding] = 1;
    ds.dataset[set].dirty = true;
    ds.sets = std::max(ds.sets, set + 1);
    ds.dirty = true;
    //}
}

void Device::CreatePresentRenderpass() {
    std::vector<vk::AttachmentDescription> attachments;

    // Color attachment.
    vk::AttachmentDescription colorAttachment;
    colorAttachment.format = GraphicsService::Presenter().SwapChainFormat();
    colorAttachment.samples = vk::SampleCountFlagBits::e1;
    colorAttachment.loadOp = vk::AttachmentLoadOp::eClear;
    colorAttachment.storeOp = vk::AttachmentStoreOp::eStore;
    colorAttachment.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
    colorAttachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
    colorAttachment.initialLayout = vk::ImageLayout::eUndefined;
    colorAttachment.finalLayout = vk::ImageLayout::ePresentSrcKHR;

    vk::AttachmentReference colorAttachmentRef;
    colorAttachmentRef.attachment = 0;
    colorAttachmentRef.layout = vk::ImageLayout::eColorAttachmentOptimal;

    vk::SubpassDescription subpass;
    subpass.pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;

    attachments.push_back(colorAttachment);

    vk::SubpassDependency dependency;
    dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
    dependency.dstSubpass = 0;
    dependency.srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
    dependency.srcAccessMask = {};
    dependency.dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
    dependency.dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;

    vk::RenderPassCreateInfo renderPassInfo;
    renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
    renderPassInfo.pAttachments = attachments.data();
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass;
    renderPassInfo.dependencyCount = 1;
    renderPassInfo.pDependencies = &dependency;

    presentRenderpass_ = gfx_.Device().createRenderPassUnique(renderPassInfo);
}

Device::FrameData& Device::Frame() {
    return frameData_[GraphicsService::ActiveFrameInFlight()];
}

const Device::FrameData& Device::Frame() const {
    return frameData_[GraphicsService::ActiveFrameInFlight()];
}

Device::DescriptorSetManager& Device::Descriptor(GPUCommandList cmd) {
    return Command(cmd).descriptor;
}

void Device::ResetDescriptors(GPUCommandList cmd) {
    auto& command{ Command(cmd) };

    command.descriptor.dirty = true;
    command.descriptor.sets = 0;
    for (uint32_t i = 0; i < MAX_DATASETS; ++i) {
        auto& dataset{ command.descriptor.dataset[i] };

        dataset.descriptor = vk::DescriptorSet{};
        dataset.dirty = false;
        dataset.bindings = 0;
        for (uint32_t j = 0; j < MAX_BINDINGS; ++j) {
            dataset.bufArrayCnt[j] = 0;
            dataset.imgArrayCnt[j] = 0;
        }
    }
}

void Device::ResetDescriptor(GPUCommandList cmd, uint32_t set) {
    auto& command(Command(cmd));

    UGINE_ASSERT(set < MAX_DATASETS);

    command.descriptor.dirty = true;
    command.descriptor.dataset[set].dirty = true;
    command.descriptor.dataset[set].bindings = 0;
    command.descriptor.dirty = true;

    if (set == command.descriptor.sets - 1) {
        --command.descriptor.sets;
    }
}

void Device::ResetBinding(GPUCommandList cmd, uint32_t set, uint32_t binding) {
    auto& command(Command(cmd));

    UGINE_ASSERT(set < MAX_DATASETS);
    UGINE_ASSERT(binding < MAX_BINDINGS);

    command.descriptor.dirty = true;
    command.descriptor.dataset[set].descriptor = nullptr;
    command.descriptor.dataset[set].dirty = true;
    command.descriptor.dataset[set].bufArrayCnt[binding] = 0;
    command.descriptor.dataset[set].imgArrayCnt[binding] = 0;
    if (binding == command.descriptor.dataset[set].bindings - 1) {
        --command.descriptor.dataset[set].bindings;
    }
}

void Device::ResetBinding(GPUCommandList cmd, uint32_t set, uint32_t binding, uint32_t count) {
    auto& command(Command(cmd));

    UGINE_ASSERT(set < MAX_DATASETS);
    UGINE_ASSERT(count > 0);
    UGINE_ASSERT(binding + count < MAX_BINDINGS);

    command.descriptor.dirty = true;
    command.descriptor.dataset[set].dirty = true;
    command.descriptor.dataset[set].descriptor = nullptr;

    for (uint32_t i = binding; i < binding + count; ++i) {
        command.descriptor.dataset[set].bufArrayCnt[i] = 0;
        command.descriptor.dataset[set].imgArrayCnt[i] = 0;
    }

    if (binding + count == command.descriptor.dataset[set].bindings) {
        command.descriptor.dataset[set].bindings -= count;
    }
}

Device::CommandData& Device::Command(GPUCommandList cmd) {
    return Frame().command[cmd];
}

const Device::CommandData& Device::Command(GPUCommandList cmd) const {
    return Frame().command[cmd];
}

} // namespace ugine::gfx
