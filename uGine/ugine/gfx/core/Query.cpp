﻿#include "Query.h"

namespace ugine::gfx {

QueryPool::QueryPool(vk::QueryType type, uint32_t queryCount)
    : count_{ queryCount } {
    vk::QueryPoolCreateInfo queryPoolInfo{};
    queryPoolInfo.queryType = type;
    queryPoolInfo.queryCount = queryCount;

    pool_ = GraphicsService::Graphics().Device().createQueryPoolUnique(queryPoolInfo);
    values_.resize(count_);
}

void QueryPool::Reset(vk::CommandBuffer cmd) {
    frameCount_ = 0;
    cmd.resetQueryPool(*pool_, 0, count_);
}

void QueryPool::FetchResults() {
    uint32_t count{ frameCount_.load() };
    if (count) {
        values_ = GraphicsService::Graphics()
                      .Device()
                      .getQueryPoolResults<uint64_t>(*pool_, 0, count, sizeof(uint64_t) * count, sizeof(uint64_t), vk::QueryResultFlagBits::e64)
                      .value;
    }
}

uint32_t QueryPool::Timestamp(vk::CommandBuffer cmd, vk::PipelineStageFlagBits stage) {
    uint32_t query{ frameCount_++ };
    UGINE_ASSERT(query < count_);

    cmd.writeTimestamp(stage, *pool_, query);
    return query;
}

} // namespace ugine::gfx
