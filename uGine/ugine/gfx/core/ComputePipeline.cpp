﻿#include "ComputePipeline.h"

#include <ugine/gfx/Graphics.h>

namespace ugine::gfx {

ComputePipeline ComputePipeline::Load(const std::filesystem::path& csShader) {
    using namespace gfx;

    std::vector<vk::UniqueDescriptorSetLayout> layouts;

    const auto& computeShader{ ShaderCache::GetShader(vk::ShaderStageFlagBits::eCompute, csShader) };
    ShaderProgram shader;
    shader.AddShader(computeShader);
    shader.Link(layouts);

    return ComputePipeline(std::move(layouts), computeShader);
}

ComputePipeline::ComputePipeline(vk::UniquePipelineLayout pipelineLayout, vk::UniquePipeline pipeline, const std::vector<vk::DescriptorSetLayout>& dsLayouts)
    : Pipeline(vk::PipelineBindPoint::eCompute, std::move(pipelineLayout), std::move(pipeline), dsLayouts) {}

ComputePipeline::ComputePipeline(const std::vector<vk::DescriptorSetLayout>& dsLayouts, const Shader& shader)
    : Pipeline(vk::PipelineBindPoint::eCompute, {}, {}, dsLayouts) {
    Init(shader);
}

ComputePipeline::ComputePipeline(std::vector<vk::UniqueDescriptorSetLayout>&& udsLayouts, const Shader& shader)
    : Pipeline(vk::PipelineBindPoint::eCompute, {}, {}, std::move(udsLayouts)) {
    Init(shader);
}

void ComputePipeline::Init(const Shader& shader) {
    bindPoint_ = vk::PipelineBindPoint::eCompute;

    vk::PipelineLayoutCreateInfo pipelineCI{};
    pipelineCI.pSetLayouts = dsLayouts_.data();
    pipelineCI.setLayoutCount = static_cast<uint32_t>(dsLayouts_.size());

    layout_ = GraphicsService::Graphics().Device().createPipelineLayoutUnique(pipelineCI);

    vk::ComputePipelineCreateInfo computePipelineCreateInfo{};
    computePipelineCreateInfo.layout = *layout_;
    computePipelineCreateInfo.stage = shader.CreateInfo();

    pipeline_ = std::move(GraphicsService::Graphics().Device().createComputePipelineUnique({}, computePipelineCreateInfo).value);
}

} // namespace ugine::gfx
