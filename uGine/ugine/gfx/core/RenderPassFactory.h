﻿#pragma once

#include <vulkan/vulkan.hpp>

#include <vector>

namespace ugine::gfx {

class RenderPassFactory {
public:
    RenderPassFactory() = default;

    void AddAttachment(vk::SampleCountFlagBits samples, vk::Format format, vk::AttachmentLoadOp loadOp, vk::AttachmentStoreOp storeOp,
        vk::ImageLayout initLayout = vk::ImageLayout::eUndefined, vk::ImageLayout finalLayout = vk::ImageLayout::eShaderReadOnlyOptimal);

    void AddDepthAttachment(vk::SampleCountFlagBits samples, vk::AttachmentLoadOp loadOp, vk::AttachmentStoreOp storeOp,
        vk::ImageLayout initLayout = vk::ImageLayout::eUndefined, vk::ImageLayout finalLayout = vk::ImageLayout::eDepthReadOnlyOptimal);

    void AddDepthStencilAttachment(vk::SampleCountFlagBits samples, vk::AttachmentLoadOp loadOp, vk::AttachmentStoreOp storeOp,
        vk::AttachmentLoadOp stencilLoadOp, vk::AttachmentStoreOp stencilStoreOp, vk::ImageLayout initLayout = vk::ImageLayout::eUndefined,
        vk::ImageLayout finalLayout = vk::ImageLayout::eDepthStencilReadOnlyOptimal);

    void AddResolve(
        vk::Format format, vk::ImageLayout initialLayout = vk::ImageLayout::eUndefined, vk::ImageLayout finalLayout = vk::ImageLayout::eShaderReadOnlyOptimal);

    void AddDepthResolve(
        vk::ImageLayout initialLayout = vk::ImageLayout::eUndefined, vk::ImageLayout finalLayout = vk::ImageLayout::eDepthStencilReadOnlyOptimal);

    void AddSubpassDependency(uint32_t srcSubpass, uint32_t dstSubpass, vk::PipelineStageFlags srcStage, vk::AccessFlags srcAccessMask,
        vk::PipelineStageFlags dstStage, vk::AccessFlags dstAccessMask, vk::DependencyFlags flags = vk::DependencyFlagBits::eByRegion);

    vk::UniqueRenderPass Create(const std::string_view& name);

private:
    std::vector<vk::AttachmentDescription2> attchmentDescriptions_;
    std::vector<vk::AttachmentReference2> colorReferences_;
    std::vector<vk::AttachmentReference2> depthReferences_;
    std::vector<vk::AttachmentReference2> resolveReferences_;
    std::vector<vk::AttachmentReference2> depthResolveReferences_;
    std::vector<vk::SubpassDescriptionDepthStencilResolve> resolveDepth_;
    std::vector<vk::SubpassDependency2> dependencies_;
};

} // namespace ugine::gfx
