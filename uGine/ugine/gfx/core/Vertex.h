﻿#pragma once

#include <glm/glm.hpp>

#include <vulkan/vulkan.hpp>

#include <vector>

namespace ugine::gfx {

using IndexType = uint16_t;

struct Vertex {
    alignas(16) glm::vec3 pos;
    alignas(16) glm::vec3 normal;
    alignas(16) glm::vec3 tangent;
    alignas(16) glm::vec3 bitangent;
    alignas(16) glm::vec2 tex;
};

struct VertexInstance {
    glm::vec4 model0;
    glm::vec4 model1;
    glm::vec4 model2;
    glm::vec4 model3;
};

static inline constexpr int VertexInputCount{ 5 };
static inline constexpr int InstanceInputCount{ 9 };

void SetupVertexInstance(VertexInstance& instanceData, const glm::mat4& model);

std::vector<vk::VertexInputAttributeDescription> VertexAttributeDescriptions(bool withInstance = true);
std::vector<vk::VertexInputBindingDescription> VertexBindingDescription(bool withInstance = true);

} // namespace ugine::gfx
