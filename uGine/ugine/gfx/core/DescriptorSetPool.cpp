﻿#include "DescriptorSetPool.h"

#include <ugine/core/CoreService.h>

#include <ugine/gfx/Graphics.h>
#include <ugine/gfx/GraphicsService.h>
#include <ugine/gfx/Presenter.h>

#include <ugine/utils/Assert.h>

#include <iterator>

namespace ugine::gfx {

DescriptorSetPool::DescriptorSetPool(vk::DescriptorSetLayout layout, const std::vector<vk::DescriptorPoolSize>& sizes, uint32_t singlePoolSize)
    : descriptorSetLayout_(layout)
    , sizes_(sizes)
    , singlePoolSize_(singlePoolSize) {
    CreateNewPool();
}

vk::DescriptorSet DescriptorSetPool::Allocate() {
    if (availableSets_.empty()) {
        if (!TryPendingSets()) {
            CreateNewPool();
        }
        UGINE_ASSERT(!availableSets_.empty());
    }

    vk::DescriptorSet descriptor{ availableSets_.back() };
    UGINE_ASSERT(descriptor != VK_NULL_HANDLE);

    availableSets_.pop_back();

    return descriptor;
}

void DescriptorSetPool::Release(vk::DescriptorSet descriptor, bool immediately) {
    UGINE_ASSERT(descriptor != VK_NULL_HANDLE);

    if (immediately) {
        availableSets_.push_back(descriptor);
    } else {
        pendingSets_.push_back(std::make_pair(core::CoreService::Frame(), descriptor));
    }
}

bool DescriptorSetPool::TryPendingSets() {
    if (pendingSets_.empty() || core::CoreService::Frame() < GraphicsService::FramesInFlight()) {
        return false;
    }

    const auto safeFrame{ core::CoreService::Frame() - GraphicsService::FramesInFlight() };
    const auto firstUnsafe{ safeFrame + 1 };

    // Shortcut - if last pending descriptor is safe, move everything to available.
    if (pendingSets_.back().first < firstUnsafe) {
        std::transform(pendingSets_.begin(), pendingSets_.end(), std::back_inserter(availableSets_), [](const auto& s) { return s.second; });
        pendingSets_.clear();
        return true;
    }

    // Find first descriptor that's NOT safe -  i.e. is potentionally still in flight (it's always sorted by framenum, use binary search).
    auto firstInFlight{ std::lower_bound(pendingSets_.begin(), pendingSets_.end(), firstUnsafe, [](const auto& p1, auto val) { return p1.first < val; }) };

    if (firstInFlight != pendingSets_.end()) {
        // If it's not the first one move everything BEFORE to available.
        UGINE_ASSERT(firstInFlight == pendingSets_.end() || firstInFlight->first > safeFrame);
        UGINE_ASSERT(firstInFlight == pendingSets_.end() || (firstInFlight - 1)->first <= safeFrame);

#ifdef UGINE_SANITY_CHECK
        for (auto it = pendingSets_.begin(); it != firstInFlight; ++it) {
            UGINE_ASSERT(it->first <= safeFrame);
        }
#endif

        std::transform(pendingSets_.begin(), firstInFlight, std::back_inserter(availableSets_), [](const auto& s) { return s.second; });
        pendingSets_.erase(pendingSets_.begin(), firstInFlight);
        return true;
    }

    return false;
}

void DescriptorSetPool::CreateNewPool() {
    vk::DescriptorPoolCreateInfo poolInfo{};
    poolInfo.pPoolSizes = sizes_.data();
    poolInfo.poolSizeCount = static_cast<uint32_t>(sizes_.size());
    poolInfo.maxSets = singlePoolSize_;

    auto pool{ GraphicsService::Graphics().Device().createDescriptorPoolUnique(poolInfo) };
    CreateDescriptorSets(*pool);
    pools_.push_back(std::move(pool));
}

void DescriptorSetPool::CreateDescriptorSets(vk::DescriptorPool pool) {
    UGINE_ASSERT(availableSets_.empty());

    auto vkDevice{ GraphicsService::Graphics().Device() };

    std::vector<vk::DescriptorSetLayout> layouts(singlePoolSize_, descriptorSetLayout_);

    vk::DescriptorSetAllocateInfo allocInfo;
    allocInfo.descriptorPool = pool;
    allocInfo.descriptorSetCount = singlePoolSize_;
    allocInfo.pSetLayouts = layouts.data();

    auto availableSets{ vkDevice.allocateDescriptorSets(allocInfo) };
    std::copy(availableSets.begin(), availableSets.end(), std::back_inserter(availableSets_));
}

DescriptorSetPoolNew::DescriptorSetPoolNew(const std::vector<vk::DescriptorPoolSize>& sizes, uint32_t singlePoolSize)
    : sizes_{ sizes }
    , singlePoolSize_{ singlePoolSize } {
    CreatePool();
}

DescriptorSetPoolNew::~DescriptorSetPoolNew() {
    DestroyPool();
}

vk::DescriptorSet DescriptorSetPoolNew::Allocate(vk::DescriptorSetLayout layout) {
    vk::DescriptorSetAllocateInfo allocInfo;
    allocInfo.descriptorPool = *pool_;
    allocInfo.descriptorSetCount = 1;
    allocInfo.pSetLayouts = &layout;

    do {
        try {
            auto ds{ GraphicsService::Graphics().Device().allocateDescriptorSets(allocInfo)[0] };
            return ds;
        } catch (...) {
            // TODO:
            //DestroyPool();
            // singlePoolSize_ <<= 1;
            //CreatePool();
        }
    } while (true);
}

void DescriptorSetPoolNew::Reset() {
    GraphicsService::Graphics().Device().resetDescriptorPool(*pool_, {});
}

void DescriptorSetPoolNew::DestroyPool() {
    SAFE_DELETE(pool_);
}

void DescriptorSetPoolNew::CreatePool() {
    vk::DescriptorPoolCreateInfo poolInfo{};
    poolInfo.pPoolSizes = sizes_.data();
    poolInfo.poolSizeCount = static_cast<uint32_t>(sizes_.size());
    poolInfo.maxSets = singlePoolSize_;

    pool_ = GraphicsService::Graphics().Device().createDescriptorPoolUnique(poolInfo);
}

} // namespace ugine::gfx
