﻿#pragma once

#include "Buffer.h"

#include <ugine/gfx/Graphics.h>

namespace ugine::gfx {

template <typename T> class TypedBuffer : public Buffer {
public:
    TypedBuffer() {}
    explicit TypedBuffer()
        : Buffer(sizeof(T), vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent) {}

    void Update(Graphics& gfx, const T& t) {
        if (mapped_) {
            memcpy(mapped_, &t, sizeof(T));
        } else {
            auto data{ Map(gfx) };
            memcpy(data, &t, sizeof(T));
            Unmap(gfx);
        }
    }
};

} // namespace ugine::gfx
