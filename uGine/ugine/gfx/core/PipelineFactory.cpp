﻿#include "PipelineFactory.h"

#include <ugine/gfx/Graphics.h>
#include <ugine/gfx/GraphicsService.h>

#include <ugine/gfx/Error.h>

namespace ugine::gfx {

PipelineFactory::PipelineFactory() {
    vertexInputInfo.vertexBindingDescriptionCount = 0;
    vertexInputInfo.vertexAttributeDescriptionCount = 0;

    inputAssembly.topology = vk::PrimitiveTopology::eTriangleList;
    inputAssembly.primitiveRestartEnable = VK_FALSE;

    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewPort;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;

    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    rasterizer.polygonMode = vk::PolygonMode::eFill;
    rasterizer.lineWidth = 1.0f;
    rasterizer.cullMode = vk::CullModeFlagBits::eBack;
    rasterizer.frontFace = vk::FrontFace::eClockwise;
    rasterizer.depthBiasEnable = VK_FALSE;

    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = vk::SampleCountFlagBits::e1;

    colorBlendAttachment.blendEnable = VK_FALSE;
    colorBlendAttachment.colorWriteMask
        = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;

    colorBlending.logicOpEnable = VK_FALSE;
    colorBlending.logicOp = vk::LogicOp::eCopy;
    colorBlending.attachmentCount = 0;

    depthStencil.depthTestEnable = VK_TRUE;
    depthStencil.depthWriteEnable = VK_TRUE;
    depthStencil.depthCompareOp = vk::CompareOp::eLessOrEqual;

    depthStencil.stencilTestEnable = VK_TRUE;
    depthStencil.front.compareOp = vk::CompareOp::eAlways;
    depthStencil.front.depthFailOp = vk::StencilOp::eReplace;
    depthStencil.front.failOp = vk::StencilOp::eReplace;
    depthStencil.front.passOp = vk::StencilOp::eReplace;
    depthStencil.front.compareMask = 0xff;
    depthStencil.front.writeMask = 0xff;
    depthStencil.front.reference = 1;
    depthStencil.back = depthStencil.front;

    dynamicState.dynamicStateCount = 0;

    pipelineInfo.stageCount = 0;
    pipelineInfo.pDynamicState = &dynamicState;
    pipelineInfo.pVertexInputState = &vertexInputInfo;
    pipelineInfo.pInputAssemblyState = &inputAssembly;
    pipelineInfo.pViewportState = &viewportState;
    pipelineInfo.pRasterizationState = &rasterizer;
    pipelineInfo.pMultisampleState = &multisampling;
    pipelineInfo.pColorBlendState = &colorBlending;
    pipelineInfo.pDepthStencilState = &depthStencil;
}

vk::UniquePipeline PipelineFactory::Create(vk::PipelineLayout layout, vk::RenderPass renderPass) {
    UGINE_ASSERT(renderPass);

    pipelineInfo.layout = layout;
    pipelineInfo.renderPass = renderPass;

    auto result{ GraphicsService::Graphics().Device().createGraphicsPipelineUnique({}, pipelineInfo) };

    if (result.result != vk::Result::eSuccess) {
        UGINE_THROW(GfxError, "Failed to create pipeline.", result.result);
    }

    return std::move(result.value);
}

vk::UniquePipeline PipelineFactory::Create(vk::PipelineLayout layout, vk::RenderPass renderPass, uint32_t attachmentCount) {
    UGINE_ASSERT(renderPass);

    std::vector<vk::PipelineColorBlendAttachmentState> blendAttachments(attachmentCount, colorBlendAttachment);
    colorBlending.attachmentCount = static_cast<uint32_t>(blendAttachments.size());
    colorBlending.pAttachments = blendAttachments.data();

    return Create(layout, renderPass);
}

void PipelineFactory::SetVertexInput(const vk::VertexInputBindingDescription* binding, const vk::VertexInputAttributeDescription* attribute) {
    vertexInputInfo.vertexBindingDescriptionCount = 1;
    vertexInputInfo.pVertexBindingDescriptions = binding;
    vertexInputInfo.vertexAttributeDescriptionCount = 1;
    vertexInputInfo.pVertexAttributeDescriptions = attribute;
}

void PipelineFactory::SetVertexInput(
    uint32_t bindingCount, const vk::VertexInputBindingDescription* binding, uint32_t attributeCount, const vk::VertexInputAttributeDescription* attribute) {
    vertexInputInfo.vertexBindingDescriptionCount = bindingCount;
    vertexInputInfo.pVertexBindingDescriptions = binding;
    vertexInputInfo.vertexAttributeDescriptionCount = attributeCount;
    vertexInputInfo.pVertexAttributeDescriptions = attribute;
}

void PipelineFactory::SetShaderStages(uint32_t count, const vk::PipelineShaderStageCreateInfo* stages) {
    pipelineInfo.stageCount = count;
    pipelineInfo.pStages = stages;
}

void PipelineFactory::SetShaderStages(const std::vector<vk::PipelineShaderStageCreateInfo>& stages) {
    pipelineInfo.stageCount = static_cast<uint32_t>(stages.size());
    pipelineInfo.pStages = stages.data();
}

void PipelineFactory::SetDynamicStates(uint32_t count, const vk::DynamicState* states) {
    dynamicState.dynamicStateCount = count;
    dynamicState.pDynamicStates = states;
}

} // namespace ugine::gfx
