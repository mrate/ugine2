﻿#include "Texture.h"

#include "Buffer.h"

#include <ugine/core/ConfigService.h>
#include <ugine/gfx/Error.h>
#include <ugine/gfx/Graphics.h>
#include <ugine/gfx/GraphicsService.h>
#include <ugine/gfx/data/SamplerCache.h>

#include <ugine/utils/KtxRead.h>
#include <ugine/utils/Log.h>
#include <ugine/utils/StbImageWrapper.h>

namespace {

inline bool IsKtxTexture(const std::filesystem::path& file) {
    return file.extension() == ".ktx" || file.extension() == ".ktx2";
}

inline bool IsHdrTexture(const std::filesystem::path& file) {
    return file.extension() == ".hdr";
}

inline bool IsDepthStencil(vk::ImageAspectFlags aspect) {
    return aspect == vk::ImageAspectFlagBits::eDepth || aspect == vk::ImageAspectFlagBits::eStencil;
}

} // namespace

namespace ugine::gfx {

TextureView TextureView::CreateSampled(
    const gfx::Texture& texture, vk::ImageViewType type, vk::ImageAspectFlags imageAspect, uint32_t baseLayer, uint32_t layers) {
    auto view{ Create(texture, type, imageAspect, baseLayer, layers) };
    auto sampler{ SamplerCache::Sampler(SamplerCache::Type::LinearRepeatBorderWhite, texture.MipLevels()) };
    GraphicsService::Device().SetObjectDebugName(sampler->get(), texture.DebugName());

    view.SetSampler(sampler);
    view.InitDescriptor(IsDepthStencil(imageAspect) ? vk::ImageLayout::eDepthStencilReadOnlyOptimal : vk::ImageLayout::eShaderReadOnlyOptimal);
    return view;
}

TextureView TextureView::CreateSampled(const Texture& texture, vk::ImageAspectFlags imageAspect) {
    auto view{ Create(texture, imageAspect) };
    auto sampler{ SamplerCache::Sampler(SamplerCache::Type::LinearRepeatBorderWhite, texture.MipLevels()) };
    GraphicsService::Device().SetObjectDebugName(sampler->get(), texture.DebugName());
    view.SetSampler(sampler);
    view.InitDescriptor(IsDepthStencil(imageAspect) ? vk::ImageLayout::eDepthStencilReadOnlyOptimal : vk::ImageLayout::eShaderReadOnlyOptimal);
    return view;
}

TextureView TextureView::CreateSampled(
    const gfx::Texture& texture, const SamplerRef& sampler, vk::ImageViewType type, vk::ImageAspectFlags imageAspect, uint32_t baseLayer, uint32_t layers) {
    auto view{ Create(texture, type, imageAspect, baseLayer, layers) };
    if (sampler) {
        view.SetSampler(sampler);
    } else {
        auto newSampler{ SamplerCache::Sampler(SamplerCache::Type::LinearRepeatBorderWhite, texture.MipLevels()) };
        GraphicsService::Device().SetObjectDebugName(newSampler->get(), texture.DebugName());
        view.SetSampler(newSampler);
    }
    view.InitDescriptor(IsDepthStencil(imageAspect) ? vk::ImageLayout::eDepthStencilReadOnlyOptimal : vk::ImageLayout::eShaderReadOnlyOptimal);
    return view;
}

TextureView TextureView::Create(const Texture& texture, vk::ImageViewType type, vk::ImageAspectFlags imageAspect, uint32_t baseLayer, uint32_t layers) {
    if (layers == 0) {
        layers = texture.Layers();
    }

    vk::ImageSubresourceRange subresourceRange{};
    subresourceRange.aspectMask = imageAspect;
    subresourceRange.baseMipLevel = 0;
    subresourceRange.levelCount = texture.MipLevels();
    subresourceRange.baseArrayLayer = baseLayer;
    subresourceRange.layerCount = layers;

    vk::ImageViewCreateInfo imageViewCI{};
    imageViewCI.image = texture.Image();
    imageViewCI.viewType = type;
    imageViewCI.format = texture.Format();
    imageViewCI.subresourceRange = subresourceRange;

    TextureView view{ GraphicsService::Graphics().Device().createImageViewUnique(imageViewCI), {}, texture.Extent(), texture.Format() };
    view.InitDescriptor(IsDepthStencil(imageAspect) ? vk::ImageLayout::eDepthStencilReadOnlyOptimal : vk::ImageLayout::eShaderReadOnlyOptimal);

    return view;
}

TextureView TextureView::Create(const Texture& texture, vk::ImageAspectFlags imageAspect) {
    vk::ImageViewType type{ vk::ImageViewType::e2D };
    if (texture.IsCube()) {
        type = vk::ImageViewType::eCube;
    } else if (texture.Layers() > 1) {
        type = vk::ImageViewType::e2DArray;
    }

    return Create(texture, type, imageAspect, 0, 0);
}

void TextureView::SetName(const std::string_view& name) {
    GraphicsService::Device().SetObjectDebugName(*view_, name);
}

void TextureView::InitDescriptor(vk::ImageLayout layout) {
    descriptor_.imageLayout = layout;
    descriptor_.sampler = sampler_ ? *(*sampler_) : nullptr;
    descriptor_.imageView = *view_;
}

// Texture
int Texture::TotalAllocation{ 0 };

Texture::Texture(Texture&& other) {
    *this = std::move(other);
}

Texture& Texture::operator=(Texture&& other) {
    Free();
    *this = other;
    other.allocation_ = nullptr;
    return *this;
}

Texture::Texture(
    const vk::Extent3D& extent, vk::Format format, vk::ImageTiling tiling, vk::ImageUsageFlags usage, vk::SampleCountFlagBits sampleCount, uint32_t mipLevels)
    : extent_(extent)
    , format_(format)
    , samples_(sampleCount)
    , mipLevels_(mipLevels)
    , layers_(1) {

    vk::ImageCreateInfo imageCI{};
    imageCI.imageType = vk::ImageType::e2D;
    imageCI.extent = extent_;
    imageCI.mipLevels = mipLevels_;
    imageCI.arrayLayers = layers_;
    imageCI.format = format_;
    imageCI.tiling = tiling;
    imageCI.initialLayout = vk::ImageLayout::eUndefined;
    imageCI.usage = usage;
    imageCI.samples = sampleCount;
    imageCI.sharingMode = vk::SharingMode::eExclusive;

    CreateImage(imageCI);
}

Texture::Texture(const vk::ImageCreateInfo& createInfo)
    : extent_(createInfo.extent)
    , format_(createInfo.format)
    , samples_(createInfo.samples)
    , mipLevels_(createInfo.mipLevels)
    , layers_(createInfo.arrayLayers) {

    CreateImage(createInfo);

    if (createInfo.flags & vk::ImageCreateFlagBits::eCubeCompatible) {
        isCubeMap_ = true;
    }
}

void Texture::GenerateMips(GPUCommandList command, vk::ImageLayout initialLayout, vk::ImageLayout finalLayout) {
    GenerateMipMaps(command, mipLevels_, initialLayout, finalLayout);
}

Texture::~Texture() {
    Free();
}

void Texture::SetName(const std::string_view& name) {
    debugName_ = name;
    GraphicsService::Device().SetObjectDebugName(image_, name);
}

void Texture::CreateImage(const vk::ImageCreateInfo& imageCI) {
    vma::AllocationCreateInfo ibAllocCreateInfo = {};
    ibAllocCreateInfo.usage = vma::MemoryUsage::eGpuOnly;
    ibAllocCreateInfo.flags = vma::AllocationCreateFlagBits::eMapped;
    auto result{ GraphicsService::Graphics().Allocator().createImage(imageCI, ibAllocCreateInfo) };
    image_ = result.first;
    allocation_ = result.second;

    ++TotalAllocation;
    UGINE_TRACE("Allocate texture: {}", TotalAllocation);
}

void Texture::SetData(const std::vector<Data>& data, vk::ImageLayout initialLayout, vk::ImageLayout finalLayout) {
    const vk::MemoryPropertyFlags memProperties{ vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent };

    vk::DeviceSize size = 0;
    for (const auto& s : data) {
        size += s.size;
    }

    Buffer stagingBuffer(size, vk::BufferUsageFlagBits::eTransferSrc, memProperties);

    auto mappedData{ reinterpret_cast<char*>(stagingBuffer.Map()) };

    uint32_t offset{ 0 };
    for (const auto& d : data) {
        memcpy(mappedData + offset, d.data, d.size);
        offset += static_cast<uint32_t>(d.size);
    }

    stagingBuffer.Unmap();

    // Copy image data.
    auto command{ GraphicsService::Device().BeginCommandList() };

    Transition(command, initialLayout, vk::ImageLayout::eTransferDstOptimal);

    std::vector<vk::BufferImageCopy> regions{};

    offset = 0;
    for (size_t i = 0; i < data.size(); ++i) {
        vk::BufferImageCopy region{};
        region.bufferOffset = offset;
        region.bufferRowLength = 0;
        region.bufferImageHeight = 0;

        region.imageSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
        region.imageSubresource.mipLevel = 0;
        region.imageSubresource.baseArrayLayer = static_cast<uint32_t>(i);
        region.imageSubresource.layerCount = 1;

        region.imageOffset = vk::Offset3D{ 0, 0, 0 };
        region.imageExtent = extent_;

        regions.push_back(region);

        offset += static_cast<uint32_t>(data[i].size);
    }

    GraphicsService::Device().CommandBuffer(command).copyBufferToImage(*stagingBuffer, image_, vk::ImageLayout::eTransferDstOptimal, regions);

    Transition(command, vk::ImageLayout::eTransferDstOptimal, finalLayout);

    GraphicsService::Device().SubmitCommandLists();
    GraphicsService::Device().WaitGpu();
}

void Texture::GenerateMipMaps(GPUCommandList command, uint32_t mipLevels, vk::ImageLayout initialLayout, vk::ImageLayout finalLayout) {
    UGINE_TRACE("Generating mip levels...");

    // Check if image format supports linear blitting
    //vk::FormatProperties formatProperties;
    //vkGetPhysicalDeviceFormatProperties(physicalDevice, imageFormat, &formatProperties);

    //if (!(formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT)) {
    //    throw std::runtime_error("texture image format does not support linear blitting!");
    //}

    auto& device{ GraphicsService::Device() };

    vk::ImageMemoryBarrier barrier{};
    barrier.image = image_;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = layers_;
    barrier.subresourceRange.levelCount = 1;

    int32_t mipWidth{ static_cast<int32_t>(extent_.width) };
    int32_t mipHeight{ static_cast<int32_t>(extent_.height) };

    for (uint32_t i = 1; i < mipLevels; i++) {
        barrier.subresourceRange.baseMipLevel = i - 1;
        barrier.oldLayout = initialLayout;
        barrier.newLayout = vk::ImageLayout::eTransferSrcOptimal;
        barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
        barrier.dstAccessMask = vk::AccessFlagBits::eTransferRead;

        device.CommandBuffer(command).pipelineBarrier(
            vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eTransfer, {}, nullptr, nullptr, barrier);

        vk::ImageBlit blit{};
        blit.srcOffsets[0] = vk::Offset3D{ 0, 0, 0 };
        blit.srcOffsets[1] = vk::Offset3D{ mipWidth, mipHeight, 1 };
        blit.srcSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
        blit.srcSubresource.mipLevel = i - 1;
        blit.srcSubresource.baseArrayLayer = 0;
        blit.srcSubresource.layerCount = layers_;
        blit.dstOffsets[0] = vk::Offset3D{ 0, 0, 0 };
        blit.dstOffsets[1] = vk::Offset3D{ mipWidth > 1 ? mipWidth / 2 : 1, mipHeight > 1 ? mipHeight / 2 : 1, 1 };
        blit.dstSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
        blit.dstSubresource.mipLevel = i;
        blit.dstSubresource.baseArrayLayer = 0;
        blit.dstSubresource.layerCount = layers_;

        device.CommandBuffer(command).blitImage(
            image_, vk::ImageLayout::eTransferSrcOptimal, image_, vk::ImageLayout::eTransferDstOptimal, blit, vk::Filter::eLinear);

        barrier.oldLayout = vk::ImageLayout::eTransferSrcOptimal;
        barrier.newLayout = finalLayout;
        barrier.srcAccessMask = vk::AccessFlagBits::eTransferRead;
        barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

        device.CommandBuffer(command).pipelineBarrier(
            vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader, {}, nullptr, nullptr, barrier);

        if (mipWidth > 1) {
            mipWidth >>= 1;
        }

        if (mipHeight > 1) {
            mipHeight >>= 1;
        }
    }

    barrier.subresourceRange.baseMipLevel = mipLevels - 1;
    barrier.oldLayout = initialLayout;
    barrier.newLayout = finalLayout;
    barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
    barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

    device.CommandBuffer(command).pipelineBarrier(
        vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader, {}, nullptr, nullptr, barrier);
}

void Texture::Free() {
    if (allocation_) {
        GraphicsService::Graphics().Allocator().destroyImage(image_, allocation_);
        allocation_ = nullptr;

        --TotalAllocation;
        UGINE_TRACE("Free texture: {}", TotalAllocation);
    }
}

uint32_t Texture::CalculateMipLevels(const vk::Extent3D& extent) {
    return static_cast<uint32_t>(std::floor(std::log2(std::max(extent.width, extent.height)))) + 1;
}

void Texture::CopyTo(
    GPUCommandList command, Texture& target, vk::ImageLayout srcLayout, vk::ImageLayout dstLayout, vk::ImageAspectFlags aspect, vk::Filter filter) const {
    if (srcLayout != vk::ImageLayout::eTransferSrcOptimal) {
        Transition(command, srcLayout, vk::ImageLayout::eTransferSrcOptimal, aspect);
    }
    if (dstLayout != vk::ImageLayout::eTransferDstOptimal) {
        target.Transition(command, dstLayout, vk::ImageLayout::eTransferDstOptimal, aspect);
    }

    vk::ImageBlit regions{};
    regions.srcOffsets[0].x = 0;
    regions.srcOffsets[0].y = 0;
    regions.srcOffsets[0].z = 0;
    regions.srcOffsets[1].x = extent_.width;
    regions.srcOffsets[1].y = extent_.height;
    regions.srcOffsets[1].z = 1;
    regions.srcSubresource.layerCount = 1;
    regions.srcSubresource.mipLevel = 0;
    regions.srcSubresource.aspectMask = aspect;

    regions.dstOffsets[0].x = 0;
    regions.dstOffsets[0].y = 0;
    regions.dstOffsets[0].z = 0;
    regions.dstOffsets[1].x = target.extent_.width;
    regions.dstOffsets[1].y = target.extent_.height;
    regions.dstOffsets[1].z = 1;
    regions.dstSubresource.layerCount = 1;
    regions.dstSubresource.mipLevel = 0;
    regions.dstSubresource.aspectMask = aspect;

    GraphicsService::Device().CommandBuffer(command).blitImage(
        image_, vk::ImageLayout::eTransferSrcOptimal, target.image_, vk::ImageLayout::eTransferDstOptimal, regions, filter);

    if (srcLayout != vk::ImageLayout::eTransferSrcOptimal) {
        Transition(command, vk::ImageLayout::eTransferSrcOptimal, srcLayout, aspect);
    }
    if (dstLayout != vk::ImageLayout::eTransferDstOptimal) {
        target.Transition(command, vk::ImageLayout::eTransferDstOptimal, dstLayout, aspect);
    }
}

void Texture::Transition(GPUCommandList command, vk::ImageLayout oldLayout, vk::ImageLayout newLayout, vk::ImageAspectFlags aspect) const {
    UGINE_ASSERT(newLayout == vk::ImageLayout::eShaderReadOnlyOptimal || newLayout == vk::ImageLayout::eTransferSrcOptimal
        || newLayout == vk::ImageLayout::eTransferDstOptimal || newLayout == vk::ImageLayout::eGeneral || newLayout == vk::ImageLayout::eColorAttachmentOptimal
        || newLayout == vk::ImageLayout::eDepthStencilReadOnlyOptimal || newLayout == vk::ImageLayout::eDepthStencilAttachmentOptimal);

    vk::ImageMemoryBarrier barrier{};
    barrier.oldLayout = oldLayout;
    barrier.newLayout = newLayout;
    barrier.srcQueueFamilyIndex = {};
    barrier.dstQueueFamilyIndex = {};
    barrier.image = image_;
    barrier.subresourceRange.aspectMask = aspect;
    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = mipLevels_;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = layers_;

    vk::PipelineStageFlags srcStageMask{ vk::PipelineStageFlagBits::eAllCommands };
    vk::PipelineStageFlags dstStageMask{ vk::PipelineStageFlagBits::eAllCommands };

    // Source layouts (old)
    // Source access mask controls actions that have to be finished on the old layout
    // before it will be transitioned to the new layout
    switch (oldLayout) {
    case vk::ImageLayout::eUndefined:
        // Image layout is undefined (or does not matter)
        // Only valid as initial layout
        // No flags required, listed only for completeness
        barrier.srcAccessMask = {};
        break;

    case vk::ImageLayout::ePreinitialized:
        // Image is preinitialized
        // Only valid as initial layout for linear images, preserves memory contents
        // Make sure host writes have been finished
        barrier.srcAccessMask = vk::AccessFlagBits::eHostWrite;
        break;

    case vk::ImageLayout::eColorAttachmentOptimal:
        // Image is a color attachment
        // Make sure any writes to the color buffer have been finished
        barrier.srcAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;
        break;

    case vk::ImageLayout::eDepthStencilAttachmentOptimal:
        // Image is a depth/stencil attachment
        // Make sure any writes to the depth/stencil buffer have been finished
        barrier.srcAccessMask = vk::AccessFlagBits::eDepthStencilAttachmentWrite;
        break;

    case vk::ImageLayout::eTransferSrcOptimal:
        // Image is a transfer source
        // Make sure any reads from the image have been finished
        barrier.srcAccessMask = vk::AccessFlagBits::eTransferRead;
        break;

    case vk::ImageLayout::eTransferDstOptimal:
        // Image is a transfer destination
        // Make sure any writes to the image have been finished
        barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
        break;

    case vk::ImageLayout::eShaderReadOnlyOptimal:
        // Image is read by a shader
        // Make sure any shader reads from the image have been finished
        barrier.srcAccessMask = vk::AccessFlagBits::eShaderRead;
        break;
    default:
        // Other source layouts aren't handled (yet)
        break;
    }

    switch (newLayout) {
    case vk::ImageLayout::eTransferSrcOptimal:
    case vk::ImageLayout::eTransferDstOptimal:
        barrier.dstAccessMask = newLayout == vk::ImageLayout::eTransferSrcOptimal ? vk::AccessFlagBits::eTransferRead : vk::AccessFlagBits::eTransferWrite;
        //srcStageMask = vk::PipelineStageFlagBits::eTopOfPipe;
        //dstStageMask = vk::PipelineStageFlagBits::eTransfer;
        break;
    case vk::ImageLayout::eShaderReadOnlyOptimal:
        barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;
        //srcStageMask = vk::PipelineStageFlagBits::eTransfer;
        //dstStageMask = vk::PipelineStageFlagBits::eFragmentShader;
        break;
    case vk::ImageLayout::eGeneral:
        barrier.dstAccessMask = {};
        break;
    case vk::ImageLayout::eColorAttachmentOptimal:
        barrier.dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;
        break;
    case vk::ImageLayout::eDepthStencilAttachmentOptimal:
        barrier.dstAccessMask = vk::AccessFlagBits::eDepthStencilAttachmentWrite;
        break;
    case vk::ImageLayout::eDepthStencilReadOnlyOptimal:
        barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;
        break;
    default:
        UGINE_ASSERT(false && "Implement me!");
        break;
    }

    GraphicsService::Device().CommandBuffer(command).pipelineBarrier(srcStageMask, dstStageMask, {}, nullptr, nullptr, barrier);
}

Texture Texture::FromFile(const std::filesystem::path& file, vk::ImageUsageFlags usage, bool generateMips, vk::ImageLayout finalLayout) {
    UGINE_TRACE("Loading texture from file '{}'...", file.string());

    // TODO: Use gfx::Image
    // Load image.
    utils::StbiScopeHolder stbPixels;
    utils::KtxScopeHolder ktxPixels;

    vk::Format format{ vk::Format::eR8G8B8A8Unorm };
    size_t pixelSize{ 4 };
    uint32_t layers{ 1 };

    std::vector<Data> data = {};

    int texWidth{};
    int texHeight{};
    if (IsKtxTexture(file)) {
        ktxResult result = ktxTexture_CreateFromNamedFile(file.string().c_str(), KTX_TEXTURE_CREATE_LOAD_IMAGE_DATA_BIT, &ktxPixels.texture);
        if (result != KTX_SUCCESS) {
            auto msg{ std::string("Failed to load texture: ") + file.string() };
            UGINE_THROW(GfxError, msg.c_str());
        }

        auto rawData{ ktxTexture_GetData(ktxPixels.texture) };
        texWidth = ktxPixels.texture->baseWidth;
        texHeight = ktxPixels.texture->baseHeight;

        pixelSize = ktxPixels.texture->dataSize / texWidth / texHeight / ktxPixels.texture->baseDepth;

        if (ktxPixels.texture->isArray) {
            layers = ktxPixels.texture->numLayers;
            pixelSize /= layers;
        }

        switch (pixelSize) {
        case 1:
            format = vk::Format::eR8Unorm;
            break;
        case 2:
            format = vk::Format::eR8G8Unorm;
            break;
        case 3:
            format = vk::Format::eR8G8B8Unorm;
            break;
        case 4:
            format = vk::Format::eR8G8B8A8Unorm;
            break;
        default:
            UGINE_THROW(GfxError, "Invalid texture format.");
            break;
        }

        UGINE_ASSERT(ktxPixels.texture->numLevels == 1);
        auto layerSize{ static_cast<vk::DeviceSize>(texWidth * texHeight * pixelSize) };

        if (ktxPixels.texture->isArray) {
            for (uint32_t i = 0; i < layers; ++i) {
                ktx_size_t offset;

                KTX_error_code ret = ktxTexture_GetImageOffset(ktxPixels.texture, 0, i, 0, &offset);
                if (ret != KTX_SUCCESS) {
                    UGINE_THROW(GfxError, "Failed to get image offset.");
                }

                data.push_back({ rawData + offset, layerSize });
            }
        } else {
            data.push_back({ rawData, layerSize });
        }

    } else {
        int texChannels{};

        if (IsHdrTexture(file) || stbi_is_hdr(file.string().c_str())) {
            format = vk::Format::eR32G32B32A32Sfloat;
            pixelSize = sizeof(float) * 4;
            stbPixels.data = reinterpret_cast<unsigned char*>(stbi_loadf(file.string().c_str(), &texWidth, &texHeight, &texChannels, STBI_rgb_alpha));
        } else {
            stbPixels.data = stbi_load(file.string().c_str(), &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
        }

        if (!stbPixels.data) {
            auto msg{ std::string("Failed to load texture: ") + file.string() };
            UGINE_THROW(GfxError, msg.c_str());
        }

        data.push_back({ stbPixels.data, static_cast<vk::DeviceSize>(texWidth * texHeight * pixelSize) });
    }

    vk::Extent3D extent{ static_cast<uint32_t>(texWidth), static_cast<uint32_t>(texHeight), 1 };

    Texture texture;

    if (data.size() == 1) {
        texture = Texture::FromData(data[0].data, data[0].size, extent, format, vk::ImageTiling::eOptimal, usage, generateMips, finalLayout);
    } else {
        vk::ImageCreateInfo imageCI{};
        imageCI.imageType = vk::ImageType::e2D;
        imageCI.extent = extent;
        imageCI.mipLevels = 1;
        imageCI.arrayLayers = layers;
        imageCI.format = format;
        imageCI.tiling = vk::ImageTiling::eOptimal;
        imageCI.initialLayout = vk::ImageLayout::eUndefined;
        imageCI.usage = usage;
        imageCI.samples = vk::SampleCountFlagBits::e1;
        imageCI.sharingMode = vk::SharingMode::eExclusive;

        texture = Texture::FromData(data, imageCI, generateMips, finalLayout);
    }
    texture.SetFileName(file.filename().string());
    return texture;
}

Texture Texture::FromCubeMapFile(const std::filesystem::path& file, vk::ImageUsageFlags usage, bool hdr) {
    UGINE_TRACE("Loading cubemap texture from file '{}'...", file.string());

    // TODO: Use gfx::Image
    if (!IsKtxTexture(file)) {
        UGINE_THROW(GfxError, "Only .ktx cube map files are supported at the moment.");
    }

    hdr = true;

    utils::KtxScopeHolder ktxTexture;
    auto result = ktxTexture_CreateFromNamedFile(file.string().c_str(), KTX_TEXTURE_CREATE_LOAD_IMAGE_DATA_BIT, &ktxTexture.texture);
    if (result != KTX_SUCCESS) {
        UGINE_THROW(GfxError, "Failed to read ktx file.");
    }

    if (ktxTexture.texture->numFaces != 6) {
        UGINE_THROW(GfxError, "Exactly 6 layers in texture required.");
    }

    std::vector<Data> imageData;

    ktx_uint8_t* ktxTextureData = ktxTexture_GetData(ktxTexture.texture);

    // TODO: Format.
    vk::Format format{ vk::Format::eR8G8B8A8Unorm };
    uint32_t pixelSize = 4;

    if (hdr) {
        pixelSize = sizeof(float) * 2;
        format = vk::Format::eR16G16B16A16Sfloat;
    }

    uint32_t level{ 0 };
    size_t faceSize = ktxTexture.texture->baseWidth * ktxTexture.texture->baseHeight * pixelSize;

    for (uint32_t i = 0; i < ktxTexture.texture->numFaces; ++i) {
        ktx_size_t offset;

        KTX_error_code ret = ktxTexture_GetImageOffset(ktxTexture.texture, level, 0, i, &offset);
        if (ret != KTX_SUCCESS) {
            UGINE_THROW(GfxError, "Failed to get image offset.");
        }

        imageData.push_back({ ktxTextureData + offset, faceSize });
    }

    const auto bufferSize{ static_cast<vk::DeviceSize>(ktxTexture.texture->dataSize) };

    vk::Extent3D extent{ static_cast<uint32_t>(ktxTexture.texture->baseWidth), static_cast<uint32_t>(ktxTexture.texture->baseHeight), 1 };

    vk::ImageCreateInfo imageCI{};
    imageCI.imageType = vk::ImageType::e2D;
    imageCI.extent = extent;
    imageCI.mipLevels = 1;
    imageCI.arrayLayers = 6;
    imageCI.format = format;
    imageCI.tiling = vk::ImageTiling::eOptimal;
    imageCI.initialLayout = vk::ImageLayout::eUndefined;
    imageCI.usage = usage | vk::ImageUsageFlagBits::eTransferDst;
    imageCI.samples = vk::SampleCountFlagBits::e1;
    imageCI.sharingMode = vk::SharingMode::eExclusive;
    imageCI.flags = vk::ImageCreateFlagBits::eCubeCompatible;

    Texture texture{ imageCI };
    texture.SetData(imageData, vk::ImageLayout::eUndefined);
    texture.SetFileName(file.filename().string());

    return texture;
}

Texture Texture::FromCubeMapFiles(const std::vector<std::filesystem::path>& files, vk::ImageUsageFlags usage) {
    UGINE_TRACE("Loading cubmpa from {} texture files...", files.size());

    if (files.size() != 6) {
        UGINE_THROW(GfxError, "Exactly 6 files are expected for cube map.");
    }

    // TODO: Use gfx::Image
    utils::StbiScopeHolder image;
    std::vector<Data> imageData;

    int sourceWidth = 0;
    int sourceHeight = 0;
    int nn = 0;

    size_t size{ 0 };
    for (auto& file : files) {
        int width;
        int height;

        auto data = stbi_load(file.string().c_str(), &width, &height, &nn, 4);
        if (!data) {
            UGINE_THROW(GfxError, "Failed to load image.");
        }

        if (sourceWidth == 0 && sourceHeight == 0) {
            sourceWidth = width;
            sourceHeight = height;
            size = sourceWidth * sourceHeight * sizeof(unsigned char) * 4;
        } else if (sourceWidth != width || sourceHeight != height) {
            UGINE_THROW(GfxError, "Cube map files have different sizes.");
        }

        image.datas.push_back(data);
        imageData.push_back({ data, size });
    }

    // TODO: SRGB.
    const vk::Format format{ vk::Format::eR8G8B8A8Unorm };
    vk::Extent3D extent{ static_cast<uint32_t>(sourceWidth), static_cast<uint32_t>(sourceHeight), 1 };

    vk::ImageCreateInfo imageCI{};
    imageCI.imageType = vk::ImageType::e2D;
    imageCI.extent = extent;
    imageCI.mipLevels = 1;
    imageCI.arrayLayers = 6;
    imageCI.format = format;
    imageCI.tiling = vk::ImageTiling::eOptimal;
    imageCI.initialLayout = vk::ImageLayout::eUndefined;
    imageCI.usage = usage | vk::ImageUsageFlagBits::eTransferDst;
    imageCI.samples = vk::SampleCountFlagBits::e1;
    imageCI.sharingMode = vk::SharingMode::eExclusive;
    imageCI.flags = vk::ImageCreateFlagBits::eCubeCompatible;

    Texture texture(imageCI);
    texture.SetData(imageData, vk::ImageLayout::eUndefined);
    return texture;
}

Texture Texture::FromData(const void* data, size_t size, const vk::Extent3D& extent, vk::Format format, vk::ImageTiling tiling, vk::ImageUsageFlags usage,
    bool generateMips, vk::ImageLayout finalLayout) {

    vk::ImageUsageFlags flags = usage | vk::ImageUsageFlagBits::eTransferDst;

    uint32_t mipLevels = generateMips ? CalculateMipLevels(extent) : 1;
    if (generateMips) {
        flags |= vk::ImageUsageFlagBits::eTransferSrc;
        finalLayout = vk::ImageLayout::eTransferDstOptimal;
    }

    Texture texture{ extent, format, vk::ImageTiling::eOptimal, flags, vk::SampleCountFlagBits::e1, mipLevels };

    std::vector<Data> imageData{ { data, size } };
    texture.SetData(imageData, vk::ImageLayout::eUndefined, finalLayout);

    if (generateMips && texture.MipLevels() > 0) {
        auto& device{ GraphicsService::Device() };

        auto cmd{ device.BeginCommandList() };

        texture.GenerateMipMaps(cmd, texture.MipLevels(), finalLayout);

        // TODO:
        //device.SubmitCommandLists();
        //device.WaitGpu();
    }

    return texture;
}

Texture Texture::FromData(const std::vector<Data>& data, vk::ImageCreateInfo createInfo, bool generateMips, vk::ImageLayout finalLayout) {

    createInfo.usage |= vk::ImageUsageFlagBits::eTransferDst;

    if (generateMips) {
        createInfo.usage |= vk::ImageUsageFlagBits::eTransferSrc;
        finalLayout = vk::ImageLayout::eTransferDstOptimal;
    }

    Texture texture{ createInfo };
    texture.SetData(data, vk::ImageLayout::eUndefined, finalLayout);

    if (generateMips && texture.MipLevels() > 0) {
        auto& device{ GraphicsService::Device() };

        auto cmd{ device.BeginCommandList() };
        texture.GenerateMipMaps(cmd, texture.MipLevels(), finalLayout);

        // TODO:
        //device.SubmitCommandLists();
        //device.WaitGpu();
    }

    return texture;
}

} // namespace ugine::gfx
