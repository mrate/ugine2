﻿#include "Vertex.h"

namespace ugine::gfx {

std::vector<vk::VertexInputAttributeDescription> VertexAttributeDescriptions(bool withInstance) {
    std::vector<vk::VertexInputAttributeDescription> attributeDescriptions = {
        // Vertex:
        { 0, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, pos) },
        { 1, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, normal) },
        { 2, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, tangent) },
        { 3, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, bitangent) },
        { 4, 0, vk::Format::eR32G32Sfloat, offsetof(Vertex, tex) },
    };

    if (withInstance) {
        // Instace:
        attributeDescriptions.push_back({ 5, 1, vk::Format::eR32G32B32A32Sfloat, offsetof(VertexInstance, model0) });
        attributeDescriptions.push_back({ 6, 1, vk::Format::eR32G32B32A32Sfloat, offsetof(VertexInstance, model1) });
        attributeDescriptions.push_back({ 7, 1, vk::Format::eR32G32B32A32Sfloat, offsetof(VertexInstance, model2) });
        attributeDescriptions.push_back({ 8, 1, vk::Format::eR32G32B32A32Sfloat, offsetof(VertexInstance, model3) });
    }

    return attributeDescriptions;
}

std::vector<vk::VertexInputBindingDescription> VertexBindingDescription(bool withInstance) {
    std::vector<vk::VertexInputBindingDescription> bindingDescription = {
        vk::VertexInputBindingDescription{ 0, sizeof(Vertex), vk::VertexInputRate::eVertex },
    };

    if (withInstance) {
        bindingDescription.push_back(vk::VertexInputBindingDescription{ 1, sizeof(VertexInstance), vk::VertexInputRate::eInstance });
    }

    return bindingDescription;
}

void SetupVertexInstance(VertexInstance& instanceData, const glm::mat4& model) {
    instanceData.model0 = model[0];
    instanceData.model1 = model[1];
    instanceData.model2 = model[2];
    instanceData.model3 = model[3];
}

} // namespace ugine::gfx
