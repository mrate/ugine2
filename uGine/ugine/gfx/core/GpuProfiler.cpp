﻿#include "GpuProfiler.h"

namespace ugine::gfx {

void GpuProfiler::Init(uint32_t queryCount) {
    for (uint32_t i = 0; i < GraphicsService::FramesInFlight() + 1; ++i) {
        FrameQueries queries{};
        queries.pool = std::make_unique<QueryPool>(vk::QueryType::eTimestamp, queryCount);
        queries.queries.resize(queryCount);

        Instance().queries_.push_back(std::move(queries));
    }

    Instance().results_.resize(queryCount);
}

void GpuProfiler::Destroy() {
    Instance().queries_.clear();
}

void GpuProfiler::BeginFrame(GPUCommandList cmd) {
    queries_[frame_].count = frameCount_;
    frame_ = (frame_ + 1) % queries_.size();

    if (core::CoreService::Frame() > GraphicsService::FramesInFlight()) {
        auto& res{ queries_[frame_] };
        res.pool->FetchResults();
        for (uint32_t i = 0; i < res.count; ++i) {
            results_[i].name = res.queries[i].name;
            results_[i].tag = res.queries[i].tag;

            auto begin{ res.pool->Result(res.queries[i].beginQuery) };
            auto end{ res.pool->Result(res.queries[i].endQuery) };
            UGINE_ASSERT(end >= begin);

            results_[i].timeDiffUS = double((end - begin) * GraphicsService::Graphics().Properties().limits.timestampPeriod) / 1000.0;
        }
        resultsCount_ = frameCount_;
        std::sort(results_.begin(), results_.begin() + resultsCount_, [](const auto& a, const auto& b) { return a.name < b.name; });
    }

    frameCount_ = 0;
    queries_[frame_].pool->Reset(GraphicsService::Device().CommandBuffer(cmd));
}

uint32_t GpuProfiler::BeginQuery(GPUCommandList cmd, const std::string_view& name, uint32_t tag, vk::PipelineStageFlagBits stage) {
    auto beginQuery{ frameCount_++ };

    auto query{ queries_[frame_].pool->Timestamp(GraphicsService::Device().CommandBuffer(cmd), stage) };
    queries_[frame_].queries[beginQuery].name = name;
    queries_[frame_].queries[beginQuery].tag = tag;
    queries_[frame_].queries[beginQuery].beginQuery = query;

    return beginQuery;
}

void GpuProfiler::EndQuery(GPUCommandList cmd, uint32_t beginQuery, vk::PipelineStageFlagBits stage) {
    auto endQuery{ queries_[frame_].pool->Timestamp(GraphicsService::Device().CommandBuffer(cmd), stage) };

    queries_[frame_].queries[beginQuery].endQuery = endQuery;
}

} // namespace ugine::gfx
