﻿#pragma once

#include <ugine/gfx/Storages.h>

#include <optional>

namespace ugine::gfx {

class FramebufferAttachments {
public:
    FramebufferAttachments();

    FramebufferAttachments(const FramebufferAttachments&) = delete;
    FramebufferAttachments& operator=(const FramebufferAttachments&) = delete;

    FramebufferAttachments(FramebufferAttachments&&) = default;
    FramebufferAttachments& operator=(FramebufferAttachments&&) = default;

    TextureViewRef CreateAttachment(vk::Extent3D extent, vk::Format format, vk::ImageAspectFlags aspect = vk::ImageAspectFlagBits::eColor,
        vk::SampleCountFlagBits samples = vk::SampleCountFlagBits::e1, vk::ImageUsageFlags usage = vk::ImageUsageFlagBits::eColorAttachment);

    TextureViewRef CreateSampledAttachment(vk::Extent3D extent, vk::Format format, vk::ImageAspectFlags aspect = vk::ImageAspectFlagBits::eColor,
        vk::SampleCountFlagBits samples = vk::SampleCountFlagBits::e1,
        vk::ImageUsageFlags usage = vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eSampled, vk::UniqueSampler sampler = {});

    const gfx::Texture& Texture(uint32_t index) const {
        return *TextureViews::TextureRef(attachments_[index]);
    }

    gfx::Texture& Texture(uint32_t index) {
        return *TextureViews::TextureRef(attachments_[index]);
    }

    vk::ImageView View(uint32_t index) const {
        return attachments_[index]->View();
    }

    const gfx::TextureView& TextureView(uint32_t index) const {
        return *attachments_[index];
    }

    gfx::TextureView& TextureView(uint32_t index) {
        return *attachments_[index];
    }

    const std::vector<vk::ImageView>& Attachments() const {
        return attachmentViews_;
    }

    size_t Size() const {
        return attachments_.size();
    }

private:
    std::vector<gfx::TextureViewRef> attachments_;
    std::vector<vk::ImageView> attachmentViews_;
};

class Framebuffer {
public:
    Framebuffer() = default;
    Framebuffer(vk::RenderPass renderPass, const vk::Extent2D& extent, FramebufferAttachments&& attachments);

    Framebuffer(const Framebuffer&) = delete;
    Framebuffer& operator=(const Framebuffer&) = delete;

    Framebuffer(Framebuffer&&) = default;
    Framebuffer& operator=(Framebuffer&&) = default;

    vk::Framebuffer operator*() const {
        return *framebuffer_;
    }

    const FramebufferAttachments& Attachments() const {
        return attachmens_;
    }

    FramebufferAttachments& Attachments() {
        return attachmens_;
    }

    operator bool() const {
        return static_cast<bool>(framebuffer_);
    }

public:
    vk::UniqueFramebuffer framebuffer_;
    FramebufferAttachments attachmens_;
};

} // namespace ugine::gfx
