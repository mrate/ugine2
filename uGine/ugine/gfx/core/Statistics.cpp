﻿//#include "Statistics.h"
//
//namespace ugine::gfx {
//
//const vk::QueryPipelineStatisticFlags PipelineStatisticsCounter::QUERY_FLAGS = vk::QueryPipelineStatisticFlagBits::eInputAssemblyVertices
//    | vk::QueryPipelineStatisticFlagBits::eInputAssemblyPrimitives | vk::QueryPipelineStatisticFlagBits::eVertexShaderInvocations
//    | vk::QueryPipelineStatisticFlagBits::eClippingInvocations | vk::QueryPipelineStatisticFlagBits::eClippingPrimitives
//    | vk::QueryPipelineStatisticFlagBits::eFragmentShaderInvocations;
//
//PipelineStatisticsCounter::PipelineStatisticsCounter()
//    : pool_{ QUERY_FLAGS, 6 } {
//    for (uint32_t i = 0; i < GraphicsService::FramesInFlight(); ++i) {
//        queries_.push_back(pool_.CreateStatisticsQuery());
//    }
//    stats_.resize(GraphicsService::FramesInFlight());
//}
//
//void PipelineStatisticsCounter::Begin(vk::CommandBuffer cmd) {
//    queries_[GraphicsService::ActiveFrameInFlight()].Reset(cmd);
//    queries_[GraphicsService::ActiveFrameInFlight()].Begin(cmd);
//}
//
//void PipelineStatisticsCounter::End(vk::CommandBuffer cmd) {
//    queries_[GraphicsService::ActiveFrameInFlight()].End(cmd);
//
//    auto& query{ queries_[GraphicsService::ActiveFrameInFlight()] };
//    query.FetchResult();
//    auto results{ query.Results() };
//
//    auto& stats{ stats_[GraphicsService::ActiveFrameInFlight()] };
//    stats.inVerticesCount = results[0];
//    stats.iaPrimitivesCount = results[1];
//    stats.clippingInvocations = results[2];
//    stats.clippingPrimitives = results[3];
//    stats.vsInvocations = results[4];
//    stats.fsInvocations = results[5];
//}
//
//const PipelineStatistics& PipelineStatisticsCounter::Stats() const {
//    return stats_[GraphicsService::ActiveFrameInFlight()];
//}
//
//} // namespace ugine::gfx
