﻿#pragma once

#include <ugine/utils/Utils.h>

#include <filesystem>
#include <memory>

namespace ugine::gfx {

namespace detail {
    class ImageImpl {
    public:
        virtual ~ImageImpl() {}

        virtual uint32_t At(uint32_t x, uint32_t y) const = 0;
        virtual uint32_t Width() const = 0;
        virtual uint32_t Height() const = 0;
        virtual uint32_t Levels() const = 0;
        virtual uint32_t Layers() const = 0;
    };
} // namespace detail

class Image {
public:
    UGINE_MOVABLE_ONLY(Image);

    Image();
    Image(const std::filesystem::path& path);
    ~Image();

    operator bool() const {
        return static_cast<bool>(image_);
    }

    uint32_t At(float u, float v) const;
    uint32_t At(uint32_t x, uint32_t y) const;

    uint32_t Width() const;
    uint32_t Height() const;
    uint32_t Levels() const;
    uint32_t Layers() const;

    const std::filesystem::path& Path() const {
        return path_;
    }

private:
    std::unique_ptr<detail::ImageImpl> image_;
    std::filesystem::path path_;
};

} // namespace ugine::gfx
