﻿#pragma once

#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/Storages.h>

#include <vector>

namespace ugine::gfx {

class RenderTarget {
public:
    RenderTarget() = default;
    ~RenderTarget();

    RenderTarget(uint32_t width, uint32_t height, vk::Format format, vk::SampleCountFlagBits samples = vk::SampleCountFlagBits::e1,
        vk::ImageUsageFlags usage = vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eColorAttachment,
        vk::ImageAspectFlags aspect = vk::ImageAspectFlagBits::eColor, const SamplerRef& sampler = {}, bool withStencils = false);

    RenderTarget(const RenderTarget&) = default;
    RenderTarget& operator=(const RenderTarget&) = default;

    void Transition(GPUCommandList cmd, vk::ImageLayout initLayout, vk::ImageLayout newLayout);
    void TransitionStencil(GPUCommandList cmd, vk::ImageLayout initLayout, vk::ImageLayout newLayout);

    const Texture& Texture() const;
    TextureRef TextureRef() const;

    const TextureView& ViewForFrame(uint32_t frame) const;
    const TextureViewRef& ViewRefForFrame(uint32_t frame) const;

    const TextureView& View() const;
    const TextureViewRef& ViewRef() const;

    const TextureView& Multisample() const;
    const TextureViewRef& MultisampleRef() const;

    const TextureView& Stencil() const;
    const TextureViewRef& StencilRef() const;

    operator bool() const {
        return !textures_.empty();
    }

    vk::Extent3D Extent() const;
    uint32_t Width() const;
    uint32_t Height() const;
    vk::Format Format() const;
    vk::SampleCountFlagBits Samples() const;

    bool Multisampled() const {
        return !multisamples_.empty();
    }

    void SetName(const std::string_view& name);

private:
    vk::ImageAspectFlags aspect_;
    std::vector<TextureViewRef> textures_;
    std::vector<TextureViewRef> multisamples_;
    std::vector<TextureViewRef> stencils_; // TODO:
};

} // namespace ugine::gfx
