﻿#include "Buffer.h"

#include <ugine/gfx/Graphics.h>
#include <ugine/gfx/GraphicsService.h>
#include <ugine/utils/Log.h>

namespace ugine::gfx {

vk::DeviceSize Buffer::TotalSize = 0;

Buffer::Buffer(vk::DeviceSize size, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties)
    : size_(size) {
    vk::BufferCreateInfo bufferCI;
    bufferCI.size = size;
    bufferCI.usage = usage;
    bufferCI.sharingMode = vk::SharingMode::eExclusive;

    vma::AllocationCreateInfo allocCI;
    allocCI.usage = {};
    allocCI.flags = {};

    if (properties & vk::MemoryPropertyFlagBits::eDeviceLocal) {
        allocCI.usage = vma::MemoryUsage::eGpuOnly;
    } else if (properties & vk::MemoryPropertyFlagBits::eHostVisible) {
        allocCI.usage = vma::MemoryUsage::eCpuToGpu;
        allocCI.flags = vma::AllocationCreateFlagBits::eMapped;
    } else {
        UGINE_ASSERT(false);
    }

    vma::AllocationInfo allocInfo;
    auto result{ GraphicsService::Graphics().Allocator().createBuffer(bufferCI, allocCI, &allocInfo) };

    UGINE_TRACE("Buffer alloc: {} / {}", size_, TotalSize);
    TotalSize += size_;

    buffer_ = result.first;
    allocation_ = result.second;
    mapped_ = allocInfo.pMappedData;
    memory_ = allocInfo.deviceMemory;
    offset_ = 0; //allocInfo.offset;
}

void Buffer::Flush() {
    Flush(0, size_);
}

void Buffer::Flush(vk::DeviceSize offset, vk::DeviceSize size) {
    vk::MappedMemoryRange mappedRange = {};
    mappedRange.memory = memory_;
    mappedRange.offset = offset;
    mappedRange.size = size;
    GraphicsService::Graphics().Device().flushMappedMemoryRanges(mappedRange);
}

void Buffer::CopyTo(GPUCommandList command, const Buffer& other, vk::DeviceSize srcOffset, vk::DeviceSize dstOffset, vk::DeviceSize size) const {
    vk::BufferCopy region;
    region.srcOffset = srcOffset;
    region.dstOffset = dstOffset;
    region.size = size > 0 ? size : size_;
    GraphicsService::Device().CopyBuffer(command, buffer_, other.buffer_, region);
}

void Buffer::CopyTo(const Buffer& other, vk::DeviceSize srcOffset, vk::DeviceSize dstOffset, vk::DeviceSize size) const {
    auto command{ GraphicsService::Device().BeginCommandList() };
    CopyTo(command, other, srcOffset, dstOffset, size);
    GraphicsService::Device().SubmitCommandLists();
    GraphicsService::Device().WaitGpu();
}

void Buffer::CopyData(GPUCommandList command, const void* data, vk::DeviceSize size, vk::DeviceSize offset) {
    Buffer stagingBuffer(size, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
    memcpy(stagingBuffer.Mapped(), data, size);

    stagingBuffer.CopyTo(command, *this, 0, offset);

    SAFE_DELETE(stagingBuffer);
}

void Buffer::CopyData(const void* data, vk::DeviceSize size, vk::DeviceSize offset) {
    auto command{ GraphicsService::Device().BeginCommandList() };
    CopyData(command, data, size, offset);
    GraphicsService::Device().SubmitCommandLists();
    GraphicsService::Device().WaitGpu();
}

void Buffer::SetName(const std::string_view& name) {
    GraphicsService::Device().SetObjectDebugName(buffer_, name);
}

void Buffer::Free() {
    if (allocation_) {
        TotalSize -= size_;
        UGINE_TRACE("Buffer free: {} / {}", size_, TotalSize);

        GraphicsService::Graphics().Allocator().destroyBuffer(buffer_, allocation_);
        allocation_ = nullptr;
    }
}

Buffer::~Buffer() {
    Free();
}

void* Buffer::Map() {
    return mapped_;
}

void Buffer::Unmap() {
    UGINE_ASSERT(mapped_ != nullptr);
}

Buffer Buffer::CreateFromData(const void** data, const size_t* size, int count, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags memProps) {
    // Copy CPU memory buffers to GPU memory using staging buffer.
    vk::DeviceSize bufferSize = 0;
    for (int i = 0; i < count; ++i) {
        bufferSize += size[i];
    }

    const vk::MemoryPropertyFlags memProperties{ vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent };
    Buffer stagingBuffer(bufferSize, vk::BufferUsageFlagBits::eTransferSrc, memProperties);

    auto mappedData{ reinterpret_cast<char*>(stagingBuffer.Map()) };
    for (int i = 0; i < count; ++i) {
        memcpy(mappedData, data[i], size[i]);
        mappedData += size[i];
    }

    stagingBuffer.Unmap();

    Buffer buffer(bufferSize, usage | vk::BufferUsageFlagBits::eTransferDst, memProps);

    auto command{ GraphicsService::Device().BeginCommandList() };
    stagingBuffer.CopyTo(command, buffer);
    GraphicsService::Device().SubmitCommandLists();
    GraphicsService::Device().WaitGpu();

    return buffer;
}

Buffer Buffer::CreateFromData(const void* data, size_t size, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags memProps) {
    const void* pData[] = { data };
    const size_t pSize[] = { size };
    return CreateFromData(pData, pSize, 1, usage, memProps);
}

} // namespace ugine::gfx
