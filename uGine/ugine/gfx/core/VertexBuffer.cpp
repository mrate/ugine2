﻿#include "VertexBuffer.h"

#include <ugine/gfx/Graphics.h>
#include <ugine/gfx/GraphicsService.h>
#include <ugine/utils/Align.h>
#include <ugine/utils/Log.h>

namespace ugine::gfx {

VertexBuffer::VertexBuffer(uint32_t initVertexSize, uint32_t initIndexSize) {
    buffers_[VERTEX].usage = vk::BufferUsageFlagBits::eVertexBuffer | vk::BufferUsageFlagBits::eStorageBuffer | vk::BufferUsageFlagBits::eTransferSrc
        | vk::BufferUsageFlagBits::eTransferDst;
    buffers_[VERTEX].buffer = Buffer(initVertexSize, buffers_[VERTEX].usage, vk::MemoryPropertyFlagBits::eDeviceLocal);
    buffers_[VERTEX].fillSize = 0;
    buffers_[VERTEX].offset = 0;
    buffers_[VERTEX].buffer.SetName("VertexBuffer");

    buffers_[INDEX].usage = vk::BufferUsageFlagBits::eIndexBuffer | vk::BufferUsageFlagBits::eStorageBuffer | vk::BufferUsageFlagBits::eTransferSrc
        | vk::BufferUsageFlagBits::eTransferDst;
    buffers_[INDEX].buffer = Buffer(initIndexSize, buffers_[INDEX].usage, vk::MemoryPropertyFlagBits::eDeviceLocal);
    buffers_[INDEX].fillSize = 0;
    buffers_[INDEX].offset = 0;
    buffers_[INDEX].buffer.SetName("IndexBuffer");
}

VertexBuffer::~VertexBuffer() {
    UGINE_TRACE("Destroying vertex buffer.");
}

uint32_t VertexBuffer::Add(const std::vector<Vertex>& vertices, size_t alignTo) {
    return Add(buffers_[VERTEX], vertices.data(), sizeof(Vertex), vertices.size(), alignTo);
}

uint32_t VertexBuffer::Add(const std::vector<gfx::IndexType>& indices, size_t alignTo) {
    return Add(buffers_[INDEX], indices.data(), sizeof(gfx::IndexType), indices.size(), alignTo);
}

void VertexBuffer::Update(uint32_t vertexStart, const std::vector<Vertex>& vertices) {
    buffers_[VERTEX].buffer.CopyData(vertices.data(), sizeof(Vertex) * vertices.size(), vertexStart * sizeof(Vertex));
}

void VertexBuffer::Update(uint32_t indexStart, const std::vector<gfx::IndexType>& indices) {
    buffers_[INDEX].buffer.CopyData(indices.data(), sizeof(gfx::IndexType) * indices.size(), indexStart * sizeof(Vertex));
}

uint32_t VertexBuffer::VertexBufferOffset() const {
    return buffers_[VERTEX].fillSize;
}

vk::DescriptorBufferInfo VertexBuffer::VertexBufferDescriptor() const {
    return buffers_[VERTEX].buffer.Descriptor();
}

uint32_t VertexBuffer::IndexBufferOffset() const {
    return buffers_[INDEX].fillSize;
}

//void VertexBuffer::Bind(vk::CommandBuffer cmd) {
//    cmd.bindVertexBuffers(0, buffers_[VERTEX].buffer.GetBuffer(), static_cast<uint32_t>(0));
//    cmd.bindIndexBuffer(buffers_[INDEX].buffer.GetBuffer(), 0, vk::IndexType::eUint16);
//}

void VertexBuffer::Bind(GPUCommandList cmd) {
    GraphicsService::Device().BindVertexBuffer(cmd, buffers_[VERTEX].buffer.GetBuffer(), static_cast<uint32_t>(0));
    GraphicsService::Device().BindIndexBuffer(cmd, buffers_[INDEX].buffer.GetBuffer(), 0, IndexType());
}

const Buffer& VertexBuffer::VertexBuf() const {
    return buffers_[VERTEX].buffer;
}

Buffer& VertexBuffer::VertexBuf() {
    return buffers_[VERTEX].buffer;
}

const Buffer& VertexBuffer::IndexBuf() const {
    return buffers_[INDEX].buffer;
}

Buffer& VertexBuffer::IndexBuf() {
    return buffers_[INDEX].buffer;
}

vk::IndexType VertexBuffer::IndexType() const {
    return vk::IndexType::eUint16;
}

uint32_t VertexBuffer::VertexBufferAllocated() const {
    return static_cast<uint32_t>(buffers_[VERTEX].buffer.Size());
}

uint32_t VertexBuffer::VertexBufferFilled() const {
    return buffers_[VERTEX].fillSize;
}

uint32_t VertexBuffer::IndexBufferAllocated() const {
    return static_cast<uint32_t>(buffers_[INDEX].buffer.Size());
}

uint32_t VertexBuffer::IndexBufferFilled() const {
    return buffers_[INDEX].fillSize;
}

uint32_t VertexBuffer::Add(BuffDesc& buffer, const void* data, size_t singleElementSize, size_t count, size_t alignTo) const {
    auto size{ singleElementSize * count };
    if (alignTo) {
        size = utils::AlignTo(size, alignTo);
    }

    if (buffer.fillSize + size >= buffer.buffer.Size()) {
        auto newSize{ 2 * buffer.buffer.Size() };
        while (buffer.fillSize + size >= newSize) {
            newSize *= 2;
        }
        ReallocateBuffer(buffer, newSize);
    }

    uint32_t result{ buffer.offset };
    buffer.buffer.CopyData(data, size, buffer.fillSize);
    buffer.fillSize += static_cast<uint32_t>(size);
    buffer.offset += static_cast<uint32_t>(count);
    return result;
}

void VertexBuffer::ReallocateBuffer(BuffDesc& buffer, vk::DeviceSize newSize) const {
    UGINE_TRACE("Reallocating buffer from {} to {}", buffer.buffer.Size(), buffer.buffer.Size() * 2);

    Buffer tmp{ std::move(buffer.buffer) };
    buffer.buffer = Buffer(
        newSize, buffer.usage | vk::BufferUsageFlagBits::eTransferSrc | vk::BufferUsageFlagBits::eTransferDst, vk::MemoryPropertyFlagBits::eDeviceLocal);

    auto cmd{ GraphicsService::Device().BeginCommandList() };
    tmp.CopyTo(cmd, buffer.buffer, 0, 0, buffer.fillSize);
    GraphicsService::Device().SubmitCommandLists();
}

} // namespace ugine::gfx
