﻿//#pragma once
//
//#include <ugine/gfx/Gfx.h>
//
//#include "Query.h"
//
//namespace ugine::gfx {
//
//struct PipelineStatistics {
//    uint64_t inVerticesCount{};
//    uint64_t iaPrimitivesCount{};
//    uint64_t clippingInvocations{};
//    uint64_t clippingPrimitives{};
//    uint64_t vsInvocations{};
//    uint64_t fsInvocations{};
//};
//
//struct Statistics {
//    uint64_t drawCalls{};
//};
//
//class PipelineStatisticsCounter {
//public:
//    static const vk::QueryPipelineStatisticFlags QUERY_FLAGS;
//
//    PipelineStatisticsCounter();
//
//    void Begin(vk::CommandBuffer cmd);
//    void End(vk::CommandBuffer cmd);
//
//    const PipelineStatistics& Stats() const;
//
//private:
//    QueryPool pool_;
//    std::vector<PipelineStatisticsQuery> queries_;
//    std::vector<PipelineStatistics> stats_;
//};
//
//} // namespace ugine::gfx
