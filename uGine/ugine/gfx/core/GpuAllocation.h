﻿#pragma once

#include <ugine/utils/Utils.h>
#include <ugine/utils/VmaWrapper.h>

namespace ugine::gfx {

struct GpuAllocation {
    vk::Buffer buffer;
    void* memory{};
    uint32_t offset{};
    uint32_t size{};
};

class GpuAllocator {
public:
    UGINE_NO_COPY(GpuAllocator);

    GpuAllocator() = default;
    ~GpuAllocator();

    void Resize(size_t size);

    GpuAllocator(GpuAllocator&& other) {
        *this = std::move(other);
    }

    void Flush(uint32_t offset, uint32_t size);
    void Flush();

    GpuAllocator& operator=(GpuAllocator&& other) {
        this->size_ = other.size_;
        this->buffer_ = other.buffer_;
        this->memory_ = other.memory_;
        this->allocation_ = other.allocation_;
        this->mapped_ = other.mapped_;
        this->current_ = other.current_;
        this->end_ = other.end_;

        other.size_ = 0;
        other.buffer_ = nullptr;
        other.memory_ = vk::DeviceMemory{};
        other.allocation_ = nullptr;
        other.mapped_ = nullptr;
        other.current_ = nullptr;
        other.end_ = nullptr;

        return *this;
    }

    void Reset();
    GpuAllocation Allocate(size_t size, size_t alignment);
    void Free(const GpuAllocation& alloc);

private:
    void Free();

    size_t size_{};
    vk::Buffer buffer_{};
    vk::DeviceMemory memory_{};
    vma::Allocation allocation_{};
    void* mapped_{};
    uint8_t* current_{};
    uint8_t* end_{};
};

} // namespace ugine::gfx
