﻿#pragma once

#include <vulkan/vulkan.hpp>

#include <ugine/gfx/core/Device.h>
#include <ugine/utils/Assert.h>
#include <ugine/utils/VmaWrapper.h>

namespace ugine::gfx {

class Graphics;

class Buffer {
public:
    static Buffer CreateFromData(const void** data, const size_t* size, int count, vk::BufferUsageFlags usage,
        vk::MemoryPropertyFlags memProps = vk::MemoryPropertyFlagBits::eDeviceLocal);
    static Buffer CreateFromData(
        const void* data, size_t size, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags memProps = vk::MemoryPropertyFlagBits::eDeviceLocal);

    Buffer(vk::DeviceSize size, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties);

    Buffer() = default;
    Buffer(const Buffer& other) = delete;
    Buffer& operator=(const Buffer& other) = delete;

    Buffer(Buffer&& other) {
        *this = std::move(other);
    }

    Buffer& operator=(Buffer&& other) {
        Free();
        size_ = other.size_;
        offset_ = other.offset_;
        buffer_ = other.buffer_;
        memory_ = other.memory_;
        mapped_ = other.mapped_;
        allocation_ = other.allocation_;
        descriptor_ = other.descriptor_;
        other.mapped_ = nullptr;
        other.size_ = 0;
        other.allocation_ = nullptr;
        return *this;
    }

    virtual ~Buffer();

    void* Map();
    void Unmap();

    vk::Buffer operator*() const {
        return buffer_;
    }

    vk::DeviceMemory Memory() {
        return memory_;
    }

    vk::DeviceSize Size() const {
        return size_;
    }

    vk::Buffer GetBuffer() const {
        return buffer_;
    }

    vk::DeviceSize Offset() const {
        return offset_;
    }

    void* Mapped() {
        return mapped_;
    }

    const void* Mapped() const {
        return mapped_;
    }

    void* Mapped(uint32_t offset) {
        UGINE_ASSERT(mapped_);
        return reinterpret_cast<char*>(mapped_) + offset;
    }

    void Flush();
    void Flush(vk::DeviceSize offset, vk::DeviceSize size);

    vk::DescriptorBufferInfo Descriptor() const {
        return vk::DescriptorBufferInfo{
            buffer_,
            0,
            VK_WHOLE_SIZE,
        };
    }

    void CopyTo(GPUCommandList command, const Buffer& other, vk::DeviceSize srcOffset = 0, vk::DeviceSize dstOffset = 0, vk::DeviceSize size = 0) const;
    void CopyTo(const Buffer& other, vk::DeviceSize srcOffset = 0, vk::DeviceSize dstOffset = 0, vk::DeviceSize size = 0) const;
    void CopyData(GPUCommandList command, const void* data, vk::DeviceSize size, vk::DeviceSize offset);
    void CopyData(const void* data, vk::DeviceSize size, vk::DeviceSize offset);

    void SetName(const std::string_view& name);

    operator bool() const {
        return buffer_;
    }

protected:
    static vk::DeviceSize TotalSize;

    void Free();

    vk::DeviceSize size_{ 0 };
    vk::DeviceSize offset_{ 0 };

    vk::Buffer buffer_{};
    vk::DeviceMemory memory_{};
    void* mapped_{};
    vma::Allocation allocation_{};
    vk::DescriptorBufferInfo descriptor_;
};

} // namespace ugine::gfx
