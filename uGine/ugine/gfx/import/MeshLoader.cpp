﻿#include "MeshLoader.h"

#include "AssimpHelper.h"
#include "MaterialImporter.h"

#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/tools/MeshOptimizer.h>
#include <ugine/utils/Log.h>

#include <algorithm>

namespace {

inline float CalcTime(double time, const aiAnimation* anim) {
    return static_cast<float>(time / static_cast<float>(anim->mTicksPerSecond ? anim->mTicksPerSecond : 25.0f));
}

} // namespace

namespace ugine::gfx {

MeshLoader::MeshLoader(const std::filesystem::path& file, const Settings& settings)
    : file_{ file }
    , settings_{ settings } {
    UGINE_TRACE("Loading skinned mesh '{}'", file_.string());
    // TODO: Use Settings.

    unsigned int flags{};
    flags |= aiProcess_CalcTangentSpace;
    flags |= aiProcess_Triangulate;
    flags |= aiProcess_SortByPType;
    flags |= aiProcess_GenUVCoords;
    //flags |= aiProcess_OptimizeMeshes;
    flags |= aiProcess_Debone;
    flags |= aiProcess_ValidateDataStructure;
    flags |= aiProcess_FlipUVs;
    flags |= aiProcess_GenNormals;
    if (settings_.preTransform) {
        flags |= aiProcess_PreTransformVertices;
    }

    importer_ = std::make_unique<Assimp::Importer>();
    scene_ = importer_->ReadFile(file.string(), flags);

    if (!scene_ || !scene_->mRootNode) {
        UGINE_THROW(core::Error, importer_->GetErrorString());
    }
}

MeshLoader::~MeshLoader() {}

std::vector<MeshData> MeshLoader::LoadMeshes() const {
    std::vector<MeshData> resultList;

    MeshData result;
    result.globalInverseTransform = glm::inverse(assimp::ToGlm(scene_->mRootNode->mTransformation));

    // Load scene tree.
    transformations_.resize(scene_->mNumMeshes);
    result.rootNode = PopulateScene(scene_->mRootNode, glm::mat4{ 1.0f });

    const auto dir{ file_.parent_path() };
    if (settings_.singleMesh) {
        uint32_t numVertices{ 0 };
        uint32_t numIndices{ 0 };
        for (uint32_t i = 0; i < scene_->mNumMeshes; ++i) {
            const auto mesh{ scene_->mMeshes[i] };

            MeshData::Mesh newMesh{};
            newMesh.name = mesh->mName.C_Str();
            newMesh.transformation = transformations_[i];
            newMesh.vertexOffset = numVertices;
            newMesh.indexOffset = numIndices;
            newMesh.indexCount = mesh->mNumFaces * 3;

            numVertices += mesh->mNumVertices;
            numIndices += mesh->mNumFaces * 3;

            result.meshes.push_back(std::move(newMesh));
        }

        for (uint32_t i = 0; i < scene_->mNumMeshes; ++i) {
            LoadMesh(dir, scene_->mMeshes[i], result, i);
        }

        resultList.push_back(result);
    } else {
        for (uint32_t i = 0; i < scene_->mNumMeshes; ++i) {
            MeshData importMesh;
            const auto mesh{ scene_->mMeshes[i] };

            MeshData::Mesh newMesh{};
            newMesh.name = mesh->mName.C_Str();
            newMesh.transformation = transformations_[i];
            newMesh.vertexOffset = 0;
            newMesh.indexOffset = 0;
            newMesh.indexCount = mesh->mNumFaces * 3;

            importMesh.meshes.push_back(std::move(newMesh));

            LoadMesh(dir, scene_->mMeshes[i], importMesh, 0);
            materialMap_.clear();

            resultList.push_back(importMesh);
        }
    }

    // Generate lods.
    //for (auto& mesh : result.meshes) {
    //    std::vector<IndexType> indices(result.indices.begin() + mesh.lod[0].baseIndex, result.indices.begin() + mesh.lod[0].baseIndex + mesh.lod[0].numIndices);
    //    MeshOptimizer optimizer{ result.vertices, indices };
    //    float lod{ 0.5f };
    //    for (int i = 0; i < lodLevels - 1; ++i) {
    //        std::vector<IndexType> newIndices;
    //        optimizer.CreateLod(lod, newIndices);

    //        if (newIndices.size() == mesh.lod[i].numIndices) {
    //            break;
    //        }

    //        MeshData::MeshLod meshLod{ static_cast<uint32_t>(result.indices.size()), static_cast<uint32_t>(newIndices.size()) };
    //        mesh.lod.push_back(meshLod);

    //        std::copy(newIndices.begin(), newIndices.end(), std::back_inserter(result.indices));

    //        lod *= 0.5f;
    //    }
    //}

    return resultList;
}

MeshData MeshLoader::LoadMesh() const {
    auto result{ LoadMeshes() };
    return result.empty() ? MeshData{} : result[0];
}

std::vector<AnimationData> MeshLoader::LoadAnimations() const {
    std::vector<AnimationData> result;
    LoadAnimations(result);
    return result;
}

AnimationData MeshLoader::LoadAnimation() const {
    auto res{ LoadAnimations() };
    if (res.empty()) {
        UGINE_THROW(GfxError, "No animation loaded.");
    }

    return res[0];
}

void MeshLoader::LoadMesh(const std::filesystem::path& dir, const aiMesh* sourceMesh, MeshData& mesh, uint32_t meshIndex) const {
    // Load vertices and indices.
    for (uint32_t i = 0; i < sourceMesh->mNumVertices; ++i) {
        Vertex vertex;
        vertex.pos.x = settings_.scale * sourceMesh->mVertices[i].x;
        vertex.pos.y = settings_.scale * sourceMesh->mVertices[i].y;
        vertex.pos.z = settings_.scale * sourceMesh->mVertices[i].z;

        if (sourceMesh->HasNormals()) {
            vertex.normal.x = sourceMesh->mNormals[i].x;
            vertex.normal.y = sourceMesh->mNormals[i].y;
            vertex.normal.z = sourceMesh->mNormals[i].z;
        }

        if (sourceMesh->HasTextureCoords(0)) {
            vertex.tex.x = sourceMesh->mTextureCoords[0][i].x;
            vertex.tex.y = sourceMesh->mTextureCoords[0][i].y;
        } else {
            vertex.tex = { 0, 0 };
        }

        if (sourceMesh->HasTangentsAndBitangents()) {
            vertex.tangent = { sourceMesh->mTangents[i].x, sourceMesh->mTangents[i].y, sourceMesh->mTangents[i].z };
            vertex.bitangent = { sourceMesh->mBitangents[i].x, sourceMesh->mBitangents[i].y, sourceMesh->mBitangents[i].z };
        } else {
            vertex.tangent = { 0, 0, 0 };
            vertex.bitangent = { 0, 0, 0 };
        }

        VertexSkin vertexSkin;
        vertexSkin.jointIndices = glm::vec4(0.0f);
        vertexSkin.jointWeights = glm::vec4(0.0f);

        math::TurnVector(vertex.pos, settings_.axisUp);
        math::TurnVector(vertex.normal, settings_.axisUp);
        math::TurnVector(vertex.tangent, settings_.axisUp);
        math::TurnVector(vertex.bitangent, settings_.axisUp);

        mesh.vertices.push_back(vertex);
        mesh.verticesSkinned.push_back(vertexSkin);

        UGINE_ASSERT(mesh.vertices.size() == mesh.verticesSkinned.size());
    }

    // Process indices.
    for (uint32_t i = 0; i < sourceMesh->mNumFaces; ++i) {
        aiFace face = sourceMesh->mFaces[i];
        assert(face.mNumIndices == 3);
        for (uint32_t j = 0; j < face.mNumIndices; ++j) {
            mesh.indices.push_back(face.mIndices[j]);
        }
    }

    // Import material.
    if (sourceMesh->mMaterialIndex >= 0) {
        auto it{ materialMap_.find(sourceMesh->mMaterialIndex) };
        if (it == materialMap_.end()) {
            it = materialMap_.insert(std::make_pair(sourceMesh->mMaterialIndex, static_cast<uint32_t>(mesh.materials.size()))).first;

            aiMaterial* material{ scene_->mMaterials[sourceMesh->mMaterialIndex] };

            MeshMaterialData mat;
            mat.name = material->GetName().C_Str();
            assimp::LoadMaterial(dir, mat, scene_, material, true);
            mesh.materials.push_back(mat);
        }
        mesh.meshes[meshIndex].material = it->second;
    }

    // Load bones.
    for (uint32_t i = 0; i < sourceMesh->mNumBones; ++i) {
        auto sourceBone{ sourceMesh->mBones[i] };
        auto boneName{ assimp::FixName(sourceBone->mName.C_Str()) };

        auto it{ mesh.boneNameToIndex.find(boneName) };

        uint32_t boneIndex{ 0 };
        if (it == mesh.boneNameToIndex.end()) {
            MeshData::Bone bone{};
            bone.name = boneName;
            bone.offsetMatrix = assimp::ToGlm(sourceBone->mOffsetMatrix);

            boneIndex = static_cast<uint32_t>(mesh.bones.size());
            mesh.bones.push_back(bone);

            mesh.boneNameToIndex.insert(std::make_pair(boneName, boneIndex));
        } else {
            boneIndex = it->second;
        }

        for (uint32_t j = 0; j < sourceBone->mNumWeights; ++j) {
            const auto weight{ sourceBone->mWeights[j] };

            uint32_t vertexId{ mesh.meshes[meshIndex].vertexOffset + weight.mVertexId };

            AddVertexWeight(mesh, vertexId, boneIndex, weight.mWeight);
        }
    }
}

void MeshLoader::AddVertexWeight(MeshData& mesh, uint32_t vertexIndex, uint32_t boneIndex, float boneWeight) const {
    UGINE_ASSERT(mesh.vertices.size() > vertexIndex);
    UGINE_ASSERT(mesh.verticesSkinned.size() > vertexIndex);
    UGINE_ASSERT(mesh.bones.size() > boneIndex);

    for (int i = 0; i < MeshData::BONES_PER_VERTEX; ++i) {
        if (mesh.verticesSkinned[vertexIndex].jointWeights[i] == 0.0f) {
            mesh.verticesSkinned[vertexIndex].jointIndices[i] = static_cast<float>(boneIndex);
            mesh.verticesSkinned[vertexIndex].jointWeights[i] = boneWeight;
            return;
        }
    }

    UGINE_ASSERT("Bone per vertex limit reached.");
}

uint32_t MeshLoader::BoneIndex(MeshData& mesh, const std::string& name) const {
    auto it{ mesh.boneNameToIndex.find(name) };
    UGINE_ASSERT(it != mesh.boneNameToIndex.end());
    return it->second;
}

std::shared_ptr<MeshData::Node> MeshLoader::PopulateScene(const aiNode* node, const glm::mat4& parentTransform) const {
    auto newNode{ std::make_shared<MeshData::Node>() };
    newNode->name = assimp::FixName(node->mName.C_Str());
    newNode->transformation = assimp::ToGlm(node->mTransformation);

    for (uint32_t i = 0; i < node->mNumMeshes; ++i) {
        transformations_[node->mMeshes[i]] = parentTransform * newNode->transformation;
    }

    for (uint32_t i = 0; i < node->mNumChildren; ++i) {
        newNode->nodes.push_back(PopulateScene(node->mChildren[i], parentTransform * newNode->transformation));
    }

    return newNode;
}

void MeshLoader::LoadAnimations(std::vector<AnimationData>& animations) const {
    for (uint32_t i = 0; i < scene_->mNumAnimations; ++i) {
        auto animation{ scene_->mAnimations[i] };

        AnimationData anim{};
        anim.name = animation->mName.C_Str();
        if (anim.name.empty()) {
            anim.name = std::string("animation#") + std::to_string(animations.size());
        }

        anim.lengthSeconds = CalcTime(animation->mDuration, animation);

        for (uint32_t c = 0; c < animation->mNumChannels; ++c) {
            const auto channel{ animation->mChannels[c] };

            AnimationData::Channel newChannel{};

            std::string nodeName{ assimp::FixName(channel->mNodeName.C_Str()) };

            for (uint32_t t = 0; t < channel->mNumPositionKeys; ++t) {
                const auto position{ channel->mPositionKeys[t] };
                auto time{ CalcTime(position.mTime, animation) };
                newChannel.positions.push_back(std::make_pair(time, assimp::ToGlm(position.mValue)));
            }

            for (uint32_t t = 0; t < channel->mNumRotationKeys; ++t) {
                const auto rotation{ channel->mRotationKeys[t] };
                auto time{ CalcTime(rotation.mTime, animation) };
                newChannel.rotations.push_back(std::make_pair(time, assimp::ToGlm(rotation.mValue)));
            }

            for (uint32_t t = 0; t < channel->mNumScalingKeys; ++t) {
                const auto scale{ channel->mScalingKeys[t] };
                auto time{ CalcTime(scale.mTime, animation) };
                newChannel.scales.push_back(std::make_pair(time, assimp::ToGlm(scale.mValue)));
            }

            anim.channels.insert(std::make_pair(nodeName, newChannel));
        }

        animations.push_back(anim);
    }
}

} // namespace ugine::gfx
