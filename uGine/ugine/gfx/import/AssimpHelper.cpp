﻿#include "AssimpHelper.h"

#include <ugine/gfx/core/Texture.h>
#include <ugine/utils/StbImageWrapper.h>

#include <ugine/utils/Log.h>

#include <ugine/assets/AssetService.h>

#include <assimp/pbrmaterial.h>

#include <iostream>

//#ifdef _DEBUG
//#pragma comment(lib, "assimp-vc142-mtd.lib")
//#else
#pragma comment(lib, "assimp-vc142-mt.lib")
//#endif

namespace ugine::gfx::assimp {

TextureRef LoadMaterialTexture(
    const std::filesystem::path& path, const aiScene* scene, const aiMaterial* mat, aiTextureType type, unsigned int index, bool generateMips, bool fixPath) {

    if (index >= mat->GetTextureCount(type)) {
        return {};
    }

    aiString str;
    mat->GetTexture(type, index, &str);

    TextureRef tex;
    auto embeddedTexture{ scene->GetEmbeddedTexture(str.C_Str()) };
    if (embeddedTexture || str.data[0] == '*') {
        int index = atoi(str.data + 1);
        int width{ static_cast<int>(embeddedTexture ? embeddedTexture->mWidth : scene->mTextures[index]->mWidth) };
        int height{ static_cast<int>(embeddedTexture ? embeddedTexture->mHeight : scene->mTextures[index]->mHeight) };
        auto* data{ embeddedTexture ? embeddedTexture->pcData : scene->mTextures[index]->pcData };

        vk::Extent3D extent{ static_cast<uint32_t>(width), static_cast<uint32_t>(height), 1 };

        if (height == 0) {
            // Compressed texture.
            int nn;
            auto textureData = stbi_load_from_memory(reinterpret_cast<unsigned char*>(data), width, &width, &height, &nn, 4);
            if (!textureData) {
                UGINE_ERROR("Error loading texture {}: {}", str.C_Str(), stbi_failure_reason());
                UGINE_THROW(GfxError, "Failed to load embedded texture");
            }

            try {
                extent.width = static_cast<uint32_t>(width);
                extent.height = static_cast<uint32_t>(height);
                tex = TEXTURE_ADD(Texture::FromData(textureData, width * height * 4, extent, vk::Format::eR8G8B8A8Unorm, vk::ImageTiling::eOptimal,
                    vk::ImageUsageFlagBits::eSampled, generateMips));
            } catch (...) {
                stbi_image_free(textureData);
                UGINE_ERROR("Failed to load texture: width={}, height={}, nn={}", width, height, nn);
                throw;
            }
            stbi_image_free(textureData);
        } else {
            int pixel = 0;
            std::vector<unsigned char> rgba(width * height * 4);
            for (int j = 0, size = width * height; j < size; j += 4) {
                rgba[pixel++] = data[j].r;
                rgba[pixel++] = data[j].g;
                rgba[pixel++] = data[j].b;
                rgba[pixel++] = data[j].a;
            }
            tex = TEXTURE_ADD(Texture::FromData(rgba.data(), width * height * 4, extent, vk::Format::eR8G8B8A8Unorm, vk::ImageTiling::eOptimal,
                vk::ImageUsageFlagBits::eSampled, generateMips));
        }

        tex->SetFileName(str.C_Str());
    } else {
        auto file{ path / str.C_Str() };

        if (!std::filesystem::exists(file) && fixPath) {
            std::filesystem::path textureFile{ str.C_Str() };
            file = path / textureFile.filename();
        }

        try {
            tex = assets::AssetService::LoadTexture(file, generateMips);
        } catch (const core::Error& ex) {
            // TODO: Error handling policy.
            UGINE_ERROR("Failed to load texture: {}", ex.what());
        }
    }

    return tex;
}

std::string FixName(const char* name) {
    // TODO: Mixamo hack.
    std::string res{ name };
    const auto pos{ res.find("mixamorig:") };
    return pos != std::string::npos ? res.substr(10, res.size() - 10) : res;
}

glm::vec4 LoadColor(const aiMaterial* material, const char* pKey, unsigned int type, unsigned int idx, const glm::vec4& def) {
    aiColor4D color;

    if (material->Get(pKey, type, idx, color) == aiReturn_SUCCESS) {
        return glm::vec4{ color.r, color.g, color.b, color.a };
    }

    return def;
}

void LoadMaterial(const std::filesystem::path& baseDir, MeshMaterialData& rawMaterial, const aiScene* sourceScene, const aiMaterial* material, bool fixPath) {
    //if (pbr) {
    //rawMaterial.diffuseMap = LoadMaterialTexture(baseDir, sourceScene, material, AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_BASE_COLOR_TEXTURE, true);
    rawMaterial.metallicRoughnessMap
        = LoadMaterialTexture(baseDir, sourceScene, material, AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_METALLICROUGHNESS_TEXTURE, true, fixPath);
    rawMaterial.aoMap = LoadMaterialTexture(baseDir, sourceScene, material, aiTextureType_LIGHTMAP, 0, true, fixPath);

    //} else {
    rawMaterial.ambientMap = LoadMaterialTexture(baseDir, sourceScene, material, aiTextureType_AMBIENT, 0, true, fixPath);
    rawMaterial.diffuseMap = LoadMaterialTexture(baseDir, sourceScene, material, aiTextureType_DIFFUSE, 0, true, fixPath);
    rawMaterial.specularMap = LoadMaterialTexture(baseDir, sourceScene, material, aiTextureType_SPECULAR, 0, true, fixPath);
    //}
    rawMaterial.emissiveMap = LoadMaterialTexture(baseDir, sourceScene, material, aiTextureType_EMISSIVE, 0, true, fixPath);
    rawMaterial.normalMap = LoadMaterialTexture(baseDir, sourceScene, material, aiTextureType_NORMALS, 0, false, fixPath);
    rawMaterial.heightMap = LoadMaterialTexture(baseDir, sourceScene, material, aiTextureType_HEIGHT, 0, false, fixPath);

    rawMaterial.ambientColor = LoadColor(material, AI_MATKEY_COLOR_AMBIENT, glm::vec4(0.0f, 0.0f, 0.0f, 0.0f));
    rawMaterial.diffuseColor = LoadColor(material, AI_MATKEY_COLOR_DIFFUSE, glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
    rawMaterial.specularColor = LoadColor(material, AI_MATKEY_COLOR_SPECULAR, glm::vec4(1.0f, 1.0f, 1.0f, 0.0f));
    rawMaterial.emissiveColor = LoadColor(material, AI_MATKEY_COLOR_EMISSIVE, glm::vec4(0.0f, 0.0f, 0.0f, 0.0f));

    rawMaterial.baseColorFactor = LoadColor(material, AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_BASE_COLOR_FACTOR, rawMaterial.diffuseColor);

    if (material->Get(AI_MATKEY_SHININESS, rawMaterial.shininess) != aiReturn_SUCCESS) {
        rawMaterial.shininess = 20.0f;
    }

    rawMaterial.metallicFactor = 1.0f;
    if (material->Get(AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_METALLIC_FACTOR, rawMaterial.metallicFactor) != aiReturn_SUCCESS) {
        auto res{ material->Get(AI_MATKEY_REFLECTIVITY, rawMaterial.metallicFactor) };
    }

    if (material->Get(AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_ROUGHNESS_FACTOR, rawMaterial.roughnessFactor) != aiReturn_SUCCESS) {
        rawMaterial.roughnessFactor = 1.0f;
    }
}

glm::mat4 ToGlm(const aiMatrix4x4& matrix) {
    // TODO: Unpack + inline.
    glm::mat4 result;
    for (int row = 0; row < 4; ++row) {
        for (int col = 0; col < 4; ++col) {
            result[col][row] = matrix[row][col];
        }
    }
    return result;
}

glm::mat3 ToGlm(const aiMatrix3x3& matrix) {
    // TODO: Unpack + inline.
    glm::mat3 result;
    for (int row = 0; row < 3; ++row) {
        for (int col = 0; col < 3; ++col) {
            result[col][row] = matrix[row][col];
        }
    }
    return result;
}

void DumpNode(const aiNode* node, uint32_t depth) {
    for (uint32_t i = 0; i < depth; ++i) {
        std::cout << "--";
    }

    std::cout << " " << node->mName.C_Str() << " " << node->mNumMeshes << " meshes, " << node->mNumChildren << " children" << std::endl;

    for (uint32_t i = 0; i < depth; ++i) {
        std::cout << "--";
    }
    std::cout << " [" << node->mNumMeshes << " meshes: ";
    for (uint32_t i = 0; i < node->mNumMeshes; ++i) {
        std::cout << node->mMeshes[i] << ", ";
    }
    std::cout << "]" << std::endl;

    for (uint32_t i = 0; i < node->mNumChildren; ++i) {
        DumpNode(node->mChildren[i], depth + 1);
    }
}

void AssimpDumper(const aiScene* scene) {
    std::cout << "    meshes: " << scene->mNumMeshes << std::endl;
    std::cout << "animations: " << scene->mNumAnimations << std::endl;
    std::cout << " materials: " << scene->mNumMaterials << std::endl;
    std::cout << " Node tree:" << std::endl;
    DumpNode(scene->mRootNode, 0);

    std::cout << std::endl << "Meshes:" << std::endl;
    for (uint32_t i = 0; i < scene->mNumMeshes; ++i) {
        std::cout << i << ": " << scene->mMeshes[i]->mName.C_Str() << std::endl;
        std::cout << "  Bones:" << std::endl;
        for (uint32_t j = 0; j < scene->mMeshes[i]->mNumBones; ++j) {
            std::cout << "   " << j << ": " << scene->mMeshes[i]->mBones[j]->mName.C_Str() << std::endl;
        }
    }

    std::cout << std::endl << "Animations: " << std::endl;
    for (uint32_t i = 0; i < scene->mNumAnimations; ++i) {
        std::cout << i << ": " << scene->mAnimations[i]->mName.C_Str() << std::endl;
        std::cout << "  Channels: " << std::endl;
        for (uint32_t j = 0; j < scene->mAnimations[i]->mNumChannels; ++j) {
            std::cout << "   - " << scene->mAnimations[i]->mChannels[j]->mNodeName.C_Str() << std::endl;
        }
    }
}

} // namespace ugine::gfx::assimp
