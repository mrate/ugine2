﻿#pragma once

#include <ugine/gfx/rendering/MaterialInstance.h>

namespace ugine::gfx {

struct MeshMaterialData;

void SetupBlinPhongMaterial(const MeshMaterialData& data, MaterialInstanceRef& material);
void SetupPbrMaterial(const MeshMaterialData& data, MaterialInstanceRef& material);
void SetupGenericMaterial(const MeshMaterialData& data, MaterialInstanceRef& material);

void SetupMaterial(const MeshMaterialData& data, MaterialInstanceRef& material);

} // namespace ugine::gfx
