﻿#include "MaterialImporter.h"

#include "MeshData.h"

namespace ugine::gfx {

TextureViewRef CreateView(const TextureRef& texture) {
    return texture ? TEXTURE_VIEW_ADD(texture, TextureView::CreateSampled(*texture)) : TextureViewRef {};
}

void SetupBlinPhongMaterial(const MeshMaterialData& data, MaterialInstanceRef& material) {
    material->SetUniformVal("ambientColor", data.ambientColor);
    material->SetUniformVal("emissiveColor", data.emissiveColor);
    material->SetUniformVal("diffuseColor", data.diffuseColor);
    material->SetUniformVal("specularColor", data.specularColor);

    material->SetUniformVal<float>("specularPower", data.shininess);

    if (data.ambientMap) {
        material->SetTexture("ambientTexture", CreateView(data.ambientMap));
        material->SetUniformVal<int>("hasAmbientTexture", 1);
    }

    if (data.emissiveMap) {
        material->SetTexture("emissiveTexture", CreateView(data.emissiveMap));
        material->SetUniformVal<int>("hasEmissiveTexture", 1);
    }

    if (data.diffuseMap) {
        material->SetTexture("diffuseTexture", CreateView(data.diffuseMap));
        material->SetUniformVal<int>("hasDiffuseTexture", 1);
    }

    if (data.specularMap) {
        material->SetTexture("specularTexture", CreateView(data.specularMap));
        material->SetUniformVal<int>("hasSpecularTexture", 1);
    }

    if (data.normalMap) {
        material->SetTexture("normalTexture", CreateView(data.normalMap));
        material->SetUniformVal<int>("hasNormalTexture", 1);
    }

    if (data.heightMap) {
        material->SetTexture("heightTexture", CreateView(data.heightMap));
        material->SetUniformVal<int>("hasHeightTexture", 1);
    }
}

void SetupPbrMaterial(const MeshMaterialData& data, MaterialInstanceRef& material) {
    material->SetUniformVal("albedo", data.baseColorFactor);
    material->SetUniformVal("emissive", data.emissiveColor);
    material->SetUniformVal("metallic", data.metallicFactor);
    material->SetUniformVal("roughness", data.roughnessFactor);

    if (data.aoMap) {
        material->SetTexture("aoTexture", CreateView(data.aoMap));
        material->SetUniformVal<int>("hasAoTexture", 1);
    }

    if (data.emissiveMap) {
        material->SetTexture("emissiveTexture", CreateView(data.emissiveMap));
        material->SetUniformVal<int>("hasEmissiveTexture", 1);
    }

    if (data.diffuseMap) {
        material->SetTexture("albedoTexture", CreateView(data.diffuseMap));
        material->SetUniformVal<int>("hasAlbedoTexture", 1);
    }

    if (data.metallicRoughnessMap) {
        material->SetTexture("metallicTexture", CreateView(data.metallicRoughnessMap));
        material->SetUniformVal<int>("hasMetallicTexture", 1);
    }

    if (data.normalMap) {
        material->SetTexture("normalTexture", CreateView(data.normalMap));
        material->SetUniformVal<int>("hasNormalTexture", 1);
    }

    if (data.heightMap) {
        material->SetTexture("heightTexture", CreateView(data.heightMap));
        material->SetUniformVal<int>("hasHeightTexture", 1);
    }
}

void SafeSetTexture(MaterialInstanceRef& mat, const char* name, const char* hasName, const TextureViewRef& texture) {
    if (texture) {
        mat->SetTexture(name, texture);
        mat->SetUniformVal(hasName, 1);
    }
}

void SetupGenericMaterial(const MeshMaterialData& data, MaterialInstanceRef& material) {
    material->SetUniformVal("ambientColor", data.ambientColor);
    material->SetUniformVal("emissiveColor", data.emissiveColor);
    material->SetUniformVal("diffuseColor", data.diffuseColor);
    material->SetUniformVal("specularColor", data.specularColor);
    material->SetUniformVal("specularPower", data.shininess);
    material->SetUniformVal("albedo", data.baseColorFactor);
    material->SetUniformVal("emissive", data.emissiveColor);
    material->SetUniformVal("metallic", data.metallicFactor);
    material->SetUniformVal("roughness", data.roughnessFactor);

    SafeSetTexture(material, "ambientTexture", "hasAmbientTexture", CreateView(data.ambientMap));
    SafeSetTexture(material, "emissiveTexture", "hasEmissiveTexture", CreateView(data.emissiveMap));
    SafeSetTexture(material, "diffuseTexture", "hasDiffuseTexture", CreateView(data.diffuseMap));
    SafeSetTexture(material, "specularTexture", "hasSpecularTexture", CreateView(data.specularMap));
    SafeSetTexture(material, "normalTexture", "hasNormalTexture", CreateView(data.normalMap));
    SafeSetTexture(material, "heightTexture", "hasHeightTexture", CreateView(data.heightMap));

    SafeSetTexture(material, "aoTexture", "hasAoTexture", CreateView(data.aoMap));
    SafeSetTexture(material, "albedoTexture", "hasAlbedoTexture", CreateView(data.diffuseMap));
    SafeSetTexture(material, "metallicTexture", "hasMetallicTexture", CreateView(data.metallicRoughnessMap));
}

void SetupMaterial(const MeshMaterialData& data, MaterialInstanceRef& material) {
    SetupPbrMaterial(data, material);
    //SetupBlinPhongMaterial(data, material);
}

} // namespace ugine::gfx
