﻿#pragma once

// TODO: da fuq
#include <optional>

#include <ugine/gfx/core/Texture.h>
#include <ugine/gfx/core/Vertex.h>
#include <ugine/gfx/rendering/MaterialInstance.h>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include <map>
#include <memory>
#include <string>
#include <vector>

namespace ugine::gfx {

struct MeshMaterialData {
    std::string name;
    TextureRef emissiveMap;
    TextureRef normalMap;
    TextureRef heightMap;
    // blinn-phong
    TextureRef ambientMap;
    TextureRef diffuseMap;
    TextureRef specularMap;
    glm::vec4 ambientColor;
    glm::vec4 diffuseColor;
    glm::vec4 specularColor;
    glm::vec4 emissiveColor;
    float shininess{};
    // pbr
    TextureRef aoMap;
    TextureRef metallicRoughnessMap;
    glm::vec4 baseColorFactor;
    float metallicFactor{};
    float roughnessFactor{};
};

struct VertexSkin {
    glm::vec4 jointIndices;
    glm::vec4 jointWeights;
};

struct MeshData {
    static const int BONES_PER_VERTEX{ 4 };
    static const uint32_t INVALID_INDEX{ uint32_t(-1) };

    struct Bone {
        std::string name;
        glm::mat4 offsetMatrix;
    };

    struct Mesh {
        std::string name;
        glm::mat4 transformation; // Root to mesh recursive transformation.
        uint32_t material{};
        uint32_t indexOffset{};
        uint32_t indexCount{};
        uint32_t vertexOffset{};
    };

    struct Node {
        std::string name;
        glm::mat4 transformation;
        std::vector<std::shared_ptr<Node>> nodes;
    };

    // Global data.
    glm::mat4 globalInverseTransform;
    std::string name;

    // Basic mesh data.
    std::vector<Vertex> vertices;
    std::vector<IndexType> indices;
    std::vector<Mesh> meshes;
    std::vector<MeshMaterialData> materials;
    std::vector<VertexSkin> verticesSkinned;

    // Hierarchy data.
    std::shared_ptr<Node> rootNode;

    // Armature data.
    std::vector<Bone> bones;
    std::map<std::string, uint32_t> boneNameToIndex;
};

struct AnimationData {
    struct Channel {
        std::vector<std::pair<float, glm::vec3>> positions;
        std::vector<std::pair<float, glm::fquat>> rotations;
        std::vector<std::pair<float, glm::vec3>> scales;
    };

    std::string name;
    float lengthSeconds;
    std::map<std::string, Channel> channels;
};

} // namespace ugine::gfx
