﻿#pragma once

#include "MeshData.h"

#include <ugine/math/Math.h>

#include <filesystem>
#include <map>
#include <vector>

struct aiMesh;
struct aiNode;
struct aiNodeAnim;
struct aiScene;

namespace Assimp {
class Importer;
}

namespace ugine::gfx {

class MeshLoader {
public:
    struct Settings {
        float scale{ 1.0f };
        math::Axis axisUp{ math::Axis::YPos };
        bool fixPaths{ true };
        bool singleMesh{ false };
        bool preTransform{ false };
    };

    MeshLoader(const std::filesystem::path& file, const Settings& settings = {});
    ~MeshLoader();

    std::vector<MeshData> LoadMeshes() const;
    MeshData LoadMesh() const;
    std::vector<AnimationData> LoadAnimations() const;
    AnimationData LoadAnimation() const;

private:
    void LoadMesh(const std::filesystem::path& dir, const aiMesh* sourceMesh, MeshData& mesh, uint32_t index) const;
    void LoadAnimations(std::vector<AnimationData>& animations) const;

    void AddVertexWeight(MeshData& mesh, uint32_t vertexIndex, uint32_t boneIndex, float boneWeight) const;

    uint32_t BoneIndex(MeshData& mesh, const std::string& name) const;

    std::shared_ptr<MeshData::Node> PopulateScene(const aiNode* node, const glm::mat4& parentTransform) const;

    std::filesystem::path file_;
    Settings settings_;

    std::unique_ptr<Assimp::Importer> importer_;
    const aiScene* scene_{};

    mutable std::map<uint32_t, uint32_t> materialMap_;
    mutable std::vector<glm::mat4> transformations_;
};

} // namespace ugine::gfx
