﻿#pragma once

#include "MeshData.h"

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include <filesystem>

namespace ugine::gfx::assimp {

std::string FixName(const char* name);

void LoadMaterial(const std::filesystem::path& baseDir, MeshMaterialData& rawMaterial, const aiScene* sourceScene, const aiMaterial* material, bool fixPath);

glm::mat4 ToGlm(const aiMatrix4x4& matrix);
glm::mat3 ToGlm(const aiMatrix3x3& matrix);

inline glm::vec3 ToGlm(const aiVector3D& vector) {
    return glm::vec3(vector.x, vector.y, vector.z);
}

inline glm::fquat ToGlm(const aiQuaternion& quat) {
    return glm::fquat(quat.w, quat.x, quat.y, quat.z);
}

void AssimpDumper(const aiScene* scene);

} // namespace ugine::gfx::assimp
