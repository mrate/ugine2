﻿#pragma once

#include <ugine/gfx/Fwd.h>
#include <ugine/gfx/Storages.h>

#include <map>
#include <memory>
#include <vector>

namespace ugine::gfx {

class Device;
class Graphics;
class FramebufferCache;
class Ibl;
class Presenter;
class VertexBuffer;

class GraphicsService {
public:
    static std::array<vk::DynamicState, 4> DynamicStates();

    static GraphicsService& Instance();

    static void Init();
    static void Destroy();
    static void BeginFrame();

    static void SetGraphics(const std::shared_ptr<Graphics>& graphics);
    static void SetPresenter(const std::shared_ptr<Presenter>& presenter);

    static Graphics& Graphics();
    static Presenter& Presenter();
    static Device& Device();

    static uint32_t FramesInFlight();
    static uint32_t ActiveFrameInFlight();

    static Ibl& Ibl();

    static const TextureViewRef& NullTextureWhite();
    static const TextureViewRef& NullTextureBlack();
    static const TextureViewRef& NullTextureArray();
    static const TextureViewRef& NullTextureCube();
    static const BufferRef& NullBuffer();

    static const SamplerRef& DepthSampler();

    static VertexBufferRef VertexBuffer();

    static bool SupportsDebug() {
        return Instance().debug_;
    }

    static void SetDebug(bool debug) {
        Instance().debug_ = debug;
    }

    // Compute / Render buffer synchronization.
    static void BufferAcquireRender(GPUCommandList command, vk::Buffer buffer, vk::DeviceSize offset, vk::DeviceSize size);
    static void BufferReleaseRender(GPUCommandList command, vk::Buffer buffer, vk::DeviceSize offset, vk::DeviceSize size);
    static void BufferAcquireCompute(GPUCommandList command, vk::Buffer buffer, vk::DeviceSize offset, vk::DeviceSize size);
    static void BufferReleaseCompute(GPUCommandList command, vk::Buffer buffer, vk::DeviceSize offset, vk::DeviceSize size);

private:
    GraphicsService() {}
    ~GraphicsService();
    GraphicsService(const GraphicsService&) = delete;
    GraphicsService(GraphicsService&&) = delete;
    GraphicsService& operator=(const GraphicsService&) = delete;
    GraphicsService& operator=(GraphicsService&&) = delete;

    void InitImpl();
    void DestroyImpl();

    std::shared_ptr<gfx::Graphics> graphics_;
    std::shared_ptr<gfx::Presenter> presenter_;
    std::shared_ptr<gfx::Device> device_;
    bool debug_{};

    gfx::VertexBufferRef vertexBuffer_;

    std::shared_ptr<gfx::Ibl> ibl_;

    // Default values.
    TextureViewRef nullTextureWhite_;
    TextureViewRef nullTextureBlack_;
    TextureViewRef nullTextureArray_;
    TextureViewRef nullTextureCube_;

    BufferRef nullBuffer_;

    SamplerRef depthSampler_;
};

} // namespace ugine::gfx
