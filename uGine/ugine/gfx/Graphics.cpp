﻿#include "Graphics.h"

#include "Error.h"

#include <set>

namespace {

void SetAllocatorCreateInfo(vma::AllocatorCreateInfo& outInfo) {
    //    if (VK_KHR_dedicated_allocation_enabled) {
    //        outInfo.flags |= VMA_ALLOCATOR_CREATE_KHR_DEDICATED_ALLOCATION_BIT;
    //    }
    //    if (VK_KHR_bind_memory2_enabled) {
    //        outInfo.flags |= VMA_ALLOCATOR_CREATE_KHR_BIND_MEMORY2_BIT;
    //    }
    //#if !defined(VMA_MEMORY_BUDGET) || VMA_MEMORY_BUDGET == 1
    //    if (VK_EXT_memory_budget_enabled && (GetVulkanApiVersion() >= VK_API_VERSION_1_1 || VK_KHR_get_physical_device_properties2_enabled)) {
    //        outInfo.flags |= VMA_ALLOCATOR_CREATE_EXT_MEMORY_BUDGET_BIT;
    //    }
    //#endif
    //    if (VK_AMD_device_coherent_memory_enabled) {
    //        outInfo.flags |= VMA_ALLOCATOR_CREATE_AMD_DEVICE_COHERENT_MEMORY_BIT;
    //    }
    //    if (g_BufferDeviceAddressEnabled) {
    //        outInfo.flags |= VMA_ALLOCATOR_CREATE_BUFFER_DEVICE_ADDRESS_BIT;
    //    }
    //
    //    if (USE_CUSTOM_CPU_ALLOCATION_CALLBACKS) {
    //        outInfo.pAllocationCallbacks = &g_CpuAllocationCallbacks;
    //    }

    // Uncomment to enable recording to CSV file.
    /*
    static VmaRecordSettings recordSettings = {};
    recordSettings.pFilePath = "VulkanSample.csv";
    outInfo.pRecordSettings = &recordSettings;
    */

    // Uncomment to enable HeapSizeLimit.
    /*
    static std::array<VkDeviceSize, VK_MAX_MEMORY_HEAPS> heapSizeLimit;
    std::fill(heapSizeLimit.begin(), heapSizeLimit.end(), VK_WHOLE_SIZE);
    heapSizeLimit[0] = 512ull * 1024 * 1024;
    outInfo.pHeapSizeLimit = heapSizeLimit.data();
    */
}

static constexpr uint32_t GetVulkanApiVersion() {
#if VMA_VULKAN_VERSION == 1002000
    return VK_API_VERSION_1_2;
#elif VMA_VULKAN_VERSION == 1001000
    return VK_API_VERSION_1_1;
#elif VMA_VULKAN_VERSION == 1000000
    return VK_API_VERSION_1_0;
#else
#error Invalid VMA_VULKAN_VERSION.
    return UINT32_MAX;
#endif
}

} // namespace

namespace ugine::gfx {

Graphics::Graphics(vk::Instance instance, vk::PhysicalDevice physicalDevice, const gfx::Queues& queues)
    : instance_(instance)
    , physicalDevice_(physicalDevice)
    , queues_(queues) {

    std::set<uint32_t> uniqueQueues = { queues.graphics, queues.compute, queues.transfer, queues.surfacePresent };

    const auto queuePriority{ 1.0f };
    std::vector<vk::DeviceQueueCreateInfo> queueCreateInfos;
    for (auto queueFamily : uniqueQueues) {
        queueCreateInfos.push_back(vk::DeviceQueueCreateInfo({}, queueFamily, 1, &queuePriority));
    }

    auto availableFeatures{ physicalDevice.getFeatures() };

    vk::PhysicalDeviceFeatures deviceFeatures{};
    if (availableFeatures.samplerAnisotropy) {
        deviceFeatures.samplerAnisotropy = VK_TRUE;
    }
    deviceFeatures.fillModeNonSolid = VK_TRUE;
    deviceFeatures.geometryShader = VK_TRUE;

    vk::DeviceCreateInfo deviceCI{};
    deviceCI.pQueueCreateInfos = queueCreateInfos.data();
    deviceCI.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
    deviceCI.pEnabledFeatures = &deviceFeatures;
    deviceCI.enabledExtensionCount = static_cast<uint32_t>(VulkanApi::VulkanApi::DEVICE_EXTENSIONS.size());
    deviceCI.ppEnabledExtensionNames = VulkanApi::DEVICE_EXTENSIONS.data();

    device_ = physicalDevice_.createDeviceUnique(deviceCI);

    device_->getQueue(queues_.graphics, 0, &graphicsQueue_);
    device_->getQueue(queues_.compute, 0, &computeQueue_);
    device_->getQueue(queues_.surfacePresent, 0, &presentQueue_);

    InitCapabilities();

    vma::AllocatorCreateInfo allocatorInfo = {};
    allocatorInfo.physicalDevice = physicalDevice_;
    allocatorInfo.device = *device_;
    allocatorInfo.instance = instance;
    allocatorInfo.vulkanApiVersion = GetVulkanApiVersion();

    SetAllocatorCreateInfo(allocatorInfo);
    allocator_ = vma::createAllocator(allocatorInfo);
}

Graphics::~Graphics() {
    allocator_.destroy();
}

bool Graphics::IsSutableSurface(vk::SurfaceKHR surface) {
    vk::Bool32 presentSupport{ false };
    auto result{ physicalDevice_.getSurfaceSupportKHR(queues_.surfacePresent, surface, &presentSupport) };
    return result == vk::Result::eSuccess && presentSupport != 0;
}

void Graphics::SubmitGfxCommand(vk::CommandBuffer command, vk::Semaphore waitSemaphore, vk::Semaphore signalSemaphore, vk::Fence fence) {
    vk::PipelineStageFlags waitStages[] = { vk::PipelineStageFlagBits::eColorAttachmentOutput };
    vk::SubmitInfo submitInfo(1, &waitSemaphore, waitStages, 1, &command, 1, &signalSemaphore);

    if (fence) {
        device_->resetFences(fence);
    }
    graphicsQueue_.submit(submitInfo, fence);
}

void Graphics::SubmitGfxCommand(
    vk::CommandBuffer command, const utils::Vector<vk::Semaphore>& waitSemaphore, const utils::Vector<vk::Semaphore>& signalSemaphore, vk::Fence fence) {

    vk::PipelineStageFlags waitStages[] = { vk::PipelineStageFlagBits::eVertexInput, vk::PipelineStageFlagBits::eColorAttachmentOutput };
    vk::SubmitInfo submitInfo(static_cast<uint32_t>(waitSemaphore.size()), waitSemaphore.data(), waitStages, 1, &command,
        static_cast<uint32_t>(signalSemaphore.size()), signalSemaphore.data());

    if (fence) {
        device_->resetFences(fence);
    }
    graphicsQueue_.submit(submitInfo, fence);
}

void Graphics::SubmitGfxCommands(const std::vector<vk::CommandBuffer>& commands, const utils::Vector<vk::Semaphore>& waitSemaphore,
    const utils::Vector<vk::Semaphore>& signalSemaphore, vk::Fence fence) {
    vk::PipelineStageFlags waitStages[] = { vk::PipelineStageFlagBits::eVertexInput, vk::PipelineStageFlagBits::eColorAttachmentOutput };
    vk::SubmitInfo submitInfo(static_cast<uint32_t>(waitSemaphore.size()), waitSemaphore.data(), waitStages, static_cast<uint32_t>(commands.size()),
        commands.data(), static_cast<uint32_t>(signalSemaphore.size()), signalSemaphore.data());

    if (fence) {
        device_->resetFences(fence);
    }
    graphicsQueue_.submit(submitInfo, fence);
}

void Graphics::SubmitComputeCommand(
    vk::CommandBuffer command, const utils::Vector<vk::Semaphore>& waitSemaphore, const utils::Vector<vk::Semaphore>& signalSemaphore, vk::Fence fence) {
    vk::PipelineStageFlags waitStages[] = { vk::PipelineStageFlagBits::eComputeShader };
    vk::SubmitInfo submitInfo(static_cast<uint32_t>(waitSemaphore.size()), waitSemaphore.data(), waitStages, 1, &command,
        static_cast<uint32_t>(signalSemaphore.size()), signalSemaphore.data());

    if (fence) {
        device_->resetFences(fence);
    }
    computeQueue_.submit(submitInfo, fence);
}

void Graphics::SubmitComputeCommands(const std::vector<vk::CommandBuffer>& commands, const utils::Vector<vk::Semaphore>& waitSemaphore,
    const utils::Vector<vk::Semaphore>& signalSemaphore, vk::Fence fence) {

    vk::PipelineStageFlags waitStages[] = { vk::PipelineStageFlagBits::eComputeShader };
    vk::SubmitInfo submitInfo(static_cast<uint32_t>(waitSemaphore.size()), waitSemaphore.data(), waitStages, static_cast<uint32_t>(commands.size()),
        commands.data(), static_cast<uint32_t>(signalSemaphore.size()), signalSemaphore.data());

    if (fence) {
        device_->resetFences(fence);
    }
    computeQueue_.submit(submitInfo, fence);
}

void Graphics::SubmitGfxCommand(vk::CommandBuffer command, vk::Fence fence) {
    graphicsQueue_.submit(vk::SubmitInfo{ 0, nullptr, nullptr, 1, &command }, fence);
}

uint32_t Graphics::FindMemoryType(uint32_t typeFilter, vk::MemoryPropertyFlags properties) const {
    vk::PhysicalDeviceMemoryProperties memProperties = physicalDevice_.getMemoryProperties();

    for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
        if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
            return i;
        }
    }

    UGINE_THROW(GfxError, "Failed to find required memory type");
}

void Graphics::WaitIdle() {
    graphicsQueue_.waitIdle();
    presentQueue_.waitIdle();
    device_->waitIdle();
}

void Graphics::InitCapabilities() {
    properties_ = physicalDevice_.getProperties();

    depthFormat_ = FindSupportedFormat({ vk::Format::eD32Sfloat, vk::Format::eD24UnormS8Uint, vk::Format::eD16UnormS8Uint, vk::Format::eD16Unorm },
        vk::ImageTiling::eOptimal, vk::FormatFeatureFlagBits::eDepthStencilAttachment);
    depthStencilFormat_ = FindSupportedFormat({ vk::Format::eD32SfloatS8Uint, vk::Format::eD24UnormS8Uint, vk::Format::eD16UnormS8Uint },
        vk::ImageTiling::eOptimal, vk::FormatFeatureFlagBits::eDepthStencilAttachment);

    availableSampleCount_ = VulkanApi::GetMaxUsableSampleCount(physicalDevice_);
}

vk::Format Graphics::FindSupportedFormat(const std::vector<vk::Format>& candidates, vk::ImageTiling tiling, vk::FormatFeatureFlags features) const {
    for (vk::Format format : candidates) {
        auto props{ physicalDevice_.getFormatProperties(format) };

        if (tiling == vk::ImageTiling::eLinear && (props.linearTilingFeatures & features) == features) {
            return format;
        } else if (tiling == vk::ImageTiling::eOptimal && (props.optimalTilingFeatures & features) == features) {
            return format;
        }
    }

    UGINE_THROW(GfxError, "Failed to find supported format");
}

} // namespace ugine::gfx
