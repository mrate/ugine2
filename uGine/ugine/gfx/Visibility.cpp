﻿#include "Visibility.h"

#include <ugine/utils/Assert.h>

namespace ugine::gfx {

void Visibility::SetCamera(const core::Camera& camera, bool omniDirectional, float omniDistance) {
    cameraPos_ = camera.Position();
    frustum_ = camera.Frustum();
    omniDirectional_ = omniDirectional;
    omniDistanceSquared_ = omniDistance * omniDistance;
}

void Visibility::SetMeshes(size_t size) {
    meshes_.Resize(size);
    meshesSize_ = 0;
}

void Visibility::SetSkinnedMeshes(size_t size) {
    skinnedMeshes_.Resize(size);
    skinnedMeshesSize_ = 0;
}

void Visibility::SetParticles(size_t size) {
    particles_.Resize(size);
}

void Visibility::SetTerrains(size_t size) {
    terrains_.Resize(size);
}

void Visibility::CheckMesh(const math::AABB& aabb, entt::entity ent, uint32_t submeshes) {
    if (Pass(aabb)) {
        meshes_.Add({ ent, submeshes });
        meshesSize_ += submeshes;
    }
}

void Visibility::CheckSkinnedMesh(const math::AABB& aabb, entt::entity ent, uint32_t submeshes) {
    if (Pass(aabb)) {
        skinnedMeshes_.Add({ ent, submeshes });
        skinnedMeshesSize_ += submeshes;
    }
}

void Visibility::CheckParticles(const math::AABB& aabb, entt::entity ent) {
    if (Pass(aabb)) {
        particles_.Add(ent);
    }
}

void Visibility::CheckTerrain(const math::AABB& aabb, entt::entity ent) {
    if (Pass(aabb)) {
        terrains_.Add(ent);
    }
}

bool Visibility::Pass(const math::AABB& aabb) const {
    return (omniDirectional_ && math::PointAABBDistanceSquared(aabb, cameraPos_) <= omniDistanceSquared_)
        || (!omniDirectional_ && math::InFrustum(frustum_, aabb));
}

void Visibility::Finalize() {
    meshes_.Finalize();
    skinnedMeshes_.Finalize();
    terrains_.Finalize();
    particles_.Finalize();
}

void LightCounter::AddLightShadow(entt::entity ent, uint32_t shadowIndex) {
    lightShadows_.Add({ ent, shadowIndex });
}

void LightCounter::SetLightShadows(size_t size) {
    lightShadows_.Resize(size);
}

void LightCounter::FinalizeLightShadows() {
    lightShadows_.Finalize();
}

} // namespace ugine::gfx
