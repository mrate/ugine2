﻿#include "RenderContext.h"

#include <ugine/core/Camera.h>
#include <ugine/gfx/data/FramebufferCache.h>

namespace ugine::gfx {

void RenderContext::SetCamera(const core::Camera* camera, const glm::mat4& viewProjPrev) {
    this->camera = camera;
    this->viewProjPrev = viewProjPrev;
}

void RenderContext::SetRenderTarget(uint32_t slot, const TextureView& view) {
    SetRenderTarget(slot, view.View());
}

void RenderContext::SetRenderTarget(uint32_t slot, vk::ImageView view) {
    UGINE_ASSERT(slot < limits::MAX_ATTACHMENTS);

    attachments[slot] = view;
    attachmentCount = std::max(attachmentCount, slot + 1);
}

void RenderContext::ClearRenderTargets() {
    attachmentCount = 0;
}

void RenderContext::BeginRenderPass(vk::RenderPass renderPass, uint32_t clearColorCount, const vk::ClearValue* clearColor) {
    UGINE_ASSERT(!isInRenderPass && "Already in active render pass");

    this->renderPass = renderPass;

    FramebufferKey key{};
    key.renderpass = renderPass;
    key.width = static_cast<uint32_t>(viewport.width);
    key.height = static_cast<uint32_t>(viewport.height);
    key.attachmentCount = attachmentCount;
    for (uint32_t i = 0; i < attachmentCount; ++i) {
        key.attachments[i] = attachments[i];
    }

    framebuffer = FramebufferCache::Instance().GetOrCreateFramebuffer(key);

    GraphicsService::Device().BeginRenderPass(command, renderPass, framebuffer, scissor, clearColorCount, clearColor);
    isInRenderPass = true;
}

void RenderContext::EndRenderPass() {
    isInRenderPass = false;

    GraphicsService::Device().EndRenderPass(command);
}

void RenderContext::SetViewport(uint32_t width, uint32_t height) {
    viewport = vk::Viewport{ 0, 0, static_cast<float>(width), static_cast<float>(height), 0.0f, 1.0f };
    GraphicsService::Device().SetViewport(command, viewport);
}

void RenderContext::SetViewportAndScrissor(uint32_t width, uint32_t height) {
    viewport = vk::Viewport{ 0, 0, static_cast<float>(width), static_cast<float>(height), 0.0f, 1.0f };
    scissor = vk::Rect2D{ { 0, 0 }, { width, height } };
    GraphicsService::Device().SetViewport(command, viewport);
    GraphicsService::Device().SetScissor(command, scissor);
}

void RenderContext::DrawFullscreen() {
    GraphicsService::Device().Draw(command, 3, 1, 0, 0);
}

void RenderContext::DrawFullscreen(DrawCall& drawcall) {
    drawcall.firstInstance = 0;
    drawcall.instanceCount = 1;
    drawcall.indexCount = 0;
    drawcall.indexStart = 0;
    drawcall.vertexOffset = 0;
    drawcall.vertexCount = 3;
}

} // namespace ugine::gfx
