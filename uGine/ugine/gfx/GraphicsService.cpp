﻿#include "GraphicsService.h"

#include "Presenter.h"

#include <ugine/core/ConfigService.h>
#include <ugine/core/CoreService.h>
#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/ShaderCache.h>
#include <ugine/gfx/Storages.h>
#include <ugine/gfx/core/Device.h>
#include <ugine/gfx/core/GpuProfiler.h>
#include <ugine/gfx/core/Texture.h>
#include <ugine/gfx/core/VertexBuffer.h>
#include <ugine/gfx/data/FramebufferCache.h>
#include <ugine/gfx/data/PipelineCache.h>
#include <ugine/gfx/data/RenderPassCache.h>
#include <ugine/gfx/data/SamplerCache.h>
#include <ugine/gfx/data/Shapes.h>
#include <ugine/gfx/rendering/FoliageDesc.h>
#include <ugine/gfx/rendering/LightShadingData.h>
#include <ugine/gfx/rendering/MeshMaterial.h>
#include <ugine/gfx/tools/Ibl.h>
#include <ugine/gfx/tools/Initializers.h>
#include <ugine/utils/Graveyard.h>

#include <ugine/utils/Assert.h>

namespace ugine::gfx {

gfx::Graphics& GraphicsService::Graphics() {
    return *Instance().graphics_;
}

const TextureViewRef& GraphicsService::NullTextureWhite() {
    return Instance().nullTextureWhite_;
}

const TextureViewRef& GraphicsService::NullTextureBlack() {
    return Instance().nullTextureBlack_;
}

const TextureViewRef& GraphicsService::NullTextureArray() {
    return Instance().nullTextureArray_;
}

const TextureViewRef& GraphicsService::NullTextureCube() {
    return Instance().nullTextureCube_;
}

const BufferRef& GraphicsService::NullBuffer() {
    return Instance().nullBuffer_;
}

const SamplerRef& GraphicsService::DepthSampler() {
    return Instance().depthSampler_;
}

VertexBufferRef GraphicsService::VertexBuffer() {
    return Instance().vertexBuffer_;
}

GraphicsService::~GraphicsService() {}

void GraphicsService::InitImpl() {
    //UGINE_ASSERT(!graphics_ && "GraphicsService already initialized!");

    device_ = std::make_shared<gfx::Device>(*graphics_);

    const unsigned char pixelBlack[] = { 0, 0, 0, 0 };
    const unsigned char pixelWhite[] = { 255, 255, 255, 0 };

    { // Null texture.
        auto textureBlack{ TEXTURE_ADD(Texture::FromData(
            pixelBlack, 4, vk::Extent3D{ 1, 1, 1 }, vk::Format::eR8G8B8A8Unorm, vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eSampled)) };
        textureBlack->SetName("NullTextureBlack");

        Instance().nullTextureBlack_ = TEXTURE_VIEW_ADD(textureBlack, TextureView::CreateSampled(*textureBlack, vk::ImageViewType::e2D));

        auto textureWhite{ TEXTURE_ADD(Texture::FromData(
            pixelWhite, 4, vk::Extent3D{ 1, 1, 1 }, vk::Format::eR8G8B8A8Unorm, vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eSampled)) };
        textureWhite->SetName("NullTextureWhite");

        Instance().nullTextureWhite_ = TEXTURE_VIEW_ADD(textureWhite, TextureView::CreateSampled(*textureWhite, vk::ImageViewType::e2D));
    }

    {
        std::vector<Texture::Data> data(1, { pixelBlack, 4 });

        vk::ImageCreateInfo imageCI{};
        imageCI.imageType = vk::ImageType::e2D;
        imageCI.extent = vk::Extent3D{ 1, 1, 1 };
        imageCI.mipLevels = 1;
        imageCI.arrayLayers = 1;
        imageCI.format = vk::Format::eR8G8B8A8Unorm;
        imageCI.tiling = vk::ImageTiling::eOptimal;
        imageCI.initialLayout = vk::ImageLayout::eUndefined;
        imageCI.usage = vk::ImageUsageFlagBits::eSampled;
        imageCI.samples = vk::SampleCountFlagBits::e1;
        imageCI.sharingMode = vk::SharingMode::eExclusive;

        auto texture{ TEXTURE_ADD(Texture::FromData(data, imageCI)) };
        texture->SetName("NullTextureArray2D");

        Instance().nullTextureArray_
            = TEXTURE_VIEW_ADD(texture, TextureView::CreateSampled(*texture, vk::ImageViewType::e2DArray, vk::ImageAspectFlagBits::eColor, 0, 1));
        Instance().nullTextureArray_->SetName("NullTextureArray2D");
    }

    {
        std::vector<Texture::Data> data(6, { pixelBlack, 4 });

        vk::ImageCreateInfo imageCI{};
        imageCI.imageType = vk::ImageType::e2D;
        imageCI.extent = vk::Extent3D{ 1, 1, 1 };
        imageCI.mipLevels = 1;
        imageCI.arrayLayers = 6;
        imageCI.format = vk::Format::eR8G8B8A8Unorm;
        imageCI.tiling = vk::ImageTiling::eOptimal;
        imageCI.initialLayout = vk::ImageLayout::eUndefined;
        imageCI.usage = vk::ImageUsageFlagBits::eSampled;
        imageCI.samples = vk::SampleCountFlagBits::e1;
        imageCI.sharingMode = vk::SharingMode::eExclusive;
        imageCI.flags = vk::ImageCreateFlagBits::eCubeCompatible;

        auto texture{ TEXTURE_ADD(Texture::FromData(data, imageCI)) };
        texture->SetName("NullTextureCube");

        Instance().nullTextureCube_
            = TEXTURE_VIEW_ADD(texture, TextureView::CreateSampled(*texture, vk::ImageViewType::eCube, vk::ImageAspectFlagBits::eColor, 0, 6));
        Instance().nullTextureCube_->SetName("NullTextureCube");
    }

    { // Null buffer
        nullBuffer_ = BUFFER_ADD(Buffer(1,
            vk::BufferUsageFlagBits::eIndexBuffer | vk::BufferUsageFlagBits::eVertexBuffer | vk::BufferUsageFlagBits::eStorageBuffer
                | vk::BufferUsageFlagBits::eUniformBuffer | vk::BufferUsageFlagBits::eIndirectBuffer,
            vk::MemoryPropertyFlagBits::eDeviceLocal));
        nullBuffer_->SetName("NullBuffer");
    }

    { // Depth sampler.
        depthSampler_ = SamplerCache::Sampler(SamplerCache::Type::LinearClampToBorderWhite);
    }

    vertexBuffer_ = VERTEX_BUFFER_ADD(core::ConfigService::VertexBufferInitialSize(), core::ConfigService::IndexBufferInitialSize());
}

void GraphicsService::DestroyImpl() {
    vertexBuffer_ = {};
    nullBuffer_ = {};
    nullTextureCube_ = {};
    nullTextureArray_ = {};
    nullTextureWhite_ = {};
    nullTextureBlack_ = {};
    ibl_ = nullptr;
    depthSampler_ = {};

    utils::Graveyard::Destroy();

    ShaderCache::Destroy();
    Shapes::Destroy();
    GpuProfiler::Destroy();
    FramebufferCache::Destroy();
    RenderPassCache::Destroy();
    PipelineCache::Destroy();
    MaterialService::Destroy();
    FoliageService::Destroy();
    SamplerCache::Destroy();
    Foliages::Destroy();
    MeshMaterials::Destroy();
    TextureViews::Destroy();
    Textures::Destroy();
    Samplers::Destroy();

    device_ = nullptr;
    presenter_ = nullptr;
    graphics_ = nullptr;
}

void GraphicsService::Init() {
    Samplers::Init("Sampler");
    Textures::Init("Texture");
    TextureViews::Init("Texture view");
    MeshMaterials::Init("Mesh Materials");
    Foliages::Init("Foliage");

    Instance().InitImpl();

    MaterialService::Init();
    FoliageService::Init();
    FramebufferCache::Init();
    RenderPassCache::Init();
    GpuProfiler::Init();
    Shapes::Init();
}

void GraphicsService::Destroy() {
    Instance().DestroyImpl();
}

void GraphicsService::BeginFrame() {
    utils::Graveyard::Purge();
}

void GraphicsService::SetGraphics(const std::shared_ptr<gfx::Graphics>& graphics) {
    Instance().graphics_ = graphics;
}

void GraphicsService::SetPresenter(const std::shared_ptr<gfx::Presenter>& presenter) {
    Instance().presenter_ = presenter;
}

gfx::Presenter& GraphicsService::Presenter() {
    return *Instance().presenter_;
}

Device& GraphicsService::Device() {
    return *Instance().device_;
}

uint32_t GraphicsService::FramesInFlight() {
    return Presenter().SwapChainImageCount();
}

uint32_t GraphicsService::ActiveFrameInFlight() {
    return Presenter().FrameIndex();
}

gfx::Ibl& GraphicsService::Ibl() {
    if (!Instance().ibl_) {
        Instance().ibl_ = std::make_shared<gfx::Ibl>();
    }

    return *Instance().ibl_;
}

std::array<vk::DynamicState, 4> GraphicsService::DynamicStates() {
    std::array<vk::DynamicState, 4> dynamicStates = {
        vk::DynamicState::eViewport,
        vk::DynamicState::eScissor,
        vk::DynamicState::eStencilWriteMask,
        vk::DynamicState::eStencilCompareMask,
    };

    return dynamicStates;
}

GraphicsService& GraphicsService::Instance() {
    static GraphicsService inst;
    return inst;
}

void GraphicsService::BufferAcquireRender(GPUCommandList command, vk::Buffer buffer, vk::DeviceSize offset, vk::DeviceSize size) {
    if (Graphics().HasComputeQueue()) {
        vk::BufferMemoryBarrier acquireBarrier{};
        acquireBarrier.buffer = buffer;
        acquireBarrier.offset = offset;
        acquireBarrier.size = size;

        acquireBarrier.srcAccessMask = vk::AccessFlagBits::eShaderWrite;
        acquireBarrier.dstAccessMask = vk::AccessFlagBits::eVertexAttributeRead;
        acquireBarrier.srcQueueFamilyIndex = GraphicsService::Graphics().ComputeQueueFamily();
        acquireBarrier.dstQueueFamilyIndex = GraphicsService::Graphics().GraphicsQueueFamily();

        GraphicsService::Device().CommandBuffer(command).pipelineBarrier(
            vk::PipelineStageFlagBits::eComputeShader, vk::PipelineStageFlagBits::eVertexInput, {}, nullptr, acquireBarrier, nullptr);
    }
}

void GraphicsService::BufferReleaseRender(GPUCommandList command, vk::Buffer buffer, vk::DeviceSize offset, vk::DeviceSize size) {
    if (GraphicsService::Graphics().HasComputeQueue()) {
        vk::BufferMemoryBarrier releaseBarrier{};
        releaseBarrier.buffer = buffer;
        releaseBarrier.offset = offset;
        releaseBarrier.size = size;

        releaseBarrier.srcAccessMask = vk::AccessFlagBits::eVertexAttributeRead;
        releaseBarrier.dstAccessMask = vk::AccessFlagBits::eShaderWrite;
        releaseBarrier.srcQueueFamilyIndex = GraphicsService::Graphics().GraphicsQueueFamily();
        releaseBarrier.dstQueueFamilyIndex = GraphicsService::Graphics().ComputeQueueFamily();

        GraphicsService::Device().CommandBuffer(command).pipelineBarrier(
            vk::PipelineStageFlagBits::eVertexInput, vk::PipelineStageFlagBits::eComputeShader, {}, nullptr, releaseBarrier, nullptr);
    }
}

void GraphicsService::BufferAcquireCompute(GPUCommandList command, vk::Buffer buffer, vk::DeviceSize offset, vk::DeviceSize size) {
    if (GraphicsService::Graphics().HasComputeQueue()) {
        vk::BufferMemoryBarrier acquireBarrier{};
        acquireBarrier.buffer = buffer;
        acquireBarrier.offset = offset;
        acquireBarrier.size = size;

        acquireBarrier.srcAccessMask = vk::AccessFlagBits::eVertexAttributeRead;
        acquireBarrier.dstAccessMask = vk::AccessFlagBits::eShaderWrite;
        acquireBarrier.srcQueueFamilyIndex = GraphicsService::Graphics().GraphicsQueueFamily();
        acquireBarrier.dstQueueFamilyIndex = GraphicsService::Graphics().ComputeQueueFamily();

        GraphicsService::Device().CommandBuffer(command).pipelineBarrier(
            vk::PipelineStageFlagBits::eVertexInput, vk::PipelineStageFlagBits::eComputeShader, {}, nullptr, acquireBarrier, nullptr);
    }
}

void GraphicsService::BufferReleaseCompute(GPUCommandList command, vk::Buffer buffer, vk::DeviceSize offset, vk::DeviceSize size) {
    if (GraphicsService::Graphics().HasComputeQueue()) {
        vk::BufferMemoryBarrier releaseBarrier{};
        releaseBarrier.buffer = buffer;
        releaseBarrier.offset = offset;
        releaseBarrier.size = size;

        releaseBarrier.srcAccessMask = vk::AccessFlagBits::eShaderWrite;
        releaseBarrier.dstAccessMask = vk::AccessFlagBits::eVertexAttributeRead;
        releaseBarrier.srcQueueFamilyIndex = GraphicsService::Graphics().ComputeQueueFamily();
        releaseBarrier.dstQueueFamilyIndex = GraphicsService::Graphics().GraphicsQueueFamily();

        GraphicsService::Device().CommandBuffer(command).pipelineBarrier(
            vk::PipelineStageFlagBits::eComputeShader, vk::PipelineStageFlagBits::eVertexInput, {}, nullptr, releaseBarrier, nullptr);
    }
}

} // namespace ugine::gfx
