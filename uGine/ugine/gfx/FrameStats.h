﻿#pragma once

#include <ugine/utils/Singleton.h>

#include <atomic>

namespace ugine::gfx {

class FrameStats : public utils::Singleton<FrameStats> {
public:
    std::atomic_uint32_t meshes;
    std::atomic_uint32_t skinnedMeshes;
    std::atomic_uint32_t terrains;
    std::atomic_uint32_t particles;

    std::atomic_uint32_t cameras;

    std::atomic_uint32_t passes{};

    std::atomic_uint32_t drawCallsIssued{};
    std::atomic_uint32_t drawCallsIndirect{};
    std::atomic_uint64_t drawCallsIndices{};
    std::atomic_uint64_t drawCallsInstances{};
    std::atomic_uint64_t drawCallsScene{};
    std::atomic_uint32_t renderDataUpdates{};
    std::atomic_uint32_t compDispatches{};
    std::atomic_uint32_t compDispatchesIndirect{};

    float cpuRenderTimeMS{};
    float cpuComputeTimeMS{};
    float cpuGuiUpdateTimeMS{};

    static void Clear() {
        Instance().meshes = 0;
        Instance().skinnedMeshes = 0;
        Instance().terrains = 0;
        Instance().particles = 0;
        Instance().cameras = 0;
        Instance().passes = 0;
        Instance().drawCallsIssued = 0;
        Instance().drawCallsIndirect = 0;
        Instance().drawCallsIndices = 0;
        Instance().drawCallsInstances = 0;
        Instance().drawCallsScene = 0;
        Instance().renderDataUpdates = 0;
        Instance().compDispatches = 0;
        Instance().compDispatchesIndirect = 0;

        //Instance().cpuRenderTimeMS = 0;
        //Instance().cpuComputeTimeMS = 0;
        //Instance().cpuGuiUpdateTimeMS = 0;
    }
};

} // namespace ugine::gfx
