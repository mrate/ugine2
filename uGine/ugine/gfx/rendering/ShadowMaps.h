﻿#pragma once

#include <ugine/gfx/Limits.h>
#include <ugine/gfx/core/Framebuffer.h>

#include <vector>

namespace ugine::gfx {

class ShadowMaps {
public:
    ShadowMaps(vk::RenderPass renderPass);

    const Texture& DirShadowMapTexture(uint32_t frameIndex, uint32_t lightIndex) const;
    const TextureView& DirShadowMap(uint32_t frameIndex, uint32_t lightIndex) const;
    const TextureView& DirShadowMapCsm(uint32_t frameIndex, uint32_t lightIndex, uint32_t csmLevel) const;

    const Texture& DirShadowMapTexture(uint32_t lightIndex) const;
    const TextureView& DirShadowMap(uint32_t lightIndex) const;
    const TextureView& DirShadowMapCsm(uint32_t lightIndex, uint32_t csmLevel) const;

    const Texture& SpotShadowMapTexture(uint32_t lightIndex) const;
    const TextureView& SpotShadowMap(uint32_t lightIndex) const;

    const Texture& SpotShadowMapTexture(uint32_t frameIndex, uint32_t lightIndex) const;
    const TextureView& SpotShadowMap(uint32_t frameIndex, uint32_t lightIndex) const;

    const Texture& PointShadowMapTexture(uint32_t lightIndex) const;
    const TextureView& PointShadowMap(uint32_t lightIndex) const;
    const TextureView& PointShadowMapDepth(uint32_t lightIndex) const;
    const TextureView& PointShadowMapFace(uint32_t lightIndex, uint32_t face) const;

    const Texture& PointShadowMapTexture(uint32_t frameIndex, uint32_t lightIndex) const;
    const TextureView& PointShadowMap(uint32_t frameIndex, uint32_t lightIndex) const;
    const TextureView& PointShadowMapDepth(uint32_t frameIndex, uint32_t lightIndex) const;
    const TextureView& PointShadowMapFace(uint32_t frameIndex, uint32_t lightIndex, uint32_t face) const;

    vk::Sampler Sampler() const;

private:
    void CreateSpotShadowMaps(vk::RenderPass renderPass);
    void CreateDirShadowMaps(vk::RenderPass renderPass);
    void CreatePointShadowMaps(vk::RenderPass renderPass);

    struct SpotShadowMaps {
        //std::shared_ptr<Framebuffer> framebuffers;
        TextureViewRef sampleView;
    };

    struct DirShadowMaps {
        TextureViewRef sampleView;
        TextureViewRef csmLevel[limits::CSM_LEVELS];
    };

    struct PointShadowMaps {
        TextureViewRef sampleView;
        TextureViewRef depthView;
        TextureViewRef faceView[6];
    };

    std::vector<std::vector<SpotShadowMaps>> spotShadowMaps_;
    std::vector<std::vector<DirShadowMaps>> dirShadowMaps_;
    std::vector<std::vector<PointShadowMaps>> pointShadowMaps_;
};

} // namespace ugine::gfx
