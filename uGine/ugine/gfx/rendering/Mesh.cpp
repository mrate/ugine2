﻿#include "Mesh.h"

#include <ugine/gfx/core/VertexBuffer.h>
#include <ugine/gfx/tools/Helpers.h>
#include <ugine/gfx/tools/MeshOptimizer.h>
#include <ugine/utils/Log.h>

#include <glm/gtc/matrix_transform.hpp>

namespace ugine::gfx {

Mesh::~Mesh() {
    UGINE_TRACE("Destroy mesh.");
}

Mesh::Mesh(int lodLevels, const std::vector<Vertex>& vertices, const std::vector<IndexType>& indices, bool isMutable, const std::string_view& name)
    : vertexBuffer_{ gfx::GraphicsService::VertexBuffer() }
    , name_{ name }
    , aabb_{ CalcAABB(vertices) }
    , mutable_{ isMutable } {

    UGINE_ASSERT(vertexBuffer_);

    drawCalls_.resize(mutable_ ? GraphicsService::FramesInFlight() : 1);
    UGINE_ASSERT(!drawCalls_.empty());

    if (indices.empty()) {
        std::vector<IndexType> defaultIndices(vertices.size());
        for (IndexType i{ 0 }; i < vertices.size(); ++i) {
            defaultIndices[i] = i;
        }

        GenerateLods(lodLevels, vertices, defaultIndices);
    } else {
        GenerateLods(lodLevels, vertices, indices);
    }
}

Mesh::Mesh(int lodLevels, const math::AABB& aabb, uint32_t indexStart, uint32_t vertexStart, uint32_t indexCount, const std::string_view& name)
    : vertexBuffer_{ gfx::GraphicsService::VertexBuffer() }
    , name_{ name }
    , aabb_{ aabb }
    , mutable_{ false } {

    gfx::DrawCall drawCall{};
    drawCall.indexCount = indexCount;
    drawCall.indexStart = indexStart;
    drawCall.vertexOffset = vertexStart;

    // TODO: Lod.
    drawCalls_.resize(1);
    drawCalls_[0].lod.push_back(drawCall);
}

void Mesh::GenerateLods(int levels, const std::vector<Vertex>& vertices, const std::vector<IndexType>& indices) {
    gfx::DrawCall drawCall;
    drawCall.vertexOffset = vertexBuffer_->Add(vertices);
    drawCall.indexCount = static_cast<uint32_t>(indices.size());
    drawCall.indexStart = vertexBuffer_->Add(indices);
    for (auto& frame : drawCalls_) {
        frame.lod.push_back(drawCall);
    }

    //MeshOptimizer meshOptimizer{ vertices, indices };

    //gfx::DrawCall drawCall;
    //drawCall.vertexStart = vertexBuffer_->Add(meshOptimizer.Vertices());

    //float lod{ 1.0f };
    //for (int i = 0; i < levels; ++i) {
    //    if (i == 0) {
    //        drawCall.indexCount = static_cast<uint32_t>(meshOptimizer.Indices().size());
    //        drawCall.indexStart = vertexBuffer_->Add(meshOptimizer.Indices());
    //    } else {
    //        std::vector<IndexType> lodIndices;
    //        meshOptimizer.CreateLod(lod, lodIndices);

    //        drawCall.indexCount = static_cast<uint32_t>(indices.size());
    //        drawCall.indexStart = vertexBuffer_->Add(lodIndices);
    //    }

    //    for (auto& frame : drawCalls_) {
    //        frame.lod.push_back(drawCall);
    //    }
    //    lod *= 0.5f;
    //}
}

void Mesh::Update() {
    UGINE_ASSERT(mutable_);

    frameIndex_ = (frameIndex_ + 1) % drawCalls_.size();
}

void Mesh::Update(int lodLevel, const std::vector<Vertex>& vertices) {
    UGINE_ASSERT(mutable_);

    frameIndex_ = (frameIndex_ + 1) % drawCalls_.size();
    vertexBuffer_->Update(drawCalls_[frameIndex_].lod[lodLevel].vertexOffset, vertices);
    aabb_ = CalcAABB(vertices);
}

} // namespace ugine::gfx
