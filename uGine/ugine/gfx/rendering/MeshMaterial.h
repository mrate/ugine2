﻿#pragma once

#include "MaterialInstance.h"
#include "Mesh.h"

#include <ugine/math/Math.h>
#include <ugine/utils/Storage.h>

#include <filesystem>

namespace ugine::gfx {

struct MeshData;

struct MeshMaterial {
    struct SubMesh {
        std::string name;
        uint32_t indexOffset{};
        uint32_t indexCount{};
        uint32_t vertexOffset{};
        MaterialInstanceRef material;

        bool HasPass(RenderPassType pass) const {
            return material && material->HasPass(pass);
        }
    };

    MeshRef mesh;
    std::vector<SubMesh> submeshes;

    MeshMaterial() = default;
    MeshMaterial(const MeshRef& mesh, const MaterialInstanceRef& mat)
        : mesh{ mesh } {
        submeshes.push_back({ mesh->Name(), 0, mesh->DrawCall().indexCount, 0, mat });
    }

    bool Empty() const {
        return submeshes.empty();
    }

    bool Transparent() const {
        return !(submeshes.empty() || !submeshes[0].material->Transparent());
    }

    operator bool() const {
        return mesh && !submeshes.empty();
    }
};

using MeshMaterials = utils::SafeDeleteStorage<MeshMaterial>;
using MeshMaterialRef = MeshMaterials::Ref;
using MeshMaterialID = MeshMaterials::Id;

#define MESH_MATERIAL_ADD(...) STORAGE_ADD(MeshMaterials, __VA_ARGS__)

struct ImportMeshSettings {
    std::filesystem::path file;
    float scale{ 1.0f };
    int lodLevels{};
    bool singleMesh{};
    bool preTransform{ false };
    math::Axis axisUp{ math::Axis::YPos };
    bool fixPaths{ true };
    MaterialRef material{};
};

MeshMaterialRef ImportMeshMaterial(const ImportMeshSettings& import);
MeshMaterialRef ImportMeshMaterial(const MeshData& meshData, const MaterialRef& material, int lodLevels = 1);

} // namespace ugine::gfx
