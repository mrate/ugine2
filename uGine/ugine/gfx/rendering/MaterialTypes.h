﻿#pragma once

#include <vulkan/vulkan.hpp>

#include <entt/entt.hpp>

#include <glm/glm.hpp>

#include <map>
#include <string>

namespace ugine::gfx {

struct Color;
class MaterialPassInstance;

constexpr uint32_t INVALID_SET{ static_cast<uint32_t>(-1) };

struct MaterialParam {
    enum class Type {
        Float,
        Float2,
        Float3,
        Float4,
        Int,
        Int2,
        Int3,
        Int4,
        Bool,
        Matrix4x4,
        Matrix3x3,
        Color,
        Texture2D,
        Texture3D,
        TextureCube,
        StorageBuffer,
    };

    uint32_t id;
    std::string name;
    std::string readableName;
    Type type;
};

struct MaterialParamDescriptor {
    uint32_t binding{};
    std::string name;
    uint32_t offset{};
    uint32_t size{};
    ugine::gfx::MaterialParam::Type type{};
};

struct MaterialParamValue {
    MaterialParam::Type type;
    union {
        glm::vec4 vec4;
        glm::ivec4 ivec4;
        glm::mat4 mat4;
    };
    entt::any texture;
};

struct MaterialParamValues {
    std::map<std::string, MaterialParamValue> values;
};

} // namespace ugine::gfx
