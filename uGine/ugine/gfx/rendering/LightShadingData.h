﻿#pragma once

#include "Light.h"

#include <ugine/gfx/core/Buffer.h>
#include <ugine/gfx/core/DescriptorSetPool.h>
#include <ugine/gfx/core/Device.h>
#include <ugine/utils/Align.h>

namespace ugine::gfx {

#pragma pack(push, 4)
struct alignas(16) LightEntry {
    enum Type {
        Directional = 0,
        Point = 1,
        Spot = 2,
    };

    glm::vec3 ambientColor;
    uint32_t typeEnabled;
    glm::vec3 color;
    float intensity;
    glm::vec3 position;
    float spotAngleRad;
    glm::vec3 direction;
    float range;
    uint32_t shadowIndex;
    uint32_t shadowMapIndex;
    uint32_t csmMaxLevel;
};

struct alignas(16) ShadowEntry {
    glm::mat4 lightViewProj[limits::CSM_LEVELS];
};

struct alignas(16) LightShadingData {
    uint32_t count{};
    uint32_t shadowCount{};
    uint32_t hasIbl{};
    float shadowStrength{ 0.5f };
    ShadowEntry shadows[limits::MAX_SHADOWS];
};
#pragma pack(pop)

class ShadowMaps;

class LightShadingDataBinder {
public:
    LightShadingDataBinder();

    void Bind(GPUCommandList cmd, const ShadowMaps& shadowMaps);

    LightShadingData& Data() {
        return data_;
    }

    std::vector<LightEntry>& Lights() {
        return lights_;
    }

    void SetIbl(const TextureViewRef& brdfLut, const TextureViewRef& skyBoxIrradiance, const TextureViewRef& skyBoxPrefiltered);
    void ResetIbl();

    void SetShadowStrength(float shadowStrength) {
        data_.shadowStrength = shadowStrength;
    }

    float ShadowStringth() const {
        return data_.shadowStrength;
    }

    void Update(gfx::GPUCommandList command);

private:
    const uint32_t ALIGNED_SIZE;

    void CopyLights(gfx::GPUCommandList command);

    LightShadingData data_;
    std::vector<LightEntry> lights_;

    Buffer uniformBuffer_;
    std::vector<Buffer> lightBuffer_;
    uint32_t lightBufferIndex_{};

    TextureViewRef brdfLut_;
    TextureViewRef skyBoxIrradiance_;
    TextureViewRef skyBoxPrefiltered_;
};

void FillLightShadingData(const Light& light, LightEntry& lightEntry);

} // namespace ugine::gfx
