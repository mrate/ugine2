﻿#include "MaterialPass.h"

#include <ugine/gfx/ShaderCache.h>
#include <ugine/gfx/core/DescriptorSetPool.h>
#include <ugine/gfx/core/PipelineFactory.h>
#include <ugine/gfx/core/Vertex.h>
#include <ugine/gfx/data/ShaderBindings.h>
#include <ugine/gfx/tools/Initializers.h>
#include <ugine/utils/File.h>
#include <ugine/utils/Utils.h>

namespace ugine::gfx {

void MaterialPass::ParseParams(const Shader& shader) {
    for (const auto& ds : shader.parsedData.descriptorSets) {
        if (ds.set != MATERIAL_DATASET) {
            continue;
        }

        for (const auto& b : ds.bindings) {
            if (b.type == vk::DescriptorType::eUniformBuffer) {
                for (const auto& m : b.members) {
                    // TODO:
                    MaterialParam::Type type{};
                    switch (m.type) {
                    case ShaderParsedData::Member::Type::Float:
                        type = MaterialParam::Type::Float;
                        break;
                    case ShaderParsedData::Member::Type::Float2:
                        type = MaterialParam::Type::Float2;
                        break;
                    case ShaderParsedData::Member::Type::Float3:
                        type = MaterialParam::Type::Float3;
                        break;
                    case ShaderParsedData::Member::Type::Float4:
                        type = MaterialParam::Type::Color;
                        break;
                    case ShaderParsedData::Member::Type::Int:
                        type = MaterialParam::Type::Int;
                        break;
                    case ShaderParsedData::Member::Type::Int2:
                        type = MaterialParam::Type::Int2;
                        break;
                    case ShaderParsedData::Member::Type::Int3:
                        type = MaterialParam::Type::Int3;
                        break;
                    case ShaderParsedData::Member::Type::Int4:
                        type = MaterialParam::Type::Int4;
                        break;
                    case ShaderParsedData::Member::Type::Bool:
                        type = MaterialParam::Type::Bool;
                        break;
                    case ShaderParsedData::Member::Type::Matrix3x3:
                        type = MaterialParam::Type::Matrix3x3;
                        break;
                    case ShaderParsedData::Member::Type::Matrix4x4:
                        type = MaterialParam::Type::Matrix4x4;
                        break;
                    default:
                        continue;
                    }

                    params_.push_back({ b.binding, m.name, m.offset, m.size, type });
                }
            } else if (b.type == vk::DescriptorType::eCombinedImageSampler) {
                switch (b.dim) {
                case ShaderParsedData::Binding::TexDim::Tex2D:
                    params_.push_back({ b.binding, b.name, 0, 0, MaterialParam::Type::Texture2D });
                    break;
                case ShaderParsedData::Binding::TexDim::Tex3D:
                    params_.push_back({ b.binding, b.name, 0, 0, MaterialParam::Type::Texture3D });
                    break;
                case ShaderParsedData::Binding::TexDim::TexCube:
                    params_.push_back({ b.binding, b.name, 0, 0, MaterialParam::Type::TextureCube });
                    break;
                }
            } else if (b.type == vk::DescriptorType::eStorageBuffer) {
                params_.push_back({ b.binding, b.name, 0, 0, MaterialParam::Type::StorageBuffer });
            }
        }
    }
}

inline void ToString(std::stringstream& res, RenderPassType& types, RenderPassType type, const char* str) {
    if (types & type) {
        res << str;
    }
    types &= ~type;
}

std::string ToString(RenderPassType types) {
    std::stringstream res;
    ToString(res, types, RenderPassType::Custom, "Custom");
    ToString(res, types, RenderPassType::Depth, "Depth");
    ToString(res, types, RenderPassType::Shadows, "Shadows");
    ToString(res, types, RenderPassType::ShadowsCube, "ShadowsCube");
    ToString(res, types, RenderPassType::Main, "Main");
    ToString(res, types, RenderPassType::Postprocess, "Postprocess");
    res << "Pass";

    UGINE_ASSERT(types == RenderPassType::None);

    return res.str();
}

MaterialPass::MaterialPass(RenderPassType type, const PipelineDesc& pipelineDesc, const std::map<vk::ShaderStageFlagBits, std::filesystem::path>& shaders,
    const std::string_view& name)
    : type_{ type }
    , pipelineDesc_{ pipelineDesc }
    , name_{ name } {

    name_ += ToString(type);

    ShaderProgram shader;
    for (auto [stage, path] : shaders) {
        const auto& s{ ShaderCache::GetShader(stage, path) };
        shader.AddShader(s);
        ParseParams(s);
    }

    shader.Link(descriptorSetLayoutHolder_);
    for (const auto& ds : descriptorSetLayoutHolder_) {
        descriptorSetLayout_.push_back(*ds);
    }

    shaderStages_ = shader.Stages();
    vertexInputCount_ = shader.Inputs(vk::ShaderStageFlagBits::eVertex);
    pushConstants_ = shader.PushConstants();
    bindings_ = shader.Bindings();
}

MaterialPass::~MaterialPass() {}

std::optional<uint32_t> MaterialPass::FindParamId(const std::string_view& name) const {
    bool found{};
    auto res{ FindParamId(name, &found) };
    return found ? res : std::optional<uint32_t>{};
}

uint32_t MaterialPass::FindParamId(const std::string_view& name, bool* found) const {
    const auto params{ Params() };
    for (size_t i = 0; i < params.size(); ++i) {
        if (params[i].name == name) {
            if (found) {
                *found = found;
            }
            return static_cast<uint32_t>(i);
        }
    }
    if (found) {
        *found = false;
    }
    return 0;
}

Pipeline MaterialPass::CreatePipeline(vk::RenderPass renderpass, vk::SampleCountFlagBits samples) const {
    auto& gfx{ GraphicsService::Graphics() };

    auto descriptorSetLayouts{ Layouts() };

    std::vector<vk::PushConstantRange> pushConstantRange;
    for (const auto& pc : pushConstants_) {
        pushConstantRange.push_back({ pc.stage, pc.offset, pc.size });
    }

    vk::PipelineLayoutCreateInfo pipelineLayoutCI;
    pipelineLayoutCI.setLayoutCount = static_cast<uint32_t>(descriptorSetLayouts.size());
    pipelineLayoutCI.pSetLayouts = descriptorSetLayouts.data();
    pipelineLayoutCI.pushConstantRangeCount = static_cast<uint32_t>(pushConstantRange.size());
    pipelineLayoutCI.pPushConstantRanges = pushConstantRange.data();

    auto pipelineLayout{ gfx.Device().createPipelineLayoutUnique(pipelineLayoutCI) };
    GraphicsService::Device().SetObjectDebugName(*pipelineLayout, name_);

    std::vector<vk::VertexInputBindingDescription> bindingDescription;
    std::vector<vk::VertexInputAttributeDescription> attributeDescriptions;

    const auto dynamicStates{ GraphicsService::DynamicStates() };

    PipelineFactory factory;

    factory.rasterizer.polygonMode = pipelineDesc_.polygonMode;
    factory.rasterizer.cullMode = pipelineDesc_.cullMode;

    factory.colorBlendAttachment.blendEnable = pipelineDesc_.blending.enabled;
    factory.colorBlendAttachment.colorBlendOp = pipelineDesc_.blending.colorBlendOp;
    factory.colorBlendAttachment.srcColorBlendFactor = pipelineDesc_.blending.srcColorBlendFactor;
    factory.colorBlendAttachment.dstColorBlendFactor = pipelineDesc_.blending.dstColorBlendFactor;
    factory.colorBlendAttachment.alphaBlendOp = pipelineDesc_.blending.alphaBlendOp;
    factory.colorBlendAttachment.srcAlphaBlendFactor = pipelineDesc_.blending.srcAlphaBlendFactor;
    factory.colorBlendAttachment.dstAlphaBlendFactor = pipelineDesc_.blending.dstAlphaBlendFactor;
    factory.colorBlendAttachment.colorWriteMask = pipelineDesc_.blending.colorWriteMask;

    factory.depthStencil.depthTestEnable = pipelineDesc_.depth.testEnable;
    factory.depthStencil.depthWriteEnable = pipelineDesc_.depth.writeEnable;
    factory.depthStencil.depthCompareOp = pipelineDesc_.depth.compareOp;

    factory.depthStencil.stencilTestEnable = pipelineDesc_.stencil.testEnable;
    factory.depthStencil.front.compareOp = pipelineDesc_.stencil.compareOp;
    factory.depthStencil.front.depthFailOp = pipelineDesc_.stencil.depthFailOp;
    factory.depthStencil.front.failOp = pipelineDesc_.stencil.failOp;
    factory.depthStencil.front.passOp = pipelineDesc_.stencil.passOp;
    factory.depthStencil.front.compareMask = pipelineDesc_.stencil.compareMask;
    factory.depthStencil.front.writeMask = pipelineDesc_.stencil.writeMask;
    factory.depthStencil.front.reference = pipelineDesc_.stencil.reference;
    factory.depthStencil.back = factory.depthStencil.front;

    factory.multisampling.rasterizationSamples = samples;
    factory.inputAssembly.topology = pipelineDesc_.topology;

    factory.SetDynamicStates(static_cast<uint32_t>(dynamicStates.size()), dynamicStates.data());

    // TODO:
    if (vertexInputCount_ == 0) {
        factory.SetVertexInput(0, nullptr, 0, nullptr);
    } else {
        bool withInstance{ vertexInputCount_ > VertexInputCount };

        bindingDescription = VertexBindingDescription(withInstance);
        attributeDescriptions = VertexAttributeDescriptions(withInstance);

        factory.SetVertexInput(static_cast<uint32_t>(bindingDescription.size()), bindingDescription.data(), static_cast<uint32_t>(attributeDescriptions.size()),
            attributeDescriptions.data());
    }

    factory.SetShaderStages(shaderStages_);

    auto pipeline{ factory.Create(*pipelineLayout, renderpass, 1) };
    GraphicsService::Device().SetObjectDebugName(*pipeline, name_);

    return Pipeline(vk::PipelineBindPoint::eGraphics, std::move(pipelineLayout), std::move(pipeline), descriptorSetLayout_);
}

} // namespace ugine::gfx
