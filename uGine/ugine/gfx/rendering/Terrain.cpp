﻿#include "Terrain.h"

#include <ugine/assets/AssetService.h>
#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/core/VertexBuffer.h>
#include <ugine/gfx/data/Shapes.h>
#include <ugine/gfx/import/MaterialImporter.h>
#include <ugine/gfx/import/MeshLoader.h>
#include <ugine/utils/Align.h>
#include <ugine/utils/Log.h>

#include <random>

namespace ugine::gfx {

// Terrain.

Terrain::Terrain() {
    auto mesh{ MESH_ADD(CreateGridMesh(GRID_WIDTH, GRID_HEIGHT, 1, true)) };
    auto material{ MaterialService::InstantiateByPath(L"assets/materials/terrain.mat") };

    meshMaterial_ = MESH_MATERIAL_ADD(mesh, material);
    meshMaterial_->submeshes[0].material->SetUniformVal("heightScale", 1);

    SetHeightMap(L"");
}

Terrain::~Terrain() {}

void Terrain::SetHeightMap(const std::filesystem::path& heightMap) {
    if (heightMap == heightMap_.Path()) {
        return;
    }

    if (!heightMap.empty()) {
        heightMap_ = Image(heightMap);
    } else {
        heightMap_ = Image{};
    }

    std::vector<Vertex> vertices;
    std::vector<IndexType> indices;

    GenerateGridMesh(GRID_WIDTH, GRID_HEIGHT, vertices, indices);

    std::vector<float> heights(GRID_WIDTH * GRID_HEIGHT, 0.0f);

    auto it{ vertices.begin() };
    for (uint32_t z = 0; z < GRID_HEIGHT; ++z) {
        for (uint32_t x = 0; x < GRID_WIDTH; ++x) {
            uint32_t pixel{};
            if (heightMap_) {
                auto px{ static_cast<uint32_t>(it->tex.x * heightMap_.Width()) };
                auto py{ static_cast<uint32_t>(it->tex.y * heightMap_.Height()) };
                pixel = (heightMap_.At(px, py)) & 0xff;
            }
            heights[x + z * GRID_WIDTH] = static_cast<float>(pixel) / 255.0f;
            ++it;
        }
    }

    it = vertices.begin();
    for (uint32_t z = 0; z < GRID_HEIGHT; ++z) {
        for (uint32_t x = 0; x < GRID_WIDTH; ++x) {
            //float heights[3][3];
            //for (auto hx = -1; hx <= 1; hx++) {
            //    for (auto hy = -1; hy <= 1; hy++) {
            //        auto px{ static_cast<uint32_t>(it->tex.x * heightMap_.Width()) };
            //        auto py{ static_cast<uint32_t>(it->tex.y * heightMap_.Height()) };

            //        if (px + hx < 0 || px + hx >= heightMap_.Width() || py + hy < 0 || py + hy >= heightMap_.Height()) {
            //            heights[hx + 1][hy + 1] = 0;
            //        } else {
            //            auto pixel{ (heightMap_.At(px + hx, py + hy) >> 8) & 0xff };
            //            heights[hx + 1][hy + 1] = static_cast<float>(pixel) / 255.0f;
            //        }
            //    }
            //}

            //glm::vec3 normal;
            // Sobel filter.
            //normal.x = heights[0][0] - heights[2][0] + 2.0f * heights[0][1] - 2.0f * heights[2][1] + heights[0][2] - heights[2][2];
            //normal.z = heights[0][0] + 2.0f * heights[1][0] + heights[2][0] - heights[0][2] - 2.0f * heights[1][2] - heights[2][2];
            //normal.y = 0.25f * sqrt(1.0f - normal.x * normal.x - normal.z * normal.z);

            //it->normal = glm::normalize(normal /** glm::vec3(2.0f, 1.0f, 2.0f)*/);

            float l{ x == 0 ? 0 : heights[z * GRID_WIDTH + x - 1] };
            float r{ x == GRID_WIDTH - 1 ? 0 : heights[z * GRID_WIDTH + x + 1] };
            float b{ z == 0 ? 0 : heights[(z - 1) * GRID_WIDTH + x] };
            float t{ z == GRID_HEIGHT - 1 ? 0 : heights[(z + 1) * GRID_WIDTH + x] };

            it->pos.y = heights[x + z * GRID_WIDTH];
            it->normal = glm::normalize(glm::vec3(l - r, 1, b - t));

            ++it;
        }
    }

    meshMaterial_->mesh->Update(0, vertices);

    auto texture{ assets::AssetService::LoadTexture(heightMap, false) };
    heightMapTexture_ = TEXTURE_VIEW_ADD(texture, TextureView::CreateSampled(*texture));
    meshMaterial_->submeshes[0].material->SetTexture("heightMap", heightMapTexture_);
}

TextureViewRef Terrain::HeightMap() const {
    TextureViewRef ref;
    meshMaterial_->submeshes[0].material->Default().GetTexture("heightMap", ref);
    return ref;
}

MeshMaterialID Terrain::MeshMaterialID() const {
    return meshMaterial_.id();
}

void Terrain::SetLayerTextures(const TextureViewRef& layers) {
    meshMaterial_->submeshes[0].material->SetTexture("layerTextures", layers);
}

void Terrain::SetSplatTexture(const TextureViewRef& splat) {
    meshMaterial_->submeshes[0].material->SetTexture("splatMap", splat);
}

void Terrain::SetScale(const glm::vec2& scale) {
    meshMaterial_->submeshes[0].material->SetUniformVal("uvScale", scale);
}

void Terrain::SetHeightScale(float heightScale) {
    meshMaterial_->submeshes[0].material->SetUniformVal("heightScale", heightScale);
}

void Terrain::Render(const RenderContext& context, int lodLevel) const {
    if (meshMaterial_->submeshes[0].material->HasPass(context.pass)) {
        auto drawCall{ meshMaterial_->mesh->DrawCall() };

        //drawCalls.push_back(drawCall);
    }
}

} // namespace ugine::gfx
