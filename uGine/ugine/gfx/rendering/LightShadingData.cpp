﻿#include "LightShadingData.h"

#include <ugine/gfx/Graphics.h>
#include <ugine/gfx/data/ShaderBindings.h>
#include <ugine/gfx/rendering/ShadowMaps.h>
#include <ugine/gfx/tools/Initializers.h>

namespace ugine::gfx {

void FillLightShadingData(const Light& light, LightEntry& lightEntry) {
    switch (light.type) {
    case Light::Type::Directional:
        lightEntry.typeEnabled = LightEntry::Directional;
        break;
    case Light::Type::Point:
        lightEntry.typeEnabled = LightEntry::Point;
        break;
    case Light::Type::Spot:
        lightEntry.typeEnabled = LightEntry::Spot;
        break;
    default:
        UGINE_ASSERT(0 && "Invalid light type.");
        break;
    }

    lightEntry.typeEnabled = (lightEntry.typeEnabled << 1) | 0x1;
    lightEntry.ambientColor = light.ambientColor.rgba;
    lightEntry.color = light.color.rgba;
    lightEntry.intensity = light.intensity;
    lightEntry.range = light.range;
    lightEntry.spotAngleRad = glm::radians(light.spotAngleDeg);
}

LightShadingDataBinder::LightShadingDataBinder()
    : ALIGNED_SIZE{ static_cast<uint32_t>(utils::AlignTo<LightShadingData>(GraphicsService::Graphics().UniformBufferOffsetAlignment())) } {
    uniformBuffer_ = Buffer(ALIGNED_SIZE * GraphicsService::FramesInFlight(), vk::BufferUsageFlagBits::eUniformBuffer | vk::BufferUsageFlagBits::eTransferDst,
        vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);

    SetIbl(GraphicsService::NullTextureBlack(), GraphicsService::NullTextureCube(), GraphicsService::NullTextureCube());
    data_.hasIbl = false;

    lightBuffer_.resize(GraphicsService::FramesInFlight());
}

void LightShadingDataBinder::Bind(GPUCommandList cmd, const ShadowMaps& shadowMaps) {
    auto& device{ GraphicsService::Device() };
    const auto frame{ GraphicsService::ActiveFrameInFlight() };

    vk::DescriptorBufferInfo bufferDesc;
    bufferDesc.buffer = uniformBuffer_.GetBuffer();
    bufferDesc.offset = frame * ALIGNED_SIZE;
    bufferDesc.range = ALIGNED_SIZE;

    device.BindUniform(cmd, GLOBAL_SHADING_DATASET, 0, bufferDesc);
    device.BindStorage(cmd, GLOBAL_SHADING_DATASET, 1, lightBuffer_[lightBufferIndex_].Descriptor());

    std::vector<vk::DescriptorImageInfo> dirShadowImage(limits::MAX_DIR_SHADOWS);
    for (uint32_t i = 0; i < limits::MAX_DIR_SHADOWS; ++i) {
        dirShadowImage[i] = shadowMaps.DirShadowMap(frame, i).Descriptor();
    }
    device.BindImageSampler(cmd, GLOBAL_SHADING_DATASET, 2, dirShadowImage);

    std::vector<vk::DescriptorImageInfo> spotShadowImage(limits::MAX_SPOT_SHADOWS);
    for (uint32_t i = 0; i < limits::MAX_SPOT_SHADOWS; ++i) {
        spotShadowImage[i] = shadowMaps.SpotShadowMap(frame, i).Descriptor();
    }
    device.BindImageSampler(cmd, GLOBAL_SHADING_DATASET, 3, spotShadowImage);

    std::vector<vk::DescriptorImageInfo> pointShadowImage(limits::MAX_POINT_SHADOWS);
    for (uint32_t i = 0; i < limits::MAX_POINT_SHADOWS; ++i) {
        pointShadowImage[i] = shadowMaps.PointShadowMap(frame, i).Descriptor();
    }
    device.BindImageSampler(cmd, GLOBAL_SHADING_DATASET, 4, pointShadowImage);

    device.BindImageSampler(cmd, GLOBAL_SHADING_DATASET, 5, *brdfLut_);
    device.BindImageSampler(cmd, GLOBAL_SHADING_DATASET, 6, *skyBoxIrradiance_);
    device.BindImageSampler(cmd, GLOBAL_SHADING_DATASET, 7, *skyBoxPrefiltered_);
}

void LightShadingDataBinder::SetIbl(const TextureViewRef& brdfLut, const TextureViewRef& skyBoxIrradiance, const TextureViewRef& skyBoxPrefiltered) {
    brdfLut_ = brdfLut;
    skyBoxIrradiance_ = skyBoxIrradiance;
    skyBoxPrefiltered_ = skyBoxPrefiltered;
    data_.hasIbl = true;
}

void LightShadingDataBinder::ResetIbl() {
    SetIbl(GraphicsService::NullTextureBlack(), GraphicsService::NullTextureCube(), GraphicsService::NullTextureCube());
    data_.hasIbl = false;
}

void LightShadingDataBinder::Update(gfx::GPUCommandList command) {
    lightBufferIndex_ = (lightBufferIndex_ + 1) % GraphicsService::FramesInFlight();
    CopyLights(command);

    data_.count = static_cast<uint32_t>(lights_.size());
    memcpy(uniformBuffer_.Mapped(GraphicsService::ActiveFrameInFlight() * ALIGNED_SIZE), &data_, sizeof(LightShadingData));
}

void LightShadingDataBinder::CopyLights(gfx::GPUCommandList command) {
    const auto size{ lights_.size() * sizeof(LightEntry) };

    if (!lightBuffer_[lightBufferIndex_] || lightBuffer_[lightBufferIndex_].Size() < size) {
        lightBuffer_[lightBufferIndex_]
            = Buffer(2 * size, vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eStorageBuffer, vk::MemoryPropertyFlagBits::eDeviceLocal);
    }

    auto allocation{ GraphicsService::Device().AllocateGPU(command, size) };

    memcpy(allocation.memory, lights_.data(), size);
    //lightBuffer_[lightBufferIndex_].CopyTo(command, lightBuffer_[lightBufferIndex_], 0, 0, size);
    vk::BufferCopy range{};
    range.srcOffset = allocation.offset;
    range.dstOffset = 0;
    range.size = size;
    GraphicsService::Device().CopyBuffer(command, allocation.buffer, lightBuffer_[lightBufferIndex_].GetBuffer(), range);

    auto barrier{ GPUBarrier::Buffer(lightBuffer_[lightBufferIndex_], vk::AccessFlagBits::eTransferWrite, vk::AccessFlagBits::eShaderRead) };
    GraphicsService::Device().Barrier(command, barrier);
}

} // namespace ugine::gfx
