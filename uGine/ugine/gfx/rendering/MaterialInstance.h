﻿#pragma once

#include "Material.h"

#include <ugine/gfx/Storages.h>
#include <ugine/gfx/core/Device.h>
#include <ugine/gfx/data/ShaderBindings.h>
#include <ugine/gfx/pass/RenderingPass.h>
#include <ugine/utils/Utils.h>
#include <ugine/utils/Vector.h>

#include <map>

namespace ugine::gfx {

// Instance.
class MaterialPassInstance {
public:
    UGINE_MOVABLE_ONLY(MaterialPassInstance);

    MaterialPassInstance() = default;
    explicit MaterialPassInstance(const MaterialRef& material, RenderPassType pass);
    ~MaterialPassInstance();

    operator bool() const {
        return material_;
    }

    RenderPassType RenderPass() const {
        return pass_;
    }

    void Bind(GPUCommandList command) const;

    const gfx::MaterialPass& Material() const {
        return material_->GetPass(pass_);
    }

    template <typename T> void GetUniformVal(const std::string_view& paramName, T& value) const {
        ReadUniform(*Material().FindParamId(paramName), &value);
    }

    template <typename T> void SetUniformVal(const std::string_view& paramName, const T& value) {
        WriteUniform(*Material().FindParamId(paramName), &value);
    }

    template <typename T> void GetUniformVal(uint32_t paramId, T& value) const {
        ReadUniform(paramId, &value);
    }

    template <typename T> void SetUniformVal(uint32_t paramId, const T& value) {
        WriteUniform(paramId, &value);
    }

    void WriteUniform(uint32_t paramId, const void* value) {
        const auto& param{ Material().ParamDescriptors()[paramId] };
        UpdateUniform(param.binding, param.offset, value, param.size);
    }

    void ReadUniform(uint32_t paramId, void* value) const {
        const auto& param{ Material().ParamDescriptors()[paramId] };
        memcpy(value, reinterpret_cast<const char*>(ReadUniform(param.binding)) + param.offset, param.size);
    }

    void SetTexture(const std::string& paramName, const TextureViewRef& texture) {
        SetTexture(*Material().FindParamId(paramName), texture);
    }

    void GetTexture(const std::string& paramName, TextureViewRef& texture) const {
        GetTexture(*Material().FindParamId(paramName), texture);
    }

    void SetTexture(uint32_t paramId, const TextureViewRef& texture) {
        const auto& param{ Material().ParamDescriptors()[paramId] };
        UpdateTexture(param.binding, texture);
    }

    void GetTexture(uint32_t paramId, TextureViewRef& texture) const {
        const auto& param{ Material().ParamDescriptors()[paramId] };
        texture = ReadTexture(param.binding);
    }

    void SetStorage(const std::string& paramName, const BufferRef& buffer) {
        SetStorage(*Material().FindParamId(paramName), buffer);
    }

    void GetStorage(const std::string& paramName, BufferRef& buffer) const {
        GetStorage(*Material().FindParamId(paramName), buffer);
    }

    void SetStorage(uint32_t paramId, const BufferRef& buffer) {
        const auto& param{ Material().ParamDescriptors()[paramId] };
        UpdateStorageBuffer(param.binding, buffer);
    }

    void GetStorage(uint32_t paramId, BufferRef& buffer) const {
        const auto& param{ Material().ParamDescriptors()[paramId] };
        buffer = ReadStorageBuffer(param.binding);
    }

    void UpdateUniform(uint32_t binding, uint32_t offset, const void* data, size_t size);
    const void* ReadUniform(uint32_t binding) const;

    void UpdateTexture(uint32_t binding, const TextureViewRef& texture);
    const TextureViewRef& ReadTexture(uint32_t binding) const;

    void UpdateStorageBuffer(uint32_t binding, const BufferRef& storage);
    const BufferRef& ReadStorageBuffer(uint32_t binding) const;

private:
    const gfx::MaterialRef material_{};
    const RenderPassType pass_{};

    using SetBinding = std::pair<uint32_t, uint32_t>;

    struct Value {
        vk::DescriptorType type;
        BufferRef gpuBuffer;
        TextureViewRef gpuTexture;
        vk::ImageViewType imgType;
    };

    struct Dataset {
        std::map<uint32_t, Value> bindings;
    };

    void AddUniformBuffer(Dataset& dataset, uint32_t binding, uint32_t size);
    void AddStorageBuffer(Dataset& dataset, uint32_t binding);
    void AddTexture(Dataset& dataset, uint32_t binding, vk::ImageViewType type);

    std::vector<Dataset> datasets_;
};

class MaterialInstance {
public:
    UGINE_MOVABLE_ONLY(MaterialInstance);

    MaterialInstance() = default;
    MaterialInstance(const gfx::MaterialRef& material, const std::string_view& name = {});
    MaterialInstance(gfx::MaterialRef&& material, const std::string_view& name = {});
    ~MaterialInstance();

    const std::string& Name() const {
        return name_;
    }

    void SetName(const std::string& name) {
        name_ = name;
    }

    const gfx::Material& Material() const {
        return *material_;
    }

    const gfx::MaterialRef& MaterialRef() const {
        return material_;
    }

    operator bool() const {
        return material_;
    }

    void Bind(RenderPassType type, GPUCommandList command) const;

    bool HasPass(RenderPassType type) const;
    const MaterialPassInstance& Pass(RenderPassType type) const;
    const MaterialPassInstance& Default() const;
    MaterialPassInstance& Pass(RenderPassType type);
    MaterialPassInstance& Default();
    RenderPassType Passes() const;

    const std::vector<MaterialPassInstance>& RenderPasses() const {
        return passes_;
    }
    std::vector<MaterialPassInstance>& RenderPasses() {
        return passes_;
    }

    template <typename T> bool SetUniformVal(const std::string_view& paramName, const T& value) {
        bool result{};
        for (auto& pass : passes_) {
            if (auto id = pass.Material().FindParamId(paramName)) {
                pass.WriteUniform(*id, &value);
                result = true;
            }
        }
        return result;
    }

    bool SetTexture(const std::string& paramName, const TextureViewRef& texture) {
        bool result{};
        for (auto& pass : passes_) {
            if (auto id = pass.Material().FindParamId(paramName)) {
                pass.SetTexture(*id, texture);
                result = true;
            }
        }
        return result;
    }

    bool SetStorage(const std::string& paramName, const BufferRef& buffer) {
        bool result{};
        for (auto& pass : passes_) {
            if (auto id = pass.Material().FindParamId(paramName)) {
                pass.SetStorage(*id, buffer);
                result = true;
            }
        }
        return result;
    }

    bool Transparent() const {
        return material_->Transparent();
    }

private:
    void Initialize();

    gfx::MaterialRef material_{};
    std::vector<MaterialPassInstance> passes_;
    std::string name_{};
};

using MaterialInstances = utils::SafeDeleteStorage<MaterialInstance>;
using MaterialInstanceRef = MaterialInstances::Ref;
using MaterialInstanceID = MaterialInstances::Id;

#define MATERIAL_INSTANCE_ADD(...) STORAGE_ADD(MaterialInstances, __VA_ARGS__)

} // namespace ugine::gfx
