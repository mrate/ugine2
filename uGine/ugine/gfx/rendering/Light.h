﻿#pragma once

#include <ugine/gfx/Color.h>
#include <ugine/gfx/Gfx.h>

#include <glm/glm.hpp>

namespace ugine::gfx {

struct Light {
    enum class Type { Directional, Point, Spot };

    Type type{ Type::Directional };
    float intensity{ 1.0f };
    gfx::Color color{ 1.0f, 1.0f, 1.0f };
    gfx::Color ambientColor;
    float range{ 10.0f };
    float spotAngleDeg{ 45.0f };
};

} // namespace ugine::gfx
