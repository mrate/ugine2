﻿#pragma once

#include "MaterialPass.h"

#include <nlohmann/json_fwd.hpp>

namespace ugine::gfx {

class MaterialInstance;

void FillParamsToInstance(const MaterialParamValues& values, MaterialInstance& instance);
void ParseParamValues(const nlohmann::json& json, const std::vector<MaterialParam>& params, MaterialParamValues& values);
void TransferMaterialParams(const MaterialInstance& src, MaterialInstance& dst);

} // namespace ugine::gfx
