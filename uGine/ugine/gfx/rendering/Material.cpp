﻿#include "Material.h"
#include "MaterialHelpers.h"
#include "MaterialInstance.h"

#include <ugine/assets/AssetService.h>
#include <ugine/utils/Log.h>

#include <ugine/gfx/tools/VulkanJson.h>
#include <ugine/utils/Json.h>

#include <fstream>

namespace ugine::gfx {

void Material::FillDefaults(MaterialInstance& instance) const {
    FillParamsToInstance(defaults_, instance);
}

Material::Material(uint32_t id, const std::filesystem::path& materialFile)
    : id_{ id }
    , path_{ materialFile } {
    using namespace nlohmann;

    try {
        std::ifstream in(materialFile);
        if (!in.good()) {
            throw std::exception("File not found");
        }

        json j;
        in >> j;

        name_ = j.value<std::string>("name", "<empty>");
        transparent_ = j.value<bool>("transparent", false);
        category_ = j.value<std::string>("category", "");

        bool first{ true };
        for (const auto& passJson : j["pass"]) {
            RenderPassType types{};
            for (const auto& type : passJson["type"]) {
                types |= RernderPassTypeFromString(type);
            }

            auto& desc{ passJson["desc"] };

            PipelineDesc pipelineDesc;
            if (desc.contains("pipeline")) {
                ParsePipelineDesc(desc["pipeline"], pipelineDesc);
            }

            std::map<vk::ShaderStageFlagBits, std::filesystem::path> shaderStages;
            auto& shaderJson(desc["shader"]);
            for (auto [name, stage] : ShaderStages) {
                if (shaderJson.contains(name.c_str())) {
                    auto shaderPath{ shaderJson[name.c_str()].get<std::string>() };
                    shaderStages[stage] = shaderPath;
                    shaders_.insert(shaderPath);
                }
            }

            UGINE_ASSERT(!shaderStages.empty());

            if (types & RenderPassType::ShadowsCube) {
                MaterialPass pass{ static_cast<RenderPassType>(types & (RenderPassType::ShadowsCube)), pipelineDesc, shaderStages, name_ };
                passes_.push_back(std::move(pass));
                types &= ~RenderPassType::ShadowsCube;
            }

            if (types & (RenderPassType::Shadows | RenderPassType::Depth)) {
                MaterialPass pass{ static_cast<RenderPassType>(types & (RenderPassType::Shadows | RenderPassType::Depth)), pipelineDesc, shaderStages, name_ };
                passes_.push_back(std::move(pass));
                types &= ~(RenderPassType::Shadows | RenderPassType::Depth);
            }

            if (types == 0) {
                continue;
            }

            if (first) {
                defaultPass_ = types;
                first = false;
            }

            passes_.push_back(MaterialPass{ types, pipelineDesc, shaderStages, name_ });
        }

        if (j.contains("defaults")) {
            // TODO: Params
            for (auto& pass : passes_) {
                ParseParamValues(j["defaults"], pass.Params(), defaults_);
            }
        }
    } catch (const std::exception& ex) {
        UGINE_ERROR("Failed to parse material file {}: {}", materialFile.string(), ex.what());
        UGINE_THROW(GfxError, "Failed to parse material file.");
    }
}

Material::~Material() {}

bool Material::HasPass(RenderPassType type) const {
    for (const auto& pass : passes_) {
        if (pass.Type() & type) {
            return true;
        }
    }
    return false;
}

const MaterialPass& Material::GetPass(RenderPassType type) const {
    for (const auto& pass : passes_) {
        if (pass.Type() & type) {
            return pass;
        }
    }
    UGINE_THROW(GfxError, "Missing pass type.");
}

const MaterialPass& Material::DefaultPass() const {
    for (const auto& pass : passes_) {
        if (pass.Type() & defaultPass_) {
            return pass;
        }
    }
    UGINE_THROW(GfxError, "Missing pass type.");
}

} // namespace ugine::gfx
