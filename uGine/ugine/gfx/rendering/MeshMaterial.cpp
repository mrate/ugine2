﻿#include "MeshMaterial.h"

#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/import/MaterialImporter.h>
#include <ugine/gfx/import/MeshLoader.h>
#include <ugine/utils/Profile.h>

namespace ugine::gfx {

MeshMaterialRef ImportMeshMaterial(const ImportMeshSettings& import) {
    utils::ScopeTimer timer{ fmt::format("Loading MeshMaterial '{}'...", import.file.string()) };

    MeshLoader::Settings settings{};
    settings.axisUp = import.axisUp;
    settings.fixPaths = import.fixPaths;
    settings.scale = import.scale;
    settings.singleMesh = import.singleMesh;
    settings.preTransform = import.preTransform;

    MeshLoader loader{ import.file, settings };
    auto meshData{ loader.LoadMesh() };

    return ImportMeshMaterial(meshData, import.material);
}

MeshMaterialRef ImportMeshMaterial(const MeshData& meshData, const MaterialRef& material, int lodLevels) {
    uint32_t materialIndex{};
    std::vector<MaterialInstanceRef> materials;
    for (const auto& m : meshData.materials) {
        MaterialInstanceRef newMat;

        if (material) {
            newMat = MaterialService::Instantiate(material);
        } else {
            // TODO: Transparent.
            newMat = MaterialService::InstantiateDefault();
        }

        SetupGenericMaterial(m, newMat);

        std::string name;
        if (m.name.empty()) {
            name = fmt::format("{}#{}", meshData.name, materialIndex++);
        } else {
            name = m.name;
        }
        newMat->SetName(MaterialService::UniqueName(name));

        materials.push_back(newMat);
    }

    auto meshMaterial{ MESH_MATERIAL_ADD() };
    meshMaterial->mesh = MESH_ADD(lodLevels, meshData.vertices, meshData.indices, false, meshData.name);

    for (const auto& subMeshData : meshData.meshes) {
        MeshMaterial::SubMesh subMesh;
        subMesh.name = subMeshData.name;
        subMesh.indexCount = subMeshData.indexCount;
        subMesh.indexOffset = subMeshData.indexOffset;
        subMesh.vertexOffset = subMeshData.vertexOffset;
        subMesh.material = materials[subMeshData.material];

        meshMaterial->submeshes.push_back(subMesh);
    }

    return meshMaterial;
}

} // namespace ugine::gfx
