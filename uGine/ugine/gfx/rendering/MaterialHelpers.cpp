﻿#include "MaterialHelpers.h"
#include "MaterialInstance.h"

#include <ugine/assets/AssetService.h>
#include <ugine/gfx/Color.h>
#include <ugine/utils/Json.h>

#include <glm/glm.hpp>

#include <nlohmann/json.hpp>

namespace ugine::gfx {

void FillParamsToInstance(const MaterialParamValues& values, MaterialInstance& instance) {
    for (const auto& [name, val] : values.values) {
        switch (val.type) {
        case MaterialParam::Type::Float:
            instance.SetUniformVal(name, val.vec4.x);
            break;
        case MaterialParam::Type::Float2:
            instance.SetUniformVal(name, glm::vec2{ val.vec4.x, val.vec4.y });
            break;
        case MaterialParam::Type::Float3:
            instance.SetUniformVal(name, glm::vec3{ val.vec4.x, val.vec4.y, val.vec4.z });
            break;
        case MaterialParam::Type::Float4:
            instance.SetUniformVal(name, val.vec4);
            break;
        case MaterialParam::Type::Color:
            instance.SetUniformVal(name, Color{ val.vec4 });
            break;
        case MaterialParam::Type::Bool:
        case MaterialParam::Type::Int:
            instance.SetUniformVal(name, val.ivec4.x);
            break;
        case MaterialParam::Type::Int2:
            instance.SetUniformVal(name, glm::ivec2{ val.ivec4.x, val.ivec4.y });
            break;
        case MaterialParam::Type::Int3:
            instance.SetUniformVal(name, glm::ivec3{ val.ivec4.x, val.ivec4.y, val.ivec4.z });
            break;
        case MaterialParam::Type::Int4:
            instance.SetUniformVal(name, val.ivec4);
            break;
        case MaterialParam::Type::Matrix3x3:
        case MaterialParam::Type::Matrix4x4:
            UGINE_ASSERT("Implement me");
            break;
        case MaterialParam::Type::Texture2D:
        case MaterialParam::Type::Texture3D:
            if (!entt::any_cast<std::string>(val.texture).empty()) {
                auto texture{ assets::AssetService::LoadTexture(entt::any_cast<std::string>(val.texture), true) };
                instance.SetTexture(name, TEXTURE_VIEW_ADD(texture, TextureView::CreateSampled(*texture)));
            }
            break;
        case MaterialParam::Type::TextureCube:
            if (!entt::any_cast<std::string>(val.texture).empty()) {
                auto texture{ assets::AssetService::LoadCubeTexture(entt::any_cast<std::string>(val.texture)) };
                instance.SetTexture(name, TEXTURE_VIEW_ADD(texture, TextureView::CreateSampled(*texture)));
            }
            break;
        case MaterialParam::Type::StorageBuffer:
            // Nothing to do here. Size?
            break;
        }
    }
}

void ParseParamValues(const nlohmann::json& json, const std::vector<MaterialParam>& params, MaterialParamValues& values) {
    for (auto p : params) {
        MaterialParamValue value{};
        value.type = p.type;

        switch (p.type) {
        case MaterialParam::Type::Float:
        case MaterialParam::Type::Float2:
        case MaterialParam::Type::Float3:
        case MaterialParam::Type::Float4:
        case MaterialParam::Type::Color:
            value.vec4 = json.value<glm::vec4>(p.name, glm::vec4{ 0.0f });
            break;
        case MaterialParam::Type::Bool:
        case MaterialParam::Type::Int:
        case MaterialParam::Type::Int2:
        case MaterialParam::Type::Int3:
        case MaterialParam::Type::Int4:
            value.ivec4 = json.value<glm::ivec4>(p.name, glm::ivec4{ 0 });
            break;
        case MaterialParam::Type::Texture2D:
        case MaterialParam::Type::Texture3D:
        case MaterialParam::Type::TextureCube:
            value.texture = json.value<std::string>(p.name, "");
            break;
        case MaterialParam::Type::StorageBuffer:
            // Nothing to do here. Size?
            break;
        }

        values.values.insert(std::make_pair(p.name, std::move(value)));
    }
}

template <typename T> void Copy(const MaterialPassInstance& from, MaterialPassInstance& to, uint32_t srcId, uint32_t dstId) {
    T v;
    from.GetUniformVal<T>(srcId, v);
    to.SetUniformVal<T>(dstId, v);
}

void CopyTexture(const MaterialPassInstance& from, MaterialPassInstance& to, uint32_t srcId, uint32_t dstId) {
    TextureViewRef v;
    from.GetTexture(srcId, v);
    to.SetTexture(dstId, v);
}

void CopyStorage(const MaterialPassInstance& from, MaterialPassInstance& to, uint32_t srcId, uint32_t dstId) {
    BufferRef buffer;
    from.GetStorage(srcId, buffer);
    to.SetStorage(dstId, buffer);
}

void TransferMaterialParams(const MaterialInstance& src, MaterialInstance& dst) {
    //for (const auto& p : src.Passes()) {
    //    if (dst.HasPass(p)) {
    const auto& srcPass{ src.Default() };
    auto& dstPass{ dst.Default() };

    auto srcParams{ srcPass.Material().Params() };
    auto dstParams{ dstPass.Material().Params() };

    for (auto& srcParam : srcParams) {
        auto dstParam{ std::find_if(dstParams.begin(), dstParams.end(), [&](const auto& p) { return p.name == srcParam.name && p.type == srcParam.type; }) };

        if (dstParam != dstParams.end()) {
            switch (srcParam.type) {
            case MaterialParam::Type::Float:
                Copy<float>(srcPass, dstPass, srcParam.id, dstParam->id);
                break;
            case MaterialParam::Type::Float2:
                Copy<glm::vec2>(srcPass, dstPass, srcParam.id, dstParam->id);
                break;
            case MaterialParam::Type::Float3:
                Copy<glm::vec3>(srcPass, dstPass, srcParam.id, dstParam->id);
                break;
            case MaterialParam::Type::Float4:
                Copy<glm::vec4>(srcPass, dstPass, srcParam.id, dstParam->id);
                break;
            case MaterialParam::Type::Int:
                Copy<int>(srcPass, dstPass, srcParam.id, dstParam->id);
                break;
            case MaterialParam::Type::Int2:
                Copy<glm::ivec2>(srcPass, dstPass, srcParam.id, dstParam->id);
                break;
            case MaterialParam::Type::Int3:
                Copy<glm::ivec3>(srcPass, dstPass, srcParam.id, dstParam->id);
                break;
            case MaterialParam::Type::Int4:
                Copy<glm::ivec4>(srcPass, dstPass, srcParam.id, dstParam->id);
                break;
            case MaterialParam::Type::Bool:
                Copy<bool>(srcPass, dstPass, srcParam.id, dstParam->id);
                break;
            case MaterialParam::Type::Color:
                Copy<Color>(srcPass, dstPass, srcParam.id, dstParam->id);
                break;
            case MaterialParam::Type::Texture2D:
            case MaterialParam::Type::Texture3D:
            case MaterialParam::Type::TextureCube:
                CopyTexture(srcPass, dstPass, srcParam.id, dstParam->id);
                break;
            case MaterialParam::Type::StorageBuffer:
                CopyStorage(srcPass, dstPass, srcParam.id, dstParam->id);
                break;
            }
        }
        //    }
        //}
    }
}

} // namespace ugine::gfx
