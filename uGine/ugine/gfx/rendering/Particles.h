﻿#pragma once

#include <ugine/gfx/core/Buffer.h>
#include <ugine/gfx/core/ComputePipeline.h>
#include <ugine/gfx/core/DescriptorSetPool.h>
#include <ugine/gfx/rendering/MaterialInstance.h>
#include <ugine/gfx/rendering/Renderable.h>
#include <ugine/math/Aabb.h>

#include <glm/glm.hpp>

namespace ugine::gfx {

class Particles {
public:
    UGINE_MOVABLE_ONLY(Particles);

    static constexpr uint32_t PARTICLE_DATASET{ 0 };

    static constexpr uint32_t PARTICLE_BINDING{ 0 };
    static constexpr uint32_t COUNTER_BINDING{ 1 };
    static constexpr uint32_t DEAD_BINDING{ 2 };
    static constexpr uint32_t ALIVE1_BINDING{ 3 };
    static constexpr uint32_t ALIVE2_BINDING{ 4 };
    static constexpr uint32_t EMITTER_BINDING{ 5 };
    static constexpr uint32_t INDIRECT_EMIT_BINDING{ 6 };
    static constexpr uint32_t INDIRECT_SIMULATION_BINDING{ 7 };

    static constexpr uint32_t MESH_VERTICES_BINDING{ 6 };
    static constexpr uint32_t MESH_INDICES_BINDING{ 7 };

    static constexpr uint32_t FINISH_INDIRECT_BINDING{ 5 };
    static constexpr uint32_t FINISH_PARAMS_BINDING{ 6 };

    static constexpr uint32_t MAX_PARTICLES{ 10 };

    struct ParticleCB {
        glm::vec4 color;
        glm::vec4 sizeBeginEnd;
        glm::vec3 position;
        float mass;
        glm::vec3 speed;
        float param1;
        glm::vec3 life;
        float param2;
    };

    Particles();
    ~Particles();

    void Render(const RenderContext& context) const;

    // TODO:
    void Update(GPUCommandList command, ComputePipeline& initP, ComputePipeline& emitP, ComputePipeline& simulateP, ComputePipeline& finishP,
        const Buffer* meshVerex, const Buffer* meshIndex, const DrawCall& dc);

    void AcquireCompute(GPUCommandList command);
    void ReleaseCompute(GPUCommandList command);
    void AcquireRender(GPUCommandList command);
    void ReleaseRender(GPUCommandList command);

    const MaterialInstance& Material() const {
        return *material_;
    }

    MaterialInstance& Material() {
        return *material_;
    }

    // TODO: Params
    void SetCount(uint32_t count);
    void SetTexture(const TextureViewRef& texture);
    void SetColor(const Color& color);
    void SetSize(const glm::vec2& size);
    void SetLife(float life, float rand);
    void SetSpeed(const glm::vec3& speed);
    void SetTransformation(const glm::mat4& transform);
    void SetStartOffset(const glm::vec3& offset);
    void SetSpeedOffset(const glm::vec3& offset);
    void SetGravity(const glm::vec3& gravity);
    void SetEmitCount(uint32_t emitCount);

    uint32_t Count() const;

    const math::AABB& Aabb() const {
        return aabb_;
    }

private:
    struct EmitterCB {
        glm::mat4 modelMatrix;
        glm::vec3 gravity{};
        float timeDelta{ 0.0f };
        glm::vec3 startSpeed{ 0.0f, 1.0f, 0.0f };
        float seed{ 0.0f };
        glm::vec3 positionOffset{};
        uint32_t count{ 0 };
        glm::vec3 speedOffset{};
        uint32_t emitCnt{ 0 };
        glm::vec4 color{ 1.0f };
        glm::vec2 size{ 1.0f, 1.0f };
        glm::vec2 life{ 100.0f, 10.0f };
        uint32_t meshIndexCount;
        uint32_t meshVertexStart;
        uint32_t meshIndexStart;
    };

    struct CounterCB {
        uint32_t deadCnt;
        uint32_t liveCurCnt;
        uint32_t liveNewCnt;
        uint32_t emitCnt;
    };

    struct FinishCB {
        uint32_t count;
    };

    struct Storage {
        uint32_t singleSize;
        gfx::BufferRef buffer;

        void Init(const char* name, uint32_t size, vk::BufferUsageFlagBits usage = {});
        void Init(const char* name, const void* initData, uint32_t size, vk::BufferUsageFlagBits usage = {});
        vk::DeviceSize Offset() const;
        vk::DescriptorBufferInfo Descriptor() const;
    };

    void UpdateAabb();

    uint32_t count_{};

    Storage particleBuffer_;
    Storage counterBuffer_;
    Storage alive1Buffer_;
    Storage alive2Buffer_;
    Storage deadBuffer_;

    Storage indirectDrawBuffer_;

    EmitterCB emitterParams_;

    MaterialInstanceRef material_;

    math::AABB aabb_;
};

} // namespace ugine::gfx
