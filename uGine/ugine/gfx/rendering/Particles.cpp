﻿#include "Particles.h"

#include <ugine/gfx/Graphics.h>
#include <ugine/gfx/GraphicsService.h>
#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/data/PipelineCache.h>
#include <ugine/gfx/tools/Initializers.h>
#include <ugine/math/Math.h>
#include <ugine/utils/Align.h>

#include <random>

namespace ugine::gfx {

Particles::Particles()
    : count_{ 0 } {
    particleBuffer_.Init("ParticleBuffer", MAX_PARTICLES * sizeof(ParticleCB));

    CounterCB counterData;
    counterData.emitCnt = 0;
    counterData.liveCurCnt = 0;
    counterData.liveNewCnt = 0;
    counterData.deadCnt = MAX_PARTICLES;
    counterBuffer_.Init("ParticleCounter", &counterData, sizeof(CounterCB));

    alive1Buffer_.Init("ParticleLive1", MAX_PARTICLES * sizeof(uint32_t));
    alive2Buffer_.Init("ParticleLive2", MAX_PARTICLES * sizeof(uint32_t));

    // TODO:
    std::vector<uint32_t> deadCnt(MAX_PARTICLES);
    std::generate(deadCnt.begin(), deadCnt.end(), [n = MAX_PARTICLES]() mutable { return --n; });
    deadBuffer_.Init("ParticleDead", deadCnt.data(), MAX_PARTICLES * sizeof(uint32_t));

    IndirectIndexedDrawCB indirect{};
    indirect.firstIndex = 0;
    indirect.firstInstance = 0;
    indirect.indexCount = 6;
    indirect.vertexOffset = 0;
    indirectDrawBuffer_.Init("ParticleIndirect", &indirect, sizeof(IndirectIndexedDrawCB), vk::BufferUsageFlagBits::eIndirectBuffer);

    material_ = MaterialService::InstantiateByPath("assets/materials/particlesRgb.mat");
    material_->SetStorage("data", particleBuffer_.buffer);
    material_->SetStorage("live", alive2Buffer_.buffer);

    auto cmd{ GraphicsService::Device().BeginCommandList() };
    GraphicsService::BufferReleaseRender(cmd, particleBuffer_.buffer->GetBuffer(), particleBuffer_.Offset(), particleBuffer_.singleSize);
    GraphicsService::Device().SubmitCommandLists();
    GraphicsService::Device().WaitGpu();
}

Particles::~Particles() {}

void Particles::Render(const RenderContext& context) const {
    if (count_ > 0) {
        auto& device{ GraphicsService::Device() };

        auto pipeline{ PipelineCache::Instance().GetPipeline(context, *material_) };
        device.BindPipeline(context.command, pipeline);
        material_->Pass(context.pass).Bind(context.command);

        device.DrawIndirect(context.command, indirectDrawBuffer_.buffer->GetBuffer(), 0, 1, sizeof(IndirectDrawCB));

        device.ResetDescriptor(context.command, MATERIAL_DATASET);
    }
}

void Particles::UpdateAabb() {
    //float maxLife{ emitterParams_.life.x + emitterParams_.life.y };
    //float maxLife2{ maxLife * maxLife };

    //glm::vec3 startDistance{ glm::abs(emitterParams_.positionOffset) };

    //glm::vec3 minStartSpeed{ emitterParams_.startSpeed - 0.5f * emitterParams_.speedOffset };
    //glm::vec3 maxStartSpeed{ emitterParams_.startSpeed + 0.5f * emitterParams_.speedOffset };

    ////// s = v0 * t + 1/2 * a * t^2
    //glm::vec3 minDistance{ minStartSpeed * maxLife + 0.5f * emitterParams_.gravity * maxLife2 };
    //glm::vec3 maxDistance{ maxStartSpeed * maxLife + 0.5f * emitterParams_.gravity * maxLife2 };

    //aabb_ = math::AABB(-startDistance, startDistance);
    //aabb_ = math::AABB(glm::min(minDistance, maxDistance), glm::max(minDistance, maxDistance));
    aabb_ = math::AABB(glm::vec3{ -0.5f }, glm::vec3{ 0.5f });
}

void Particles::Update(GPUCommandList command, ComputePipeline& initP, ComputePipeline& emitP, ComputePipeline& simulateP, ComputePipeline& finishP,
    const Buffer* meshVertex, const Buffer* meshIndex, const DrawCall& dc) {
    AcquireCompute(command);

    auto& device{ GraphicsService::Device() };

    GPUBarrier memoryBarrier{ GPUBarrier::Memory() };

    auto emitBuf{ device.AllocateGPU(command, sizeof(EmitterCB)) };
    {
        emitterParams_.timeDelta = core::CoreService::TimeDiffS();
        emitterParams_.seed = static_cast<float>(core::CoreService::Time() + math::RandomFloat());
        emitterParams_.meshIndexCount = dc.indexCount;
        emitterParams_.meshVertexStart = dc.vertexOffset;
        emitterParams_.meshIndexStart = dc.indexStart;
        memcpy(emitBuf.memory, &emitterParams_, sizeof(EmitterCB));
    }

    auto indirectEmit{ device.AllocateGPU(command, sizeof(IndirectDispatchCB)) };
    auto indirectSimul{ device.AllocateGPU(command, sizeof(IndirectDispatchCB)) };

    device.Barrier(command,
        GPUBarrier::Buffer(indirectEmit.buffer, indirectEmit.offset, sizeof(IndirectDispatchCB), vk::AccessFlagBits::eIndirectCommandRead,
            vk::AccessFlagBits::eShaderRead | vk::AccessFlagBits::eShaderWrite));
    device.Barrier(command,
        GPUBarrier::Buffer(indirectSimul.buffer, indirectSimul.offset, sizeof(IndirectDispatchCB), vk::AccessFlagBits::eIndirectCommandRead,
            vk::AccessFlagBits::eShaderRead | vk::AccessFlagBits::eShaderWrite));

    { // Init.
        device.BindStorage(command, PARTICLE_DATASET, PARTICLE_BINDING, particleBuffer_.Descriptor());
        device.BindStorage(command, PARTICLE_DATASET, COUNTER_BINDING, counterBuffer_.Descriptor());
        device.BindStorage(command, PARTICLE_DATASET, DEAD_BINDING, deadBuffer_.Descriptor());
        device.BindStorage(command, PARTICLE_DATASET, ALIVE1_BINDING, alive1Buffer_.Descriptor());
        device.BindStorage(command, PARTICLE_DATASET, ALIVE2_BINDING, alive2Buffer_.Descriptor());
        device.BindUniform(command, PARTICLE_DATASET, EMITTER_BINDING, emitBuf.buffer, emitBuf.offset, emitBuf.size);
        device.BindStorage(command, PARTICLE_DATASET, INDIRECT_EMIT_BINDING, indirectEmit.buffer, indirectEmit.offset, indirectEmit.size);
        device.BindStorage(command, PARTICLE_DATASET, INDIRECT_SIMULATION_BINDING, indirectSimul.buffer, indirectSimul.offset, indirectSimul.size);
        device.BindPipeline(command, &initP);
        device.Dispatch(command, 1, 1, 1);

        device.ResetBinding(command, PARTICLE_DATASET, INDIRECT_EMIT_BINDING, 2);

        device.Barrier(command, memoryBarrier);
    }

    device.Barrier(command,
        GPUBarrier::Buffer(indirectEmit.buffer, indirectEmit.offset, sizeof(IndirectDispatchCB),
            vk::AccessFlagBits::eShaderRead | vk::AccessFlagBits::eShaderWrite, vk::AccessFlagBits::eIndirectCommandRead));
    device.Barrier(command,
        GPUBarrier::Buffer(indirectSimul.buffer, indirectSimul.offset, sizeof(IndirectDispatchCB),
            vk::AccessFlagBits::eShaderRead | vk::AccessFlagBits::eShaderWrite, vk::AccessFlagBits::eIndirectCommandRead));

    { // Emit.
        if (meshVertex && meshIndex) {
            device.BindStorage(command, PARTICLE_DATASET, MESH_VERTICES_BINDING, meshVertex->Descriptor());
            device.BindStorage(command, PARTICLE_DATASET, MESH_INDICES_BINDING, meshIndex->Descriptor());
        }

        device.BindPipeline(command, &emitP);
        device.DispatchIndirect(command, indirectEmit.buffer, indirectEmit.offset);

        if (meshVertex && meshIndex) {
            device.ResetBinding(command, PARTICLE_DATASET, MESH_VERTICES_BINDING, 2);
        }

        device.Barrier(command, memoryBarrier);
    }

    { // Simulate.
        device.BindPipeline(command, &simulateP);
        device.DispatchIndirect(command, indirectSimul.buffer, indirectSimul.offset);
        device.ResetBinding(command, PARTICLE_DATASET, EMITTER_BINDING);

        device.Barrier(command, memoryBarrier);
    }

    device.Barrier(command,
        GPUBarrier::Buffer(
            *indirectDrawBuffer_.buffer, vk::AccessFlagBits::eIndirectCommandRead, vk::AccessFlagBits::eShaderRead | vk::AccessFlagBits::eShaderWrite));

    auto finishBuf{ device.AllocateGPU(command, sizeof(FinishCB)) };
    {
        FinishCB finishCB_{};
        finishCB_.count = 1;
        memcpy(finishBuf.memory, &finishCB_, sizeof(FinishCB));
    }

    { // Finish.
        device.BindStorage(command, PARTICLE_DATASET, FINISH_INDIRECT_BINDING, indirectDrawBuffer_.Descriptor());
        device.BindUniform(command, PARTICLE_DATASET, FINISH_PARAMS_BINDING, finishBuf.buffer, finishBuf.offset, finishBuf.size);

        device.BindPipeline(command, &finishP);
        device.Dispatch(command, 1, 1, 1);

        device.ResetDescriptor(command, PARTICLE_DATASET);
    }

    device.Barrier(command,
        GPUBarrier::Buffer(
            *indirectDrawBuffer_.buffer, vk::AccessFlagBits::eShaderRead | vk::AccessFlagBits::eShaderWrite, vk::AccessFlagBits::eIndirectCommandRead));

    ReleaseCompute(command);

    device.Free(command, indirectSimul);
    device.Free(command, indirectEmit);
    device.Free(command, emitBuf);
}

void Particles::AcquireRender(GPUCommandList command) {
    GraphicsService::BufferAcquireRender(command, particleBuffer_.buffer->GetBuffer(), particleBuffer_.Offset(), particleBuffer_.singleSize);
}

void Particles::ReleaseRender(GPUCommandList command) {
    GraphicsService::BufferReleaseRender(command, particleBuffer_.buffer->GetBuffer(), particleBuffer_.Offset(), particleBuffer_.singleSize);
}

void Particles::AcquireCompute(GPUCommandList command) {
    GraphicsService::BufferAcquireCompute(command, particleBuffer_.buffer->GetBuffer(), particleBuffer_.Offset(), particleBuffer_.singleSize);
}

void Particles::ReleaseCompute(GPUCommandList command) {
    GraphicsService::BufferReleaseCompute(command, particleBuffer_.buffer->GetBuffer(), particleBuffer_.Offset(), particleBuffer_.singleSize);
}

void Particles::SetCount(uint32_t count) {
    count_ = count;
    emitterParams_.count = count_;
}

void Particles::SetTexture(const TextureViewRef& texture) {
    TextureViewRef tex;
    material_->Default().GetTexture("inputTexture", tex);

    if (tex != texture) {
        material_->SetTexture("inputTexture", texture);
    }
}

void Particles::SetColor(const Color& color) {
    emitterParams_.color = color.rgba;
}

void Particles::SetSize(const glm::vec2& size) {
    emitterParams_.size = size;
    UpdateAabb();
}

void Particles::SetLife(float life, float rand) {
    emitterParams_.life = glm::vec2{ life, rand };
    UpdateAabb();
}

void Particles::SetSpeed(const glm::vec3& speed) {
    emitterParams_.startSpeed = speed;
    UpdateAabb();
}

void Particles::SetTransformation(const glm::mat4& pos) {
    emitterParams_.modelMatrix = pos;
}

void Particles::SetStartOffset(const glm::vec3& offset) {
    emitterParams_.positionOffset = offset;
    UpdateAabb();
}

void Particles::SetSpeedOffset(const glm::vec3& offset) {
    emitterParams_.speedOffset = offset;
    UpdateAabb();
}

void Particles::SetGravity(const glm::vec3& gravity) {
    emitterParams_.gravity = gravity;
    UpdateAabb();
}

void Particles::SetEmitCount(uint32_t emitCount) {
    emitterParams_.emitCnt = emitCount;
}

uint32_t Particles::Count() const {
    return count_;
}

vk::DescriptorBufferInfo Particles::Storage::Descriptor() const {
    vk::DescriptorBufferInfo desc{};
    desc.offset = buffer->Offset();
    desc.range = singleSize;
    desc.buffer = buffer->GetBuffer();

    return desc;
}

void Particles::Storage::Init(const char* name, uint32_t size, vk::BufferUsageFlagBits usage) {
    singleSize = size;

    buffer = BUFFER_ADD(
        gfx::Buffer(size, vk::BufferUsageFlagBits::eStorageBuffer | vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eTransferSrc | usage,
            vk::MemoryPropertyFlagBits::eDeviceLocal));
    buffer->SetName(name);
}

void Particles::Storage::Init(const char* name, const void* initData, uint32_t size, vk::BufferUsageFlagBits usage) {
    singleSize = size;

    buffer = BUFFER_ADD(gfx::Buffer::CreateFromData(initData, size,
        vk::BufferUsageFlagBits::eStorageBuffer | vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eTransferSrc | usage,
        vk::MemoryPropertyFlagBits::eDeviceLocal));
    buffer->SetName(name);
}

vk::DeviceSize Particles::Storage::Offset() const {
    return 0;
}

} // namespace ugine::gfx
