﻿#include "Foliage.h"

#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/core/Image.h>
#include <ugine/gfx/data/PipelineCache.h>
#include <ugine/gfx/data/ShaderBindings.h>
#include <ugine/math/Poisson.h>

#include <random>

namespace ugine::gfx {

// TODO: Pipeline.
Foliage::Foliage(const ComputePipeline* compute)
    : cullLodPipeline_{ compute } {
    positionBuffer_ = BUFFER_ADD(MAX_INSTANCES * sizeof(Position),
        vk::BufferUsageFlagBits::eStorageBuffer | vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eTransferSrc,
        vk::MemoryPropertyFlagBits::eDeviceLocal);

    lodSize_ = MAX_INSTANCES * sizeof(glm::mat4);

    drawInstancesBuffer_ = BUFFER_ADD(MAX_LOD * lodSize_,
        vk::BufferUsageFlagBits::eVertexBuffer | vk::BufferUsageFlagBits::eStorageBuffer | vk::BufferUsageFlagBits::eTransferDst
            | vk::BufferUsageFlagBits::eTransferSrc,
        vk::MemoryPropertyFlagBits::eDeviceLocal);
}

Foliage::~Foliage() {}

void Foliage::Update(const RenderContext& context, const glm::mat4& world) {
    auto command{ context.command };

    auto& device{ GraphicsService::Device() };

    auto cullParamsBuff{ device.AllocateGPU(command, sizeof(CullParamsCB)) };

    { // Cull params.
        CullParamsCB cull;
        cull.world = world;
        cull.lodLevels = static_cast<uint32_t>(foliage_->lodLevels.size());
        cull.aabbMin = foliage_->lodLevels[0].meshMaterial->mesh->Aabb().Min(); // TODO:
        cull.aabbMax = foliage_->lodLevels[0].meshMaterial->mesh->Aabb().Max(); // TODO:
        cull.count = count_;
        for (int i = 0; i < 6; ++i) {
            cull.frustumPlanes[i] = context.camera->Frustum().planes[i];
        }
        for (int i{ 0 }; i < static_cast<int>(foliage_->lodLevels.size()); ++i) {
            cull.lodDistances[i] = foliage_->lodLevels[i].maxDistance;
            cull.subMeshes[i] = static_cast<int>(foliage_->lodLevels[i].meshMaterial ? foliage_->lodLevels[i].meshMaterial->submeshes.size() : 0);
        }

        auto size{ sizeof(CullParamsCB) };
        auto size1{ sizeof(glm::vec4) };
        memcpy(cullParamsBuff.memory, &cull, sizeof(CullParamsCB));
    }

    { // Indirect buffer.
        device.UpdateBuffer(command, drawIndirectBuffer_->GetBuffer(), drawIndirectBuffer_->Offset(), drawIndirectBuffer_->Size(), indirectDraw_.data());

        device.Barrier(command,
            GPUBarrier::Buffer(
                *drawIndirectBuffer_, vk::AccessFlagBits::eIndirectCommandRead, vk::AccessFlagBits::eShaderRead | vk::AccessFlagBits::eShaderWrite));
    }

    { // Cull + lod.
        device.BindStorage(command, CULL_DATASET, POSITION_BINDING, positionBuffer_->Descriptor());
        device.BindStorage(command, CULL_DATASET, DRAW_INSTANCE_BINDING0, drawInstancesBuffer_->GetBuffer(), drawInstancesBuffer_->Offset(), lodSize_);
        device.BindStorage(
            command, CULL_DATASET, DRAW_INSTANCE_BINDING1, drawInstancesBuffer_->GetBuffer(), drawInstancesBuffer_->Offset() + lodSize_, lodSize_);
        device.BindStorage(
            command, CULL_DATASET, DRAW_INSTANCE_BINDING2, drawInstancesBuffer_->GetBuffer(), drawInstancesBuffer_->Offset() + 2 * lodSize_, lodSize_);
        device.BindStorage(
            command, CULL_DATASET, DRAW_INSTANCE_BINDING3, drawInstancesBuffer_->GetBuffer(), drawInstancesBuffer_->Offset() + 3 * lodSize_, lodSize_);
        device.BindStorage(command, CULL_DATASET, INDIRECT_BINDING, drawIndirectBuffer_->Descriptor());
        device.BindUniform(command, CULL_DATASET, PARAM_BINDING, cullParamsBuff.buffer, cullParamsBuff.offset, sizeof(CullParamsCB));

        device.BindPipeline(command, cullLodPipeline_);
        device.Dispatch(command, static_cast<uint32_t>(std::ceil(static_cast<float>(count_) / static_cast<float>(CULLLOD_THREAD_COUNT))), 1, 1);
    }

    device.Barrier(command,
        GPUBarrier::Buffer(*drawIndirectBuffer_, vk::AccessFlagBits::eShaderRead | vk::AccessFlagBits::eShaderWrite, vk::AccessFlagBits::eIndirectCommandRead));
    device.ResetDescriptor(command, CULL_DATASET);

    device.FlushBarriers(command);
    //device.Free(command, cullParamsBuff);
}

void Foliage::Render(const RenderContext& context) const {
    bool bound{};

    auto& device{ GraphicsService::Device() };

    uint32_t drawIndex{};
    for (uint32_t lod{ 0 }; lod < foliage_->lodLevels.size(); ++lod) {
        const auto& foliageLod{ foliage_->lodLevels[lod] };

        if (!foliageLod.meshMaterial) {
            continue;
        }

        const auto& vbo{ foliageLod.meshMaterial->mesh->VertexBuffer() };

        vk::Buffer vboBuffers[2] = {
            vbo->VertexBuf().GetBuffer(),
            drawInstancesBuffer_->GetBuffer(),
        };

        vk::DeviceSize offsets[2] = {
            vbo->VertexBuf().Offset(),
            drawInstancesBuffer_->Offset() + lod * lodSize_,
        };

        device.BindVertexBuffers(context.command, 2, vboBuffers, offsets);
        device.BindIndexBuffer(context.command, vbo->IndexBuf().GetBuffer(), 0, vbo->IndexType());

        for (const auto& subMesh : foliageLod.meshMaterial->submeshes) {
            if (subMesh.material->HasPass(context.pass)) {
                bound = true;

                auto pipeline{ PipelineCache::Instance().GetPipeline(context, *subMesh.material) };
                device.BindPipeline(context.command, pipeline);
                subMesh.material->Bind(context.pass, context.command);

                device.DrawIndexedIndirect(
                    context.command, drawIndirectBuffer_->GetBuffer(), drawIndex * sizeof(IndirectIndexedDrawCB), 1, sizeof(IndirectIndexedDrawCB));
            }

            ++drawIndex;
        }
    }

    if (bound) {
        device.ResetDescriptor(context.command, MATERIAL_DATASET);
    }
}

void Foliage::SetCount(uint32_t count) {
    count_ = std::min<uint32_t>(MAX_INSTANCES, count);
}

void Foliage::Place(const glm::vec3& mmin, const glm::vec3& mmax, const Image* heightMap) {
    if (count_ == 0 || !foliage_) {
        return;
    }

    auto rangeX{ abs(mmin.x - mmax.x) };
    auto rangeZ{ abs(mmin.z - mmax.z) };

    std::random_device r;
    std::mt19937 e(r());
    std::uniform_real_distribution<> distribY(mmin.y, mmax.y);
    std::uniform_real_distribution<> distribA(0, 2 * math::PI);

    std::vector<Position> newPositions;

    std::vector<glm::vec2> output;
    math::FastPoissonDisk2D(mmax.x - mmin.x, mmax.z - mmin.z, foliage_->minDistance, output, count_);
    count_ = std::min(static_cast<uint32_t>(output.size()), MAX_INSTANCES);

    for (const auto& out : output) {
        Position pos;
        pos.positionYaw.x = out.x;
        pos.positionYaw.z = out.y;
        if (heightMap) {
            float nX{ out.x / rangeX };
            float nZ{ out.y / rangeZ };

            pos.positionYaw.y = static_cast<float>(heightMap->At(nX, nZ) & 0xff) / 255.0f;
        } else {
            pos.positionYaw.y = static_cast<float>(distribY(e));
        }

        pos.positionYaw.w = static_cast<float>(distribA(e));
        newPositions.push_back(pos);
    }

    positionBuffer_->CopyData(newPositions.data(), newPositions.size() * sizeof(Position), 0);

    //std::vector<Position> newPositions(count - count_);

    // TODO: Fill positions.
    //std::random_device r;
    //std::mt19937 e(r());
    //std::uniform_real_distribution<> distribX(mmin.x, mmax.x);
    //std::uniform_real_distribution<> distribY(mmin.y, mmax.y);
    //std::uniform_real_distribution<> distribZ(mmin.z, mmax.z);
    //std::uniform_real_distribution<> distribA(0, 2 * 3.1415926f); // TODO: Const.

    //for (auto& pos : newPositions) {
    //    auto x{ static_cast<float>(distribX(e)) };
    //    float y{};
    //    auto z{ static_cast<float>(distribZ(e)) };
    //    if (heightMap) {
    //        float nX{ x / rangeX };
    //        float nZ{ z / rangeZ };

    //        y = static_cast<float>(heightMap->At(nX, nZ) & 0xff) / 255.0f;
    //    } else {
    //        y = static_cast<float>(distribY(e));
    //    }

    //    float yaw{ static_cast<float>(distribA(e)) };
    //    pos.positionYaw = glm::vec4{ x, y, z, yaw };
    //}

    //positionBuffer_->CopyData(newPositions.data(), newPositions.size() * sizeof(Position), count_ * sizeof(Position));
}

void Foliage::SetFoliageDesc(const FoliageRef& foliage) {
    if (foliage_ == foliage) {
        return;
    }

    foliage_ = foliage;

    uint32_t indirectCount{};
    for (const auto& lodLevel : foliage->lodLevels) {
        indirectCount += static_cast<uint32_t>(lodLevel.meshMaterial->submeshes.size());
    }

    if (indirectCount > 0) {
        drawIndirectBuffer_ = BUFFER_ADD(indirectCount * sizeof(IndirectIndexedDrawCB),
            vk::BufferUsageFlagBits::eStorageBuffer | vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eTransferSrc
                | vk::BufferUsageFlagBits::eIndirectBuffer,
            vk::MemoryPropertyFlagBits::eDeviceLocal);
    }

    // Prepare default indirect draw buffers for each mesh and it's submesh.
    uint32_t index{};
    indirectDraw_.resize(indirectCount);
    for (const auto& lod : foliage_->lodLevels) {
        if (!lod.meshMaterial) {
            UGINE_ASSERT(false);
            break;
        }

        for (uint32_t subMesh{ 0 }; subMesh < lod.meshMaterial->submeshes.size(); ++subMesh) {
            auto dc{ lod.meshMaterial->mesh->DrawCall() };

            indirectDraw_[index].indexCount = lod.meshMaterial->submeshes[subMesh].indexCount;
            indirectDraw_[index].instanceCount = 0;
            indirectDraw_[index].firstIndex = dc.indexStart + lod.meshMaterial->submeshes[subMesh].indexOffset;
            indirectDraw_[index].vertexOffset = dc.vertexOffset + lod.meshMaterial->submeshes[subMesh].vertexOffset;
            indirectDraw_[index].firstInstance = 0;
            ++index;
        }
    }
}

} // namespace ugine::gfx
