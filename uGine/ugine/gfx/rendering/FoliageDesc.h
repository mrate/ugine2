﻿#pragma once

#include <ugine/gfx/rendering/MeshMaterial.h>
#include <ugine/utils/Singleton.h>
#include <ugine/utils/Storage.h>

#include <vector>

namespace ugine::gfx {

struct FoliageDesc {
    struct Lod {
        gfx::MeshMaterialRef meshMaterial;
        float maxDistance{};
    };

    std::string name;
    std::vector<Lod> lodLevels;
    float minDistance{ 1.0f };

    operator bool() const {
        return !lodLevels.empty();
    }
};

// TODO: SafeDelete
using Foliages = utils::SafeDeleteStorage<FoliageDesc>;
using FoliageRef = Foliages::Ref;
using FoliageID = Foliages::Id;

#define FOLIAGE_ADD(...) STORAGE_ADD(Foliages, __VA_ARGS__)

class FoliageService : public utils::Singleton<FoliageService> {
public:
    static void Init() {}
    static void Destroy() {
        Instance().foliages_.clear();
    }

    static FoliageRef Add(const std::string_view& name) {
        return Instance().AddImpl(name);
    }

    static void Remove(const FoliageRef& foliage) {
        Instance().RemoveImpl(foliage);
    }

    static const std::vector<FoliageRef>& Foliages() {
        return Instance().foliages_;
    }

    static FoliageRef LoadFromFile(const std::filesystem::path& path);

private:
    FoliageRef AddImpl(const std::string_view& name) {
        auto foliage{ FOLIAGE_ADD() };
        foliage->name = name;
        foliages_.push_back(foliage);
        return foliage;
    }

    void RemoveImpl(const FoliageRef& foliage) {
        foliages_.erase(std::remove(foliages_.begin(), foliages_.end(), foliage), foliages_.end());
    }

    std::vector<FoliageRef> foliages_;
};

} // namespace ugine::gfx
