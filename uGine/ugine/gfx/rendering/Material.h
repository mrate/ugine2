﻿#pragma once

#include "MaterialPass.h"

#include <ugine/gfx/Color.h>
#include <ugine/utils/Strings.h>

#include <glm/glm.hpp>

#include <set>
#include <vector>

namespace ugine::gfx {

class MaterialInstance;

class Material {
public:
    UGINE_MOVABLE_ONLY(Material);

    Material() = default;
    Material(uint32_t id, const std::filesystem::path& materialFile);
    ~Material();

    operator bool() const {
        return !passes_.empty();
    }

    bool HasPass(RenderPassType type) const;
    const MaterialPass& GetPass(RenderPassType type) const;
    const MaterialPass& DefaultPass() const;

    const std::string& Name() const {
        return name_;
    }

    const std::string& Category() const {
        return category_;
    }

    uint32_t Id() const {
        return id_;
    }

    const std::vector<MaterialPass>& Passes() const {
        return passes_;
    }

    void FillDefaults(MaterialInstance& instance) const;

    bool Transparent() const {
        return transparent_;
    }

    const std::filesystem::path& Path() const {
        return path_;
    }

    const std::set<std::filesystem::path>& Shaders() const {
        return shaders_;
    }

private:
    uint32_t id_{};
    std::filesystem::path path_{};
    std::set<std::filesystem::path> shaders_;

    std::string name_;
    std::string category_;
    RenderPassType defaultPass_{};
    std::vector<MaterialPass> passes_;
    MaterialParamValues defaults_;
    bool transparent_{};
};

using Materials = utils::SafeDeleteStorage<Material>;
using MaterialRef = Materials::Ref;
using MaterialID = Materials::Id;

#define MATERIAL_ADD(...) STORAGE_ADD(Materials, __VA_ARGS__)

} // namespace ugine::gfx
