﻿#pragma once

#include "FoliageDesc.h"
#include "Mesh.h"

#include <ugine/gfx/core/ComputePipeline.h>
#include <ugine/gfx/rendering/MaterialInstance.h>

#include <vector.>

namespace ugine::gfx {

class Image;

class Foliage {
public:
    UGINE_MOVABLE_ONLY(Foliage);

    static const uint32_t MAX_LOD{ 4 };
    static const uint32_t MAX_INSTANCES{ 4096 };

    static const uint32_t CULL_DATASET{ 3 };

    static const uint32_t POSITION_BINDING{ 0 };
    static const uint32_t DRAW_INSTANCE_BINDING0{ 1 };
    static const uint32_t DRAW_INSTANCE_BINDING1{ 2 };
    static const uint32_t DRAW_INSTANCE_BINDING2{ 3 };
    static const uint32_t DRAW_INSTANCE_BINDING3{ 4 };
    static const uint32_t PARAM_BINDING{ 5 };
    static const uint32_t INDIRECT_BINDING{ 6 };

    static const uint32_t CULLLOD_THREAD_COUNT{ 256 };

    Foliage() = default;

    explicit Foliage(const ComputePipeline* compute);
    virtual ~Foliage();

    void Update(const RenderContext& context, const glm::mat4& world);
    void Render(const RenderContext& context) const;

    operator bool() const {
        return foliage_ && !foliage_->lodLevels.empty() && count_ > 0;
    }

    uint32_t Count() const {
        return count_;
    }
    void SetCount(uint32_t count);

    void Place(const glm::vec3& mmin, const glm::vec3& mmax, const Image* heightMap = nullptr);

    void SetFoliageDesc(const FoliageRef& foliage);
    FoliageRef FoliageDesc() const {
        return foliage_;
    }

    const math::AABB& Aabb() const {
        return aabb_;
    }

protected:
    struct Position {
        glm::vec4 positionYaw{};
    };

    struct CullParamsCB {
        glm::mat4 world;
        glm::vec3 aabbMin;
        uint32_t count;
        glm::vec3 aabbMax;
        uint32_t lodLevels;
        glm::vec4 frustumPlanes[6];
        glm::vec4 lodDistances;
        glm::ivec4 subMeshes;
    };

    const ComputePipeline* cullLodPipeline_{};

    BufferRef positionBuffer_;
    BufferRef drawInstancesBuffer_;
    BufferRef drawIndirectBuffer_;

    FoliageRef foliage_;

    uint32_t count_{};
    uint32_t bufferSize_{};

    uint32_t lodSize_{};
    std::vector<IndirectIndexedDrawCB> indirectDraw_;

    math::AABB aabb_;
};

struct FoliageArray {
    std::vector<Foliage> layers;
};

} // namespace ugine::gfx
