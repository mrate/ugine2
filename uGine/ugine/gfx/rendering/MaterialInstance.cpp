﻿#include "MaterialInstance.h"
#include "MaterialHelpers.h"

#include <ugine/gfx/data/ShaderBindings.h>
#include <ugine/gfx/tools/Initializers.h>
#include <ugine/utils/Align.h>
#include <ugine/utils/Assert.h>

namespace ugine::gfx {

// Instance.
MaterialPassInstance::MaterialPassInstance(const MaterialRef& material, RenderPassType pass)
    : material_{ material }
    , pass_{ pass } {

    const auto& materialPass{ Material() };

    uint32_t lastSet{ 0 };
    for (const auto& binding : materialPass.Bindings()) {
        if (binding.set > lastSet + 1) {
            UGINE_THROW(GfxError, "Material sets are not sorted or zero based.");
        }

        lastSet = binding.set;

        if (binding.set >= datasets_.size()) {
            datasets_.resize(binding.set + 1);
        }

        auto& set{ datasets_[binding.set] };

        if (binding.set != MATERIAL_DATASET) {
            continue;
        }

        switch (binding.type) {
        case vk::DescriptorType::eUniformBuffer:
            AddUniformBuffer(set, binding.binding, binding.size);
            break;
        case vk::DescriptorType::eCombinedImageSampler:
            AddTexture(set, binding.binding, binding.imgType);
            break;
        case vk::DescriptorType::eStorageBuffer:
            AddStorageBuffer(set, binding.binding);
            break;
        default:
            UGINE_ASSERT(false && "Unsupported material binding type.");
        }
    }
}

MaterialPassInstance::~MaterialPassInstance() {}

void MaterialPassInstance::UpdateUniform(uint32_t binding, uint32_t offset, const void* data, size_t size) {
    UGINE_ASSERT(offset + size <= datasets_[MATERIAL_DATASET].bindings.at(binding).gpuBuffer->Size());

    memcpy(datasets_[MATERIAL_DATASET].bindings.at(binding).gpuBuffer->Mapped(offset), data, size);
}

const void* MaterialPassInstance::ReadUniform(uint32_t binding) const {
    UGINE_ASSERT(MATERIAL_DATASET < datasets_.size());
    return datasets_[MATERIAL_DATASET].bindings.at(binding).gpuBuffer->Mapped();
}

void MaterialPassInstance::UpdateTexture(uint32_t binding, const TextureViewRef& texture) {
    UGINE_ASSERT(MATERIAL_DATASET < datasets_.size());
    datasets_[MATERIAL_DATASET].bindings.at(binding).gpuTexture = texture;
}

const TextureViewRef& MaterialPassInstance::ReadTexture(uint32_t binding) const {
    UGINE_ASSERT(MATERIAL_DATASET < datasets_.size());
    return datasets_[MATERIAL_DATASET].bindings.at(binding).gpuTexture;
}

void MaterialPassInstance::UpdateStorageBuffer(uint32_t binding, const BufferRef& storage) {
    UGINE_ASSERT(MATERIAL_DATASET < datasets_.size());
    datasets_[MATERIAL_DATASET].bindings.at(binding).gpuBuffer = storage;
}

const BufferRef& MaterialPassInstance::ReadStorageBuffer(uint32_t binding) const {
    UGINE_ASSERT(MATERIAL_DATASET < datasets_.size());
    return datasets_[MATERIAL_DATASET].bindings.at(binding).gpuBuffer;
}

void MaterialPassInstance::Bind(GPUCommandList command) const {
    UGINE_ASSERT(GraphicsService::Device().PipelineLayout(command));

    if (datasets_.empty()) {
        return;
    }

    std::vector<vk::DescriptorSet> descriptors;

    for (uint32_t dataset = 0, dsSize = static_cast<uint32_t>(datasets_.size()); dataset < dsSize; ++dataset) {
        for (const auto& [b, value] : datasets_[dataset].bindings) {
            switch (value.type) {
            case vk::DescriptorType::eUniformBuffer:
                if (value.gpuBuffer) {
                    GraphicsService::Device().BindUniform(command, dataset, b, *value.gpuBuffer);
                } else {
                    // TODO:
                    //GraphicsService::Device().BindStorage(command, dataset, b, *GraphicsService::NullBuffer());
                    UGINE_ASSERT(false);
                }
                break;
            case vk::DescriptorType::eStorageBuffer:
                if (value.gpuBuffer) {
                    GraphicsService::Device().BindStorage(command, dataset, b, *value.gpuBuffer);
                } else {
                    // TODO:
                    //GraphicsService::Device().BindStorage(command, dataset, b, *GraphicsService::NullBuffer());
                    UGINE_ASSERT(false);
                }
                break;
            case vk::DescriptorType::eCombinedImageSampler:
                if (value.gpuTexture) {
                    GraphicsService::Device().BindImageSampler(command, dataset, b, *value.gpuTexture);
                } else {
                    switch (value.imgType) {
                    case vk::ImageViewType::e2D:
                        GraphicsService::Device().BindImageSampler(command, dataset, b, *GraphicsService::NullTextureBlack());
                        break;
                    case vk::ImageViewType::e2DArray:
                        GraphicsService::Device().BindImageSampler(command, dataset, b, *GraphicsService::NullTextureArray());
                        break;
                    case vk::ImageViewType::eCube:
                        GraphicsService::Device().BindImageSampler(command, dataset, b, *GraphicsService::NullTextureCube());
                        break;
                    }
                }
                break;
            }
        }
    }
}

void MaterialPassInstance::AddUniformBuffer(Dataset& dataset, uint32_t binding, uint32_t size) {
    dataset.bindings[binding].type = vk::DescriptorType::eUniformBuffer;

    auto buffSize{ utils::AlignTo(size * GraphicsService::ActiveFrameInFlight(), GraphicsService::Graphics().UniformBufferOffsetAlignment()) };
    dataset.bindings[binding].gpuBuffer = BUFFER_ADD(size, vk::BufferUsageFlagBits::eUniformBuffer | vk::BufferUsageFlagBits::eTransferDst,
        vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
}

void MaterialPassInstance::AddStorageBuffer(Dataset& dataset, uint32_t binding) {
    dataset.bindings[binding].type = vk::DescriptorType::eStorageBuffer;
}

void MaterialPassInstance::AddTexture(Dataset& dataset, uint32_t binding, vk::ImageViewType type) {
    dataset.bindings[binding].type = vk::DescriptorType::eCombinedImageSampler;
    dataset.bindings[binding].imgType = type;
}

MaterialInstance::MaterialInstance(const gfx::MaterialRef& material, const std::string_view& name)
    : material_{ material }
    , name_{ name } {
    Initialize();
}

MaterialInstance::MaterialInstance(gfx::MaterialRef&& material, const std::string_view& name)
    : material_{ std::move(material) }
    , name_{ name } {
    Initialize();
}

void MaterialInstance::Initialize() {
    if (name_.empty()) {
        name_ = material_->Name() + std::string(" instance");
    }

    for (const auto& p : material_->Passes()) {
        passes_.push_back(MaterialPassInstance{ material_, p.Type() });
    }

    material_->FillDefaults(*this);
}

MaterialInstance::~MaterialInstance() {}

void MaterialInstance::Bind(RenderPassType type, GPUCommandList command) const {
    for (auto& p : passes_) {
        if (p.RenderPass() & type) {
            p.Bind(command);
        }
    }
}

bool MaterialInstance::HasPass(RenderPassType type) const {
    for (const auto& p : passes_) {
        if (p.RenderPass() & type) {
            return true;
        }
    }
    return false;
}

const MaterialPassInstance& MaterialInstance::Pass(RenderPassType type) const {
    for (const auto& p : passes_) {
        if (p.RenderPass() & type) {
            return p;
        }
    }
    UGINE_THROW(GfxError, "Missing pass type.");
}

const MaterialPassInstance& MaterialInstance::Default() const {
    const auto defaultType{ material_->DefaultPass().Type() };
    for (const auto& p : passes_) {
        if (p.RenderPass() & defaultType) {
            return p;
        }
    }
    UGINE_THROW(GfxError, "Missing pass type.");
}

MaterialPassInstance& MaterialInstance::Pass(RenderPassType type) {
    for (auto& p : passes_) {
        if (p.RenderPass() & type) {
            return p;
        }
    }
    UGINE_THROW(GfxError, "Missing pass type.");
}

MaterialPassInstance& MaterialInstance::Default() {
    const auto defaultType{ material_->DefaultPass().Type() };
    for (auto& p : passes_) {
        if (p.RenderPass() & defaultType) {
            return p;
        }
    }
    UGINE_THROW(GfxError, "Missing pass type.");
}

RenderPassType MaterialInstance::Passes() const {
    RenderPassType res{ RenderPassType::None };
    for (const auto& p : passes_) {
        res |= p.RenderPass();
    }
    return res;
}

} // namespace ugine::gfx
