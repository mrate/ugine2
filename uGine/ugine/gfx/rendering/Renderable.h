﻿#pragma once

#include <ugine/gfx/DrawCall.h>
#include <ugine/gfx/Gfx.h>

#include <ugine/core/Camera.h>

namespace ugine::gfx {

class Renderable {
public:
    virtual ~Renderable() {}

    virtual void Render(const RenderContext& context, int lodLevel, DrawCallList& drawCalls) const = 0;
};

struct LightShadingData;

} // namespace ugine::gfx
