﻿#include "FoliageDesc.h"
#include "Foliage.h"
#include "MeshMaterial.h"

#include <ugine/core/Scheduler.h>
#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/import/MeshData.h>
#include <ugine/gfx/import/MeshLoader.h>
#include <ugine/utils/Log.h>
#include <ugine/utils/Profile.h>

#include <nlohmann/json.hpp>

#include <fstream>

namespace ugine::gfx {

FoliageRef FoliageService::LoadFromFile(const std::filesystem::path& path) {
    utils::ScopeTimer timer{ fmt::format("Loading Foliage '{}'...", path.string()) };

    try {
        using namespace nlohmann;

        std::ifstream in(path);
        if (!in.good()) {
            throw std::exception("File not found");
        }

        json j;
        in >> j;

        FoliageDesc foliage{};
        foliage.name = j.value<std::string>("name", path.filename().string());
        foliage.minDistance = j.value<float>("minDistance", 1.0f);

        struct ImportLod {
            MeshData mesh;
            MaterialRef material;
            float distance;
        };

        std::vector<ImportLod> lods(Foliage::MAX_LOD);

        //core::Scheduler::Group grp;
        int i{};
        for (i = 0; i < Foliage::MAX_LOD; ++i) {
            std::string name = std::string{ "lod" } + std::to_string(i);

            if (j.contains(name)) {
                auto lod{ j[name] };

                lods[i].distance = lod.value<float>("maxDistance", 0.0f);
                std::filesystem::path meshFile{ lod.value<std::string>("mesh", "") };

                if (lod.contains("material")) {
                    auto materialName{ lod["material"] };
                    lods[i].material = MaterialService::FindMaterial(materialName);

                    if (!lods[i].material) {
                        UGINE_ERROR("Failed to load foliage material '{}'", materialName);
                    }
                }

                //core::Scheduler::Schedule(grp, [&lods, path, meshFile, i]() {
                MeshLoader::Settings settings{};

                MeshLoader loader{ path.parent_path() / meshFile, settings };
                lods[i].mesh = loader.LoadMesh();
                //});
            } else {
                break;
            }
        }

        //core::Scheduler::Wait(grp);
        lods.resize(i);

        for (const auto& lod : lods) {
            auto meshMaterial{ ImportMeshMaterial(lod.mesh, lod.material) };
            foliage.lodLevels.push_back({ meshMaterial, lod.distance });
        }

        auto newFoliage{ FOLIAGE_ADD(foliage) };
        Instance().foliages_.push_back(newFoliage);
        return newFoliage;

    } catch (const std::exception& ex) {
        UGINE_ERROR("Failed to load foliage: {}", ex.what());
    }

    return {};
}

} // namespace ugine::gfx
