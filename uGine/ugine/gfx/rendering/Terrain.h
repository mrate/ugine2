﻿#pragma once

#include <ugine/core/Camera.h>
#include <ugine/core/Transformation.h>
#include <ugine/gfx/core/Image.h>
#include <ugine/gfx/core/VertexBuffer.h>
#include <ugine/gfx/rendering/Material.h>
#include <ugine/gfx/rendering/MaterialInstance.h>
#include <ugine/gfx/rendering/MeshMaterial.h>

namespace ugine::gfx {

class Terrain {
public:
    UGINE_MOVABLE_ONLY(Terrain);

    Terrain();
    ~Terrain();

    void SetHeightMap(const std::filesystem::path& heightMap);
    TextureViewRef HeightMap() const;

    MeshMaterialID MeshMaterialID() const;

    void SetLayerTextures(const TextureViewRef& layers);
    void SetSplatTexture(const TextureViewRef& splat);
    void SetScale(const glm::vec2& scale);
    void SetHeightScale(float heightScale);

    MeshMaterial& MeshMaterial() {
        return *meshMaterial_;
    }

    MaterialInstance& Material() {
        return *meshMaterial_->submeshes[0].material;
    }

    const Image& HeightMapImage() const {
        return heightMap_;
    }

    void Render(const RenderContext& context, int lodLevel) const;

    const math::AABB& Aabb() const {
        return meshMaterial_->mesh->Aabb();
    }

private:
    static const auto GRID_WIDTH{ 64 };
    static const auto GRID_HEIGHT{ 64 };

    MeshMaterialRef meshMaterial_;

    Image heightMap_;
    TextureViewRef heightMapTexture_;
};

} // namespace ugine::gfx
