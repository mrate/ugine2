﻿#pragma once

#include <ugine/gfx/core/Buffer.h>
#include <ugine/gfx/core/ComputePipeline.h>
#include <ugine/gfx/core/VertexBuffer.h>
#include <ugine/gfx/import/MeshData.h>
#include <ugine/gfx/rendering/Mesh.h>
#include <ugine/gfx/rendering/MeshMaterial.h>
#include <ugine/math/Aabb.h>

namespace ugine::gfx {

class SkinnedMesh {
public:
    UGINE_MOVABLE_ONLY(SkinnedMesh);

    SkinnedMesh() = default;
    explicit SkinnedMesh(MeshData&& data);

    void Init(DescriptorSetPool& pool);
    void Destroy(DescriptorSetPool& pool);
    bool Initialized() const {
        return !descriptors_.empty();
    }

    const std::string& Name() const {
        return name_;
    }

    void SetName(const std::string& name) {
        name_ = name;
    }

    // TODO: MeshMaterial.
    MeshMaterialID MeshMaterialID() const;

    uint32_t MeshCount() const {
        return static_cast<uint32_t>(mesh_.meshes.size());
    }

    const MeshMaterial& MeshMaterial() const {
        return *(meshMaterials_[frame_]);
    }

    int LodLevels() const;

    const math::AABB& Aabb() const {
        return aabb_;
    }

    // TODO: Armature.
    uint32_t BoneCount() const {
        return static_cast<uint32_t>(mesh_.bones.size());
    }

    uint32_t BoneIndex(const std::string& name) const;

    // TODO: Animation.
    void UpdateMatrices(const AnimationData& animation, float time);

    const MeshData& Mesh() const {
        return mesh_;
    }

    operator bool() const {
        return loaded_;
    }

    // TODO: Animator.
    void AcquireCompute(GPUCommandList command);
    void ReleaseCompute(GPUCommandList command);
    void AcquireRender(GPUCommandList command);
    void ReleaseRender(GPUCommandList command);

    vk::DescriptorSet Descriptor() const;
    uint32_t VertexCount() const;

private:
    void CreateVertexBuffer(const std::vector<IndexType>& indices);
    uint32_t VertexBufferOffset() const;
    uint32_t VertexBufferSize() const;

    glm::mat4 InterpolateChannel(float time, const AnimationData::Channel& channel) const;

    bool loaded_{};

    std::string name_;
    MeshData mesh_;

    // Const buffers.
    struct AnimationCB {
        uint32_t vertexCount;
    };

    Buffer animationCB_;
    Buffer sourceVertexSkinBuffer_;

    // Volatile buffers.
    Buffer matrixBuffer_;

    size_t matrixBufferAlignedSize_;
    size_t singleVertexBufferSize_;

    std::vector<uint32_t> vertexBufferOffset_;
    std::vector<uint32_t> vertexBufferStart_;
    uint32_t indexStart_{};

    uint32_t frame_{};

    math::AABB aabb_{};

    bool renderAcquired_{};
    bool computeAcquired_{};

    std::vector<vk::DescriptorSet> descriptors_;

    std::vector<gfx::MeshMaterialRef> meshMaterials_;
    //std::vector<std::vector<MeshRef>> meshes_;
};

} // namespace ugine::gfx
