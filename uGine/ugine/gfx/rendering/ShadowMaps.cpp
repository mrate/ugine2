﻿#include "ShadowMaps.h"

#include <ugine/core/ConfigService.h>
#include <ugine/gfx/GraphicsService.h>

namespace ugine::gfx {

ShadowMaps::ShadowMaps(vk::RenderPass renderPass) {
    CreateSpotShadowMaps(renderPass);
    CreateDirShadowMaps(renderPass);
    CreatePointShadowMaps(renderPass);

    // Make all shadow maps sample ready.
    auto command{ GraphicsService::Device().BeginCommandList() };

    for (auto& shadowMap : spotShadowMaps_) {
        for (auto& view : shadowMap) {
            auto texture{ TextureViews::TextureRef(view.sampleView) };
            texture->Transition(command, vk::ImageLayout::eUndefined, vk::ImageLayout::eDepthStencilReadOnlyOptimal, vk::ImageAspectFlagBits::eDepth);
        }
    }

    for (auto& shadowMap : dirShadowMaps_) {
        for (auto& view : shadowMap) {
            vk::ImageMemoryBarrier barrier;
            barrier.pNext = nullptr;
            barrier.srcAccessMask = vk::AccessFlagBits::eShaderRead;
            barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;
            barrier.oldLayout = vk::ImageLayout::eUndefined;
            barrier.newLayout = vk::ImageLayout::eDepthStencilReadOnlyOptimal;
            barrier.image = TextureViews::TextureRef(view.sampleView)->Image();
            barrier.subresourceRange = vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eDepth, 0, 1, 0, limits::CSM_LEVELS);

            GraphicsService::Device().CommandBuffer(command).pipelineBarrier(
                vk::PipelineStageFlagBits::eFragmentShader, vk::PipelineStageFlagBits::eFragmentShader, {}, 0, nullptr, 0, nullptr, 1, &barrier);
        }
    }

    for (auto& shadowMap : pointShadowMaps_) {
        for (auto& image : shadowMap) {
            // TODO:
            //image.Transition(*command, vk::ImageLayout::eShaderReadOnlyOptimal);

            vk::ImageMemoryBarrier barrier;
            barrier.pNext = nullptr;
            barrier.srcAccessMask = vk::AccessFlagBits::eShaderRead;
            barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;
            barrier.oldLayout = vk::ImageLayout::eUndefined;
            barrier.newLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
            barrier.image = TextureViews::TextureRef(image.sampleView)->Image();
            barrier.subresourceRange = vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 6);

            GraphicsService::Device().CommandBuffer(command).pipelineBarrier(
                vk::PipelineStageFlagBits::eFragmentShader, vk::PipelineStageFlagBits::eFragmentShader, {}, 0, nullptr, 0, nullptr, 1, &barrier);
        }
    }

    GraphicsService::Device().SubmitCommandLists();
    GraphicsService::Device().WaitGpu();
}

const Texture& ShadowMaps::DirShadowMapTexture(uint32_t lightIndex) const {
    return DirShadowMapTexture(GraphicsService::ActiveFrameInFlight(), lightIndex);
}

const TextureView& ShadowMaps::DirShadowMap(uint32_t lightIndex) const {
    return DirShadowMap(GraphicsService::ActiveFrameInFlight(), lightIndex);
}

const TextureView& ShadowMaps::DirShadowMapCsm(uint32_t lightIndex, uint32_t csmLevel) const {
    return DirShadowMapCsm(GraphicsService::ActiveFrameInFlight(), lightIndex, csmLevel);
}

const Texture& ShadowMaps::DirShadowMapTexture(uint32_t frameIndex, uint32_t lightIndex) const {
    return *TextureViews::TextureRef(dirShadowMaps_[frameIndex][lightIndex].sampleView);
}

const TextureView& ShadowMaps::DirShadowMap(uint32_t frameIndex, uint32_t lightIndex) const {
    return *dirShadowMaps_[frameIndex][lightIndex].sampleView;
}

const TextureView& ShadowMaps::DirShadowMapCsm(uint32_t frameIndex, uint32_t lightIndex, uint32_t csmLevel) const {
    return *dirShadowMaps_[frameIndex][lightIndex].csmLevel[csmLevel];
}

void ShadowMaps::CreateSpotShadowMaps(vk::RenderPass renderPass) {
    auto& gfx{ GraphicsService::Graphics() };

    const vk::Extent3D size{ core::ConfigService::ShadowMapResolution(), 1 };

    for (uint32_t i = 0; i < GraphicsService::FramesInFlight(); ++i) {
        std::vector<SpotShadowMaps> maps;
        for (uint32_t j = 0; j < limits::MAX_SPOT_SHADOWS; ++j) {
            auto texture{ TEXTURE_ADD(size, gfx.DepthFormat(), vk::ImageTiling::eOptimal,
                vk::ImageUsageFlagBits::eDepthStencilAttachment | vk::ImageUsageFlagBits::eSampled, vk::SampleCountFlagBits::e1) };
            DEBUG_ATTACH(texture, "SpotLight shadow map {}", j);

            maps.push_back({ TEXTURE_VIEW_ADD(
                texture, TextureView::CreateSampled(*texture, GraphicsService::DepthSampler(), vk::ImageViewType::e2D, vk::ImageAspectFlagBits::eDepth)) });
        }
        spotShadowMaps_.push_back(maps);
    }
}

void ShadowMaps::CreateDirShadowMaps(vk::RenderPass renderPass) {
    auto shadowMapExtent{ core::ConfigService::ShadowMapResolution() };

    for (uint32_t i = 0; i < GraphicsService::FramesInFlight(); ++i) {
        std::vector<DirShadowMaps> maps;

        for (uint32_t j = 0; j < limits::MAX_DIR_SHADOWS; ++j) {
            DirShadowMaps dir;

            vk::ImageCreateInfo imageCI{};
            imageCI.imageType = vk::ImageType::e2D;
            imageCI.extent = vk::Extent3D(shadowMapExtent.width, shadowMapExtent.height, 1);
            imageCI.mipLevels = 1;
            imageCI.arrayLayers = limits::CSM_LEVELS;
            imageCI.format = GraphicsService::Graphics().DepthFormat();
            imageCI.tiling = vk::ImageTiling::eOptimal;
            imageCI.initialLayout = vk::ImageLayout::eUndefined;
            imageCI.usage = vk::ImageUsageFlagBits::eDepthStencilAttachment | vk::ImageUsageFlagBits::eSampled;
            imageCI.samples = vk::SampleCountFlagBits::e1;
            imageCI.sharingMode = vk::SharingMode::eExclusive;

            auto texture{ TEXTURE_ADD(imageCI) };
            DEBUG_ATTACH(texture, "DirLight shadowmap {}", j);

            dir.sampleView = TEXTURE_VIEW_ADD(texture,
                TextureView::CreateSampled(
                    *texture, GraphicsService::DepthSampler(), vk::ImageViewType::e2DArray, vk::ImageAspectFlagBits::eDepth, 0, limits::CSM_LEVELS));

            for (uint32_t c = 0; c < limits::CSM_LEVELS; ++c) {
                dir.csmLevel[c] = TEXTURE_VIEW_ADD(texture,
                    TextureView::CreateSampled(*texture, GraphicsService::DepthSampler(), vk::ImageViewType::e2D, vk::ImageAspectFlagBits::eDepth, c, 1));
                DEBUG_ATTACH(dir.csmLevel[c], "DirLight shadowmap {} CSM={}", j, c);
            }

            maps.push_back(dir);
        }

        dirShadowMaps_.push_back(maps);
    }
}

void ShadowMaps::CreatePointShadowMaps(vk::RenderPass renderPass) {
    auto& gfx{ GraphicsService::Graphics() };

    auto size{ vk::Extent3D(core::ConfigService::ShadowMapCubeResolution(), 1) };
    for (uint32_t i = 0; i < GraphicsService::FramesInFlight(); ++i) {
        std::vector<PointShadowMaps> maps;

        for (uint32_t j = 0; j < limits::MAX_POINT_SHADOWS; ++j) {
            PointShadowMaps map;

            vk::ImageCreateInfo imageCI{};
            imageCI.imageType = vk::ImageType::e2D;
            imageCI.extent = size;
            imageCI.mipLevels = 1;
            imageCI.arrayLayers = 6;
            imageCI.format = config::PointShadowMapFormat;
            imageCI.tiling = vk::ImageTiling::eOptimal;
            imageCI.initialLayout = vk::ImageLayout::eUndefined;
            imageCI.usage = vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eSampled;
            imageCI.samples = vk::SampleCountFlagBits::e1;
            imageCI.sharingMode = vk::SharingMode::eExclusive;
            imageCI.flags = vk::ImageCreateFlagBits::eCubeCompatible;

            auto texture{ TEXTURE_ADD(imageCI) };
            DEBUG_ATTACH(texture, "Point light shadow map {}", j);

            map.sampleView = TEXTURE_VIEW_ADD(texture,
                TextureView::CreateSampled(*texture, GraphicsService::DepthSampler(), vk::ImageViewType::eCube, vk::ImageAspectFlagBits::eColor, 0, 6));

            auto depthTexture{ TEXTURE_ADD(
                size, gfx.DepthFormat(), vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eDepthStencilAttachment | vk::ImageUsageFlagBits::eSampled) };
            DEBUG_ATTACH(depthTexture, "Point light shadow map {} depth buffer", j);

            map.depthView = TEXTURE_VIEW_ADD(depthTexture, TextureView::Create(*depthTexture, vk::ImageAspectFlagBits::eDepth));
            for (int k = 0; k < 6; ++k) {
                map.faceView[k] = TEXTURE_VIEW_ADD(texture, TextureView::Create(*texture, vk::ImageViewType::e2D, vk::ImageAspectFlagBits::eColor, k, 1));
                DEBUG_ATTACH(map.faceView[k], "Point light shadow map {} face {}", j, k);
            }

            maps.push_back(map);
        }

        pointShadowMaps_.push_back(maps);
    }
}

const Texture& ShadowMaps::SpotShadowMapTexture(uint32_t lightIndex) const {
    return SpotShadowMapTexture(GraphicsService::ActiveFrameInFlight(), lightIndex);
}

const TextureView& ShadowMaps::SpotShadowMap(uint32_t lightIndex) const {
    return SpotShadowMap(GraphicsService::ActiveFrameInFlight(), lightIndex);
}

const Texture& ShadowMaps::SpotShadowMapTexture(uint32_t frameIndex, uint32_t lightIndex) const {
    return *TextureViews::TextureRef(spotShadowMaps_[frameIndex][lightIndex].sampleView);
}

const TextureView& ShadowMaps::SpotShadowMap(uint32_t frameIndex, uint32_t lightIndex) const {
    return *spotShadowMaps_[frameIndex][lightIndex].sampleView;
}

const Texture& ShadowMaps::PointShadowMapTexture(uint32_t lightIndex) const {
    return PointShadowMapTexture(GraphicsService::ActiveFrameInFlight(), lightIndex);
}

const TextureView& ShadowMaps::PointShadowMap(uint32_t lightIndex) const {
    return PointShadowMap(GraphicsService::ActiveFrameInFlight(), lightIndex);
}

const TextureView& ShadowMaps::PointShadowMapDepth(uint32_t lightIndex) const {
    return PointShadowMapDepth(GraphicsService::ActiveFrameInFlight(), lightIndex);
}

const TextureView& ShadowMaps::PointShadowMapFace(uint32_t lightIndex, uint32_t face) const {
    return PointShadowMapFace(GraphicsService::ActiveFrameInFlight(), lightIndex, face);
}

const Texture& ShadowMaps::PointShadowMapTexture(uint32_t frameIndex, uint32_t lightIndex) const {
    return *TextureViews::TextureRef(pointShadowMaps_[frameIndex][lightIndex].sampleView);
}

const TextureView& ShadowMaps::PointShadowMap(uint32_t frameIndex, uint32_t lightIndex) const {
    return *pointShadowMaps_[frameIndex][lightIndex].sampleView;
}

const TextureView& ShadowMaps::PointShadowMapDepth(uint32_t frameIndex, uint32_t lightIndex) const {
    return *pointShadowMaps_[frameIndex][lightIndex].depthView;
}

const TextureView& ShadowMaps::PointShadowMapFace(uint32_t frameIndex, uint32_t lightIndex, uint32_t face) const {
    return *pointShadowMaps_[frameIndex][lightIndex].faceView[face];
}

vk::Sampler ShadowMaps::Sampler() const {
    return *(*GraphicsService::DepthSampler());
}

} // namespace ugine::gfx
