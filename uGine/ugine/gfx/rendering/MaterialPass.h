﻿#pragma once

#include "MaterialTypes.h"

#include <ugine/gfx/Error.h>
#include <ugine/gfx/Fwd.h>

#include <ugine/gfx/ShaderCache.h>
#include <ugine/gfx/core/Buffer.h>
#include <ugine/gfx/core/DescriptorSetPool.h>
#include <ugine/gfx/core/Device.h>
#include <ugine/gfx/core/Pipeline.h>
#include <ugine/gfx/pass/RenderingPass.h>
#include <ugine/utils/Storage.h>
#include <ugine/utils/Strings.h>

#include <glm/glm.hpp>

#include <entt/entt.hpp>

#include <array>
#include <filesystem>
#include <map>
#include <set>
#include <string>
#include <vector>

namespace ugine::gfx {

class ShaderProgram;

class MaterialPass {
public:
    UGINE_MOVABLE_ONLY(MaterialPass);

    MaterialPass(RenderPassType type, const PipelineDesc& pipelineDesc, const std::map<vk::ShaderStageFlagBits, std::filesystem::path>& shaders,
        const std::string_view& name);
    ~MaterialPass();

    const std::vector<vk::DescriptorSetLayout>& Layouts() const {
        return descriptorSetLayout_;
    }

    std::vector<ShaderBinding> Bindings() const {
        return bindings_;
    }

    std::vector<MaterialParam> Params() const {
        using namespace ugine::gfx;

        std::vector<MaterialParam> params;

        uint32_t index{};
        for (const auto& p : params_) {
            params.push_back({ index++, p.name, utils::CamelCaseToHuman(p.name), p.type });
        }

        return params;
    }

    const std::vector<MaterialParamDescriptor>& ParamDescriptors() const {
        return params_;
    }

    Pipeline CreatePipeline(vk::RenderPass renderpass, vk::SampleCountFlagBits samples = vk::SampleCountFlagBits::e1) const;

    uint32_t FindParamId(const std::string_view& name, bool* found) const;
    std::optional<uint32_t> FindParamId(const std::string_view& name) const;

    RenderPassType Type() const {
        return type_;
    }

protected:
    void ParseParams(const Shader& shader);

    RenderPassType type_;
    PipelineDesc pipelineDesc_;

    std::vector<GPUPushConstant> pushConstants_;
    std::vector<vk::UniqueDescriptorSetLayout> descriptorSetLayoutHolder_;
    std::vector<vk::DescriptorSetLayout> descriptorSetLayout_;
    std::vector<ShaderBinding> bindings_;
    uint32_t vertexInputCount_{};
    std::vector<MaterialParamDescriptor> params_;

    std::vector<vk::PipelineShaderStageCreateInfo> shaderStages_;

    std::string name_;
};

} // namespace ugine::gfx
