﻿#pragma once

#include <ugine/gfx/DrawCall.h>
#include <ugine/gfx/Storages.h>
#include <ugine/gfx/core/Vertex.h>
#include <ugine/gfx/core/VertexBuffer.h>
#include <ugine/math/Aabb.h>
#include <ugine/utils/Storage.h>
#include <ugine/utils/Utils.h>

namespace ugine::gfx {

class Mesh {
public:
    Mesh() = default;
    Mesh(const Mesh& mesh) = default;
    ~Mesh();

    Mesh(int lodLevels, const std::vector<Vertex>& vertices, const std::vector<IndexType>& indices = {}, bool isMutable = false,
        const std::string_view& name = "");

    Mesh(int lodLevels, const math::AABB& aabb, uint32_t indexStart, uint32_t vertexStart, uint32_t indexCount, const std::string_view& name = "");

    const std::string& Name() const {
        return name_;
    }

    void SetName(const std::string& name) {
        name_ = name;
    }

    const math::AABB& Aabb() const {
        return aabb_;
    }

    operator bool() const {
        return !drawCalls_.empty();
    }

    const DrawCall& DrawCall(int lodLevel = 0) const {
        UGINE_ASSERT(!drawCalls_.empty() && !drawCalls_[frameIndex_].lod.empty());

        return drawCalls_[frameIndex_].lod[std::min<size_t>(lodLevel, drawCalls_[frameIndex_].lod.size() - 1)];
    }

    int LodLevels() const {
        return static_cast<int>(drawCalls_[0].lod.size());
    }

    VertexBufferRef VertexBuffer() {
        return vertexBuffer_;
    }

    const VertexBufferRef& VertexBuffer() const {
        return vertexBuffer_;
    }

    VertexBufferID VertexBufferId() const {
        return vertexBuffer_.id();
    }

    bool IsMutable() const {
        return mutable_;
    }

    void Update();
    void Update(int lodLevel, const std::vector<Vertex>& vertices);

private:
    struct FrameDrawCall {
        std::vector<gfx::DrawCall> lod;
    };

    void GenerateLods(int levels, const std::vector<Vertex>& vertices, const std::vector<IndexType>& indices);

    std::vector<FrameDrawCall> drawCalls_;
    VertexBufferRef vertexBuffer_;

    std::string name_;
    math::AABB aabb_;

    // Mutable.
    uint32_t frameIndex_{};
    bool mutable_{ false };
};

using Meshes = utils::SafeDeleteStorage<Mesh>;
using MeshRef = Meshes::Ref;
using MeshID = Meshes::Id;

#define MESH_ADD(...) STORAGE_ADD(Meshes, __VA_ARGS__)

} // namespace ugine::gfx
