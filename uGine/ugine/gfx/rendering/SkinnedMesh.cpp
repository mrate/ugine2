﻿#include "SkinnedMesh.h"

#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/core/VertexBuffer.h>
#include <ugine/gfx/import/MaterialImporter.h>
#include <ugine/gfx/tools/Helpers.h>
#include <ugine/gfx/tools/Initializers.h>
#include <ugine/math/Math.h>
#include <ugine/utils/Align.h>
#include <ugine/utils/Log.h>
#include <ugine/utils/Profile.h>

#include <stack>

namespace {

template <typename T> inline T Interpolate(float t, const std::vector<std::pair<float, T>>& values) {
    if (values.size() == 1) {
        return values[0].second;
    }

    std::pair<float, T> start{};
    std::pair<float, T> end{};

    float startTime{};
    float endTime{};

    for (uint32_t i = 0; i < values.size(); ++i) {
        if (i == values.size() - 1) {
            // At the end.
            start = values[i];
            end = values[0];
            break;
        } else if (t < values[i + 1].first) {
            start = values[i];
            end = values[i + 1];
            break;
        }
    }

    UGINE_ASSERT(end.first != start.first);
    float p{ (t - start.first) / (end.first - start.first) };
    return ugine::math::Interpolate(start.second, end.second, p);
}

} // namespace

namespace ugine::gfx {

SkinnedMesh::SkinnedMesh(MeshData&& data)
    : loaded_(true)
    , mesh_(std::move(data))
    , aabb_(CalcAABB(mesh_.vertices)) {
    if (!mesh_.indices.empty()) {
        CreateVertexBuffer(mesh_.indices);
    } else {
        std::vector<IndexType> defaultIndices(mesh_.vertices.size());
        for (IndexType i = 0; i < mesh_.vertices.size(); ++i) {
            defaultIndices[i] = i;
        }
        CreateVertexBuffer(defaultIndices);
    }
}

int SkinnedMesh::LodLevels() const {
    return mesh_.meshes.empty() ? 0 : 1;
}

uint32_t SkinnedMesh::BoneIndex(const std::string& name) const {
    auto it{ mesh_.boneNameToIndex.find(name) };
    return it != mesh_.boneNameToIndex.end() ? it->second : static_cast<uint32_t>(-1);
}

void SkinnedMesh::CreateVertexBuffer(const std::vector<IndexType>& indices) {
    const void* vertexData[] = {
        mesh_.vertices.data(),
        mesh_.verticesSkinned.data(),
    };

    const size_t vertexSizes[] = {
        sizeof(mesh_.vertices[0]) * mesh_.vertices.size(),
        sizeof(mesh_.verticesSkinned[0]) * mesh_.verticesSkinned.size(),
    };

    // Vertex + skin buffer.
    sourceVertexSkinBuffer_
        = Buffer::CreateFromData(vertexData, vertexSizes, 2, vk::BufferUsageFlagBits::eStorageBuffer, vk::MemoryPropertyFlagBits::eDeviceLocal);
    sourceVertexSkinBuffer_.SetName("SkinnedMeshSourceVertex");

    // Target vertex buffer.
    singleVertexBufferSize_ = utils::AlignTo(sizeof(Vertex) * mesh_.vertices.size(), GraphicsService::Graphics().StorageBufferOffsetAlignment());

    // Bone storage.
    matrixBufferAlignedSize_ = utils::AlignTo(sizeof(glm::mat4) * BoneCount(), GraphicsService::Graphics().StorageBufferOffsetAlignment());
    vk::DeviceSize matrixBufferSize{ matrixBufferAlignedSize_ * GraphicsService::FramesInFlight() };

    if (matrixBufferSize) {
        matrixBuffer_ = Buffer(matrixBufferSize, vk::BufferUsageFlagBits::eStorageBuffer, vk::MemoryPropertyFlagBits::eHostVisible);
        matrixBuffer_.SetName("SkinnedMeshMatrixBuffer");
    } else {
        matrixBuffer_ = {};
    }

    // TODO:
    AnimationCB animationCB{};
    animationCB.vertexCount = static_cast<int>(mesh_.vertices.size());

    animationCB_ = Buffer::CreateFromData(&animationCB, sizeof(animationCB), vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eDeviceLocal);
    animationCB_.SetName("SkinnedMeshAnimationCB");

    // Materials.
    std::vector<MaterialInstanceRef> materials;
    for (auto& mat : mesh_.materials) {
        auto newMaterial{ MaterialService::InstantiateDefault() };
        SetupMaterial(mat, newMaterial);
        materials.push_back(newMaterial);
    }

    // Index buffer.
    indexStart_ = GraphicsService::VertexBuffer()->Add(indices);
    for (uint32_t i = 0; i < GraphicsService::FramesInFlight(); ++i) {
        vertexBufferOffset_.push_back(GraphicsService::VertexBuffer()->VertexBufferOffset());
        vertexBufferStart_.push_back(GraphicsService::VertexBuffer()->Add(mesh_.vertices));

        // TODO:
        auto meshMaterial{ MESH_MATERIAL_ADD() };
        meshMaterial->mesh = MESH_ADD(1, aabb_, indexStart_, vertexBufferStart_.back(), static_cast<uint32_t>(mesh_.indices.size()), mesh_.rootNode->name);

        for (const auto& subMeshData : mesh_.meshes) {
            MeshMaterial::SubMesh subMesh;
            subMesh.indexCount = subMeshData.indexCount;
            subMesh.indexOffset = subMeshData.indexOffset;
            subMesh.material = materials[subMeshData.material];

            meshMaterial->submeshes.push_back(subMesh);
        }

        meshMaterials_.push_back(meshMaterial);
    }

    // Reset storage ownership.
    auto cmd{ GraphicsService::Device().BeginCommandList() };
    for (uint32_t i = 0; i < GraphicsService::FramesInFlight(); ++i) {
        GraphicsService::BufferReleaseRender(cmd, GraphicsService::VertexBuffer()->VertexBuf().GetBuffer(), vertexBufferOffset_[i], VertexBufferSize());
    }
    GraphicsService::Device().SubmitCommandLists();
}

void SkinnedMesh::Init(DescriptorSetPool& pool) {
    for (uint32_t i = 0; i < GraphicsService::FramesInFlight(); ++i) {
        std::vector<vk::DescriptorBufferInfo> bufferDesc(5);
        std::vector<vk::WriteDescriptorSet> writeDescriptorSets(5);

        bufferDesc[0] = sourceVertexSkinBuffer_.Descriptor();
        bufferDesc[0].offset = 0;
        bufferDesc[0].range = sizeof(mesh_.vertices[0]) * mesh_.vertices.size();

        bufferDesc[1] = sourceVertexSkinBuffer_.Descriptor();
        bufferDesc[1].offset = sizeof(mesh_.vertices[0]) * mesh_.vertices.size();
        bufferDesc[1].range = sizeof(mesh_.verticesSkinned[0]) * mesh_.vertices.size();

        bufferDesc[2] = matrixBuffer_.Descriptor();
        bufferDesc[2].offset = i * matrixBufferAlignedSize_;
        bufferDesc[2].range = matrixBufferAlignedSize_;

        bufferDesc[3] = GraphicsService::VertexBuffer()->VertexBufferDescriptor();
        bufferDesc[3].offset = vertexBufferOffset_[i];
        bufferDesc[3].range = sizeof(Vertex) * mesh_.vertices.size();

        bufferDesc[4] = animationCB_.Descriptor();

        auto descriptor{ pool.Allocate() };
        descriptors_.push_back(descriptor);

        writeDescriptorSets[0] = DescriptorBuffer(descriptor, vk::DescriptorType::eStorageBuffer, &bufferDesc[0], 0);
        writeDescriptorSets[1] = DescriptorBuffer(descriptor, vk::DescriptorType::eStorageBuffer, &bufferDesc[1], 1);
        writeDescriptorSets[2] = DescriptorBuffer(descriptor, vk::DescriptorType::eStorageBuffer, &bufferDesc[2], 2);
        writeDescriptorSets[3] = DescriptorBuffer(descriptor, vk::DescriptorType::eStorageBuffer, &bufferDesc[3], 3);
        writeDescriptorSets[4] = DescriptorBuffer(descriptor, vk::DescriptorType::eUniformBuffer, &bufferDesc[4], 4);

        GraphicsService::Graphics().Device().updateDescriptorSets(writeDescriptorSets, {});
    }
}

void SkinnedMesh::Destroy(DescriptorSetPool& pool) {
    for (auto& d : descriptors_) {
        pool.Release(d);
    }
}

MeshMaterialID SkinnedMesh::MeshMaterialID() const {
    return meshMaterials_[frame_].id();
}

void SkinnedMesh::AcquireCompute(GPUCommandList command) {
    if (!computeAcquired_) {
        computeAcquired_ = true;
        GraphicsService::BufferAcquireCompute(command, GraphicsService::VertexBuffer()->VertexBuf().GetBuffer(), VertexBufferOffset(), VertexBufferSize());
    }
}

void SkinnedMesh::ReleaseCompute(GPUCommandList command) {
    if (computeAcquired_) {
        computeAcquired_ = false;
        GraphicsService::BufferReleaseCompute(command, GraphicsService::VertexBuffer()->VertexBuf().GetBuffer(), VertexBufferOffset(), VertexBufferSize());
    }
}

void SkinnedMesh::AcquireRender(GPUCommandList command) {
    if (!renderAcquired_) {
        renderAcquired_ = true;
        GraphicsService::BufferAcquireRender(command, GraphicsService::VertexBuffer()->VertexBuf().GetBuffer(), VertexBufferOffset(), VertexBufferSize());
    }
}

void SkinnedMesh::ReleaseRender(GPUCommandList command) {
    if (renderAcquired_) {
        renderAcquired_ = false;
        GraphicsService::BufferReleaseRender(command, GraphicsService::VertexBuffer()->VertexBuf().GetBuffer(), VertexBufferOffset(), VertexBufferSize());
    }
}

uint32_t SkinnedMesh::VertexBufferOffset() const {
    return vertexBufferOffset_[frame_];
}

uint32_t SkinnedMesh::VertexBufferSize() const {
    return static_cast<uint32_t>(singleVertexBufferSize_);
}

vk::DescriptorSet SkinnedMesh::Descriptor() const {
    return descriptors_[frame_];
}

uint32_t SkinnedMesh::VertexCount() const {
    return static_cast<uint32_t>(mesh_.vertices.size());
}

glm::mat4 SkinnedMesh::InterpolateChannel(float time, const AnimationData::Channel& channel) const {
    core::Transformation trans{ Interpolate(time, channel.positions), Interpolate(time, channel.rotations), Interpolate(time, channel.scales) };
    return trans.Matrix();
}

void SkinnedMesh::UpdateMatrices(const AnimationData& animation, float time) {
    if (!matrixBuffer_) {
        return;
    }

    PROFILE_EVENT("UpdateMatrices");

    struct Entry {
        const MeshData::Node* node;
        glm::mat4 parentTransform;
    };

    frame_ = (frame_ + 1) % GraphicsService::FramesInFlight();

    std::stack<Entry> nodeStack;
    nodeStack.push(Entry{ mesh_.rootNode.get(), glm::mat4(1.0f) });

    auto matrices{ reinterpret_cast<glm::mat4*>(matrixBuffer_.Mapped(static_cast<uint32_t>(frame_ * matrixBufferAlignedSize_))) };

    while (!nodeStack.empty()) {
        Entry entry{ nodeStack.top() };
        nodeStack.pop();

        auto channelIt{ animation.channels.find(entry.node->name) };
        const glm::mat4 nodeTransform{ channelIt != animation.channels.end() ? InterpolateChannel(time, channelIt->second) : entry.node->transformation };
        const auto globalTransform{ entry.parentTransform * nodeTransform };

        auto it{ mesh_.boneNameToIndex.find(entry.node->name) };
        if (it != mesh_.boneNameToIndex.end()) {
            matrices[it->second] = mesh_.globalInverseTransform * globalTransform * mesh_.bones[it->second].offsetMatrix;
        }

        for (const auto& child : entry.node->nodes) {
            nodeStack.push(Entry{ child.get(), globalTransform });
        }
    }

    matrixBuffer_.Flush(frame_ * matrixBufferAlignedSize_, matrixBufferAlignedSize_);
}

} // namespace ugine::gfx
