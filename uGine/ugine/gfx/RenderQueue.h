﻿#pragma once

#include <ugine/gfx/Storages.h>
#include <ugine/gfx/rendering/MaterialInstance.h>
#include <ugine/gfx/rendering/Mesh.h>
#include <ugine/gfx/rendering/MeshMaterial.h>
#include <ugine/utils/Hash.h>

#include <entt/entt.hpp>

#include <algorithm>
#include <execution>

namespace ugine::gfx {

struct RenderBatch {
    uint64_t hash{};
    entt::entity entity{ entt::null };
    MeshMaterialID meshMaterialId{};
    uint32_t submeshId{};
    float distance{};
    int lod{};

    static RenderBatch Create(entt::entity entity, MeshMaterialID mesh, uint32_t submeshId, float distance, int lod) {
        // Sort first by distance then by mesh id.
        size_t meshMat{ static_cast<uint32_t>(mesh) };
        utils::HashCombine(meshMat, submeshId);

        uint64_t hash{ (static_cast<uint64_t>(distance) << 32) | (meshMat & 0xffffffff) };
        return { hash, entity, mesh, submeshId, distance, lod };
    }

    bool operator<(const RenderBatch& other) const {
        return hash < other.hash;
    }
};

struct RenderQueue {
    enum class SortBy { BackToFront, FrontToBack };

    utils::Vector<RenderBatch> batches;

    bool Empty() const {
        return batches.empty();
    }

    void Sort(SortBy sort = SortBy::FrontToBack) {
        std::sort(std::execution::par, batches.begin(), batches.end(),
            [sort](const RenderBatch& a, const RenderBatch& b) { return sort == SortBy::FrontToBack ? a < b : b < a; });
    }
};

struct RenderInstance {
    VertexBufferID vboID{};
    MeshMaterialID meshMaterialId{};
    uint32_t subMeshId{};
    uint32_t instanceCount{};
    uint32_t stencilWrite{};
    int lod{};

    inline bool CanMerge(const RenderInstance& other) const {
        return this->meshMaterialId == other.meshMaterialId && this->subMeshId == other.subMeshId && this->lod == other.lod
            && this->stencilWrite == other.stencilWrite;
    }
};

} // namespace ugine::gfx
