﻿#pragma once

#include "RenderingPass.h"

#include <ugine/core/Scene.h>
#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/core/Texture.h>

namespace ugine::gfx {

class DepthPass : public RenderingPass {
public:
    DepthPass() = default;
    explicit DepthPass(vk::SampleCountFlagBits samples);

    void Begin(RenderContext& context, const RenderTarget& depth) const;
    void End(RenderContext& context) const;

private:
    vk::ClearValue clearValue_;
    bool multisampled_{};
};

} // namespace ugine::gfx
