﻿#include "RenderingPass.h"

#include <ugine/gfx/Error.h>
#include <ugine/utils/Strings.h>

#include <algorithm>
#include <cctype>

namespace ugine::gfx {

RenderPassType RernderPassTypeFromString(const std::string_view& name) {
    const auto lower{ utils::ToLower(name) };

    if (lower == "main") {
        return RenderPassType::Main;
    } else if (lower == "depth") {
        return RenderPassType::Depth;
    } else if (lower == "shadow") {
        return RenderPassType::Shadows;
    } else if (lower == "shadowcube") {
        return RenderPassType::ShadowsCube;
    } else if (lower == "postprocess") {
        return RenderPassType::Postprocess;
    } else if (lower == "custom") {
        return RenderPassType::Custom;
    } else {
        UGINE_THROW(GfxError, "Invalid render pass type");
    }
}

bool IsShadowRenderPass(RenderPassType renderPass) {
    return renderPass == RenderPassType::Shadows || renderPass == RenderPassType::ShadowsCube;
}

} // namespace ugine::gfx
