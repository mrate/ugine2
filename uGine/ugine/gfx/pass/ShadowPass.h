﻿#pragma once

#include "RenderingPass.h"

#include <ugine/core/Scene.h>
#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/core/Texture.h>

namespace ugine::gfx {

class ShadowPass : public RenderingPass {
public:
    ShadowPass();

    void Begin(RenderContext& context, const TextureView& output) const;
    void BeginPoint(RenderContext& context, const TextureView& output, const TextureView& depth) const;
    void End(RenderContext& context) const;

private:
    void CreateRenderPass();
    void CreatePointRenderPass();

    vk::UniqueRenderPass pointRenderPass_;
};

} // namespace ugine::gfx
