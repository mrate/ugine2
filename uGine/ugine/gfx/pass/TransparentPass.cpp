﻿#include "TransparentPass.h"

#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/core/RenderPassFactory.h>
#include <ugine/gfx/tools/Initializers.h>

namespace ugine::gfx {

void TransparentPass::Begin(RenderContext& context, const RenderTarget& output) const {
    context.attachmentCount = 0;

    if (multisampled_) {
        context.attachments[context.attachmentCount++] = output.Multisample().View();
        context.attachments[context.attachmentCount++] = output.View().View();
    } else {
        context.attachments[context.attachmentCount++] = output.View().View();
    }

    context.samples = multisampled_ ? output.Samples() : vk::SampleCountFlagBits::e1;
    context.SetViewportAndScrissor(output.Extent().width, output.Extent().height);

    context.BeginRenderPass(*renderPass_, 0, nullptr);
}

void TransparentPass::End(RenderContext& context) const {
    context.EndRenderPass();
    context.ClearRenderTargets();
}

TransparentPass::TransparentPass(vk::SampleCountFlagBits samples, vk::Format framebufferFormat) {
    using namespace vk;

    multisampled_ = samples != SampleCountFlagBits::e1;

    // TODO: Multisampling.
    RenderPassFactory factory;

    factory.AddAttachment(samples, //
        framebufferFormat, //
        vk::AttachmentLoadOp::eLoad, //
        vk::AttachmentStoreOp::eStore, //
        vk::ImageLayout::eColorAttachmentOptimal, //
        vk::ImageLayout::eShaderReadOnlyOptimal);

    if (multisampled_) {
        factory.AddResolve(framebufferFormat);
    }

    // Synchronize with opaque pass.

    factory.AddSubpassDependency(VK_SUBPASS_EXTERNAL, 0, //
        vk::PipelineStageFlagBits::eColorAttachmentOutput, //
        vk::AccessFlagBits::eColorAttachmentWrite, //
        vk::PipelineStageFlagBits::eColorAttachmentOutput, //
        vk::AccessFlagBits::eColorAttachmentWrite);

    // Synchronize with postprocess.

    factory.AddSubpassDependency(0, VK_SUBPASS_EXTERNAL, //
        vk::PipelineStageFlagBits::eColorAttachmentOutput, //
        vk::AccessFlagBits::eColorAttachmentWrite, //
        vk::PipelineStageFlagBits::eFragmentShader, //
        vk::AccessFlagBits::eShaderRead);

    renderPass_ = factory.Create("Transparent RP");
}

} // namespace ugine::gfx
