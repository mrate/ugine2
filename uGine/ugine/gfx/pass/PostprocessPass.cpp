﻿#include "PostprocessPass.h"

#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/core/RenderPassFactory.h>
#include <ugine/gfx/tools/Initializers.h>

namespace ugine::gfx {

void PostprocessPass::Begin(RenderContext& context, const TextureView& output) const {
    std::array<vk::ClearValue, 1> clearValues;
    ClearColor(clearValues[0], 0, 0, 0, 1);

    context.SetRenderTarget(0, output);
    context.SetViewportAndScrissor(output.Extent().width, output.Extent().height);

    context.BeginRenderPass(*renderPass_, static_cast<uint32_t>(clearValues.size()), clearValues.data());
}

void PostprocessPass::End(RenderContext& context) const {
    context.EndRenderPass();
    context.ClearRenderTargets();
}

PostprocessPass::PostprocessPass(vk::Format format) {
    using namespace vk;

    RenderPassFactory factory;

    factory.AddAttachment( //
        SampleCountFlagBits::e1, //
        format, //
        AttachmentLoadOp::eDontCare, //
        AttachmentStoreOp::eStore, //
        ImageLayout::eUndefined, //
        ImageLayout::eShaderReadOnlyOptimal);

    // Sync with render pass.

    factory.AddSubpassDependency(VK_SUBPASS_EXTERNAL, 0, //
        PipelineStageFlagBits::eFragmentShader, //
        AccessFlagBits::eShaderRead, //
        PipelineStageFlagBits::eColorAttachmentOutput, //
        AccessFlagBits::eColorAttachmentWrite);

    factory.AddSubpassDependency(0, VK_SUBPASS_EXTERNAL, //
        PipelineStageFlagBits::eColorAttachmentOutput, //
        AccessFlagBits::eColorAttachmentWrite, //
        PipelineStageFlagBits::eFragmentShader, //
        AccessFlagBits::eShaderRead);

    renderPass_ = factory.Create("Postprocess RP");
}

} // namespace ugine::gfx
