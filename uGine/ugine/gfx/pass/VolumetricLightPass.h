﻿#pragma once

#include "RenderingPass.h"

#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/core/RenderTarget.h>
#include <ugine/gfx/rendering/MaterialInstance.h>

namespace ugine::gfx {

class VolumetricLightPass : public RenderingPass {
public:
    VolumetricLightPass();

    void Begin(RenderContext& context, const RenderTarget& output) const;
    void End(RenderContext& context) const;
};

class LightShaftsPass : public RenderingPass {
public:
    LightShaftsPass();

    void Begin(RenderContext& context, const RenderTarget& output, const RenderTarget& depth) const;
    void End(RenderContext& context) const;
};

class SsaoPass : public RenderingPass {
public:
    SsaoPass();

    void Begin(RenderContext& context, const RenderTarget& output) const;
    void End(RenderContext& context) const;
};

} // namespace ugine::gfx
