﻿#include "OpaquePass.h"

#include <ugine/gfx/Graphics.h>
#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/core/RenderPassFactory.h>
#include <ugine/gfx/core/RenderTarget.h>
#include <ugine/gfx/tools/Initializers.h>

namespace ugine::gfx {

OpaquePass::OpaquePass(vk::SampleCountFlagBits samples, vk::Format framebufferFormat) {
    using namespace vk;

    multisampled_ = samples != SampleCountFlagBits::e1;

    RenderPassFactory factory;
    factory.AddAttachment( //
        samples, //
        framebufferFormat, //
        AttachmentLoadOp::eClear, //
        AttachmentStoreOp::eStore, //
        ImageLayout::eUndefined, //
        ImageLayout::eColorAttachmentOptimal);

    factory.AddDepthStencilAttachment( //
        samples, //
        AttachmentLoadOp::eLoad, //
        AttachmentStoreOp::eStore, //
        AttachmentLoadOp::eClear, //
        AttachmentStoreOp::eStore, //
        ImageLayout::eDepthStencilReadOnlyOptimal, //
        ImageLayout::eDepthStencilReadOnlyOptimal);

    if (multisampled_) {
        factory.AddResolve(framebufferFormat);
        factory.AddDepthResolve();
    }

    // Synchronize with Depth pre-pass.

    factory.AddSubpassDependency( //
        VK_SUBPASS_EXTERNAL, //
        0, //
        //PipelineStageFlagBits::eEarlyFragmentTests | PipelineStageFlagBits::eLateFragmentTests, //
        //AccessFlagBits::eDepthStencilAttachmentWrite,
        PipelineStageFlagBits::eFragmentShader, //
        AccessFlagBits::eShaderRead,
        //PipelineStageFlagBits::eTransfer, //
        //AccessFlagBits::eTransferRead,
        PipelineStageFlagBits::eColorAttachmentOutput | PipelineStageFlagBits::eEarlyFragmentTests | PipelineStageFlagBits::eLateFragmentTests,
        AccessFlagBits::eColorAttachmentWrite | AccessFlagBits::eDepthStencilAttachmentRead | AccessFlagBits::eDepthStencilAttachmentWrite);

    // Synchronize with Transparent pass.

    factory.AddSubpassDependency( //
        0, //
        VK_SUBPASS_EXTERNAL, //
        PipelineStageFlagBits::eColorAttachmentOutput | PipelineStageFlagBits::eEarlyFragmentTests | PipelineStageFlagBits::eLateFragmentTests, //
        AccessFlagBits::eColorAttachmentWrite | AccessFlagBits::eDepthStencilAttachmentWrite, //
        PipelineStageFlagBits::eColorAttachmentOutput, //
        AccessFlagBits::eColorAttachmentWrite);

    renderPass_ = factory.Create("Opaque RP");
}

void OpaquePass::Begin(RenderContext& context, const RenderTarget& output, const RenderTarget& depth) const {
    context.attachmentCount = 0;

    if (multisampled_) {
        context.attachments[context.attachmentCount++] = output.Multisample().View();
        context.attachments[context.attachmentCount++] = depth.Multisample().View();
        context.attachments[context.attachmentCount++] = output.View().View();
        context.attachments[context.attachmentCount++] = depth.View().View();
    } else {
        context.attachments[context.attachmentCount++] = output.View().View();
        context.attachments[context.attachmentCount++] = depth.View().View();
    }

    context.samples = multisampled_ ? output.Samples() : vk::SampleCountFlagBits::e1;
    context.SetViewportAndScrissor(output.Extent().width, output.Extent().height);

    std::array<vk::ClearValue, 2> clearValues;
    ClearColor(clearValues[0], 0.4f, 0.5f, 0.8f, 0.0f);
    ClearDepthStencil(clearValues[1], 1.0f, 0);

    context.BeginRenderPass(*renderPass_, clearValues);
}

void OpaquePass::End(RenderContext& context) const {
    context.EndRenderPass();
    context.ClearRenderTargets();
}

} // namespace ugine::gfx
