﻿#pragma once

#include "RenderingPass.h"

#include <ugine/core/Scene.h>
#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/core/Texture.h>

namespace ugine::gfx {

class PostprocessPass : public RenderingPass {
public:
    explicit PostprocessPass(vk::Format format);

    void Begin(RenderContext& context, const TextureView& output) const;
    void End(RenderContext& context) const;
};

} // namespace ugine::gfx
