﻿#pragma once

#include <ugine/utils/Utils.h>

#include <vulkan/vulkan.hpp>

#include <stdint.h>
#include <string_view>

namespace ugine::gfx {

enum class RenderPassType : uint32_t {
    None = 0,
    Depth = 1 << 1,
    Main = 1 << 2,
    Shadows = 1 << 3,
    ShadowsCube = 1 << 4,
    Postprocess = 1 << 5,
    Custom = 1 << 6,
};

UGINE_FLAGS(RenderPassType, uint32_t);

enum class RenderFlags : uint32_t {
    None = 0,
    Opaque = 1 << 0,
    Transparent = 1 << 1,
    Particles = 1 << 2,
    SkyBox = 1 << 3,
    Foliage = 1 << 4,
    VolumetricLights = 1 << 5,
    LensFlare = 1 << 6,
    LightShafts = 1 << 7,
    AmbientOcclusion = 1 << 8,
    All = Opaque | Transparent | Particles | SkyBox | Foliage | VolumetricLights | LensFlare | LightShafts | AmbientOcclusion,
};

UGINE_FLAGS(RenderFlags, uint32_t);

RenderPassType RernderPassTypeFromString(const std::string_view& name);
bool IsShadowRenderPass(RenderPassType renderPass);

class RenderingPass {
public:
    UGINE_MOVABLE_ONLY(RenderingPass);

    RenderingPass() = default;
    virtual ~RenderingPass() {}

    vk::RenderPass RenderPass() const {
        return *renderPass_;
    }

protected:
    vk::UniqueRenderPass renderPass_;
};

} // namespace ugine::gfx
