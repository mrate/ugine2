﻿#include "ShadowPass.h"

#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/core/RenderPassFactory.h>
#include <ugine/gfx/tools/Initializers.h>

namespace ugine::gfx {

ShadowPass::ShadowPass() {
    CreateRenderPass();
    CreatePointRenderPass();
}

void ShadowPass::Begin(RenderContext& context, const TextureView& output) const {
    context.SetRenderTarget(0, output);
    context.SetViewportAndScrissor(output.Extent().width, output.Extent().height);

    vk::ClearValue clearValue;
    ClearDepthStencil(clearValue, 1.0f, 0);

    context.BeginRenderPass(*renderPass_, 1, &clearValue);
}

void ShadowPass::End(RenderContext& context) const {
    context.EndRenderPass();
    context.ClearRenderTargets();
}

void ShadowPass::BeginPoint(RenderContext& context, const TextureView& output, const TextureView& depth) const {
    context.SetRenderTarget(0, output);
    context.SetRenderTarget(1, depth);
    context.SetViewportAndScrissor(output.Extent().width, output.Extent().height);

    std::array<vk::ClearValue, 2> clearValue;
    // TODO: Max distance.
    ClearColor(clearValue[0], 999.0f, 1.0f, 1.0f, 1.0f);
    ClearDepthStencil(clearValue[1], 1.0f, 0);

    context.BeginRenderPass(*pointRenderPass_, clearValue);
}

void ShadowPass::CreateRenderPass() {
    using namespace vk;

    RenderPassFactory factory;

    factory.AddDepthAttachment( //
        SampleCountFlagBits::e1, //
        AttachmentLoadOp::eClear, //
        AttachmentStoreOp::eStore, //
        ImageLayout::eUndefined, //
        ImageLayout::eDepthStencilReadOnlyOptimal);

    renderPass_ = factory.Create("Shadow RP");
}

void ShadowPass::CreatePointRenderPass() {
    using namespace vk;

    RenderPassFactory factory;

    factory.AddAttachment( //
        SampleCountFlagBits::e1, //
        config::PointShadowMapFormat, //
        AttachmentLoadOp::eClear, //
        AttachmentStoreOp::eStore, //
        ImageLayout::eUndefined, //
        ImageLayout::eShaderReadOnlyOptimal);

    factory.AddDepthAttachment( //
        SampleCountFlagBits::e1, //
        AttachmentLoadOp::eClear, //
        AttachmentStoreOp::eDontCare, //
        ImageLayout::eUndefined, //
        ImageLayout::eDepthStencilAttachmentOptimal);

    factory.AddSubpassDependency(VK_SUBPASS_EXTERNAL, 0, //
        PipelineStageFlagBits::eBottomOfPipe, //
        AccessFlagBits::eMemoryRead, //
        PipelineStageFlagBits::eColorAttachmentOutput, //
        AccessFlagBits::eColorAttachmentWrite);

    factory.AddSubpassDependency(0, VK_SUBPASS_EXTERNAL, //
        PipelineStageFlagBits::eColorAttachmentOutput, //
        AccessFlagBits::eColorAttachmentWrite, //
        PipelineStageFlagBits::eBottomOfPipe, //
        AccessFlagBits::eMemoryRead);

    pointRenderPass_ = factory.Create("Point shadow RP");
}

} // namespace ugine::gfx
