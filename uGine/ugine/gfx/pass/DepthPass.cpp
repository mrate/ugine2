﻿#include "DepthPass.h"

#include <ugine/gfx/Graphics.h>
#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/core/RenderPassFactory.h>
#include <ugine/gfx/tools/Initializers.h>

namespace ugine::gfx {

DepthPass::DepthPass(vk::SampleCountFlagBits samples) {
    using namespace vk;

    multisampled_ = samples != SampleCountFlagBits::e1;

    RenderPassFactory factory;

    factory.AddDepthStencilAttachment( //
        samples, //
        AttachmentLoadOp::eClear, //
        AttachmentStoreOp::eStore, //
        AttachmentLoadOp::eDontCare, //
        AttachmentStoreOp::eDontCare, //
        ImageLayout::eUndefined, //
        ImageLayout::eDepthStencilReadOnlyOptimal //
    );

    if (multisampled_) {
        factory.AddDepthResolve(ImageLayout::eUndefined, ImageLayout::eDepthStencilReadOnlyOptimal);
    }

    factory.AddSubpassDependency(0, VK_SUBPASS_EXTERNAL, //
        PipelineStageFlagBits::eEarlyFragmentTests | PipelineStageFlagBits::eLateFragmentTests, //
        AccessFlagBits::eDepthStencilAttachmentWrite, //
        //PipelineStageFlagBits::eTransfer, //
        //AccessFlagBits::eTransferRead);
        PipelineStageFlagBits::eFragmentShader, //
        AccessFlagBits::eShaderRead);
    //PipelineStageFlagBits::eEarlyFragmentTests | PipelineStageFlagBits::eLateFragmentTests, //
    //AccessFlagBits::eDepthStencilAttachmentWrite | AccessFlagBits::eDepthStencilAttachmentRead);

    renderPass_ = factory.Create("Depth RP");
}

void DepthPass::Begin(RenderContext& context, const RenderTarget& depth) const {
    context.attachmentCount = 0;

    if (multisampled_) {
        context.attachments[context.attachmentCount++] = depth.Multisample().View();
        context.attachments[context.attachmentCount++] = depth.View().View();
    } else {
        context.attachments[context.attachmentCount++] = depth.View().View();
    }

    context.samples = multisampled_ ? depth.Samples() : vk::SampleCountFlagBits::e1;
    context.SetViewportAndScrissor(depth.Extent().width, depth.Extent().height);

    std::array<vk::ClearValue, 1> clearValues;
    ClearDepthStencil(clearValues[0], 1.0f, 0);

    context.BeginRenderPass(*renderPass_, clearValues);
}

void DepthPass::End(RenderContext& context) const {
    context.EndRenderPass();
    context.ClearRenderTargets();
}

} // namespace ugine::gfx
