﻿#pragma once

#include "RenderingPass.h"

#include <ugine/gfx/Gfx.h>

namespace ugine::gfx {

class RenderTarget;

class OpaquePass : public RenderingPass {
public:
    OpaquePass() = default;
    explicit OpaquePass(vk::SampleCountFlagBits samples, vk::Format framebufferFormat);

    void Begin(RenderContext& context, const RenderTarget& output, const RenderTarget& depth) const;
    void End(RenderContext& context) const;

private:
    vk::ClearValue clearValue_;
    bool multisampled_{};
};

} // namespace ugine::gfx
