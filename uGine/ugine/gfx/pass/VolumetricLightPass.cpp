﻿#include "VolumetricLightPass.h"

#include <ugine/core/Scene.h>
#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/core/RenderPassFactory.h>
#include <ugine/gfx/tools/Initializers.h>

namespace ugine::gfx {

VolumetricLightPass::VolumetricLightPass() {
    RenderPassFactory factory;

    factory.AddAttachment( //
        vk::SampleCountFlagBits::e1, //
        config::VolumetricLightFormat, //
        vk::AttachmentLoadOp::eClear, //
        vk::AttachmentStoreOp::eStore, //
        vk::ImageLayout::eUndefined, //
        vk::ImageLayout::eGeneral // Will be upscaled in compute shader afterwards.
    );

    // Synchronize with bilateral upscale.

    factory.AddSubpassDependency( //
        0, //
        VK_SUBPASS_EXTERNAL, //
        vk::PipelineStageFlagBits::eColorAttachmentOutput, //
        vk::AccessFlagBits::eColorAttachmentWrite, //
        vk::PipelineStageFlagBits::eComputeShader, //
        vk::AccessFlagBits::eShaderRead //
    );

    renderPass_ = factory.Create("Volumetric RP");
}

void VolumetricLightPass::Begin(RenderContext& context, const RenderTarget& output) const {
    context.attachmentCount = 0;
    context.attachments[context.attachmentCount++] = output.View().View();

    context.samples = vk::SampleCountFlagBits::e1;
    context.SetViewportAndScrissor(output.Extent().width, output.Extent().height);

    std::array<vk::ClearValue, 1> clearValues;
    ClearColor(clearValues[0], 0.0f, 0.0f, 0.0f, 0.0f);

    context.BeginRenderPass(*renderPass_, clearValues);
}

void VolumetricLightPass::End(RenderContext& context) const {
    context.EndRenderPass();
    context.ClearRenderTargets();
}

// Light-shafts.

LightShaftsPass::LightShaftsPass() {
    using namespace vk;

    RenderPassFactory factory;

    factory.AddAttachment( //
        SampleCountFlagBits::e1, //
        config::SunFormat, //
        AttachmentLoadOp::eClear, //
        AttachmentStoreOp::eStore, //
        ImageLayout::eGeneral, //
        ImageLayout::eGeneral);

    factory.AddDepthStencilAttachment( //
        SampleCountFlagBits::e1, //
        AttachmentLoadOp::eLoad, //
        AttachmentStoreOp::eStore, //
        AttachmentLoadOp::eLoad, //
        AttachmentStoreOp::eStore, //
        ImageLayout::eDepthStencilReadOnlyOptimal, //
        ImageLayout::eDepthStencilReadOnlyOptimal);

    // Sync
    //factory.AddSubpassDependency( //
    //    VK_SUBPASS_EXTERNAL, //
    //    0, //
    //    PipelineStageFlagBits::
    //);

    renderPass_ = factory.Create("Light shafts RP");
}

void LightShaftsPass::Begin(RenderContext& context, const RenderTarget& output, const RenderTarget& depth) const {
    context.attachmentCount = 0;
    context.attachments[context.attachmentCount++] = output.View().View();
    context.attachments[context.attachmentCount++] = depth.View().View();

    context.samples = vk::SampleCountFlagBits::e1;
    context.SetViewportAndScrissor(output.Extent().width, output.Extent().height);

    std::array<vk::ClearValue, 1> clearValues;
    ClearColor(clearValues[0], 0.0f, 0.0f, 0.0f, 0.0f);

    context.BeginRenderPass(*renderPass_, clearValues);
}

void LightShaftsPass::End(RenderContext& context) const {
    context.EndRenderPass();
    context.ClearRenderTargets();
}

// SSAO

SsaoPass::SsaoPass() {
    using namespace vk;

    RenderPassFactory factory;

    factory.AddAttachment( //
        SampleCountFlagBits::e1, //
        config::SsaoFormat, //
        AttachmentLoadOp::eDontCare, //
        AttachmentStoreOp::eStore, //
        ImageLayout::eUndefined, //
        ImageLayout::eGeneral);

    renderPass_ = factory.Create("SSAO RP");
}

void SsaoPass::Begin(RenderContext& context, const RenderTarget& output) const {
    context.attachmentCount = 0;
    context.attachments[context.attachmentCount++] = output.View().View();
    context.samples = vk::SampleCountFlagBits::e1;
    context.SetViewportAndScrissor(output.Extent().width, output.Extent().height);

    std::array<vk::ClearValue, 1> clearValues;
    ClearColor(clearValues[0], 0.0f, 0.0f, 0.0f, 0.0f);

    context.BeginRenderPass(*renderPass_, clearValues);
}

void SsaoPass::End(RenderContext& context) const {
    context.EndRenderPass();
    context.ClearRenderTargets();
}

} // namespace ugine::gfx
