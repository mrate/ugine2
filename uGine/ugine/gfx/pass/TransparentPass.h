﻿#pragma once

#include "RenderingPass.h"

#include <ugine/core/Scene.h>
#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/core/Texture.h>

namespace ugine::gfx {

class TransparentPass : public RenderingPass {
public:
    TransparentPass(vk::SampleCountFlagBits samples, vk::Format framebufferFormat);

    void Begin(RenderContext& context, const RenderTarget& output) const;
    void End(RenderContext& context) const;

private:
    bool multisampled_{};
};

} // namespace ugine::gfx
