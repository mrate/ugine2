﻿#pragma once

#include <glm/glm.hpp>

namespace ugine::gfx {

struct Color {
    Color()
        : rgba{ 0.0f, 0.0f, 0.0f, 1.0f } {}
    Color(float r, float g, float b, float a = 1.0f)
        : rgba{ r, g, b, a } {}
    Color(const glm::vec3& color)
        : rgba{ color.r, color.g, color.b, 1.0f } {}
    Color(const glm::vec4& color)
        : rgba{ color } {}
    Color(const float* color)
        : rgba{ color[0], color[1], color[2], color[3] } {}

    glm::vec4 rgba;

    glm::vec3 rgb() const {
        return glm::vec3(rgba.r, rgba.g, rgba.b);
    }

    void fromRgb(const glm::vec3& rgb) {
        rgba = glm::vec4(rgb.r, rgb.g, rgb.b, rgba.a);
    }

    float r() const {
        return rgba.r;
    }
    float g() const {
        return rgba.g;
    }
    float b() const {
        return rgba.b;
    }
    float a() const {
        return rgba.a;
    }
    float& operator[](int i) {
        return rgba[i];
    }
    float operator[](int i) const {
        return rgba[i];
    }
};

} // namespace ugine::gfx
