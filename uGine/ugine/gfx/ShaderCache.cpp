﻿#include "ShaderCache.h"

#include <ugine/gfx/Error.h>
#include <ugine/gfx/tools/Initializers.h>
#include <ugine/gfx/tools/SpirvCompiler.h>
#include <ugine/gfx/tools/SpirvParser.h>
#include <ugine/gfx/tools/SpirvPreprocessor.h>

#include <ugine/utils/File.h>
#include <ugine/utils/Profile.h>

namespace {

using namespace ugine::gfx;

std::vector<GPUPushConstant> ReduceOverlappingPC(const std::vector<GPUPushConstant>& pushConstantRange) {
    if (pushConstantRange.empty()) {
        return {};
    }

    uint32_t end{};
    uint32_t begin{};
    vk::ShaderStageFlags stage{};

    for (const auto& pc : pushConstantRange) {
        stage |= pc.stage;

        if (pc.offset < begin) {
            begin = pc.offset;
        }

        if (pc.offset + pc.size > end) {
            end = pc.offset + pc.size;
        }
    }

    return { GPUPushConstant{ stage, begin, end - begin } };
}

} // namespace

namespace ugine::gfx {

void ShaderCache::Destroy() {
    std::scoped_lock lock(Instance().shaderLock_);

    Instance().shaders_.clear();
}

inline std::filesystem::path SpirvFilePath(const std::filesystem::path& path) {
    return L".cache" / path.parent_path() / (path.filename().native() + L".spirv");
}

inline std::filesystem::path CacheFilePath(const std::filesystem::path& path) {
    return L".cache" / path.parent_path() / (path.filename().native() + L".spirv.cache");
}

bool HasValidCache(const std::filesystem::path& path) {
    // Two cache files - one holds compiled shader one holds set of include files for faster dependency checks.
    const auto spirvFile{ SpirvFilePath(path) };
    const auto cacheFile{ CacheFilePath(path) };
    auto hasCache{ std::filesystem::exists(cacheFile) && std::filesystem::exists(spirvFile) };
    auto cacheValid{ hasCache && utils::IsNewer(spirvFile, path) };

    if (hasCache && cacheValid) {
        // Check whether some of the include files changed.
        auto files{ utils::ReadFileLinesW(cacheFile, true) };
        for (auto& file : files) {
            if (utils::IsNewer(file, spirvFile)) {
                return false;
            }
        }
    }

    return cacheValid;
}

std::vector<uint8_t> ShaderCache::LoadShader(vk::ShaderStageFlags stage, const std::filesystem::path& path, const std::string& entryPoint) {
    if (!std::filesystem::exists(path)) {
        UGINE_THROW(GfxError, "Shader file not found.");
    }

    const auto spirvFile{ SpirvFilePath(path) };

    if (HasValidCache(path)) {
        UGINE_DEBUG("Loading shader {}", spirvFile.string());
        return utils::ReadFileBinary(spirvFile);
    } else {
        UGINE_DEBUG("Compiling shader {}", path.string());

        auto start{ std::chrono::high_resolution_clock::now() };

        std::string text{ utils::ReadFile(path) };
        std::string fileName{ path.string() };

        std::set<std::filesystem::path> includes;

        SpirvCompiler compiler;
        if (!compiler.Compile(stage, text, entryPoint, fileName, true, &includes)) {
            UGINE_THROW(GfxError, compiler.Error().c_str());
        }

        CompileTime += std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start).count();

        // Update cache files.
        std::set<std::wstring> includeStr;
        for (const auto& p : includes) {
            includeStr.insert(p.relative_path().native());
        }

        auto cachePath{ spirvFile.parent_path() };
        std::filesystem::create_directories(cachePath);

        utils::WriteFileLinesW(CacheFilePath(path), includeStr);
        utils::WriteFileBinary(spirvFile, compiler.Data());
        return compiler.Data();
    }
}

void ShaderCache::ParseBindings(Shader& shader) {
    auto start{ std::chrono::high_resolution_clock::now() };

    SpirvParser parser(shader.binary.data(), shader.binary.size());

    shader.parsedData = parser.ParsedData();
    shader.inputs = static_cast<uint32_t>(shader.parsedData.inputs.size());

    for (const auto& pc : shader.parsedData.pushConstants) {
        shader.pushConstants.push_back({ shader.stage, pc.offset, pc.size });
    }

    for (const auto& ds : shader.parsedData.descriptorSets) {
        uint32_t set{ ds.set };
        for (const auto& b : ds.bindings) {
            vk::ImageViewType imgType{};
            switch (b.dim) {
            case ShaderParsedData::Binding::TexDim::Tex2D:
                imgType = b.isArray ? vk::ImageViewType::e2DArray : vk::ImageViewType::e2D;
                break;
            case ShaderParsedData::Binding::TexDim::Tex3D:
                UGINE_ASSERT(!b.isArray);
                imgType = vk::ImageViewType::e3D;
                break;
            case ShaderParsedData::Binding::TexDim::TexCube:
                imgType = b.isArray ? vk::ImageViewType::eCubeArray : vk::ImageViewType::eCube;
                break;
            default:
                UGINE_ASSERT("Unsupported texture dimension.");
                break;
            }

            shader.bindings.push_back({ ds.set, b.binding, b.size, b.count, shader.stage, b.type, imgType });
        }
    }

    ParseTime += std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start).count();
}

std::set<std::filesystem::path> ShaderCache::ShaderIncludes(const std::filesystem::path& file) {
    std::set<std::filesystem::path> includes;
    auto files{ utils::ReadFileLinesW(CacheFilePath(file), true) };
    for (const auto& f : files) {
        includes.insert(f);
    }
    return includes;
}

const Shader& ShaderCache::GetShader(vk::ShaderStageFlagBits stage, const std::filesystem::path& file) {
    return Instance().GetShaderImpl(stage, file);
}

const Shader& ShaderCache::GetShaderImpl(vk::ShaderStageFlagBits stage, const std::filesystem::path& file) {
    bool load{ false };
    Key key{ stage, file };
    std::map<Key, Shader>::iterator it;

    {
        std::unique_lock lock(shaderLock_);

        it = shaders_.find(key);
        if (it == shaders_.end()) {
            load = true;
            it = shaders_.insert(std::make_pair(key, Shader{})).first;
            loadingShaders_.insert(key);
        } else {
            if (loadingShaders_.contains(key)) {
                auto now{ std::chrono::high_resolution_clock::now() };
                while (loadingShaders_.contains(key)) {
                    loadedCV_.wait(lock, [&]() { return !loadingShaders_.contains(key); });
                }

                auto dur{ std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - now).count() };
                UGINE_DEBUG("Waiting for shader {}: {}s", file.string(), float(dur) / 1000.0f);
                WaitTime += dur;
            } else if (!HasValidCache(file)) {
                UGINE_DEBUG("Reloading shader {}", file.string());
                load = true;
                it = shaders_.insert(std::make_pair(key, Shader{})).first;
                loadingShaders_.insert(key);
            }
        }
    }

    if (load) {
        utils::ScopeTimer timer(fmt::format("Load and parse shader {}", file.string()));

        Shader& shader{ it->second };

        shader.stage = stage;
        shader.entryPoint = "main";
        try {
            shader.binary = LoadShader(stage, file, shader.entryPoint);
        } catch (const std::exception& ex) {
            UGINE_ERROR("Failed to read shader {}: {}", file.string(), ex.what());
            // TODO:
        }

        vk::ShaderModuleCreateInfo moduleCreateInfo{};
        moduleCreateInfo.codeSize = shader.binary.size();
        moduleCreateInfo.pCode = reinterpret_cast<const uint32_t*>(shader.binary.data());

        shader.module = GraphicsService::Graphics().Device().createShaderModuleUnique(moduleCreateInfo);
        GraphicsService::Device().SetObjectDebugName(*shader.module, file.filename().string());

        ParseBindings(shader);

        {
            std::unique_lock lock(shaderLock_);
            loadingShaders_.erase(key);
        }

        loadedCV_.notify_all();
    }

    return it->second;
}

void ShaderProgram::AddShader(const Shader& shader) {
    shaderStages_.push_back({ {}, shader.stage, *shader.module, shader.entryPoint.c_str() });
    inputs_[shader.stage] = shader.inputs;

    std::copy(shader.bindings.begin(), shader.bindings.end(), std::back_inserter(bindings_));
    std::copy(shader.pushConstants.begin(), shader.pushConstants.end(), std::back_inserter(pushConstants_));
}

void ShaderProgram::Link(std::vector<vk::UniqueDescriptorSetLayout>& descriptorSetLayouts) {
    pushConstants_ = ReduceOverlappingPC(pushConstants_);

    std::map<std::pair<uint32_t, uint32_t>, ShaderBinding> uniqueSets;
    std::set<uint32_t> sets;

    const auto bindings{ Bindings() };
    for (const auto& binding : bindings) {
        auto key{ std::make_pair(binding.set, binding.binding) };
        auto it{ uniqueSets.find(key) };
        if (it == uniqueSets.end()) {
            uniqueSets.insert(std::make_pair(key, binding));
        } else {
            it->second.stage |= binding.stage;
        }

        sets.insert(binding.set);
    }

    bindings_.clear();
    for (const auto& [key, binding] : uniqueSets) {
        bindings_.push_back(binding);
    }

    std::map<uint32_t, std::vector<vk::DescriptorSetLayoutBinding>> dsLayoutBindigs;
    for (auto [set, binding] : uniqueSets) {
        dsLayoutBindigs[set.first].push_back(ShaderLayoutBinding(binding.stage, binding.type, binding.binding, binding.count));
    }

    descriptorSetLayouts.resize(sets.size());
    for (auto& [set, layouts] : dsLayoutBindigs) {
        vk::DescriptorSetLayoutCreateInfo descriptorSetLayoutCI;
        descriptorSetLayoutCI.bindingCount = static_cast<uint32_t>(layouts.size());
        descriptorSetLayoutCI.pBindings = layouts.data();

        descriptorSetLayouts[set] = GraphicsService::Graphics().Device().createDescriptorSetLayoutUnique(descriptorSetLayoutCI);
    }
}

uint32_t ShaderProgram::Inputs(vk::ShaderStageFlagBits stage) const {
    auto it{ inputs_.find(stage) };
    return it == inputs_.end() ? 0 : it->second;
}

} // namespace ugine::gfx
