﻿#pragma once

#include <ugine/utils/Vector.h>

#include <vulkan/vulkan.hpp>

#include <vector>

namespace ugine::gfx {

class Graphics;

class Presenter {
public:
    explicit Presenter(bool srgb);
    ~Presenter();

    bool BeginFrame();
    void Present(vk::CommandBuffer command, const utils::Vector<vk::Semaphore>& waitSemaphores = {});
    void Present();

    uint32_t FrameIndex() const {
        return frameIndex_;
    }

    bool RecreateSwapchain(vk::UniqueSurfaceKHR surface, uint32_t width, uint32_t height);

    vk::Format SwapChainFormat() const {
        return swapChainImageFormat_;
    }

    uint32_t SwapChainImageCount() const {
        return static_cast<uint32_t>(swapChainImages_.size());
    }

    const std::vector<vk::UniqueImageView>& SwapChainViews() const {
        return swapChainViews_;
    }

    vk::Extent2D SwapChainExtent() const {
        return swapChainExtent_;
    }

    vk::Semaphore WaitSemaphore() const;
    vk::Semaphore SignalSemaphore() const;
    vk::Fence Fence() const;

private:
    static const int MAX_FRAMES_IN_FLIGHT{ 3 };

    vk::UniqueSurfaceKHR surface_;

    bool Present(vk::Semaphore signalSemaphore, uint32_t imageIndex);

    bool AcquireNextImage(vk::Semaphore semaphore, uint32_t& imageIndex);

    void CreateSyncs();

    void CreateSwapChain(uint32_t width, uint32_t height);
    void DestroySwapChain();

    // Syncs.
    std::vector<vk::UniqueSemaphore> imageAvailableSemaphores_;
    std::vector<vk::UniqueSemaphore> renderFinishedSemaphores_;
    std::vector<vk::UniqueFence> inFlightFences_;
    std::vector<vk::Fence> imagesInFlight_;

    size_t currentFrame_{ 0 };
    uint32_t frameIndex_{ 0 };

    // Swapchain.
    vk::UniqueSwapchainKHR swapChain_;
    std::vector<vk::Image> swapChainImages_;
    std::vector<vk::UniqueImageView> swapChainViews_;
    vk::Format swapChainImageFormat_;
    vk::Extent2D swapChainExtent_;

    bool srgb_{ false };
};

} // namespace ugine::gfx
