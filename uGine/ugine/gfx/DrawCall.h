﻿#pragma once

#include <ugine/utils/Vector.h>

namespace ugine::gfx {

struct DrawCall {
    uint32_t indexCount{};
    uint32_t indexStart{};
    uint32_t vertexCount{};
    uint32_t vertexOffset{};
    uint32_t instanceCount{ 1 };
    uint32_t firstInstance{ 0 };
};

using DrawCallList = utils::Vector<DrawCall>;

} // namespace ugine::gfx
