﻿#pragma once

#include "Gfx.h"

#include "RenderQueue.h"
#include "Visibility.h"

#include <ugine/gfx/core/Device.h>

#include <ugine/gfx/pass/RenderingPass.h>
#include <ugine/math/Frustum.h>

#include <array>
#include <memory>

namespace ugine::core {
class Camera;
class Scene;
} // namespace ugine::core

namespace ugine::gfx {

class RenderContext {
public:
    GPUCommandList command;

    core::Scene* scene{}; // TODO: Const.
    const core::Camera* camera{};
    glm::mat4 viewProjPrev{};
    const Visibility* visibility{};

    RenderQueue opaqueQueue;
    RenderQueue transparentQueue;

    LightCounter lightCounter;

    vk::Viewport viewport{};
    vk::Rect2D scissor{};
    vk::RenderPass renderPass;
    std::array<vk::ImageView, limits::MAX_ATTACHMENTS> attachments;
    uint32_t attachmentCount{};

    vk::SampleCountFlagBits samples{ vk::SampleCountFlagBits::e1 };

    bool isInRenderPass{};
    vk::Framebuffer framebuffer;

    uint32_t stencil{};

    RenderPassType pass{ RenderPassType::Main };
    RenderFlags renderFlags{ RenderFlags::All };

    void SetCamera(const core::Camera* camera, const glm::mat4& viewProjPrev = glm::mat4{ 1.0 });

    void SetRenderTarget(uint32_t slot, const TextureView& texture);
    void SetRenderTarget(uint32_t slot, vk::ImageView texture);
    void ClearRenderTargets();

    void BeginRenderPass(vk::RenderPass renderPass, uint32_t clearColorCount = 0, const vk::ClearValue* clearColor = nullptr);
    void EndRenderPass();

    void BeginRenderPass(vk::RenderPass renderPass, const vk::ClearValue& clearColor) {
        BeginRenderPass(renderPass, 1, &clearColor);
    }

    template <typename T> void BeginRenderPass(vk::RenderPass renderPass, const T& t) {
        BeginRenderPass(renderPass, static_cast<uint32_t>(t.size()), t.data());
    }

    void SetViewport(uint32_t width, uint32_t height);
    void SetViewportAndScrissor(uint32_t width, uint32_t height);
    void DrawFullscreen();
    void DrawFullscreen(DrawCall& drawcall);
};

} // namespace ugine::gfx
