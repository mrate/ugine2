﻿#pragma once

#include <ugine/gfx/rendering/Material.h>
#include <ugine/gfx/rendering/MaterialInstance.h>

#include <filesystem>
#include <map>
#include <memory>
#include <string>

namespace ugine::utils {
class FileWatcher;
}

namespace ugine::gfx {

class Material;
class MaterialInstance;

class MaterialService {
public:
    static MaterialService& Instance();

    static void Init();
    static void Destroy();

    static std::vector<MaterialRef> Register(const std::vector<std::filesystem::path>& materialFiles);
    static std::vector<MaterialInstanceRef> Instantiate(const std::vector<std::filesystem::path>& materialFiles);

    static MaterialRef Register(const std::filesystem::path& materialFile);
    static MaterialRef FindMaterial(const std::string_view& name);
    static MaterialRef DefaultMaterial(bool transparent);

    static void Reload(const MaterialRef& material);
    static void Reload(MaterialID id);
    static bool ReloadChagned();

    static MaterialInstanceRef InstantiateDefault(bool transparent = false);
    static MaterialInstanceRef Instantiate(const std::string_view& material, const std::string_view& name = "");
    static MaterialInstanceRef Instantiate(const MaterialRef& material, const std::string_view& name = "");
    static MaterialInstanceRef Instantiate(const MaterialInstance& instance, const std::string_view& name = "");
    static MaterialInstanceRef InstantiateByPath(const std::filesystem::path& name);
    static MaterialInstanceRef InstanceFromFile(const std::filesystem::path& file);

    static void Remove(const MaterialRef& ref);
    static void RemoveInstance(const MaterialInstanceRef& ref);

    static const std::vector<MaterialRef>& Materials() {
        return Instance().materials_;
    }

    static std::vector<MaterialRef> Materials(const std::string_view& category);

    static const std::vector<MaterialInstanceRef>& Instances() {
        return Instance().instances_;
    }

    static std::vector<MaterialInstanceRef> Instances(const std::string_view& category);

    static std::string UniqueName(const std::string& name);

private:
    MaterialService() = default;

    MaterialService(MaterialService&) = delete;
    MaterialService& operator=(MaterialService&) = delete;

    MaterialRef Find(const std::string_view& name);
    void Register(MaterialRef& material, const std::filesystem::path& file);
    void RegisterFileWatch(MaterialID materialId, const std::set<std::filesystem::path>& files);

    void ShaderChanged(const std::filesystem::path& shader);

    static std::string GenerateName(const MaterialRef& material);

    // TODO:
    uint32_t nextId_{};

    std::vector<MaterialRef> materials_;
    std::map<std::filesystem::path, MaterialRef> materialsByPath_;

    MaterialRef defaultMaterial_;
    MaterialRef defaultMaterialTransparent_;

    std::vector<MaterialInstanceRef> instances_;

    uint32_t counter_{};

    // Set of all shaders for hot-reloading.
    std::unique_ptr<utils::FileWatcher> fileWatcher_;
    std::map<std::filesystem::path, std::set<MaterialID>> shaderMaterialMap_;

    std::mutex updatedFilesMutex_;
    std::set<MaterialID> updatedMaterials_;
};

} // namespace ugine::gfx
