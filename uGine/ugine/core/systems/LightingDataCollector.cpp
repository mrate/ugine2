﻿#include "LightingDataCollector.h"

#include <ugine/core/Camera.h>
#include <ugine/core/Component.h>
#include <ugine/core/GameObject.h>
#include <ugine/gfx/RenderContext.h>

#include <ugine/utils/Log.h>

#include <glm/glm.hpp>

#include <algorithm>
#include <execution>

namespace ugine::core {

LightingDataCollector::LightingDataCollector(gfx::LightShadingData& data, std::vector<gfx::LightEntry>& lights)
    : lightData_{ data }
    , lights_{ lights } {}

void LightingDataCollector::Collect(gfx::RenderContext& context) {
    auto& registry{ context.scene->Registry() };

    lightCount_ = 0;
    shadowCount_ = 0;
    dirLightCount_ = 0;
    spotLightCount_ = 0;
    pointLightCount_ = 0;

    lightData_.count = 0;
    lightData_.shadowCount = 0;

    context.lightCounter.SetLightShadows(context.scene->Registry().size<core::LightComponent>());

    auto view{ registry.view<LightComponent>() };

    lights_.resize(view.size());

    std::for_each(std::execution::par, view.begin(), view.end(), [&](auto ent) {
        auto go{ context.scene->Get(ent) };
        CollectLights(go, go.Component<core::LightComponent>(), context);
    });

    context.lightCounter.FinalizeLightShadows();

    lightData_.count = lightCount_;
    lightData_.shadowCount = shadowCount_;
}

void LightingDataCollector::CollectLights(const core::GameObject& go, core::LightComponent& light, gfx::RenderContext& context) {
    if (!go.IsEnabled()) {
        return;
    }

    auto lightId{ lightCount_.fetch_add(1) };
    if (lightId >= gfx::limits::MAX_LIGHTS) {
        lightCount_.fetch_sub(1);
        UGINE_WARN("Reached maximum of lights.");
        return;
    }

    light.lightId = lightId;
    const auto trans{ go.GlobalTransformation() };

    gfx::FillLightShadingData(light.light, lights_[lightId]);
    lights_[lightId].shadowIndex = -1;
    lights_[lightId].position = trans.position;
    lights_[lightId].direction = glm::normalize(trans.rotation * FORWARD);

    if (light.generatesShadows) {
        auto shadowId{ shadowCount_.fetch_add(1) };
        if (shadowId >= gfx::limits::MAX_SHADOWS) {
            shadowCount_.fetch_sub(1);
            return;
        }

        uint32_t shadowMapIndex;
        switch (light.light.type) {
        case gfx::Light::Type::Directional:
            shadowMapIndex = dirLightCount_.fetch_add(1);
            if (shadowMapIndex >= gfx::limits::MAX_DIR_SHADOWS) {
                dirLightCount_.fetch_sub(1);
                return;
            }
            break;
        case gfx::Light::Type::Spot:
            shadowMapIndex = spotLightCount_.fetch_add(1);
            if (shadowMapIndex >= gfx::limits::MAX_SPOT_SHADOWS) {
                spotLightCount_.fetch_sub(1);
                return;
            }
            break;
        case gfx::Light::Type::Point:
            shadowMapIndex = pointLightCount_.fetch_add(1);
            if (shadowMapIndex >= gfx::limits::MAX_POINT_SHADOWS) {
                pointLightCount_.fetch_sub(1);
                return;
            }
            break;
        }

        context.lightCounter.AddLightShadow(go.Entity(), shadowMapIndex);

        lights_[lightId].shadowMapIndex = shadowMapIndex;
        for (int i = 0; i < gfx::limits::CSM_LEVELS; ++i) {
            auto camera{ light.shadowMapCamera[i] };
            lightData_.shadows[shadowId].lightViewProj[i] = camera.ProjectionMatrix() * camera.ViewMatrix();

            if (!light.isCsm) {
                break;
            }
        }

        lights_[lightId].shadowIndex = shadowId;
        lights_[lightId].csmMaxLevel = light.isCsm ? gfx::limits::CSM_LEVELS : 0;
    }
}

} // namespace ugine::core
