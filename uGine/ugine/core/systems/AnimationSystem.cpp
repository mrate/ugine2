﻿#include "AnimationSystem.h"
#include "../Scene.h"

#include <ugine/assets/AssetService.h>
#include <ugine/core/Component.h>
#include <ugine/gfx/ShaderCache.h>
#include <ugine/gfx/tools/Initializers.h>
#include <ugine/utils/Profile.h>

#include <ugine/core/EventService.h>

namespace {
struct AnimationDebug {
    bool animatedThisFrame{};
};
} // namespace

namespace ugine::core {

AnimationSystem::AnimationSystem() {
    using namespace gfx;

    ShaderProgram shaderProgram;

    const auto& computeShader{ ShaderCache::GetShader(vk::ShaderStageFlagBits::eCompute, L"assets/shaders/compute/animation.comp") };
    shaderProgram.AddShader(computeShader);

    shaderProgram.Link(computeLayouts_);

    std::vector<vk::DescriptorSetLayout> dsLayouts;
    std::transform(computeLayouts_.begin(), computeLayouts_.end(), std::back_inserter(dsLayouts), [](auto& layout) { return *layout; });

    computePipeline_ = ComputePipeline(dsLayouts, computeShader);

    // TODO: Remove pool.
    // Descriptors.
    UGINE_ASSERT(computeLayouts_.size() == 1);
    std::vector<vk::DescriptorPoolSize> size{
        { vk::DescriptorType::eStorageBuffer, 4 * GraphicsService::FramesInFlight() },
        { vk::DescriptorType::eUniformBuffer, GraphicsService::FramesInFlight() },
    };

    pool_ = std::make_unique<DescriptorSetPool>(*computeLayouts_[0], size, GraphicsService::FramesInFlight());
}

void AnimationSystem::Update(entt::registry& reg) {
    // Update.
    for (auto [ent, mesh, animator] : reg.view<SkinnedMeshComponent, AnimatorComponent>().each()) {
        auto& debug{ reg.get_or_emplace<AnimationDebug>(ent) };
        debug.animatedThisFrame = false;

        if (animator.animation >= animator.animations.size()) {
            continue;
        }

        if (animator.isRunning) {
            animator.animationTime
                = std::fmod(animator.animationTime + animator.speed * CoreService::TimeDiffS(), animator.animations[animator.animation].lengthSeconds);
        }
    }
}

bool AnimationSystem::NeedCompute(entt::registry& reg) const {
    return !reg.view<AnimatorComponent>().empty();
}

void AnimationSystem::Compute(entt::registry& reg, gfx::GPUCommandList command) {
    PROFILE_EVENT("Animation compute");
    gfx::GPUDebugLabel label{ command, "SkinAnimation" };

    stats_.animations = 0;

    // Dispatch.
    for (auto [ent, mesh, animator] : reg.view<SkinnedMeshComponent, AnimatorComponent>().each()) {
        animator.lastUpdateTime += CoreService::TimeDiffMS();
        if (!animator.isRunning || animator.lastUpdateTime < animator.resolution || animator.animation >= animator.animations.size()) {
            continue;
        }

        if (mesh.mesh) {
            auto& debug{ reg.get_or_emplace<AnimationDebug>(ent) };
            debug.animatedThisFrame = true;

            ++stats_.animations;

            if (!mesh.mesh.Initialized()) {
                mesh.mesh.Init(*pool_);
            }

            // TODO: Update matrices in parallel.
            animator.lastUpdateTime = 0.0f;
            mesh.mesh.UpdateMatrices(animator.animations[animator.animation], animator.animationTime);
            Dispatch(command, mesh.mesh);
        }
    }
}

void AnimationSystem::PreRender(entt::registry& reg, gfx::GPUCommandList command) {
    for (auto [ent, mesh, animator] : reg.view<SkinnedMeshComponent, AnimatorComponent>().each()) {
        auto& debug{ reg.get<AnimationDebug>(ent) };
        if (debug.animatedThisFrame) {
            if (mesh.mesh) {
                mesh.mesh.AcquireRender(command);
            }
        }
    }
}

void AnimationSystem::PostRender(entt::registry& reg, gfx::GPUCommandList command) {
    for (auto [ent, mesh, animator] : reg.view<SkinnedMeshComponent, AnimatorComponent>().each()) {
        auto& debug{ reg.get<AnimationDebug>(ent) };
        if (debug.animatedThisFrame) {
            if (mesh.mesh) {
                mesh.mesh.ReleaseRender(command);
            }
        }
    }
}

void AnimationSystem::Dispatch(gfx::GPUCommandList command, gfx::SkinnedMesh& mesh) {
    using namespace gfx;

    mesh.AcquireCompute(command);

    auto& device{ GraphicsService::Device() };

    // TODO:
    device.BindPipeline(command, &computePipeline_);
    device.BindDescriptor(command, 0, mesh.Descriptor());
    device.Dispatch(command, 1 + static_cast<uint32_t>(mesh.VertexCount() / 256), 1, 1);

    mesh.ReleaseCompute(command);
}

} // namespace ugine::core
