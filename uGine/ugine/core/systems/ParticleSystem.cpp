﻿#include "ParticleSystem.h"

#include <ugine/assets/AssetService.h>
#include <ugine/core/Component.h>
#include <ugine/core/Scene.h>
#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/ShaderCache.h>
#include <ugine/gfx/core/GpuProfiler.h>
#include <ugine/gfx/tools/Initializers.h>
#include <ugine/utils/Profile.h>

namespace ugine::core {

ParticleSystem::ParticleSystem() {
    using namespace gfx;

    std::vector<vk::DescriptorSetLayoutBinding> bindings{
        ShaderLayoutBinding(vk::ShaderStageFlagBits::eCompute, vk::DescriptorType::eStorageBuffer, 0),
        ShaderLayoutBinding(vk::ShaderStageFlagBits::eCompute, vk::DescriptorType::eUniformBuffer, 1),
    };

    vk::DescriptorSetLayoutCreateInfo descriptorSetLayoutCI;
    descriptorSetLayoutCI.bindingCount = static_cast<uint32_t>(bindings.size());
    descriptorSetLayoutCI.pBindings = bindings.data();

    computeLayout_ = GraphicsService::Graphics().Device().createDescriptorSetLayoutUnique(descriptorSetLayoutCI);

    std::vector<vk::DescriptorSetLayout> dsLayouts{ *computeLayout_ };

    emitPipeline_ = ComputePipeline::Load(L"assets/shaders/compute/particleEmit.comp");
    emitMeshPipeline_ = ComputePipeline::Load(L"assets/shaders/compute/particleEmitMesh.comp");
    initPipeline_ = ComputePipeline::Load(L"assets/shaders/compute/particleInit.comp");
    finishPipeline_ = ComputePipeline::Load(L"assets/shaders/compute/particleFinish.comp");
    simulatePipeline_ = ComputePipeline::Load(L"assets/shaders/compute/particleSimulation.comp");
}

void ParticleSystem::Init(entt::registry& reg) {
    reg.on_construct<core::ParticleComponent>().connect<&ParticleSystem::AddParticle>(this);
    reg.on_destroy<core::ParticleComponent>().connect<&ParticleSystem::RemoveParticle>(this);
    reg.on_update<core::ParticleComponent>().connect<&ParticleSystem::UpdateParticle>(this);
}

void ParticleSystem::Destroy(entt::registry& reg) {
    reg.on_construct<core::ParticleComponent>().disconnect<&ParticleSystem::AddParticle>(this);
    reg.on_destroy<core::ParticleComponent>().disconnect<&ParticleSystem::RemoveParticle>(this);
    reg.on_update<core::ParticleComponent>().disconnect<&ParticleSystem::UpdateParticle>(this);

    emitPipeline_ = {};
    simulatePipeline_ = {};
    initPipeline_ = {};
    finishPipeline_ = {};
}

bool ParticleSystem::NeedCompute(entt::registry& reg) const {
    return !reg.view<gfx::Particles>().empty();
}

void ParticleSystem::Update(entt::registry& reg) {
    auto& scene{ Scene::FromRegistry(reg) };
    for (auto [ent, particle] : reg.view<gfx::Particles>().each()) {
        particle.SetTransformation(scene.Get(ent).GlobalTransformation().Matrix());
    }
}

void ParticleSystem::Compute(entt::registry& reg, gfx::GPUCommandList command) {
    PROFILE_EVENT("ParticleSystem::Compute");

    stats_.particles = 0;
    auto particles{ reg.view<gfx::Particles>() };

    if (!particles.empty()) {
        gfx::GPUDebugLabel label{ command, "ParticleSystem" };

        for (auto [ent, particle] : particles.each()) {
            if (particle.Count() > 0) {
                ++stats_.particles;

                bool useMesh{};

                const gfx::Buffer* vertex{};
                const gfx::Buffer* index{};

                uint32_t vertexCnt{};
                uint32_t indexCnt{};
                uint32_t vertexStart{};
                uint32_t indexStart{};

                gfx::DrawCall dc;

                const auto& comp{ reg.get<ParticleComponent>(ent) };
                if (comp.mesh) {
                    const gfx::MeshMaterial* meshMat{};
                    if (reg.has<SkinnedMeshComponent>(ent)) {
                        const auto& meshComp{ reg.get<SkinnedMeshComponent>(ent) };
                        meshMat = &meshComp.mesh.MeshMaterial();
                    } else if (reg.has<MeshComponent>(ent)) {
                        const auto& meshComp{ reg.get<MeshComponent>(ent) };
                        meshMat = &meshComp.meshMaterial;
                    }
                    vertex = &meshMat->mesh->VertexBuffer()->VertexBuf();
                    index = &meshMat->mesh->VertexBuffer()->IndexBuf();
                    dc = meshMat->mesh->DrawCall();
                    useMesh = true;
                }

                particle.Update(command, initPipeline_, useMesh ? emitMeshPipeline_ : emitPipeline_, simulatePipeline_, finishPipeline_, vertex, index, dc);
            }
        }
    }
}

void ParticleSystem::PreRender(entt::registry& reg, gfx::GPUCommandList command) {
    for (auto [entt, particle] : reg.view<gfx::Particles>().each()) {
        particle.AcquireRender(command);
    }
}

void ParticleSystem::PostRender(entt::registry& reg, gfx::GPUCommandList command) {
    for (auto [entt, particle] : reg.view<gfx::Particles>().each()) {
        particle.ReleaseRender(command);
    }
}

void ParticleSystem::AddParticle(entt::registry& reg, entt::entity ent) {
    auto& particles{ reg.get<ParticleComponent>(ent) };

    auto& particleSystem{ reg.emplace<gfx::Particles>(ent) };

    UpdateParticle(reg, ent);
}

void ParticleSystem::RemoveParticle(entt::registry& reg, entt::entity ent) {
    if (reg.has<gfx::Particles>(ent)) {
        auto& particleSystem{ reg.get<gfx::Particles>(ent) };
        reg.remove<gfx::Particles>(ent);
    }
}

void ParticleSystem::UpdateParticle(entt::registry& reg, entt::entity ent) {
    auto& particle{ reg.get<ParticleComponent>(ent) };
    auto& particleSystem{ reg.get<gfx::Particles>(ent) };

    particleSystem.SetCount(particle.count);
    particleSystem.SetTexture(particle.texture);
    particleSystem.SetColor(particle.color);
    particleSystem.SetSize(particle.size);
    particleSystem.SetLife(particle.life, particle.lifeSpan);
    particleSystem.SetSpeed(particle.speed);
    particleSystem.SetGravity(particle.gravity);
    particleSystem.SetStartOffset(particle.startOffset);
    particleSystem.SetSpeedOffset(particle.speedOffset);
    particleSystem.SetEmitCount(particle.emitCount);
}

} // namespace ugine::core
