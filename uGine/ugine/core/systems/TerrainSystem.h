﻿#pragma once

#include <ugine/core/systems/System.h>
#include <ugine/gfx/core/ComputePipeline.h>

namespace ugine::core {

class TerrainSystem : public System {
public:
    void Init(entt::registry& reg) override;
    void Destroy(entt::registry& reg) override;

private:
    void AddTerrain(entt::registry& reg, entt::entity ent);
    void RemoveTerrain(entt::registry& reg, entt::entity ent);
    void UpdateTerrain(entt::registry& reg, entt::entity ent);

    void AddFoliage(entt::registry& reg, entt::entity ent);
    void RemoveFoliage(entt::registry& reg, entt::entity ent);
    void UpdateFoliage(entt::registry& reg, entt::entity ent);

    gfx::ComputePipeline cullLodPipeline_;
};

} // namespace ugine::core
