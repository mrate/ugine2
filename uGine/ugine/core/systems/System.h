﻿#pragma once

#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/core/Device.h>

#include <entt/entt.hpp>

namespace ugine::core {

class System {
public:
    virtual ~System() {}

    virtual void Init(entt::registry& reg) {}
    virtual void Destroy(entt::registry& reg) {}

    virtual bool NeedCompute(entt::registry& reg) const {
        return false;
    }
    virtual void Compute(entt::registry& reg, gfx::GPUCommandList command) {}

    virtual void Update(entt::registry& reg) {}
    virtual void PreRender(entt::registry& reg, gfx::GPUCommandList command) {}
    virtual void Render(entt::registry& reg, gfx::GPUCommandList command) {}
    virtual void PostRender(entt::registry& reg, gfx::GPUCommandList command) {}
};

} // namespace ugine::core
