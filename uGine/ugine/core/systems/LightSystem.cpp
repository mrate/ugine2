﻿#include "LightSystem.h"

#include <ugine/core/Component.h>
#include <ugine/core/ConfigService.h>
#include <ugine/core/Core.h>
#include <ugine/core/Scene.h>

#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/data/LensFlare.h>

#include <ugine/utils/Profile.h>

namespace ugine::core {

void CreateCamera(uint32_t csm, LightComponent& light, const Transformation& transformation, float csmLightSize = 0.0f) {
    auto shadowResolution{ ConfigService::ShadowMapResolution() };

    switch (light.light.type) {
    case gfx::Light::Type::Directional:
        light.shadowMapCamera[csm] = Camera::Ortho(-csmLightSize, csmLightSize, csmLightSize, -csmLightSize, -100.0f, 100.0f);
        break;
    case gfx::Light::Type::Spot:
        light.shadowMapCamera[csm] = Camera::Perspective(
            2 * light.light.spotAngleDeg, static_cast<float>(shadowResolution.width), static_cast<float>(shadowResolution.height), 0.01f, light.light.range);
        break;
    case gfx::Light::Type::Point:
        light.shadowMapCamera[csm]
            = Camera::Perspective(90.0f, static_cast<float>(shadowResolution.width), static_cast<float>(shadowResolution.height), 0.01f, light.light.range);
        break;
    }
}

void LightSystem::Init(entt::registry& reg) {
    reg.on_construct<core::LightComponent>().connect<&LightSystem::LightUpdate>(this);
    reg.on_update<core::LightComponent>().connect<&LightSystem::LightUpdate>(this);
    reg.on_destroy<core::LightComponent>().connect<&LightSystem::LightRemove>(this);
}

void LightSystem::Destroy(entt::registry& reg) {
    reg.on_construct<core::LightComponent>().disconnect<&LightSystem::LightUpdate>(this);
    reg.on_update<core::LightComponent>().disconnect<&LightSystem::LightUpdate>(this);
    reg.on_destroy<core::LightComponent>().disconnect<&LightSystem::LightRemove>(this);
}

void LightSystem::Update(entt::registry& registry) {
    PROFILE_EVENT("LightSystem::Update");

    auto scene{ registry.ctx<Scene*>() };

    // TODO: On camera changed.
    auto mainCameraGO{ scene->Camera() };
    glm::vec3 mainCamPos{ 0.0f };
    if (mainCameraGO) {
        mainCamPos = mainCameraGO.GlobalTransformation().position;
    }

    for (auto [ent, light] : registry.view<LightComponent, gfx::ShadowFlag>().each()) {
        auto trans{ scene->Get(ent).GlobalTransformation() };

        for (auto& camera : light.shadowMapCamera) {
            if (light.light.type == gfx::Light::Type::Directional) {
                camera.SetViewMatrix(glm::lookAtRH(mainCamPos + trans.position + trans.rotation * core::FORWARD, mainCamPos + trans.position, core::UP));
            } else {
                camera.SetViewMatrix(glm::inverse(trans.Matrix()));
            }

            if (!light.isCsm) {
                break;
            }
        }
    }
}

void LightSystem::LightUpdate(entt::registry& registry, entt::entity ent) {
    auto& light{ registry.get<LightComponent>(ent) };
    auto scene{ registry.ctx<Scene*>() };
    const auto trans{ scene->Get(ent).GlobalTransformation() };

    if (light.generatesShadows) {
        if (light.isCsm && light.light.type == gfx::Light::Type::Directional) {
            for (int i = 0; i < gfx::limits::CSM_LEVELS; ++i) {
                const auto size{ ConfigService::DirLightCsmSize(i) };

                CreateCamera(i, light, trans, size);
            }
        } else {
            CreateCamera(0, light, trans, ConfigService::DirLightCsmSize(gfx::limits::CSM_LEVELS - 1));
        }
    }

    if (!light.flare && registry.has<gfx::LensFlare>(ent)) {
        registry.remove<gfx::LensFlare>(ent);
    } else if (light.flare) {
        gfx::LensFlare& lensFlare{ registry.get_or_emplace<gfx::LensFlare>(ent) };
        if (!lensFlare.material) {
            lensFlare.material = gfx::MaterialService::InstantiateByPath(L"assets/materials/flare.mat");
        }
        lensFlare.material->SetUniformVal("size", light.flareSize);
        lensFlare.material->SetUniformVal("fade", light.flareFade);
        lensFlare.material->SetTexture("flareTex", light.flareTexture);
    }

    if (light.volumetric != registry.has<gfx::VolumetricFlag>(ent)) {
        if (light.volumetric) {
            registry.emplace<gfx::VolumetricFlag>(ent);
        } else {
            registry.remove<gfx::VolumetricFlag>(ent);
        }
    }

    if (light.generatesShadows != registry.has<gfx::ShadowFlag>(ent)) {
        if (light.generatesShadows) {
            registry.emplace<gfx::ShadowFlag>(ent);
        } else {
            registry.remove<gfx::ShadowFlag>(ent);
        }
    }
}

void LightSystem::LightRemove(entt::registry& registry, entt::entity ent) {
    if (registry.has<gfx::VolumetricFlag>(ent)) {
        registry.remove<gfx::VolumetricFlag>(ent);
    }

    if (registry.has<gfx::ShadowFlag>(ent)) {
        registry.remove<gfx::ShadowFlag>(ent);
    }
}

} // namespace ugine::core
