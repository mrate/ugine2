﻿#include "TerrainSystem.h"

#include <ugine/core/Component.h>
#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/data/Shapes.h>
#include <ugine/gfx/rendering/Foliage.h>
#include <ugine/gfx/rendering/Terrain.h>

namespace ugine::core {

void TerrainSystem::Init(entt::registry& reg) {
    reg.on_construct<core::TerrainComponent>().connect<&TerrainSystem::AddTerrain>(this);
    reg.on_destroy<core::TerrainComponent>().connect<&TerrainSystem::RemoveTerrain>(this);
    reg.on_update<core::TerrainComponent>().connect<&TerrainSystem::UpdateTerrain>(this);

    reg.on_construct<core::FoliageComponent>().connect<&TerrainSystem::AddFoliage>(this);
    reg.on_destroy<core::FoliageComponent>().connect<&TerrainSystem::RemoveFoliage>(this);
    reg.on_update<core::FoliageComponent>().connect<&TerrainSystem::UpdateFoliage>(this);

    cullLodPipeline_ = gfx::ComputePipeline::Load(L"assets/shaders/compute/cullLod.comp");
}

void TerrainSystem::Destroy(entt::registry& reg) {
    reg.on_construct<core::TerrainComponent>().disconnect<&TerrainSystem::AddTerrain>(this);
    reg.on_destroy<core::TerrainComponent>().disconnect<&TerrainSystem::RemoveTerrain>(this);
    reg.on_update<core::TerrainComponent>().disconnect<&TerrainSystem::UpdateTerrain>(this);

    reg.on_construct<core::FoliageComponent>().disconnect<&TerrainSystem::AddFoliage>(this);
    reg.on_destroy<core::FoliageComponent>().disconnect<&TerrainSystem::RemoveFoliage>(this);
    reg.on_update<core::FoliageComponent>().disconnect<&TerrainSystem::UpdateFoliage>(this);

    cullLodPipeline_ = {};
}

void TerrainSystem::AddTerrain(entt::registry& reg, entt::entity ent) {
    auto& component{ reg.get<core::TerrainComponent>(ent) };
    auto& terrain{ reg.emplace<gfx::Terrain>(ent) };

    UpdateTerrain(reg, ent);
}

void TerrainSystem::RemoveTerrain(entt::registry& reg, entt::entity ent) {
    if (reg.has<gfx::Terrain>(ent)) {
        reg.remove<gfx::Terrain>(ent);
    }
}

void TerrainSystem::UpdateTerrain(entt::registry& reg, entt::entity ent) {
    const auto& component{ reg.get<core::TerrainComponent>(ent) };
    auto& terrain{ reg.get<gfx::Terrain>(ent) };

    // TODO: Compare.
    terrain.SetHeightScale(component.heightScale);
    terrain.SetHeightMap(component.heightMap);
    terrain.SetLayerTextures(component.layers);
    terrain.SetSplatTexture(component.splat);
    terrain.SetScale(component.size);
}

void TerrainSystem::AddFoliage(entt::registry& reg, entt::entity ent) {
    auto& component{ reg.get<core::FoliageComponent>(ent) };
    auto& foliage{ reg.emplace<gfx::FoliageArray>(ent) };
    foliage.layers.reserve(core::FoliageComponent::MAX_LAYERS);

    UpdateFoliage(reg, ent);
}

void TerrainSystem::RemoveFoliage(entt::registry& reg, entt::entity ent) {
    if (reg.has<gfx::FoliageArray>(ent)) {
        reg.remove<gfx::FoliageArray>(ent);
    }
}

void TerrainSystem::UpdateFoliage(entt::registry& reg, entt::entity ent) {
    auto& component{ reg.get<core::FoliageComponent>(ent) };
    auto& foliage{ reg.get<gfx::FoliageArray>(ent) };

    if (component.layerCount != foliage.layers.size()) {
        auto oldSize{ foliage.layers.size() };
        foliage.layers.resize(component.layerCount);

        for (auto i = oldSize; i < foliage.layers.size(); ++i) {
            foliage.layers[i] = gfx::Foliage(&cullLodPipeline_); // TODO: Pipeline.
        }
    }

    for (uint32_t i = 0; i < component.layerCount; ++i) {
        const auto& componentLayer{ component.layers[i] };
        auto& foliageLayer{ foliage.layers[i] };

        if (componentLayer.foliage != foliageLayer.FoliageDesc() || componentLayer.count != foliageLayer.Count()) {
            foliageLayer.SetFoliageDesc(componentLayer.foliage);
            foliageLayer.SetCount(componentLayer.count);

            const gfx::Image* heightMap{};
            glm::vec3 mmin{ 0, 0, 0 };
            glm::vec3 mmax{ 10, 0, 10 };

            if (reg.has<gfx::Terrain>(ent)) {
                const auto& terrain{ reg.get<gfx::Terrain>(ent) };
                mmin = terrain.Aabb().Min();
                mmax = terrain.Aabb().Max();
                heightMap = terrain.HeightMapImage() ? &terrain.HeightMapImage() : nullptr;
            }

            foliageLayer.Place(mmin, mmax, heightMap);

            component.realCount[i] = foliageLayer.Count();
        }
    }
}

} // namespace ugine::core
