﻿#pragma once

#include <ugine/core/systems/System.h>
#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/core/ComputePipeline.h>
#include <ugine/gfx/core/DescriptorSetPool.h>

#include <ugine/core/Events.h>

#include <entt/entt.hpp>

namespace ugine::gfx {
class SkinnedMesh;
}

namespace ugine::core {

class AnimationSystem : public System {
public:
    struct Stats {
        uint32_t animations{};
    };

    AnimationSystem();

    bool NeedCompute(entt::registry& reg) const override;
    void Compute(entt::registry& reg, gfx::GPUCommandList command) override;

    void Update(entt::registry& reg) override;
    void PreRender(entt::registry& reg, gfx::GPUCommandList command) override;
    void PostRender(entt::registry& reg, gfx::GPUCommandList command) override;

    const Stats& FrameStats() const {
        return stats_;
    }

private:
    void Dispatch(gfx::GPUCommandList command, gfx::SkinnedMesh& mesh);

    gfx::ComputePipeline computePipeline_;
    std::vector<vk::UniqueDescriptorSetLayout> computeLayouts_;
    std::unique_ptr<gfx::DescriptorSetPool> pool_;

    Stats stats_;
};
} // namespace ugine::core
