﻿#pragma once

#include <ugine/core/systems/System.h>

namespace ugine::core {

class CameraSystem : public System {
public:
    void Init(entt::registry& reg) override;
    void Destroy(entt::registry& reg) override;

    void Update(entt::registry& reg) override;
    void PostRender(entt::registry& reg, gfx::GPUCommandList command) override;

private:
    void CameraCheck(entt::registry& reg, entt::entity ent);
};

} // namespace ugine::core
