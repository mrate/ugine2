﻿#pragma once

#include <ugine/core/systems/System.h>
#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/core/ComputePipeline.h>
#include <ugine/gfx/core/DescriptorSetPool.h>
#include <ugine/gfx/core/Pipeline.h>
#include <ugine/gfx/rendering/Material.h>
#include <ugine/gfx/rendering/Particles.h>

namespace ugine::core {

class ParticleSystem : public core::System {
public:
    struct Stats {
        uint32_t particles{};
    };

    ParticleSystem();

    void Init(entt::registry& reg) override;
    void Destroy(entt::registry& reg) override;

    bool NeedCompute(entt::registry& reg) const override;
    void Update(entt::registry& reg) override;
    void Compute(entt::registry& reg, gfx::GPUCommandList command) override;
    void PreRender(entt::registry& reg, gfx::GPUCommandList command) override;
    void PostRender(entt::registry& reg, gfx::GPUCommandList command) override;

    void AddParticle(entt::registry& reg, entt::entity ent);
    void RemoveParticle(entt::registry& reg, entt::entity ent);
    void UpdateParticle(entt::registry& reg, entt::entity ent);

    const Stats& FrameStats() const {
        return stats_;
    }

private:
    gfx::ComputePipeline emitPipeline_;
    gfx::ComputePipeline emitMeshPipeline_;
    gfx::ComputePipeline simulatePipeline_;
    gfx::ComputePipeline initPipeline_;
    gfx::ComputePipeline finishPipeline_;

    vk::UniqueDescriptorSetLayout computeLayout_;
    std::unique_ptr<gfx::DescriptorSetPool> pool_;

    Stats stats_;
};

} // namespace ugine::core
