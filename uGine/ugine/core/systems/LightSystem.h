﻿#pragma once

#include <ugine/core/systems/System.h>

namespace ugine::core {

class LightSystem : public System {
public:
    void Init(entt::registry& registry) override;
    void Destroy(entt::registry& registry) override;

    void Update(entt::registry& registry) override;

private:
    void LightUpdate(entt::registry& registry, entt::entity ent);
    void LightRemove(entt::registry& registry, entt::entity ent);
};

} // namespace ugine::core
