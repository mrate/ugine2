﻿#include "CameraSystem.h"

#include <ugine/core/Component.h>
#include <ugine/core/Scene.h>
#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/Storages.h>
#include <ugine/gfx/data/SamplerCache.h>
#include <ugine/gfx/tools/Initializers.h>

#include <algorithm>

namespace ugine::core {

void CameraSystem::Init(entt::registry& reg) {
    reg.on_construct<CameraComponent>().connect<&CameraSystem::CameraCheck>(this);
    reg.on_update<CameraComponent>().connect<&CameraSystem::CameraCheck>(this);
}

void CameraSystem::Destroy(entt::registry& reg) {
    reg.on_construct<CameraComponent>().disconnect<&CameraSystem::CameraCheck>(this);
    reg.on_update<CameraComponent>().disconnect<&CameraSystem::CameraCheck>(this);
}

void CameraSystem::Update(entt::registry& reg) {
    auto& scene{ Scene::FromRegistry(reg) };
    reg.view<CameraComponent>().each([&](auto ent, auto& camera) {
        auto go{ scene.Get(ent) };
        camera.camera.SetViewMatrix(ViewMatrixFromTransformation(go.GlobalTransformation()));
    });
}

void CameraSystem::PostRender(entt::registry& reg, gfx::GPUCommandList command) {
    auto& scene{ Scene::FromRegistry(reg) };
    reg.view<CameraComponent>().each([&](auto ent, auto& camera) { camera.viewProjectionPrev = camera.camera.ViewProjectionMatrix(); });
}

void CameraSystem::CameraCheck(entt::registry& reg, entt::entity ent) {
    auto& camera{ reg.get<CameraComponent>(ent) };
    if (camera.renderTarget) {
        camera.camera.SetSize(static_cast<float>(camera.renderTarget.Width()), static_cast<float>(camera.renderTarget.Height()));
    }

    if (camera.isMain) {
        // Ensure there is only single main camera in one scene.
        for (auto entt : reg.view<CameraComponent>()) {
            if (entt == ent) {
                continue;
            }

            reg.patch<CameraComponent>(entt, [](auto& c) { c.isMain = false; });
        }
    }

    if (camera.renderTarget) {
        gfx::GPUCommandList cmd{ gfx::NullCommandList };

        if (camera.renderTarget.Width() != camera.postprocess.Width() || camera.renderTarget.Height() != camera.postprocess.Height()) {
            camera.postprocess.Resize(camera.renderTarget.Width(), camera.renderTarget.Height());
        }

        if (!camera.depthBuffer || camera.depthBuffer.Width() != camera.renderTarget.Width() || camera.depthBuffer.Height() != camera.renderTarget.Height()
            || camera.renderTarget.Samples() != camera.depthBuffer.Samples()) {
            SAFE_DELETE(camera.depthBuffer);

            camera.depthBuffer = gfx::RenderTarget(camera.renderTarget.Width(), camera.renderTarget.Height(),
                gfx::GraphicsService::Graphics().DepthStencilFormat(), camera.renderTarget.Samples(),
                vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eDepthStencilAttachment | vk::ImageUsageFlagBits::eTransferSrc, // TODO
                vk::ImageAspectFlagBits::eDepth, gfx::GraphicsService::DepthSampler(), true);
            camera.depthBuffer.SetName("CameraDepthbuffer");

            // TODO:
            using namespace ugine::gfx;
            auto depthCopy{ TEXTURE_ADD(camera.depthBuffer.Extent(), gfx::GraphicsService::Graphics().DepthStencilFormat(), vk::ImageTiling::eOptimal,
                vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled) };
            depthCopy->SetName("CameraDepthBuffer copy");

            camera.depthCopy = TEXTURE_VIEW_ADD(depthCopy,
                TextureView::CreateSampled(*depthCopy, gfx::SamplerCache::Sampler(gfx::SamplerCache::Type::LinearClampToBorderWhite), vk::ImageViewType::e2D,
                    vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil));

            // Init.
            gfx::EnsureCommand(cmd);
            camera.depthBuffer.TransitionStencil(cmd, vk::ImageLayout::eUndefined, vk::ImageLayout::eDepthStencilReadOnlyOptimal);
            depthCopy->Transition(
                cmd, vk::ImageLayout::eUndefined, vk::ImageLayout::eShaderReadOnlyOptimal, vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil);
        }

        // TODO: Volumetric
        if (!camera.volumetricLightRT) {
            using namespace ugine::gfx;

            const auto width{ std::max<uint32_t>(1, uint32_t(camera.renderTarget.Width() * gfx::config::VolumentricLightDownsample)) };
            const auto height{ std::max<uint32_t>(1, uint32_t(camera.renderTarget.Height() * gfx::config::VolumentricLightDownsample)) };

            camera.volumetricLightRT = gfx::RenderTarget(width, height, gfx::config::VolumetricLightFormat, vk::SampleCountFlagBits::e1,
                vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eStorage);
            camera.volumetricLightRT.SetName("VolumetricLight RT");

            auto volUpscaleTexture{ TEXTURE_ADD(camera.renderTarget.Extent(), gfx::config::VolumetricLightFormat, vk::ImageTiling::eOptimal,
                vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eStorage) };
            volUpscaleTexture->SetName("Volumetric Upscale");

            camera.volumetricUpscale = TEXTURE_VIEW_ADD(volUpscaleTexture, gfx::TextureView::CreateSampled(*volUpscaleTexture));

            // Init.
            gfx::EnsureCommand(cmd);
            volUpscaleTexture->Transition(cmd, vk::ImageLayout::eUndefined, vk::ImageLayout::eShaderReadOnlyOptimal);
        }

        // Sun
        if (!camera.sunRToutput || camera.sunRToutput.Extent() != camera.renderTarget.Extent()) {
            using namespace ugine::gfx;

            camera.sunRTstencil = gfx::RenderTarget(camera.renderTarget.Width(), camera.renderTarget.Height(), gfx::config::SunFormat,
                vk::SampleCountFlagBits::e1, vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eSampled);
            camera.sunRTstencil.SetName("Sun inter. RT");

            camera.sunRToutput = gfx::RenderTarget(camera.renderTarget.Width(), camera.renderTarget.Height(), gfx::config::SunFormat,
                vk::SampleCountFlagBits::e1, vk::ImageUsageFlagBits::eStorage | vk::ImageUsageFlagBits::eSampled);
            camera.sunRToutput.SetName("Sun RT");

            // Init.
            gfx::EnsureCommand(cmd);
            camera.sunRTstencil.Transition(cmd, vk::ImageLayout::eUndefined, vk::ImageLayout::eGeneral);
            camera.sunRToutput.Transition(cmd, vk::ImageLayout::eUndefined, vk::ImageLayout::eShaderReadOnlyOptimal);
        }

        // AO
        if (!camera.aoRT || camera.aoRT.Extent() != camera.aoRT.Extent()) {
            using namespace ugine::gfx;

            camera.aoRTtmp = gfx::RenderTarget(camera.renderTarget.Width(), camera.renderTarget.Height(), gfx::config::SsaoFormat, vk::SampleCountFlagBits::e1,
                vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eStorage);
            camera.aoRTtmp.SetName("SSAO Intermediate RT");

            camera.aoRT = gfx::RenderTarget(camera.renderTarget.Width(), camera.renderTarget.Height(), gfx::config::SsaoFormat, vk::SampleCountFlagBits::e1,
                vk::ImageUsageFlagBits::eStorage | vk::ImageUsageFlagBits::eSampled);
            camera.aoRT.SetName("SSAO RT");

            // Init.
            gfx::EnsureCommand(cmd);
            camera.aoRT.Transition(cmd, vk::ImageLayout::eUndefined, vk::ImageLayout::eShaderReadOnlyOptimal);
        }

        if (!camera.sceneCopy || camera.sceneCopy->Extent() != camera.renderTarget.Extent()) {
            using namespace ugine::gfx;

            // TODO: MIPS.
            auto sceneTexture{ TEXTURE_ADD(camera.renderTarget.Extent(), camera.renderTarget.Format(), vk::ImageTiling::eOptimal,
                vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled, vk::SampleCountFlagBits::e1) };
            sceneTexture->SetName("CameraSceneCopy");
            camera.sceneCopy = TEXTURE_VIEW_ADD(sceneTexture, gfx::TextureView::CreateSampled(*sceneTexture));
        }

        if (cmd != gfx::NullCommandList) {
            gfx::GraphicsService::Device().SubmitCommandLists();
        }
    }
}

} // namespace ugine::core
