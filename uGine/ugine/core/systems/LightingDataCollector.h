﻿#pragma once

#include <ugine/core/Scene.h>
#include <ugine/gfx/rendering/LightShadingData.h>

#include <vector>

namespace ugine::core {

class LightingDataCollector {
public:
    LightingDataCollector(gfx::LightShadingData& data, std::vector<gfx::LightEntry>& lights);

    void Collect(gfx::RenderContext& context);

private:
    void CollectLights(const core::GameObject& go, core::LightComponent& light, gfx::RenderContext& context);

    gfx::LightShadingData& lightData_;
    std::vector<gfx::LightEntry>& lights_;

    std::atomic_uint32_t dirLightCount_{};
    std::atomic_uint32_t spotLightCount_{};
    std::atomic_uint32_t pointLightCount_{};

    std::atomic_uint32_t lightCount_{};
    std::atomic_uint32_t shadowCount_{};
};

} // namespace ugine::core
