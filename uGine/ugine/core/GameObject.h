﻿#pragma once

#include "Component.h"
#include "Core.h"
#include "Error.h"

#include <ugine/math/Math.h>
#include <ugine/utils/Assert.h>

#include <entt/entt.hpp>

#include <glm/glm.hpp>

#include <string>
#include <string_view>

namespace ugine::core {

class Scene;

struct Tag {
    enum Flags {
        Disabled = 1 << 0,
        Outline = 1 << 1,
    };

    std::string name;
    uint32_t flags{};
};

struct Parent {
    entt::entity parent{ entt::null };
};

struct Relationship {
    uint32_t children{};
    entt::entity fistChild{ entt::null };
    entt::entity lastChild{ entt::null };
    entt::entity prevSibling{ entt::null };
    entt::entity nextSibling{ entt::null };
};

class GameObject {
public:
    static GameObject Create(entt::registry& registry, const std::string& name);
    static void Destroy(const GameObject& go);

    GameObject() {}

    GameObject(entt::registry& registry, entt::entity entity)
        : handle_(registry, entity) {}

    GameObject Parent() const {
        return handle_.has<core::Parent>() ? GameObject(*handle_.registry(), handle_.get<core::Parent>().parent) : GameObject{};
    }

    Scene* Scene() {
        return IsValid() ? handle_.registry()->ctx<core::Scene*>() : nullptr;
    }

    const core::Scene* Scene() const {
        return IsValid() ? handle_.registry()->ctx<core::Scene*>() : nullptr;
    }

    bool IsValid() const {
        return handle_.entity() != entt::null;
    }

    const std::string& Name() const {
        UGINE_ASSERT(handle_.has<Tag>());
        return handle_.get<Tag>().name;
    }

    void SetName(const std::string& name) {
        UGINE_ASSERT(handle_.has<Tag>());
        handle_.patch<Tag>([&](auto& t) { t.name = name; });
    }

    bool IsEnabled() const {
        UGINE_ASSERT(handle_.has<Tag>());
        return !(handle_.get<Tag>().flags & Tag::Flags::Disabled);
    }

    void SetEnabled(bool enabled) {
        UGINE_ASSERT(handle_.has<Tag>());
        handle_.patch<Tag>([enabled](Tag& t) { t.flags = enabled ? (t.flags & ~Tag::Flags::Disabled) : (t.flags | Tag::Flags::Disabled); });
    }

    bool HasChild() const {
        return handle_.get<Relationship>().children > 0;
    }

    uint32_t ChildrenCount() const {
        return handle_.get<Relationship>().children;
    }

    void AddChild(entt::entity child);
    void RemoveChild(entt::entity child);

    GameObject PrevSibling() const {
        const auto& rel{ handle_.get<Relationship>() };
        return rel.prevSibling == entt::null ? GameObject{} : GameObject(*handle_.registry(), rel.prevSibling);
    }

    GameObject NextSibling() const {
        const auto& rel{ handle_.get<Relationship>() };
        return rel.nextSibling == entt::null ? GameObject{} : GameObject(*handle_.registry(), rel.nextSibling);
    }

    GameObject FirstChild() const {
        const auto& rel{ handle_.get<Relationship>() };
        return rel.fistChild == entt::null ? GameObject{} : GameObject(*handle_.registry(), rel.fistChild);
    }

    template <typename T> T& Component() {
        return handle_.get<T>();
    }

    template <typename T> const T& Component() const {
        return handle_.get<T>();
    }

    template <typename T> bool Has() const {
        return handle_.has<T>();
    }

    template <typename T, typename... Args> T& CreateComponent(Args&&... args) {
        handle_.emplace<T>(std::forward<Args>(args)...);
        return handle_.get<T>();
    }

    template <typename T> void RemoveComponent() {
        handle_.remove<T>();
    }

    template <typename T, typename... Args> T& GetOrCreateComponent(Args&&... args) {
        return handle_.has<T>() ? handle_.get<T>() : handle_.emplace<T>(std::forward<Args>(args)...);
    }

    template <typename T, typename... Args> T& Replace(Args&&... args) {
        return handle_.replace<T>(std::forward<Args>(args)...);
    }

    template <typename T, typename... Args> void Patch(Args&&... args) {
        handle_.patch<T>(std::forward<Args>(args)...);
    }

    template <> void RemoveComponent<Tag>() {
        UGINE_ASSERT(false && "Cannot remove Tag component.");
        UGINE_THROW(core::Error, "Cannot remove Tag component.");
    }

    template <> void RemoveComponent<Relationship>() {
        UGINE_ASSERT(false && "Cannot remove Relationship component.");
        UGINE_THROW(core::Error, "Cannot remove Relationship component.");
    }

    void MoveTo(const glm::vec3& to) {
        auto trans{ GlobalTransformation() };
        trans.position = to;
        SetGlobalTransformation(trans);
    }

    void MoveTo(float x, float y, float z) {
        MoveTo({ x, y, z });
    }

    void Scale(const glm::vec3& scale) {
        auto trans{ GlobalTransformation() };
        trans.scale = scale;
        SetGlobalTransformation(trans);
    }

    void Scale(float x, float y, float z) {
        Scale({ x, y, z });
    }

    void Scale(float scale) {
        Scale({ scale, scale, scale });
    }

    void Rotate(const glm::fquat& quat) {
        auto trans{ GlobalTransformation() };
        trans.rotation = quat;
        SetGlobalTransformation(trans);
    }

    void Rotate(const glm::vec3& euler) {
        Rotate(glm::fquat(euler));
    }

    void Rotate(float x, float y, float z) {
        Rotate(glm::fquat({ x, y, z }));
    }

    void LookAt(const glm::vec3& to) {
        auto trans{ GlobalTransformation() };
        trans.rotation = math::LookAt(to - trans.position, UP);
        SetGlobalTransformation(trans);
    }

    void LookAt(const GameObject& other) {
        LookAt(other.GlobalTransformation().position);
    }

    operator bool() const {
        return IsValid();
    }

    operator uint32_t() const {
        return static_cast<uint32_t>(handle_.entity());
    }

    operator entt::entity() const {
        return handle_.entity();
    }

    entt::entity Entity() const {
        return handle_.entity();
    }

    Transformation GlobalTransformation() const {
        // TODO: Cache.
        const auto& t{ Component<TransformationComponent>().transformation };
        return Parent() ? Parent().GlobalTransformation() * t : t;
    }

    void SetGlobalTransformation(const Transformation& trans) {
        // TODO: Cache.
        if (Parent()) {
            SetLocalTransformation(Parent().GlobalTransformation().Inverse() * trans);
        } else {
            SetLocalTransformation(trans);
        }
    }

    const Transformation& LocalTransformation() const {
        return Component<TransformationComponent>().transformation;
    }

    void SetLocalTransformation(const Transformation& trans) {
        Patch<TransformationComponent>([&](auto& t) { t.transformation = trans; });

        for (auto c{ FirstChild() }; c.IsValid(); c = c.NextSibling()) {
            c.Patch<TransformationComponent>([](auto&) {});
        }
    }

    GameObject Clone();

private:
    GameObject CloneImpl(bool isRoot);

    entt::handle handle_;
};

bool operator==(const GameObject& a, const GameObject& b);
bool operator!=(const GameObject& a, const GameObject& b);

} // namespace ugine::core
