﻿#include "ConfigService.h"
#include "Core.h"

#include <ugine/gfx/rendering/Light.h>
#include <ugine/gfx/tools/VulkanJson.h>
#include <ugine/utils/Log.h>

#include <nlohmann/json.hpp>

#include <fstream>

namespace ugine::core {

ConfigService& ConfigService::Instance() {
    static ConfigService inst;
    return inst;
}

void ConfigService::Load(const std::filesystem::path& file) {
    using json = nlohmann::json;

    try {
        std::ifstream in(file);
        if (!in.fail()) {
            json j;
            in >> j;

            const auto& config{ j["config"] };
            Instance().msaa_ = static_cast<vk::SampleCountFlagBits>(config.value<VkSampleCountFlagBits>("msaa", VK_SAMPLE_COUNT_1_BIT));
            Instance().anisotropicFilteringLevels_ = config.value("anisotropy", 1);
            Instance().vertexBufferInitialSize_ = config.value<uint32_t>("vertexBufferInitSize", 1 << 28);
            Instance().indexBufferInitialSize_ = config.value<uint32_t>("indexBufferInitSize", 1 << 24);

            if (config.contains("shadows")) {
                const auto& shadows{ config["shadows"] };
                Instance().shadowsEnabled_ = shadows["enabled"];
                Instance().shadowMapResolution_.width = shadows["width"];
                Instance().shadowMapResolution_.height = shadows["height"];
            }
        }
    } catch (const std::exception& ex) {
        UGINE_ERROR("Failed to read config file {}: {}", file.string(), ex.what());
    }
}

void ConfigService::Save(const std::filesystem::path& file) {
    using json = nlohmann::json;

    try {
        std::ofstream out(file);
        if (!out.fail()) {
            json j;

            auto& config{ j["config"] };
            config["msaa"] = static_cast<VkSampleCountFlagBits>(Instance().msaa_);
            config["anisotropy"] = Instance().anisotropicFilteringLevels_;
            config["vertexBufferInitSize"] = Instance().vertexBufferInitialSize_;
            config["indexBufferInitSize"] = Instance().indexBufferInitialSize_;

            {
                auto& shadows{ config["shadows"] };
                shadows["width"] = Instance().shadowMapResolution_.width;
                shadows["height"] = Instance().shadowMapResolution_.height;
                shadows["enabled"] = Instance().shadowsEnabled_;
            }

            out << j;
        }
    } catch (const std::exception& ex) {
        UGINE_ERROR("Failed to read config file {}: {}", file.string(), ex.what());
    }
}

uint32_t ConfigService::VertexBufferInitialSize() {
    return Instance().vertexBufferInitialSize_;
}

uint32_t ConfigService::IndexBufferInitialSize() {
    return Instance().indexBufferInitialSize_;
}

int ConfigService::DefaultLodLevels() {
    return 4;
}

ConfigService::ConfigService() {
    dirLightCsmSize_.resize(gfx::limits::CSM_LEVELS);
    dirLightCsmSize_[0] = 10.0f;
    dirLightCsmSize_[1] = 25.0f;
    dirLightCsmSize_[2] = 50.0f;
}

vk::Extent2D ConfigService::ShadowMapResolution() {
    return Instance().shadowMapResolution_;
}

void ConfigService::SetShadowMapResolution(vk::Extent2D resolution) {
    Instance().shadowMapResolution_ = resolution;
}

vk::Extent2D ConfigService::ShadowMapCubeResolution() {
    return Instance().shadowMapCubeResolution_;
}

void ConfigService::SetShadowMapCubeResolution(vk::Extent2D resolution) {
    Instance().shadowMapCubeResolution_ = resolution;
}

bool ConfigService::ShadowsEnabled() {
    return Instance().shadowsEnabled_;
}

void ConfigService::SetShadowEnabled(bool enabled) {
    Instance().shadowsEnabled_ = enabled;
}

vk::SampleCountFlagBits ConfigService::MsaaSamples() {
    return Instance().msaa_;
}

void ConfigService::SetMsaaSamples(vk::SampleCountFlagBits samples) {
    Instance().msaa_ = samples;
}

float ConfigService::DirLightCsmSize(int csmLevel) {
    return Instance().dirLightCsmSize_[csmLevel];
}

void ConfigService::SetDirLightCsmSize(int csmLevel, float size) {
    Instance().dirLightCsmSize_[csmLevel] = size;
}

float ConfigService::VolumetricIntensity() {
    return Instance().volumetricIntensity_;
}

void ConfigService::SetVolumetricIntensity(float intensity) {
    Instance().volumetricIntensity_ = intensity;
}

float ConfigService::ShadowIntensity() {
    return Instance().shadowIntensity_;
}

void ConfigService::SetShadowIntensity(float intensity) {
    Instance().shadowIntensity_ = intensity;
}

int ConfigService::AnisotropicFilteringLevels() {
    return Instance().anisotropicFilteringLevels_;
}

void ConfigService::SetAnisotropicFilteringLevels(int levels) {
    Instance().anisotropicFilteringLevels_ = levels;
}

} // namespace ugine::core
