﻿#pragma once

#include <ugine/utils/ConcurentRingBuffer.h>
#include <ugine/utils/Singleton.h>

#include <TaskScheduler.h>

#include <atomic>
#include <memory>

namespace ugine::core {

class Scheduler : public utils::Singleton<Scheduler> {
public:
    struct Group {
        std::atomic_uint32_t count;
        std::array<enki::TaskSet, 4096> tasks;
    };

    static void Init();
    static void Destroy();

    static void Schedule(Group& grp, std::function<void()> func);
    static void Schedule(Group& grp, uint32_t num, std::function<void(uint32_t, uint32_t, uint32_t)> func);
    static void Wait(Group& grp);

    static void Schedule(enki::ITaskSet* task);
    static void WaitFor(enki::ICompletable* task);
    static void WaitForAll();
    static uint32_t TaskThreads();

private:
    void InitThreads(bool startup);

    std::unique_ptr<enki::TaskScheduler> scheduler_;
};

} // namespace ugine::core
