﻿#pragma once

#include <atomic>
#include <chrono>
#include <thread>

namespace ugine::utils {
class MemAllocator;
}

namespace ugine::core {

class CoreService {
public:
    using Clock = std::chrono::high_resolution_clock;

    static CoreService& Instance();

    static void Init(std::chrono::milliseconds maxDelta);
    static void Destroy();

    static bool IsMainThread();

    static bool PhysicsActive();
    static void SetPhysicsActive(bool active);

    static Clock::time_point FrameTime();
    static uint64_t Time();
    static float TimeDiffMS();
    static float TimeDiffS();
    static uint64_t Frame();
    static void BeginFrame();

    static bool IsDebug() {
#ifdef _DEBUG
        return true;
#endif
        return false;
    }

    // Memory allocations.
    static utils::MemAllocator& MainAllocator();
    static utils::MemAllocator& StlAllocator();

private:
    CoreService();

    CoreService(const CoreService&) = delete;
    CoreService& operator=(const CoreService&) = delete;

    CoreService(CoreService&&) = delete;
    CoreService& operator=(CoreService&&) = delete;

    float maxDeltaMicros_{};
    Clock::time_point startTime_{};
    Clock::time_point frameTime_{};
    Clock::time_point lastFrameTime_{};
    float timeDiffMS_{};
    float timeDiffS_{};
    uint64_t frameNum_{};

    std::unique_ptr<utils::MemAllocator> mainAllocator_;
    std::unique_ptr<utils::MemAllocator> stlAllocator_;

    std::thread::id mainThreadId_;

    bool physicsActive_{ true };
};

} // namespace ugine::core
