﻿#include "Scheduler.h"

#include <ugine/utils/Log.h>
#include <ugine/utils/Profile.h>

#include <algorithm>
#include <vector>

namespace ugine::core {

class ProfilerInitializer : public enki::IPinnedTask {
public:
    ProfilerInitializer(uint32_t threadNum, bool startup)
        : enki::IPinnedTask{ threadNum }
        , startup_{ startup } {}

    void Execute() override {
        if (startup_) {
            PROFILE_START_THREAD(fmt::format("Worker thread {}", threadNum).c_str());
        } else {
            PROFILE_STOP_THREAD();
        }
    }

private:
    bool startup_{};
};

void Scheduler::InitThreads(bool startup) {

    std::vector<std::unique_ptr<ProfilerInitializer>> tasks(scheduler_->GetNumTaskThreads());
    for (uint32_t i = 0; i < scheduler_->GetNumTaskThreads(); ++i) {
        tasks[i] = std::make_unique<ProfilerInitializer>(i, startup);
        scheduler_->AddPinnedTask(tasks[i].get());
    }

    std::for_each(tasks.begin(), tasks.end(), [&](auto& t) { scheduler_->WaitforTask(t.get()); });
}

void Scheduler::Init() {
    Instance().scheduler_ = std::make_unique<enki::TaskScheduler>();
    Instance().scheduler_->Initialize();

    Instance().InitThreads(true);
}

void Scheduler::Destroy() {
    WaitForAll();
    Instance().InitThreads(false);
    Instance().scheduler_ = nullptr;
}

void Scheduler::Schedule(Group& grp, std::function<void()> func) {
    auto index{ grp.count.fetch_add(1) };

    auto task = new (reinterpret_cast<void*>(&grp.tasks[index])) enki::TaskSet{ [func](enki::TaskSetPartition t, uint32_t num) { func(); } };
    Instance().scheduler_->AddTaskSetToPipe(task);
}

void Scheduler::Schedule(Group& grp, uint32_t num, std::function<void(uint32_t, uint32_t, uint32_t)> func) {
    auto index{ grp.count.fetch_add(1) };

    auto task
        = new (reinterpret_cast<void*>(&grp.tasks[index])) enki::TaskSet{ num, [func](enki::TaskSetPartition t, uint32_t num) { func(t.start, t.end, num); } };
    Instance().scheduler_->AddTaskSetToPipe(task);
}

void Scheduler::Wait(Group& grp) {
    //Instance().scheduler_->WaitforAll();
    auto cnt{ grp.count.load() };
    for (uint32_t i = 0; i < cnt; ++i) {
        Instance().scheduler_->WaitforTask(&grp.tasks[i]);
        grp.tasks[i].~TaskSet();
    }
}

void Scheduler::Schedule(enki::ITaskSet* task) {
    Instance().scheduler_->AddTaskSetToPipe(task);
}

void Scheduler::WaitFor(enki::ICompletable* task) {
    Instance().scheduler_->WaitforTask(task);
}

void Scheduler::WaitForAll() {
    Instance().scheduler_->WaitforAll();
}

uint32_t Scheduler::TaskThreads() {
    return Instance().scheduler_->GetNumTaskThreads();
}

} // namespace ugine::core
