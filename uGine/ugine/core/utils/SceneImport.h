﻿#pragma once

#include "../GameObject.h"
#include "../Scene.h"

#include <filesystem>

namespace ugine::gfx {
struct ImportMeshSettings;
}

namespace ugine::core {

GameObject ImportMesh(const gfx::ImportMeshSettings& settings, Scene& scene);

} // namespace ugine::core
