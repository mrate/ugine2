﻿#include "SceneImport.h"

#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/import/MaterialImporter.h>
#include <ugine/gfx/import/MeshLoader.h>
#include <ugine/utils/Profile.h>

namespace ugine::core {

GameObject ImportMesh(const gfx::ImportMeshSettings& settings, Scene& scene) {
    using namespace ugine::gfx;

    utils::ScopeTimer timer{ fmt::format("Importing scene {}...", settings.file.string()) };

    auto go{ scene.CreateObject(settings.file.filename().string()) };
    auto material{ settings.material ? settings.material : MaterialService::DefaultMaterial(false) };

    if (settings.singleMesh) {
        auto meshMaterial{ ImportMeshMaterial(settings) };
        go.CreateComponent<MeshComponent>(meshMaterial);
    } else {
        MeshLoader::Settings importSettings{};
        importSettings.axisUp = settings.axisUp;
        importSettings.fixPaths = settings.fixPaths;
        importSettings.scale = settings.scale;
        importSettings.singleMesh = false;
        importSettings.preTransform = false;

        MeshLoader loader{ settings.file, importSettings };
        std::vector<MeshData> meshDataMulti;
        meshDataMulti = loader.LoadMeshes();

        uint32_t materialIndex{};
        for (const auto& meshData : meshDataMulti) {
            std::vector<MaterialInstanceRef> materials;
            for (const auto& m : meshData.materials) {
                MaterialInstanceRef newMat;
                newMat = MaterialService::Instantiate(material);
                SetupGenericMaterial(m, newMat);

                newMat->SetName(fmt::format("{}#{}", settings.file.filename().string(), materialIndex++));
                materials.push_back(newMat);
            }

            for (const auto& subMeshData : meshData.meshes) {
                std::vector<IndexType> indices(
                    meshData.indices.begin() + subMeshData.indexOffset, meshData.indices.begin() + subMeshData.indexOffset + subMeshData.indexCount);

                auto mesh{ MESH_ADD(settings.lodLevels, meshData.vertices, indices, false, subMeshData.name) };
                GameObject child{ scene.CreateObject(subMeshData.name) };
                child.CreateComponent<MeshComponent>(MESH_MATERIAL_ADD(mesh, materials[subMeshData.material]));
                child.SetGlobalTransformation(Transformation{ subMeshData.transformation });
                go.AddChild(child);
            }
        }
    }

    return go;
}

} // namespace ugine::core
