﻿#include "Component.h"

#include <ugine/script/NativeScript.h>

namespace ugine::core {

void NativeScriptComponent::Add(const GameObject& go, const std::shared_ptr<script::NativeScript>& script) {
    scripts_.push_back(script);
    script->OnAttach(go);
}

void NativeScriptComponent::Remove(const GameObject& go, const std::shared_ptr<script::NativeScript>& script) {
    scripts_.erase(std::remove(scripts_.begin(), scripts_.end(), script), scripts_.end());
    script->OnDetach(go);
}

} // namespace ugine::core
