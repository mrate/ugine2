﻿#pragma once

#include "Camera.h"
#include "GameObject.h"

#include <ugine/core/systems/System.h>
#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/core/Device.h>
#include <ugine/math/Raycast.h>

#include <entt/entt.hpp>

#include <string>

namespace ugine::core {

class Scene {
public:
    UGINE_MOVABLE_ONLY(Scene);

    template <typename... Args> struct ViewWrapper {
        using View = entt::basic_view<entt::entity, entt::exclude_t<>, Args...>;
        using VIterator = typename View::iterator;

        struct Iterator {
            Iterator(Scene* scene, const typename VIterator& it)
                : scene_{ scene }
                , it_{ it } {}

            GameObject operator*() {
                return GameObject{ scene_->registry_, *it_ };
            }

            bool operator==(const Iterator& oth) const {
                return it_ == oth.it_;
            }
            bool operator!=(const Iterator& oth) const {
                return it_ != oth.it_;
            }
            void operator++() {
                ++it_;
            }
            void operator++(int) {
                it_++;
            }

        private:
            Scene* scene_{};
            VIterator it_;
        };

        ViewWrapper(Scene* scene, View v)
            : scene_(scene)
            , view_(v) {}

        template <typename F> void Each(F&& f) {
            view_.each([&](entt::entity e, Args&... args) { f(GameObject{ scene_->registry_, e }, std::forward<Args>(args)...); });
        }

        Iterator begin() {
            return Iterator{ scene_, view_.begin() };
        }

        Iterator end() {
            return Iterator{ scene_, view_.end() };
        }

    private:
        Scene* scene_{};
        View view_;
    };

    static Scene& FromRegistry(const entt::registry& reg);

    Scene();
    ~Scene();

    GameObject CreateObject(const std::string& name);

    void Init();
    void Destroy();

    void Update();

    void Compute();

    void PreRender(gfx::GPUCommandList command);
    void Render(gfx::GPUCommandList command);
    void PostRender(gfx::GPUCommandList command);

    uint32_t Size() {
        return static_cast<uint32_t>(registry_.alive());
    }

    gfx::GPUSemaphore& ComputeSemaphore() {
        return computeSemaphore_;
    }

    // Game objects.
    template <typename T> void Each(T&& t) {
        registry_.each([&](auto ent) { t(GameObject{ registry_, ent }); });
    }

    template <typename... Args> auto View() {
        return ViewWrapper<Args...>{ this, registry_.view<Args...>() };
    }

    GameObject Get(entt::entity ent) {
        return GameObject{ registry_, ent };
    }

    template <typename T> GameObject GetFromComponent(T&& t) {
        return GameObject{ registry_, entt::to_entity(registry_, t) };
    }

    entt::registry& Registry() {
        return registry_;
    }

    const entt::registry& Registry() const {
        return registry_;
    }

    GameObject RayCast(const math::Ray& ray);

    // Camera.
    GameObject Camera();

    gfx::TextureViewRef SkyBox() const {
        return skyBox_;
    }

    void SetSkyBox(const gfx::TextureViewRef& texture) {
        skyBox_ = texture;
    }

    bool DynamicSky() const {
        return dynamicSky_;
    }

    void SetDynamicSky(bool sky) {
        dynamicSky_ = sky;
    }

    void SetSunLight(const GameObject& lightObject) {
        sunLight_ = lightObject;
    }

    const GameObject& SunLight() const {
        return sunLight_;
    }

    // TODO:
    bool LightShadingChanged() const {
        return lightShadingChanged_;
    }

    void SetLightShadingChanged(bool changed) {
        lightShadingChanged_ = changed;
    }

private:
    void AabbUpdate(entt::registry& reg, entt::entity ent);

    entt::registry registry_;
    std::vector<std::unique_ptr<System>> systems_;

    // Env.
    gfx::TextureViewRef skyBox_;
    bool dynamicSky_{};
    GameObject sunLight_;

    // TODO:
    gfx::GPUSemaphore computeSemaphore_;

    bool lightShadingChanged_{};
};

} // namespace ugine::core
