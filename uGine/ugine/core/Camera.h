﻿#pragma once

#include "Transformation.h"

#include <ugine/math/Frustum.h>

#include <glm/glm.hpp>

#include <memory>

namespace ugine::core {

class GameObject;

glm::mat4 ViewMatrixFromTransformation(const Transformation& trans, bool up = true);

class Camera {
public:
    enum class ProjectionType { Ortho, Perspective };

    Camera() = default;
    Camera(float left, float top, float right, float bottom, float nearZ, float farZ);
    Camera(float vFovDeg, float width, float height, float nearZ, float farZ);
    Camera(ProjectionType type, const glm::mat4& projectionMatrix, float nearZ, float farZ);

    static Camera Ortho(float left, float top, float right, float bottom, float nearZ, float farZ);
    static Camera Perspective(float vFovDeg, float width, float height, float nearZ, float farZ);
    static Camera Perspective(float leftTan, float rightTan, float topTan, float bottomTan, float nearZ, float farZ);

    ProjectionType Type() const {
        return type_;
    }

    const glm::mat4& ViewMatrix() const {
        return viewMatrix_;
    }

    const glm::mat4& ProjectionMatrix() const {
        return projectionMatrix_;
    }

    const glm::mat4 ViewProjectionMatrix() const {
        return projectionMatrix_ * viewMatrix_;
    }

    void SetViewMatrix(const glm::mat4& viewMatrix);

    glm::vec3 Position() const;
    glm::fquat Rotation() const;

    void SetSize(float width, float height);

    const math::Frustum& Frustum() const {
        return frustum_;
    }

    float Left() const {
        return left_;
    }
    float Right() const {
        return right_;
    }
    float Top() const {
        return top_;
    }
    float Bottom() const {
        return bottom_;
    }
    float Fov() const {
        return fov_;
    }
    float Width() const {
        return width_;
    }
    float Height() const {
        return height_;
    }
    float Near() const {
        return near_;
    }
    float Far() const {
        return far_;
    }

    size_t Hash() const {
        return hash_;
    }

private:
    void UpdateMatrix();
    void UpdateFrustum();
    void UpdateHash();

    ProjectionType type_;
    glm::mat4 projectionMatrix_;
    glm::mat4 viewMatrix_{ 1.0f };
    math::Frustum frustum_;

    float left_{};
    float right_{};
    float top_{};
    float bottom_{};
    float fov_{};
    float width_{};
    float height_{};
    float near_{};
    float far_{};

    size_t hash_{};
};

} // namespace ugine::core
