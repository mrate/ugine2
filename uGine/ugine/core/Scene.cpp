﻿#include "Scene.h"

#include <ugine/core/Component.h>
#include <ugine/core/EventService.h>

#include <ugine/gfx/rendering/MaterialInstance.h>
#include <ugine/gfx/rendering/Mesh.h>
#include <ugine/gfx/rendering/Terrain.h>
#include <ugine/script/ScriptSystem.h>
#include <ugine/utils/Log.h>
#include <ugine/utils/Profile.h>

#include <ugine/gfx/FrameStats.h>
#include <ugine/gfx/rendering/Foliage.h>

#include "systems/AnimationSystem.h"
#include "systems/CameraSystem.h"
#include "systems/LightSystem.h"
#include "systems/ParticleSystem.h"
#include "systems/TerrainSystem.h"

#include <ugine/physics/PhysicsSystem.h>

#include <ugine/utils/Debug.h>

namespace ugine::core {

Scene& Scene::FromRegistry(const entt::registry& reg) {
    auto scene{ reg.ctx<Scene*>() };

    UGINE_ASSERT(scene);

    return *scene;
}

Scene::Scene() {
    registry_.on_update<TransformationComponent>().connect<&Scene::AabbUpdate>(this);

    registry_.on_construct<MeshComponent>().connect<&Scene::AabbUpdate>(this);
    registry_.on_update<MeshComponent>().connect<&Scene::AabbUpdate>(this);
    registry_.on_construct<SkinnedMeshComponent>().connect<&Scene::AabbUpdate>(this);
    registry_.on_update<SkinnedMeshComponent>().connect<&Scene::AabbUpdate>(this);
    registry_.on_construct<TerrainComponent>().connect<&Scene::AabbUpdate>(this);
    registry_.on_update<TerrainComponent>().connect<&Scene::AabbUpdate>(this);
    registry_.on_construct<ParticleComponent>().connect<&Scene::AabbUpdate>(this);
    registry_.on_update<ParticleComponent>().connect<&Scene::AabbUpdate>(this);
    registry_.on_construct<FoliageComponent>().connect<&Scene::AabbUpdate>(this);
    registry_.on_update<FoliageComponent>().connect<&Scene::AabbUpdate>(this);

    gfx::GraphicsService::Device().InitSemaphore(computeSemaphore_);
}

Scene::~Scene() {
    registry_.on_update<TransformationComponent>().disconnect<&Scene::AabbUpdate>(this);

    registry_.on_construct<MeshComponent>().disconnect<&Scene::AabbUpdate>(this);
    registry_.on_update<MeshComponent>().disconnect<&Scene::AabbUpdate>(this);
    registry_.on_construct<SkinnedMeshComponent>().disconnect<&Scene::AabbUpdate>(this);
    registry_.on_update<SkinnedMeshComponent>().disconnect<&Scene::AabbUpdate>(this);
    registry_.on_construct<TerrainComponent>().disconnect<&Scene::AabbUpdate>(this);
    registry_.on_update<TerrainComponent>().disconnect<&Scene::AabbUpdate>(this);
    registry_.on_construct<ParticleComponent>().disconnect<&Scene::AabbUpdate>(this);
    registry_.on_update<ParticleComponent>().disconnect<&Scene::AabbUpdate>(this);
    registry_.on_construct<FoliageComponent>().disconnect<&Scene::AabbUpdate>(this);
    registry_.on_update<FoliageComponent>().disconnect<&Scene::AabbUpdate>(this);
}

GameObject Scene::CreateObject(const std::string& name) {
    return GameObject::Create(registry_, name);
}

void Scene::Update() {
    PROFILE_EVENT("Scene::Update");

    for (auto& system : systems_) {
        system->Update(registry_);
    }

    EventService::Trigger<events::SceneUpdate>(this);
}

void Scene::Compute() {
    PROFILE_EVENT("Scene::Compute");

    bool needCompute{ true };
    //for (const auto& system : systems_) {
    //    if (system->NeedCompute(registry_)) {
    //        needCompute = true;
    //        break;
    //    }
    //}

    if (needCompute) {
        using Clock = std::chrono::high_resolution_clock;
        auto frameStartTime{ Clock::now() };

        auto command{ gfx::GraphicsService::Device().BeginCommandList(gfx::Device::CommandType::Compute) };

        for (auto& system : systems_) {
            system->Compute(registry_, command);
        }

        gfx::GraphicsService::Device().SubmitCommandLists(nullptr, &computeSemaphore_);

        gfx::FrameStats::Instance().cpuComputeTimeMS
            = static_cast<float>(std::chrono::duration_cast<std::chrono::microseconds>(Clock::now() - frameStartTime).count()) / 1000.0f;
    } else {
        gfx::FrameStats::Instance().cpuComputeTimeMS = 0;
    }
}

void Scene::PreRender(gfx::GPUCommandList command) {
    PROFILE_EVENT("Scene::PreRender");

    for (auto& system : systems_) {
        system->PreRender(registry_, command);
    }
}

void Scene::Render(gfx::GPUCommandList command) {
    PROFILE_EVENT("Scene::Render");

    for (auto& system : systems_) {
        system->Render(registry_, command);
    }
}

void Scene::PostRender(gfx::GPUCommandList command) {
    PROFILE_EVENT("Scene::PostRender");

    for (auto& system : systems_) {
        system->PostRender(registry_, command);
    }
}

void Scene::Init() {
    registry_.set<Scene*>(this);

    systems_.emplace_back(std::make_unique<LightSystem>());
    systems_.emplace_back(std::make_unique<CameraSystem>());
    systems_.emplace_back(std::make_unique<AnimationSystem>());
    systems_.emplace_back(std::make_unique<ParticleSystem>());
    systems_.emplace_back(std::make_unique<physics::PhysicsSystem>());
    systems_.emplace_back(std::make_unique<script::NativeScriptSystem>());
    systems_.emplace_back(std::make_unique<TerrainSystem>());

#ifdef _DEBUG
    registry_.on_construct<utils::Debug>().connect<&utils::OnDebugAttach>();
    registry_.on_destroy<utils::Debug>().connect<&utils::OnDebugDetach>();
#endif

    for (auto& system : systems_) {
        system->Init(registry_);
    }
}

void Scene::Destroy() {
    registry_.each([&](const auto entity) { registry_.destroy(entity); });

    for (auto& system : systems_) {
        system->Destroy(registry_);
    }

    gfx::GraphicsService::Device().DestroySemaphore(computeSemaphore_);
    systems_.clear();
    skyBox_ = {};
}

GameObject Scene::RayCast(const math::Ray& ray) {
    entt::entity m{ entt::null };
    float minDistance{ -1 };

    auto compare = [&](auto ent, auto& t) {
        float distance{};
        if (math::Intersect(ray, t.aabb, &distance)) {
            if (minDistance < 0.0f || distance < minDistance) {
                minDistance = distance;
                m = ent;
            }
        }
    };

    registry_.view<MeshComponent>().each(compare);
    registry_.view<SkinnedMeshComponent>().each(compare);
    registry_.view<TerrainComponent>().each(compare);
    registry_.view<ParticleComponent>().each(compare);

    return m == entt::null ? GameObject{} : Get(m);
}

GameObject Scene::Camera() {
    for (auto ent : registry_.view<CameraComponent>()) {
        if (registry_.get<CameraComponent>(ent).isMain) {
            return Get(ent);
        }
    }

    return {};
}

void Scene::AabbUpdate(entt::registry& reg, entt::entity ent) {
    auto transformation{ Get(ent).GlobalTransformation() };
    auto matrix{ transformation.Matrix() };

    if (reg.has<MeshComponent>(ent)) {
        auto& comp{ reg.get<MeshComponent>(ent) };
        if (comp.meshMaterial) {
            comp.aabb = comp.meshMaterial->mesh->Aabb().Transform(matrix);
        }
    }

    if (reg.has<SkinnedMeshComponent>(ent)) {
        auto& comp{ reg.get<SkinnedMeshComponent>(ent) };
        if (comp.mesh) {
            comp.aabb = comp.mesh.Aabb().Transform(matrix);
        }
    }

    if (reg.has<TerrainComponent>(ent) && reg.has<gfx::Terrain>(ent)) {
        auto& comp{ reg.get<TerrainComponent>(ent) };
        const auto& terrain{ reg.get<gfx::Terrain>(ent) };
        comp.aabb = terrain.Aabb().Transform(matrix);
    }

    if (reg.has<ParticleComponent>(ent) && reg.has<gfx::Particles>(ent)) {
        auto& comp{ reg.get<ParticleComponent>(ent) };
        const auto& particles{ reg.get<gfx::Particles>(ent) };
        comp.aabb = particles.Aabb().Transform(matrix);
    }

    if (reg.has<FoliageComponent>(ent) && reg.has<gfx::Foliage>(ent)) {
        auto& comp{ reg.get<FoliageComponent>(ent) };
        const auto& foliage{ reg.get<gfx::Foliage>(ent) };
        comp.aabb = foliage.Aabb().Transform(matrix);
    }
}

} // namespace ugine::core
