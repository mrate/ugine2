﻿#pragma once

namespace ugine::core {
class Scene;
}

namespace ugine::core::events {

struct SceneUpdate {
    Scene* scene{};
};

} // namespace ugine::core::events
