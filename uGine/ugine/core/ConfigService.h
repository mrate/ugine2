﻿#pragma once

#include <ugine/gfx/Gfx.h>
#include <ugine/utils/Singleton.h>

#include <filesystem>

namespace ugine::core {

class ConfigService : public utils::Singleton<ConfigService> {
public:
    static ConfigService& Instance();

    static void Load(const std::filesystem::path& file);
    static void Save(const std::filesystem::path& file);

    static vk::Extent2D ShadowMapResolution();
    static void SetShadowMapResolution(vk::Extent2D resolution);

    static vk::Extent2D ShadowMapCubeResolution();
    static void SetShadowMapCubeResolution(vk::Extent2D resolution);

    static bool ShadowsEnabled();
    static void SetShadowEnabled(bool enabled);

    static vk::SampleCountFlagBits MsaaSamples();
    static void SetMsaaSamples(vk::SampleCountFlagBits samples);

    static float DirLightCsmSize(int csmLevel);
    static void SetDirLightCsmSize(int csmLevel, float size);

    static float VolumetricIntensity();
    static void SetVolumetricIntensity(float intensity);

    static float ShadowIntensity();
    static void SetShadowIntensity(float intensity);

    static int AnisotropicFilteringLevels();
    static void SetAnisotropicFilteringLevels(int levels);

    static float LightShaftsSunSize() {
        return Instance().lightShaftsSunSize_;
    }

    static float LightShaftsSize() {
        return Instance().lightShaftsSize_;
    }

    static float LightShaftsWeight() {
        return Instance().lightShaftsWeight_;
    }

    static float LightShaftsDecayFallof() {
        return Instance().lightShaftsDecayFallof_;
    }

    static float LightShaftsExposure() {
        return Instance().lightShaftsExposure_;
    }

    static void SetLightShaftsSunSize(float val) {
        Instance().lightShaftsSunSize_ = val;
    }

    static void SetLightShaftsSize(float val) {
        Instance().lightShaftsSize_ = val;
    }

    static void SetLightShaftsWeight(float val) {
        Instance().lightShaftsWeight_ = val;
    }

    static void SetLightShaftsDecayFallof(float val) {
        Instance().lightShaftsDecayFallof_ = val;
    }

    static void SetLightShaftsExposure(float val) {
        Instance().lightShaftsExposure_ = val;
    }

    static float LightShaftsBlurRadius() {
        return Instance().lightShaftsBlurRadius_;
    }
    static float LightShaftsBlurAmount() {
        return Instance().lightShaftsBlurAmount_;
    }
    static void SetLightShaftsBlurRadius(float v) {
        Instance().lightShaftsBlurRadius_ = v;
    }
    static void SetLightShaftsBlurAmount(float v) {
        Instance().lightShaftsBlurAmount_ = v;
    }

    static float SsaoStrength() {
        return Instance().ssaoStrength_;
    }
    static void SetSsaoStrength(float s) {
        Instance().ssaoStrength_ = s;
    }
    static float SsaoRadius() {
        return Instance().ssaoRadius_;
    }
    static void SetSsaoRadius(float s) {
        Instance().ssaoRadius_ = s;
    }
    static float SsaoBias() {
        return Instance().ssaoBias_;
    }
    static void SetSsaoBias(float s) {
        Instance().ssaoBias_ = s;
    }

    static uint32_t VertexBufferInitialSize();
    static uint32_t IndexBufferInitialSize();

    static int DefaultLodLevels();

private:
    ConfigService();

    vk::Extent2D shadowMapResolution_{ 1024, 1024 };
    vk::Extent2D shadowMapCubeResolution_{ 512, 512 };
    vk::SampleCountFlagBits msaa_{ vk::SampleCountFlagBits::e1 };

    bool shadowsEnabled_{ true };
    std::vector<float> dirLightCsmSize_;

    int anisotropicFilteringLevels_{ 16 };

    uint32_t vertexBufferInitialSize_{ 1 << 28 };
    uint32_t indexBufferInitialSize_{ 1 << 24 };

    float volumetricIntensity_{ 0.1f };
    float shadowIntensity_{ 0.75f };

    float lightShaftsSunSize_{ 5.0f };
    float lightShaftsSize_{ 0.65f };
    float lightShaftsWeight_{ 0.25f };
    float lightShaftsDecayFallof_{ 0.945f };
    float lightShaftsExposure_{ 0.2f };
    float lightShaftsBlurRadius_{ 0.2f };
    float lightShaftsBlurAmount_{ 0.2f };

    float ssaoStrength_{ 1.0f };
    float ssaoRadius_{ 0.4f };
    float ssaoBias_{ 0.000025f };
};

} // namespace ugine::core
