﻿#pragma once

#include <ugine/utils/Singleton.h>

#include <entt/entt.hpp>

namespace ugine::core {

class EventService : public utils::Singleton<EventService> {
public:
    template <typename Event, auto Listener, typename... Args> static void Register(Args&&... args) {
        Instance().dispatcher_.sink<Event>().connect<Listener>(std::forward<Args>(args)...);
    }

    template <typename Event, auto Listener, typename... Args> static void Unregister(Args&&... args) {
        Instance().dispatcher_.sink<Event>().disconnect<Listener>(std::forward<Args>(args)...);
    }

    template <typename Event, typename... Args> static void Trigger(Args&&... args) {
        Instance().dispatcher_.trigger<Event>(std::forward<Args>(args)...);
    }

private:
    entt::dispatcher dispatcher_;
};

} // namespace ugine::core
