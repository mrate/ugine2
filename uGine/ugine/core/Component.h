﻿#pragma once

#include <ugine/core/Camera.h>
#include <ugine/core/Transformation.h>
#include <ugine/gfx/core/RenderTarget.h>
#include <ugine/gfx/postprocess/PostprocessChain.h>
#include <ugine/gfx/rendering/Foliage.h>
#include <ugine/gfx/rendering/FoliageDesc.h>
#include <ugine/gfx/rendering/Light.h>
#include <ugine/gfx/rendering/MaterialInstance.h>
#include <ugine/gfx/rendering/Mesh.h>
#include <ugine/gfx/rendering/MeshMaterial.h>
#include <ugine/gfx/rendering/Particles.h>
#include <ugine/gfx/rendering/SkinnedMesh.h>

#include <array>
#include <memory>

namespace ugine::gfx {
class TextureView;
}

namespace ugine::script {
class NativeScript;
}

namespace ugine::core {

class GameObject;

struct CameraComponent {
    Camera camera;
    bool isMain{};
    vk::SampleCountFlagBits samples{ vk::SampleCountFlagBits::e1 };
    gfx::PostprocessChain postprocess{};
    glm::mat4 viewProjectionPrev{};
    // Render targets.
    gfx::RenderTarget renderTarget{};
    gfx::RenderTarget depthBuffer{};
    gfx::RenderTarget volumetricLightRT;
    gfx::RenderTarget sunRTstencil;
    gfx::RenderTarget sunRToutput;
    gfx::RenderTarget aoRTtmp;
    gfx::RenderTarget aoRT;
    gfx::TextureViewRef sceneCopy;
    gfx::TextureViewRef depthCopy;
    gfx::TextureViewRef volumetricUpscale;
};

struct TransformationComponent {
    Transformation transformation{};
};

struct MeshComponent {
    gfx::MeshMaterialRef meshMaterial;

    bool castsShadows{ true };
    int lod{ -1 };
    bool render{ true };
    //
    math::AABB aabb;
    int debugSubmesh{ -1 };
};

struct SkinnedMeshComponent {
    gfx::SkinnedMesh mesh;
    bool castsShadows{ true };
    int lod{ -1 };
    bool render{ true };
    //
    math::AABB aabb;
};

struct AnimatorComponent {
    uint32_t animation{};
    float animationTime{};
    bool isRunning{ false };
    float speed{ 1.0f };
    float resolution{ 30.0f }; // 30ms ~ 30fps.
    float lastUpdateTime{};

    std::vector<gfx::AnimationData> animations;
};

struct NativeScriptComponent {
    void Add(const GameObject& go, const std::shared_ptr<script::NativeScript>& script);
    void Remove(const GameObject& go, const std::shared_ptr<script::NativeScript>& script);

    const std::vector<std::shared_ptr<script::NativeScript>>& Scripts() const {
        return scripts_;
    }

private:
    std::vector<std::shared_ptr<script::NativeScript>> scripts_;
};

struct LightComponent {
    gfx::Light light;
    bool isCsm{};
    bool generatesShadows{};
    bool volumetric{};
    bool flare{};
    gfx::TextureViewRef flareTexture;
    glm::vec2 flareSize{ 0.25f, 0.25f };
    float flareFade{ 0.001f };
    // TODO: Make other component.
    std::array<Camera, gfx::limits::CSM_LEVELS> shadowMapCamera;

    // TODO: For volumetric lights...
    uint32_t lightId{};
};

struct ParticleComponent {
    uint32_t count{ 1 };
    gfx::TextureViewRef texture{};
    gfx::Color color{};
    glm::vec2 size{ 1.0f };
    float life{ 1.0f };
    float lifeSpan{ 0.1f };
    float intensity{ 1.0f };
    glm::vec3 gravity{};
    glm::vec3 speed{};
    glm::vec3 startOffset{};
    glm::vec3 speedOffset{};
    uint32_t emitCount{ 0 };
    bool mesh{};

    //
    math::AABB aabb;
};

struct RigidBodyComponent {
    enum class Shape {
        Box,
        Sphere,
        Plane,
    };

    Shape shape{ Shape::Box };
    glm::vec3 scale{ 1.0f };
    glm::vec3 offset{ 0.0f };
    glm::vec3 linearVelocity{};
    glm::vec3 angularVelocity{};
    glm::vec3 force{};
    glm::vec3 torque{};
    bool isDynamic{};
    bool isKinematic{};

    void AddForce(const glm::vec3& v) {
        force += v;
    }

    void AddTorque(const glm::vec3& v) {
        torque += v;
    }
};

struct TerrainComponent {
    std::filesystem::path heightMap;
    gfx::TextureViewRef layers;
    gfx::TextureViewRef splat;
    glm::vec2 size{ 64, 64 };
    float heightScale{ 1.0f };

    //
    math::AABB aabb;
};

struct FoliageComponent {
    static constexpr int MAX_LAYERS{ 8 };

    struct Layer {
        uint32_t count;
        gfx::FoliageRef foliage;
        bool shadows{ false };
    };

    uint32_t layerCount{ 0 };
    std::array<Layer, MAX_LAYERS> layers;

    //
    std::array<uint32_t, MAX_LAYERS> realCount{};
    math::AABB aabb;
};

} // namespace ugine::core
