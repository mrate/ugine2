﻿#include "GameObject.h"
#include "Scene.h"

namespace ugine::core {

namespace {
    template <typename T> void Copy(const GameObject& src, GameObject& dst) {
        if (src.Has<T>()) {
            dst.CreateComponent<T>(src.Component<T>());
        }
    }

    template <> void Copy<SkinnedMeshComponent>(const GameObject& src, GameObject& dst) {
        // TODO:
    }

    template <> void Copy<CameraComponent>(const GameObject& src, GameObject& dst) {
        if (src.Has<CameraComponent>()) {
            const auto& c{ src.Component<CameraComponent>() };

            CameraComponent newCamera{};
            newCamera.camera = c.camera;
            newCamera.isMain = false;
            newCamera.samples = c.samples;
            // TODO: Postprocess chain.

            dst.CreateComponent<CameraComponent>(std::move(newCamera));
        }
    }
} // namespace

GameObject GameObject::Create(entt::registry& registry, const std::string& name) {
    auto go{ registry.create() };
    registry.emplace<Tag>(go, name);
    registry.emplace<Relationship>(go);
    registry.emplace<TransformationComponent>(go);

    return GameObject{ registry, go };
}

void GameObject::Destroy(const GameObject& go) {
    for (auto child{ go.FirstChild() }; child.IsValid();) {
        auto tmp{ child };
        child = child.NextSibling();

        if (tmp.Parent().IsValid()) {
            tmp.Parent().RemoveChild(tmp);
        }

        Destroy(tmp);
    }

    if (go.Parent().IsValid()) {
        go.Parent().RemoveChild(go);
    }

    // TODO:
    if (go.Scene()->SunLight().Entity() == go.Entity()) {
        go.handle_.registry()->ctx<core::Scene*>()->SetSunLight({});
    }

    go.handle_.registry()->destroy(go.Entity());
}

void GameObject::AddChild(entt::entity child) {
    auto childGO{ Scene()->Get(child) };

    auto& rel{ Component<Relationship>() };
    auto& childRel{ childGO.Component<Relationship>() };
    auto childGlob{ childGO.GlobalTransformation() };

    UGINE_ASSERT(!handle_.registry()->has<core::Parent>(child));
    UGINE_ASSERT(childRel.prevSibling == entt::null);
    UGINE_ASSERT(childRel.nextSibling == entt::null);

    handle_.registry()->emplace<core::Parent>(child, Entity());

    if (rel.children == 0) {
        rel.fistChild = rel.lastChild = child;
    } else {
        auto& lastChildRel{ handle_.registry()->get<Relationship>(rel.lastChild) };
        childRel.prevSibling = rel.lastChild;
        lastChildRel.nextSibling = child;
        rel.lastChild = child;
    }

    ++rel.children;

    childGO.SetGlobalTransformation(childGlob);
}

void GameObject::RemoveChild(entt::entity child) {
    auto childGO{ Scene()->Get(child) };

    auto& rel{ Component<Relationship>() };
    auto& childRel{ childGO.Component<Relationship>() };

    auto childGlob{ childGO.GlobalTransformation() };

    UGINE_ASSERT(handle_.registry()->get<core::Parent>(child).parent == Entity());
    UGINE_ASSERT(rel.children > 0);

    if (rel.fistChild == child) {
        rel.fistChild = childRel.nextSibling;
    }

    if (rel.lastChild == child) {
        rel.lastChild = childRel.prevSibling;
    }

    if (childRel.prevSibling != entt::null) {
        auto& prevRel{ handle_.registry()->get<Relationship>(childRel.prevSibling) };
        prevRel.nextSibling = childRel.nextSibling;
    }

    if (childRel.nextSibling != entt::null) {
        auto& nextRel{ handle_.registry()->get<Relationship>(childRel.nextSibling) };
        nextRel.prevSibling = childRel.prevSibling;
    }

    childRel.prevSibling = childRel.nextSibling = entt::null;
    childGO.RemoveComponent<core::Parent>();

    --rel.children;

    childGO.SetGlobalTransformation(childGlob);
}

GameObject GameObject::Clone() {
    return CloneImpl(true);
}

GameObject GameObject::CloneImpl(bool isRoot) {
    auto newGo{ Scene()->CreateObject(Name()) };
    newGo.SetGlobalTransformation(GlobalTransformation());

    // Copy components.
    Copy<CameraComponent>(*this, newGo);
    Copy<MeshComponent>(*this, newGo);
    Copy<SkinnedMeshComponent>(*this, newGo);
    Copy<AnimatorComponent>(*this, newGo);
    Copy<NativeScriptComponent>(*this, newGo);
    Copy<LightComponent>(*this, newGo);
    Copy<ParticleComponent>(*this, newGo);
    Copy<RigidBodyComponent>(*this, newGo);
    Copy<TerrainComponent>(*this, newGo);
    Copy<FoliageComponent>(*this, newGo);

    for (auto child{ FirstChild() }; child.IsValid(); child = child.NextSibling()) {
        newGo.AddChild(child.CloneImpl(false));
    }

    if (isRoot) {
        auto parent{ Parent() };
        if (parent) {
            parent.AddChild(newGo);
        }
    }

    return newGo;
}

bool operator==(const GameObject& a, const GameObject& b) {
    return (!a && !b) || (a.Entity() == b.Entity());
}

bool operator!=(const GameObject& a, const GameObject& b) {
    return !(a == b);
}

} // namespace ugine::core
