﻿#include "CoreService.h"

#include <ugine/gfx/FrameStats.h>
#include <ugine/gfx/core/GpuProfiler.h>
#include <ugine/utils/MemAllocator.h>

#pragma comment(lib, "enkiTS.lib")

namespace ugine::core {

CoreService& CoreService::Instance() {
    static CoreService inst;
    return inst;
}

void CoreService::Init(std::chrono::milliseconds maxDelta) {
    Instance().mainThreadId_ = std::this_thread::get_id();

    Instance().startTime_ = Clock::now();
    Instance().frameTime_ = Clock::now();
    Instance().mainAllocator_ = std::make_unique<utils::MemAllocator>(256 * 1024 * 1024);
    Instance().stlAllocator_ = std::make_unique<utils::MemAllocator>(2 * 256 * 1024 * 1024);
    Instance().maxDeltaMicros_ = static_cast<float>(std::chrono::duration_cast<std::chrono::microseconds>(maxDelta).count());
}

void CoreService::Destroy() {
    Instance().mainAllocator_ = nullptr;
    Instance().stlAllocator_ = nullptr;
}

bool CoreService::IsMainThread() {
    return std::this_thread::get_id() == Instance().mainThreadId_;
}

bool CoreService::PhysicsActive() {
    return Instance().physicsActive_;
}

void CoreService::SetPhysicsActive(bool active) {
    Instance().physicsActive_ = active;
}

CoreService::Clock::time_point CoreService::FrameTime() {
    return Instance().frameTime_;
}

uint64_t CoreService::Time() {
    return static_cast<uint64_t>(std::chrono::duration_cast<std::chrono::milliseconds>(Instance().frameTime_ - Instance().startTime_).count());
}

float CoreService::TimeDiffMS() {
    return Instance().timeDiffMS_;
}

float CoreService::TimeDiffS() {
    return Instance().timeDiffS_;
}

uint64_t CoreService::Frame() {
    return Instance().frameNum_;
}

void CoreService::BeginFrame() {
    // Frame data.
    Instance().frameNum_ = Instance().frameNum_ + 1;
    Instance().lastFrameTime_ = Instance().frameTime_;
    Instance().frameTime_ = Clock::now();
    float micros{ static_cast<float>(std::chrono::duration_cast<std::chrono::microseconds>(Instance().frameTime_ - Instance().lastFrameTime_).count()) };
    micros = std::min<float>(micros, Instance().maxDeltaMicros_);

    Instance().timeDiffMS_ = static_cast<float>(micros / 1000.0f);
    Instance().timeDiffS_ = static_cast<float>(micros / 1000000.0f);
}

utils::MemAllocator& CoreService::MainAllocator() {
    return *Instance().mainAllocator_;
}

utils::MemAllocator& CoreService::StlAllocator() {
    return *Instance().stlAllocator_;
}

CoreService::CoreService() {}

} // namespace ugine::core
