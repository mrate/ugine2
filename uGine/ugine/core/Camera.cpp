﻿#include "Camera.h"

#include <ugine/core/GameObject.h>
#include <ugine/core/Transformation.h>
#include <ugine/utils/Hash.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

namespace ugine::core {

glm::mat4 ViewMatrixFromTransformation(const Transformation& trans, bool up) {
    return up ? glm::inverse(trans.Matrix()) : glm::lookAtRH(trans.position, trans.position + trans.rotation * core::FORWARD, trans.rotation * core::UP);
}

Camera Camera::Ortho(float left, float top, float right, float bottom, float nearZ, float farZ) {
    return Camera(left, top, right, bottom, nearZ, farZ);
}

Camera Camera::Perspective(float vFovDeg, float width, float height, float nearZ, float farZ) {
    return Camera(vFovDeg, width, height, nearZ, farZ);
}

Camera Camera::Perspective(float leftTan, float rightTan, float topTan, float bottomTan, float nearZ, float farZ) {
    auto projectionMatrix{ glm::frustum(leftTan * nearZ, rightTan * nearZ, bottomTan * nearZ, topTan * nearZ, nearZ, farZ) };
    return Camera(ProjectionType::Perspective, projectionMatrix, nearZ, farZ);
}

void Camera::SetViewMatrix(const glm::mat4& viewMatrix) {
    viewMatrix_ = viewMatrix;
    UpdateFrustum();
}

glm::vec3 Camera::Position() const {
    return core::Transformation(glm::inverse(viewMatrix_)).position;
}

glm::fquat Camera::Rotation() const {
    return core::Transformation(glm::inverse(viewMatrix_)).rotation;
}

void Camera::SetSize(float width, float height) {
    if (type_ == ProjectionType::Perspective) {
        width_ = width;
        height_ = height;
    } else {
        right_ = left_ + width;
        top_ = bottom_ + height;
    }

    UpdateMatrix();
}

void Camera::UpdateMatrix() {
    if (type_ == ProjectionType::Perspective) {
        projectionMatrix_ = glm::perspectiveFovRH(glm::radians(fov_), width_, height_, near_, far_);
    } else {
        projectionMatrix_ = glm::ortho(left_, right_, bottom_, top_, near_, far_);
    }
    UpdateFrustum();
}

ugine::core::Camera::Camera(float left, float top, float right, float bottom, float nearZ, float farZ)
    : left_(left)
    , top_(top)
    , right_(right)
    , bottom_(bottom)
    , near_(nearZ)
    , far_(farZ)
    , type_(ProjectionType::Ortho) {
    projectionMatrix_ = glm::ortho(left, right, bottom, top, nearZ, farZ);
    UpdateFrustum();
}

ugine::core::Camera::Camera(float vFovDeg, float width, float height, float nearZ, float farZ)
    : fov_(vFovDeg)
    , width_(width)
    , height_(height)
    , near_(nearZ)
    , far_(farZ)
    , type_(ProjectionType::Perspective) {
    projectionMatrix_ = glm::perspectiveFovRH(glm::radians(vFovDeg), width, height, nearZ, farZ);
    UpdateFrustum();
}

Camera::Camera(ProjectionType type, const glm::mat4& projectionMatrix, float nearZ, float farZ)
    : type_(type)
    , projectionMatrix_(projectionMatrix)
    , near_(nearZ)
    , far_(farZ) {
    UpdateFrustum();
}

void Camera::UpdateFrustum() {
    enum { LEFT = 0, RIGHT, TOP, BOTTOM, BACK, FRONT };

    auto matrix{ ProjectionMatrix() * ViewMatrix() };
    UpdateHash();

    frustum_.planes[LEFT].x = matrix[0].w + matrix[0].x;
    frustum_.planes[LEFT].y = matrix[1].w + matrix[1].x;
    frustum_.planes[LEFT].z = matrix[2].w + matrix[2].x;
    frustum_.planes[LEFT].w = matrix[3].w + matrix[3].x;

    frustum_.planes[RIGHT].x = matrix[0].w - matrix[0].x;
    frustum_.planes[RIGHT].y = matrix[1].w - matrix[1].x;
    frustum_.planes[RIGHT].z = matrix[2].w - matrix[2].x;
    frustum_.planes[RIGHT].w = matrix[3].w - matrix[3].x;

    frustum_.planes[TOP].x = matrix[0].w - matrix[0].y;
    frustum_.planes[TOP].y = matrix[1].w - matrix[1].y;
    frustum_.planes[TOP].z = matrix[2].w - matrix[2].y;
    frustum_.planes[TOP].w = matrix[3].w - matrix[3].y;

    frustum_.planes[BOTTOM].x = matrix[0].w + matrix[0].y;
    frustum_.planes[BOTTOM].y = matrix[1].w + matrix[1].y;
    frustum_.planes[BOTTOM].z = matrix[2].w + matrix[2].y;
    frustum_.planes[BOTTOM].w = matrix[3].w + matrix[3].y;

    frustum_.planes[BACK].x = matrix[0].w + matrix[0].z;
    frustum_.planes[BACK].y = matrix[1].w + matrix[1].z;
    frustum_.planes[BACK].z = matrix[2].w + matrix[2].z;
    frustum_.planes[BACK].w = matrix[3].w + matrix[3].z;

    frustum_.planes[FRONT].x = matrix[0].w - matrix[0].z;
    frustum_.planes[FRONT].y = matrix[1].w - matrix[1].z;
    frustum_.planes[FRONT].z = matrix[2].w - matrix[2].z;
    frustum_.planes[FRONT].w = matrix[3].w - matrix[3].z;

    for (auto i = 0; i < frustum_.planes.size(); i++) {
        glm::normalize(frustum_.planes[i]);
        //float length = sqrtf(planes[i].x * planes[i].x + planes[i].y * planes[i].y + planes[i].z * planes[i].z);
        //planes[i] /= length;
    }
}

void Camera::UpdateHash() {
    hash_ = 0;
    utils::HashCombine(hash_, projectionMatrix_);
    utils::HashCombine(hash_, viewMatrix_);
}

} // namespace ugine::core
