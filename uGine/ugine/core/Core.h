﻿#pragma once

#include <glm/glm.hpp>

namespace ugine::core {

constexpr glm::vec3 FORWARD{ 0.0f, 0.0f, -1.0f };
constexpr glm::vec3 UP{ 0.0f, 1.0f, 0.0f };

} // namespace ugine::core
