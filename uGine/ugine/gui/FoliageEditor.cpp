﻿#include "FoliageEditor.h"

#include "PropertyTable.h"
#include "Utils.h"

#include <ugine/gfx/rendering/Foliage.h>

namespace ugine::gui {

void FoliageEditor::Populate(gfx::FoliageDesc& foliage) {
    //scope::Indent ident;

    {
        PropertyTable table{ "Foliage" };

        table.EditProperty("Name", foliage.name);

        int levels{ static_cast<int>(foliage.lodLevels.size()) };
        if (table.EditProperty("Level of details", levels, 1, gfx::Foliage::MAX_LOD)) {
            foliage.lodLevels.resize(levels);
        }

        table.EditProperty("Min. distance", foliage.minDistance);
    }

    int flags{ ImGuiTreeNodeFlags_DefaultOpen };

    int index{};
    for (auto& lodLevel : foliage.lodLevels) {
        scope::Id id{ index };

        auto name{ "Lod " + std::to_string(index + 1) };
        if (ImGui::CollapsingHeader("Lod ", flags)) {
            PropertyTable table{ name };

            table.BeginProperty("Mesh material");
            auto meshMat{ lodLevel.meshMaterial };
            if (LoadMeshMaterial(*gui_, *images_, meshMat)) {
                lodLevel.meshMaterial = meshMat;
            }
            table.EndProperty();

            table.EditProperty("Maximum distance", lodLevel.maxDistance);
        }
    }
}

} // namespace ugine::gui
