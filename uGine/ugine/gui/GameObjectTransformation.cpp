﻿#include "GameObjectTransformation.h"
#include "Utils.h"

#include <ugine/utils/Log.h>

#include <glm/glm.hpp>

#include <imgui.h>

#include <ImGuizmo.h>

namespace ugine::gui {

bool GameObjectTransformation::Render(
    glm::mat4& deltaTransformation, const core::Camera& camera, core::GameObject& go, Operation operation, std::optional<float> snap, bool local) {
    if (!go.Has<core::TransformationComponent>()) {
        return {};
    }

    float cameraView[16];
    MatrixToFloatArray(camera.ViewMatrix(), cameraView);

    float cameraProjection[16];
    MatrixToFloatArray(camera.ProjectionMatrix(), cameraProjection);

    float matrix[16];
    if (local) {
        MatrixToFloatArray(go.LocalTransformation().Matrix(), matrix);
    } else {
        MatrixToFloatArray(go.GlobalTransformation().Matrix(), matrix);
    }

    ImGuizmo::OPERATION op{ ImGuizmo::TRANSLATE };
    switch (operation) {
    case Operation::Translate:
        op = ImGuizmo::TRANSLATE;
        break;
    case Operation::Rotate:
        op = ImGuizmo::ROTATE;
        break;
    case Operation::Scale:
        op = ImGuizmo::SCALE;
        break;
    case Operation::Bounds:
        op = ImGuizmo::BOUNDS;
        break;
    }

    float* snapValue{ snap.has_value() ? &snap.value() : nullptr };
    float deltaMatrix[16];

    if (ImGuizmo::Manipulate(cameraView, cameraProjection, op, local ? ImGuizmo::LOCAL : ImGuizmo::WORLD, matrix, deltaMatrix, snapValue, nullptr, nullptr)) {
        FloatArrayToMatrix(deltaMatrix, deltaTransformation);
        return true;
    }

    return false;
}

bool GameObjectTransformation::WantCapture() {
    return ImGuizmo::IsOver() || ImGuizmo::IsUsing();
}

void GameObjectTransformation::SetRect(float x, float y, float width, float height) {
    ImGuizmo::SetRect(x, y, width, height);
}

} // namespace ugine::gui
