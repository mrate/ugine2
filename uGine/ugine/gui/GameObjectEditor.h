﻿#pragma once

#include <ugine/core/Component.h>
#include <ugine/core/GameObject.h>

#include "PostprocessEditor.h"
#include "Utils.h"

namespace ugine::gui {

class Gui;
class GuiImages;

class GameObjectEditor {
public:
    entt::delegate<void(core::GameObject, core::MeshComponent&, const std::string&)> OnLoadMesh;

    void SetGui(const Gui* gui) {
        gui_ = gui;
    }

    void SetImages(const GuiImages* images) {
        images_ = images;
    }

    void Populate(core::GameObject& go, bool open = true) const;

private:
    template <typename T> void RenderComponent(core::GameObject& go, const char* label, int flags) const {
        if (go.Has<T>()) {
            auto visible{ true };
            if (ImGui::CollapsingHeader(label, &visible, flags)) {
                scope::Indent i;
                Populate(go, go.Component<T>());
            }
            if (!visible) {
                go.RemoveComponent<T>();
            }
        }
    }

    void Populate(core::GameObject& go, const core::Tag& tag) const;
    void Populate(core::GameObject& go, const core::TransformationComponent& trans) const;
    void Populate(core::GameObject& go, const core::CameraComponent& camera) const;
    void Populate(core::GameObject& go, const core::MeshComponent& mesh) const;
    void Populate(core::GameObject& go, const core::SkinnedMeshComponent& script) const;
    void Populate(core::GameObject& go, const core::NativeScriptComponent& script) const;
    void Populate(core::GameObject& go, const core::LightComponent& script) const;
    void Populate(core::GameObject& go, const core::AnimatorComponent& animator) const;
    void Populate(core::GameObject& go, const core::ParticleComponent& particle) const;
    void Populate(core::GameObject& go, const core::RigidBodyComponent& body) const;
    void Populate(core::GameObject& go, const core::TerrainComponent& terrain) const;
    void Populate(core::GameObject& go, const core::FoliageComponent& foliage) const;

    const Gui* gui_{};
    const GuiImages* images_{};
    PostprocessEditor psEditor_{};
};

} // namespace ugine::gui
