﻿#pragma once

#include <ugine/gfx/rendering/FoliageDesc.h>

namespace ugine::gui {

class Gui;
class GuiImages;

class FoliageEditor {
public:
    void SetGui(const Gui* gui) {
        gui_ = gui;
    }

    void SetImages(const GuiImages* images) {
        images_ = images;
    }

    void Populate(gfx::FoliageDesc& foliage);

private:
    const Gui* gui_{};
    const GuiImages* images_{};
};

} // namespace ugine::gui
