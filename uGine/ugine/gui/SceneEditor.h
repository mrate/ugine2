﻿#pragma once

#include <ugine/core/Scene.h>

#include <ugine/gfx/Gfx.h>

#include <entt/entt.hpp>

#include <imgui.h>

#include <filesystem>
#include <functional>
#include <memory>

namespace ugine::gui {

class Gui;
class GuiImages;

struct GuiFlags {
    enum Flags {
        Selected = 1 << 0,
    };

    uint32_t flags{};
};

class SceneEditor {
public:
    entt::delegate<void(core::GameObject)> OnClicked;

    void SetGui(const Gui* gui) {
        gui_ = gui;
    }

    void SetImages(const GuiImages* images) {
        images_ = images;
    }

    void PopulateGameObjects(core::Scene& scene) const;
    void PopulateSceneSettings(core::Scene& scene) const;

private:
    void PopulateGameObject(core::GameObject& go, int& id) const;
    std::string PopulateIcons(const core::GameObject& go) const;

    const Gui* gui_{};
    const GuiImages* images_{};
};

} // namespace ugine::gui
