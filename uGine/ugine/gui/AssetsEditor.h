﻿#pragma once

#include <ugine/gfx/rendering/FoliageDesc.h>
#include <ugine/gfx/rendering/Material.h>
#include <ugine/gfx/rendering/MaterialInstance.h>

namespace ugine::gui {

class Gui;
class GuiImages;

class AssetsEditor {
public:
    void SetGui(const Gui* gui) {
        gui_ = gui;
    }

    void SetImages(const GuiImages* images) {
        images_ = images;
    }

    void PopulateAssets();

    gfx::MaterialInstanceID SelectedInstance() const {
        return selectedInstance_;
    }

    void SetSelectedInstance(gfx::MaterialInstanceID inst) {
        selectedInstance_ = inst;
        selection_ = Selection::MaterialInstances;
    }

    gfx::MaterialID SelectedMaterial() const {
        return selectedInstance_;
    }

    void SetSelectedMaterial(gfx::MaterialID inst) {
        selectedMaterial_ = inst;
        selection_ = Selection::Materials;
    }

    gfx::FoliageID SelectedFoliage() const {
        return selectedFoliage_;
    }

    void SetSelectedFoliage(gfx::FoliageID id) {
        selectedFoliage_ = id;
        selection_ = Selection::Foliage;
    }

    gfx::MeshMaterialID SelectedMeshMaterial() const {
        return selectedMeshMaterial_;
    }

    void SetSelectedMeshMaterial(gfx::MeshMaterialID id) {
        selectedMeshMaterial_ = id;
        selection_ = Selection::MeshMaterials;
    }

    void SetIconSize(float width, float height) {
        iconWidth_ = width;
        iconHeight_ = height;
    }

private:
    enum class Selection {
        None,
        Materials,
        MaterialInstances,
        Foliage,
        Meshes,
        MeshMaterials,
    };

    void PopulateMaterials();
    void PopulateMaterialInstances();
    void PopulateFoliages();
    void PopulateMeshes();
    void PopulateMeshMaterials();

    const Gui* gui_{};
    const GuiImages* images_{};

    Selection selection_{ Selection::MaterialInstances };

    gfx::MaterialID selectedMaterial_{ gfx::Materials::Invalid };
    gfx::MaterialInstanceID selectedInstance_{ gfx::MaterialInstances::Invalid };
    gfx::FoliageID selectedFoliage_{ gfx::Foliages::Invalid };
    gfx::MeshID selectedMesh_{ gfx::Meshes::Invalid };
    gfx::MeshMaterialID selectedMeshMaterial_{ gfx::MeshMaterials::Invalid };

    float iconWidth_{ 64.0f };
    float iconHeight_{ 64.0f };
};

} // namespace ugine::gui
