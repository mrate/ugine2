#include "DragAndDrop.h"

namespace ugine::gui {

void DragAndDrop::CanDragGameObject(const core::GameObject& go) {
    Drag(go, Type::GameObject, go_, PAYLOAD_GO, go.Name().c_str());
}

bool DragAndDrop::DropGameObject(core::GameObject& go) {
    return Drop(PAYLOAD_GO, go, go_);
}

void DragAndDrop::CanDragMaterialInstance(const gfx::MaterialInstanceRef& mat) {
    Drag(mat, Type::MaterialInstance, materialInst_, PAYLOAD_MATERIAL_INSTANCE, mat->Name().c_str());
}

bool DragAndDrop::DropMaterialInstance(gfx::MaterialInstanceRef& mat) {
    return Drop(PAYLOAD_MATERIAL_INSTANCE, mat, materialInst_);
}

void DragAndDrop::CanDragMeshMaterial(const gfx::MeshMaterialRef& mat) {
    Drag(mat, Type::MeshMaterial, meshMaterial_, PAYLOAD_MESH_MATERIAL, mat->mesh->Name().c_str());
}

bool DragAndDrop::DropMeshMaterial(gfx::MeshMaterialRef& mat) {
    return Drop(PAYLOAD_MESH_MATERIAL, mat, meshMaterial_);
}

void DragAndDrop::CanDragFoliage(const gfx::FoliageRef& foliage) {
    Drag(foliage, Type::Foliage, foliage_, PAYLOAD_FOLIAGE, foliage->name.c_str());
}

bool DragAndDrop::DropFoliage(gfx::FoliageRef& foliage) {
    return Drop(PAYLOAD_FOLIAGE, foliage, foliage_);
}

void DragAndDrop::CanDragMesh(const gfx::MeshRef& mesh) {
    Drag(mesh, Type::Mesh, mesh_, PAYLOAD_MESH, mesh->Name().c_str());
}

bool DragAndDrop::DropMesh(gfx::MeshRef& mesh) {
    return Drop(PAYLOAD_MESH, mesh, mesh_);
}

void DragAndDrop::Drop() {
    switch (type_) {
    case Type::GameObject:
        go_ = {};
        break;
    case Type::MaterialInstance:
        materialInst_ = {};
        break;
    case Type::MeshMaterial:
        meshMaterial_ = {};
        break;
    case Type::Foliage:
        foliage_ = {};
        break;
    case Type::Mesh:
        mesh_ = {};
        break;
    }

    type_ = Type::None;
}

} // namespace ugine::gui