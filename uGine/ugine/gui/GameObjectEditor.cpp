﻿#include "GameObjectEditor.h"

#include "Gui.h"
#include "GuiImages.h"
#include "PropertyTable.h"

#include <ugine/assets/AssetService.h>
#include <ugine/core/Camera.h>
#include <ugine/core/ConfigService.h>

#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/data/Shapes.h>
#include <ugine/gfx/import/MaterialImporter.h>
#include <ugine/gfx/import/MeshLoader.h>
#include <ugine/gfx/rendering/MaterialHelpers.h>
#include <ugine/gfx/rendering/MaterialInstance.h>
#include <ugine/gfx/rendering/Mesh.h>
#include <ugine/utils/ResourceManagement.h>

#include <ugine/script/NativeScript.h>
#include <ugine/script/NativeScripts.h>

#define EDIT_PROPERTY(table, name, type, component, member, ...)                                                                                               \
    {                                                                                                                                                          \
        auto member{ component.member };                                                                                                                       \
        if (table.EditProperty(name, member, __VA_ARGS__)) {                                                                                                   \
            go.Patch<type>([&](auto& c) { c.member = member; });                                                                                               \
        }                                                                                                                                                      \
    }

#define EDIT_PROPERTY_COMBO(table, name, type, component, member, names)                                                                                       \
    {                                                                                                                                                          \
        auto member{ component.member };                                                                                                                       \
        if (table.EditPropertyCombo(name, member, names)) {                                                                                                    \
            go.Patch<type>([&](auto& c) { c.member = member; });                                                                                               \
        }                                                                                                                                                      \
    }

namespace {

using namespace ugine;
using namespace ugine::core;
using namespace ugine::gfx;
using namespace ugine::gui;

std::map<const char*, ugine::core::Camera::ProjectionType> CameraTypes = {
    { "Perspective", ugine::core::Camera::ProjectionType::Perspective },
    { "Ortho", ugine::core::Camera::ProjectionType::Ortho },
};

std::map<const char*, ugine::gfx::Light::Type> LightTypes = {
    { "Directional", ugine::gfx::Light::Type::Directional },
    { "Spot", ugine::gfx::Light::Type::Spot },
    { "Point", ugine::gfx::Light::Type::Point },
};

std::map<const char*, vk::SampleCountFlagBits> Samples = {
    { "1x", vk::SampleCountFlagBits::e1 },
    { "2x", vk::SampleCountFlagBits::e2 },
    { "4x", vk::SampleCountFlagBits::e4 },
    { "8x", vk::SampleCountFlagBits::e8 },
    { "16x", vk::SampleCountFlagBits::e16 },
};

std::map<const char*, RigidBodyComponent::Shape> RigidBodyShapes = {
    { "Box", RigidBodyComponent::Shape::Box },
    { "Sphere", RigidBodyComponent::Shape::Sphere },
    { "Plane", RigidBodyComponent::Shape::Plane },
}; // namespace

template <typename T> void AddComponent(core::GameObject& go, const char* name) {
    if (!go.Has<T>()) {
        if (ImGui::Selectable(name)) {
            go.CreateComponent<T>();
        }
    }
}

template <typename T, typename F> void AddComponent(core::GameObject& go, const char* name, F&& f) {
    if (!go.Has<T>()) {
        if (ImGui::Selectable(name)) {
            f();
        }
    }
}

template <typename T> void RemoveComponent(core::GameObject& go, const char* name) {
    if (go.Has<T>()) {
        if (ImGui::Selectable(name)) {
            go.RemoveComponent<T>();
        }
    }
}

} // namespace

namespace ugine::gui {

void GameObjectEditor::Populate(core::GameObject& go, bool open) const {
    UGINE_ASSERT(gui_);
    UGINE_ASSERT(images_);

    int flags{};

    if (open) {
        flags |= ImGuiTreeNodeFlags_DefaultOpen;
    }

    // Add / Remove component.
    if (ImGui::Button(ICON_FA_PLUS " Add component")) {
        ImGui::OpenPopup("AddComponent");
    }

    if (ImGui::BeginPopup("AddComponent")) {
        using namespace ugine::core;

        //AddComponent<core::TransformationComponent>(go, "Transformation");
        AddComponent<core::CameraComponent>(
            go, ICON_FA_CAMERA " Camera", [&]() { go.CreateComponent<core::CameraComponent>(core::Camera::Perspective(60, 100, 100, 0.1f, 100.0f), false); });
        AddComponent<core::MeshComponent>(go, ICON_FA_CUBE " Mesh");
        AddComponent<core::SkinnedMeshComponent>(go, ICON_FA_BONE " Skinned Mesh");
        AddComponent<core::AnimatorComponent>(go, ICON_FA_FILM " Animator");
        AddComponent<core::LightComponent>(go, ICON_FA_LIGHTBULB " Light");
        AddComponent<core::NativeScriptComponent>(go, ICON_FA_CLIPBOARD " Native script");
        AddComponent<core::ParticleComponent>(go, ICON_FA_ATOM " Particle system");
        AddComponent<core::RigidBodyComponent>(go, ICON_FA_CUBES " Rigid body");
        AddComponent<core::TerrainComponent>(go, ICON_FA_MOUNTAIN " Terrain");
        AddComponent<core::FoliageComponent>(go, ICON_FA_TREE " Foliage");

        ImGui::EndPopup();
    }

    // Edit components.
    if (ImGui::CollapsingHeader(ICON_FA_TAG " Tag", flags)) {
        scope::Indent i;
        Populate(go, go.Component<core::Tag>());
    }

    if (go.Has<core::TransformationComponent>()) {
        if (ImGui::CollapsingHeader(ICON_FA_ARROWS_ALT " Transformation component", flags)) {
            scope::Indent i;
            Populate(go, go.Component<core::TransformationComponent>());
        }
    }

    RenderComponent<core::CameraComponent>(go, ICON_FA_CAMERA " Camera component", flags);
    RenderComponent<core::MeshComponent>(go, ICON_FA_CUBE " Mesh component", flags);
    RenderComponent<core::SkinnedMeshComponent>(go, ICON_FA_BONE " Skinned mesh component", flags);
    RenderComponent<core::AnimatorComponent>(go, ICON_FA_FILM " Animator component", flags);
    RenderComponent<core::LightComponent>(go, ICON_FA_LIGHTBULB " Light component", flags);
    RenderComponent<core::NativeScriptComponent>(go, ICON_FA_CLIPBOARD " Native script component", flags);
    RenderComponent<core::ParticleComponent>(go, ICON_FA_ATOM " Particle system component", flags);
    RenderComponent<core::RigidBodyComponent>(go, ICON_FA_CUBES " Rigid body component", flags);
    RenderComponent<core::TerrainComponent>(go, ICON_FA_MOUNTAIN " Terrain component", flags);
    RenderComponent<core::FoliageComponent>(go, ICON_FA_TREE " Foliage component", flags);
}

void GameObjectEditor::Populate(core::GameObject& go, const core::Tag& tag) const {
    auto changed{ false };

    PropertyTable table("Tag");

    EDIT_PROPERTY(table, "Name", core::Tag, tag, name);

    bool enabled{ (tag.flags & core::Tag::Disabled) == 0 };
    if (table.EditProperty("Enabled", enabled)) {
        go.Patch<Tag>([&](auto& t) { t.flags = enabled ? (t.flags & ~core::Tag::Disabled) : (t.flags | core::Tag::Disabled); });
    }
}

void GameObjectEditor::Populate(core::GameObject& go, const core::TransformationComponent& trans) const {
    auto newTrans{ trans };
    if (TransformationGui("Transformation (global)", newTrans.transformation)) {
        go.Replace<core::TransformationComponent>(newTrans);
    }
}

//template<typename T, typename V> bool EditComponent(const std::string_view& name, GameObject& go, V& value) {
//    V val { value };
//    if (table.EditProperty(name.data(), val)) {
//        go.Patch<T>([&](T& t) { t. })
//    }
//}

void GameObjectEditor::Populate(core::GameObject& go, const core::CameraComponent& cameraC) const {
    const auto& camera{ cameraC.camera };
    bool changed{ false };

    PropertyTable table("Camera");

    float fov{ camera.Fov() };
    float nearZ{ camera.Near() };
    float farZ{ camera.Far() };

    core::Camera::ProjectionType type{ camera.Type() };
    if (table.EditPropertyCombo("Type", type, CameraTypes)) {
        changed = true;
    }

    changed |= table.EditProperty("Fov", fov);
    changed |= table.EditProperty("Near", nearZ);
    changed |= table.EditProperty("Far", farZ);

    bool isMain{ cameraC.isMain };
    if (table.EditProperty("Main camera", isMain)) {
        go.Patch<core::CameraComponent>([&](auto& c) { c.isMain = isMain; });
        changed = true;
    }

    auto samples{ cameraC.samples };
    if (table.EditPropertyCombo("Multisampling", samples, Samples)) {
        go.Patch<core::CameraComponent>([&](auto& c) {
            c.samples = samples;
            if (c.renderTarget) {
                c.renderTarget = RenderTarget(cameraC.renderTarget.Width(), cameraC.renderTarget.Height(), cameraC.renderTarget.Format(), c.samples);
            }
        });
    }

    table.BeginProperty("Render target:");
    int w{ cameraC.renderTarget ? static_cast<int>(cameraC.renderTarget.Width()) : 0 };
    int h{ cameraC.renderTarget ? static_cast<int>(cameraC.renderTarget.Height()) : 0 };
    // TODO: Format.
    auto format{ cameraC.renderTarget ? cameraC.renderTarget.Format() : vk::Format::eR8G8B8A8Unorm };

    // TODO: Widget width.
    const auto size{ ImGui::GetContentRegionAvail() };
    {
        scope::ItemWidth itemWidth((size.x - 4) / 3);

        auto wChanged{ ImGui::DragInt("##width", &w) };
        ImGui::SameLine();
        ImGui::Text(" x ");
        ImGui::SameLine();
        auto hChanged{ ImGui::DragInt("##height", &h) };

        if (wChanged || hChanged) {
            if (w == 0 || h == 0) {
                if (w == 0 && h != 0) {
                    w = h;
                } else if (w != 0 && h == 0) {
                    h = w;
                }
            }

            go.Patch<CameraComponent>([&](auto& cameraC) {
                if (w == 0 && h == 0) {
                    cameraC.renderTarget = RenderTarget();
                } else {
                    cameraC.renderTarget = RenderTarget(w, h, format, cameraC.samples);
                }
            });
        }
    }

    table.EndProperty();

    if (changed) {
        switch (type) {
        case core::Camera::ProjectionType::Perspective:
            go.Patch<core::CameraComponent>([&](auto& c) { c.camera = core::Camera::Perspective(std::max<float>(fov, 1.0f), 100, 100, nearZ, farZ); });
            break;
        case core::Camera::ProjectionType::Ortho:
            go.Patch<core::CameraComponent>([&](auto& c) { c.camera = core::Camera::Ortho(-1, 1, 1, -1, nearZ, farZ); });
            break;
        }
    }

    // Postprocess
    ImGui::Columns(1);
    psEditor_.Populate(cameraC.postprocess.Postprocesses());
}

void GameObjectEditor::Populate(core::GameObject& go, const core::MeshComponent& comp) const {
    using namespace ugine::gfx;

    PropertyTable table("Mesh component");

    EDIT_PROPERTY(table, "Render", core::MeshComponent, comp, render);
    EDIT_PROPERTY(table, "Casts shadows", core::MeshComponent, comp, castsShadows);

    table.BeginProperty("Mesh");

    MeshMaterialRef mesh{ comp.meshMaterial };
    if (LoadMeshMaterial(*gui_, *images_, mesh)) {
        go.Patch<core::MeshComponent>([&](auto& m) { m.meshMaterial = mesh; });
    } else if (gui_->DragAndDrop().DropMeshMaterial(mesh)) {
        go.Patch<core::MeshComponent>([&](auto& m) { m.meshMaterial = mesh; });
    }

    ImGui::SameLine();

    if (ImGui::Button(ICON_FA_CUBES)) {
        ImGui::OpenPopup("SetShape");
    }

    ImGui::SameLine();

    if (ImGui::Button(ICON_FA_MINUS)) {
        go.Patch<core::MeshComponent>([&](auto& comp) { comp.meshMaterial = {}; });
    }

    if (ImGui::BeginPopup("SetShape")) {
        using namespace ugine::core;

        if (ImGui::Selectable("Cube")) {
            go.Patch<core::MeshComponent>([&](auto& m) {
                MaterialInstanceRef mat;
                if (m.meshMaterial && !m.meshMaterial->submeshes.empty()) {
                    mat = m.meshMaterial->submeshes[0].material;
                } else {
                    mat = MaterialService::InstantiateDefault(false);
                }

                m.meshMaterial = MESH_MATERIAL_ADD(gfx::Shapes::Cube(), mat);
            });
        }

        if (ImGui::Selectable("Plane")) {
            go.Patch<core::MeshComponent>([&](auto& m) {
                MaterialInstanceRef mat;
                if (m.meshMaterial && !m.meshMaterial->submeshes.empty()) {
                    mat = m.meshMaterial->submeshes[0].material;
                } else {
                    mat = MaterialService::InstantiateDefault(false);
                }

                m.meshMaterial = MESH_MATERIAL_ADD(gfx::Shapes::Plane(), mat);
            });
        }

        if (ImGui::Selectable("Sphere")) {
            go.Patch<core::MeshComponent>([&](auto& m) {
                MaterialInstanceRef mat;
                if (m.meshMaterial && !m.meshMaterial->submeshes.empty()) {
                    mat = m.meshMaterial->submeshes[0].material;
                } else {
                    mat = MaterialService::InstantiateDefault(false);
                }

                m.meshMaterial = MESH_MATERIAL_ADD(gfx::Shapes::Sphere(), mat);
            });
        }

        ImGui::EndPopup();
    }

    table.EndProperty();

    EDIT_PROPERTY(table, "LOD", core::MeshComponent, comp, lod);

    if (comp.meshMaterial) {
        table.ConstProperty("Submeshes", std::to_string(comp.meshMaterial->submeshes.size()));

        int _id{};
        for (size_t i = 0; i < comp.meshMaterial->submeshes.size(); ++i) {
            table.ConstProperty("Name", comp.meshMaterial->submeshes[i].name);

            table.BeginProperty("Material #%d", i);

            {
                scope::Id id{ _id };

                const std::string_view materialName{ comp.meshMaterial->submeshes[i].material ? comp.meshMaterial->submeshes[i].material->Name().c_str()
                                                                                              : "<none>" };
                if (ImGui::BeginCombo("##materialselect", materialName.data())) {

                    for (auto& matInst : gfx::MaterialService::Instances("material")) {
                        if (ImGui::Selectable(matInst->Name().c_str())) {
                            go.Patch<core::MeshComponent>([&](auto& comp) { comp.meshMaterial->submeshes[i].material = matInst; });
                        }
                    }

                    ImGui::EndCombo();
                }

                if (ImGui::IsItemHovered()) {
                    ImGui::SetTooltip(materialName.data());
                }

                gfx::MaterialInstanceRef mat;
                if (gui_->DragAndDrop().DropMaterialInstance(mat)) {
                    go.Patch<core::MeshComponent>([&](auto& comp) { comp.meshMaterial->submeshes[i].material = mat; });
                }
            }

            ImGui::SameLine();

            {
                scope::Id id{ _id };
                if (ImGui::Button(ICON_FA_MINUS)) {
                    go.Patch<core::MeshComponent>([&](auto& comp) { comp.meshMaterial->submeshes[i].material = {}; });
                }
            }
            table.EndProperty();
        }
    }

    table.ConstProperty("DEBUG", "");
    table.ConstProperty("AABB",
        fmt::format("[{},{},{}][{},{},{}]", comp.aabb.Min().x, comp.aabb.Min().y, comp.aabb.Min().z, comp.aabb.Max().x, comp.aabb.Max().y, comp.aabb.Max().z)
            .c_str());

    EDIT_PROPERTY(table, "Debug submesh", core::MeshComponent, comp, debugSubmesh);
}

void GameObjectEditor::Populate(core::GameObject& go, const core::SkinnedMeshComponent& comp) const {
    using namespace ugine::gfx;

    PropertyTable table("Mesh component");

    EDIT_PROPERTY(table, "Render", core::SkinnedMeshComponent, comp, render);
    EDIT_PROPERTY(table, "Casts shadows", core::SkinnedMeshComponent, comp, castsShadows);
    EDIT_PROPERTY(table, "LOD", core::SkinnedMeshComponent, comp, lod);

    table.BeginProperty("Skinned Mesh");

    auto fileName{ OpenFileDialog("##mesh", *gui_, *images_, comp.mesh ? comp.mesh.Name().c_str() : "<none>", MESH_FILES_FILTER) };
    if (fileName) {
        MeshLoader::Settings settings{};
        settings.singleMesh = true;
        settings.preTransform = false;
        MeshLoader loader{ *fileName, settings };
        auto data{ loader.LoadMesh() };

        go.Patch<SkinnedMeshComponent>([&](auto& c) {
            c.mesh = SkinnedMesh(std::move(data));
            c.mesh.SetName(fileName->filename().string());
        });
    }
}

void GameObjectEditor::Populate(core::GameObject& go, const core::NativeScriptComponent& script) const {
    using namespace ugine::script;

    PropertyTable table("Native script component");

    table.BeginProperty("Scripts:");

    if (ImGui::Button(ICON_FA_PLUS)) {
        ImGui::OpenPopup("AddNativeScript");
    }

    if (ImGui::BeginPopup("AddNativeScript")) {
        using namespace ugine::core;

        for (const auto& [name, r] : script::NativeScripts::Instance().Registered()) {
            if (ImGui::Selectable(name.c_str())) {
                go.Patch<NativeScriptComponent>([&](auto& c) { c.Add(go, script::NativeScripts::Instance().Create(name)); });
            }
        }

        ImGui::EndPopup();
    }

    table.EndProperty();

    std::shared_ptr<NativeScript> toRemove;
    for (const auto& t : script.Scripts()) {
        table.BeginProperty("Script");

        ImGui::Text(t->Name());
        ImGui::SameLine();

        if (ImGui::Button(ICON_FA_MINUS)) {
            toRemove = t;
        }

        table.EndProperty();
    }

    if (toRemove) {
        go.Patch<NativeScriptComponent>([&](auto& c) { c.Remove(go, toRemove); });
        toRemove = {};
    }
}

void GameObjectEditor::Populate(core::GameObject& go, const core::LightComponent& comp) const {
    PropertyTable table("Light", gui_, images_);

    bool compChanged{};

    auto newLight{ comp.light };
    bool lightChanged{};

    lightChanged |= table.EditPropertyCombo("Type", newLight.type, LightTypes);
    lightChanged |= table.EditProperty("Ambient color", newLight.ambientColor);
    lightChanged |= table.EditProperty("Color", newLight.color);
    lightChanged |= table.EditProperty("Intensity", newLight.intensity, 0.0f, 20.0f);

    if (newLight.type != gfx::Light::Type::Directional) {
        lightChanged |= table.EditProperty("Range", newLight.range, 0.0f, 10.0f);
    }

    if (newLight.type == Light::Type::Spot) {
        lightChanged |= table.EditProperty("Spot angle", newLight.spotAngleDeg, 0.01f, 90.0f);
    }

    if (lightChanged) {
        go.Patch<core::LightComponent>([&](auto& c) { c.light = newLight; });
    }

    if (newLight.type == Light::Type::Directional) {
        EDIT_PROPERTY(table, "Cascaded shadow map", core::LightComponent, comp, isCsm);
    }

    EDIT_PROPERTY(table, "Generate shadows", core::LightComponent, comp, generatesShadows);
    EDIT_PROPERTY(table, "Volumetric", core::LightComponent, comp, volumetric);
    EDIT_PROPERTY(table, "Flare", core::LightComponent, comp, flare);
    EDIT_PROPERTY(table, "Flare texture", core::LightComponent, comp, flareTexture);
    EDIT_PROPERTY(table, "Flare size", core::LightComponent, comp, flareSize);
    EDIT_PROPERTY(table, "Flare fade", core::LightComponent, comp, flareFade, 0.001f, 0.01f);
}

void GameObjectEditor::Populate(core::GameObject& go, const core::AnimatorComponent& animator) const {
    PropertyTable table("Animator");

    table.BeginProperty("Animations");

    auto fileName{ OpenFileDialog("##animator", *gui_, *images_, nullptr, MESH_FILES_FILTER) };
    if (fileName) {
        MeshLoader loader{ *fileName };
        auto animations{ loader.LoadAnimations() };

        go.Patch<AnimatorComponent>([&](auto& a) { std::copy(animations.begin(), animations.end(), std::back_inserter(a.animations)); });
    }

    table.EndProperty();

    const auto animationCount{ static_cast<int>(animator.animations.size()) };
    table.BeginProperty("Animation");
    if (!animator.animations.empty()) {
        if (ImGui::BeginCombo("##AnimSelect", animator.animations[animator.animation].name.c_str())) {
            int tmp{};
            for (uint32_t i = 0; i < animator.animations.size(); ++i) {
                scope::Id id(tmp);

                if (ImGui::Selectable(animator.animations[i].name.c_str())) {
                    go.Patch<core::AnimatorComponent>([&](auto& a) {
                        a.animation = i;
                        a.animationTime = 0;
                        a.lastUpdateTime = 0;
                    });
                }
            }

            ImGui::EndCombo();
        }
    } else {
        ImGui::Text("<none>");
    }
    table.EndProperty();

    table.ConstProperty("Time", std::to_string(animator.animationTime));
    EDIT_PROPERTY(table, "Running", core::AnimatorComponent, animator, isRunning);
    EDIT_PROPERTY(table, "Speed", core::AnimatorComponent, animator, speed, 0.0f, 50.0f);
    EDIT_PROPERTY(table, "Resolution", core::AnimatorComponent, animator, resolution, 1.0f, 1000.0f);
}

void GameObjectEditor::Populate(core::GameObject& go, const core::ParticleComponent& particle) const {
    PropertyTable table("Particles", gui_, images_);

    EDIT_PROPERTY(table, "Count", core::ParticleComponent, particle, count);
    EDIT_PROPERTY(table, "Texture", core::ParticleComponent, particle, texture);
    EDIT_PROPERTY(table, "Color", core::ParticleComponent, particle, color);
    EDIT_PROPERTY(table, "Size", core::ParticleComponent, particle, size);
    EDIT_PROPERTY(table, "Speed", core::ParticleComponent, particle, speed);
    EDIT_PROPERTY(table, "Gravity", core::ParticleComponent, particle, gravity);
    EDIT_PROPERTY(table, "Offset", core::ParticleComponent, particle, startOffset);
    EDIT_PROPERTY(table, "Speed offset", core::ParticleComponent, particle, speedOffset);
    EDIT_PROPERTY(table, "Lifetime", core::ParticleComponent, particle, life);
    EDIT_PROPERTY(table, "Life span", core::ParticleComponent, particle, lifeSpan);
    EDIT_PROPERTY(table, "Emit count", core::ParticleComponent, particle, emitCount);
    EDIT_PROPERTY(table, "Mesh", core::ParticleComponent, particle, mesh);
}

void GameObjectEditor::Populate(core::GameObject& go, const core::RigidBodyComponent& body) const {
    PropertyTable table("Rigid body");

    EDIT_PROPERTY_COMBO(table, "Shape", core::RigidBodyComponent, body, shape, RigidBodyShapes);

    auto scale{ body.scale };
    if (table.EditProperty("Scale", scale, 0.001f, 1000.0f)) {
        if (scale.x > 0 && scale.y > 0 && scale.z > 0) {
            go.Patch<core::RigidBodyComponent>([&](auto& a) { a.scale = scale; });
        }
    }

    EDIT_PROPERTY(table, "Offset", core::RigidBodyComponent, body, offset);
    EDIT_PROPERTY(table, "Linear velocity", core::RigidBodyComponent, body, linearVelocity);
    EDIT_PROPERTY(table, "Angular velocity", core::RigidBodyComponent, body, angularVelocity);
    EDIT_PROPERTY(table, "Dynamic", core::RigidBodyComponent, body, isDynamic);
    EDIT_PROPERTY(table, "Kinematic", core::RigidBodyComponent, body, isKinematic);
}

void GameObjectEditor::Populate(core::GameObject& go, const core::TerrainComponent& terrain) const {
    PropertyTable table("Terrain", gui_, images_);

    EDIT_PROPERTY(table, "Height map", core::TerrainComponent, terrain, heightMap, TEXTURE_FILES_FILTER);
    EDIT_PROPERTY(table, "Height scale", core::TerrainComponent, terrain, heightScale);
    EDIT_PROPERTY(table, "Layer texture array", core::TerrainComponent, terrain, layers);
    EDIT_PROPERTY(table, "Splat texture", core::TerrainComponent, terrain, splat);
    EDIT_PROPERTY(table, "Size", core::TerrainComponent, terrain, size, 0.001f, 10000.f);
}

void GameObjectEditor::Populate(core::GameObject& go, const core::FoliageComponent& comp) const {
    PropertyTable table("Foliage", gui_, images_);

    EDIT_PROPERTY(table, "Layers", core::FoliageComponent, comp, layerCount, 0, core::FoliageComponent::MAX_LAYERS);

    int _id{};
    for (uint32_t i = 0; i < comp.layerCount; ++i) {
        scope::Id id{ _id };

        ImGui::Separator();

        table.ConstProperty("Layer", std::to_string(i));

        const auto& layer{ comp.layers[i] };
        {
            auto count{ layer.count };
            if (table.EditProperty("Count", count)) {
                go.Patch<core::FoliageComponent>([&](auto& c) { c.layers[i].count = count; });
            }
        }

        {
            auto shadows{ layer.shadows };
            if (table.EditProperty("Shadows", shadows)) {
                go.Patch<core::FoliageComponent>([&](auto& c) { c.layers[i].shadows = shadows; });
            }
        }

        table.BeginProperty("Foliage");
        if (ImGui::BeginCombo("##foliage", layer.foliage ? layer.foliage->name.c_str() : "<none>")) {
            for (const auto& foliage : FoliageService::Foliages()) {
                bool selected{ layer.foliage == foliage };
                if (ImGui::Selectable(foliage->name.c_str(), &selected)) {
                    go.Patch<core::FoliageComponent>([&](auto& c) { c.layers[i].foliage = foliage; });
                }
            }

            ImGui::EndCombo();
        }
        table.EndProperty();

        table.ConstProperty("Real count", std::to_string(comp.realCount[i]));
    }
}

} // namespace ugine::gui
