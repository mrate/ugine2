﻿#include "ViewCube.h"

#include "Utils.h"

#include <ugine/core/Camera.h>

#include <ImGuizmo.h>

namespace ugine::gui {

bool ViewCube::Render(glm::mat4& viewMatrix, float length, float x, float y, float width, float height) {
    float cameraView[16];
    MatrixToFloatArray(viewMatrix, cameraView);

    ImGuiIO& io = ImGui::GetIO();
    if (x < 0.0f) {
        x = io.DisplaySize.x + x;
    }

    if (y < 0.0f) {
        y = io.DisplaySize.y + y;
    }

    ImGuizmo::ViewManipulate(cameraView, length, ImVec2(x, y), ImVec2(width, height), 0x10101010);
    FloatArrayToMatrix(cameraView, viewMatrix);
    return true;
}

} // namespace ugine::gui
