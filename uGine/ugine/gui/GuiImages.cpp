﻿#include "GuiImages.h"

#include "Gui.h"

#include <ugine/gfx/GraphicsService.h>
#include <ugine/gfx/Storages.h>

namespace ugine::gui {

TextureList::TextureList(const Gui& gui, int count)
    : gui_{ gui }
    , count_{ count } {

    const auto RESERVE{ gfx::GraphicsService::FramesInFlight() };
    const auto descriptorCount{ RESERVE * count * gfx::GraphicsService::FramesInFlight() };

    auto& gfx{ gfx::GraphicsService::Graphics() };

    vk::DescriptorPoolSize poolSize{ vk::DescriptorType::eCombinedImageSampler, 1 };
    vk::DescriptorPoolCreateInfo descriptorPoolInfo{};
    descriptorPoolInfo.maxSets = descriptorCount;
    descriptorPoolInfo.poolSizeCount = 1;
    descriptorPoolInfo.pPoolSizes = &poolSize;
    descriptorPool_ = gfx.Device().createDescriptorPoolUnique(descriptorPoolInfo);

    vk::DescriptorSet descriptorSet;
    std::vector<vk::DescriptorSetLayout> layouts(descriptorCount, gui.Layout());

    vk::DescriptorSetAllocateInfo alloc_info = {};
    alloc_info.descriptorPool = *descriptorPool_;
    alloc_info.descriptorSetCount = descriptorCount;
    alloc_info.pSetLayouts = layouts.data();

    auto descriptors{ gfx.Device().allocateDescriptorSets(alloc_info) };
    std::copy(descriptors.begin(), descriptors.end(), std::back_inserter(freeDescriptors_));

    images_.resize(count_);
    for (auto& image : images_) {
        image.descriptors.resize(gfx::GraphicsService::FramesInFlight());
    }
}

void TextureList::SetImage(int imageIndex, float width, float height, const gfx::TextureRef& texture, vk::ImageLayout layout, int flags) {
    auto view{ gfx::TEXTURE_VIEW_ADD(texture, gfx::TextureView::CreateSampled(*texture)) };
    gfx::GraphicsService::Device().SetObjectDebugName(view->View(), fmt::format("GuiImage {}", imageIndex));
    DEBUG_ATTACH(view, "GuiImages[{}]", imageIndex);

    auto idx{ DescriptorIndex(imageIndex) };

    auto& image{ images_[imageIndex] };
    AllocateDescriptor(image.descriptors[idx]);

    image.isRenderTarget = false;
    image.image.resize(1);
    image.size = ImVec2(width, height);

    image.image[0].texture = view;
    image.image[0].id = gui_.AddTexture(view->Sampler(), view->View(), layout, image.descriptors[idx], flags);
}

void TextureList::SetImage(int imageIndex, float width, float height, const gfx::TextureViewRef& texture, vk::ImageLayout layout, int flags) {
    auto idx{ DescriptorIndex(imageIndex) };

    auto& image{ images_[imageIndex] };
    AllocateDescriptor(image.descriptors[idx]);

    image.isRenderTarget = false;
    image.image.resize(1);
    image.size = ImVec2(width, height);

    image.image[0].texture = texture;
    image.image[0].id = gui_.AddTexture(texture->Sampler(), texture->View(), layout, image.descriptors[idx], flags);
}

void TextureList::SetImage(int imageIndex, float width, float height, const gfx::RenderTarget& texture, vk::ImageLayout layout, int flags) {
    auto& image{ images_[imageIndex] };
    image.isRenderTarget = true;

    image.image.resize(gfx::GraphicsService::FramesInFlight());
    image.size = ImVec2(width, height);

    for (uint32_t i = 0; i < gfx::GraphicsService::FramesInFlight(); ++i) {
        AllocateDescriptor(image.descriptors[i]);

        image.image[i].texture = texture.ViewRefForFrame(i);
        image.image[i].id
            = gui_.AddTexture(texture.View().Sampler(), texture.ViewForFrame(i).View(), vk::ImageLayout::eShaderReadOnlyOptimal, image.descriptors[i], flags);
    }
}

ImTextureID TextureList::Image(int imageIndex) const {
    if (images_[imageIndex].isRenderTarget) {
        return images_[imageIndex].image[gfx::GraphicsService::ActiveFrameInFlight()].id;
    } else {
        return images_[imageIndex].image[0].id;
    }
}

uint32_t TextureList::DescriptorIndex(int imageIndex) {
    auto& image{ images_[imageIndex] };
    if (image.isRenderTarget) {
        image.activeFrame = gfx::GraphicsService::ActiveFrameInFlight();
    } else {
        image.activeFrame = (image.activeFrame + 1) % gfx::GraphicsService::FramesInFlight();
    }
    return image.activeFrame;
}

void TextureList::AllocateDescriptor(vk::DescriptorSet& descriptor) {
    UGINE_ASSERT(!freeDescriptors_.empty());

    if (descriptor) {
        freeDescriptors_.push_front(descriptor);
    }
    descriptor = freeDescriptors_.back();
    freeDescriptors_.pop_back();
}

GuiImages::GuiImages(const Gui& gui)
    : TextureList(gui, static_cast<int>(Type::TypeCount)) {}

void GuiImages::SetImage(Type type, const gfx::TextureRef& texture, vk::ImageLayout layout, int flags) {
    setImages_.insert(type);
    TextureList::SetImage(static_cast<int>(type), defaultSize_.x, defaultSize_.y, texture, layout, flags);
}

void GuiImages::SetImage(Type type, const gfx::TextureViewRef& texture, vk::ImageLayout layout, int flags) {
    setImages_.insert(type);
    TextureList::SetImage(static_cast<int>(type), defaultSize_.x, defaultSize_.y, texture, layout, flags);
}

void GuiImages::SetImage(Type type, const gfx::RenderTarget& texture, vk::ImageLayout layout, int flags) {
    setImages_.insert(type);
    TextureList::SetImage(static_cast<int>(type), defaultSize_.x, defaultSize_.y, texture, layout, flags);
}

ImTextureID GuiImages::Image(Type type) const {
    if (setImages_.contains(type)) {
        return TextureList::Image(static_cast<int>(type));
    } else {
        return TextureList::Image(static_cast<int>(Type::Empty));
    }
}

} // namespace ugine::gui
