﻿#pragma once

namespace ugine::gfx {
class MaterialInstance;
}

namespace ugine::gui {

class Gui;
class GuiImages;

class MaterialEditor {
public:
    void SetGui(const Gui* gui) {
        gui_ = gui;
    }

    void SetImages(const GuiImages* images) {
        images_ = images;
    }

    void PopulateMaterial(gfx::MaterialInstance& material);

private:
    const Gui* gui_{};
    const GuiImages* images_{};
};

} // namespace ugine::gui
