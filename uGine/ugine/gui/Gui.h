﻿#pragma once

#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/Storages.h>
#include <ugine/gfx/core/VertexBuffer.h>
#include <ugine/gui/DragAndDrop.h>

#include <ugine/system/Window.h>

#include <glm/glm.hpp>

#include <vulkan/vulkan.hpp>

#include <imgui.h>

namespace ugine::gui {

class Gui {
public:
    enum Flags {
        None = 0,
        FlipY = 1 << 0,
        Depth = 1 << 1,
    };

    explicit Gui(const std::shared_ptr<system::Window>& window);
    ~Gui();

    void Init(vk::RenderPass renderPass, vk::Extent2D extent);

    void NewFrame();
    void EndFrame();

    void Render(gfx::GPUCommandList buffer);

    bool CaptureKeyboard() const;
    bool CaptureMouse() const;

    std::optional<std::filesystem::path> OpenFileDialog(const std::vector<std::wstring>& filter) const;
    std::vector<std::filesystem::path> OpenMultipleFilesDialog(const std::vector<std::wstring>& filter) const;

    // TODO: Layout.
    ImTextureID AddTexture(const gfx::TextureViewRef& image, vk::ImageLayout imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal, int flags = 0) const;
    ImTextureID AddTexture(vk::Sampler sampler, vk::ImageView imageView, vk::ImageLayout imageLayout, int flags = 0) const;
    ImTextureID AddTexture(vk::Sampler sampler, vk::ImageView imageView, vk::ImageLayout imageLayout, vk::DescriptorSet ds, int flags = 0) const;

    vk::DescriptorSetLayout Layout() const {
        return *descriptorSetLayout_;
    }

    const std::vector<std::string> Themes() const {
        return themes_;
    }

    void SetTheme(const std::string& theme);

    const std::string& SelectedTheme() const {
        return selectedTheme_;
    }

    DragAndDrop& DragAndDrop() const {
        return dragAndDrop_;
    }

private:
    void InitResources(vk::RenderPass renderPass);
    void UpdateBuffers();

    struct PushConstBlock {
        glm::vec2 scale;
        glm::vec2 translate;
        int flags{};
    };

    struct VertexIndexBuffer {
        int32_t vertexCount{ 0 };
        int32_t indexCount{ 0 };
        std::unique_ptr<gfx::Buffer> vertex;
        std::unique_ptr<gfx::Buffer> index;
    };

    std::shared_ptr<system::Window> window_;

    mutable gui::DragAndDrop dragAndDrop_;

    std::vector<VertexIndexBuffer> buffer_;

    gfx::TextureViewRef fontTextureView_;

    vk::UniquePipelineCache pipelineCache_;
    gfx::Pipeline pipeline_;

    vk::UniqueDescriptorPool descriptorPool_;
    vk::UniqueDescriptorSetLayout descriptorSetLayout_;

    PushConstBlock pushConstBlock_;

    std::string selectedTheme_;
    std::vector<std::string> themes_;
    std::map<std::string, std::function<void()>> themeSetters_;

    mutable std::map<ImTextureID, int> textureFlags_;
};

} // namespace ugine::gui
