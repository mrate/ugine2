﻿#pragma once

#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/core/RenderTarget.h>

#include <imgui.h>

#include <fonts/IconsFontAwesome5.h>

#include <map>

namespace ugine::gui {

class Gui;

class TextureList {
public:
    virtual ~TextureList() {}

    TextureList(const Gui& gui, int count);

    void SetImage(
        int image, float width, float height, const gfx::TextureRef& texture, vk::ImageLayout layout = vk::ImageLayout::eShaderReadOnlyOptimal, int flags = 0);
    void SetImage(int image, float width, float height, const gfx::TextureViewRef& texture, vk::ImageLayout layout = vk::ImageLayout::eShaderReadOnlyOptimal,
        int flags = 0);
    void SetImage(int image, float width, float height, const gfx::RenderTarget& texture, vk::ImageLayout layout = vk::ImageLayout::eShaderReadOnlyOptimal,
        int flags = 0);
    ImTextureID Image(int image) const;

    ImVec2 Size(int image) const {
        return images_[image].size;
    }

    void SetSize(int index, float w, float h) {
        images_[index].size = ImVec2(w, h);
    }

    void SetSize(float w, float h) {
        for (auto& image : images_) {
            image.size = ImVec2(w, h);
        }
    }

private:
    struct FrameImage {
        gfx::TextureViewRef texture;
        ImTextureID id;
    };

    struct ImageEntry {
        std::vector<FrameImage> image;
        uint32_t activeFrame{};
        bool isRenderTarget{};
        std::vector<vk::DescriptorSet> descriptors;
        ImVec2 size;
    };

    uint32_t DescriptorIndex(int image);
    void AllocateDescriptor(vk::DescriptorSet& descriptor);

    const Gui& gui_;
    vk::UniqueDescriptorPool descriptorPool_;
    std::deque<vk::DescriptorSet> freeDescriptors_;

    std::vector<ImageEntry> images_;

    int count_{};
};

class GuiImages : public TextureList {
public:
    enum Type {
        Empty = 0,
        Add,
        Remove,
        Object,
        View,
        Camera,
        Hidden,
        Preview,
        Preview2,
        Open,
        Delete,
        Copy,
        ComponentAnimator,
        ComponentCamera,
        ComponentLight,
        ComponentLogic,
        ComponentMaterial,
        ComponentSkinnedMesh,
        ComponentSkybox,
        ComponentStaticMesh,
        MaterialIcon,
        FxMaterialIcon,
        FoliageIcon,
        MeshIcon,
        FileIcon,
        TypeCount,
    };

    GuiImages(const Gui& gui);

    void SetDefaultSize(float width, float height) {
        defaultSize_ = ImVec2{ width, height };
    }

    ImVec2 Size() const {
        return defaultSize_;
    }

    void SetImage(Type type, const gfx::TextureRef& texture, vk::ImageLayout layout = vk::ImageLayout::eShaderReadOnlyOptimal, int flags = 0);
    void SetImage(Type type, const gfx::TextureViewRef& texture, vk::ImageLayout layout = vk::ImageLayout::eShaderReadOnlyOptimal, int flags = 0);
    void SetImage(Type image, const gfx::RenderTarget& texture, vk::ImageLayout layout = vk::ImageLayout::eShaderReadOnlyOptimal, int flags = 0);

    ImTextureID Image(Type type) const;

private:
    std::set<Type> setImages_;
    ImVec2 defaultSize_{ 32, 32 };
};

} // namespace ugine::gui
