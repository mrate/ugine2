﻿#pragma once

#include <ugine/core/Camera.h>
#include <ugine/core/GameObject.h>

#include <optional>

namespace ugine::gui {

class GameObjectTransformation {
public:
    enum class Operation {
        Translate,
        Rotate,
        Scale,
        Bounds,
    };

    static bool Render(
        glm::mat4& deltaTransformation, const core::Camera& camera, core::GameObject& go, Operation operation, std::optional<float> snap, bool local = false);

    static bool WantCapture();

    static void SetRect(float x, float y, float width, float height);
};

} // namespace ugine::gui
