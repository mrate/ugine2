#pragma once

#include <ugine/core/GameObject.h>
#include <ugine/gfx/rendering/Foliage.h>
#include <ugine/gfx/rendering/MaterialInstance.h>

#include <imgui.h>

#include <optional>

namespace ugine::gui {

class DragAndDrop {
public:
    enum class Type {
        None,
        GameObject,
        MaterialInstance,
        MeshMaterial,
        Foliage,
        Mesh,
    };

    void CanDragGameObject(const core::GameObject& go);
    bool DropGameObject(core::GameObject& go);

    void CanDragMaterialInstance(const gfx::MaterialInstanceRef& mat);
    bool DropMaterialInstance(gfx::MaterialInstanceRef& mat);

    void CanDragMeshMaterial(const gfx::MeshMaterialRef& mat);
    bool DropMeshMaterial(gfx::MeshMaterialRef& mat);

    void CanDragFoliage(const gfx::FoliageRef& foliage);
    bool DropFoliage(gfx::FoliageRef& foliage);

    void CanDragMesh(const gfx::MeshRef& mesh);
    bool DropMesh(gfx::MeshRef& mesh);

    Type DragType() const {
        return type_;
    }

private:
    inline static const char* PAYLOAD_GO = "ugine::gameobject";
    inline static const char* PAYLOAD_MATERIAL_INSTANCE = "ugine::materialinstance";
    inline static const char* PAYLOAD_MESH_MATERIAL = "ugine::meshmaterial";
    inline static const char* PAYLOAD_FOLIAGE = "ugine::foliage";
    inline static const char* PAYLOAD_MESH = "ugine::mesh";

    template <typename _Type> void Drag(const _Type& value, Type type, _Type& storage, const char* payload, const char* name) {
        if (ImGui::BeginDragDropSource()) {
            Drop();
            type_ = type;
            storage = value;
            ImGui::SetDragDropPayload(payload, this, sizeof(this));
            ImGui::TextUnformatted(name);
            ImGui::EndDragDropSource();
        }
    }

    template <typename _Type> bool Drop(const char* payloadName, _Type& out, const _Type& storage) {
        bool res{};
        if (ImGui::BeginDragDropTarget()) {
            const ImGuiPayload* payload{ ImGui::AcceptDragDropPayload(payloadName) };
            if (payload) {
                out = storage;
                Drop();
                res = true;
            }
            ImGui::EndDragDropTarget();
        }
        return res;
    }

    void Drop();

    Type type_{ Type::None };

    core::GameObject go_;
    gfx::MaterialInstanceRef materialInst_;
    gfx::MeshMaterialRef meshMaterial_;
    gfx::FoliageRef foliage_;
    gfx::MeshRef mesh_;
};

} // namespace ugine::gui