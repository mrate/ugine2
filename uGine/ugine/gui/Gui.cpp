﻿#include "Gui.h"

#include <ugine/gfx/Graphics.h>
#include <ugine/gfx/GraphicsService.h>
#include <ugine/gfx/ShaderCache.h>
#include <ugine/gfx/core/Texture.h>
#include <ugine/gfx/data/SamplerCache.h>

#include <ugine/assets/AssetService.h>

#include <ugine/system/win32/Win32Window.h>

#include <backends/imgui_impl_vulkan.h>

#include <imgui.h>

#include <ImGuizmo.h>

#include <array>

// Themes.
#include "themes/Dark.h"
#include "themes/DeusEx.h"
#include "themes/Light.h"
#include "themes/RedBlack.h"

#include <fonts/IconsFontAwesome5.h>

std::vector<ImGui_ImplVulkanH_Frame> imguiFrameBuffers_;

namespace {

void ImGuiErrorHandler(VkResult result) {}

} // namespace

namespace ugine::gui {

Gui::Gui(const std::shared_ptr<system::Window>& window)
    : window_(window) {
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    (void)io;

    themeSetters_.emplace(std::make_pair("Dark", &theme::Dark));
    themeSetters_.emplace(std::make_pair("Light", &theme::Light));
    themeSetters_.emplace(std::make_pair("Deus Ex", &theme::DeusEx));
    themeSetters_.emplace(std::make_pair("Red Black", &theme::RedBlack));

    for (auto [k, v] : themeSetters_) {
        themes_.push_back(k);
    }

    SetTheme("Dark");

    window->InitGui();
}

Gui::~Gui() {
    for (auto& buffer : buffer_) {
        if (buffer.vertex) {
            buffer.vertex->Unmap();
        }

        if (buffer.index) {
            buffer.index->Unmap();
        }
    }

    window_->DestroyGui();
    ImGui::DestroyContext();
}

void Gui::Init(vk::RenderPass renderPass, vk::Extent2D extent) {
    auto& io{ ImGui::GetIO() };
#ifdef IMGUI_DOCKING
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
#endif
    io.DisplaySize = ImVec2(static_cast<float>(extent.width), static_cast<float>(extent.height));
    io.DisplayFramebufferScale = ImVec2(1.0f, 1.0f);

    io.Fonts->AddFontFromFileTTF("assets/fonts/Roboto-Medium.ttf", 14.0f);

    // Icons.
    static const ImWchar icons_ranges[] = { ICON_MIN_FA, ICON_MAX_FA, 0 };
    ImFontConfig icons_config;
    icons_config.MergeMode = true;
    icons_config.PixelSnapH = true;
    io.Fonts->AddFontFromFileTTF("assets/fonts/" FONT_ICON_FILE_NAME_FAS, 16.0f, &icons_config, icons_ranges);

    InitResources(renderPass);

    buffer_.resize(gfx::GraphicsService::Presenter().SwapChainImageCount());
}

std::optional<std::filesystem::path> Gui::OpenFileDialog(const std::vector<std::wstring>& filter) const {
    auto res{ window_->OpenFileDialog(filter, false) };
    return res.empty() ? std::optional<std::filesystem::path>() : res[0];
}

std::vector<std::filesystem::path> Gui::OpenMultipleFilesDialog(const std::vector<std::wstring>& filter) const {
    return window_->OpenFileDialog(filter, true);
}

void Gui::UpdateBuffers() {
    const auto frameIndex{ gfx::GraphicsService::Presenter().FrameIndex() };

    ImDrawData* imDrawData = ImGui::GetDrawData();

    // Note: Alignment is done inside buffer creation
    vk::DeviceSize vertexBufferSize{ imDrawData->TotalVtxCount * sizeof(ImDrawVert) };
    vk::DeviceSize indexBufferSize{ imDrawData->TotalIdxCount * sizeof(ImDrawIdx) };

    if ((vertexBufferSize == 0) || (indexBufferSize == 0)) {
        return;
    }

    // Update buffers only if vertex or index count has been changed compared to current buffer size

    // Vertex buffer
    auto& buffer{ buffer_[frameIndex] };

    if ((!buffer.vertex) || (buffer.vertexCount != imDrawData->TotalVtxCount)) {
        if (buffer.vertex) {
            buffer.vertex->Unmap();
        }
        buffer.vertex = std::make_unique<gfx::Buffer>(vertexBufferSize, vk::BufferUsageFlagBits::eVertexBuffer, vk::MemoryPropertyFlagBits::eHostVisible);
        buffer.vertexCount = imDrawData->TotalVtxCount;
        buffer.vertex->Map();
    }

    // Index buffer
    if ((!buffer.index) || (buffer.indexCount < imDrawData->TotalIdxCount)) {
        if (buffer.index) {
            buffer.index->Unmap();
        }
        buffer.index = std::make_unique<gfx::Buffer>(indexBufferSize, vk::BufferUsageFlagBits::eIndexBuffer, vk::MemoryPropertyFlagBits::eHostVisible);
        buffer.indexCount = imDrawData->TotalIdxCount;
        buffer.index->Map();
    }

    // Upload data
    auto vtxDst{ reinterpret_cast<ImDrawVert*>(buffer.vertex->Mapped()) };
    auto idxDst{ reinterpret_cast<ImDrawIdx*>(buffer.index->Mapped()) };

    for (int n = 0; n < imDrawData->CmdListsCount; n++) {
        const ImDrawList* cmd_list = imDrawData->CmdLists[n];
        memcpy(vtxDst, cmd_list->VtxBuffer.Data, cmd_list->VtxBuffer.Size * sizeof(ImDrawVert));
        memcpy(idxDst, cmd_list->IdxBuffer.Data, cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx));
        vtxDst += cmd_list->VtxBuffer.Size;
        idxDst += cmd_list->IdxBuffer.Size;
    }

    // TODO: Flush?
    //buffer.vertex->Flush(gfx);
    //buffer.index->Flush(gfx);
}

void Gui::NewFrame() {
    window_->NewFrame();
    ImGui::NewFrame();

    ImGuizmo::SetOrthographic(false);
    ImGuizmo::BeginFrame();
}

void Gui::EndFrame() {
    ImGui::Render();
    UpdateBuffers();
}

// Draw current imGui frame into a command buffer
void Gui::Render(gfx::GPUCommandList cmd) {
    auto& device{ gfx::GraphicsService::Device() };

    ImGuiIO& io = ImGui::GetIO();
    auto flags = io.ConfigFlags;

    vk::Viewport viewport{};
    viewport.width = ImGui::GetIO().DisplaySize.x;
    viewport.height = ImGui::GetIO().DisplaySize.y;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    // TODO:
    device.ResetDescriptors(cmd);

    device.BindPipeline(cmd, &pipeline_);
    device.SetViewport(cmd, viewport);

    // UI scale and translate via push constants
    pushConstBlock_.scale = glm::vec2(2.0f / io.DisplaySize.x, 2.0f / io.DisplaySize.y);
    pushConstBlock_.translate = glm::vec2(-1.0f);

    // Render commands
    auto imDrawData{ ImGui::GetDrawData() };
    int32_t vertexOffset = 0;
    int32_t indexOffset = 0;

    auto& buffer{ buffer_[gfx::GraphicsService::Presenter().FrameIndex()] };

    if (imDrawData->CmdListsCount > 0) {
        auto vertexBuffer{ buffer.vertex->GetBuffer() };

        device.BindVertexBuffer(cmd, vertexBuffer, 0);
        device.BindIndexBuffer(cmd, buffer.index->GetBuffer(), 0, vk::IndexType::eUint16);

        for (int32_t i = 0; i < imDrawData->CmdListsCount; i++) {
            const ImDrawList* cmd_list = imDrawData->CmdLists[i];
            for (int32_t j = 0; j < cmd_list->CmdBuffer.Size; j++) {
                const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[j];
                vk::Rect2D scissorRect;
                scissorRect.offset.x = std::max((int32_t)(pcmd->ClipRect.x), 0);
                scissorRect.offset.y = std::max((int32_t)(pcmd->ClipRect.y), 0);
                scissorRect.extent.width = (uint32_t)(pcmd->ClipRect.z - pcmd->ClipRect.x);
                scissorRect.extent.height = (uint32_t)(pcmd->ClipRect.w - pcmd->ClipRect.y);

                auto it{ textureFlags_.find(pcmd->TextureId) };

                pushConstBlock_.flags = it != textureFlags_.end() ? it->second : 0;
                device.CommandBuffer(cmd).pushConstants(
                    pipeline_.Layout(), vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, 0, sizeof(PushConstBlock), &pushConstBlock_);

                device.BindDescriptor(cmd, 0, static_cast<VkDescriptorSet>(pcmd->TextureId));

                device.SetScissor(cmd, scissorRect);
                device.DrawIndexed(cmd, pcmd->ElemCount, 1, indexOffset, vertexOffset, 0);
                indexOffset += pcmd->ElemCount;
            }
            vertexOffset += cmd_list->VtxBuffer.Size;
        }
    }
}

bool Gui::CaptureKeyboard() const {
    return ImGui::GetIO().WantCaptureKeyboard;
}

bool Gui::CaptureMouse() const {
    return ImGui::GetIO().WantCaptureMouse || ImGuizmo::IsUsing();
}

void Gui::SetTheme(const std::string& theme) {
    auto it{ themeSetters_.find(theme) };
    if (it != themeSetters_.end()) {
        selectedTheme_ = theme;
        it->second();
    }
}

void Gui::InitResources(vk::RenderPass renderPass) {
    using namespace ugine::gfx;

    ImGuiIO& io = ImGui::GetIO();

    // Create font texture
    unsigned char* fontData;
    int texWidth, texHeight;
    io.Fonts->GetTexDataAsRGBA32(&fontData, &texWidth, &texHeight);
    vk::DeviceSize uploadSize = texWidth * texHeight * 4 * sizeof(char);

    auto& gfx{ GraphicsService::Graphics() };

    // Create target image for copy
    auto fontSampler{ SamplerCache::Sampler(SamplerCache::Type::LinearClampToEdge) };
    auto fontTexture{ TEXTURE_ADD(Texture::FromData(fontData, uploadSize, vk::Extent3D{ static_cast<uint32_t>(texWidth), static_cast<uint32_t>(texHeight), 1 },
        vk::Format::eR8G8B8A8Unorm, vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eSampled)) };
    fontTextureView_ = TEXTURE_VIEW_ADD(fontTexture, gfx::TextureView::CreateSampled(*fontTexture, fontSampler));

    // Descriptor pool
    vk::DescriptorPoolSize poolSize = { vk::DescriptorType::eCombinedImageSampler, 1 };
    vk::DescriptorPoolCreateInfo descriptorPoolInfo{};
    descriptorPoolInfo.maxSets = 2;
    descriptorPoolInfo.poolSizeCount = 1;
    descriptorPoolInfo.pPoolSizes = &poolSize;
    descriptorPool_ = gfx.Device().createDescriptorPoolUnique(descriptorPoolInfo);

    // Descriptor set layout
    vk::DescriptorSetLayoutBinding setLayoutBindings{};
    setLayoutBindings.stageFlags = vk::ShaderStageFlagBits::eFragment;
    setLayoutBindings.descriptorCount = 1;
    setLayoutBindings.descriptorType = vk::DescriptorType::eCombinedImageSampler;
    setLayoutBindings.binding = 0;

    vk::DescriptorSetLayoutCreateInfo descriptorLayout;
    descriptorLayout.pBindings = &setLayoutBindings;
    descriptorLayout.bindingCount = 1;

    descriptorSetLayout_ = gfx.Device().createDescriptorSetLayoutUnique(descriptorLayout);

    // Font texture.
    ImTextureID fontTextureID = AddTexture(fontTextureView_);
    io.Fonts->TexID = fontTextureID;

    // Pipeline cache
    pipelineCache_ = gfx.Device().createPipelineCacheUnique({});

    // Pipeline layout
    // Push constants for UI rendering parameters
    vk::PushConstantRange pushConstantRange{};
    pushConstantRange.stageFlags = vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment;
    pushConstantRange.offset = 0;
    pushConstantRange.size = sizeof(PushConstBlock);

    std::vector<vk::DescriptorSetLayout> dsLayouts = { *descriptorSetLayout_ };

    vk::PipelineLayoutCreateInfo pipelineLayoutCreateInfo{};
    pipelineLayoutCreateInfo.setLayoutCount = static_cast<uint32_t>(dsLayouts.size());
    pipelineLayoutCreateInfo.pSetLayouts = dsLayouts.data();
    pipelineLayoutCreateInfo.pushConstantRangeCount = 1;
    pipelineLayoutCreateInfo.pPushConstantRanges = &pushConstantRange;

    auto pipelineLayout{ gfx.Device().createPipelineLayoutUnique(pipelineLayoutCreateInfo) };

    // Setup graphics pipeline for UI rendering
    vk::PipelineInputAssemblyStateCreateInfo inputAssemblyState{};
    inputAssemblyState.topology = vk::PrimitiveTopology::eTriangleList;

    vk::PipelineRasterizationStateCreateInfo rasterizationState{};
    rasterizationState.polygonMode = vk::PolygonMode::eFill;
    rasterizationState.cullMode = vk::CullModeFlagBits::eNone;
    rasterizationState.frontFace = vk::FrontFace::eCounterClockwise;
    rasterizationState.lineWidth = 1.0f;
    rasterizationState.depthClampEnable = VK_FALSE;

    // Enable blending
    vk::PipelineColorBlendAttachmentState blendAttachmentState{};
    blendAttachmentState.blendEnable = VK_TRUE;
    blendAttachmentState.colorWriteMask
        = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
    blendAttachmentState.srcColorBlendFactor = vk::BlendFactor::eSrcAlpha;
    blendAttachmentState.dstColorBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha;
    blendAttachmentState.colorBlendOp = vk::BlendOp::eAdd;
    blendAttachmentState.srcAlphaBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha;
    blendAttachmentState.dstAlphaBlendFactor = vk::BlendFactor::eZero;
    blendAttachmentState.alphaBlendOp = vk::BlendOp::eAdd;

    vk::PipelineColorBlendStateCreateInfo colorBlending;
    colorBlending.logicOpEnable = VK_FALSE;
    colorBlending.logicOp = vk::LogicOp::eCopy;
    colorBlending.attachmentCount = 1;
    colorBlending.pAttachments = &blendAttachmentState;

    vk::PipelineDepthStencilStateCreateInfo depthStencilState;
    depthStencilState.depthTestEnable = VK_FALSE;
    depthStencilState.depthWriteEnable = VK_FALSE;
    depthStencilState.depthCompareOp = vk::CompareOp::eLessOrEqual;
    depthStencilState.depthBoundsTestEnable = VK_FALSE;
    depthStencilState.stencilTestEnable = VK_FALSE;

    vk::PipelineViewportStateCreateInfo pipelineViewportStateCreateInfo{};
    pipelineViewportStateCreateInfo.viewportCount = 1;
    pipelineViewportStateCreateInfo.scissorCount = 1;

    vk::PipelineMultisampleStateCreateInfo pipelineMultisampleStateCreateInfo{};
    pipelineMultisampleStateCreateInfo.rasterizationSamples = /*gfx.AvailableSampleCounts()*/ vk::SampleCountFlagBits::e1;

    std::vector<vk::DynamicState> dynamicStateEnables = { vk::DynamicState::eViewport, vk::DynamicState::eScissor };

    vk::PipelineDynamicStateCreateInfo pipelineDynamicStateCreateInfo{};
    pipelineDynamicStateCreateInfo.pDynamicStates = dynamicStateEnables.data();
    pipelineDynamicStateCreateInfo.dynamicStateCount = static_cast<uint32_t>(dynamicStateEnables.size());

    std::array<vk::PipelineShaderStageCreateInfo, 2> shaderStages{};

    vk::GraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.layout = *pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyState;
    pipelineCreateInfo.pRasterizationState = &rasterizationState;
    pipelineCreateInfo.pColorBlendState = &colorBlending;
    pipelineCreateInfo.pMultisampleState = &pipelineMultisampleStateCreateInfo;
    pipelineCreateInfo.pViewportState = &pipelineViewportStateCreateInfo;
    pipelineCreateInfo.pDepthStencilState = &depthStencilState;
    pipelineCreateInfo.pDynamicState = &pipelineDynamicStateCreateInfo;
    pipelineCreateInfo.stageCount = static_cast<uint32_t>(shaderStages.size());
    pipelineCreateInfo.pStages = shaderStages.data();

    // Vertex bindings an attributes based on ImGui vertex definition
    std::vector<vk::VertexInputBindingDescription> vertexInputBindings = { { 0, sizeof(ImDrawVert), vk::VertexInputRate::eVertex } };

    std::vector<vk::VertexInputAttributeDescription> vertexInputAttributes = {
        { 0, 0, vk::Format::eR32G32Sfloat, offsetof(ImDrawVert, pos) }, // Location 0: Position
        { 1, 0, vk::Format::eR32G32Sfloat, offsetof(ImDrawVert, uv) }, // Location 1: UV
        { 2, 0, vk::Format::eR8G8B8A8Unorm, offsetof(ImDrawVert, col) }, // Location 0: Color
    };

    vk::PipelineVertexInputStateCreateInfo vertexInputState{};
    vertexInputState.vertexBindingDescriptionCount = static_cast<uint32_t>(vertexInputBindings.size());
    vertexInputState.pVertexBindingDescriptions = vertexInputBindings.data();
    vertexInputState.vertexAttributeDescriptionCount = static_cast<uint32_t>(vertexInputAttributes.size());
    vertexInputState.pVertexAttributeDescriptions = vertexInputAttributes.data();

    pipelineCreateInfo.pVertexInputState = &vertexInputState;

    const auto& vertexShader{ ShaderCache::GetShader(vk::ShaderStageFlagBits::eVertex, L"assets/shaders/imgui/ui.vert") };
    const auto& fragmentShader{ ShaderCache::GetShader(vk::ShaderStageFlagBits::eFragment, L"assets/shaders/imgui/ui.frag") };

    shaderStages[0] = vk::PipelineShaderStageCreateInfo{ {}, vk::ShaderStageFlagBits::eVertex, *vertexShader.module, vertexShader.entryPoint.c_str() };
    shaderStages[1] = vk::PipelineShaderStageCreateInfo{ {}, vk::ShaderStageFlagBits::eFragment, *fragmentShader.module, fragmentShader.entryPoint.c_str() };

    auto result{ gfx.Device().createGraphicsPipelineUnique(*pipelineCache_, pipelineCreateInfo) };

    if (result.result != vk::Result::eSuccess) {
        UGINE_THROW(core::Error, "Failed to create ImGui pipeline.");
    }

    pipeline_ = gfx::Pipeline(vk::PipelineBindPoint::eGraphics, std::move(pipelineLayout), std::move(result.value), dsLayouts);
}

ImTextureID Gui::AddTexture(const gfx::TextureViewRef& image, vk::ImageLayout imageLayout, int flags) const {
    return AddTexture(image->Sampler(), image->View(), imageLayout, flags);
}

ImTextureID Gui::AddTexture(vk::Sampler sampler, vk::ImageView imageView, vk::ImageLayout imageLayout, int flags) const {
    auto& gfx{ gfx::GraphicsService::Graphics() };

    vk::DescriptorSet descriptorSet;
    std::array<vk::DescriptorSetLayout, 1> layouts = { *descriptorSetLayout_ };

    vk::DescriptorSetAllocateInfo alloc_info = {};
    alloc_info.descriptorPool = *descriptorPool_;
    alloc_info.descriptorSetCount = 1;
    alloc_info.pSetLayouts = layouts.data();
    descriptorSet = gfx.Device().allocateDescriptorSets(alloc_info)[0];

    return AddTexture(sampler, imageView, imageLayout, descriptorSet, flags);
}

ImTextureID Gui::AddTexture(vk::Sampler sampler, vk::ImageView imageView, vk::ImageLayout imageLayout, vk::DescriptorSet ds, int flags) const {
    auto& gfx{ gfx::GraphicsService::Graphics() };

    vk::DescriptorImageInfo desc_image[1] = {};
    desc_image[0].sampler = sampler;
    desc_image[0].imageView = imageView;
    desc_image[0].imageLayout = imageLayout;

    vk::WriteDescriptorSet descriptorWrite;
    descriptorWrite.dstSet = ds;
    descriptorWrite.dstBinding = 0;
    descriptorWrite.dstArrayElement = 0;
    descriptorWrite.descriptorType = vk::DescriptorType::eCombinedImageSampler;
    descriptorWrite.descriptorCount = 1;
    descriptorWrite.pImageInfo = desc_image;

    gfx.Device().updateDescriptorSets(descriptorWrite, {});

    auto textureId = static_cast<ImTextureID>(ds);

    if (flags != 0) {
        textureFlags_[textureId] = flags;
    } else {
        textureFlags_.erase(textureId);
    }

    return textureId;
}

} // namespace ugine::gui
