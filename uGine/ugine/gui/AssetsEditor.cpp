﻿#include "AssetsEditor.h"

#include "Gui.h"
#include "GuiImages.h"
#include "Utils.h"

#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/data/PipelineCache.h>
#include <ugine/gfx/rendering/MaterialHelpers.h>
#include <ugine/gfx/rendering/MaterialInstance.h>

using namespace ugine::gfx;

namespace ugine::gui {

void AssetsEditor::PopulateAssets() {
    auto flags{ ImGuiWindowFlags_HorizontalScrollbar };

    const auto selectionWidth{ 200 };

    ImGui::BeginChild("Assets", ImVec2(selectionWidth, 0), false, flags);
    {
        bool selected{ selection_ == Selection::Materials };
        if (ImGui::Selectable(ICON_FA_FOLDER " Materials", &selected)) {
            selection_ = Selection::Materials;
        }
    }

    {
        bool selected{ selection_ == Selection::MaterialInstances };
        if (ImGui::Selectable(ICON_FA_FOLDER " Material instances", &selected)) {
            selection_ = Selection::MaterialInstances;
        }
    }

    {
        bool selected{ selection_ == Selection::Foliage };
        if (ImGui::Selectable(ICON_FA_FOLDER " Foliage", &selected)) {
            selection_ = Selection::Foliage;
        }
    }

    {
        bool selected{ selection_ == Selection::Meshes };
        if (ImGui::Selectable(ICON_FA_FOLDER " Meshes", &selected)) {
            selection_ = Selection::Meshes;
        }
    }

    {
        bool selected{ selection_ == Selection::MeshMaterials };
        if (ImGui::Selectable(ICON_FA_FOLDER " Mesh materials", &selected)) {
            selection_ = Selection::MeshMaterials;
        }
    }

    ImGui::EndChild();

    ImGui::SameLine();

    ImGui::BeginChild("Details", ImVec2(ImGui::GetWindowContentRegionWidth() - selectionWidth, 0), false, flags);
    switch (selection_) {
    case Selection::Materials:
        PopulateMaterials();
        break;
    case Selection::MaterialInstances:
        PopulateMaterialInstances();
        break;
    case Selection::Foliage:
        PopulateFoliages();
        break;
    case Selection::Meshes:
        PopulateMeshes();
        break;
    case Selection::MeshMaterials:
        PopulateMeshMaterials();
        break;
    }
    ImGui::EndChild();
}

void AssetsEditor::PopulateMaterials() {
    int counter{};
    bool end{};

    auto sorted{ MaterialService::Materials() };
    std::sort(sorted.begin(), sorted.end(), [](const auto& a, const auto& b) { return a->Category() < b->Category(); });

    for (const auto& material : sorted) {
        scope::Id id{ counter };

        GuiImages::Type icon{ GuiImages::MaterialIcon };
        if (material->Category() == "fx") {
            icon = GuiImages::FxMaterialIcon;
        }

        bool selected{ material.id() == selectedMaterial_ };
        if (ImageListItem(material->Name().c_str(), images_->Image(icon), iconWidth_, iconHeight_, &selected)) {
            selectedMaterial_ = material.id();
        }

        if (ImGui::BeginPopupContextItem("Material menu")) {
            if (ImGui::Selectable(ICON_FA_PLUS " New instance")) {
                auto inst{ MaterialService::Instantiate(material) };
                SetSelectedInstance(inst.id());
            }

            if (ImGui::Selectable(ICON_FA_TRASH " Delete")) {
                MaterialService::Remove(material);
                end = true;
            }

            ImGui::Separator();

            if (ImGui::Selectable(ICON_FA_RECYCLE " Reload")) {
                PipelineCache::Instance().Invalidate(*material);
                MaterialService::Reload(material);
            }

            ImGui::EndPopup();

            if (end) {
                break;
            }
        }
    }
}

void AssetsEditor::PopulateMaterialInstances() {
    int counter{};
    for (const auto& inst : MaterialService::Instances("material")) {
        scope::Id id{ counter };

        bool selected{ inst.id() == selectedInstance_ };
        if (ImageListItem(inst->Name().c_str(), images_->Image(GuiImages::MaterialIcon), iconWidth_, iconHeight_, &selected)) {
            selectedInstance_ = inst.id();
        }

        gui_->DragAndDrop().CanDragMaterialInstance(inst);

        if (ImGui::BeginPopupContextItem("Instance menu")) {
            bool end{};

            if (ImGui::Selectable(ICON_FA_COPY " Copy")) {
                MaterialService::Instantiate(*inst);
            }

            if (ImGui::Selectable(ICON_FA_TRASH " Delete")) {
                MaterialService::RemoveInstance(inst);
                end = true;
            }

            ImGui::EndPopup();

            if (end) {
                break;
            }
        }
    }
}

void AssetsEditor::PopulateFoliages() {
    int counter{};
    for (const auto& foliage : gfx::FoliageService::Foliages()) {
        scope::Id id{ counter };

        bool selected{ foliage.id() == selectedFoliage_ };
        if (ImageListItem(foliage->name.c_str(), images_->Image(GuiImages::FoliageIcon), iconWidth_, iconHeight_, &selected)) {
            selectedFoliage_ = foliage.id();
        }

        gui_->DragAndDrop().CanDragFoliage(foliage);
    }
}

void AssetsEditor::PopulateMeshes() {
    int counter{};
    Meshes::Each([&](const auto& mesh) {
        scope::Id id{ counter };

        bool selected{ mesh.id() == selectedMesh_ };
        if (ImageListItem(mesh->Name().c_str(), images_->Image(GuiImages::MeshIcon), iconWidth_, iconHeight_, &selected)) {
            selectedMesh_ = mesh.id();
        }

        gui_->DragAndDrop().CanDragMesh(mesh);
    });
}

void AssetsEditor::PopulateMeshMaterials() {
    int counter{};
    MeshMaterials::Each([&](const auto& mesh) {
        scope::Id id{ counter };

        bool selected{ mesh.id() == selectedMeshMaterial_ };
        if (ImageListItem(mesh->mesh->Name().c_str(), images_->Image(GuiImages::MeshIcon), iconWidth_, iconHeight_, &selected)) {
            selectedMeshMaterial_ = mesh.id();
        }

        gui_->DragAndDrop().CanDragMeshMaterial(mesh);
    });
}

} // namespace ugine::gui
