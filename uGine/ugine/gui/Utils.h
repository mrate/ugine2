﻿#pragma once

#include <ugine/gfx/rendering/MeshMaterial.h>

#include <imgui.h>

#include <glm/glm.hpp>

#include <filesystem>
#include <optional>
#include <string>
#include <vector>

namespace ugine {
namespace core {
    struct Transformation;
}
namespace gfx {
    struct Color;
}
} // namespace ugine

namespace ugine::gui {

class Gui;
class GuiImages;

extern const std::vector<std::wstring> TEXTURE_FILES_FILTER;
extern const std::vector<std::wstring> TEXTURE_CUBE_FILES_FILTER;
extern const std::vector<std::wstring> MESH_FILES_FILTER;
extern const std::vector<std::wstring> MATERIAL_FILES_FILTER;
extern const std::vector<std::wstring> FOLIAGE_FILES_FILTER;

void MatrixToFloatArray(const glm::mat4& mat, float* array);
void FloatArrayToMatrix(const float* array, glm::mat4& mat);

bool Vec2Gui(const char* title, glm::vec2& vec, float min = 0.0f, float max = 0.0f, float speed = 1.0f);
bool Vec3Gui(const char* title, glm::vec3& vec, float min = 0.0f, float max = 0.0f, float speed = 1.0f);
bool Vec4Gui(const char* title, glm::vec4& vec, float min = 0.0f, float max = 0.0f, float speed = 1.0f);
bool QuatGui(const char* title, glm::fquat& q, float min = 0.0f, float max = 0.0f, float speed = 1.0f);
bool ColorGui(const char* title, gfx::Color& color);
bool TransformationGui(const char* name, core::Transformation& transformation);

bool ImageButton(const char* id, ImTextureID texture, const ImVec2& size);
bool ImageListItem(const char* label, ImTextureID texture, float width, float height, bool* selected = nullptr);

std::optional<std::filesystem::path> OpenFileDialog(
    const char* id, const Gui& gui, const GuiImages& images, const char* label, const std::vector<std::wstring>& filter);
std::vector<std::filesystem::path> OpenFilesDialog(
    const char* id, const Gui& gui, const GuiImages& images, const char* label, const std::vector<std::wstring>& filter);

bool LoadMeshMaterial(const Gui& gui, const GuiImages& images, gfx::MeshMaterialRef& meshMat);

namespace scope {
    class Id {
    public:
        UGINE_NO_COPY(Id)

        explicit Id(int& id) {
            ImGui::PushID(id++);
        }

        explicit Id(const char* name) {
            ImGui::PushID(name);
        }

        ~Id() {
            ImGui::PopID();
        }
    };

    class Indent {
    public:
        UGINE_NO_COPY(Indent)

        explicit Indent(float i = 32.0f)
            : i_(i) {
            ImGui::Indent(i_);
        }

        ~Indent() {
            ImGui::Unindent(i_);
        }

    private:
        float i_;
    };

    class Color {
    public:
        UGINE_NO_COPY(Color)

        Color(ImGuiCol col, const ImVec4& value) {
            ImGui::PushStyleColor(col, value);
        }

        ~Color() {
            ImGui::PopStyleColor();
        }
    };

    class StyleVar {
    public:
        UGINE_NO_COPY(StyleVar)

        template <typename T> StyleVar(ImGuiStyleVar style, const T& value) {
            ImGui::PushStyleVar(style, value);
        }

        ~StyleVar() {
            ImGui::PopStyleVar();
        }
    };

    class ItemWidth {
    public:
        UGINE_NO_COPY(ItemWidth)

        explicit ItemWidth(float w) {
            ImGui::PushItemWidth(w);
        }

        ~ItemWidth() {
            ImGui::PopItemWidth();
        }
    };

} // namespace scope

} // namespace ugine::gui
