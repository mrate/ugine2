﻿#include "SceneEditor.h"

#include "Gui.h"
#include "GuiImages.h"
#include "PropertyTable.h"

#include <ugine/core/Component.h>
#include <ugine/core/ConfigService.h>
#include <ugine/core/GameObject.h>
#include <ugine/utils/Log.h>

#include <glm/gtc/type_ptr.hpp>

using namespace ugine::core;
using namespace ugine::gfx;
using namespace ugine::gui;

namespace {
entt::entity dragDrop{};
}

namespace ugine::gui {

#define EDIT_PROPERTY(table, name, member, ...)                                                                                                                \
    {                                                                                                                                                          \
        auto value{ core::ConfigService::member() };                                                                                                           \
        if (table.EditProperty(name, value, __VA_ARGS__)) {                                                                                                    \
            core::ConfigService::Set##member(value);                                                                                                           \
        }                                                                                                                                                      \
    }

void SceneEditor::PopulateSceneSettings(core::Scene& scene) const {
    const auto flags{ ImGuiTreeNodeFlags_DefaultOpen };

    if (ImGui::CollapsingHeader(ICON_FA_CLOUD " Environment", flags)) {
        PropertyTable table("Environment", gui_, images_);

        auto skyBox{ scene.SkyBox() };
        if (table.EditProperty("Skybox", skyBox, true)) {
            scene.SetSkyBox(skyBox);
        }

        auto sky{ scene.DynamicSky() };
        if (table.EditProperty("Dynamic sky", sky)) {
            scene.SetDynamicSky(sky);
        }

        {
            const auto& lightGO{ scene.SunLight() };
            table.BeginProperty("Sun light");
            if (ImGui::BeginCombo("", lightGO ? lightGO.Name().c_str() : "<none>")) {
                for (auto [ent, light] : scene.Registry().view<core::LightComponent>().each()) {
                    auto go{ scene.Get(ent) };
                    if (light.light.type == Light::Type::Directional) {
                        bool selected{ go == lightGO };
                        if (ImGui::Selectable(go.Name().c_str(), &selected)) {
                            scene.SetSunLight(go);
                        }
                    }
                }

                ImGui::EndCombo();
            }
            table.EndProperty();
        }
    }

    if (ImGui::CollapsingHeader(ICON_FA_LIGHTBULB " Lighting", flags)) {
        PropertyTable table("Lighting", gui_, images_);

        {
            const auto resolution{ core::ConfigService::ShadowMapResolution() };
            table.ConstProperty("Shadowmap resolution", "%dx%d", resolution.width, resolution.height);
        }

        {
            const auto resolution{ core::ConfigService::ShadowMapCubeResolution() };
            table.ConstProperty("Shadowmap cube resolution", "%dx%d", resolution.width, resolution.height);
        }

        for (int i = 0; i < gfx::limits::CSM_LEVELS; ++i) {
            float csm{ core::ConfigService::DirLightCsmSize(i) };
            if (table.EditProperty(ICON_FA_RULER " CSM " + std::to_string(i), csm)) {
                core::ConfigService::SetDirLightCsmSize(i, csm);
            }
        }

        EDIT_PROPERTY(table, "Volumetric intensity", VolumetricIntensity, 0.0f, 1.0f);
        EDIT_PROPERTY(table, "Shadow intensity", ShadowIntensity, 0.0f, 1.0f);
        EDIT_PROPERTY(table, "Light shafts sun size", LightShaftsSunSize, 2.0f, 20.0f);
        EDIT_PROPERTY(table, "Light shafts size", LightShaftsSize, 0.1f, 1.0f);
        EDIT_PROPERTY(table, "Light shafts weight", LightShaftsWeight, 0.0f, 1.0f);
        EDIT_PROPERTY(table, "Light shafts decay", LightShaftsDecayFallof, 0.0f, 1.0f);
        EDIT_PROPERTY(table, "Light shafts exposure", LightShaftsExposure, 0.0f, 10.0f);
        EDIT_PROPERTY(table, "Light shafts blur rad.", LightShaftsBlurRadius, 0.0f, 2.0f);
        EDIT_PROPERTY(table, "Light shafts blur amt.", LightShaftsBlurAmount, 0.0f, 2.0f);
    }

    if (ImGui::CollapsingHeader(ICON_FA_ADJUST " SSAO", flags)) {
        PropertyTable table("SSAO", gui_, images_);

        EDIT_PROPERTY(table, "Radius", SsaoRadius, 0.0f, 2.0f);
        EDIT_PROPERTY(table, "Bias", SsaoBias, 0.0f, 0.001f);
    }
}

void SceneEditor::PopulateGameObject(core::GameObject& go, int& id) const {
    scope::Id sId(id);

    scope::Color color(ImGuiCol_Button, ImVec4(0, 0, 0, 0));
    scope::Color colorHover(ImGuiCol_ButtonHovered, ImVec4(0, 0, 0, 0));
    scope::Color colorActive(ImGuiCol_ButtonActive, ImVec4(0, 0, 0, 0));

    const uint32_t goFlags{ go.Has<GuiFlags>() ? go.Component<GuiFlags>().flags : 0 };

    int flags{ ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_FramePadding | ImGuiTreeNodeFlags_DefaultOpen
        | (go.ChildrenCount() > 0 ? 0 : ImGuiTreeNodeFlags_Leaf) };

    if ((goFlags & GuiFlags::Selected) != 0) {
        flags |= ImGuiTreeNodeFlags_Selected;
    }
    flags |= ImGuiTreeNodeFlags_SpanAvailWidth;

    auto popStyle{ false };
    if (!go.IsEnabled()) {
        ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.2f, 0.2f, 0.2f, 1.0f));
        popStyle = true;
    }

    auto pos{ ImGui::GetCursorPosX() };

    //{
    //    ImGui::SetCursorPosX(8);

    //    if (ImGui::ImageButton(
    //            images_->Image(go.IsEnabled() ? GuiImages::View : GuiImages::Hidden), images_->Size(), ImVec2(0.0f, 0.0f), ImVec2(1.0f, 1.0f), 0)) {
    //        go.SetEnabled(!go.IsEnabled());
    //    }
    //}

    //ImGui::PushItemWidth(-1);
    //ImGui::SetCursorPosX(pos + images_->Size().x);

    auto open{ ImGui::TreeNodeEx(
        go.Name().c_str(), flags, "%s %s", /*go.IsEnabled() ? ICON_FA_EYE : ICON_FA_EYE_SLASH,*/ PopulateIcons(go).c_str(), go.Name().c_str()) };

    gui_->DragAndDrop().CanDragGameObject(go);

    GameObject drop;
    if (gui_->DragAndDrop().DropGameObject(drop)) {
        if (drop != go) {
            bool fail{};
            for (auto p = go.Parent(); p; p = p.Parent()) {
                if (p == drop) {
                    fail = true;
                    break;
                }
            }

            if (!fail) {
                if (drop.Parent()) {
                    drop.Parent().RemoveChild(drop);
                }

                go.AddChild(drop);
            }
        }
    }

    if (ImGui::IsItemClicked()) {
        OnClicked(go);
    }

    const auto rightEdge{ ImGui::GetCursorPosX() + ImGui::GetColumnWidth() };

    if (open) {
        for (auto child{ go.FirstChild() }; child.IsValid(); child = child.NextSibling()) {
            PopulateGameObject(child, id);
        }

        ImGui::TreePop();
    }

    if (popStyle) {
        ImGui::PopStyleColor();
    }
}

std::string SceneEditor::PopulateIcons(const core::GameObject& go) const {
    std::stringstream str;

    if (go.Has<MeshComponent>()) {
        str << ICON_FA_CUBE << " ";
    }

    if (go.Has<SkinnedMeshComponent>()) {
        str << ICON_FA_BONE << " ";
    }

    if (go.Has<AnimatorComponent>()) {
        str << ICON_FA_FILM << " ";
    }

    if (go.Has<ParticleComponent>()) {
        str << ICON_FA_ATOM << " ";
    }

    if (go.Has<TerrainComponent>()) {
        str << ICON_FA_MOUNTAIN << " ";
    }

    if (go.Has<FoliageComponent>()) {
        str << ICON_FA_TREE << " ";
    }

    if (go.Has<CameraComponent>()) {
        str << ICON_FA_CAMERA << " ";
    }

    if (go.Has<LightComponent>()) {
        str << ICON_FA_LIGHTBULB << " ";
    }

    if (go.Has<NativeScriptComponent>()) {
        str << ICON_FA_CLIPBOARD << " ";
    }

    return str.str();
}

void SceneEditor::PopulateGameObjects(core::Scene& scene) const {
    bool selected{ true };
    for (auto [ent, gui] : scene.Registry().view<GuiFlags>().each()) {
        auto flags{ gui.flags };
        if (flags & GuiFlags::Selected) {
            selected = false;
            break;
        }
    }

    if (ImGui::Selectable(ICON_FA_SITEMAP "  <root>", &selected)) {
        OnClicked(GameObject{});
    }

    GameObject drop;
    if (gui_->DragAndDrop().DropGameObject(drop)) {
        if (drop.Parent()) {
            drop.Parent().RemoveChild(drop);
        }
    }

    int id{ 0 };
    scene.Registry().group<core::Tag>(entt::exclude<core::Parent>).each([&](auto ent, const Tag&) {
        auto go{ scene.Get(ent) };
        PopulateGameObject(go, id);
    });

    //ImGui::Columns(1);
}

} // namespace ugine::gui
