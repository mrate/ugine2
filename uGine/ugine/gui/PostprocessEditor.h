﻿#pragma once

#include <memory>
#include <vector>

namespace ugine::gfx {
class Bloom;
class DepthOfField;
class Fog;
class LensFlare;
class MotionBlur;
class Postprocess;
class SSAO;
class ToneMapping;
} // namespace ugine::gfx

namespace ugine::gui {

class PostprocessEditor {
public:
    void Populate(const std::vector<std::shared_ptr<gfx::Postprocess>>& fx) const;

private:
    void Populate(gfx::ToneMapping& fx) const;
    void Populate(gfx::Bloom& fx) const;
    void Populate(gfx::Fog& fx) const;
    void Populate(gfx::SSAO& fx) const;
    void Populate(gfx::DepthOfField& fx) const;
    void Populate(gfx::MotionBlur& fx) const;
    void Populate(gfx::LensFlare& fx) const;

    //void Populate(gfx::ScreenSpaceReflections& fx);
};

} // namespace ugine::gui
