﻿#pragma once

#include <glm/glm.hpp>

namespace ugine::gui {

class ViewCube {
public:
    static bool Render(glm::mat4& viewMatrix, float length, float x, float y, float width, float height);
};

} // namespace ugine::gui
