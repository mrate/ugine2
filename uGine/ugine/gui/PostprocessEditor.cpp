﻿#include "PostprocessEditor.h"

#include "PropertyTable.h"

#include <ugine/gfx/postprocess/Bloom.h>
#include <ugine/gfx/postprocess/DepthOfField.h>
#include <ugine/gfx/postprocess/Fog.h>
#include <ugine/gfx/postprocess/LensFlare.h>
#include <ugine/gfx/postprocess/MotionBlur.h>
#include <ugine/gfx/postprocess/SSAO.h>
#include <ugine/gfx/postprocess/ToneMapping.h>

#include <imgui.h>

namespace ugine::gui {

void PostprocessEditor::Populate(const std::vector<std::shared_ptr<gfx::Postprocess>>& fx) const {
    int i{};
    for (auto& f : fx) {
        scope::Id id(i);

        if (auto* tonemapping = dynamic_cast<gfx::ToneMapping*>(f.get())) {
            Populate(*tonemapping);
        } else if (auto* bloom = dynamic_cast<gfx::Bloom*>(f.get())) {
            Populate(*bloom);
        } else if (auto* fog = dynamic_cast<gfx::Fog*>(f.get())) {
            Populate(*fog);
        } else if (auto* ssao = dynamic_cast<gfx::SSAO*>(f.get())) {
            Populate(*ssao);
        } else if (auto* mb = dynamic_cast<gfx::MotionBlur*>(f.get())) {
            Populate(*mb);
        } else if (auto* dof = dynamic_cast<gfx::DepthOfField*>(f.get())) {
            Populate(*dof);
        } else if (auto* lf = dynamic_cast<gfx::LensFlare*>(f.get())) {
            Populate(*lf);
        }
    }
}

void PostprocessEditor::Populate(gfx::ToneMapping& fx) const {
    using namespace ugine::gfx;

    if (ImGui::CollapsingHeader("Tone mapping")) {
        PropertyTable table("ToneMapping");

        table.EditProperty("Gamma", fx, &ToneMapping::Gamma, &ToneMapping::SetGamma, 0.0f, 4.0f);
        table.EditProperty("Exposure", fx, &ToneMapping::Exposure, &ToneMapping::SetExposure, 0.0f, 5.0f);
        table.EditProperty("Algorithm", fx, &ToneMapping::Algorithm, &ToneMapping::SetAlgorithm, 0, 1);
    }
}

void PostprocessEditor::Populate(gfx::Bloom& fx) const {
    using namespace ugine::gfx;

    if (ImGui::CollapsingHeader("Bloom")) {
        PropertyTable table("Bloom");

        table.EditProperty("Threshold", fx, &Bloom::Threshold, &Bloom::SetThreshold, 0.0f, 10.0f);
        table.EditProperty("Scale", fx, &Bloom::ThresholdScale, &Bloom::SetThresholdScale, 0.0f, 10.0f);
        table.EditProperty("Amount", fx, &Bloom::Amount, &Bloom::SetAmount, 0.0f, 1.0f);
        table.EditProperty("Debug", fx, &Bloom::Debug, &Bloom::SetDebug, 0.0f, 1.0f);
        table.EditProperty("Blur count", fx, &Bloom::BlurCount, &Bloom::SetBlurCount, 1, 20);
    }
}

void PostprocessEditor::Populate(gfx::Fog& fx) const {
    using namespace ugine::gfx;

    if (ImGui::CollapsingHeader("Fog")) {
        PropertyTable table("Fog");

        table.EditProperty("Enabled", fx, &Fog::Enabled, &Fog::SetEnabled);
        table.EditProperty("Color", fx, &Fog::Color, &Fog::SetColor, 0.0f, 10.0f, 0.1f);
        table.EditProperty("Sun color", fx, &Fog::SunColor, &Fog::SetSunColor, 0.0f, 10.0f, 0.1f);
        table.EditProperty("Near / Far", fx, &Fog::NearFar, &Fog::SetNearFar, 0.0f, 100.0f, 0.5f);
        table.EditProperty("Min / Max", fx, &Fog::MinMax, &Fog::SetMinMax, 0.0f, 1.0f, 0.01f);
    }
}

void PostprocessEditor::Populate(gfx::SSAO& fx) const {
    using namespace ugine::gfx;

    if (ImGui::CollapsingHeader("SSAO")) {
        PropertyTable table("SSAO");

        table.EditProperty("Strength", fx, &SSAO::Strength, &SSAO::SetStrength, 0.0f, 1.0f);
        table.EditProperty("Radius", fx, &SSAO::Radius, &SSAO::SetRadius, 0.0f, 1.0f);
        table.EditProperty("Debug", fx, &SSAO::Debug, &SSAO::SetDebug);
    }
}

void PostprocessEditor::Populate(gfx::DepthOfField& fx) const {
    using namespace ugine::gfx;

    if (ImGui::CollapsingHeader("Depth of field")) {
        PropertyTable table("Depth of field");

        table.EditProperty("Enabled", fx, &DepthOfField::Enabled, &DepthOfField::SetEnabled);
        table.EditProperty("Distance", fx, &DepthOfField::Distance, &DepthOfField::SetDistance, 0.0f, 100.0f);
        table.EditProperty("Range", fx, &DepthOfField::Range, &DepthOfField::SetRange, 0.0f, 100.0f);
        table.EditProperty("Size", fx, &DepthOfField::Size, &DepthOfField::SetSize, 0, 10);
        table.EditProperty("Separation", fx, &DepthOfField::Separation, &DepthOfField::SetSeparation, 0.0f, 20.0f);
        table.EditProperty("Min threshold", fx, &DepthOfField::MinThreshold, &DepthOfField::SetMinThreshold, 0.0f, 10.0f);
        table.EditProperty("Max threshold", fx, &DepthOfField::MaxThreshold, &DepthOfField::SetMaxThreshold, 0.0f, 10.0f);
    }
}

void PostprocessEditor::Populate(gfx::MotionBlur& fx) const {
    using namespace ugine::gfx;

    if (ImGui::CollapsingHeader("Motion blur")) {
        PropertyTable table("Motion blur");

        table.EditProperty("Strength", fx, &MotionBlur::Strength, &MotionBlur::SetStrength, 0.0f, 1.0f);
    }
}

void PostprocessEditor::Populate(gfx::LensFlare& fx) const {
    using namespace ugine::gfx;

    if (ImGui::CollapsingHeader("Lens flare")) {
        PropertyTable table("Lens flare");

        table.EditProperty("Threshold", fx, &LensFlare::Threshold, &LensFlare::SetThreshold, 0.0f, 10.0f);
        table.EditProperty("Scale", fx, &LensFlare::ThresholdScale, &LensFlare::SetThresholdScale, 0.0f, 1.0f);
        table.EditProperty("Ghost", fx, &LensFlare::Ghost, &LensFlare::SetGhost, 0, 10);
        table.EditProperty("Ghost dispersal", fx, &LensFlare::GhostDispersal, &LensFlare::SetGhostDispersal, 0.0f, 10.0f);
        table.EditProperty("Distortion", fx, &LensFlare::GhostDistortion, &LensFlare::SetGhostDistortion, 0.0f, 10.0f);
        table.EditProperty("Halo width", fx, &LensFlare::GhostHaloWidth, &LensFlare::SetGhostHaloWidth, 0.0f, 10.0f);
    }
}

//void PostprocessEditor(DepthOfField& fx) {
//    if (ImGui::CollapsingHeader("Depth of Field")) {
//        PropertyTable table("DoF");
//
//        table.EditProperty("Enabled", fx, &DepthOfField::Enabled, &DepthOfField::SetEnabled);
//        table.EditProperty("Distance", fx, &DepthOfField::Distance, &DepthOfField::SetDistance, 0.0f, 100.0f);
//        table.EditProperty("Range", fx, &DepthOfField::Range, &DepthOfField::SetRange, 0.0f, 100.0f);
//        //table.EditProperty("Near", fx, &DepthOfField::Near, &DepthOfField::SetNear, 0.0f, 100.0f);
//        //table.EditProperty("Far", fx, &DepthOfField::Far, &DepthOfField::SetFar, 0.0f, 100.0f);
//        table.EditProperty("Size", fx, &DepthOfField::Size, &DepthOfField::SetSize, 0, 10);
//        table.EditProperty("Separation", fx, &DepthOfField::Separation, &DepthOfField::SetSeparation, 0.0f, 5.0f);
//        table.EditProperty("Min threshold", fx, &DepthOfField::MinThreshold, &DepthOfField::SetMinThreshold, 0.0f, 1.0f);
//        table.EditProperty("Max threshold", fx, &DepthOfField::MaxThreshold, &DepthOfField::SetMaxThreshold, 0.0f, 1.0f);
//    }
//}
//
//
//void PostprocessEditor(ScreenSpaceReflections& fx) {
//    if (ImGui::CollapsingHeader("SSR")) {
//        PropertyTable table("SSR");
//
//        table.EditProperty("Step", fx, &ScreenSpaceReflections::Step, &ScreenSpaceReflections::SetStep, 0.01f, 1.0f);
//        table.EditProperty("Min. ray step", fx, &ScreenSpaceReflections::MinRayStep, &ScreenSpaceReflections::SetMinRayStep, 0.1f, 1.0f);
//        table.EditProperty("Max steps", fx, &ScreenSpaceReflections::MaxSteps, &ScreenSpaceReflections::SetMaxSteps, 1.0f, 100.0f);
//        table.EditProperty(
//            "Num. binary search steps", fx, &ScreenSpaceReflections::NumBinarySearchSteps, &ScreenSpaceReflections::SetNumBinarySearchSteps, 1, 10);
//        table.EditProperty(
//            "Reflection specular fallow", fx, &ScreenSpaceReflections::ReflectionSpecFallof, &ScreenSpaceReflections::SetReflectionSpecFallof, 0.1f, 10.0f);
//    }
//}

} // namespace ugine::gui
