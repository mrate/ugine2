﻿#include "MaterialEditor.h"

#include "GuiImages.h"
#include "PropertyTable.h"
#include "Utils.h"

#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/rendering/Material.h>
#include <ugine/gfx/rendering/MaterialInstance.h>

using namespace ugine::gfx;
using namespace ugine::gui;

namespace {

template <typename T> void PopulateMaterialParam(PropertyTable& table, MaterialInstance& instance, MaterialParam& param) {
    T val;
    instance.Default().GetUniformVal(param.id, val);
    if (table.EditProperty(param.readableName, val)) {
        instance.Default().SetUniformVal(param.id, val);
    }
}

} // namespace

namespace ugine::gui {

void MaterialEditor::PopulateMaterial(gfx::MaterialInstance& material) {
    scope::Indent ident;

    if (ImGui::Button(ICON_FA_COPY)) {
        MaterialService::Instantiate(material);
    }

    PropertyTable table("Material");

    table.ConstProperty("Material", material.Material().Name());
    table.ConstProperty("Transparent", material.Material().Transparent() ? "Yes" : "No");
    table.EditProperty("Name", material, &MaterialInstance::Name, &MaterialInstance::SetName);

    for (auto& param : material.Material().DefaultPass().Params()) {
        scope::Id id(param.name.c_str());

        switch (param.type) {
        case MaterialParam::Type::Float:
            PopulateMaterialParam<float>(table, material, param);
            break;
        case MaterialParam::Type::Float2:
            PopulateMaterialParam<glm::vec2>(table, material, param);
            break;
        case MaterialParam::Type::Float3:
            PopulateMaterialParam<glm::vec3>(table, material, param);
            break;
        case MaterialParam::Type::Float4:
            PopulateMaterialParam<glm::vec4>(table, material, param);
            break;
        case MaterialParam::Type::Int:
            PopulateMaterialParam<int>(table, material, param);
            break;
        case MaterialParam::Type::Int2:
            PopulateMaterialParam<glm::ivec2>(table, material, param);
            break;
        case MaterialParam::Type::Int3:
            PopulateMaterialParam<glm::ivec3>(table, material, param);
            break;
        case MaterialParam::Type::Int4:
            PopulateMaterialParam<glm::ivec4>(table, material, param);
            break;
        case MaterialParam::Type::Color:
            PopulateMaterialParam<Color>(table, material, param);
            break;
        case MaterialParam::Type::Texture2D:
        case MaterialParam::Type::Texture3D:
        case MaterialParam::Type::TextureCube: {
            table.BeginProperty(param.name);

            TextureViewRef texture;
            material.Default().GetTexture(param.id, texture);

            TextureRef tex;

            if (texture) {
                tex = TextureViews::TextureRef(texture);
            }

            if (ImGui::Button(ICON_FA_MINUS)) {
                material.Default().SetTexture(param.id, {});
            }

            ImGui::SameLine();

            if (ImGui::Button(ICON_FA_EYE)) {
                // TODO:
            }

            ImGui::SameLine();

            auto fileName{ OpenFileDialog("##texture", *gui_, *images_, tex ? tex->FileName().c_str() : "<none>",
                param.type == MaterialParam::Type::TextureCube ? TEXTURE_CUBE_FILES_FILTER : TEXTURE_FILES_FILTER) };

            if (fileName) {
                // TODO: Mips.
                const auto cubeMap{ param.type == MaterialParam::Type::TextureCube };

                TextureRef textureRef;
                if (cubeMap) {
                    textureRef = assets::AssetService::LoadCubeTexture(*fileName);
                } else {
                    textureRef = assets::AssetService::LoadTexture(*fileName, false);
                }

                auto sampleTexture{ TEXTURE_VIEW_ADD(
                    textureRef, TextureView::CreateSampled(*textureRef, cubeMap ? vk::ImageViewType::eCube : vk::ImageViewType::e2D)) };

                material.Default().SetTexture(param.id, sampleTexture);
            }

            table.EndProperty();
            break;
        }
        }
    }
}

} // namespace ugine::gui
