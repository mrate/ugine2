﻿#pragma once

#include "Utils.h"

#include <ugine/assets/AssetService.h>
#include <ugine/core/Transformation.h>
#include <ugine/gfx/Color.h>
#include <ugine/gfx/Storages.h>
#include <ugine/utils/Log.h>

#include <imgui.h>

#include <glm/glm.hpp>

#include <map>
#include <string_view>

namespace ugine::gui {

class Gui;
class GuiImages;

struct PropertyTable {
    PropertyTable(const std::string_view& id, const Gui* gui = nullptr, const GuiImages* guiImages = nullptr)
        : gui_{ gui }
        , guiImages_{ guiImages } {
        ImGui::Columns(2);
        ImGui::PushID(id.data());
    }

    ~PropertyTable() {
        ImGui::PopID();
        ImGui::Columns(1);
    }

    // Getter / Setter.
    template <typename T> bool GuiWidget(T& object, float (T::*getter)() const, void (T::*setter)(float), float min, float max) {
        auto value{ (object.*(getter))() };
        if (ImGui::SliderFloat("", &value, min, max)) {
            (object.*(setter))(value);
            return true;
        }
        return false;
    }

    template <typename T> bool GuiWidget(T& object, int (T::*getter)() const, void (T::*setter)(int), int min, int max) {
        auto value{ (object.*(getter))() };
        if (ImGui::SliderInt("", &value, min, max)) {
            (object.*(setter))(value);
            return true;
        }
        return false;
    }

    template <typename T> bool GuiWidget(T& object, const gfx::Color& (T::*getter)() const, void (T::*setter)(const gfx::Color&)) {
        auto value{ (object.*(getter))() };
        if (ColorGui("", value)) {
            (object.*(setter))(value);
            return true;
        }
        return false;
    }

    template <typename T> bool GuiWidget(T& object, bool (T::*getter)() const, void (T::*setter)(bool)) {
        auto value{ (object.*(getter))() };
        if (ImGui::Checkbox("", &value)) {
            (object.*(setter))(value);
            return true;
        }
        return false;
    }

    template <typename T>
    bool GuiWidget(T& object, const glm::vec2& (T::*getter)() const, void (T::*setter)(const glm::vec2&), float min, float max, float speed = 1.0f) {
        auto& value{ (object.*(getter))() };
        float values[2] = { value.x, value.y };
        if (ImGui::DragFloat2("", values, speed, min, max)) {
            (object.*(setter))(glm::vec2(values[0], values[1]));
            return true;
        }
        return false;
    }

    template <typename T>
    bool GuiWidget(T& object, const glm::vec3& (T::*getter)() const, void (T::*setter)(const glm::vec3&), float min, float max, float speed = 1.0f) {
        auto value{ (object.*(getter))() };
        //float values[3] = { value.x, value.y, value.z };
        //if (ImGui::DragFloat3("", values, speed, min, max)) {
        if (Vec3Gui("", value)) {
            (object.*(setter))(value);
            return true;
        }
        return false;
    }

    template <typename T>
    bool GuiWidget(T& object, const glm::vec4& (T::*getter)() const, void (T::*setter)(const glm::vec4&), float min, float max, float speed = 1.0f) {
        const auto& value{ (object.*(getter))() };
        float values[4] = { value.x, value.y, value.z, value.w };
        if (ImGui::DragFloat4("", values, speed, min, max)) {
            (object.*(setter))(glm::vec4(values[0], values[1], values[2], values[3]));
            return true;
        }
        return false;
    }

    template <typename T> bool GuiWidget(T& object, const std::string& (T::*getter)() const, void (T::*setter)(const std::string&)) {
        // TODO: String.
        char buffer[64];
        snprintf(buffer, 64, (object.*(getter))().c_str());
        if (ImGui::InputText("", buffer, 64)) {
            (object.*(setter))(buffer);
            return true;
        }
        return false;
    }

    // By value.
    bool GuiWidget(bool& value) {
        return ImGui::Checkbox("", &value);
    }

    bool GuiWidget(float& value) {
        return ImGui::InputFloat("", &value);
    }

    bool GuiWidget(float& value, float min, float max) {
        return ImGui::SliderFloat("", &value, min, max);
    }

    bool GuiWidget(int& value) {
        return ImGui::InputInt("", &value);
    }

    bool GuiWidget(int& value, int min, int max) {
        return ImGui::SliderInt("", &value, min, max);
    }

    bool GuiWidget(uint32_t& value) {
        auto i{ static_cast<int>(value) };
        if (ImGui::InputInt("", &i)) {
            value = i;
            return true;
        }
        return false;
    }

    bool GuiWidget(uint32_t& value, uint32_t min, uint32_t max) {
        auto i{ static_cast<int>(value) };
        if (ImGui::InputInt("", &i, min, max)) {
            value = i;
            return true;
        }
        return false;
    }

    bool GuiWidget(gfx::Color& value) {
        return ColorGui("", value);
    }

    bool GuiWidget(glm::vec2& value, float min = 0.0f, float max = 0.0f, float speed = 1.0f) {
        return Vec2Gui("", value, min, max, speed);
    }

    bool GuiWidget(glm::vec3& value, float min = 0.0f, float max = 0.0f, float speed = 1.0f) {
        return Vec3Gui("", value, min, max, speed);
    }

    bool GuiWidget(glm::vec4& value, float min = 0.0f, float max = 0.0f, float speed = 1.0f) {
        return Vec4Gui("", value, min, max, speed);
    }

    bool GuiWidget(glm::ivec2& value, int min = 0.0f, int max = 0.0f) {
        int val[2] = { value.x, value.y };
        if (ImGui::SliderInt2("", val, min, max)) {
            value = glm::ivec2(val[0], val[1]);
            return true;
        }
        return false;
    }

    bool GuiWidget(glm::ivec3& value, int min = 0.0f, int max = 0.0f, int speed = 1.0f) {
        int val[3] = { value.x, value.y, value.z };
        if (ImGui::SliderInt3("", val, min, max)) {
            value = glm::ivec3(val[0], val[1], val[2]);
            return true;
        }
        return false;
    }

    bool GuiWidget(glm::ivec4& value, int min = 0.0f, int max = 0.0f, int speed = 1.0f) {
        int val[4] = { value.x, value.y, value.z, value.w };
        if (ImGui::SliderInt4("", val, min, max)) {
            value = glm::ivec4(val[0], val[1], val[2], val[3]);
            return true;
        }
        return false;
    }

    bool GuiWidget(std::string& str) {
        // TODO: String.
        char buffer[64];
        snprintf(buffer, 64, str.c_str());
        if (ImGui::InputText("", buffer, 64)) {
            str = buffer;
            return true;
        }
        return false;
    }

    bool GuiWidget(std::filesystem::path& path, const std::vector<std::wstring>& filters) {
        auto fileName{ OpenFileDialog("", *gui_, *guiImages_, path.filename().string().c_str(), filters) };
        if (fileName.has_value()) {
            path = *fileName;
            return true;
        }
        return false;
    }

    bool GuiWidget(gfx::TextureViewRef& textureView, bool isCube = false) {
        UGINE_ASSERT(gui_ && guiImages_ && "Missing gui and guiImages in PropertyTable");

        auto texture{ textureView ? gfx::TextureViews::TextureRef(textureView) : gfx::TextureRef{} };

        if (isCube) {
            auto fileNames{ OpenFilesDialog(
                "", *gui_, *guiImages_, texture ? texture->FileName().c_str() : "<none>", isCube ? TEXTURE_CUBE_FILES_FILTER : TEXTURE_FILES_FILTER) };

            if (fileNames.size() == 6) {
                assets::SortCubeMapFiles(fileNames);
                texture = assets::AssetService::LoadCubeTexture(fileNames);

                textureView = gfx::TEXTURE_VIEW_ADD(
                    texture, gfx::TextureView::CreateSampled(*texture, vk::ImageViewType::eCube, vk::ImageAspectFlagBits::eColor, 0, 6));

                return true;
            } else if (fileNames.size() == 1) {
                texture = assets::AssetService::LoadCubeTexture(fileNames[0]);

                textureView = gfx::TEXTURE_VIEW_ADD(
                    texture, gfx::TextureView::CreateSampled(*texture, vk::ImageViewType::eCube, vk::ImageAspectFlagBits::eColor, 0, 6));

                return true;
            } else if (!fileNames.empty()) {
                UGINE_ERROR("Invalid number of files.");
            }
        } else {
            auto fileName{ OpenFileDialog(
                "", *gui_, *guiImages_, texture ? texture->FileName().c_str() : "<none>", isCube ? TEXTURE_CUBE_FILES_FILTER : TEXTURE_FILES_FILTER) };

            if (fileName) {
                texture = assets::AssetService::LoadTexture(*fileName, false);
                textureView = gfx::TEXTURE_VIEW_ADD(texture, gfx::TextureView::CreateSampled(*texture));

                return true;
            }
        }

        return false;
    }

    template <typename... Args> bool EditProperty(const std::string_view& name, Args&&... args) {
        scope::Id id(id_);

        BeginProperty(name.data());
        auto result{ GuiWidget(std::forward<Args>(args)...) };
        EndProperty();

        return result;
    }

    template <typename T> bool EditPropertyCombo(const std::string_view& name, T& value, const std::map<const char*, T>& values) {
        scope::Id id(id_);

        const char* label = "<invalid>";
        for (auto [k, v] : values) {
            if (v == value) {
                label = k;
                break;
            }
        }

        bool result{ false };

        BeginProperty(name.data());
        if (ImGui::BeginCombo("", label)) {
            for (auto [k, v] : values) {
                bool selected{ v == value };
                if (ImGui::Selectable(k, selected)) {
                    value = v;
                    result = true;
                }
            }
            ImGui::EndCombo();
        }
        EndProperty();

        return result;
    }

    void ConstProperty(const std::string_view& name, const std::string_view& value) {
        scope::Color style{ ImGuiCol_Text, ImVec4{ 0.4f, 0.4f, 0.4f, 1.0f } };
        BeginProperty(name);
        ImGui::Text(value.data());
        EndProperty();
    }

    template <typename... Args> void ConstProperty(const std::string_view& name, const std::string_view& fmt, Args... args) {
        BeginProperty(name);
        ImGui::Text(fmt.data(), args...);
        EndProperty();
    }

    template <typename... Args> void BeginProperty(const std::string_view& name, Args... args) {
        ImGui::Text(name.data(), args...);
        ImGui::NextColumn();
    }

    void EndProperty() {
        ImGui::NextColumn();
        ImGui::Separator();
    }

private:
    int id_{ 0 };

    const Gui* gui_{};
    const GuiImages* guiImages_{};
};

} // namespace ugine::gui
