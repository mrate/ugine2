﻿#include "Utils.h"
#include "Gui.h"
#include "GuiImages.h"
#include "PropertyTable.h"

#include <ugine/core/ConfigService.h>
#include <ugine/core/Transformation.h>
#include <ugine/gfx/Color.h>
#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/import/MaterialImporter.h>
#include <ugine/gfx/import/MeshLoader.h>
#include <ugine/gfx/rendering/Mesh.h>
#include <ugine/math/Math.h>

#include <imgui_internal.h>

#include <glm/gtc/type_ptr.hpp>

namespace ugine::gui {

const std::vector<std::wstring> TEXTURE_FILES_FILTER{ L"Texture Files", L"*.png;*.ktx;*.jpg", L"All files", L"*.*" };
const std::vector<std::wstring> TEXTURE_CUBE_FILES_FILTER{ L"Cube Texture Files", L"*.ktx;*.hdr;*.png;*.jpg", L"All files", L"*.*" };
const std::vector<std::wstring> MESH_FILES_FILTER{ L"Mesh files", L"*.obj;*.fbx;*.gltf", L"All files", L"*.*" };
const std::vector<std::wstring> MATERIAL_FILES_FILTER{ L"Material files (*.mat;*.imat)", L"*.mat;*.imat", L"All files", L"*.*" };
const std::vector<std::wstring> FOLIAGE_FILES_FILTER{ L"Foliage files (*.fol)", L"*.fol", L"All files", L"*.*" };

const ImVec4 COLOR_X = { 0.7f, 0.1f, 0.1f, 1.0f };
const ImVec4 COLOR_Y = { 0.1f, 0.7f, 0.1f, 1.0f };
const ImVec4 COLOR_Z = { 0.1f, 0.1f, 0.7f, 1.0f };
const ImVec4 COLOR_W = { 0.3f, 0.6f, 0.5f, 1.0f };

constexpr float HOVER{ 1.5f };

const ImVec4 COLOR_X_HOVER = { COLOR_X.x * HOVER, COLOR_X.y* HOVER, COLOR_X.z* HOVER, 1.0f };
const ImVec4 COLOR_Y_HOVER = { COLOR_Y.x * HOVER, COLOR_Y.y* HOVER, COLOR_Y.z* HOVER, 1.0f };
const ImVec4 COLOR_Z_HOVER = { COLOR_Z.x * HOVER, COLOR_Z.y* HOVER, COLOR_Z.z* HOVER, 1.0f };
const ImVec4 COLOR_W_HOVER = { COLOR_W.x * HOVER, COLOR_W.y* HOVER, COLOR_W.z* HOVER, 1.0f };

constexpr float ACTIVE{ 0.5f };

const ImVec4 COLOR_X_ACTIVE = { COLOR_X.x * ACTIVE, COLOR_X.y* ACTIVE, COLOR_X.z* ACTIVE, 1.0f };
const ImVec4 COLOR_Y_ACTIVE = { COLOR_Y.x * ACTIVE, COLOR_Y.y* ACTIVE, COLOR_Y.z* ACTIVE, 1.0f };
const ImVec4 COLOR_Z_ACTIVE = { COLOR_Z.x * ACTIVE, COLOR_Z.y* ACTIVE, COLOR_Z.z* ACTIVE, 1.0f };
const ImVec4 COLOR_W_ACTIVE = { COLOR_W.x * ACTIVE, COLOR_W.y* ACTIVE, COLOR_W.z* ACTIVE, 1.0f };

bool QuatGui(const char* title, glm::fquat& quat, float min, float max, float speed) {
    //glm::vec4 v{ quat.x, quat.y, quat.z, quat.w };
    //if (Vec4Gui(title, v, min, max, speed)) {
    //    quat = glm::normalize(glm::fquat{ v.w, v.x, v.y, v.z });
    //    return true;
    //}
    auto vec{ math::QuatToEuler(quat) };
    if (Vec3Gui(title, vec)) {
        //vec.x = std::min(std::max(vec.x, -89.9f), 89.9f);
        //vec = glm::vec3(fmod(vec.x, 90.0f), fmod(vec.y, 180.0f), vec.z);
        quat = glm::fquat(glm::radians(vec));
        return true;
    }

    return false;
}

bool Vec4Gui(const char* title, glm::vec4& vec, float min, float max, float speed) {
    scope::Id id(title);
    bool changed{ false };

    const auto size{ ImGui::GetContentRegionAvail() };
    scope::ItemWidth itemWidth((size.x - 4) / 4);
    scope::StyleVar noSpacing(ImGuiStyleVar_ItemSpacing, ImVec2{ 0, 0 });

    {
        scope::Color color(ImGuiCol_FrameBg, COLOR_X);
        scope::Color colorHover(ImGuiCol_FrameBgHovered, COLOR_X_HOVER);
        scope::Color colorActive(ImGuiCol_FrameBgActive, COLOR_X_ACTIVE);

        changed |= ImGui::DragFloat("##x", &vec.x, speed, min, max);
    }

    ImGui::SameLine();

    {
        scope::Color color(ImGuiCol_FrameBg, COLOR_Y);
        scope::Color colorHover(ImGuiCol_FrameBgHovered, COLOR_Y_HOVER);
        scope::Color colorActive(ImGuiCol_FrameBgActive, COLOR_Y_ACTIVE);

        changed |= ImGui::DragFloat("##y", &vec.y);
    }

    ImGui::SameLine();

    {
        scope::Color color(ImGuiCol_FrameBg, COLOR_Z);
        scope::Color colorHover(ImGuiCol_FrameBgHovered, COLOR_Z_HOVER);
        scope::Color colorActive(ImGuiCol_FrameBgActive, COLOR_Z_ACTIVE);

        changed |= ImGui::DragFloat("##z", &vec.z);
    }

    ImGui::SameLine();

    {
        scope::Color color(ImGuiCol_FrameBg, COLOR_W);
        scope::Color colorHover(ImGuiCol_FrameBgHovered, COLOR_W_HOVER);
        scope::Color colorActive(ImGuiCol_FrameBgActive, COLOR_W_ACTIVE);

        changed |= ImGui::DragFloat("##w", &vec.w);
    }

    return changed;
}

bool Vec3Gui(const char* title, glm::vec3& vec, float min, float max, float speed) {
    scope::Id id(title);
    bool changed{ false };

    const auto size{ ImGui::GetContentRegionAvail() };
    scope::ItemWidth itemWidth((size.x - 4) / 3);
    scope::StyleVar noSpacing(ImGuiStyleVar_ItemSpacing, ImVec2{ 0, 0 });

    {
        scope::Color color(ImGuiCol_FrameBg, COLOR_X);
        scope::Color colorHover(ImGuiCol_FrameBgHovered, COLOR_X_HOVER);
        scope::Color colorActive(ImGuiCol_FrameBgActive, COLOR_X_ACTIVE);

        changed |= ImGui::DragFloat("##x", &vec.x, speed, min, max);
    }

    ImGui::SameLine();

    {
        scope::Color color(ImGuiCol_FrameBg, COLOR_Y);
        scope::Color colorHover(ImGuiCol_FrameBgHovered, COLOR_Y_HOVER);
        scope::Color colorActive(ImGuiCol_FrameBgActive, COLOR_Y_ACTIVE);

        changed |= ImGui::DragFloat("##y", &vec.y);
    }

    ImGui::SameLine();

    {
        scope::Color color(ImGuiCol_FrameBg, COLOR_Z);
        scope::Color colorHover(ImGuiCol_FrameBgHovered, COLOR_Z_HOVER);
        scope::Color colorActive(ImGuiCol_FrameBgActive, COLOR_Z_ACTIVE);

        changed |= ImGui::DragFloat("##z", &vec.z);
    }

    return changed;
}

bool Vec2Gui(const char* title, glm::vec2& vec, float min, float max, float speed) {
    scope::Id id(title);
    bool changed{ false };

    const auto size{ ImGui::GetContentRegionAvail() };
    scope::ItemWidth itemWidth((size.x - 4) / 3);
    scope::StyleVar noSpacing(ImGuiStyleVar_ItemSpacing, ImVec2{ 0, 0 });

    {
        scope::Color color(ImGuiCol_FrameBg, COLOR_X);
        scope::Color colorHover(ImGuiCol_FrameBgHovered, COLOR_X_HOVER);
        scope::Color colorActive(ImGuiCol_FrameBgActive, COLOR_X_ACTIVE);

        changed |= ImGui::DragFloat("##x", &vec.x, speed, min, max);
    }

    ImGui::SameLine();

    {
        scope::Color color(ImGuiCol_FrameBg, COLOR_Y);
        scope::Color colorHover(ImGuiCol_FrameBgHovered, COLOR_Y_HOVER);
        scope::Color colorActive(ImGuiCol_FrameBgActive, COLOR_Y_ACTIVE);

        changed |= ImGui::DragFloat("##y", &vec.y);
    }

    return changed;
}

//bool QuatGui(const char* title, glm::fquat& q, bool direction) {
//    if (direction) {
//        if (ImGui::gizmo3D(title, q, 100.0f, imguiGizmo::modeDirPlane)) {
//            return true;
//        }
//    } else {
//        if (ImGui::gizmo3D(title, q, 100.0f, imguiGizmo::mode3Axes | imguiGizmo::cubeAtOrigin)) {
//            return true;
//        }
//    }
//
//    if (ImGui::Button("reset")) {
//        q = glm::fquat(1.0f, 0.0f, 0.0f, 0.0f);
//        return true;
//    }
//
//    return false;
//}

bool ColorGui(const char* title, gfx::Color& color) {
    float f[4] = { color.r(), color.g(), color.b(), color.a() };
    if (ImGui::ColorEdit4(title, f)) {
        color[0] = f[0];
        color[1] = f[1];
        color[2] = f[2];
        color[3] = f[3];
        return true;
    }
    return false;
}

bool TransformationGui(const char* name, core::Transformation& transformation) {
    PropertyTable table(name);

    auto changed = table.EditProperty("Position", transformation.position, -999999.9f, 999999.9f, 0.1f);

    table.BeginProperty("Rotation rotation");
    if (QuatGui("##Rotation", transformation.rotation)) {
        changed = true;
    }
    table.EndProperty();

    table.BeginProperty("Scale");
    static bool uniform{ false }; // TODO:
    //ImGui::Checkbox("Uniform", &uniform);
    //if (!uniform) {
    if (Vec3Gui("##Scale", transformation.scale)) {
        changed = true;
    }
    //} else {
    //    float scale{ transformation.scale.x };
    //    if (ImGui::DragFloat("##Scale", &scale, 0.01f)) {
    //        transformation.scale = { scale, scale, scale };
    //        changed = true;
    //    }
    //}
    table.EndProperty();

    return changed;
}

bool ImageButton(const char* id, ImTextureID texture, const ImVec2& size) {
    scope::Id i(id);
    return ImGui::ImageButton(texture, size);
}

bool ImageListItem(const char* label, ImTextureID image, float width, float height, bool* selected) {
    // TODO: Padding.

    // Based on https://github.com/dfranx/ImFileDialog/blob/main/ImFileDialog.cpp.
    ImGuiStyle& style = ImGui::GetStyle();
    ImGuiContext& g = *ImGui::GetCurrentContext();
    ImGuiWindow* window = g.CurrentWindow;
    ImVec2 pos = window->DC.CursorPos;

    float windowSpace = ImGui::GetWindowPos().x + ImGui::GetWindowContentRegionMax().x;
    bool ret = false;

    ImVec2 size{ width, height };

    if (ImGui::InvisibleButton(label, size)) {
        ret = true;
    }

    bool hovered = ImGui::IsItemHovered();
    bool active = ImGui::IsItemActive();
    bool doubleClick = ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left);
    if (doubleClick && hovered) {
        ret = true;
    }

    float iconSize = size.y - g.FontSize * 2;
    float iconPosX = pos.x + (size.x - iconSize) / 2.0f;
    ImVec2 textSize = ImGui::CalcTextSize(label, 0, true, size.x);

    bool isSelected{ selected ? *selected : false };
    if (hovered || active || isSelected) {
        window->DrawList->AddRectFilled(window->DC.LastItemRect.Min, window->DC.LastItemRect.Max,
            ImGui::ColorConvertFloat4ToU32(ImGui::GetStyle().Colors[active ? ImGuiCol_HeaderActive : (isSelected ? ImGuiCol_Header : ImGuiCol_HeaderHovered)]));
    }

    ImVec2 availSize = ImVec2(size.x, iconSize);

    float scale = std::min<float>(availSize.x / size.x, availSize.y / size.y);
    availSize.x = size.x * scale;
    availSize.y = size.y * scale;

    float previewPosX = pos.x + (size.x - availSize.x) / 2.0f;
    float previewPosY = pos.y + (iconSize - availSize.y) / 2.0f;

    window->DrawList->AddImage(image, ImVec2(previewPosX, previewPosY), ImVec2(previewPosX + availSize.x, previewPosY + availSize.y));

    window->DrawList->AddText(g.Font, g.FontSize, ImVec2(pos.x + (size.x - textSize.x) / 2.0f, pos.y + iconSize),
        ImGui::ColorConvertFloat4ToU32(ImGui::GetStyle().Colors[ImGuiCol_Text]), label, 0, size.x);

    float lastButtomPos = ImGui::GetItemRectMax().x;
    float thisButtonPos = lastButtomPos + style.ItemSpacing.x + size.x; // Expected position if next button was on same line
    if (thisButtonPos < windowSpace) {
        ImGui::SameLine();
    }

    if (selected) {
        *selected = ret;
    }

    return ret;
}

void MatrixToFloatArray(const glm::mat4& mat, float* array) {
    memcpy(array, glm::value_ptr(mat), sizeof(float) * 16);
}

void FloatArrayToMatrix(const float* array, glm::mat4& mat) {
    memcpy(glm::value_ptr(mat), array, sizeof(float) * 16);
}

std::optional<std::filesystem::path> OpenFileDialog(
    const char* id, const Gui& gui, const GuiImages& images, const char* label, const std::vector<std::wstring>& filter) {
    scope::Id sid(id);

    std::optional<std::filesystem::path> res;

    if (ImGui::Button(ICON_FA_FOLDER_OPEN)) {
        res = gui.OpenFileDialog(filter);
    }

    if (label) {
        ImGui::SameLine();
        ImGui::Text(label);
    }

    return res;
}

std::vector<std::filesystem::path> OpenFilesDialog(
    const char* id, const Gui& gui, const GuiImages& images, const char* label, const std::vector<std::wstring>& filter) {
    scope::Id sid(id);

    std::vector<std::filesystem::path> res;

    if (ImGui::Button(ICON_FA_FOLDER_OPEN)) {
        res = gui.OpenMultipleFilesDialog(filter);
    }

    if (label) {
        ImGui::SameLine();
        ImGui::Text(label);
    }

    return res;
}

bool LoadMeshMaterial(const Gui& gui, const GuiImages& images, gfx::MeshMaterialRef& meshMat) {
    bool transparent{}; // TODO:

    auto fileName{ OpenFileDialog("##mesh", gui, images, meshMat ? meshMat->mesh->Name().c_str() : "<none>", MESH_FILES_FILTER) };
    if (fileName) {
        using namespace ugine::gfx;

        MeshLoader::Settings settings{};
        settings.singleMesh = true;
        settings.preTransform = true;

        MeshLoader loader{ *fileName, settings };
        auto meshData{ loader.LoadMesh() };

        // TODO: Mesh caching.
        meshMat = MESH_MATERIAL_ADD();
        meshMat->mesh = MESH_ADD(core::ConfigService::DefaultLodLevels(), meshData.vertices, meshData.indices, false, fileName->filename().string());

        std::vector<MaterialInstanceRef> materials;
        for (const auto& m : meshData.materials) {
            auto newMat{ MaterialService::InstantiateDefault(transparent) };
            newMat->SetName(fmt::format("{}#{}", meshMat->mesh->Name(), materials.size()));
            SetupMaterial(m, newMat);
            materials.push_back(newMat);
        }

        for (const auto& submeshData : meshData.meshes) {
            MeshMaterial::SubMesh submesh;
            submesh.indexCount = submeshData.indexCount;
            submesh.indexOffset = submeshData.indexOffset;
            submesh.vertexOffset = submeshData.vertexOffset;
            submesh.material = materials[submeshData.material];
            meshMat->submeshes.push_back(submesh);
        }

        return true;
    }

    return false;
}

} // namespace ugine::gui
