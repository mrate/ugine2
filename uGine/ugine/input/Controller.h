﻿#pragma once

#include <memory>

namespace ugine::input {

class Controller {
public:
    virtual ~Controller() {}

    virtual void ReadState() = 0;
    virtual bool IsConnected() const = 0;

    virtual float LeftAnalogX() const = 0;
    virtual float LeftAnalogY() const = 0;
    virtual bool LeftAnalogPressed() const = 0;
    virtual float RightAnalogX() const = 0;
    virtual float RightAnalogY() const = 0;
    virtual bool RightAnalogPressed() const = 0;

    virtual float ButtonA() const = 0;
    virtual float ButtonB() const = 0;
    virtual float ButtonX() const = 0;
    virtual float ButtonY() const = 0;
};

using SharedController = std::shared_ptr<Controller>;

} // namespace ugine::input
