﻿#pragma once

#include "Controller.h"
#include "Input.h"

#include <map>
#include <set>
#include <vector>

namespace ugine::input {

class InputService {
public:
    static InputService& Instance();

    static void SetupDefaultMapping();

    static void SetInput(InputIdRef input, float value);
    static void AddInput(InputIdRef input, float value);

    static void SetMapping(InputIdRef input, ActionIdRef action);
    static void RemoveMapping(InputIdRef input, ActionIdRef action);

    static float GetAction(ActionIdRef action);

    static void SetKeyDown(unsigned long key);
    static void SetKeyUp(unsigned long key);
    static bool KeyDown(unsigned long key);
    static bool KeyPressed(unsigned long key);

    static void UpdateControllers();
    static void RegisterController(const SharedController& controller);
    static void UnRegisterController(const SharedController& controller);

private:
    InputService();

    InputService(const InputService&) = delete;
    InputService& operator=(const InputService&) = delete;

    InputService(InputService&&) = delete;
    InputService& operator=(InputService&&) = delete;

    std::map<ActionId, float> actionValues_;
    std::map<InputId, std::set<ActionId>> actionMappings_;

    // Key -> frame number mapping.
    std::map<unsigned long, uint64_t> keys_;

    std::vector<SharedController> controllers_;
};

} // namespace ugine::input
