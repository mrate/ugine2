﻿#pragma once

#include <string>

namespace ugine::input {

using InputId = std::string;
using ActionId = std::string;

using InputIdRef = const std::string&;
using ActionIdRef = const std::string&;

const InputId INPUT_MOUSE_X = "MOUSE_X";
const InputId INPUT_MOUSE_Y = "MOUSE_Y";
const InputId INPUT_MOUSE_Z = "MOUSE_Z";
const InputId INPUT_MOUSE_LBUTTON = "MOUSE_LBUTTON";
const InputId INPUT_MOUSE_RBUTTON = "MOUSE_RBUTTON";
const InputId INPUT_MOUSE_MBUTTON = "MOUSE_MBUTTON";

const InputId INPUT_MOUSE_DRAG_X = "MOUSE_DRAG_X";
const InputId INPUT_MOUSE_DRAG_Y = "MOUSE_DRAG_Y";

const InputId INPUT_LEFT_ANALOG_X = "LEFT_ANALOG_X";
const InputId INPUT_LEFT_ANALOG_Y = "LEFT_ANALOG_Y";
const InputId INPUT_LEFT_ANALOG_Z = "LEFT_ANALOG_Z";

const InputId INPUT_RIGHT_ANALOG_X = "RIGHT_ANALOG_X";
const InputId INPUT_RIGHT_ANALOG_Y = "RIGHT_ANALOG_Y";
const InputId INPUT_RIGHT_ANALOG_Z = "RIGHT_ANALOG_Z";

const InputId INPUT_CONTROLLER_B1 = "CONTROLLER_B1";
const InputId INPUT_CONTROLLER_B2 = "CONTROLLER_B2";
const InputId INPUT_CONTROLLER_B3 = "CONTROLLER_B3";
const InputId INPUT_CONTROLLER_B4 = "CONTROLLER_B4";

const ActionId ACTION_LEFT_ANALOG_X = "LEFT_ANALOG_X";
const ActionId ACTION_LEFT_ANALOG_Y = "LEFT_ANALOG_Y";
const ActionId ACTION_LEFT_ANALOG_Z = "LEFT_ANALOG_Z";

const ActionId ACTION_RIGHT_ANALOG_X = "RIGHT_ANALOG_X";
const ActionId ACTION_RIGHT_ANALOG_Y = "RIGHT_ANALOG_Y";
const ActionId ACTION_RIGHT_ANALOG_Z = "RIGHT_ANALOG_Z";

const ActionId ACTION_CONTROLLER_B1 = "CONTROLLER_B1";
const ActionId ACTION_CONTROLLER_B2 = "CONTROLLER_B2";
const ActionId ACTION_CONTROLLER_B3 = "CONTROLLER_B3";
const ActionId ACTION_CONTROLLER_B4 = "CONTROLLER_B4";

const ActionId ACTION_POINTER_X = "POINTER_X";
const ActionId ACTION_POINTER_Y = "POINTER_Y";
const ActionId ACTION_POINTER_Z = "POINTER_Z";
const ActionId ACTION_POINTER_B1 = "POINTER_B1";
const ActionId ACTION_POINTER_B2 = "POINTER_B2";
const ActionId ACTION_POINTER_B3 = "POINTER_B3";

} // namespace ugine::input
