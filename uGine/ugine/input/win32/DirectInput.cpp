﻿#ifdef _WIN32

#include "DirectInput.h"
#include "DirectInputDevice.h"

#include "../../core/Error.h"
#include "../../utils/Strings.h"

#include <dinput.h>

namespace {
bool GuidToString(const GUID& guid, std::string& result) {
    LPOLESTR str;
    auto res = StringFromCLSID(guid, &str);
    result = ugine::utils::ToString(str);
    CoTaskMemFree(str);

    return SUCCEEDED(res);
}

bool GuidFromString(const std::string& guidStr, GUID& result) {
    std::wstring wstr = ugine::utils::ToWString(guidStr);

    return SUCCEEDED(CLSIDFromString(wstr.c_str(), &result));
}
} // namespace

namespace ugine::input::win32 {

DirectInput::DirectInput() {
    HINSTANCE instance = GetModuleHandle(nullptr);

    if (FAILED(DirectInput8Create(instance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&directInput_, NULL))) {
        UGINE_THROW(core::Error, "Failed to create the main DirectInput 8 COM object!");
    }

    if (FAILED(directInput_->EnumDevices(DI8DEVCLASS_GAMECTRL, &StaticEnumerateGameControllers, this, DIEDFL_ATTACHEDONLY))) {
        UGINE_THROW(core::Error, "Failed to enumerate DirectInput devices!");
    }
}

DirectInput::~DirectInput() {
    for (auto& device : deviceInstances_) {
        device.second.handle->Unacquire();
    }
}

BOOL CALLBACK DirectInput::StaticEnumerateGameControllers(LPCDIDEVICEINSTANCE devInst, LPVOID pvRef) {
    auto instance = static_cast<DirectInput*>(pvRef);
    return instance->EnumerateGameControllers(devInst);
}

bool DirectInput::EnumerateGameControllers(LPCDIDEVICEINSTANCE devInst) {
    Microsoft::WRL::ComPtr<IDirectInputDevice8> device;

    if (SUCCEEDED(directInput_->CreateDevice(devInst->guidInstance, &device, NULL))) {
        std::string guid;
        GuidToString(devInst->guidInstance, guid);

        deviceInstances_.insert(std::make_pair(guid, Controller{ devInst->tszInstanceName, device }));
    }

    return DIENUM_CONTINUE;
}

std::vector<XControllerDevice::Info> DirectInput::AttachedControllers() const {
    std::vector<XControllerDevice::Info> result;

    for (auto device : deviceInstances_) {
        result.push_back({ device.second.name, device.first });
    }

    return result;
}

SharedController DirectInput::CreateController(const std::string& id) {
    Microsoft::WRL::ComPtr<IDirectInputDevice8> device;

    // When ID is empty use first controller. Otherwise use only controller with given ID.
    GUID guid;
    bool validGuid = GuidFromString(id, guid);
    bool continueOnError = id.empty() || !validGuid;

    std::shared_ptr<DirectInputDevice> instance;
    for (auto controller : deviceInstances_) {
        if (id.empty() || !validGuid || controller.first == id) {
            instance = std::make_shared<DirectInputDevice>(controller.second.handle, shared_from_this());

            if (instance || !continueOnError) {
                break;
            }
        }
    }

    return instance;
}

} // namespace ugine::input::win32

#endif
