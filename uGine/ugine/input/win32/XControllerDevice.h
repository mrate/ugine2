﻿#pragma once
#ifdef _WIN32

#include "../Controller.h"

#include <cmath>
#include <string>

namespace ugine::input::win32 {

class XControllerDevice : public Controller {
public:
    struct Info {
        std::wstring name;
        std::string id;
    };

    virtual ~XControllerDevice() {}

    float LeftAnalogX() const override {
        return m_leftAnalog.x;
    }
    float LeftAnalogY() const override {
        return m_leftAnalog.y;
    }
    bool LeftAnalogPressed() const override {
        return m_leftAnalog.pressed;
    }

    float RightAnalogX() const override {
        return m_rightAnalog.x;
    }
    float RightAnalogY() const override {
        return m_rightAnalog.y;
    }
    bool RightAnalogPressed() const override {
        return m_rightAnalog.pressed;
    }

    float ButtonA() const override {
        return m_buttons.a;
    }
    float ButtonB() const override {
        return m_buttons.b;
    }
    float ButtonX() const override {
        return m_buttons.x;
    }
    float ButtonY() const override {
        return m_buttons.y;
    }

protected:
    struct Analog {
        float x{};
        float y{};
        bool pressed{};
    };

    struct Buttons {
        bool a{};
        bool b{};
        bool x{};
        bool y{};
    };

    float DeadZoneNorm(float x, float val) const {
        if (x < 0) {
            x += val;
            x = fminf(0, fmaxf(x, -32000)); // clamp(x, -32000, 0);
        } else {
            x -= val;
            x = fminf(32000, fmaxf(x, 0)); // clamp(x, 0, 32000);
        }
        x *= (1 / 32000.0f);
        return x;
    }

    void SetState(const Analog& leftAnalog, const Analog& rightAnalog, const Buttons& buttons) {
        m_leftAnalog = leftAnalog;
        m_rightAnalog = rightAnalog;
        m_buttons = buttons;
    }

private:
    Analog m_leftAnalog{};
    Analog m_rightAnalog{};
    Buttons m_buttons{};
};

} // namespace ugine::input::win32

#endif
