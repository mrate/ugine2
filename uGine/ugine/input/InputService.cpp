﻿#include "InputService.h"
#include "Input.h"

#include "../core/CoreService.h"

namespace ugine::input {

InputService& InputService::Instance() {
    static InputService inst;
    return inst;
}

void InputService::SetupDefaultMapping() {
    Instance().SetMapping(INPUT_MOUSE_DRAG_X, ACTION_LEFT_ANALOG_X);
    Instance().SetMapping(INPUT_MOUSE_DRAG_Y, ACTION_LEFT_ANALOG_Y);

    Instance().SetMapping(INPUT_MOUSE_X, ACTION_POINTER_X);
    Instance().SetMapping(INPUT_MOUSE_Y, ACTION_POINTER_Y);
    Instance().SetMapping(INPUT_MOUSE_Z, ACTION_POINTER_Z);

    Instance().SetMapping(INPUT_MOUSE_LBUTTON, ACTION_POINTER_B1);
    Instance().SetMapping(INPUT_MOUSE_RBUTTON, ACTION_POINTER_B2);
    Instance().SetMapping(INPUT_MOUSE_MBUTTON, ACTION_POINTER_B3);

    Instance().SetMapping(INPUT_LEFT_ANALOG_X, ACTION_LEFT_ANALOG_X);
    Instance().SetMapping(INPUT_LEFT_ANALOG_Y, ACTION_LEFT_ANALOG_Y);
    Instance().SetMapping(INPUT_LEFT_ANALOG_Z, ACTION_LEFT_ANALOG_Z);

    Instance().SetMapping(INPUT_RIGHT_ANALOG_X, ACTION_RIGHT_ANALOG_X);
    Instance().SetMapping(INPUT_RIGHT_ANALOG_Y, ACTION_RIGHT_ANALOG_Y);
    Instance().SetMapping(INPUT_RIGHT_ANALOG_Z, ACTION_RIGHT_ANALOG_Z);

    Instance().SetMapping(INPUT_CONTROLLER_B1, ACTION_CONTROLLER_B1);
    Instance().SetMapping(INPUT_CONTROLLER_B2, ACTION_CONTROLLER_B2);
    Instance().SetMapping(INPUT_CONTROLLER_B3, ACTION_CONTROLLER_B3);
    Instance().SetMapping(INPUT_CONTROLLER_B4, ACTION_CONTROLLER_B4);
}

InputService::InputService() {}

void InputService::SetInput(InputIdRef input, float value) {
    auto it{ Instance().actionMappings_.find(input) };
    if (it != Instance().actionMappings_.end()) {
        for (const auto& mapping : it->second) {
            Instance().actionValues_[mapping] = value;
        }
    }
}

void InputService::AddInput(InputIdRef input, float value) {
    auto it{ Instance().actionMappings_.find(input) };
    if (it != Instance().actionMappings_.end()) {
        for (const auto& mapping : it->second) {
            Instance().actionValues_[mapping] += value;
        }
    }
}

void InputService::SetMapping(InputIdRef input, ActionIdRef action) {
    Instance().actionMappings_[input].insert(action);
}

void InputService::RemoveMapping(InputIdRef input, ActionIdRef action) {
    Instance().actionMappings_[input].erase(action);
}

float InputService::GetAction(ActionIdRef action) {
    auto it{ Instance().actionValues_.find(action) };
    return it != Instance().actionValues_.end() ? it->second : 0.0f;
}

void InputService::SetKeyDown(unsigned long key) {
    Instance().keys_.insert(std::make_pair(key, core::CoreService::Frame()));
}

void InputService::SetKeyUp(unsigned long key) {
    Instance().keys_.erase(key);
}

bool InputService::KeyDown(unsigned long key) {
    return Instance().keys_.find(key) != Instance().keys_.end();
}

bool InputService::KeyPressed(unsigned long key) {
    auto it{ Instance().keys_.find(key) };
    return it != Instance().keys_.end() && it->second == core::CoreService::Frame();
}

void InputService::UpdateControllers() {
    for (auto& ctrl : Instance().controllers_) {
        ctrl->ReadState();

        if (!ctrl->IsConnected()) {
            continue;
        }

        AddInput(INPUT_LEFT_ANALOG_X, ctrl->LeftAnalogX());
        AddInput(INPUT_LEFT_ANALOG_Y, ctrl->LeftAnalogY());

        AddInput(INPUT_RIGHT_ANALOG_X, ctrl->RightAnalogX());
        AddInput(INPUT_RIGHT_ANALOG_Y, ctrl->RightAnalogY());

        AddInput(INPUT_CONTROLLER_B1, ctrl->ButtonA());
        AddInput(INPUT_CONTROLLER_B2, ctrl->ButtonB());
        AddInput(INPUT_CONTROLLER_B3, ctrl->ButtonX());
        AddInput(INPUT_CONTROLLER_B4, ctrl->ButtonY());
    }
}

void InputService::RegisterController(const SharedController& controller) {
    Instance().controllers_.push_back(controller);
}

void InputService::UnRegisterController(const SharedController& controller) {
    auto it{ std::remove(Instance().controllers_.begin(), Instance().controllers_.end(), controller) };
    Instance().controllers_.erase(it, Instance().controllers_.end());
}

} // namespace ugine::input
