﻿#include "File.h"

#include <fstream>
#include <sstream>

#ifdef _WIN32
#include <Windows.h>
#endif

namespace ugine::utils {

std::string ReadFile(const std::filesystem::path& path) {
    std::string str;

    std::ifstream file(path.c_str());
    if (file.good()) {
        std::stringstream sourceBuffer;
        sourceBuffer << file.rdbuf();
        str = sourceBuffer.str();
    }

    return str;
}

template <typename T, typename S> std::vector<T> ReadFileLinesT(S& stream, bool skipEmpty) {
    std::vector<T> result;

    if (stream.good()) {
        while (!stream.eof()) {
            T line;
            std::getline(stream, line);
            if (!line.empty() || !skipEmpty) {
                result.push_back(line);
            }
        }
    }

    return result;
}

std::vector<std::string> ReadFileLines(const std::filesystem::path& path, bool skipEmpty) {
    std::ifstream file(path.c_str());

    return ReadFileLinesT<std::string>(file, skipEmpty);
}

std::vector<std::wstring> ReadFileLinesW(const std::filesystem::path& path, bool skipEmpty) {
    std::wifstream file(path.c_str());

    return ReadFileLinesT<std::wstring>(file, skipEmpty);
}

std::vector<uint8_t> ReadFileBinary(const std::filesystem::path& path) {
    std::ifstream file{ path, std::ios::binary };
    if (file.good()) {
        file.seekg(0, std::ios::end);
        const auto size{ file.tellg() };
        file.seekg(0, std::ios::beg);

        std::vector<uint8_t> data(size);
        file.read(reinterpret_cast<char*>(data.data()), size);
        file.close();

        return data;
    }

    return {};
}

void WriteFileBinary(const std::filesystem::path& path, const std::vector<uint8_t>& data) {
    std::ofstream file{ path, std::ios::binary };
    file.write(reinterpret_cast<const char*>(data.data()), data.size());
    file.close();
}

bool IsNewer(const std::filesystem::path& thisFile, const std::filesystem::path& thatFile) {
    return std::filesystem::last_write_time(thisFile) > std::filesystem::last_write_time(thatFile);
}

std::filesystem::path CurrentWorkingDir() {
#ifdef _WIN32
    WCHAR currentDir[256] = { 0 };
    return GetCurrentDirectory(256, currentDir) ? currentDir : L"";
#else
    static_assert(false, "Implement me");
#endif
}

std::filesystem::path MakeRelative(const std::filesystem::path& srcPath, const std::filesystem::path& file) {
    if (file.is_absolute()) {
        return file;
    } else {
        auto workingPath{ CurrentWorkingDir() };

        auto fullPath{ srcPath / file };
        return std::filesystem::relative(fullPath, workingPath);
    }
}

} // namespace ugine::utils
