﻿#include "Log.h"

#include <spdlog/sinks/stdout_color_sinks.h>

namespace ugine::utils {

void InitLogger() {
    auto logger{ spdlog::stdout_color_mt("console") };

#ifdef _DEBUG
    //logger->set_level(spdlog::level::trace);
    logger->set_level(spdlog::level::debug);
#endif

    spdlog::set_default_logger(logger);
}

} // namespace ugine::utils
