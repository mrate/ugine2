﻿#pragma once

#include <functional>
#include <map>
#include <memory>
#include <tuple>

namespace ugine::utils {

template <typename _Value, typename... _Keys> class Cache {
public:
    using KeyType = std::tuple<_Keys...>;
    using ValueType = std::shared_ptr<_Value>;
    // TODO:
    using ContainerType = std::map<KeyType, std::shared_ptr<_Value>>;
    using Constructor = std::function<std::shared_ptr<_Value>(const _Keys&...)>;

    template <typename T>
    Cache(T t)
        : constructor_(t) {}

    Cache()
        : constructor_([](const _Keys&... keys) { return std::make_shared<_Value>(keys...); }) {}

    ValueType GetByKey(const KeyType& key) {
        auto it{ cache_.find(key) };

        if (it != cache_.end()) {
            //auto result{ it->second.lock() };
            auto result{ it->second };
            if (result) {
                return result;
            }
            cache_.erase(it);
        }

        auto newObject{ std::apply(constructor_, key) };
        cache_.insert(std::make_pair(key, newObject));
        return newObject;
    }

    template <typename... _Args> ValueType Get(const _Args&... arg) {
        return GetByKey(std::make_tuple(arg...));
    }

    void Clear() {
        cache_.clear();
    }

private:
    ContainerType cache_;
    Constructor constructor_;
};

//using WeakCache<T, Args...> = Cache<T, std::weak_ptr, Args...>;

} // namespace ugine::utils
