#pragma once

#include "Locking.h"

#include <array>
#include <mutex>

namespace ugine::utils {

template <typename _T, uint32_t _Capacity, typename _MutexType = AtomicSpinLock> class ConcurentRingbuffer {
public:
    static const uint32_t Capacity{ _Capacity };
    using MutexType = _MutexType;
    using ValueType = _T;

    inline bool PushBack(const ValueType& item) {
        bool result{};

        std::scoped_lock lock{ mutex_ };
        uint32_t next = (head_ + 1) % Capacity;
        if (next != tail_) {
            data_[head_] = item;
            head_ = next;
            result = true;
        }

        return result;
    }

    inline bool PopFront(ValueType& item) {
        bool result{};

        std::scoped_lock lock{ mutex_ };
        if (tail_ != head_) {
            item = data_[tail_];
            tail_ = (tail_ + 1) % Capacity;
            result = true;
        }

        return result;
    }

private:
    std::array<ValueType, Capacity> data_;
    uint32_t head_{};
    uint32_t tail_{};
    MutexType mutex_{};
};

} // namespace ugine::utils