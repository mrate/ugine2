﻿#pragma once

#include <vector>

#ifdef _MSC_VER
// Microsoft Visual C++ Compiler
#pragma warning(push, 0)
#endif

#include <stb_image.h>

namespace ugine::utils {

struct StbiScopeHolder {
    StbiScopeHolder(unsigned char* data = nullptr)
        : data(data) {}
    StbiScopeHolder(const std::vector<unsigned char*>& data)
        : datas(data) {}

    ~StbiScopeHolder() {
        if (data) {
            stbi_image_free(data);
        }

        for (auto& d : datas) {
            stbi_image_free(d);
        }
    }

    unsigned char* data{};
    std::vector<unsigned char*> datas;
};

} // namespace ugine::utils

// Restore warning levels.
#ifdef _MSC_VER
// Microsoft Visual C++ Compiler
#pragma warning(pop)
#endif
