#pragma once

#include <atomic>

namespace ugine::utils {

class AtomicSpinLock {
public:
    AtomicSpinLock() = default;
    AtomicSpinLock(const AtomicSpinLock&) = delete;
    AtomicSpinLock& operator=(const AtomicSpinLock&) = delete;

    void lock() {
        while (lock_.test_and_set(std::memory_order_acquire))
            ;
    }

    void unlock() {
        lock_.clear(std::memory_order_release);
    }

private:
    std::atomic_flag lock_ = ATOMIC_FLAG_INIT;
};

} // namespace ugine::utils
