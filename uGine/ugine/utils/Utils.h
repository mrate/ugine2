﻿#pragma once

#include <vector>

#define UGINE_MOVABLE(T)                                                                                                                                       \
    T(T&&) = default;                                                                                                                                          \
    T& operator=(T&&) = default;

#define UGINE_NO_COPY(T)                                                                                                                                       \
    T(const T&) = delete;                                                                                                                                      \
    T& operator=(const T&) = delete;

#define UGINE_MOVABLE_ONLY(T)                                                                                                                                  \
    UGINE_NO_COPY(T)                                                                                                                                           \
    UGINE_MOVABLE(T)

#define UGINE_FLAGS(T, UT)                                                                                                                                     \
    inline T operator|(T a, T b) {                                                                                                                             \
        return static_cast<T>(static_cast<UT>(a) | static_cast<UT>(b));                                                                                        \
    }                                                                                                                                                          \
    inline UT operator&(T a, T b) {                                                                                                                            \
        return (static_cast<UT>(a) & static_cast<UT>(b));                                                                                                      \
    }                                                                                                                                                          \
    inline T& operator&=(T& a, T b) {                                                                                                                          \
        a = static_cast<T>(a & b);                                                                                                                             \
        return a;                                                                                                                                              \
    }                                                                                                                                                          \
    inline T& operator|=(T& a, T b) {                                                                                                                          \
        a = a | b;                                                                                                                                             \
        return a;                                                                                                                                              \
    }                                                                                                                                                          \
    inline T operator~(T a) {                                                                                                                                  \
        return static_cast<T>(~static_cast<UT>(a));                                                                                                            \
    }                                                                                                                                                          \
    inline bool operator==(T& a, UT b) {                                                                                                                       \
        return static_cast<UT>(a) == b;                                                                                                                        \
    }                                                                                                                                                          \
    inline bool operator!=(T& a, UT b) {                                                                                                                       \
        return !(a == b);                                                                                                                                      \
    }

namespace ugine::utils {

template <typename T> class ScopedValue {
public:
    ScopedValue(T& t, T val)
        : val_(t)
        , ref_(t) {
        ref_ = val;
    }

    ~ScopedValue() {
        ref_ = val_;
    }

private:
    T& ref_;
    T val_;
};

template <typename T> inline void EnsureSize(std::vector<T>& vec, size_t size) {
    if (vec.size() < size) {
        vec.resize(size);
    }
}

} // namespace ugine::utils
