﻿#include "Hash.h"

namespace std {

size_t hash<glm::vec3>::operator()(const glm::vec3& vec) {
    size_t res{ 0 };
    ugine::utils::HashCombine(res, vec.x, vec.y, vec.z);
    return res;
}

size_t hash<glm::vec4>::operator()(const glm::vec4& vec) {
    size_t res{ 0 };
    ugine::utils::HashCombine(res, vec.x, vec.y, vec.z, vec.w);
    return res;
}

size_t hash<glm::mat3>::operator()(const glm::mat3& mat) {
    size_t res{ 0 };
    for (int i = 0; i < 3; ++i) {
        ugine::utils::HashCombine(res, mat[i]);
    }
    return res;
}

size_t hash<glm::mat4>::operator()(const glm::mat4& mat) {
    size_t res{ 0 };
    for (int i = 0; i < 4; ++i) {
        ugine::utils::HashCombine(res, mat[i]);
    }
    return res;
}

} // namespace std
