﻿#pragma once

#include "Assert.h"

#include "../core/CoreService.h"

#include "MemAllocator.h"

#include <atomic>
#include <stdint.h>

namespace ugine::utils {

// Based on lockfree stack from Intrinsic engine: https://github.com/begla/Intrinsic
template <class T> class LockFreeStack {
public:
    LockFreeStack(uint32_t capacity) {
        data_ = reinterpret_cast<T*>(core::CoreService::MainAllocator().Alloc(capacity * sizeof(T)));
        capacity_ = capacity;
        size_ = 0u;
    }

    ~LockFreeStack() {
        core::CoreService::MainAllocator().Free(data_);
    }

    void push_back(const T& element) {
        auto oldSize = size_.fetch_add(1);
        UGINE_ASSERT(oldSize + 1u <= capacity_ && "Stack overflow");
        data_[oldSize] = element;
    }

    void pop_back() {
        const uint64_t oldSize = size_.fetch_sub(1);
        UGINE_ASSERT(oldSize - 1u <= capacity_ && "Stack underflow");
    }

    void resize(uint64_t size) {
        size_ = size;
    }

    void clear() {
        resize(0u);
    }

    bool empty() const {
        return size_ == 0u;
    }

    T& back() {
        UGINE_ASSERT(!empty());
        return data_[size_ - 1u];
    }

    const T& back() const {
        UGINE_ASSERT(!empty());
        return data_[size_ - 1u];
    }

    uint64_t capacity() const {
        return capacity_;
    }

    uint64_t size() const {
        return size_;
    }

    T& operator[](uint64_t idx) {
        return data_[idx];
    }

    T& operator[](uint64_t idx) const {
        return data_[idx];
    }

    void insert(const std::vector<T>& vals) {
        const std::atomic_uint64_t oldSize = size_.fetch_add(vals.size());
        UGINE_ASSERT(oldSize + vals.size() <= capacity_);
        memcpy(&data_[oldSize], vals.data(), vals.size() * sizeof(T));
    }

    template <typename Container> void copy(Container& vals) const {
        const uint32_t startIdx = static_cast<uint32_t>(vals.size());
        vals.resize(vals.size() + size_);
        memcpy(&vals.data()[startIdx], data_, size_ * sizeof(T));
    }

private:
    T* data_{};
    uint64_t capacity_;
    std::atomic_uint64_t size_;
};

} // namespace ugine::utils
