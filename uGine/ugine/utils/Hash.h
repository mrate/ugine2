﻿#pragma once

#include <glm/glm.hpp>

#include <functional>

namespace std {

template <> struct hash<glm::vec3> { size_t operator()(const glm::vec3& vec); };
template <> struct hash<glm::vec4> { size_t operator()(const glm::vec4& vec); };
template <> struct hash<glm::mat3> { size_t operator()(const glm::mat3& mat); };
template <> struct hash<glm::mat4> { size_t operator()(const glm::mat4& mat); };

} // namespace std

namespace ugine::utils {

inline void HashCombine(std::size_t& seed) {}

template <typename T, typename... Rest> inline void HashCombine(std::size_t& seed, const T& v, Rest... rest) {
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    HashCombine(seed, rest...);
}

} // namespace ugine::utils
