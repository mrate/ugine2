﻿#pragma once

#include <ugine/core/Error.h>

#include <nlohmann/json.hpp>

#include <glm/glm.hpp>

#include <algorithm>

namespace nlohmann {

template <> struct adl_serializer<glm::vec4> {
    static void to_json(json& j, const glm::vec4& v) {}
    static void from_json(const json& j, glm::vec4& v) {
        if (j.is_array()) {
            for (size_t i = 0; i < std::min<size_t>(4, j.size()); ++i) {
                v[static_cast<int>(i)] = j[i];
            }
            for (size_t i = j.size(); i < 4; ++i) {
                v[static_cast<int>(i)] = 0.0f;
            }
        } else if (j.is_object()) {
            v.x = j.value<float>("x", 0.0f);
            v.y = j.value<float>("y", 0.0f);
            v.z = j.value<float>("z", 0.0f);
            v.w = j.value<float>("w", 0.0f);
        } else if (j.is_number()) {
            v = glm::vec4{ static_cast<float>(j) };
        } else {
            UGINE_THROW(ugine::core::Error, "Invalid vector value.");
        }
    }
};

template <> struct adl_serializer<glm::ivec4> {
    static void to_json(json& j, const glm::ivec4& v) {}
    static void from_json(const json& j, glm::ivec4& v) {
        if (j.is_array()) {
            for (size_t i = 0; i < std::min<size_t>(4, j.size()); ++i) {
                v[static_cast<int>(i)] = j[i];
            }
            for (size_t i = j.size(); i < 4; ++i) {
                v[static_cast<int>(i)] = 0;
            }
        } else if (j.is_object()) {
            v.x = j.value<int>("x", 0);
            v.y = j.value<int>("y", 0);
            v.z = j.value<int>("z", 0);
            v.w = j.value<int>("w", 0);
        } else if (j.is_number()) {
            v = glm::ivec4{ static_cast<int>(j) };
        } else {
            UGINE_THROW(ugine::core::Error, "Invalid vector value.");
        }
    }
};

} // namespace nlohmann
