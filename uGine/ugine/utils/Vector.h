﻿#pragma once

#include "Assert.h"
#include "MemAllocator.h"
#include "StlMemAllocator.h"

#include <vector>

namespace ugine::utils {

template <typename T> using Vector = std::vector<T, StlMemAllocator<T>>;

template <typename T> class TSVector {
public:
    TSVector() = default;
    TSVector(const TSVector& other) {
        *this = other;
    }
    TSVector& operator=(const TSVector& other) {
        entities_ = other.entities_;
        counter_ = other.counter_.load();
        return *this;
    }

    void Resize(size_t size) {
        entities_.resize(size);
    }

    void Finalize() {
        entities_.resize(counter_);
    }

    const std::vector<T>& Entries() const {
        return entities_;
    }

    // Thread-Safe.
    void Add(T ent) {
        auto index{ counter_.fetch_add(1) };

        UGINE_ASSERT(index < entities_.size());

        entities_[index] = ent;
    }

    void Clear() {
        counter_ = 0;
    }

private:
    std::vector<T> entities_;
    std::atomic_uint32_t counter_;
};

} // namespace ugine::utils
