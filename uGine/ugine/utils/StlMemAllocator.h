﻿#pragma once

#include "../core/CoreService.h"

#include "MemAllocator.h"

namespace ugine::utils {

template <class T> class StlMemAllocator {
public:
    typedef T value_type;
    typedef T* pointer;
    typedef const T* const_pointer;
    typedef T& reference;
    typedef const T& const_reference;
    typedef std::size_t size_type;
    typedef std::ptrdiff_t difference_type;

    template <class U> struct rebind { typedef StlMemAllocator<U> other; };

    pointer address(reference value) const {
        return &value;
    }
    const_pointer address(const_reference value) const {
        return &value;
    }

    StlMemAllocator() throw() {}
    StlMemAllocator(const StlMemAllocator&) throw() {}
    template <class U> StlMemAllocator(const StlMemAllocator<U>&) throw() {}
    ~StlMemAllocator() throw() {}

    size_type max_size() const throw() {
        return std::numeric_limits<size_type>::max() / sizeof(T);
    }

    pointer allocate(size_type num, const void* = 0) {
        return reinterpret_cast<pointer>(core::CoreService::StlAllocator().Alloc(static_cast<uint32_t>(num * sizeof(T))));
    }

    void construct(pointer p, const T& value) {
        new (reinterpret_cast<void*>(p)) T(value);
    }

    void destroy(pointer p) {
        p->~T();
    }

    void deallocate(pointer p, size_type num) {
        core::CoreService::StlAllocator().Free(p);
    }
};

template <class T1, class T2> bool operator==(const StlMemAllocator<T1>&, const StlMemAllocator<T2>&) throw() {
    return true;
}
template <class T1, class T2> bool operator!=(const StlMemAllocator<T1>&, const StlMemAllocator<T2>&) throw() {
    return false;
}

} // namespace ugine::utils
