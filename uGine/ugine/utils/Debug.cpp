﻿#include "Debug.h"

#include "Log.h"

#include <ugine/core/CoreService.h>

namespace ugine::utils {

void OnDebugAttached(const Debug& debug) {
    UGINE_TRACE("__DEBUG ATTACHED [FRAME={}]: {}, {}, {}", core::CoreService::Frame(), debug.name, debug.file, debug.line);
}

void OnDebugDetached(const Debug& debug) {
    UGINE_TRACE("__DEBUG DETACHED [FRAME={}]: {}, {}, {}", core::CoreService::Frame(), debug.name, debug.file, debug.line);
}

void OnDebugTrace(const Debug& debug) {
    UGINE_TRACE("__DEBUG TRACE    [FRAME={}]: {}, {}, {}", core::CoreService::Frame(), debug.name, debug.file, debug.line);
}

void OnDebugAttach(entt::registry& reg, entt::entity e) {
    OnDebugAttached(reg.get<Debug>(e));
}

void OnDebugDetach(entt::registry& reg, entt::entity e) {
    OnDebugDetached(reg.get<Debug>(e));
}

} // namespace ugine::utils
