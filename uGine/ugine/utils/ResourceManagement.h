﻿#pragma once

#include "Graveyard.h"

#include <type_traits>

#define SAFE_DELETE(t)                                                                                                                                         \
    if (t) {                                                                                                                                                   \
        ugine::utils::Graveyard::Burry(std::move(t));                                                                                                          \
    }
