#pragma once

#include <ktx.h>

#pragma comment(lib, "ktx_read.lib")

namespace ugine::utils {

struct KtxScopeHolder {
    KtxScopeHolder(ktxTexture* tex = nullptr)
        : texture{ tex } {}

    ~KtxScopeHolder() {
        if (texture) {
            ktxTexture_Destroy(texture);
            texture = nullptr;
        }
    }

    ktxTexture* texture{};
};

} // namespace ugine::utils