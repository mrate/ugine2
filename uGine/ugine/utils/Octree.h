﻿#pragma once

#include <ugine/math/Aabb.h>
#include <ugine/math/Raycast.h>

#include "Utils.h"

#include <array>
#include <type_traits>

namespace ugine::utils {

template <typename _ValueType, _ValueType _NullValue> class Octree {
public:
    UGINE_MOVABLE_ONLY(Octree);

    static constexpr _ValueType Null{ _NullValue };

    using OctreeType = Octree<_ValueType, _NullValue>;
    using Type = _ValueType;
    // TODO:
    using TypeRef = Type&;
    using TypePtr = Type*;

    struct NodeData {
        math::AABB aabb;
        Type data;
    };

    std::array<OctreeType*, 8> child_;
    math::AABB aabb_;
    Type data_{ Null };

    Octree() {
        for (auto& c : child_) {
            c = nullptr;
        }
    }

    explicit Octree(const TypeRef data, const math::AABB& aabb)
        : data_{ data }
        , aabb_{ aabb } {
        for (auto& c : child_) {
            c = nullptr;
        }
    }

    ~Octree() {
        // TODO:
        for (auto& c : child_) {
            delete c;
        }
    }

    void Insert(const TypeRef t, const math::AABB& aabb) {
        if (IsLeaf()) {
            if (data_ == Null) {
                data_ = t;
                aabb_ = aabb;
            } else {
                for (auto& c : child_) {
                    c = new OctreeType{};
                }

                child_[OctantForPoint(aabb.CenterPoint())]->Insert(t, aabb);
                child_[OctantForPoint(aabb_.CenterPoint())]->Insert(data_, aabb_);

                data_ = Null;
                aabb_ = aabb_.Merge(aabb);
            }
        } else {
            child_[OctantForPoint(aabb.CenterPoint())]->Insert(t, aabb);
            aabb_ = aabb_.Merge(aabb);
        }
    }

    int OctantForPoint(const glm::vec3& point) const {
        auto res{ 0 };
        auto center{ aabb_.CenterPoint() };
        if (point.x >= center.x) {
            res |= 4;
        }
        if (point.y >= center.y) {
            res |= 2;
        }
        if (point.z >= center.z) {
            res |= 1;
        }
        return res;
    }

    void Collect(const math::Ray& ray, std::vector<Type>& values) const {
        if (math::Intersect(ray, aabb_)) {
            if (IsLeaf()) {
                if (data_ != Null) {
                    values.push_back(data_);
                }
            } else {
                for (const auto& c : child_) {
                    c.Collect(ray, values);
                }
            }
        }
    }

    bool Closest(const math::Ray& ray, Type& result, float& distance) const {
        if (math::Intersect(ray, aabb_, &distance)) {
            if (IsLeaf()) {
                if (data_ != Null) {
                    result = data_;
                    return true;
                } else {
                    return false;
                }
            } else {
                bool found{ false };
                float minDistance;
                Type minResult{ Null };
                for (const auto& c : child_) {
                    float localDistance;
                    float localResult;
                    if (c.Closest(ray, localResult, localDistance)) {
                        if (!found || localDistance < minDistance) {
                            minDistance = localDistance;
                            minResult = localResult;
                            found = true;
                        }
                    }
                }
                if (found) {
                    result = minResult;
                    distance = minDistance;
                }
                return found;
            }
        } else {
            return false;
        }
    }

    bool IsLeaf() const {
        return child_[0] == nullptr;
    }

    const math::AABB& Aabb() const {
        return aabb_;
    }

    uint32_t Size() const {
        if (IsLeaf()) {
            return data_ != Null ? 1 : 0;
        } else {
            uint32_t res{};
            for (const auto& c : child_) {
                res += c->Size();
            }
            return res;
        }
    }
};

} // namespace ugine::utils
