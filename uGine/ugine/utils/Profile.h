﻿#pragma once

#include "Log.h"
#include "Strings.h"

#include <chrono>
#include <string_view>

#ifdef UGINE_PROFILE

#include <optick.h>

#ifdef _DEBUG
#pragma comment(lib, "OptickCored.lib")
#else
#pragma comment(lib, "OptickCore.lib")
#endif // _DEBUG

#define PROFILE_EVENT OPTICK_EVENT
#define PROFILE_FRAME OPTICK_FRAME
#define PROFILE_THREAD OPTICK_THREAD
#define PROFILE_START_THREAD OPTICK_START_THREAD
#define PROFILE_STOP_THREAD OPTICK_STOP_THREAD

#else // UGINE_PROFILE

#define PROFILE_EVENT(...)
#define PROFILE_FRAME(...)
#define PROFILE_THREAD(...)
#define PROFILE_START_THREAD(...)
#define PROFILE_STOP_THREAD(...)

#endif // UGINE_PROFILE

namespace ugine::utils {

class ScopeTimer {
public:
    ScopeTimer(const std::string_view& text)
        : text_{ text }
        , start_{ Clock::now() } {}

    ~ScopeTimer() {
        UGINE_DEBUG("{}: {}", text_, utils::FormatTime(Clock::now() - start_));
    }

private:
    using Clock = std::chrono::high_resolution_clock;

    std::string text_;
    Clock::time_point start_;
};

} // namespace ugine::utils
