﻿#pragma once

#include "MemAllocator.h"

#include <deque>

namespace ugine::utils {

class Zombie {
public:
    virtual ~Zombie() {}
};

template <typename T> class ZombieImpl : public Zombie {
public:
    ZombieImpl(T&& v)
        : value{ std::move(v) } {}

    ZombieImpl(ZombieImpl&) = delete;
    ZombieImpl& operator=(ZombieImpl&) = delete;

    T value;
};

class Graveyard {
public:
    static Graveyard& Instance();

    template <typename T> static void Burry(T&& zombie) {
        if (!Instance().destroyed_) {
            Zombie* ptr = new (Instance().allocator_.Alloc(sizeof(ZombieImpl<T>))) ZombieImpl<T>(std::move(zombie));
            Instance().graveyard_.push_back(std::make_pair(Instance().Frame(), ptr));
        }
    }

    static void Purge() {
        Instance().PurgeImpl(false);
    }

    static void Destroy() {
        Instance().destroyed_ = true;
        Instance().PurgeImpl(true);
    }

private:
    Graveyard()
        : allocator_(16 * 1024 * 1024) {}

    uint64_t Frame() const;
    void PurgeImpl(bool all);

    Graveyard(const Graveyard&) = delete;
    Graveyard& operator=(const Graveyard&) = delete;

    std::deque<std::pair<uint64_t, Zombie*>> graveyard_;
    MemAllocator allocator_;
    bool destroyed_{};
};

} // namespace ugine::utils
