﻿#pragma once

#ifdef _WIN32
#include <Windows.h>
#endif

#include <cassert>

namespace ugine::utils {

#define UGINE_ASSERT(x) assert(x)

#ifdef _WIN32
#ifdef _DEBUG
#define UGINE_BREAK                                                                                                                                            \
    if (IsDebuggerPresent()) {                                                                                                                                 \
        DebugBreak();                                                                                                                                          \
    }
#endif // _DEBUG
#endif // WIN32

#ifndef UGINE_BREAK
#define UGINE_BREAK
#endif

} // namespace ugine::utils
