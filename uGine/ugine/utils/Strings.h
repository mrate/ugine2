﻿#pragma once

#include <glm/glm.hpp>

#include <chrono>
#include <map>
#include <string>

namespace ugine::utils {

std::wstring ToWString(const std::string& str);
std::string ToString(const std::wstring& str);

std::string ToLower(const std::string_view& v);
std::string ToUpper(const std::string_view& v);

void DumpMatrix(const glm::mat4& mat);

std::string CamelCaseToHuman(const std::string_view& camel);

bool Contains(const std::string_view& v, const std::string_view what, bool ignoreCase = false);

std::string FormatTime(long long ms);

void Replace(std::string& src, const std::map<std::string, std::string>& values);

template <typename T> std::string FormatTime(T t) {
    return FormatTime(std::chrono::duration_cast<std::chrono::milliseconds>(t).count());
}

} // namespace ugine::utils
