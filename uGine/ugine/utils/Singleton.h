﻿#pragma once

namespace ugine::utils {

template <typename T> class Singleton {
public:
    Singleton(const Singleton&) = delete;
    Singleton& operator=(const Singleton&) = delete;
    virtual ~Singleton() {}

    static T& Instance() {
        static T instance;
        return instance;
    }

protected:
    Singleton() = default;
};

} // namespace ugine::utils
