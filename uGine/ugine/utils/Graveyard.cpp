﻿#include "Graveyard.h"

#include <ugine/core/CoreService.h>
#include <ugine/gfx/GraphicsService.h>
#include <ugine/utils/Log.h>

namespace ugine::utils {

Graveyard& Graveyard::Instance() {
    static Graveyard i;
    return i;
}

void Graveyard::PurgeImpl(bool all) {
    if (all) {
        std::for_each(graveyard_.begin(), graveyard_.end(), [&](auto& v) {
            v.second->~Zombie();
            allocator_.Free(v.second);
        });
        Instance().graveyard_.clear();
    } else {
        const auto frame{ core::CoreService::Frame() };
        const auto numFrames{ gfx::GraphicsService::FramesInFlight() + 1 }; // +1 in case this object was already used in this frame.

        if (graveyard_.empty() || frame < numFrames) {
            return;
        }

        auto it{ std::find_if(graveyard_.begin(), graveyard_.end(), [&](const auto& v) { return v.first <= frame - numFrames; }) };
        if (it != graveyard_.end()) {
            std::for_each(it, graveyard_.end(), [&](auto& v) {
                v.second->~Zombie();
                allocator_.Free(v.second);
            });
            graveyard_.erase(it, graveyard_.end());
        }
    }
}

uint64_t Graveyard::Frame() const {
    return core::CoreService::Frame();
}

} // namespace ugine::utils
