﻿#pragma once

#include "Assert.h"

#include <stdint.h>

namespace ugine::utils {

void* alignedAlloc(size_t size, size_t alignment);
void alignedFree(void* data);

template <typename T> class AlignedBuffer {
public:
    AlignedBuffer(size_t count, size_t alignment)
        : count_(count)
        , alignment_(alignment)
        , size_(count * alignment) {
        data_ = alignedAlloc(size_, alignment_);
        UGINE_ASSERT(data_);
    }

    ~AlignedBuffer() {
        if (data_) {
            alignedFree(data_);
            data_ = nullptr;
        }
    }

    AlignedBuffer(const AlignedBuffer&) = delete;
    AlignedBuffer& operator=(const AlignedBuffer&) = delete;

    AlignedBuffer(AlignedBuffer&& other) {
        count_ = other.count_;
        alignment_ = other.alignment_;
        size_ = other.size_;
        data_ = other.data_;
        other.data_ = nullptr;
    }

    AlignedBuffer& operator=(AlignedBuffer&& other) {
        count_ = other.count_;
        alignment_ = other.alignment_;
        size_ = other.size_;
        data_ = other.data_;
        other.data_ = nullptr;
    }

    size_t Size() const {
        return size_;
    }

    size_t Count() const {
        return count_;
    }

    size_t Alignment() const {
        return alignment_;
    }

    T* operator[](size_t index) {
        UGINE_ASSERT(index < count_);
        return reinterpret_cast<T*>(reinterpret_cast<uint64_t>(data_) + (index * alignment_));
    }

    const T* operator[](size_t index) const {
        UGINE_ASSERT(index < count_);
        return reinterpret_cast<const T*>(reinterpret_cast<uint64_t>(data_) + (index * alignment_));
    }

    void* Memory() {
        return data_;
    }

private:
    size_t count_{};
    size_t alignment_{};
    size_t size_{};
    void* data_{};
};

} // namespace ugine::utils
