﻿#pragma once

#include <entt/entt.hpp>
#include <spdlog/fmt/fmt.h>

#include <string>

namespace ugine::utils {

struct Debug {
    std::string name;
    std::string file;
    int line;
};

void OnDebugAttached(const Debug& debug);
void OnDebugDetached(const Debug& debug);
void OnDebugTrace(const Debug& debug);
void OnDebugAttach(entt::registry& reg, entt::entity e);
void OnDebugDetach(entt::registry& reg, entt::entity e);

#ifdef _DEBUG
#define DEBUG_ATTACH(val, name, ...) val._DebugAttach(ugine::utils::Debug{ fmt::format(name, __VA_ARGS__), __FILE__, __LINE__ })
#define DEBUG_DETACH(val, name) val._DebugDetach(ugine::utils::Debug{ name, __FILE__, __LINE__ })
#define DEBUG_TRACE(val) OnDebugTrace(val._DebugInfo())
#else
#define DEBUG_ATTACH(val, name, ...)
#define DEBUG_DETACH(val, name)
#define DEBUG_TRACE(val)
#endif

} // namespace ugine::utils
