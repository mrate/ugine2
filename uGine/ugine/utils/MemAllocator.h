﻿#pragma once

#include <memory>

#include <stdint.h>

namespace ugine::utils {

class MemAllocatorImpl;

class MemAllocator {
public:
    MemAllocator(uint32_t size);
    ~MemAllocator();

    void* Alloc(uint32_t size);
    void* AlignedAlloc(uint32_t size, uint32_t alignment);
    void Free(void* memory);

private:
    std::unique_ptr<MemAllocatorImpl> impl_;
};

} // namespace ugine::utils
