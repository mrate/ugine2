﻿#include "MemAllocator.h"

#include "Assert.h"

#include <tlsf.h>

#include <mutex>
#include <stdlib.h>

namespace ugine::utils {

class MemAllocatorImpl {
public:
    MemAllocatorImpl(uint32_t size)
        : size_{ size } {
        memory_ = malloc(size);
        Init(memory_, size);
    }

    MemAllocatorImpl(void* memory, uint32_t size)
        : size_{ size } {
        Init(memory, size);
    }

    ~MemAllocatorImpl() {
        if (memory_) {
            free(memory_);
        }
    }

    void Init(void* memory, uint32_t size) {
        pool_ = tlsf_create_with_pool(memory, size);
        UGINE_ASSERT(pool_ && "Failed to create memory pool.");
    }

    void* Alloc(uint32_t size) {
        std::scoped_lock lock(mtx);

        auto mem{ tlsf_malloc(pool_, size) };
        UGINE_ASSERT(mem && "Failed to allocate memory from pool.");
        //allocated_ += tlsf_block_size(mem);
        return mem;
    }

    void* AllocAligned(uint32_t size, uint32_t alignment) {
        std::scoped_lock lock(mtx);

        return tlsf_memalign(pool_, alignment, size);
    }

    void Free(void* mem) {
        std::scoped_lock lock(mtx);

        //allocated_ -= tlsf_block_size(mem);
        tlsf_free(pool_, mem);
    }

private:
    tlsf_t pool_;
    void* memory_{};
    size_t size_{};
    size_t allocated_{};

    // TODO:
    std::mutex mtx;
};

MemAllocator::MemAllocator(uint32_t size)
    : impl_(std::make_unique<MemAllocatorImpl>(size)) {}

MemAllocator::~MemAllocator() {}

void* MemAllocator::Alloc(uint32_t size) {
    return impl_->Alloc(size);
}

void* MemAllocator::AlignedAlloc(uint32_t size, uint32_t alignment) {
    return impl_->AllocAligned(size, alignment);
}

void MemAllocator::Free(void* memory) {
    impl_->Free(memory);
}

} // namespace ugine::utils
