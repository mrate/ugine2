﻿#pragma once

#ifdef NOMINMAX
#undef NOMINMAX
#include <FileWatch.hpp>
#define NOMINMAX
#endif

#include <filesystem>
#include <vector>

namespace ugine::utils {

class FileWatcher {
public:
    using Watcher = filewatch::FileWatch<std::filesystem::path>;

    FileWatcher() {}

    template <typename T> void Add(const std::filesystem::path& file, T&& t) {
        watchers_.push_back(std::make_unique<Watcher>(file, [file, t](const std::filesystem::path& path, const filewatch::Event changeType) {
            if (changeType == filewatch::Event::modified) {
                t(file);
            }
        }));
    }

private:
    std::vector<std::unique_ptr<Watcher>> watchers_;
};

} // namespace ugine::utils
