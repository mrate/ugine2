﻿#pragma once

#include <filesystem>
#include <fstream>
#include <vector>

namespace ugine::utils {

std::string ReadFile(const std::filesystem::path& path);
std::vector<std::string> ReadFileLines(const std::filesystem::path& path, bool skipEmpty);
std::vector<std::wstring> ReadFileLinesW(const std::filesystem::path& path, bool skipEmpty);

template <typename T> inline void WriteFileLines(const std::filesystem::path& path, const T& lines) {
    std::ofstream file{ path };
    for (const auto& i : lines) {
        file << i << std::endl;
    }
    file.close();
}

template <typename T> inline void WriteFileLinesW(const std::filesystem::path& path, const T& lines) {
    std::wofstream file{ path };
    for (const auto& i : lines) {
        file << i << std::endl;
    }
    file.close();
}

std::vector<uint8_t> ReadFileBinary(const std::filesystem::path& path);
void WriteFileBinary(const std::filesystem::path& file, const std::vector<uint8_t>& data);

bool IsNewer(const std::filesystem::path& thisFile, const std::filesystem::path& thatFile);

std::filesystem::path CurrentWorkingDir();

std::filesystem::path MakeRelative(const std::filesystem::path& srcPath, const std::filesystem::path& file);

} // namespace ugine::utils
