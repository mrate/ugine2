﻿#include <iostream>

#undef _DEBUG

#include <ugine/utils/Align.h>
#include <ugine/utils/Storage.h>

using namespace ugine::utils;

struct MyType {
    std::string name;
};

class MyStorage : public Storage<MyType, MyStorage> {
public:
    void PreDestroy(Id id) override {
        std::cout << "PreDestroy" << std::endl;
    }

    void PostDestroy(Id id) override {
        std::cout << "PostDestroy" << std::endl;
    }
};

int main(int argc, char* argv[]) {
    //MyStorage::Init();

    //std::cout << "Create" << std::endl;
    //{ auto ref{ MyStorage::Add({ "test" }) }; }
    //std::cout << "Destroy" << std::endl;

    //auto ref2{ MyStorage::Add("test") };

    //MyStorage::Destroy();

    auto c = AlignBy(0, 4, 16) + 4;
    assert(c == 4);
    c = AlignBy(c, 12, 16) + 12;
    assert(c == 16 + 12);
    c = AlignBy(c, 12, 16) + 12;
    assert(c == 32 + 12);
    c = AlignBy(c, 12, 16) + 12;
    assert(c == 48 + 12);

    auto b = AlignBy(0, 4, 16) + 4;
    assert(b == 4);
    b = AlignBy(b, 64, 16) + 64;
    assert(b == 16 + 64);

    auto a = AlignBy(0, 12, 16) + 12;
    assert(a == 12);
    std::cout << a << std::endl;
    a = AlignBy(a, 14, 16) + 8;
    assert(a == 16 + 8);
    std::cout << a << std::endl;
    a = AlignBy(a, 8, 16) + 8;
    assert(a == 32);
    std::cout << a << std::endl;
    a = AlignBy(a, 32, 16) + 32;
    assert(a == 64);
    std::cout << a << std::endl;
    a = AlignBy(a, 8, 16) + 8;
    assert(a == 72);
    std::cout << a << std::endl;
    a = AlignBy(a, 10, 16) + 10;
    assert(a == 90);
    std::cout << a << std::endl;
}
