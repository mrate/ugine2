﻿#include "Tests.h"

#include <string>

#include <entt/entt.hpp>

#include <ugine/gfx/tools/SpirvPreprocessor.h>
#include <ugine/utils/File.h>

int main(int argc, char* argv[]) {
    //ugine::gfx::SpirvPreprocessor p;
    //std::filesystem::path path{ L"assets/shaders/pbr.frag" };
    //auto file{ ugine::utils::ReadFile(path) };
    //p.Process(path, file);

    //EnTTParentChild();
    ////GlobalArrayTest();

    ////TestScene1();

    ////TestEvents2();
    ////TestEvents3();

    //OrganizerTest1();

    //OctreeTest();

    ReflectionTest();

    return 0;
}
