#include <entt/entt.hpp>

#include <iostream>

class MyClass {
public:
    entt::delegate<int(int)> OnAdd;
    entt::delegate<int(int)> OnRemove;

    entt::sigh<void(int)> OnAddSignal;

    void Add(int a) {
        OnAdd(a);
        OnAddSignal.publish(a);
    }

    void Remove(int a) {
        OnRemove(a);
    }
};

class SignalHandler {
public:
    entt::sink<void(int)> sink;
    entt::scoped_connection conn;

    SignalHandler(entt::sigh<void(int)>& signal)
        : sink{ signal } {
        conn = sink.connect<&SignalHandler::OnAdd>(this);
    }

    ~SignalHandler() {
        //sink.disconnect<&SignalHandler::OnAdd>(this);
    }

    void OnAdd(int a) {
        std::cout << "Listener::OnAdd(" << a << ")" << std::endl;
    }
};

struct MouseEvent {
    int x;
    int y;
};

class EventHandler {
public:
    EventHandler(entt::dispatcher& dispatcher) {
        dispatcher.sink<MouseEvent>().connect<&EventHandler::OnMouse>(this);
    }

    void OnMouse(const MouseEvent& e) {
        std::cout << "EventHandler::OnMouse{" << e.x << ", " << e.y << "}" << std::endl;
    }
};

int addHandler(int a) {
    std::cout << "Add(" << a << ")" << std::endl;
    return 0;
}

int addHandler2() {
    std::cout << "Add2" << std::endl;
    return 0;
}

int removeHandler(int a) {
    std::cout << "Remove(" << a << ")" << std::endl;
    return 0;
}

void TestEvents1() {
    MyClass c;

    c.OnAdd.connect<&addHandler>();
    c.OnAdd.connect<&addHandler2>();
    c.OnRemove.connect<&removeHandler>();

    c.Add(1);
    c.Remove(1);

    {
        SignalHandler sh{ c.OnAddSignal };
        c.Add(2);
        //sh.sink.connect(c.OnAddSignal);

        SignalHandler sh2{ c.OnAddSignal };
        c.Add(3);
    }

    c.Add(4);
}

void TestEvents2() {
    entt::dispatcher dispatcher;

    EventHandler h1{ dispatcher };
    dispatcher.trigger<MouseEvent>({ 1, 2 });
    dispatcher.enqueue<MouseEvent>({ 3, 4 });

    std::cout << "Update:" << std::endl;
    dispatcher.update();
}

class Emitter : public entt::emitter<Emitter> {
public:
    void Emit(int a) {
        publish<MouseEvent>(a, a);
    }
};

void TestEvents3() {
    Emitter emitter;

    emitter.Emit(1);

    auto con1 = emitter.on<MouseEvent>([](const MouseEvent& e, Emitter& sender) { std::cout << "OnMouse(" << e.x << ", " << e.y << ")" << std::endl; });
    auto con2 = emitter.once<MouseEvent>([](const MouseEvent& e, Emitter& sender) { std::cout << "Once OnMouse(" << e.x << ", " << e.y << ")" << std::endl; });

    emitter.Emit(5);
    emitter.Emit(6);
    emitter.Emit(7);
}