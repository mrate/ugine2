﻿#include <ugine/core/GameObject.h>
#include <ugine/utils/Assert.h>

#include <entt/entt.hpp>

#include <iostream>

using namespace ugine::core;

void Iterate(const GameObject& go, int depth = 0) {
    for (int i = 0; i < depth; ++i) {
        std::cout << "  ";
    }
    std::cout << go.Name() << " (" << go.ChildrenCount() << ")" << std::endl;

    for (auto child{ go.FirstChild() }; child.IsValid(); child = child.NextSibling()) {
        Iterate(child, depth + 1);
    }
}

void SingleTest(entt::registry& registry, GameObject& parent) {
    auto c1{ GameObject::Create(registry, "C1") };
    auto c2{ GameObject::Create(registry, "C2") };
    auto c3{ GameObject::Create(registry, "C3") };

    auto c1_c1{ GameObject::Create(registry, "C1_C1") };
    auto c1_c2{ GameObject::Create(registry, "C1_C2") };

    auto c2_c1{ GameObject::Create(registry, "C2_C1") };

    {
        UGINE_ASSERT(!c1.Parent().IsValid());
        UGINE_ASSERT(!c2.Parent().IsValid());
        UGINE_ASSERT(!c3.Parent().IsValid());

        UGINE_ASSERT(parent.ChildrenCount() == 0);
        UGINE_ASSERT(c1.ChildrenCount() == 0);
        UGINE_ASSERT(c2.ChildrenCount() == 0);
        UGINE_ASSERT(c3.ChildrenCount() == 0);
    }

    parent.AddChild(c1);

    {
        UGINE_ASSERT(parent.ChildrenCount() == 1);
        UGINE_ASSERT(!c1.PrevSibling().IsValid());
        UGINE_ASSERT(!c1.NextSibling().IsValid());
    }

    parent.AddChild(c2);

    {
        UGINE_ASSERT(parent.ChildrenCount() == 2);

        UGINE_ASSERT(!c1.PrevSibling().IsValid());
        UGINE_ASSERT(c1.NextSibling() == c2);
        UGINE_ASSERT(c2.PrevSibling() == c1);
        UGINE_ASSERT(!c2.NextSibling().IsValid());
    }

    parent.AddChild(c3);

    {
        UGINE_ASSERT(parent.ChildrenCount() == 3);

        UGINE_ASSERT(!c1.PrevSibling().IsValid());
        UGINE_ASSERT(c1.NextSibling() == c2);
        UGINE_ASSERT(c2.PrevSibling() == c1);
        UGINE_ASSERT(c2.NextSibling() == c3);
        UGINE_ASSERT(c3.PrevSibling() == c2);
        UGINE_ASSERT(!c3.NextSibling().IsValid());

        UGINE_ASSERT(c1.Parent() == parent);
        UGINE_ASSERT(c2.Parent() == parent);
        UGINE_ASSERT(c3.Parent() == parent);
    }

    c1.AddChild(c1_c1);

    {
        UGINE_ASSERT(c1.ChildrenCount() == 1);
        UGINE_ASSERT(c1_c1.ChildrenCount() == 0);
        UGINE_ASSERT(c1_c1.Parent() == c1);

        UGINE_ASSERT(!c1_c1.PrevSibling().IsValid());
        UGINE_ASSERT(!c1_c1.NextSibling().IsValid());
    }

    c1.AddChild(c1_c2);

    {
        UGINE_ASSERT(c1.ChildrenCount() == 2);

        UGINE_ASSERT(c1_c1.Parent() == c1);
        UGINE_ASSERT(c1_c2.Parent() == c1);

        UGINE_ASSERT(parent.ChildrenCount() == 3);
    }

    c2.AddChild(c2_c1);

    UGINE_ASSERT(c2_c1.Parent() == c2);

    // Iteratre children.
    Iterate(parent);

    // Remove.
    parent.RemoveChild(c2);

    {
        UGINE_ASSERT(parent.ChildrenCount() == 2);
        UGINE_ASSERT(!c1.PrevSibling().IsValid());
        UGINE_ASSERT(c1.NextSibling() == c3);
        UGINE_ASSERT(c3.PrevSibling() == c1);
        UGINE_ASSERT(!c3.NextSibling().IsValid());
    }

    Iterate(parent);

    parent.RemoveChild(c1);
    parent.RemoveChild(c3);

    { UGINE_ASSERT(parent.ChildrenCount() == 0); }

    Iterate(parent);

    GameObject::Destroy(c1);
    GameObject::Destroy(c2);
    GameObject::Destroy(c3);
}

void EnTTParentChild() {
    entt::registry registry;

    auto parent{ GameObject::Create(registry, "Parent") };

    //for (int i = 0; i < 10; ++i) {
    //    std::cout << "-------------------" << std::endl;
    //    SingleTest(registry, parent);

    //    auto tmp{ GameObject::Create(registry, "test") };
    //    registry.each([&registry](entt::entity en) {
    //        GameObject go{ registry, en };
    //        std::cout << "Live entity: " << go.Name() << std::endl;
    //    });

    //    GameObject::Destroy(tmp);

    //    registry.each([&registry](entt::entity en) {
    //        GameObject go{ registry, en };
    //        std::cout << "Live entity: " << go.Name() << std::endl;
    //    });

    //    std::cout << "Registry size: " << registry.size() << std::endl;
    //}

    std::cout << "Size: " << registry.size() << std::endl;
    std::cout << "Alive: " << registry.alive() << std::endl;
    registry.each([&](entt::entity id) {
        registry.remove_all(id);
        registry.destroy(id);
    });
    std::cout << "Size: " << registry.size() << std::endl;
    std::cout << "Alive: " << registry.alive() << std::endl;

    for (auto [ent, tag] : registry.view<Tag>().each()) {
        std::cout << "Tag: " << tag.name << std::endl;
    }
}
