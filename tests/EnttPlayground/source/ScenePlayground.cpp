﻿#include <ugine/core/GameObject.h>

#include <iostream>

using namespace ugine::core;

struct TestComponent {
    int x;
    int y;
};

class Scene {
public:
    template <typename... Args> struct ViewWrapper {
        using View = entt::basic_view<entt::entity, entt::exclude_t<>, Args...>;
        using VIterator = typename View::iterator;

        struct Iterator {
            Iterator(Scene* scene, const typename VIterator& it)
                : scene_{ scene }
                , it_{ it } {}

            GameObject operator*() {
                return GameObject{ scene_->registry_, *it_ };
            }

            bool operator==(const Iterator& oth) const {
                return it_ == oth.it_;
            }
            bool operator!=(const Iterator& oth) const {
                return it_ != oth.it_;
            }
            void operator++() {
                ++it_;
            }
            void operator++(int) {
                it_++;
            }

        private:
            Scene* scene_{};
            VIterator it_;
        };

        ViewWrapper(Scene* scene, View v)
            : scene_(scene)
            , view_(v) {}

        template <typename F> void Each(F&& f) {
            view_.each([&](entt::entity e, Args&... args) { f(GameObject{ scene_->registry_, e }, std::forward<Args>(args)...); });
        }

        Iterator begin() {
            return Iterator{ scene_, view_.begin() };
        }

        Iterator end() {
            return Iterator{ scene_, view_.end() };
        }

    private:
        Scene* scene_{};
        View view_;
    };

    Scene() {}

    GameObject CreateObject(const std::string& name) {
        return GameObject::Create(registry_, name);
    }

    template <typename T> void Each(T&& t) {
        registry_.each([&](auto ent) { t(GameObject{ registry_, ent }); });
    }

    template <typename... Args> auto View() {
        return ViewWrapper<Args...>{ this, registry_.view<Args...>() };
    }

    GameObject Get(entt::entity ent) {
        return GameObject{ registry_, ent };
    }

    template <typename T> GameObject GetFromComponent(T&& t) {
        return GameObject{ registry_, entt::to_entity(registry_, t) };
    }

    // TODO: DEBUG
    entt::registry& Registry() {
        return registry_;
    }

private:
    entt::registry registry_;
};

void TestScene1() {
    ::Scene scene;

    auto obj1{ scene.CreateObject("Test") };
    auto obj2{ scene.CreateObject("Obj2") };

    obj1.CreateComponent<TestComponent>(1, 2);

    //scene.View<Tag>().;

    //auto view{ scene.Registry().view<Tag>() };
    scene.Each([](const GameObject& go) { std::cout << go.Name() << std::endl; });
    auto view = scene.View<Tag>(); //[](const GameObject& go, const Tag& tag) { std::cout << tag.name << std::endl; });
    auto view2 = scene.View<Tag, TestComponent>();

    view.Each([](const GameObject& go, const Tag& tag) { std::cout << go.Name() << ", tag: " << tag.name << std::endl; });
    view2.Each([](const GameObject& go, const Tag& tag, const TestComponent& test) {
        std::cout << go.Name() << ", tag: " << tag.name << ", test: " << test.x << "x" << test.y << std::endl;
    });

    for (auto go : view) {
        std::cout << go.Name() << std::endl;
    }

    for (auto go : view2) {
        std::cout << go.Component<TestComponent>().x << std::endl;
    }

    for (auto e : scene.Registry().view<TestComponent>()) {
        auto go{ scene.Get(e) };
        std::cout << go.Component<TestComponent>().x << std::endl;
    }

    for (auto [ent, test] : scene.Registry().view<TestComponent>().each()) {
        std::cout << "Test.x=" << test.x << std::endl;
        auto go{ scene.GetFromComponent(test) };
        std::cout << "Name=" << go.Name() << std::endl;
    }
}
