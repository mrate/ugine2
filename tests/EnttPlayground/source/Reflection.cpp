﻿#include <ugine/utils/Assert.h>

#include <entt/entt.hpp>
#include <glm/glm.hpp>
#include <nlohmann/json.hpp>

#include <pybind11/embed.h>
#include <pybind11/pybind11.h>

#include <iostream>

struct Nested {
    int test{};
};

struct MyComponent {
    glm::vec3 xyz{};
    int value{};
    std::string name;
    Nested nested;
};

const char* NAME = "name";

std::ostream& operator<<(std::ostream& out, const MyComponent& t) {
    out << "value: " << t.value << std::endl;
    out << "name: " << t.name << std::endl;
    out << "xyz: x=" << t.xyz.x << ", y=" << t.xyz.y << ", z=" << t.xyz.z << std::endl;
    out << "nested: test=" << t.nested.test << std::endl;
    return out;
}

#define BEGIN_REGISTER(TYPE_, MODULE_)                                                                                                                         \
    {                                                                                                                                                          \
        using namespace entt;                                                                                                                                  \
        using _TYPE = TYPE_;                                                                                                                                   \
        auto __m{ entt::meta<_TYPE>().prop(NAME, #TYPE_).type() };                                                                                             \
        pybind11::class_<_TYPE> __c(MODULE_, #TYPE_);                                                                                                          \
        __c.def(pybind11::init<>())
#define MEMBER(NAME_)                                                                                                                                          \
    __m.data<&_TYPE::NAME_>(#NAME_##_hs).prop(NAME, #NAME_);                                                                                                   \
    __c.def_readwrite(#NAME_, &_TYPE::NAME_)
#define END_REGISTER() }

class Reflection {

public:
    static entt::id_type IntID;
    static entt::id_type FloatID;
    static entt::id_type StringID;

private:
    static Reflection instance;

    template <typename T> entt::id_type Register() {
        entt::meta<T>().type();
        return entt::resolve<T>().id();
    }

    Reflection() {
        IntID = Register<int>();
        FloatID = Register<float>();
        StringID = Register<std::string>();
    }
};

entt::id_type Reflection::IntID{};
entt::id_type Reflection::FloatID{};
entt::id_type Reflection::StringID{};
Reflection Reflection::instance{};

class Serializer {
public:
    Serializer(nlohmann::json& json)
        : json_{ &json } {}

    template <typename T> Serializer& operator<<(const T& t) {
        try {
            auto meta{ entt::resolve<T>() };
            if (meta && meta.id()) {
                auto name{ meta.prop(NAME).value().cast<const char*>() };
                Serialize(*json_, name, meta, t);
            } else {
                std::cout << "Invalid type" << std::endl;
            }
        } catch (const std::exception& ex) {
            std::cout << "Error: " << ex.what() << std::endl;
        }
        return *this;
    }

    template <typename T> static void Deserialize(const nlohmann::json& json, T& t) {
        try {
            auto meta{ entt::resolve<T>() };
            if (meta && meta.id()) {
                Deserialize(json, meta, t);
            }
        } catch (const std::exception& ex) {
            std::cout << "Error: " << ex.what() << std::endl;
        }
    }

private:
    struct SingleInit {};

    using SerializeF = void (*)(nlohmann::json& json, const std::string_view& name, const entt::meta_any& value);
    using DeserializeF = entt::meta_any (*)(const nlohmann::json& json, const std::string_view& name);

    struct TypeDS {
        SerializeF serialize;
        DeserializeF deserialize;
    };

    static Serializer Instance;
    static std::unordered_map<entt::id_type, TypeDS> Map;

    template <typename T> static void Serialize(nlohmann::json& json, const std::string_view& name, const entt::meta_any& value) {
        json[name.data()] = value.cast<T>();
    }

    template <typename T> static entt::meta_any Deserialize(const nlohmann::json& json, const std::string_view& name) {
        auto value{ json.value<T>(name.data(), T{}) };
        return value;
    }

    void Serialize(nlohmann::json& json, const std::string_view& name, entt::meta_type type, const entt::meta_any& handle) {
        auto it{ Map.find(type.id()) };
        if (it != Map.end()) {
            it->second.serialize(json, name, handle);
        } else {
            auto& cJson{ json[name.data()] };
            for (auto member : type.data()) {
                auto memberName{ member.prop(NAME).value().cast<const char*>() };
                Serialize(cJson, memberName, member.type(), member.get(handle));
            }
        }
    }

    static void Deserialize(const nlohmann::json& json, entt::meta_type type, const entt::meta_handle& instance) {
        for (auto member : type.data()) {
            auto memberName{ member.prop(NAME).value().cast<const char*>() };

            auto it{ Map.find(member.type().id()) };
            if (it != Map.end()) {
                member.set(instance, it->second.deserialize(json, memberName));
            } else {
                const auto& cJson{ json[memberName] };

                auto memberValue{ member.get(instance) };
                Deserialize(cJson, member.type(), memberValue);
                member.set(instance, memberValue);
            }
        }
    }

    template <typename T> void Register() {
        TypeDS t;
        t.serialize = &Serializer::Serialize<T>;
        t.deserialize = &Serializer::Deserialize<T>;

        auto resolve{ entt::resolve<T>().id() };
        Instance.Map.insert(std::make_pair(resolve, t));
    }

    Serializer(const SingleInit&) {
        Register<int>();
        Register<float>();
        Register<std::string>();
    }

    nlohmann::json* json_{};
};

std::unordered_map<entt::id_type, Serializer::TypeDS> Serializer::Map;
Serializer Serializer::Instance{ Serializer::SingleInit{} };

#define STRINGIFY(T) #T

template <typename T, typename V> void PythonProperty(pybind11::class_<T>& c, const entt::meta_data& member) {
    auto memberName{ member.prop(NAME).value().cast<const char*>() };
    c.def_property(
        memberName, [member](const T& cls) { return member.get(cls).cast<V>(); }, [member](T& cls, V v) { member.set(cls, v); });
}

template <typename T> void PythonRegister(pybind11::module_& m) {
    namespace py = pybind11;

    auto meta{ entt::resolve<T>() };
    auto name{ meta.prop(NAME).value().cast<const char*>() };

    py::class_<T> c(m, name);
    c.def(py::init<>());

    for (auto member : meta.data()) {
        auto id{ member.type().id() };

        if (id == Reflection::IntID) {
            PythonProperty<T, int>(c, member);
        } else if (id == Reflection::FloatID) {
            PythonProperty<T, float>(c, member);
        } else if (id == Reflection::StringID) {
            PythonProperty<T, std::string>(c, member);
        } else {
            // TODO:
        }
    }
}

void RegisterReflection(pybind11::module_& m) {
    BEGIN_REGISTER(glm::vec3, m);
    MEMBER(x);
    MEMBER(y);
    MEMBER(z);
    END_REGISTER();

    BEGIN_REGISTER(Nested, m);
    MEMBER(test);
    END_REGISTER();

    BEGIN_REGISTER(MyComponent, m);
    MEMBER(value);
    MEMBER(name);
    MEMBER(xyz);
    MEMBER(nested);
    END_REGISTER();
}

PYBIND11_EMBEDDED_MODULE(test, m) {
    RegisterReflection(m);

    m.def("message", []() { return "Embedded"; });
    m.def("getNested", []() { return Nested{ 666 }; });
    m.def("getComponent", []() {
        MyComponent c{};
        c.value = 5;
        c.name = "My name";
        c.xyz = { 1, 4, 6 };
        c.nested.test = 666;
        return c;
    });
    m.def("setComponent", [](const MyComponent& c) { std::cout << c << std::endl; });
}

void ReflectionTest() {
    MyComponent c{};
    c.value = 5;
    c.name = "My name";
    c.xyz = { 1, 4, 6 };
    c.nested.test = 666;

    nlohmann::json json;

    //{
    //    std::cout << std::endl << "----- SERIALIZE -----" << std::endl;

    //    Serializer ser{ json["Component1"] };
    //    ser << c;
    //    std::cout << json << std::endl;
    //}

    //{
    //    std::cout << std::endl << "----- DESERIALIZE -----" << std::endl;

    //    MyComponent newComp{};
    //    const auto& c1{ json["Component1"] };
    //    Serializer::Deserialize(c1["MyComponent"], newComp);

    //    std::cout << newComp << std::endl;
    //}

    {
        namespace py = pybind11;

        std::cout << std::endl << "----- PYTHON -----" << std::endl;

        py::scoped_interpreter guard{};

        auto test = py::module_::import("test");

        try {
            py::exec(R"(
import test
print("Hello from Python: " + test.message())
print(dir(test.Nested))
a=test.Nested()
print(a.test)
b=test.getNested()
print(b.test)
print(dir(test.MyComponent))
c=test.getComponent()
print(c.value)
print(c.name)
print(c.nested.test)
print(c.xyz.y)
c.value=124
c.name="PythonName"
test.setComponent(c)
)");

        } catch (const std::exception& ex) {
            std::cerr << "Pythyon error: " << ex.what() << std::endl;
        }
    }
}
