#include <entt/entt.hpp>

#include <iostream>
#include <list>

template <typename T, size_t _MaxSize> class GlobalArray {
public:
    static const auto MaxSize{ _MaxSize };
    struct Ref {
        entt::entity ent{};

        operator uint32_t() const {
            return static_cast<uint32_t>(ent);
        }
    };

    struct Updates {
        std::set<Ref> inserts;
        std::set<Ref> removes;
        std::set<Ref> updates;

        void Reset() {
            inserts.clear();
            removes.clear();
            updates.clear();
        }
    };

    GlobalArray() {}

    template <typename V> Ref Add(V&& t) {
        auto ent{ registry_.create() };
        registry_.emplace<T>(ent, std::move(t));

        Ref ref{ ent };

        OnAdd(ref);

        return ref;
    }

    T Remove(Ref ref) {
        T res{ std::move(registry_.get<T>(ref.ent)) };
        registry_.remove<T>(ref.ent);
        registry_.destroy(ref.ent);

        OnRemove(ref);

        return res;
    }

    void Update(Ref ref, T&& t) {
        registry_.replace<T>(ref.ent, t);

        OnUpdate(ref);
    }

    void RegisterUpdates(Updates* updates) {
        updates_.push_back(updates);
    }

    void UnregisterUpdates(Updates* updates) {
        auto it{ std::find(updates_.begin(), updates_.end(), updates) };
        if (it != updates_.end()) {
            updates_.erase(it);
        }
    }

    //auto Refs() const {
    //    return registry_.view<T>();
    //}

    //template <typename T> void OnCreate(T&& t) {
    //    registry_.on_construct().
    //}

    const T& operator[](Ref ref) const {
        return registry_.get<T>(ref.ent);
    }

private:
    void OnAdd(Ref r) {
        for (auto u : updates_) {
            auto it{ std::find(u->removes.begin(), u->removes.end(), r) };
            if (it != u->removes.end()) {
                u->removes.erase(it);
                u->updates.insert(r);
            } else {
                u->inserts.insert(r);
            }
        }
    }

    void OnRemove(Ref r) {
        for (auto u : updates_) {
            auto it{ std::find(u->inserts.begin(), u->inserts.end(), r) };
            if (it != u->inserts.end()) {
                u->inserts.erase(it);
            } else {
                u->removes.insert(r);
            }
        }
    }

    void OnUpdate(Ref r) {
        for (auto u : updates_) {
            u->updates.insert(r);
        }
    }

    entt::registry registry_;
    std::list<Updates*> updates_;
};

struct Texture {
    int w{};
    int h{};
};

using TextureArray = GlobalArray<Texture, 10>;
using TextureRef = TextureArray::Ref;
using TextureUpdates = TextureArray::Updates;

std::ostream& operator<<(std::ostream& o, const Texture& t) {
    o << "{" << t.w << "x" << t.h << "}";
    return o;
}

void DumpTextures(const TextureArray& arr) {
    //for (const auto& [ref, texture] : arr.Refs()) {
    //}
}

void DumpUpdates(const TextureArray& a, const TextureUpdates& updates) {
    std::cout << "Changed:\n";
    if (!updates.inserts.empty()) {
        std::cout << "  Inserts: ";
        for (const auto i : updates.inserts) {
            std::cout << i << "{" << a[i] << "},";
        }
        std::cout << std::endl;
    }

    if (!updates.removes.empty()) {
        std::cout << "  Removes: ";
        for (const auto i : updates.removes) {
            std::cout << i << ", ";
        }
        std::cout << std::endl;
    }

    if (!updates.updates.empty()) {
        std::cout << "  Updates: ";
        for (const auto i : updates.updates) {
            std::cout << i << ", ";
        }
        std::cout << std::endl;
    }
}

TextureRef AddTexture(TextureArray& a, const Texture& t) {
    std::cout << ">> Adding texture " << t << std::endl;
    return a.Add(t);
}

void RemoveTexture(TextureArray& a, TextureRef t) {
    std::cout << ">> Removing texture " << a[t] << std::endl;
    a.Remove(t);
}

void GlobalArrayTest() {
    TextureUpdates updates;

    TextureArray arr;
    arr.RegisterUpdates(&updates);

    DumpUpdates(arr, updates);

    auto t1 = AddTexture(arr, { 1, 1 });
    DumpUpdates(arr, updates);

    auto t2 = AddTexture(arr, { 2, 2 });
    DumpUpdates(arr, updates);

    updates.Reset();

    auto t3 = AddTexture(arr, { 3, 3 });
    DumpUpdates(arr, updates);

    RemoveTexture(arr, t3);
    DumpUpdates(arr, updates);

    RemoveTexture(arr, t2);
    DumpUpdates(arr, updates);

    AddTexture(arr, { 4, 4 });
    DumpUpdates(arr, updates);
}