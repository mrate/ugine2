#include <entt/entt.hpp>

#include <iostream>

void func1() {
    std::cout << "func1()" << std::endl;
}

class MyClass {
public:
    void mem1() {
        std::cout << "MyClass::mem1()" << std::endl;
    }
};

void OrganizerTest1() {
    entt::organizer organizer;

    organizer.emplace<&func1>();

    MyClass c1;
    organizer.emplace<&MyClass::mem1>(c1);

    //organizer.emplace(+[](const void*, entt::registry&) { std::cout << "lambda()" << std::endl; });
}