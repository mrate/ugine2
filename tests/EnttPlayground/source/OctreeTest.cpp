﻿#include <ugine/utils/Octree.h>

#include <entt/entt.hpp>

#include <iostream>

using EOctree = ugine::utils::Octree<entt::entity, entt::null>;

std::ostream& operator<<(std::ostream& out, const glm::vec3& v3) {
    out << "{" << v3.x << ", " << v3.y << ", " << v3.z << "}";
    return out;
}

std::ostream& operator<<(std::ostream& out, const ugine::math::AABB& aabb) {
    out << "{" << aabb.Min() << ", " << aabb.Max() << "}";
    return out;
}

std::ostream& operator<<(std::ostream& out, const EOctree& tree) {
    out << tree.Aabb() << std::endl;
    out << "Size: " << tree.Size() << std::endl;
    return out;
}

void OctreeTest() {
    using namespace ugine::utils;
    using namespace ugine::math;

    entt::registry r;
    auto e1{ r.create() };
    auto e2{ r.create() };

    EOctree octree;

    octree.Insert(e1, AABB{ glm::vec3{ 0, 0, 0 }, glm::vec3{ 1, 1, 1 } });
    std::cout << octree << std::endl;

    octree.Insert(e2, AABB{ glm::vec3{ 0.5f, 0.5f, 0.5f }, glm::vec3{ 0.75f, 1.75f, 1 } });
    std::cout << octree << std::endl;
}
