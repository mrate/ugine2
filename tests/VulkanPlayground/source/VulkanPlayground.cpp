﻿#include <iostream>
#include <random>

#include <ugine/core/Camera.h>
#include <ugine/core/Transformation.h>
#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/core/PipelineFactory.h>
#include <ugine/gfx/core/RenderTarget.h>
#include <ugine/gfx/core/VertexBuffer.h>
#include <ugine/gfx/data/ShaderBindings.h>
#include <ugine/gfx/data/Shapes.h>
#include <ugine/gfx/import/MeshLoader.h>
#include <ugine/gfx/rendering/LightShadingData.h>
#include <ugine/gfx/rendering/Material.h>
#include <ugine/gfx/rendering/MaterialInstance.h>
#include <ugine/gfx/rendering/Mesh.h>
#include <ugine/gfx/tools/Initializers.h>
#include <ugine/gui/Gui.h>
#include <ugine/gui/PropertyTable.h>
#include <ugine/system/Platform.h>

#include <ugine/gfx/tools/SpirvCompiler.h>
#include <ugine/utils/File.h>

#include <EngineBase.h>

#include <vulkan/vulkan.hpp>

#include <spirv_reflect.h>

#include "Output.h"

using namespace ugine;
using namespace ugine::core;
using namespace ugine::common;
using namespace ugine::gfx;
using namespace ugine::gui;
using namespace ugine::system;

class VulkanPlayground : public EngineBase {
public:
    using BaseClass = EngineBase;

    VulkanPlayground()
        : ugine::common::EngineBase(L"Vulkan playground", 1024, 768, false, true) {}

protected:
    void OnStart() override {
        InitGui();

        //TestShaderReflect(vk::ShaderStageFlagBits::eVertex, L"assets\\shaders\\test\\materialMulti.vert");
        //TestShaderReflect(L"assets\\shaders\\test\\forward.frag");
        //TestShaderReflect(vk::ShaderStageFlagBits::eVertex, L"assets\\shaders\\shadowmap.vert");
        //TestShaderReflect(vk::ShaderStageFlagBits::eFragment, L"assets\\shaders\\blinnphong.frag");

        InitGraphics();
    }

    void OnEnd() override {
        DestroyGraphics();
    }

    void OnEvent(const Event& event) override {
        if (event.type == Event::Type::KeyDown) {
            switch (event.key) {
            case 109: // Num '-'
                --lodLevel_;
                if (lodLevel_ < 0) {
                    lodLevel_ += static_cast<int>(mesh_.LodLevels());
                }
                std::cout << "Lod level: " << lodLevel_ << std::endl;
                break;
            case 107: // Num '+'
                lodLevel_ = (lodLevel_ + 1) % mesh_.LodLevels();
                std::cout << "Lod level: " << lodLevel_ << std::endl;
                break;
            }
        }
    }

    void OnUpdate(float deltaMS) override {
        float fps{};
        if (Fps(fps)) {
            char text[64];
            snprintf(text, 63, "uGine Vulkan Playground (%.0f fps)", fps);
            SetWindowTitle(text);
        }

        angle_ += deltaMS / 1000.0f;
        time_ += deltaMS;
    }

    void OnRender() override {
        //Gui().NewFrame();
        //DoGui();
        //Gui().EndFrame();

        auto& device{ GraphicsService::Device() };

        auto command{ device.BeginCommandList() };

        RenderContext context{};
        context.command = command;
        context.pass = RenderPassType::Main;

        int instanceCnt = 4;
        auto instanceBuffer{ device.AllocateGPU(command, instanceCnt * sizeof(VertexInstance)) };

        VertexInstance* instances{ reinterpret_cast<VertexInstance*>(instanceBuffer.memory) };
        for (int i = 0; i < instanceCnt; ++i) {
            glm::mat4 model{ glm::translate(glm::mat4{ 1.0f }, { -1.0f + (i & 1) * 2.0f, -1.0f + ((i >> 1) & 1) * 1.0f, 1.0f })
                * glm::rotate(glm::mat4{ 1.0f }, angle_, glm::vec3{ 0.0f, 1.0f, 0.0f }) * glm::scale(glm::mat4{ 1.0f }, { 1.0f, 1.0f, 1.0f }) };

            SetupVertexInstance(instances[i], model);
        }

        // Material.
        material_->Default().SetUniformVal("thisCouldBeSomeColor", glm::vec4{ 0.5f + std::sinf(angle_), 0.0f, 0.5f + std::cosf(angle_), 1.0f });

        FrameCB frameCB;
        frameCB.time = CoreService::TimeDiffS();

        CameraCB cameraCB;
        camera_.SetViewMatrix(glm::lookAt(glm::vec3(0.0f, std::sinf(angle_) * 2.0f, -4.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f)));
        SetupCameraCB(cameraCB, camera_);

        memcpy(frameBuffer_.Mapped(), &frameCB, frameBuffer_.Size());
        memcpy(cameraBuffer_.Mapped(), &cameraCB, cameraBuffer_.Size());

        //memcpy(matCB_.Mapped(), &matCB, matCB_.Size());
        //device.UpdateBuffer(command, frameBuffer_.GetBuffer(), 0, frameBuffer_.Size(), &frameCB);
        //device.UpdateBuffer(command, cameraBuffer_.GetBuffer(), 0, cameraBuffer_.Size(), &cameraCB);
        //device.UpdateBuffer(command, matCB_.GetBuffer(), 0, matCB_.Size(), &matCB);

        // Begin rendering.
        device.PresentBegin(command, Color{ 0.2f, 0.2f, 0.6f, 1.0f });

        context.renderPass = device.PresentRenderpass();
        context.SetViewportAndScrissor(Width(), Height());

        // Draw mesh.
        auto drawCall{ mesh_.DrawCall(lodLevel_) };

        device.BindPipeline(command, &pipeline_);
        material_->Default().Bind(command);

        device.BindDescriptor(command, GLOBAL_SHADING_DATASET, shadingData_->Descriptor());
        device.BindUniform(command, FRAME_DATASET, 0, frameBuffer_);
        device.BindUniform(command, CAMERA_DATASET, 0, cameraBuffer_);

        vk::Buffer VB[] = {
            mesh_.VertexBuffer()->VertexBuf().GetBuffer(),
            instanceBuffer.buffer,
        };

        vk::DeviceSize offsets[] = {
            0,
            instanceBuffer.offset,
        };

        device.BindVertexBuffers(command, 2, VB, offsets);
        device.BindIndexBuffer(command, mesh_.VertexBuffer()->IndexBuf().GetBuffer(), 0, vk::IndexType::eUint16);

        device.DrawIndexed(command, drawCall.indexCount, instanceCnt, drawCall.indexStart, drawCall.vertexStart, drawCall.firstInstance);

        // Present.
        device.PresentEnd(command);

        //Gui().Render(command);

        // Present pass.
        //device.PresentBegin(command);
        //RenderFullscreen(context, fullscreen_.output.ViewRef(), ActiveSwapChainView());
        //device.PresentEnd(command);

        //device.SubmitCommandLists();
    }

private:
    void RenderFullscreen(RenderContext& context, const TextureViewRef& input, vk::ImageView output) {
        fullscreen_.material->Default().SetTexture("inputTexture", input);

        vk::ClearValue clearValue{};
        ClearColor(clearValue, 0.4f, 0.5f, 0.8f, 1.0f);

        context.SetRenderTarget(0, output);

        GraphicsService::Device().BindPipeline(context.command, &fullscreen_.pipeline);
        fullscreen_.material->Bind(RenderPassType::Main, context.command);
        context.DrawFullscreen();

        context.ClearRenderTargets();
    }

    void DoGui() {
        ImGui::Begin("Playground");

        ImGui::End();
    }

    void InitGraphics() {
        MeshLoader loader;
        auto data{ loader.LoadMesh(L"assets/models/CesiumMan/glTF/CesiumMan.gltf") };

        mesh_ = Mesh{ 4, data[0].vertices, data[0].indices };
        material_ = MaterialService::InstantiateByPath(L"assets/materials/material.mat");
        pipeline_ = material_->Material().DefaultPass().CreatePipeline(GraphicsService::Device().PresentRenderpass());

        matCB_ = Buffer(sizeof(TestMaterialCB), vk::BufferUsageFlagBits::eUniformBuffer,
            vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);

        auto texture = TEXTURE_ADD(Texture::FromFile(L"assets/textures/bricks2.jpg"));
        matTexture_ = TEXTURE_VIEW_ADD(texture, TextureView::CreateSampled(*texture));

        shadingData_ = std::make_unique<LightShadingDataBinder>();

        // Common
        InitFullscreen(false);
        Transformation cameraPos{ { 0.0f, 0.0f, -5.0f }, { 1.0f, 0.0f, 0.0f, 0.0f }, { 1.0f, 1.0f, 1.0f } };
        camera_ = Camera::Perspective(60, static_cast<float>(Width()), static_cast<float>(Height()), 0.1f, 1000.f);
        camera_.SetViewMatrix(cameraPos.Matrix());

        cameraBuffer_ = Buffer(
            sizeof(CameraCB), vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCached);
        frameBuffer_ = Buffer(
            sizeof(FrameCB), vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCached);
    }

    void DestroyGraphics() {
        pipeline_ = {};
        material_ = {};
        mesh_ = {};
        cameraBuffer_ = {};
        shadingData_ = nullptr;

        // Common
        DestroyFullscreen();
    }

    void InitFullscreen(bool depth = false) {
        auto mat{ MaterialService::Register(depth ? L"assets/materials/quadDepth.mat" : L"assets/materials/quad.mat") };
        fullscreen_.material = MaterialService::Instantiate(mat->Name());
        fullscreen_.pipeline = mat->DefaultPass().CreatePipeline(GraphicsService::Device().PresentRenderpass());
    }

    void DestroyFullscreen() {
        fullscreen_.pipeline = {};
        fullscreen_.material = {};
    }

    MaterialInstanceRef material_;
    Pipeline pipeline_;

    Mesh mesh_;
    int lodLevel_{};

    struct {
        MaterialInstanceRef material;
        Pipeline pipeline;
        RenderTarget output;
    } fullscreen_;

    // Common data.
    Camera camera_;
    Buffer cameraBuffer_;
    Buffer frameBuffer_;

    std::unique_ptr<LightShadingDataBinder> shadingData_;

    struct TestMaterialCB {
        glm::vec4 color;
        glm::vec4 param;
    };

    Buffer matCB_;
    TextureViewRef matTexture_;

    float angle_{};
    float time_{ 0.0f };
};

int main(int argc, char* argv[]) {
    try {
        VulkanPlayground vk;
        vk.Run();
    } catch (const std::exception& ex) {
        std::cerr << "Error: " << ex.what() << std::endl;
        return 1;
    }

    return 0;
}
