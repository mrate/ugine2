﻿#include "Output.h"

#include <common/output_stream.h>
#include <spirv_reflect.h>

#include <cstring>
#include <fstream>
#include <sstream>

#include <ugine/gfx/tools/SpirvCompiler.h>
#include <ugine/utils/File.h>

void TestShaderReflect(vk::ShaderStageFlags stage, const std::filesystem::path& file) {
    using namespace ugine;
    using namespace ugine::gfx;

    SpvReflectResult result{};
    SpvReflectShaderModule reflectModule{};

    {
        std::string text{ utils::ReadFile(file) };
        std::string fileName{ file.string() };

        SpirvCompiler compiler;
        if (!compiler.Compile(stage, text, "main", fileName)) {
            std::cerr << "Failed to compile shader:" << compiler.Error() << std::endl;
            return;
        }

        result = spvReflectCreateShaderModule(compiler.Data().size(), compiler.Data().data(), &reflectModule);
        assert(result == SPV_REFLECT_RESULT_SUCCESS);
    }

    uint32_t count = 0;
    result = spvReflectEnumerateDescriptorSets(&reflectModule, &count, NULL);
    assert(result == SPV_REFLECT_RESULT_SUCCESS);

    std::vector<SpvReflectDescriptorSet*> sets(count);
    result = spvReflectEnumerateDescriptorSets(&reflectModule, &count, sets.data());
    assert(result == SPV_REFLECT_RESULT_SUCCESS);

    result = spvReflectEnumerateInputVariables(&reflectModule, &count, nullptr);
    assert(result == SPV_REFLECT_RESULT_SUCCESS);

    std::vector<SpvReflectInterfaceVariable*> inVars(count);
    result = spvReflectEnumerateInputVariables(&reflectModule, &count, inVars.data());
    assert(result == SPV_REFLECT_RESULT_SUCCESS);

    result = spvReflectEnumerateOutputVariables(&reflectModule, &count, nullptr);
    assert(result == SPV_REFLECT_RESULT_SUCCESS);

    std::vector<SpvReflectInterfaceVariable*> outVars(count);
    result = spvReflectEnumerateOutputVariables(&reflectModule, &count, outVars.data());
    assert(result == SPV_REFLECT_RESULT_SUCCESS);

#if defined(SPIRV_REFLECT_HAS_VULKAN_H)
    // Demonstrates how to generate all necessary data structures to create a
    // VkDescriptorSetLayout for each descriptor set in this shader.
    std::vector<DescriptorSetLayoutData> set_layouts(sets.size(), DescriptorSetLayoutData{});
    for (size_t i_set = 0; i_set < sets.size(); ++i_set) {
        const SpvReflectDescriptorSet& refl_set = *(sets[i_set]);
        DescriptorSetLayoutData& layout = set_layouts[i_set];
        layout.bindings.resize(refl_set.binding_count);
        for (uint32_t i_binding = 0; i_binding < refl_set.binding_count; ++i_binding) {
            const SpvReflectDescriptorBinding& refl_binding = *(refl_set.bindings[i_binding]);
            VkDescriptorSetLayoutBinding& layout_binding = layout.bindings[i_binding];
            layout_binding.binding = refl_binding.binding;
            layout_binding.descriptorType = static_cast<VkDescriptorType>(refl_binding.descriptor_type);
            layout_binding.descriptorCount = 1;
            for (uint32_t i_dim = 0; i_dim < refl_binding.array.dims_count; ++i_dim) {
                layout_binding.descriptorCount *= refl_binding.array.dims[i_dim];
            }
            layout_binding.stageFlags = static_cast<VkShaderStageFlagBits>(module.shader_stage);
        }
        layout.set_number = refl_set.set;
        layout.create_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        layout.create_info.bindingCount = refl_set.binding_count;
        layout.create_info.pBindings = layout.bindings.data();
    }
    // Nothing further is done with set_layouts in this sample; in a real application
    // they would be merged with similar structures from other shader stages and/or pipelines
    // to create a VkPipelineLayout.
#endif

    // Log the descriptor set contents to stdout
    const char* t = "  ";
    const char* tt = "    ";

    PrintModuleInfo(std::cout, reflectModule);
    std::cout << "\n\n";

    std::cout << "Descriptor sets:"
              << "\n";
    for (size_t index = 0; index < sets.size(); ++index) {
        auto p_set = sets[index];

        // descriptor sets can also be retrieved directly from the module, by set index
        auto p_set2 = spvReflectGetDescriptorSet(&reflectModule, p_set->set, &result);
        assert(result == SPV_REFLECT_RESULT_SUCCESS);
        assert(p_set == p_set2);
        (void)p_set2;

        std::cout << t << index << ":"
                  << "\n";
        PrintDescriptorSet(std::cout, *p_set, tt);
        std::cout << "\n\n";
    }

    for (const auto& var : inVars) {
        std::cout << "Input " << var->location << ": " << var->name << std::endl;
    }

    std::cout << std::endl;

    for (const auto& var : outVars) {
        std::cout << "Output " << var->location << ": " << var->name << std::endl;
    }

    SpvReflectToYaml yaml(reflectModule, 1);
    std::cout << yaml << std::endl;

    spvReflectDestroyShaderModule(&reflectModule);

    std::cout << "Ok" << std::endl;
}

void PrintModuleInfo(std::ostream& os, const SpvReflectShaderModule& obj, const char* /*indent*/) {
    os << "entry point     : " << obj.entry_point_name << "\n";
    os << "source lang     : " << spvReflectSourceLanguage(obj.source_language) << "\n";
    os << "source lang ver : " << obj.source_language_version << "\n";
    if (obj.source_language == SpvSourceLanguageHLSL) {
        os << "stage           : ";
        switch (obj.shader_stage) {
        default:
            break;
        case SPV_REFLECT_SHADER_STAGE_VERTEX_BIT:
            os << "VS";
            break;
        case SPV_REFLECT_SHADER_STAGE_TESSELLATION_CONTROL_BIT:
            os << "HS";
            break;
        case SPV_REFLECT_SHADER_STAGE_TESSELLATION_EVALUATION_BIT:
            os << "DS";
            break;
        case SPV_REFLECT_SHADER_STAGE_GEOMETRY_BIT:
            os << "GS";
            break;
        case SPV_REFLECT_SHADER_STAGE_FRAGMENT_BIT:
            os << "PS";
            break;
        case SPV_REFLECT_SHADER_STAGE_COMPUTE_BIT:
            os << "CS";
            break;
        }
    }
}

void PrintDescriptorSet(std::ostream& os, const SpvReflectDescriptorSet& obj, const char* indent) {
    const char* t = indent;
    std::string tt = std::string(indent) + "  ";
    std::string ttttt = std::string(indent) + "    ";

    os << t << "set           : " << obj.set << "\n";
    os << t << "binding count : " << obj.binding_count;
    os << "\n";
    for (uint32_t i = 0; i < obj.binding_count; ++i) {
        const SpvReflectDescriptorBinding& binding = *obj.bindings[i];
        os << tt << i << ":"
           << "\n";
        PrintDescriptorBinding(os, binding, false, ttttt.c_str());
        if (i < (obj.binding_count - 1)) {
            os << "\n";
        }
    }
}

void PrintDescriptorBinding(std::ostream& os, const SpvReflectDescriptorBinding& obj, bool write_set, const char* indent) {
    const char* t = indent;
    os << t << "binding : " << obj.binding << "\n";
    if (write_set) {
        os << t << "set     : " << obj.set << "\n";
    }
    os << t << "type    : " << ToStringDescriptorType(obj.descriptor_type) << "\n";

    // array
    if (obj.array.dims_count > 0) {
        os << t << "array   : ";
        for (uint32_t dim_index = 0; dim_index < obj.array.dims_count; ++dim_index) {
            os << "[" << obj.array.dims[dim_index] << "]";
        }
        os << "\n";
    }

    // counter
    if (obj.uav_counter_binding != nullptr) {
        os << t << "counter : ";
        os << "(";
        os << "set=" << obj.uav_counter_binding->set << ", ";
        os << "binding=" << obj.uav_counter_binding->binding << ", ";
        os << "name=" << obj.uav_counter_binding->name;
        os << ");";
        os << "\n";
    }

    os << t << "name    : " << obj.name;
    if ((obj.type_description->type_name != nullptr) && (strlen(obj.type_description->type_name) > 0)) {
        os << " "
           << "(" << obj.type_description->type_name << ")";
    }
}

void PrintInterfaceVariable(std::ostream& os, SpvSourceLanguage src_lang, const SpvReflectInterfaceVariable& obj, const char* indent) {
    const char* t = indent;
    os << t << "location  : ";
    if (obj.decoration_flags & SPV_REFLECT_DECORATION_BUILT_IN) {
        os << ToStringSpvBuiltIn(obj.built_in) << " "
           << "(built-in)";
    } else {
        os << obj.location;
    }
    os << "\n";
    if (obj.semantic != nullptr) {
        os << t << "semantic  : " << obj.semantic << "\n";
    }
    os << t << "type      : " << ToStringType(src_lang, *obj.type_description) << "\n";
    os << t << "format    : " << ToStringFormat(obj.format) << "\n";
    os << t << "qualifier : ";
    if (obj.decoration_flags & SPV_REFLECT_DECORATION_FLAT) {
        os << "flat";
    } else if (obj.decoration_flags & SPV_REFLECT_DECORATION_NOPERSPECTIVE) {
        os << "noperspective";
    }
    os << "\n";

    os << t << "name      : " << obj.name;
    if ((obj.type_description->type_name != nullptr) && (strlen(obj.type_description->type_name) > 0)) {
        os << " "
           << "(" << obj.type_description->type_name << ")";
    }
}
