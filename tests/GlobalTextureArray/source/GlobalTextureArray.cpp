﻿#include <ugine/utils/assert.h>

#include <array>
#include <iostream>
#include <optional>
#include <stdint.h>
#include <vector>

template <typename T, size_t _MaxSize> class GlobalArray {
public:
    struct Ref {
        static constexpr auto INVALID{ static_cast<uint32_t>(-1) };

        uint32_t id{};

        operator uint32_t() const {
            return id;
        }

        bool Valid() const {
            return id != INVALID;
        }
    };

    using ValueType = T;

    static constexpr auto MAX_SIZE{ _MaxSize };

    GlobalArray() {
        Init();
    }

    GlobalArray(std::optional<T> nullValue)
        : nullValue_(nullValue) {
        Init();
    }

    Ref Add(T&& value) {
        UGINE_ASSERT(!freeSlots_.empty());

        auto idx{ freeSlots_.back() };
        freeSlots_.pop_back();
        storage_[idx] = std::move(value);
        return Ref{ idx };
    }

    T&& Remove(Ref ref) {
        auto idx{ ref.id };
        auto res{ std::move(storage_[idx]) };
        if (nullValue_.has_value()) {
            storage_[idx] = nullValue_;
        }
        freeSlots_.push_back(idx);
        return res;
    }

private:
    void Init() {
        freeSlots_.resize(MAX_SIZE);
        for (uint32_t i = 0; i < MAX_SIZE; ++i) {
            freeSlots_[i] = MAX_SIZE - i - 1;
        }

        if (nullValue_.has_value()) {
            storage_.fill(*nullValue_);
        }
    }

    std::array<T, MAX_SIZE> storage_;
    std::vector<uint32_t> freeSlots_;
    std::optional<T> nullValue_;
};

struct Texture {
    int w{};
    int h{};
};

using TextureArray = GlobalArray<Texture, 10>;
using TextureRef = TextureArray::Ref;

void Test1() {
    TextureArray textures(Texture{ -1, -1 });

    for (uint32_t i = 0; i < textures.MAX_SIZE; ++i) {
    }
}

void Test2() {}

int main(int argc, char* argv[]) {
    Test1();
    return 0;
}
