﻿#include <iostream>

#include <ugine/core/Scene.h>
#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/Renderer.h>
#include <ugine/gfx/core/RenderTarget.h>
#include <ugine/gfx/data/Shapes.h>
#include <ugine/gui/Gui.h>
#include <ugine/script/DefaultNatives.h>
#include <ugine/system/Platform.h>

#include <EngineBase.h>

#include "Physics.h"

#include <cstdlib>

using namespace ugine;
using namespace ugine::core;
using namespace ugine::gfx;
using namespace ugine::script;
using namespace ugine::system;

class Demo : public ugine::common::EngineBase {
public:
    using BaseClass = ugine::common::EngineBase;

    Demo()
        : ugine::common::EngineBase(L"PhysX test", 1024, 768, false, true) {
        std::srand(static_cast<unsigned int>(std::time(nullptr)));
    }

protected:
    void OnStart() override {
        CreateCommands();
        CreateRenderPass();
        CreateFramebuffers();

        InitGui();

        output_ = RenderTarget(Width(), Height(), vk::Format::eR8G8B8A8Unorm);
        renderSystem_ = std::make_unique<ugine::core::RenderSystem>();

        auto mat{ MaterialService::Register(L"assets\\materials\\quad.mat") };
        fullScreenMat_ = MaterialService::Instantiate(mat->Name());
        fullScreenPipeline_ = mat->DefaultPass().CreatePipeline(RenderPass());

        physics_ = std::make_unique<Physics>();

        InitScene();
    }

    void OnEnd() override {
        physics_ = nullptr;

        scene_ = {};
        renderSystem_ = nullptr;
        fullScreenPipeline_ = {};
        fullScreenMat_ = {};
        output_ = {};

        DestroyFramebuffers();
        DestroyRenderPass();
        DestroyCommands();
    }

    void OnResize(unsigned int width, unsigned int height) override {
        BaseClass::OnResize(width, height);

        output_ = RenderTarget(Width(), Height(), vk::Format::eR8G8B8A8Unorm);
    }

    void OnEvent(const Event& event) override {
        if (event.type == Event::Type::KeyUp) {
            if (event.key == 'S') {
                SpawnSphere(0, 4, 0);
            } else if (event.key == 'B') {
                SpawnBox(0, 4, 0);
            } else if (event.key == 'P') {
                physPaused_ = !physPaused_;
                std::cout << "Physics " << (physPaused_ ? "paused" : "resumed") << std::endl;
            }
        }
    }

    void OnUpdate(float deltaMS) override {
        float fps{};
        if (Fps(fps)) {
            char text[32];
            snprintf(text, sizeof(text) - 1, "uGine2 demo! (%.0f fps)", fps);
            SetWindowTitle(text);
        }

        scene_->Update();
        if (!physPaused_) {
            physics_->Simulate(deltaMS);
            physics_->Update(*scene_);
        }
    }

    void OnRender() override {
        Color clearColor{ 0.4f, 0.4f, 0.8f, 1.0f };
        auto command{ BeginRenderCommand() };

        renderSystem_->Render(command, *scene_);
        scene_->PostRender(command);

        fullScreenMat_->Default().SetTexture("inputTexture", output_.ViewRef());
        fullScreenMat_->Synchronize(RenderPassType::Default, command);

        BeginRenderPass(command, clearColor);

        fullScreenPipeline_.Bind(command);
        command.setViewport(0, Viewport());
        command.setScissor(0, Scissor());
        fullScreenMat_->Bind(RenderPassType::Default, command, fullScreenPipeline_.Layout());
        command.draw(3, 1, 0, 0);

        // Render gui.
        EndRenderPass(command);

        EndRenderCommand(command);
    }

    float RandFloat() {
        return static_cast<float>(std::rand()) / static_cast<float>(RAND_MAX);
    }

    glm::vec3 RandomColor() {
        return glm::vec4{ RandFloat(), RandFloat(), RandFloat(), 1.0f };
    }

    void SpawnBox(float x, float y, float z) {
        static int index = 0;
        std::string name = "Box ";

        auto go{ scene_->CreateObject(name + std::to_string(++index)) };
        go.MoveTo(x, y, z);
        auto& mat{ go.CreateComponent<MeshComponent>(Cube(), MaterialService::Instantiate("BlinnPhong")).material };
        auto col{ RandomColor() };
        mat->Default().SetUniformVal("diffuseColor", col);
        mat->Default().SetUniformVal("ambientColor", col);
        go.CreateComponent<PhysicsComponent>(PhysicsComponent::Shape::Box, true);

        physics_->Add(go);
    }

    void SpawnSphere(float x, float y, float z) {
        static int index = 0;
        std::string name = "Sphere ";

        auto go{ scene_->CreateObject(name + std::to_string(++index)) };
        go.MoveTo(x, y, z);
        auto& mat{ go.CreateComponent<MeshComponent>(Sphere(), MaterialService::Instantiate("BlinnPhong")).material };
        mat->Default().SetUniformVal("diffuseColor", RandomColor());
        go.CreateComponent<PhysicsComponent>(PhysicsComponent::Shape::Sphere, true);

        physics_->Add(go);
    }

    void InitScene() {
        auto lightGO{ scene_->CreateObject("Light") };
        //lightGO.MoveTo(-2, -2, -4);
        //lightGO.LookAt(glm::vec3{ 0, 0, 0 });
        lightGO.MoveTo(10, 10, 10);
        auto& light{ lightGO.CreateComponent<LightComponent>() };
        light.light.type = Light::Type::Point;
        //light.light.ambientColor = glm::vec4{ 1.0f, 1.0f, 1.0f, 1.0f };
        light.light.color = glm::vec4{ 1.0f, 1.0f, 1.0f, 1.0f };
        light.light.range = 100.0f;
        light.generatesShadows = true;

        auto cameraGO{ scene_->CreateObject("Camera") };
        cameraGO.CreateComponent<CameraComponent>(Camera::Perspective(90, 100, 100, 0.1f, 100.0f), true);
        cameraGO.MoveTo(2, 2, 4);
        cameraGO.LookAt(glm::vec3{ 0, 0, 0 });
        cameraGO.CreateComponent<NativeScriptComponent>().Add(cameraGO, NativeScripts::Instance().Create("Camera controller"));

        auto floorGO{ scene_->CreateObject("Floor") };
        auto& floorMat{ floorGO.CreateComponent<MeshComponent>(Cube(), MaterialService::Instantiate("BlinnPhong")).material };
        floorMat->Default().SetUniformVal("diffuseColor", glm::vec4{ 1.0f, 1.0f, 1.0f, 1.0f });
        floorGO.Scale(100, 0.2f, 100);
        floorGO.MoveTo(0, -1, 0);
        floorGO.CreateComponent<PhysicsComponent>(PhysicsComponent::Shape::Box, false);

        physics_->Init(*scene_);

        //SpawnBox(0, 4, 0);
    }

private:
    RenderTarget output_;

    MaterialInstanceRef fullScreenMat_;
    Pipeline fullScreenPipeline_;
    std::unique_ptr<ugine::core::Scene> scene_;
    std::unique_ptr<ugine::core::RenderSystem> renderSystem_;
    std::unique_ptr<Physics> physics_;
    bool physPaused_{};
};

int main(int argc, char* argv[]) {
    try {
        Demo demo;
        demo.Run();
    } catch (const std::exception& ex) {
        std::cerr << "Error: " << ex.what() << std::endl;
        return 1;
    }

    return 0;
}
