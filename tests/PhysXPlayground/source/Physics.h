#pragma once

#include <ugine/core/Scene.h>

struct PhysicsComponent {
    enum class Shape { Box, Sphere };

    Shape shape;
    bool dynamic{};
    glm::vec3 velocity{ 0.0f };
};

class Physics {
public:
    Physics();
    ~Physics();

    void Add(const ugine::core::GameObject& go);

    void Init(ugine::core::Scene& scene);
    void Simulate(float deltaMS);
    void Update(ugine::core::Scene& scene);
};