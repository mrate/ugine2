﻿#include "Physics.h"

#include <ugine/utils/Log.h>

#include <PxPhysicsAPI.h>

#pragma comment(lib, "PhysX_64.lib")
#pragma comment(lib, "PhysXFoundation_64.lib")
#pragma comment(lib, "PhysXCommon_64.lib")
#pragma comment(lib, "PhysXPvdSDK_static_64.lib")
#pragma comment(lib, "PhysXExtensions_static_64.lib")

using namespace physx;

namespace {

#define PX_RELEASE(x)                                                                                                                                          \
    if (x) {                                                                                                                                                   \
        x->release();                                                                                                                                          \
        x = NULL;                                                                                                                                              \
    }

PxDefaultAllocator gAllocator;
//PxDefaultErrorCallback gErrorCallback;

PxFoundation* gFoundation{};
PxPhysics* gPhysics{};

PxDefaultCpuDispatcher* gDispatcher{};
PxScene* gScene{};

PxMaterial* gMaterial{};

//PxPvd* gPvd{};

class UserErrorCallback : public PxErrorCallback {
public:
    void reportError(PxErrorCode::Enum code, const char* message, const char* file, int line) override {
        UGINE_ERROR("PhysX error {}: {} ({}, {})", code, message, file, line);
    }
};

UserErrorCallback gErrorCallback;

PxRigidDynamic* createDynamic(const PxTransform& t, const PxGeometry& geometry, const PxVec3& velocity = PxVec3(0)) {
    PxRigidDynamic* dynamic = PxCreateDynamic(*gPhysics, t, geometry, *gMaterial, 10.0f);
    dynamic->setAngularDamping(0.5f);
    dynamic->setLinearVelocity(velocity);
    gScene->addActor(*dynamic);
    return dynamic;
}

PxRigidStatic* createStatic(const PxTransform& t, const PxGeometry& geometry) {
    PxRigidStatic* st = PxCreateStatic(*gPhysics, t, geometry, *gMaterial);
    gScene->addActor(*st);
    return st;
}

void initPhysics() {
    gFoundation = PxCreateFoundation(PX_PHYSICS_VERSION, gAllocator, gErrorCallback);
    UGINE_ASSERT(gFoundation);

    //gPvd = PxCreatePvd(*gFoundation);
    //PxPvdTransport* transport = PxDefaultPvdSocketTransportCreate("127.0.0.1", 5425, 10);
    //gPvd->connect(*transport, PxPvdInstrumentationFlag::eALL);

    gPhysics = PxCreatePhysics(PX_PHYSICS_VERSION, *gFoundation, PxTolerancesScale(), true, nullptr);

    PxSceneDesc sceneDesc(gPhysics->getTolerancesScale());
    sceneDesc.gravity = PxVec3(0.0f, -9.81f, 0.0f);
    gDispatcher = PxDefaultCpuDispatcherCreate(2);
    sceneDesc.cpuDispatcher = gDispatcher;
    sceneDesc.filterShader = PxDefaultSimulationFilterShader;
    gScene = gPhysics->createScene(sceneDesc);

    PxPvdSceneClient* pvdClient = gScene->getScenePvdClient();
    if (pvdClient) {
        pvdClient->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_CONSTRAINTS, true);
        pvdClient->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_CONTACTS, true);
        pvdClient->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_SCENEQUERIES, true);
    }
    gMaterial = gPhysics->createMaterial(0.5f, 0.5f, 0.6f);

    PxRigidStatic* groundPlane = PxCreatePlane(*gPhysics, PxPlane(0, 1, 0, 0), *gMaterial);
    gScene->addActor(*groundPlane);
}

void stepPhysics(float s) {
    gScene->simulate(s);
    gScene->fetchResults(true);
}

void cleanupPhysics() {
    PX_RELEASE(gScene);
    PX_RELEASE(gDispatcher);
    PX_RELEASE(gPhysics);
    //if (gPvd) {
    //    PxPvdTransport* transport = gPvd->getTransport();
    //    gPvd->release();
    //    gPvd = NULL;
    //    PX_RELEASE(transport);
    //}
    PX_RELEASE(gFoundation);

    printf("SnippetHelloWorld done.\n");
}

PxVec3 ToPhys(const glm::vec3& g) {
    return PxVec3{ g.x, g.y, g.z };
}

PxQuat ToPhys(const glm::fquat& g) {
    return PxQuat{ g.x, g.y, g.z, g.w };
}

PxTransform ToPhys(const ugine::core::Transformation& t) {
    return PxTransform{ ToPhys(t.position), ToPhys(t.rotation) };
}

glm::vec3 FromPhys(const PxVec3& v) {
    return glm::vec3{ v.x, v.y, v.z };
}

glm::fquat FromPhys(const PxQuat& v) {
    return glm::fquat{ v.w, v.x, v.y, v.z };
}

ugine::core::Transformation FromPhys(const PxTransform& t) {
    return ugine::core::Transformation{ FromPhys(t.p), FromPhys(t.q), glm::vec3{ 1.0f } };
}

} // namespace

Physics::Physics() {
    initPhysics();
}

Physics::~Physics() {
    cleanupPhysics();
}

void Physics::Add(const ugine::core::GameObject& go) {
    const auto& trans{ go.Component<ugine::core::TransformationComponent>().transformation };
    auto t{ ToPhys(trans) };
    const auto& phys{ go.Component<PhysicsComponent>() };

    PxActor* actor{};
    if (phys.dynamic) {
        switch (phys.shape) {
        case PhysicsComponent::Shape::Box:
            actor = createDynamic(t, PxBoxGeometry{ ToPhys(0.5f * trans.scale) }, ToPhys(phys.velocity));
            break;
        case PhysicsComponent::Shape::Sphere:
            actor = createDynamic(t, PxSphereGeometry{ 0.5f * trans.scale.x }, ToPhys(phys.velocity));
            break;
        }
    } else {
        switch (phys.shape) {
        case PhysicsComponent::Shape::Box:
            actor = createStatic(t, PxBoxGeometry{ ToPhys(0.5f * trans.scale) });
            break;
        case PhysicsComponent::Shape::Sphere:
            actor = createStatic(t, PxSphereGeometry{ 0.5f * trans.scale.x });
            break;
        }
    }

    actor->userData = reinterpret_cast<void*>(static_cast<uint64_t>(go.Entity()));
}

void Physics::Init(ugine::core::Scene& scene) {
    for (auto [entt, trans, mesh, phys] : scene.Registry().view<ugine::core::TransformationComponent, ugine::core::MeshComponent, PhysicsComponent>().each()) {
        Add(scene.Get(entt));
    }
}

void Physics::Simulate(float deltaMS) {
    stepPhysics(deltaMS / 1000.0f);
}

void Physics::Update(ugine::core::Scene& scene) {
    PxScene* pScene;
    PxGetPhysics().getScenes(&pScene, 1);
    PxU32 nbActors = pScene->getNbActors(PxActorTypeFlag::eRIGID_DYNAMIC | PxActorTypeFlag::eRIGID_STATIC);
    if (nbActors) {
        std::vector<PxRigidActor*> actors(nbActors);
        pScene->getActors(PxActorTypeFlag::eRIGID_DYNAMIC | PxActorTypeFlag::eRIGID_STATIC, reinterpret_cast<PxActor**>(&actors[0]), nbActors);

        for (const auto& actor : actors) {
            auto t{ actor->getGlobalPose() };

            auto v{ reinterpret_cast<uint64_t>(actor->userData) };
            auto e{ static_cast<entt::entity>(v) };

            auto go{ scene.Get(e) };
            go.Patch<ugine::core::TransformationComponent>([&](auto& trans) { trans.transformation = FromPhys(t); });
        }
    }
}
