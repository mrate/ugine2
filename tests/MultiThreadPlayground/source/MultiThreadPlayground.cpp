﻿#include <iostream>
#include <random>

#include "Sync.h"

#include <ugine/core/Camera.h>
#include <ugine/core/Transformation.h>
#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/core/PipelineFactory.h>
#include <ugine/gfx/core/RenderTarget.h>
#include <ugine/gfx/core/VertexBuffer.h>
#include <ugine/gfx/data/Shapes.h>
#include <ugine/gfx/import/MeshLoader.h>
#include <ugine/gfx/rendering/Material.h>
#include <ugine/gfx/rendering/MaterialInstance.h>
#include <ugine/gfx/rendering/Mesh.h>
#include <ugine/gfx/tools/Initializers.h>
#include <ugine/gui/Gui.h>
#include <ugine/gui/PropertyTable.h>
#include <ugine/system/Platform.h>

#include <ugine/core/Scheduler.h>

#include <ugine/gfx/tools/SpirvCompiler.h>
#include <ugine/utils/File.h>

#include <EngineBase.h>

#include <TaskScheduler.h>

#include <vulkan/vulkan.hpp>

#include <thread>

using namespace ugine;
using namespace ugine::core;
using namespace ugine::common;
using namespace ugine::gfx;
using namespace ugine::gui;
using namespace ugine::system;

class MultiThreadPlayground : public EngineBase {
public:
    using BaseClass = EngineBase;

    MultiThreadPlayground()
        : ugine::common::EngineBase(L"Vulkan playground", 1024, 768, false, true) {

        SetMultithread(true);
    }

protected:
    void OnStart() override {
        InitGui();

        InitGraphics();

        rendering_ = true;
        renderThread_ = std::thread([&]() { RenderThread(); });
    }

    void OnEnd() override {
        Signal(updateStage_, renderStage_, updateFrame_);
        rendering_ = false;
        if (renderThread_.joinable()) {
            renderThread_.join();
        }

        DestroyGraphics();
    }

    void OnEvent(const Event& event) override {
        if (event.type == Event::Type::KeyDown) {
            switch (event.key) {
            case 109: // Num '-'
                --lodLevel_;
                if (lodLevel_ < 0) {
                    lodLevel_ += static_cast<int>(mesh_.LodLevels());
                }
                std::cout << "Lod level: " << lodLevel_ << std::endl;
                break;
            case 107: // Num '+'
                lodLevel_ = (lodLevel_ + 1) % mesh_.LodLevels();
                std::cout << "Lod level: " << lodLevel_ << std::endl;
                break;
            }
        }
    }

    std::mutex mutex_;
    SyncStage renderStage_;
    SyncStage updateStage_;

    uint64_t updateFrame_{};

    void Signal(SyncStage& srcStage, SyncStage& dstStage, uint64_t frame) {
        {
            std::scoped_lock lock(mutex_);
            srcStage.frame = frame;
            std::swap(dstStage.pendingList, srcStage.renderList);
        }

        srcStage.cv.notify_all();
    }

    void Wait(SyncStage& srcStage, SyncStage& dstStage) {
        std::unique_lock lock(mutex_);
        srcStage.cv.wait(lock, [&]() { return srcStage.frame == dstStage.frame || !rendering_; });

        std::swap(srcStage.pendingList, srcStage.renderList);
    }

    void FillRenderList(RenderList& list) {
        Renderpass pass;

        pass.model = glm::translate(glm::mat4{ 1.0f }, { 0.0f, 0.0f, 1.0f }) * glm::scale(glm::mat4{ 1.0f }, { 1.0f, 1.0f, 1.0f });
        pass.view = camera_->ViewMatrix();
        pass.projection = camera_->ProjectionMatrix();

        pass.clearColor = Color(0.2f, 0.2f, 0.6f, 1.0f);
        pass.viewportWidth = Width();
        pass.viewportHeight = Height();
        pass.renderPass = GraphicsService::Device().PresentRenderpass();

        pass.pipeline = &pipeline_;

        auto drawCall{ mesh_.DrawCall(lodLevel_) };
        drawCall.material = &material_;
        pass.drawCalls.push_back(drawCall);

        list.passes.push_back(pass);
    }

    void OnUpdate(float deltaMS) override {
        float fps{};
        if (Fps(fps)) {
            char text[64];
            snprintf(text, 63, "uGine Vulkan Playground (%.0f fps)", fps);
            SetWindowTitle(text);

            std::cout << "Update thread: " << fps << std::endl;
        }

        time_ += deltaMS;

        // Fill render list.
        updateStage_.renderList.passes.clear();
        FillRenderList(updateStage_.renderList);

        ++updateFrame_;
        Signal(updateStage_, renderStage_, updateFrame_);
        Wait(renderStage_, updateStage_);
    }

    std::atomic_bool rendering_{};
    std::thread renderThread_{};

    void RenderThread() {
        utils::FpsCounter renderFps;

        uint64_t frame{};
        while (rendering_) {
            float fps{};
            if (renderFps.Fps(fps)) {
                std::cout << "Render thread: " << fps << std::endl;
            }

            BeginFrame();

            auto command{ GraphicsService::Device().BeginCommandList() };

            RenderContext context{};
            context.command = command;
            context.pass = RenderPassType::ForwardTransparent;

            // Shadow pass.
            {
                glm::mat4 model{ glm::translate(glm::mat4{ 1.0f }, { 0.0f, 0.0f, 1.0f }) * glm::scale(glm::mat4{ 1.0f }, { 1.0f, 1.0f, 1.0f }) };
                glm::mat4 modelView{ camera_->ViewMatrix() * model };
                glm::mat4 modelViewProj{ camera_->ProjectionMatrix() * modelView };

                material_->Default().SetVsCommon(model, modelView, modelViewProj);
                material_->Default().Sync(command);

                vk::ClearValue clearValue{};
                ClearColor(clearValue, 0.2f, 0.2f, 0.6f, 1.0f);

                context.SetViewportAndScrissor(Width(), Height());
                context.SetRenderTarget(0, *GraphicsService::Presenter().SwapChainViews()[GraphicsService::ActiveFrameInFlight()]);
                context.BeginRenderPass(GraphicsService::Device().PresentRenderpass(), 1, &clearValue);

                GraphicsService::VertexBuffer()->Bind(command);
                GraphicsService::Device().BindPipeline(command, &pipeline_);
                material_->Bind(RenderPassType::ForwardTransparent, command);
                auto drawCall{ mesh_.DrawCall(lodLevel_) };
                drawCall.material = &material_;
                GraphicsService::MaterialRenderer().Render(context, DrawCallList{ drawCall });

                context.EndRenderPass();
                context.ClearRenderTargets();
            }

            //Gui().Render(command);

            // Present pass.
            //RenderFullscreen(context, fullscreen_.output.ViewRef(), ActiveSwapChainView());

            GraphicsService::Device().PresentEnd(command);

            ++frame;
            Signal(renderStage_, updateStage_, frame);
            Wait(updateStage_, renderStage_);
        }
    }

    void OnRender() override {
        //Gui().NewFrame();
        //DoGui();
        //Gui().EndFrame();
    }

private:
    void RenderFullscreen(RenderContext& context, const TextureViewRef& input, vk::ImageView output) {
        fullscreen_.material->Default().SetTexture("inputTexture", input);
        fullscreen_.material->Default().Sync(context.command);

        vk::ClearValue clearValue{};
        ClearColor(clearValue, 0.4f, 0.5f, 0.8f, 1.0f);

        context.SetRenderTarget(0, output);
        context.BeginRenderPass(GraphicsService::Device().PresentRenderpass(), 1, &clearValue);

        GraphicsService::Device().BindPipeline(context.command, &fullscreen_.pipeline);
        fullscreen_.material->Bind(RenderPassType::Default, context.command);
        context.DrawFullscreen();

        context.EndRenderPass();
        context.ClearRenderTargets();
    }

    void DoGui() {
        ImGui::Begin("Playground");

        ImGui::Text("Hello threads");

        ImGui::End();
    }

    void InitGraphics() {
        //mesh_ = Sphere();

        MeshLoader loader;
        auto data{ loader.LoadMesh(L"assets/models/CesiumMan/glTF/CesiumMan.gltf") };

        mesh_ = Mesh{ 4, data[0].vertices, data[0].indices };

        material_ = MaterialService::InstantiateByPath(L"assets/materials/wireframe.mat");
        pipeline_ = material_->Material().DefaultPass().CreatePipeline(GraphicsService::Device().PresentRenderpass());

        // Common
        InitFullscreen(false);
        Transformation cameraPos{ { 0.0f, 0.0f, -5.0f }, { 1.0f, 0.0f, 0.0f, 0.0f }, { 1.0f, 1.0f, 1.0f } };
        camera_ = std::make_unique<Camera>(Camera::Perspective(60, static_cast<float>(Width()), static_cast<float>(Height()), 0.1f, 1000.f));
        camera_->SetViewMatrix(cameraPos.Matrix());
    }

    void DestroyGraphics() {
        pipeline_ = {};
        material_ = {};

        mesh_ = {};

        // Common
        DestroyFullscreen();
        camera_ = nullptr;
    }

    void InitFullscreen(bool depth = false) {
        auto mat{ MaterialService::Register(depth ? L"assets/materials/quadDepth.mat" : L"assets/materials/quad.mat") };
        fullscreen_.material = MaterialService::Instantiate(mat->Name());
        fullscreen_.pipeline = mat->DefaultPass().CreatePipeline(GraphicsService::Device().PresentRenderpass());
        //fullscreen_.output = RenderTarget(Width(), Height(), GraphicsService::Presenter().SwapChainFormat());
    }

    void DestroyFullscreen() {
        fullscreen_.pipeline = {};
        fullscreen_.material = {};
    }

    MaterialInstanceRef material_;
    Pipeline pipeline_;

    Mesh mesh_;
    int lodLevel_{};

    struct {
        MaterialInstanceRef material;
        Pipeline pipeline;
        RenderTarget output;
    } fullscreen_;

    // Common data.
    std::unique_ptr<Camera> camera_;

    float angle_{};
    float time_{ 0.0f };
};

int main(int argc, char* argv[]) {
    try {
        MultiThreadPlayground vk;
        vk.Run();
    } catch (const std::exception& ex) {
        std::cerr << "Error: " << ex.what() << std::endl;
        return 1;
    }

    return 0;
}
