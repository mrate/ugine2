﻿#pragma once

#include <condition_variable>
#include <mutex>

#include <ugine/gfx/Color.h>
#include <ugine/gfx/DrawCall.h>
#include <ugine/gfx/Gfx.h>

#include <glm/glm.hpp>

namespace ugine::gfx {
struct Renderpass {
    RenderPassType pass;
    Color clearColor;

    glm::mat4 model;
    glm::mat4 view;
    glm::mat4 projection;

    uint32_t viewportWidth;
    uint32_t viewportHeight;

    vk::RenderPass renderPass;
    Pipeline* pipeline{};

    DrawCallList drawCalls;
};

struct RenderList {
    std::vector<Renderpass> passes;
};
} // namespace ugine::gfx

struct SyncStage {
    std::condition_variable cv;
    std::mutex mutex;
    uint64_t frame{};
    ugine::gfx::RenderList renderList;
    ugine::gfx::RenderList pendingList;
};
