#pragma once

#include <ugine/script/NativeScript.h>

class VrScript : public ugine::script::NativeScript {
public:
    void OnAttach() override;
    void OnDetach() override;
    void Update() override;
    void PostRender() override;

protected:
    virtual void InitVr() = 0;
    virtual void DestroyVr() = 0;
    virtual void GetProjection(ugine::core::Camera& left, ugine::core::Camera& right) = 0;
    virtual void GetHmdPose(ugine::core::Transformation& trans) = 0;
    virtual void GetCameraPoses(ugine::core::Transformation& left, ugine::core::Transformation& right) = 0;
    virtual void GetResolution(uint32_t& width, uint32_t& height) = 0;
    virtual void Present(const ugine::gfx::TextureViewRef& left, const ugine::gfx::TextureViewRef& right) = 0;

private:
    ugine::core::GameObject leftCam_;
    ugine::core::GameObject rightCam_;

    ugine::gfx::RenderTarget leftRenderTarget_;
    ugine::gfx::RenderTarget rightRenderTarget_;
};