﻿#ifdef VRG_HMD
#include "Vr.h"

// Needed Vulkan extensions:
//, VK_KHR_EXTERNAL_MEMORY_WIN32_EXTENSION_NAME, VK_KHR_WIN32_KEYED_MUTEX_EXTENSION_NAME };

#include <ugine/script/NativeScripts.h>
#include <ugine/utils/Log.h>

#include "../../../vrg_sdk/generated/vrg.h"
#include "../../../vrg_sdk/generated/vrg_vulkan.h"

#pragma comment(lib, "hmd64")

namespace {

void Check(VrgErrorCode error) {
    if (error != VRG_ERROR_OK) {
        const char* str = nullptr;
        vrgGetErrorMessage(error, &str);
        UGINE_ERROR("Vrg error: {}", str);
        throw std::exception("VRG ERROR");
    }
}

} // namespace

class Vrg : public VrScript {
protected:
    const char* Name() const override {
        return "VRG hmd";
    };

    void InitVr() override {
        Check(vrgInitApi(&VRG_API_INIT_INFO));

        uint32_t count{};
        Check(vrgEnumerateDevices(&count, nullptr));
        if (count == 0) {
            throw std::exception("NO HMD");
        }

        std::vector<VrgDevice> devices(count);
        Check(vrgEnumerateDevices(&count, devices.data()));

        id_ = devices[0].id;

        Check(vrgInitDevice(id_));

        Check(vrgSetBool(id_, VRG_BOOL_ASYNC_TIMEWARP, 0));

        VkDevice device{ ugine::gfx::GraphicsService::Graphics().Device() };
        VkPhysicalDevice physDevice{ ugine::gfx::GraphicsService::Graphics().PhysicalDevice() };
        VkInstance instance{ ugine::gfx::GraphicsService::Graphics().Instance() };

        VrgVulkanDeviceDescriptor vulkan{};
        vulkan.device = &device;
        vulkan.physicalDevice = &physDevice;
        vulkan.instance = &instance;
        Check(vrgAttachGraphicsDevice(id_, VRG_API_VULKAN, &vulkan));
    }

    void DestroyVr() override {
        vrgFreeDevice(id_);
        vrgFreeApi();
    }

    void GetProjection(ugine::core::Camera& left, ugine::core::Camera& right) override {
        VrgFov leftFov, rightFov;
        Check(vrgGetFov(id_, VRG_FOV_LEFT, &leftFov));
        Check(vrgGetFov(id_, VRG_FOV_RIGHT, &rightFov));

        left = ugine::core::Camera::Perspective(leftFov.leftTan, leftFov.rightTan, leftFov.topTan, leftFov.bottomTan, 0.1f, 100.0f);
        right = ugine::core::Camera::Perspective(rightFov.leftTan, rightFov.rightTan, rightFov.topTan, rightFov.bottomTan, 0.1f, 100.0f);
    }

    void GetHmdPose(ugine::core::Transformation& trans) override {
        VrgPose pose{};
        Check(vrgGetPoseEx(id_, VRG_POSE_HMD, &pose));

        ToTransformation(pose, trans);
    }

    void GetCameraPoses(ugine::core::Transformation& left, ugine::core::Transformation& right) override {
        VrgPose leftPose, rightPose;
        Check(vrgGetPoseEx(id_, VRG_POSE_CAMERA_LEFT, &leftPose));
        Check(vrgGetPoseEx(id_, VRG_POSE_CAMERA_RIGHT, &rightPose));

        ToTransformation(leftPose, left);
        ToTransformation(rightPose, right);
    }

    void GetResolution(uint32_t& width, uint32_t& height) override {
        int w, h;
        Check(vrgGetInt(id_, VRG_INT_RENDER_TARGET_WIDTH, &w));
        Check(vrgGetInt(id_, VRG_INT_RENDER_TARGET_HEIGHT, &h));

        width = w;
        height = h;
    }

    void Present(const ugine::gfx::TextureViewRef& left, const ugine::gfx::TextureViewRef& right) override {
        const auto& leftTexture{ ugine::gfx::TextureViews::TextureRef(left) };
        VkImage leftImage{ leftTexture->Image() };

        VrgVulkanTextureDescriptor leftDesc;
        leftDesc.width = leftTexture->Extent().width;
        leftDesc.height = leftTexture->Extent().height;
        leftDesc.format = static_cast<VkFormat>(leftTexture->Format());
        leftDesc.texture = &leftImage;

        const auto& rightTexture{ ugine::gfx::TextureViews::TextureRef(right) };
        VkImage rightImage{ rightTexture->Image() };

        VrgVulkanTextureDescriptor rightDesc;
        rightDesc.width = rightTexture->Extent().width;
        rightDesc.height = rightTexture->Extent().height;
        rightDesc.format = static_cast<VkFormat>(rightTexture->Format());
        rightDesc.texture = &rightImage;

        Check(vrgBeginFrame(id_));

        Check(vrgSubmitFrameLayer(id_, VRG_LAYER_LEFT, &leftDesc));
        Check(vrgSubmitFrameLayer(id_, VRG_LAYER_RIGHT, &rightDesc));

        Check(vrgEndFrame(id_));
    }

private:
    void ToTransformation(const VrgPose& pose, ugine::core::Transformation& trans) const {
        trans.position.x = pose.position.x;
        trans.position.y = pose.position.y;
        trans.position.z = pose.position.z;

        trans.rotation.x = static_cast<float>(pose.orientation.x);
        trans.rotation.y = static_cast<float>(pose.orientation.y);
        trans.rotation.z = static_cast<float>(pose.orientation.z);
        trans.rotation.w = static_cast<float>(pose.orientation.w);

        trans.scale.x = 1;
        trans.scale.y = 1;
        trans.scale.z = 1;
    }

    uint32_t id_{};
};

namespace {
ugine::script::NativeScripts::Register<Vrg> __vrg("VRG hmd");
}

#endif
