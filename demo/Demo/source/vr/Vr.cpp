﻿#include "Vr.h"

#include <ugine/core/Component.h>
#include <ugine/core/Scene.h>

using namespace ugine::core;
using namespace ugine::gfx;

void VrScript::OnAttach() {
    InitVr();

    leftCam_ = Scene().CreateObject("LeftVRCam");
    rightCam_ = Scene().CreateObject("LeftVRCam");

    uint32_t width, height;
    GetResolution(width, height);

    leftRenderTarget_ = RenderTarget(width, height, vk::Format::eR8G8B8A8Unorm, vk::SampleCountFlagBits::e1,
        vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eTransferSrc
            | vk::ImageUsageFlagBits::eTransferDst);
    rightRenderTarget_ = RenderTarget(width, height, vk::Format::eR8G8B8A8Unorm, vk::SampleCountFlagBits::e1,
        vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eTransferSrc
            | vk::ImageUsageFlagBits::eTransferDst);

    Camera leftCam, rightCam;
    GetProjection(leftCam, rightCam);

    leftCam_.CreateComponent<CameraComponent>(leftCam, false, vk::SampleCountFlagBits::e1, PostprocessChain{}, glm::mat4{ 1.0f }, leftRenderTarget_);
    rightCam_.CreateComponent<CameraComponent>(rightCam, false, vk::SampleCountFlagBits::e1, PostprocessChain{}, glm::mat4{ 1.0f }, rightRenderTarget_);
}

void VrScript::OnDetach() {
    GameObject::Destroy(leftCam_);
    GameObject::Destroy(rightCam_);

    DestroyVr();
}

void VrScript::Update() {
    ugine::core::Transformation hmdPose{};
    GetHmdPose(hmdPose);

    Owner().Patch<TransformationComponent>([&](auto& trans) { trans.transformation = hmdPose; });

    ugine::core::Transformation leftTrans, rightTrans;
    GetCameraPoses(leftTrans, rightTrans);

    leftCam_.Patch<TransformationComponent>([&](auto& trans) { trans.transformation = hmdPose * leftTrans; });
    rightCam_.Patch<TransformationComponent>([&](auto& trans) { trans.transformation = hmdPose * rightTrans; });
}

void VrScript::PostRender() {
    Present(leftRenderTarget_.ViewRef(), rightRenderTarget_.ViewRef());
}
