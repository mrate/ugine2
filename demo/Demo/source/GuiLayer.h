﻿#pragma once

#include "GuiContext.h"

#include <vulkan/vulkan.hpp>

#include <ugine/core/utils/SceneImport.h>
#include <ugine/gui/AssetsEditor.h>
#include <ugine/gui/FoliageEditor.h>
#include <ugine/gui/GameObjectEditor.h>
#include <ugine/gui/GameObjectTransformation.h>
#include <ugine/gui/Gui.h>
#include <ugine/gui/GuiImages.h>
#include <ugine/gui/MaterialEditor.h>
#include <ugine/gui/PostprocessEditor.h>
#include <ugine/gui/SceneEditor.h>
#include <ugine/math/Math.h>

#include <ugine/gfx/Renderer.h>
#include <ugine/gfx/tools/GizmoRenderer.h>
#include <ugine/system/Event.h>

#include <memory>

namespace ImGuiUtils {
class ProfilersWindow;
}

class Game;
class GuiViewport;

class GuiLayer {
public:
    explicit GuiLayer(Game& game);
    ~GuiLayer();

    void Start(ugine::gui::Gui* gui);
    void End();

    void Update(float deltaMS);
    void PreRender(ugine::gfx::GPUCommandList command);
    void Render(ugine::gfx::GPUCommandList command);

    bool HandleEvent(const ugine::system::Event& event);

    bool IsVisible() const {
        return isVisible_;
    }

    void SetVisible(bool visible) {
        isVisible_ = visible;
    }

    ugine::gfx::RenderFlags RenderFlags() const {
        return renderFlags_;
    }

private:
    void MainMenu();
    void ViewportMenu();

    void RenderFlagMenu(const std::string_view& name, ugine::gfx::RenderFlags flag);

    void UpdateGizmos();
    void RenderGizmos(ugine::gfx::GPUCommandList command);

    void DebugWindow();
    void PreviewTexture(const ugine::gfx::TextureRef& ref);

    void ComponentGizmo(ugine::gfx::GPUCommandList command, const glm::mat4& mvp, const ugine::core::RigidBodyComponent& c);
    void ComponentGizmo(
        ugine::gfx::GPUCommandList command, const glm::mat4& viewProj, const ugine::core::Transformation& trans, const ugine::core::LightComponent& c);
    void ComponentGizmo(ugine::gfx::GPUCommandList command, const glm::mat4& mvp, const ugine::core::CameraComponent& c);

    void ImportMeshWindow();
    void ImportMesh(const ugine::gfx::ImportMeshSettings& import);

    void ShowError(const std::string_view& error);

    void InitPrefabs();
    void PrefabList();

    void RenderProfiler();
    void HotReloadMaterials();

    Game& game_;
    std::unique_ptr<ugine::gui::GuiImages> guiImages_;
    ugine::gui::SceneEditor sceneEditor_;
    ugine::gui::GameObjectEditor goEditor_;
    ugine::gui::PostprocessEditor postprocessEditor_;
    ugine::gui::AssetsEditor assetsEditor_;
    ugine::gui::MaterialEditor materialEditor_;
    ugine::gui::FoliageEditor foliageEditor_;

    std::unique_ptr<ImGuiUtils::ProfilersWindow> profilerWin_;

    ugine::gui::Gui* gui_{};

    std::string errorMessage_;

    bool isVisible_{ true };

    bool sceneEditorVisible_{ true };
    bool sceneSettingsVisible_{ true };
    bool goEditorVisible_{ true };
    bool cameraCubeVisible_{ false };
    bool debugWindowVisible_{ true };
    bool previewWindowVisible_{};
    bool cameraPreviewVisible_{ true };
    bool gizmoVisible_{ true };
    bool rigidBodiesVisible_{ false };
    bool boundingBoxesVisible_{ false };
    bool importMeshWindowVisible_{ false };
    bool imguiDemoVisible_{ false };
    bool assetsEditorVisible_{ true };
    bool errorMessageVisible_{ false };
    bool prefabsVisible_{ true };
    bool profilerVisible_{ false };
    bool shaderHotReload_{};

    ugine::gfx::ImportMeshSettings importMesh_{};

    GuiContext context_;

    // Gizmos
    float gizmoSize_{ 0.2f };
    float iconSize_{ 1.5f };

    ugine::gfx::Pipeline gizmoPipeline_;

    ugine::gfx::Color gizmoColor_{ 1.0f, 1.0f, 1.0f, 1.0f };
    ugine::gfx::GizmoRenderer gizmoRender_;

    ugine::gfx::RenderFlags renderFlags_{ ugine::gfx::RenderFlags::All };

    glm::mat4 orthoProjection_;

    std::vector<ugine::gfx::MeshMaterialRef> prefabs_;

    struct Gizmo {
        ugine::gfx::TextureViewRef texture;
    };

    enum Gizmos {
        GizmoDirLight,
        GizmoSpotLight,
        GizmoPointLight,
        GizmoCount,
    };

    struct GizmoParams {
        glm::mat4 mvp;
        glm::vec4 color;
        glm::vec2 size;
    };

    std::array<Gizmo, GizmoCount> gizmos_;

    std::vector<std::unique_ptr<GuiViewport>> sceneViewports_;
};
