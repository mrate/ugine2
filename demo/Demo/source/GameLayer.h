﻿#pragma once

#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/Renderer.h>
#include <ugine/system/Event.h>

class Game;

class GameLayer {
public:
    explicit GameLayer(Game& game);

    void Start();
    void End();

    void Update(float deltaMS);

    void Compute();
    void Render(ugine::gfx::GPUCommandList command);

    bool HandleEvent(const ugine::system::Event& event);

    void SetRenderFlags(ugine::gfx::RenderFlags flags);

private:
    Game& game_;
    std::unique_ptr<ugine::gfx::Renderer> renderer_;

    uint32_t width_{};
    uint32_t height_{};
};
