#pragma once

#include "GuiContext.h"

#include <ugine/core/Scene.h>
#include <ugine/gfx/core/RenderTarget.h>
#include <ugine/gui/GameObjectTransformation.h>
#include <ugine/gui/GuiImages.h>

class GuiViewport {
public:
    enum class Output {
        Default,
        Depth,
        Ssao,
    };

    GuiViewport(ugine::gui::Gui& gui, ugine::core::Scene& scene, bool updateCameraViewport);

    void RenderViewport(ugine::gfx::GPUCommandList command, GuiContext& context);
    void RenderGui(GuiContext& context);

    Output ActiveOutput() const {
        return output_;
    }

    void SetActiveOutput(Output output) {
        output_ = output;
    }

    bool LocalTransformation() const {
        return localTransformation_;
    }

    void SetLocalTransformation(bool t) {
        localTransformation_ = t;
    }

    bool ManipulationVisible() const {
        return goManipulationVisible_;
    }

    void SetManipulationVisible(bool v) {
        goManipulationVisible_ = v;
    }

private:
    void RenderOutputDefault(
        ugine::gfx::GPUCommandList command, GuiContext& context, const ugine::core::CameraComponent& camera, const ugine::gfx::TextureView& view);
    void RenderOutputSingleColor(
        ugine::gfx::GPUCommandList command, GuiContext& context, const ugine::core::CameraComponent& camera, const ugine::gfx::TextureView& view);
    void RenderOutputDepth(ugine::gfx::GPUCommandList command, GuiContext& context, const ugine::core::CameraComponent& camera);

    void UpdateCameraRenderTarget(uint32_t width, uint32_t height);

    void HandleInput(GuiContext& context, const Rect& sceneRect);
    void HandleDragAndDrop(GuiContext& context);

    ugine::gui::Gui& gui_;
    ugine::core::Scene& scene_;

    Output output_{ Output::Default };
    ugine::gui::GameObjectTransformation::Operation transformOperation_{ ugine::gui::GameObjectTransformation::Operation::Translate };

    ugine::gfx::RenderTarget viewportRT_;
    std::unique_ptr<ugine::gui::TextureList> viewport_;

    bool goManipulationVisible_{ true };
    bool localTransformation_{};
    bool updateCameraViewport_{};
};