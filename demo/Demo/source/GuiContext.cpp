#include "GuiContext.h"

#include <ugine/core/Scene.h>
#include <ugine/gfx/MaterialService.h>
#include <ugine/gui/SceneEditor.h>
#include <ugine/math/Raycast.h>

using namespace ugine;
using namespace ugine::gfx;
using namespace ugine::gui;
using namespace ugine::core;

void ApplyMaterial(ugine::core::GameObject& go, const MaterialInstanceRef& mat) {
    if (go.Has<MeshComponent>()) {
        auto& meshComp{ go.Component<MeshComponent>() };
        for (auto& submesh : meshComp.meshMaterial->submeshes) {
            submesh.material = mat;
        }
    }
}

void ApplyFoliage(ugine::core::GameObject& go, const ugine::gfx::FoliageRef& foliage) {
    go.GetOrCreateComponent<FoliageComponent>();
    go.Patch<FoliageComponent>([&](auto& f) {
        if (f.layerCount < FoliageComponent::MAX_LAYERS) {
            f.layers[f.layerCount++].foliage = foliage;
        }
    });
}

void ApplyMesh(ugine::core::GameObject& go, const ugine::gfx::MeshRef& mesh) {
    if (go.Has<MeshComponent>()) {
        go.Patch<MeshComponent>([&](auto& m) {
            auto mat{ m.meshMaterial->submeshes.empty() ? MaterialService::InstantiateDefault() : m.meshMaterial->submeshes[0].material };
            m.meshMaterial = MESH_MATERIAL_ADD(mesh, mat);
        });
    } else {
        go.CreateComponent<MeshComponent>();
        go.Patch<MeshComponent>([&](auto& m) { m.meshMaterial = MESH_MATERIAL_ADD(mesh, MaterialService::InstantiateDefault()); });
    }
}

void OutlineSelected(GuiContext& context, bool outline) {
    for (auto&& [ent, tag, guiFlags] : context.scene->Registry().view<Tag, GuiFlags>().each()) {
        if (guiFlags.flags & GuiFlags::Selected) {
            if (outline) {
                tag.flags |= Tag::Flags::Outline;
            } else {
                tag.flags &= ~Tag::Flags::Outline;
            }

            auto go{ context.scene->Get(ent) };
            for (auto child{ go.FirstChild() }; child.IsValid(); child = child.NextSibling()) {
                if (outline) {
                    child.Component<Tag>().flags |= Tag::Flags::Outline;
                } else {
                    child.Component<Tag>().flags &= ~Tag::Flags::Outline;
                }
            }
        }
    }
}

void SelectGO(GuiContext& context, ugine::core::GameObject go) {
    DeselectGO(context);
    if (go) {
        go.GetOrCreateComponent<GuiFlags>().flags = GuiFlags::Selected;

        OutlineSelected(context, true);
    }
}

void DeselectGO(GuiContext& context) {
    for (auto ent : context.scene->Registry().view<GuiFlags>()) {
        OutlineSelected(context, false);

        auto go{ context.scene->Get(ent) };
        go.RemoveComponent<GuiFlags>();
    }
}

ugine::core::GameObject SelectedGO(GuiContext& context) {
    for (auto ent : context.scene->Registry().view<GuiFlags>()) {
        return context.scene->Get(ent);
    }
    return {};
}

void SetHoverGO(GuiContext& context, const ugine::core::GameObject& go) {
    if (context.hoverGO) {
        context.hoverGO.Component<Tag>().flags &= ~Tag::Outline;
    }

    context.hoverGO = go;

    if (context.hoverGO) {
        context.hoverGO.Component<Tag>().flags |= Tag::Outline;
    }
}

void SetHoverCapture(GuiContext& context, bool capture) {
    if (capture != context.captureHover) {
        context.captureHover = capture;
        if (capture) {
            OutlineSelected(context, false);
        } else {
            SetHoverGO(context, {});
            OutlineSelected(context, true);
        }
    }
}

ugine::core::GameObject GameObjectAt(GuiContext& context, const Rect& sceneRect, float x, float y) {
    auto camera{ context.scene->Camera() };
    if (camera) {
        const auto& cameraC{ camera.Component<CameraComponent>() };
        const auto& t{ camera.Component<TransformationComponent>().transformation };

        float normX{ (x - sceneRect.x) / sceneRect.width };
        float normY{ (y - sceneRect.y) / sceneRect.height };

        auto ray{ ugine::math::RayFromCamera(cameraC.camera.ProjectionMatrix(), cameraC.camera.ViewMatrix(), normX, normY) };
        ray.origin = t.position;

        return context.scene->RayCast(ray);
    }
    return {};
}
