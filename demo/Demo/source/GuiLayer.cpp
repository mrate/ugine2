﻿#include "GuiLayer.h"
#include "GuiViewport.h"

#include "Game.h"

#include <ugine/core/Camera.h>
#include <ugine/core/ConfigService.h>
#include <ugine/gfx/FrameStats.h>
#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/core/GpuProfiler.h>
#include <ugine/gfx/core/Texture.h>
#include <ugine/gfx/data/FramebufferCache.h>
#include <ugine/gfx/data/PipelineCache.h>
#include <ugine/gfx/data/RenderPassCache.h>
#include <ugine/gfx/data/ShaderBindings.h>
#include <ugine/gfx/data/Shapes.h>
#include <ugine/gfx/import/MaterialImporter.h>
#include <ugine/gfx/import/MeshLoader.h>
#include <ugine/gfx/postprocess/ToneMapping.h>
#include <ugine/gfx/rendering/Terrain.h>
#include <ugine/gui/GuiImages.h>
#include <ugine/gui/Utils.h>
#include <ugine/gui/ViewCube.h>
#include <ugine/input/InputService.h>
#include <ugine/math/Raycast.h>
#include <ugine/utils/Log.h>
#include <ugine/utils/Profile.h>

#include <imgui.h>

#include <ProfilerTask.h>

#include <ImGuiProfilerRenderer.h>

using namespace ugine::core;
using namespace ugine::gfx;
using namespace ugine::gui;
using namespace ugine::input;
using namespace ugine::math;
using namespace ugine::system;
using namespace ugine::utils;

namespace {

static const char* IMPORT_MESH_POPUP{ ICON_FA_CUBE " Import mesh##ImportMeshPopup" };
static const auto ONE_MB{ 1024.0f * 1024.0f };

struct GizmoComponent {
    glm::vec4 color;
    int type{};
    bool visible{};
};

} // namespace

GuiLayer::GuiLayer(Game& game)
    : game_(game) {
    sceneEditor_.OnClicked.connect<&SelectGO>(context_);
}

GuiLayer::~GuiLayer() {}

void GuiLayer::Start(ugine::gui::Gui* gui) {
    gui_ = gui;

    guiImages_ = std::make_unique<GuiImages>(*gui_);
    guiImages_->SetDefaultSize(16.0f, 16.0f);
    guiImages_->SetImage(GuiImages::Empty, TEXTURE_ADD(Texture::FromFile(L"assets/gui/view.png")));
    guiImages_->SetImage(GuiImages::Add, TEXTURE_ADD(Texture::FromFile(L"assets/gui/add.png")));
    guiImages_->SetImage(GuiImages::Remove, TEXTURE_ADD(Texture::FromFile(L"assets/gui/remove.png")));
    guiImages_->SetImage(GuiImages::Camera, TEXTURE_ADD(Texture::FromFile(L"assets/gui/camera.png")));
    guiImages_->SetImage(GuiImages::View, TEXTURE_ADD(Texture::FromFile(L"assets/gui/view.png")));
    guiImages_->SetImage(GuiImages::Hidden, TEXTURE_ADD(Texture::FromFile(L"assets/gui/view_dis.png")));
    guiImages_->SetImage(GuiImages::Open, TEXTURE_ADD(Texture::FromFile(L"assets/gui/open.png")));
    guiImages_->SetImage(GuiImages::Delete, TEXTURE_ADD(Texture::FromFile(L"assets/gui/delete.png")));
    guiImages_->SetImage(GuiImages::Copy, TEXTURE_ADD(Texture::FromFile(L"assets/gui/copy.png")));
    guiImages_->SetImage(GuiImages::ComponentAnimator, TEXTURE_ADD(Texture::FromFile(L"assets/gui/animator.png")));
    guiImages_->SetImage(GuiImages::ComponentCamera, TEXTURE_ADD(Texture::FromFile(L"assets/gui/camera.png")));
    guiImages_->SetImage(GuiImages::ComponentLight, TEXTURE_ADD(Texture::FromFile(L"assets/gui/light.png")));
    guiImages_->SetImage(GuiImages::ComponentLogic, TEXTURE_ADD(Texture::FromFile(L"assets/gui/logic.png")));
    guiImages_->SetImage(GuiImages::ComponentMaterial, TEXTURE_ADD(Texture::FromFile(L"assets/gui/material.png")));
    guiImages_->SetImage(GuiImages::ComponentSkinnedMesh, TEXTURE_ADD(Texture::FromFile(L"assets/gui/skinned_mesh.png")));
    guiImages_->SetImage(GuiImages::ComponentSkybox, TEXTURE_ADD(Texture::FromFile(L"assets/gui/skybox.png")));
    guiImages_->SetImage(GuiImages::ComponentStaticMesh, TEXTURE_ADD(Texture::FromFile(L"assets/gui/mesh.png")));
    guiImages_->SetImage(GuiImages::MaterialIcon, TEXTURE_ADD(Texture::FromFile(L"assets/gui/materialIcon.png")));
    guiImages_->SetImage(GuiImages::FxMaterialIcon, TEXTURE_ADD(Texture::FromFile(L"assets/gui/fxMaterialIcon.png")));
    guiImages_->SetImage(GuiImages::FoliageIcon, TEXTURE_ADD(Texture::FromFile(L"assets/gui/foliageIcon.png")));
    guiImages_->SetImage(GuiImages::MeshIcon, TEXTURE_ADD(Texture::FromFile(L"assets/gui/foliageIcon.png")));
    guiImages_->SetImage(GuiImages::FileIcon, TEXTURE_ADD(Texture::FromFile(L"assets/gui/fileIcon.png")));

    sceneEditor_.SetGui(gui_);
    sceneEditor_.SetImages(guiImages_.get());

    goEditor_.SetGui(gui_);
    goEditor_.SetImages(guiImages_.get());

    assetsEditor_.SetGui(gui_);
    assetsEditor_.SetImages(guiImages_.get());

    materialEditor_.SetGui(gui_);
    materialEditor_.SetImages(guiImages_.get());

    foliageEditor_.SetGui(gui_);
    foliageEditor_.SetImages(guiImages_.get());

    // Target viewport pass and texture list.
    auto viewportPass{ RenderPassCache::Transparent(GuiContext::FORMAT).RenderPass() };

    // Gizmos.
    orthoProjection_ = glm::ortho(-1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f);

    auto mat{ MaterialService::Register(L"assets/materials/gui/gizmo.mat") };
    gizmoPipeline_ = mat->DefaultPass().CreatePipeline(viewportPass);
    gizmoRender_ = GizmoRenderer(viewportPass);

    auto dirLightTex{ TEXTURE_ADD(Texture::FromFile(L"assets/gui/gizmo/dir_light.png")) };
    auto spotLightTex{ TEXTURE_ADD(Texture::FromFile(L"assets/gui/gizmo/spot_light.png")) };
    auto pointLightTex{ TEXTURE_ADD(Texture::FromFile(L"assets/gui/gizmo/point_light.png")) };

    importMesh_.scale = 1.0f;
    importMesh_.lodLevels = ConfigService::DefaultLodLevels();
    importMesh_.material = MaterialService::DefaultMaterial(false);
    importMesh_.singleMesh = false;

    for (int i = 0; i < GizmoCount; ++i) {
        switch (i) {
        case GizmoDirLight:
            gizmos_[i].texture = TEXTURE_VIEW_ADD(dirLightTex, TextureView::CreateSampled(*dirLightTex));
            break;
        case GizmoSpotLight:
            gizmos_[i].texture = TEXTURE_VIEW_ADD(spotLightTex, TextureView::CreateSampled(*spotLightTex));
            break;
        case GizmoPointLight:
            gizmos_[i].texture = TEXTURE_VIEW_ADD(pointLightTex, TextureView::CreateSampled(*pointLightTex));
            break;
        }
    }

    auto outlineMat{ MaterialService::Register(L"assets/materials/fullscreenOutline.mat") };
    context_.outlinePipeline = outlineMat->DefaultPass().CreatePipeline(viewportPass);

    auto depthMat{ MaterialService::Register(L"assets/materials/fullscreenDepth.mat") };
    context_.depthPipeline = depthMat->DefaultPass().CreatePipeline(viewportPass);

    auto singleMat{ MaterialService::Register(L"assets/materials/fullscreenR.mat") };
    context_.singleColorPipeline = singleMat->DefaultPass().CreatePipeline(viewportPass);

    InitPrefabs();

    // Init context.
    context_.scene = &game_.Scene();

    profilerWin_ = std::make_unique<ImGuiUtils::ProfilersWindow>();

    sceneViewports_.push_back(std::make_unique<GuiViewport>(*gui_, game_.Scene(), true));
    //sceneViewports_.push_back(std::make_unique<GuiViewport>(*gui_, game_.Scene(), false));
}

void GuiLayer::End() {
    sceneViewports_.clear();

    prefabs_.clear();

    importMesh_ = {};

    guiImages_ = nullptr;
    gizmoPipeline_ = {};
    gizmoRender_ = {};

    context_.outlinePipeline = {};
    context_.depthPipeline = {};
    context_.singleColorPipeline = {};

    for (auto& gizmo : gizmos_) {
        gizmo.texture = {};
    }
}

void GuiLayer::Update(float deltaMS) {
    PROFILE_EVENT("Gui update");
    auto updateStartTime{ std::chrono::high_resolution_clock::now() };

    context_.selectionColor = glm::vec4{ 1.0f, 0.5f + 0.5f * cos(static_cast<float>(CoreService::Time()) / 1000.0f), 0.0f, 1.0f };

    // TODO: Move somewhere else...
    if (shaderHotReload_) {
        HotReloadMaterials();
    }

    if (!IsVisible()) {
        return;
    }

    auto selectedGO{ SelectedGO(context_) };
    auto cameraGO{ game_.Scene().Camera() };

    gui_->NewFrame();

    MainMenu();
    //Toolbar();

    ImGui::DockSpaceOverViewport(nullptr, ImGuiDockNodeFlags_PassthruCentralNode);

    int i{};
    for (auto& v : sceneViewports_) {
        ImGui::Begin(fmt::format("Viewport #{}", ++i).c_str(), nullptr, ImGuiWindowFlags_NoScrollbar);
        v->RenderGui(context_);
        ImGui::End();
    }

    if (profilerVisible_) {
        RenderProfiler();
    }

    if (prefabsVisible_) {
        PrefabList();
    }

    if (sceneSettingsVisible_) {
        ImGui::Begin(ICON_FA_WRENCH " Scene settings");
        sceneEditor_.PopulateSceneSettings(game_.Scene());
        ImGui::End();
    }

    if (sceneEditorVisible_) {
        ImGui::Begin(ICON_FA_LIST " Scene hierarchy##SceneEditor");

        int id{};
        {
            scope::Id _id{ id };
            if (ImGui::Button(ICON_FA_PLUS)) {
                auto go{ game_.Scene().CreateObject("Empty") };
                if (selectedGO) {
                    selectedGO.AddChild(go);
                }
            }
        }

        if (selectedGO.IsValid()) {
            ImGui::SameLine();

            {
                scope::Id _id{ id };
                if (ImGui::Button(ICON_FA_MINUS)) {
                    GameObject::Destroy(selectedGO);
                    selectedGO = {};
                    // We might have just deleted a camera.
                    cameraGO = game_.Scene().Camera();
                }
            }
            ImGui::SameLine();

            {
                scope::Id _id{ id };
                if (ImGui::Button(ICON_FA_COPY)) {
                    auto prev{ selectedGO };
                    selectedGO = selectedGO.Clone();
                    selectedGO.SetName(prev.Name() + " Copy");
                }
            }
        }

        ImGui::Separator();

        {
            scope::Color style(ImGuiCol_FrameBg, ImVec4{ 0.8f, 0.8f, 0.8f, 1.0f });
            sceneEditor_.PopulateGameObjects(game_.Scene());
        }
        ImGui::End();
    }

    if (selectedGO.IsValid() && goEditorVisible_) {
        ImGui::Begin("Object inspector");
        goEditor_.Populate(selectedGO);
        ImGui::End();
    }

    if (cameraCubeVisible_ && cameraGO.IsValid()) {
        auto viewMatrix{ cameraGO.GlobalTransformation().Matrix() };
        if (ViewCube::Render(viewMatrix, 8.0f, 0.0f, 0.0f, 128.0f, 128.0f)) {
            //camera.Patch<CameraComponent>([&](auto& c) { c.camera = newCamera; });
            //camera.Patch<TransformationComponent>([&](auto& t) { t.transformation = Transformation{ viewMatrix }; });
        }
    }

    if (debugWindowVisible_) {
        DebugWindow();
    }

    if (previewWindowVisible_) {
        if (ImGui::Begin("Preview", &previewWindowVisible_)) {
            ImVec2 size = ImGui::GetWindowContentRegionMax();
            ImGui::Image(guiImages_->Image(GuiImages::Preview), size);
        }
        ImGui::End();
    }

    if (assetsEditorVisible_) {
        ImGui::Begin("Assets");
        assetsEditor_.PopulateAssets();
        ImGui::End();

        if (assetsEditor_.SelectedInstance() != MaterialInstances::Invalid) {
            auto materialEditorVisible{ true };

            if (ImGui::Begin("Material instance", &materialEditorVisible)) {
                materialEditor_.PopulateMaterial(MaterialInstances::Get(assetsEditor_.SelectedInstance()));
                ImGui::End();
            }

            if (!materialEditorVisible) {
                assetsEditor_.SetSelectedInstance(MaterialInstances::Invalid);
            }
        } else if (assetsEditor_.SelectedFoliage() != Foliages::Invalid) {
            auto foliageEditorVisible{ true };

            if (ImGui::Begin("Foliage", &foliageEditorVisible)) {
                foliageEditor_.Populate(Foliages::Get(assetsEditor_.SelectedFoliage()));
                ImGui::End();
            }

            if (!foliageEditorVisible) {
                assetsEditor_.SetSelectedFoliage(Foliages::Invalid);
            }
        }
    }

    if (imguiDemoVisible_) {
        ImGui::ShowDemoWindow(&imguiDemoVisible_);
    }

    if (selectedGO && selectedGO != cameraGO && selectedGO.Has<CameraComponent>() && cameraPreviewVisible_) {
        auto& cameraC{ selectedGO.Component<CameraComponent>() };

        if (cameraC.renderTarget) {
            // TODO:
            guiImages_->SetImage(GuiImages::Type::Preview2, cameraC.renderTarget.ViewRef(), vk::ImageLayout::eShaderReadOnlyOptimal, Gui::FlipY);

            if (ImGui::Begin("Camera preview", &cameraPreviewVisible_)) {
                ImVec2 size = ImGui::GetWindowContentRegionMax();
                ImGui::Image(guiImages_->Image(GuiImages::Type::Preview2), size);
                ImGui::End();
            }
        }
    }

    if (errorMessageVisible_) {
        // TODO: Style.
        if (ImGui::Begin("Error", &errorMessageVisible_)) {
            ImGui::Text(errorMessage_.c_str());
            ImGui::End();
        }
    }

    gui_->EndFrame();

    FrameStats::Instance().cpuGuiUpdateTimeMS
        = static_cast<float>(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - updateStartTime).count())
        / 1000.0f;
}

void GuiLayer::PreRender(ugine::gfx::GPUCommandList command) {
    PROFILE_EVENT("Gui::PreRender");
    GPUDebugLabel label(command, "Gui pre-pass");

    UpdateGizmos();

    if (auto cameraGO = game_.Scene().Camera()) {
        const auto& cameraComp{ cameraGO.Component<CameraComponent>() };
        if (cameraComp.depthBuffer) {
            //TODO:
            auto barrier{ GPUBarrier::Image(*TextureViews::TextureRef(cameraComp.depthBuffer.StencilRef()), //
                vk::AccessFlagBits::eDepthStencilAttachmentWrite,
                vk::AccessFlagBits::eShaderRead, //
                vk::ImageLayout::eDepthStencilReadOnlyOptimal, //
                vk::ImageLayout::eDepthStencilReadOnlyOptimal, //
                vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil) };
            GraphicsService::Device().Barrier(command, barrier);
        }
    }

    for (auto& v : sceneViewports_) {
        v->RenderViewport(command, context_);
    }
}

void GuiLayer::Render(ugine::gfx::GPUCommandList command) {
    PROFILE_EVENT("Gui::Render");
    GPUDebugLabel label(command, "Gui pass");

    if (IsVisible()) {
        gui_->Render(command);
    }
}

bool GuiLayer::HandleEvent(const Event& event) {
    if (event.type == Event::Type::KeyDown) {
        switch (event.key) {
        case 192: // '`'
            SetVisible(!IsVisible());
            return true;
        case 'T':
            /*
            switch (transformOperation_) {
            case GameObjectTransformation::Operation::Translate:
                transformOperation_ = GameObjectTransformation::Operation::Rotate;
                break;
            case GameObjectTransformation::Operation::Rotate:
                transformOperation_ = GameObjectTransformation::Operation::Scale;
                break;
            case GameObjectTransformation::Operation::Scale:
                transformOperation_ = GameObjectTransformation::Operation::Translate;
                break;
            }
            */

            return true;
        case 46: // Delete.
            if (auto selected = SelectedGO(context_)) {
                GameObject::Destroy(selected);
                DeselectGO(context_);
            }
            break;
        }
    }

    return false;
}

void GuiLayer::MainMenu() {
    auto importMesh{ false };

    if (ImGui::BeginMainMenuBar()) {
        if (ImGui::BeginMenu("File")) {
            if (ImGui::BeginMenu(ICON_FA_PLUS " Add")) {
                if (ImGui::Selectable("Foliage")) {
                    assetsEditor_.SetSelectedFoliage(FoliageService::Add("New foliage").id());
                }

                ImGui::EndMenu();
            }

            ImGui::Separator();

            if (ImGui::Selectable(ICON_FA_FOLDER_OPEN " Import mesh...")) {
                auto fileName{ gui_->OpenFileDialog(MESH_FILES_FILTER) };
                if (fileName) {
                    importMesh_.file = *fileName;
                    importMesh = true;
                }
            }

            if (ImGui::Selectable(ICON_FA_FOLDER_OPEN " Import material instance...")) {
                auto fileName{ gui_->OpenFileDialog(MATERIAL_FILES_FILTER) };
                if (fileName) {
                    try {
                        MaterialInstanceRef newInst;
                        if (fileName->extension() == ".imat") {
                            newInst = MaterialService::InstanceFromFile(*fileName);
                        } else {
                            newInst = MaterialService::InstantiateByPath(*fileName);
                        }
                        newInst->SetName(fileName->filename().string());
                    } catch (const std::exception& ex) {
                        ShowError(ex.what());
                    }
                }
            }

            if (ImGui::Selectable(ICON_FA_FOLDER_OPEN " Import foliage...")) {
                auto fileName{ gui_->OpenFileDialog(FOLIAGE_FILES_FILTER) };
                if (fileName) {
                    FoliageService::LoadFromFile(*fileName);
                }
            }

            ImGui::Separator();

            ImGui::Checkbox("Materials auto reloading", &shaderHotReload_);

            if (ImGui::Selectable(ICON_FA_RECYCLE " Reload materials")) {
                HotReloadMaterials();
            }

            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Debug")) {
            ImGui::Checkbox("Debug view", &debugWindowVisible_);

            bool physicsActive{ CoreService::PhysicsActive() };
            if (ImGui::Checkbox("Physics active", &physicsActive)) {
                CoreService::SetPhysicsActive(physicsActive);
            }

            if (ImGui::BeginMenu("Render flags")) {
                RenderFlagMenu("Opaque", RenderFlags::Opaque);
                RenderFlagMenu("Transparent", RenderFlags::Transparent);
                RenderFlagMenu("Skybox", RenderFlags::SkyBox);
                RenderFlagMenu("Lens flares", RenderFlags::LensFlare);
                RenderFlagMenu("Volumetric lights", RenderFlags::VolumetricLights);
                RenderFlagMenu("Foliage", RenderFlags::Foliage);
                RenderFlagMenu("Particles", RenderFlags::Particles);
                RenderFlagMenu("Light shafts", RenderFlags::LightShafts);
                RenderFlagMenu("Ambient occlusion", RenderFlags::AmbientOcclusion);

                ImGui::Separator();

                if (ImGui::Selectable("All")) {
                    renderFlags_ = RenderFlags::All;
                }

                if (ImGui::Selectable("None")) {
                    renderFlags_ = RenderFlags::None;
                }

                ImGui::EndMenu();
            }

            ImGui::Checkbox("ImGui demo", &imguiDemoVisible_);

            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Viewport")) {
            ViewportMenu();
            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Gizmo")) {
            ImGui::Checkbox("Visible", &gizmoVisible_);
            ImGui::SliderFloat("Size", &gizmoSize_, 0.05f, 0.5f, "%0.2f%");
            ColorGui("Color", gizmoColor_);
            if (ImGui::SliderFloat("Icon Size", &iconSize_, 0.05f, 2.0f, "%0.2f%")) {
                assetsEditor_.SetIconSize(64.0f * iconSize_, 64.0f * iconSize_);
            }
            ImGui::Checkbox("Show rigid bodies", &rigidBodiesVisible_);
            ImGui::Checkbox("Show bounding boxes", &boundingBoxesVisible_);

            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Window")) {
            ImGui::Checkbox("Scene hierarchy", &sceneEditorVisible_);
            ImGui::Checkbox("Scene settings", &sceneSettingsVisible_);
            ImGui::Checkbox("Assets", &assetsEditorVisible_);
            ImGui::Checkbox("Object inspector", &goEditorVisible_);
            ImGui::Checkbox("Camera cube", &cameraCubeVisible_);
            ImGui::Checkbox("Camera preview", &cameraPreviewVisible_);
            ImGui::Checkbox("Preview window", &previewWindowVisible_);
            ImGui::Checkbox("Prefabs", &prefabsVisible_);
            ImGui::Checkbox("Profiler", &profilerVisible_);
            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Theme")) {
            for (const auto& theme : gui_->Themes()) {
                bool selected{ theme == gui_->SelectedTheme() };
                if (ImGui::Checkbox(theme.c_str(), &selected)) {
                    gui_->SetTheme(theme);
                }
            }
            ImGui::EndMenu();
        }

        ImGui::EndMainMenuBar();
    }

    if (importMesh) {
        ImGui::OpenPopup(IMPORT_MESH_POPUP);
    }

    ImportMeshWindow();
}

void GuiLayer::ViewportMenu() {
    int i{};
    for (auto& v : sceneViewports_) {
        if (ImGui::BeginMenu(fmt::format("Viewport #{}", ++i).c_str())) {

            if (ImGui::BeginMenu("Output")) {
                if (ImGui::RadioButton("Default", v->ActiveOutput() == GuiViewport::Output::Default)) {
                    v->SetActiveOutput(GuiViewport::Output::Default);
                }

                if (ImGui::RadioButton("Depth", v->ActiveOutput() == GuiViewport::Output::Depth)) {
                    v->SetActiveOutput(GuiViewport::Output::Depth);
                }

                if (ImGui::RadioButton("SSAO", v->ActiveOutput() == GuiViewport::Output::Ssao)) {
                    v->SetActiveOutput(GuiViewport::Output::Ssao);
                }

                ImGui::EndMenu();
            }
            bool localTransform{ v->LocalTransformation() };
            if (ImGui::Checkbox("Local transformation", &localTransform)) {
                v->SetLocalTransformation(localTransform);
            }

            ImGui::EndMenu();
        }
    }
}

void GuiLayer::RenderFlagMenu(const std::string_view& name, ugine::gfx::RenderFlags flag) {
    bool selected{ (renderFlags_ & flag) != 0 };
    if (ImGui::Checkbox(name.data(), &selected)) {
        if (selected) {
            renderFlags_ |= flag;
        } else {
            renderFlags_ &= ~flag;
        }
    }
}

void GuiLayer::UpdateGizmos() {
    if (!gizmoVisible_) {
        return;
    }

    auto cameraGO{ game_.Scene().Camera() };
    if (!cameraGO) {
        return;
    }

    const auto& camera{ cameraGO.Component<CameraComponent>().camera };
    for (auto [ent, light, transC] : game_.Scene().Registry().view<LightComponent, TransformationComponent>().each()) {
        auto go{ game_.Scene().Get(ent) };
        auto& trans{ transC.transformation };
        auto& gizmo{ go.GetOrCreateComponent<GizmoComponent>() };

        switch (light.light.type) {
        case Light::Type::Directional:
            gizmo.type = GizmoDirLight;
            break;
        case Light::Type::Point:
            gizmo.type = GizmoPointLight;
            break;
        case Light::Type::Spot:
            gizmo.type = GizmoSpotLight;
            break;
        }

        gizmo.visible = true;
    }
}

void GuiLayer::RenderGizmos(ugine::gfx::GPUCommandList command) {
    auto& device{ GraphicsService::Device() };

    auto selectedGO{ SelectedGO(context_) };
    auto cameraGO{ game_.Scene().Camera() };

    glm::mat4 viewProj{ cameraGO ? cameraGO.Component<CameraComponent>().camera.ViewProjectionMatrix() : glm::mat4{ 1.0f } };
    glm::mat4 proj{ cameraGO ? cameraGO.Component<CameraComponent>().camera.ProjectionMatrix() : glm::mat4{ 1.0f } };
    glm::mat4 view{ cameraGO ? cameraGO.Component<CameraComponent>().camera.ViewMatrix() : glm::mat4{ 1.0f } };

    device.ResetDescriptors(command);

    device.BindPipeline(command, &gizmoPipeline_);

    { // Render light gizmos.
        auto gizmos{ game_.Scene().Registry().view<GizmoComponent>().size() };

        //game_.Scene().Registry().sort<GizmoComponent>([](const auto& g1, const auto& g2) { return g1.type < g2.type; });

        for (auto [ent, gizmo] : game_.Scene().Registry().view<GizmoComponent>().each()) {
            if (gizmo.visible) {
                auto trans{ game_.Scene().Get(ent).GlobalTransformation() };
                auto vm{ view * trans.Matrix() };
                vm[0] = glm::vec4{ 1, 0, 0, 1 };
                vm[1] = glm::vec4{ 0, 1, 0, 1 };
                vm[2] = glm::vec4{ 0, 0, 1, 1 };
                //vm[0] = glm::vec4{ 1, 0, 0, vm[0][3] };
                //vm[1] = glm::vec4{ 0, 1, 0, vm[1][3] };
                //vm[2] = glm::vec4{ 0, 0, 1, vm[2][3] };

                GizmoParams params;
                params.mvp = proj * vm;
                params.color = selectedGO && selectedGO.Entity() == ent ? context_.selectionColor : gizmoColor_.rgba;
                params.size = glm::vec2{ gizmoSize_ };

                device.BindImageSampler(command, 0, 0, *gizmos_[gizmo.type].texture);
                device.PushConstants(command, GPUPushConstant::Data(vk::ShaderStageFlagBits::eVertex, params));

                device.Draw(command, 6, 1, 0, 0);
            }
        }

        device.ResetDescriptors(command);

        for (auto [ent, gizmo] : game_.Scene().Registry().view<GizmoComponent>().each()) {
            if (gizmo.visible) {
                auto trans{ game_.Scene().Get(ent).GlobalTransformation() };
                auto groundPosition{ trans.position };
                groundPosition.y = 0;
                gizmoRender_.RenderLine(command, gizmoColor_.rgba, viewProj, trans.position, groundPosition);
            }
        }
    }

    device.ResetDescriptors(command);

    if (selectedGO && cameraGO && selectedGO.Has<TransformationComponent>()) {
        const auto& trans{ selectedGO.Component<TransformationComponent>().transformation };

        if (selectedGO.Has<CameraComponent>()) {
            const auto& cam{ selectedGO.Component<CameraComponent>() };
            if (!cam.isMain) {
                ComponentGizmo(command, viewProj * trans.Matrix(), cam);
            }
        }

        if (selectedGO.Has<LightComponent>()) {
            ComponentGizmo(command, viewProj, trans, selectedGO.Component<LightComponent>());
        }
    }

    if (cameraGO && rigidBodiesVisible_) {
        for (auto [ent, trans, rigidBody] : game_.Scene().Registry().view<TransformationComponent, RigidBodyComponent>().each()) {
            auto& debug{ cameraGO.Component<CameraComponent>().camera }; // TODO:

            Transformation newTrans{ trans.transformation.position + trans.transformation.rotation * rigidBody.offset, trans.transformation.rotation,
                rigidBody.scale };
            ComponentGizmo(command, viewProj * newTrans.Matrix(), rigidBody);
        }
    }

    if (cameraGO && boundingBoxesVisible_) {
        for (auto [ent, mesh] : game_.Scene().Registry().view<MeshComponent>().each()) {
            if (mesh.meshMaterial) {
                auto trans{ game_.Scene().Get(ent).GlobalTransformation().Matrix() };
                gizmoRender_.RenderAABB(command, mesh.meshMaterial->mesh->Aabb(), glm::vec4{ 0, 1, 0, 1 }, viewProj * trans);
                gizmoRender_.RenderAABB(command, mesh.aabb, glm::vec4{ 0, 0, 1, 1 }, viewProj);
            }
        }
        for (auto [ent, mesh] : game_.Scene().Registry().view<SkinnedMeshComponent>().each()) {
            auto trans{ game_.Scene().Get(ent).GlobalTransformation().Matrix() };
            gizmoRender_.RenderAABB(command, mesh.mesh.Aabb(), glm::vec4{ 0, 1, 0, 1 }, viewProj * trans);
            gizmoRender_.RenderAABB(command, mesh.aabb, glm::vec4{ 0, 0, 1, 1 }, viewProj);
        }
        for (auto [ent, terrain, t] : game_.Scene().Registry().view<TerrainComponent, Terrain>().each()) {
            auto trans{ game_.Scene().Get(ent).GlobalTransformation().Matrix() };
            gizmoRender_.RenderAABB(command, t.Aabb(), glm::vec4{ 0, 1, 0, 1 }, viewProj * trans);
            gizmoRender_.RenderAABB(command, terrain.aabb, glm::vec4{ 0, 0, 1, 1 }, viewProj);
        }
        for (auto [ent, particles, p] : game_.Scene().Registry().view<ParticleComponent, Particles>().each()) {
            auto trans{ game_.Scene().Get(ent).GlobalTransformation().Matrix() };
            gizmoRender_.RenderAABB(command, p.Aabb(), glm::vec4{ 0, 1, 0, 1 }, viewProj * trans);
            gizmoRender_.RenderAABB(command, particles.aabb, glm::vec4{ 0, 0, 1, 1 }, viewProj);
        }
    }
}

void GuiLayer::DebugWindow() {
    static const ImVec4 TITLE_COLOR{ 0.4f, 0.6f, 0.8f, 1.0f };

    auto& frameStats{ ugine::gfx::FrameStats::Instance() };

    ImGui::Begin("Debug");

    {
        scope::Color color{ ImGuiCol_Text, TITLE_COLOR };
        ImGui::Text(ICON_FA_CLOCK " CPU profiling");
    }

    ImGui::Text("Compute CPU time (ms): %f", frameStats.cpuComputeTimeMS);
    ImGui::Text("Render CPU time (ms): %f", frameStats.cpuRenderTimeMS);
    ImGui::Text("GUI CPU time (ms): %f", frameStats.cpuGuiUpdateTimeMS);
    ImGui::Text("Render passes: %u", frameStats.passes.load());
    ImGui::Text("Draw calls (scene only): %u", frameStats.drawCallsScene.load());
    ImGui::Text("Draw calls: %u", frameStats.drawCallsIssued.load());
    ImGui::Text("Indirect draw calls: %u", frameStats.drawCallsIndirect.load());
    ImGui::Text("Compute dispatches: %u", frameStats.compDispatches.load());
    ImGui::Text("Compute dispatches indirect: %u", frameStats.compDispatchesIndirect.load());
    //ImGui::Text("Meshes: %u", frameStats.meshes.load());
    //ImGui::Text("Skinned: %u", frameStats.skinnedMeshes.load());
    //ImGui::Text("Terrains: %u", frameStats.terrains.load());
    //ImGui::Text("Particle systems: %u", frameStats.particles.load());

    ImGui::Separator();

    {
        scope::Color color{ ImGuiCol_Text, TITLE_COLOR };
        ImGui::Text(ICON_FA_CLOCK " GPU profiling");
    }

    const auto& results{ GpuProfiler::Instance().Results() };
    for (uint32_t i = 0; i < GpuProfiler::Instance().ResultsCount(); ++i) {
        ImGui::Text("%s: %0.4f ms", results[i].name.c_str(), results[i].timeDiffUS / 1000.0);
    }

    ImGui::Separator();

    {
        scope::Color color{ ImGuiCol_Text, TITLE_COLOR };
        ImGui::Text(ICON_FA_CALCULATOR " Counters");
    }

    ImGui::Text("Vertex buffer: %.02f MB / %.02f MB", static_cast<float>(GraphicsService::VertexBuffer()->VertexBufferFilled() / ONE_MB),
        static_cast<float>(GraphicsService::VertexBuffer()->VertexBufferAllocated() / 1048576.0f));
    ImGui::Text("Index buffer: %.02f MB / %.02f MB", static_cast<float>(GraphicsService::VertexBuffer()->IndexBufferFilled() / ONE_MB),
        static_cast<float>(GraphicsService::VertexBuffer()->IndexBufferAllocated() / 1048576.0f));

    ImGui::Text("Scene objects: %u", game_.Scene().Size());
    ImGui::Text("Scene lights: %u", game_.Scene().Registry().view<LightComponent>().size());
    ImGui::Text("Materials: %u", MaterialService::Materials().size());
    ImGui::Text("Material instances: %u", MaterialService::Instances().size());
    {
        auto zombies{ MaterialInstances::Size() - MaterialService::Instances().size() };
        if (zombies) {
            scope::Color color{ ImGuiCol_Text, ImVec4{ 1, 0, 0, 1 } };
            ImGui::Text("Material zombies: %u", zombies);
        }
    }

    ImGui::Text("Pipelines: %u", PipelineCache::Size());
    ImGui::Text("Framebuffers: %u", FramebufferCache::Size());

    ImGui::Separator();

    {
        scope::Color color{ ImGuiCol_Text, TITLE_COLOR };
        ImGui::Text(ICON_FA_CLOCK " Startup");
    }

    ImGui::Text("Shader compile time: %ums", ShaderCache::CompileTime.load());
    ImGui::Text("Shader parse time: %ums", ShaderCache::ParseTime.load());
    ImGui::Text("Shader wait time: %ums", ShaderCache::WaitTime.load());

    ImGui::Separator();

    {
        scope::Color color{ ImGuiCol_Text, TITLE_COLOR };
        ImGui::Text(ICON_FA_BOOKMARK " Buffers");
    }

    uint32_t buffersSize{};
    Buffers::Each([&](BufferRef b) { buffersSize += static_cast<uint32_t>(b->Size()); });
    ImGui::Text("Buffers: %u", Buffers::Size());
    ImGui::Text("Total buffer size: %.02f MB", static_cast<float>(buffersSize) / ONE_MB);

    ImGui::Separator();

    {
        scope::Color color{ ImGuiCol_Text, TITLE_COLOR };
        ImGui::Text(ICON_FA_IMAGES " Texture");
    }

    ImGui::Text("Active textures: %d", Textures::Size());
    ImGui::Text("Active views: %d", TextureViews::Size());
    ImGui::Text("Active samplers: %d", Samplers::Size());

    ImGui::Separator();

    if (ImGui::CollapsingHeader("Texture Views")) {
        TextureViews::Each([&](TextureViewRef ref) {
            auto texture{ TextureViews::TextureRef(ref) };
            auto viewCnt{ TextureViews::Count(ref) };
            auto textureCnt{ Textures::Count(texture) };

            ImGui::PushID(static_cast<int>(ref.id()));

            if (ImGui::Button(ICON_FA_EYE)) {
                PreviewTexture(texture);
            }

            ImGui::SameLine();

            ImGui::PopID();

            ImGui::Text(TextureViews::TextureRef(ref)->FileName().c_str());

#ifdef _DEBUG
            if (TextureViews::Has<Debug>(ref)) {
                ImGui::SameLine();
                ImGui::Text("[%s]", TextureViews::Get<Debug>(ref).name.c_str());
            } else if (Textures::Has<Debug>(texture)) {
                ImGui::SameLine();
                ImGui::Text("[%s]", Textures::Get<Debug>(texture).name.c_str());
            }
#endif

            ImGui::SameLine();
            ImGui::Text(" [Refs=%d, Tex. refs=%d]", viewCnt, textureCnt);
        });
    }

    ImGui::End();
}

void GuiLayer::PreviewTexture(const TextureRef& ref) {
    ImGui::OpenPopup("PreviewWindow");

    if (ref->Format() == GraphicsService::Graphics().DepthFormat()) {
        auto view{ TEXTURE_VIEW_ADD(ref, TextureView::CreateSampled(*ref, vk::ImageViewType::e2D, vk::ImageAspectFlagBits::eDepth)) };
        guiImages_->SetImage(GuiImages::Preview, view, vk::ImageLayout::eDepthStencilReadOnlyOptimal);
    } else {
        guiImages_->SetImage(GuiImages::Preview, ref);
    }
    previewWindowVisible_ = true;
}

void GuiLayer::ComponentGizmo(ugine::gfx::GPUCommandList command, const glm::mat4& mvp, const RigidBodyComponent& c) {
    switch (c.shape) {
    case RigidBodyComponent::Shape::Box:
        gizmoRender_.RenderMesh(command, *Shapes::Cube(), gizmoColor_.rgba, mvp);
        break;
    case RigidBodyComponent::Shape::Sphere:
        gizmoRender_.RenderMesh(command, *Shapes::Sphere(), gizmoColor_.rgba, mvp);
        break;
    case RigidBodyComponent::Shape::Plane:
        gizmoRender_.RenderMesh(command, *Shapes::Plane(), gizmoColor_.rgba, mvp);
        break;
    }
}

void GuiLayer::ComponentGizmo(
    ugine::gfx::GPUCommandList command, const glm::mat4& viewProj, const ugine::core::Transformation& trans, const ugine::core::LightComponent& c) {
    switch (c.light.type) {
    case Light::Type::Directional:
        gizmoRender_.RenderDirection(command, gizmoColor_.rgba, viewProj * trans.Matrix());
        break;
    case Light::Type::Spot:
        gizmoRender_.RenderFrustum(command, 2.0f * c.light.spotAngleDeg, 1, 0, c.light.range, gizmoColor_.rgba, viewProj * trans.Matrix());
        break;
    case Light::Type::Point:
        // Scale.
        //gizmoRender_.RenderMesh(command, *Shapes::Sphere(), gizmoColor_.rgba,
        //    viewProj * Transformation{ trans.position, glm::fquat{ 1, 0, 0, 0 }, glm::vec3{ c.light.range } }.Matrix());
        break;
    }
}

void GuiLayer::ComponentGizmo(ugine::gfx::GPUCommandList command, const glm::mat4& mvp, const ugine::core::CameraComponent& c) {
    if (c.camera.Type() == Camera::ProjectionType::Perspective) {
        float aspectRatio{ static_cast<float>(c.camera.Width()) / static_cast<float>(c.camera.Height()) };
        gizmoRender_.RenderFrustum(command, c.camera.Fov(), aspectRatio, c.camera.Near(), c.camera.Far(), gizmoColor_.rgba, mvp);
    } else {
        // TODO:
        gizmoRender_.RenderMesh(command, *Shapes::Cube(), gizmoColor_.rgba, mvp);
    }
}

void GuiLayer::ImportMeshWindow() {
    ImVec2 center{ ImGui::GetMainViewport()->GetCenter() };
    ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));

    if (ImGui::BeginPopupModal(IMPORT_MESH_POPUP, nullptr, ImGuiWindowFlags_AlwaysAutoResize)) {
        ImGui::Columns(2);

        ImGui::TextUnformatted("File:");
        ImGui::NextColumn();
        ImGui::TextUnformatted(importMesh_.file.filename().string().c_str());
        ImGui::NextColumn();

        ImGui::Text("Scale:");
        ImGui::NextColumn();
        ImGui::InputFloat("##scale", &importMesh_.scale);
        ImGui::NextColumn();

        ImGui::Text("Lod levels:");
        ImGui::NextColumn();
        ImGui::InputInt("##lod", &importMesh_.lodLevels);
        ImGui::NextColumn();

        ImGui::Text("Material:");
        ImGui::NextColumn();
        if (ImGui::BeginCombo("##material", importMesh_.material ? importMesh_.material->Name().c_str() : "<select>")) {
            for (const auto& mat : MaterialService::Materials()) {
                bool selected{ mat == importMesh_.material };
                if (ImGui::Selectable(mat->Name().c_str(), &selected)) {
                    importMesh_.material = mat;
                }
            }
            ImGui::EndCombo();
        }
        ImGui::NextColumn();

        ImGui::Text("Single mesh:");
        ImGui::NextColumn();
        ImGui::Checkbox("##separate", &importMesh_.singleMesh);
        ImGui::NextColumn();

        ImGui::Text("Axis up:");
        ImGui::NextColumn();

        std::map<const char*, Axis> Axes = {
            { "Y+", Axis::YPos },
            { "Y-", Axis::YNeg },
            { "X+", Axis::XPos },
            { "X-", Axis::XNeg },
            { "Z+", Axis::ZPos },
            { "Z-", Axis::ZNeg },
        };

        const char* axisStr{};
        for (auto [k, v] : Axes) {
            if (v == importMesh_.axisUp) {
                axisStr = k;
                break;
            }
        }

        if (ImGui::BeginCombo("##axisup", axisStr)) {
            for (auto [k, v] : Axes) {
                bool selected{ v == importMesh_.axisUp };
                if (ImGui::Selectable(k, &selected)) {
                    importMesh_.axisUp = v;
                    break;
                }
            }

            ImGui::EndCombo();
        }
        ImGui::NextColumn();

        ImGui::Text("Fix paths:");
        ImGui::NextColumn();
        ImGui::Checkbox("##fixpath", &importMesh_.fixPaths);

        ImGui::Columns(1);

        if (ImGui::Button(ICON_FA_FOLDER_OPEN " Import", ImVec2(120, 0))) {
            importMesh_.preTransform = importMesh_.singleMesh;

            ImportMesh(importMesh_);
            ImGui::CloseCurrentPopup();
        }
        ImGui::SetItemDefaultFocus();
        ImGui::SameLine();
        if (ImGui::Button(ICON_FA_WINDOW_CLOSE " Cancel", ImVec2(120, 0))) {
            ImGui::CloseCurrentPopup();
        }
        ImGui::EndPopup();
    }
}

void GuiLayer::ImportMesh(const ugine::gfx::ImportMeshSettings& import) {
    auto go{ ugine::core::ImportMesh(import, game_.Scene()) };

    auto selected{ SelectedGO(context_) };
    if (selected) {
        selected.AddChild(go);
    }
}

void GuiLayer::ShowError(const std::string_view& error) {
    UGINE_ERROR(error);

    errorMessage_ = error;
    if (!errorMessage_.empty()) {
        errorMessageVisible_ = true;
    }
}

void GuiLayer::InitPrefabs() {
    auto material{ MaterialService::InstantiateDefault() };
    material->SetName("Prefab material");
    material->SetUniformVal("albedo", glm::vec4{ 1.0f });

    auto cube{ Shapes::Cube() };
    auto sphere{ Shapes::Sphere() };
    auto plane{ Shapes::Plane() };

    prefabs_.push_back(MESH_MATERIAL_ADD(cube, material));
    prefabs_.push_back(MESH_MATERIAL_ADD(sphere, material));
    prefabs_.push_back(MESH_MATERIAL_ADD(plane, material));
}

void GuiLayer::PrefabList() {
    ImGui::Begin(ICON_FA_CUBES " Prefabs");

    for (auto& prefab : prefabs_) {
        ImGui::Selectable(prefab->mesh->Name().c_str());
        gui_->DragAndDrop().CanDragMeshMaterial(prefab);
    }

    ImGui::End();
}

void GuiLayer::RenderProfiler() {

    static const uint32_t COLORS[] = {
        legit::Colors::turqoise,
        legit::Colors::peterRiver,
        legit::Colors::sunFlower,
        legit::Colors::carrot,
        legit::Colors::silver,
    };

    static const auto COLORS_CNT{ sizeof(COLORS) / sizeof(COLORS[0]) };

    std::vector<legit::ProfilerTask> tasks(GpuProfiler::Instance().ResultsCount());

    const auto& results{ GpuProfiler::Instance().Results() };
    double start{ 0 };
    for (uint32_t i = 0; i < GpuProfiler::Instance().ResultsCount(); ++i) {
        tasks[i].color = COLORS[i % COLORS_CNT];
        tasks[i].name = results[i].name;

        if (results[i].tag == QUERY_FRAME) {
            tasks[i].startTime = 0;
            tasks[i].endTime = results[i].timeDiffUS / 1000000.0;
        } else {
            tasks[i].startTime = start;
            start += results[i].timeDiffUS / 1000000.0;
            tasks[i].endTime = start;
        }
    }

    profilerWin_->gpuGraph.LoadFrameData(tasks.data(), tasks.size());
    profilerWin_->Render(ImGuiUtils::ProfilersWindow::RenderGpu);
}

void GuiLayer::HotReloadMaterials() {
    if (MaterialService::ReloadChagned()) {
        PipelineCache::Instance().Invalidate();
    }
}
