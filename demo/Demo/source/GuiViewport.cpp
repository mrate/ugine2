#include "GuiViewport.h"
#include "GuiContext.h"

#include <ugine/gfx/RenderContext.h>
#include <ugine/gfx/data/RenderPassCache.h>
#include <ugine/gui/Gui.h>
#include <ugine/gui/Utils.h>
#include <ugine/input/InputService.h>

using namespace ugine;
using namespace ugine::core;
using namespace ugine::gfx;
using namespace ugine::gui;
using namespace ugine::input;

GuiViewport::GuiViewport(ugine::gui::Gui& gui, ugine::core::Scene& scene, bool updateCameraViewport)
    : gui_{ gui }
    , scene_{ scene }
    , updateCameraViewport_{ updateCameraViewport } {
    viewport_ = std::make_unique<TextureList>(gui, 1);
}

void GuiViewport::RenderViewport(ugine::gfx::GPUCommandList command, GuiContext& guiContext) {
    if (!viewportRT_) {
        return;
    }

    auto& device{ GraphicsService::Device() };
    auto& pass{ RenderPassCache::Postprocess(GuiContext::FORMAT) };

    RenderContext context;
    context.command = command;

    pass.Begin(context, viewportRT_.View());

    if (auto camera = scene_.Camera()) {
        auto& cameraComp{ camera.Component<ugine::core::CameraComponent>() };

        context.camera = &cameraComp.camera;
        if (cameraComp.renderTarget) {
            switch (output_) {
            case Output::Default:
                RenderOutputDefault(command, guiContext, cameraComp, cameraComp.renderTarget.View());
                break;
            case Output::Depth:
                RenderOutputDepth(command, guiContext, cameraComp);
                break;
            case Output::Ssao:
                RenderOutputSingleColor(command, guiContext, cameraComp, cameraComp.aoRT.View());
                break;
            }
        }
    }

    //if (IsVisible()) {
    //    if (gizmoVisible_) {
    //        RenderGizmos(command);
    //    }
    //}

    pass.End(context);

    device.ResetDescriptors(command);
}

void GuiViewport::RenderGui(GuiContext& context) {
    const auto position{ ImGui::GetCursorScreenPos() };
    const auto viewport{ ImGui::GetContentRegionAvail() };
    const auto toolbarSize{ 30 };
    const ImVec2 toolbarViewport{ viewport.x, toolbarSize };
    const ImVec2 sceneViewport{ viewport.x, viewport.y - toolbarSize - 5 };
    const Rect sceneRect{ position.x, position.y, viewport.x, viewport.y }; // TODO:
    const ImVec2 toolbarButtonSize{ 30, 30 };

    const ImVec4 selectedColor{ 1.0f, 0.5f, 0.5f, 1.0f };
    const ImVec4 defaultColor{ 1.0f, 1.0f, 1.0f, 1.0f };

    if (viewport.x <= 0 || viewport.y <= 0) {
        return;
    }

    scope::StyleVar styleBorder{ ImGuiStyleVar_ChildBorderSize, 0.0f };
    //scope::StyleVar styleSspacing{ ImGuiStyleVar_ItemSpacing, 0.0f };

    GameObjectTransformation::SetRect(position.x, position.y, viewport.x, viewport.y);

    auto selectedGO{ SelectedGO(context) };
    auto cameraGO{ scene_.Camera() };

    ImGuiWindowFlags flags{ ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove
        | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoSavedSettings };

    auto toolbarIcon = [=](const char* label, GameObjectTransformation::Operation operation) {
        scope::Color color{ ImGuiCol_Text, transformOperation_ == operation ? selectedColor : defaultColor };
        if (ImGui::Button(label, toolbarButtonSize)) {
            transformOperation_ = operation;
        }
    };

    ImGui::BeginChild("Toolbar", toolbarViewport, flags);
    toolbarIcon(ICON_FA_ARROWS_ALT, GameObjectTransformation::Operation::Translate);
    ImGui::SameLine();
    toolbarIcon(ICON_FA_REDO, GameObjectTransformation::Operation::Rotate);
    ImGui::SameLine();
    toolbarIcon(ICON_FA_CROP_ALT, GameObjectTransformation::Operation::Scale);
    ImGui::EndChild();

    ImGui::BeginChild("Scene", sceneViewport, flags);
    if (ImGui::IsWindowHovered()) {
        if (selectedGO.IsValid() && cameraGO.IsValid() && goManipulationVisible_) {
            bool isDynamicBody{ selectedGO.Has<RigidBodyComponent>() && selectedGO.Component<RigidBodyComponent>().isDynamic
                && !selectedGO.Component<RigidBodyComponent>().isKinematic };

            bool localTransform{ localTransformation_ && !isDynamicBody };
            glm::mat4 deltaTransform;
            if (GameObjectTransformation::Render(
                    deltaTransform, cameraGO.Component<CameraComponent>().camera, selectedGO, transformOperation_, {}, localTransform)) {
                if (isDynamicBody) {
                    glm::vec3 deltaPos{ deltaTransform[3] };
                    selectedGO.Patch<RigidBodyComponent>([&](auto& r) { r.linearVelocity = deltaPos; });
                } else {
                    Transformation t{ localTransform ? selectedGO.LocalTransformation() : selectedGO.GlobalTransformation() };

                    switch (transformOperation_) {
                    case GameObjectTransformation::Operation::Translate:
                        t.Translate(deltaTransform);
                        break;
                    case GameObjectTransformation::Operation::Rotate:
                        t.Rotate(deltaTransform);
                        break;
                    case GameObjectTransformation::Operation::Scale:
                    case GameObjectTransformation::Operation::Bounds:
                        t.Scale(deltaTransform);
                        break;
                    }

                    if (localTransform) {
                        selectedGO.SetLocalTransformation(t);
                    } else {
                        selectedGO.SetGlobalTransformation(t);
                    }
                }
            }
        }
    }

    SetHoverCapture(context, gui_.DragAndDrop().DragType() != DragAndDrop::Type::None && gui_.DragAndDrop().DragType() != DragAndDrop::Type::MeshMaterial);
    if (!GameObjectTransformation::WantCapture()) {
        HandleInput(context, sceneRect);
    }

    auto camera{ scene_.Camera() };
    if (camera) {
        if (!viewportRT_ || viewportRT_.Width() != viewport.x || viewportRT_.Height() != viewport.y) {
            viewportRT_ = RenderTarget(uint32_t(viewport.x), uint32_t(viewport.y), GuiContext::FORMAT);
            viewportRT_.SetName("GuiViewport RT");

            auto cmd{ GraphicsService::Device().BeginCommandList() };
            viewportRT_.Transition(cmd, vk::ImageLayout::eUndefined, vk::ImageLayout::eShaderReadOnlyOptimal);

            viewport_->SetImage(0, viewport.x, viewport.y, viewportRT_, vk::ImageLayout::eShaderReadOnlyOptimal, Gui::Flags::FlipY);

            // TODO: Textures not destroyed.
            if (updateCameraViewport_) {
                UpdateCameraRenderTarget(uint32_t(viewport.x), uint32_t(viewport.y));
                camera.Component<CameraComponent>().renderTarget.TransitionStencil(cmd, vk::ImageLayout::eUndefined, vk::ImageLayout::eShaderReadOnlyOptimal);
            }
            GraphicsService::Device().SubmitCommandLists();

            // Wait for GPU, don't expect viewport size to change very often.
            GraphicsService::Device().WaitGpu();
        }
    }

    ImGui::Image(viewport_->Image(0), sceneViewport);
    HandleDragAndDrop(context);

    ImGui::EndChild();
}

void GuiViewport::HandleInput(GuiContext& context, const Rect& sceneRect) {
    ImGuiIO& io = ImGui::GetIO();
    if (io.MousePos.x >= sceneRect.x && io.MousePos.x < sceneRect.x + sceneRect.width && io.MousePos.y >= sceneRect.y
        && io.MousePos.y < sceneRect.y + sceneRect.height) {

        if (ImGui::IsMousePosValid()) {
            if (context.captureHover) {
                SetHoverGO(context, GameObjectAt(context, sceneRect, io.MousePos.x, io.MousePos.y));
            } else if (ImGui::IsWindowHovered() && io.WantCaptureMouse) {
                if (io.MouseClicked[0]) {
                    auto go{ GameObjectAt(context, sceneRect, io.MousePos.x, io.MousePos.y) };
                    if (go) {
                        SelectGO(context, go);
                    } else {
                        DeselectGO(context);
                    }
                } else if (io.MouseDown[1]) {
                    InputService::AddInput(INPUT_MOUSE_DRAG_X, io.MouseDelta.x);
                    InputService::AddInput(INPUT_MOUSE_DRAG_Y, io.MouseDelta.y);
                }
            }
        }
    }
}

void GuiViewport::UpdateCameraRenderTarget(uint32_t width, uint32_t height) {
    auto camera{ scene_.Camera() };
    if (camera) {
        camera.Patch<CameraComponent>([&](auto& cameraC) {
            cameraC.renderTarget = RenderTarget(width, height, GuiContext::CAMERA_FORMAT, vk::SampleCountFlagBits::e1,
                vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eTransferSrc);
            cameraC.renderTarget.SetName("CameraRenderTarget");
        });
    }
}

void GuiViewport::HandleDragAndDrop(GuiContext& context) {
    MeshMaterialRef meshMaterial;
    if (gui_.DragAndDrop().DropMeshMaterial(meshMaterial)) {
        GameObject go{ scene_.CreateObject("New " + meshMaterial->mesh->Name()) };
        go.CreateComponent<MeshComponent>(meshMaterial);

        auto cameraGO{ scene_.Camera() };
        if (cameraGO) {
            auto trans{ cameraGO.GlobalTransformation() };
            trans.position += trans.rotation * GuiContext::PREFAB_OFFSET;
            //go.SetGlobalTransformation(trans);
            go.MoveTo(trans.position);
            SelectGO(context, go);
        }
    }

    MaterialInstanceRef material;
    if (gui_.DragAndDrop().DropMaterialInstance(material)) {
        if (context.hoverGO) {
            ApplyMaterial(context.hoverGO, material);
        }
    }

    FoliageRef foliage;
    if (gui_.DragAndDrop().DropFoliage(foliage)) {
        if (context.hoverGO) {
            ApplyFoliage(context.hoverGO, foliage);
        }
    }

    MeshRef mesh;
    if (gui_.DragAndDrop().DropMesh(mesh)) {
        if (context.hoverGO) {
            ApplyMesh(context.hoverGO, mesh);
        }
    }

    GameObject child;
    if (gui_.DragAndDrop().DropGameObject(child)) {
        if (context.hoverGO && context.hoverGO != child) {
            if (child.Parent()) {
                child.Parent().RemoveChild(child);
            }
            context.hoverGO.AddChild(child);
        }
    }
}

void GuiViewport::RenderOutputDefault(
    ugine::gfx::GPUCommandList command, GuiContext& guiContext, const ugine::core::CameraComponent& camera, const TextureView& view) {
    auto& device{ GraphicsService::Device() };

    device.BindPipeline(command, &guiContext.outlinePipeline);
    device.PushConstants(command, GPUPushConstant::Data(vk::ShaderStageFlagBits::eFragment, guiContext.selectionColor));
    device.BindImageSampler(command, 0, 0, view);
    device.BindImageSampler(command, 0, 1, camera.depthBuffer.Stencil());
    device.Draw(command, 3, 1, 0, 0);
}

void GuiViewport::RenderOutputSingleColor(
    ugine::gfx::GPUCommandList command, GuiContext& guiContext, const ugine::core::CameraComponent& camera, const TextureView& view) {
    auto& device{ GraphicsService::Device() };

    device.BindPipeline(command, &guiContext.singleColorPipeline);
    device.BindImageSampler(command, 0, 0, view);
    device.Draw(command, 3, 1, 0, 0);
}

void GuiViewport::RenderOutputDepth(ugine::gfx::GPUCommandList command, GuiContext& guiContext, const ugine::core::CameraComponent& camera) {
    auto& device{ GraphicsService::Device() };

    glm::vec2 cameraNearFar{ camera.camera.Near(), camera.camera.Far() };

    device.BindPipeline(command, &guiContext.depthPipeline);
    device.PushConstants(command, GPUPushConstant::Data(vk::ShaderStageFlagBits::eFragment, cameraNearFar));
    device.BindImageSampler(command, 0, 0, camera.depthBuffer.View());
    device.Draw(command, 3, 1, 0, 0);
}