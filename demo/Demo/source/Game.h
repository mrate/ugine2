﻿#pragma once

#include <ugine/core/Scene.h>
#include <ugine/gfx/postprocess/Postprocess.h>

class Game {
public:
    Game();

    void Start();
    void End();

    void Update(float deltaMS);

    ugine::core::Scene& Scene() {
        return scene_;
    }

private:
    void ProfileScene(ugine::core::GameObject& camera, int x, int y, int z, bool singleMaterial);
    void TestScene(ugine::core::GameObject& camera);
    void TerrainScene(ugine::core::GameObject& camera);
    void LightScene(ugine::core::GameObject& camera);

    ugine::gfx::TextureViewRef LoadSkyBox(const std::filesystem::path& file);
    ugine::gfx::TextureViewRef LoadSkyBox(const std::vector<std::filesystem::path>& files);

    ugine::core::Scene scene_;
};
