﻿#include "Game.h"

#include <ugine/assets/AssetService.h>
#include <ugine/core/Component.h>
#include <ugine/core/utils/SceneImport.h>
#include <ugine/gfx/MaterialService.h>
#include <ugine/gfx/core/Texture.h>
#include <ugine/gfx/data/Shapes.h>
#include <ugine/gfx/postprocess/Bloom.h>
#include <ugine/gfx/postprocess/DepthOfField.h>
#include <ugine/gfx/postprocess/Fog.h>
#include <ugine/gfx/postprocess/LensFlare.h>
#include <ugine/gfx/postprocess/MotionBlur.h>
#include <ugine/gfx/postprocess/SSAO.h>
#include <ugine/gfx/postprocess/ToneMapping.h>
#include <ugine/gfx/rendering/Material.h>

#include <ugine/script/DefaultNatives.h>
#include <ugine/script/NativeScripts.h>

using namespace ugine;
using namespace ugine::core;
using namespace ugine::gfx;
using namespace ugine::script;

Game::Game() {}

void Game::Start() {
    std::filesystem::path assetsPath{ "../../assets" };

    scene_.Init();

    {
        std::filesystem::path skyboxPath{ assetsPath / "textures_skybox/sky2" };
        std::vector<std::filesystem::path> skybox = {
            skyboxPath / "blue_posx.jpg",
            skyboxPath / "blue_negx.jpg",
            skyboxPath / "blue_posy.jpg",
            skyboxPath / "blue_negy.jpg",
            skyboxPath / "blue_posz.jpg",
            skyboxPath / "blue_negz.jpg",
        };

        //scene_.SetSkyBox(LoadSkyBox(skybox));
    }

    // Camera.
    auto cameraGO{ scene_.CreateObject("Camera") };
    auto& cameraC{ cameraGO.CreateComponent<CameraComponent>(Camera::Perspective(60, 100, 100, 0.1f, 100.0f), true) };

    {
        std::vector<std::shared_ptr<Postprocess>> fx;
        //fx.push_back(std::make_shared<DepthOfField>());
        fx.push_back(std::make_shared<Bloom>());
        fx.push_back(std::make_shared<MotionBlur>());
        fx.push_back(std::make_shared<Fog>());
        fx.push_back(std::make_shared<LensFlare>());
        fx.push_back(std::make_shared<ToneMapping>());
        cameraC.postprocess.Init(uint32_t(cameraC.camera.Width()), uint32_t(cameraC.camera.Height()), fx);
    }

    cameraGO.MoveTo(2, 2, 4);
    cameraGO.CreateComponent<NativeScriptComponent>().Add(cameraGO, NativeScripts::Instance().Create("Camera controller"));

    MaterialService::Register(L"assets/materials/geometry.mat");

    MaterialService::Register(L"assets/materials/material.mat");
    MaterialService::Register(L"assets/materials/burningMaterial.mat");
    MaterialService::Register(L"assets/materials/wireframe.mat");

    // Default foliage.
    {
        auto foliage{ FoliageService::Add("Default foliage") };

        auto mat1{ gfx::MaterialService::InstantiateByPath(L"assets/materials/pbr.mat") };
        mat1->SetName("Foliage mat 1");
        mat1->SetUniformVal("emissive", glm::vec4{ 0, 1, 1, 1 });

        auto mat2{ gfx::MaterialService::InstantiateByPath(L"assets/materials/pbr.mat") };
        mat2->SetName("Foliage mat 2");
        mat2->SetUniformVal("emissive", glm::vec4{ 1, 0, 1, 1 });

        auto mat3{ gfx::MaterialService::InstantiateByPath(L"assets/materials/pbr.mat") };
        mat3->SetName("Foliage mat 3");
        mat3->SetUniformVal("emissive", glm::vec4{ 1, 1, 0, 1 });

        auto mat4{ gfx::MaterialService::InstantiateByPath(L"assets/materials/pbr.mat") };
        mat4->SetName("Foliage mat 4");
        mat4->SetUniformVal("emissive", glm::vec4{ 1, 1, 1, 1 });

        foliage->lodLevels.resize(4);

        foliage->lodLevels[0].meshMaterial = MESH_MATERIAL_ADD(gfx::Shapes::Sphere(), mat1);
        foliage->lodLevels[0].maxDistance = 5;

        foliage->lodLevels[1].meshMaterial = MESH_MATERIAL_ADD(gfx::Shapes::Cube(), mat2);
        foliage->lodLevels[1].maxDistance = 15;

        foliage->lodLevels[2].meshMaterial = MESH_MATERIAL_ADD(gfx::Shapes::Cube(), mat3);
        foliage->lodLevels[2].maxDistance = 30;

        foliage->lodLevels[3].meshMaterial = MESH_MATERIAL_ADD(gfx::Shapes::Cube(), mat4);
        foliage->lodLevels[3].maxDistance = 1000;
    }

    auto dir{ scene_.CreateObject("Dir light") };
    dir.CreateComponent<LightComponent>(Light{ Light::Type::Directional });
    dir.Patch<LightComponent>([&](auto& c) {
        c.volumetric = false;
        c.generatesShadows = true;
        c.isCsm = true;
        c.flare = false;

        //auto texture{ assets::AssetService::LoadTexture(assetsPath / "textures/flare1.jpg") };
        //c.flareTexture = TEXTURE_VIEW_ADD(texture, TextureView::CreateSampled(*texture));
        c.flareSize = glm::vec2{ 0.75f };
    });
    dir.Rotate(glm::radians(135.0f), glm::radians(-45.0f), 0);

    //ProfileScene(cameraGO, 5, 5, 5, true);
    TestScene(cameraGO);
    //TerrainScene(cameraGO);
    //LightScene(cameraGO);

    //core::ImportMeshSettings settings;
    //settings.file = L"d://downloads//tmp//sponza//sponza_gltf_khronos_fixed_and_split.gltf";
    //settings.separateObjects = true;
    //ImportMesh(settings, scene_);
}

void Game::End() {
    scene_.Destroy();
}

void Game::Update(float deltaMS) {
    scene_.Update();
}

void Game::ProfileScene(ugine::core::GameObject& cameraGO, int mx, int my, int mz, bool singleMaterial) {
    MaterialInstanceRef mat;

    auto pbr{ false };
    if (pbr) {
        mat = MaterialService::Instantiate("PBR");
        mat->SetUniformVal("albedo", glm::vec4{ 1.0f, 1.0f, 0.0f, 1.0f });
        mat->SetUniformVal("emissive", glm::vec4{ 1.0f, 0.0f, 0.0f, 1.0f });
    } else {
        mat = MaterialService::Instantiate("BlinnPhong");
        mat->SetUniformVal("diffuseColor", glm::vec4{ 1.0f, 1.0f, 0.0f, 1.0f });
        mat->SetUniformVal("emissiveColor", glm::vec4{ 1.0f, 0.0f, 0.0f, 1.0f });
    }
    mat->SetName("Box material");

    MeshMaterialRef meshMat;
    if (singleMaterial) {
        meshMat = MESH_MATERIAL_ADD(Shapes::Cube(), mat);
    }

    auto parentGO{ scene_.CreateObject("Box group") };

    for (int x = 0; x < mx; ++x) {
        for (int y = 0; y < my; ++y) {
            for (int z = 0; z < mz; ++z) {
                if (!singleMaterial) {
                    meshMat = MESH_MATERIAL_ADD(Shapes::Cube(), mat);
                }

                auto boxGO{ scene_.CreateObject("Box") };
                boxGO.CreateComponent<MeshComponent>(meshMat);
                boxGO.Scale(0.9f);
                boxGO.MoveTo(glm::vec3{ x, y, z });

                parentGO.AddChild(boxGO);
            }
        }
    }

    cameraGO.LookAt(glm::vec3{ 0, 0, 0 });
}

void Game::TestScene(ugine::core::GameObject& cameraGO) {
    auto mat{ MaterialService::Instantiate("BlinnPhong", "Box material") };
    mat->SetUniformVal("diffuseColor", glm::vec4{ 1.0f, 1.0f, 0.0f, 1.0f });
    mat->SetUniformVal("emissiveColor", glm::vec4{ 1.0f, 0.0f, 0.0f, 1.0f });

    const auto BOX_CNT{ 1 };

    for (int i = 0; i < BOX_CNT; ++i) {
        auto blackBoxGO{ scene_.CreateObject("Box") };
        auto boxMeshMat{ MESH_MATERIAL_ADD(Shapes::Cube(), mat) };
        blackBoxGO.CreateComponent<MeshComponent>(boxMeshMat);

        if (i == 0) {
            cameraGO.LookAt(blackBoxGO);
        }
    }

    auto lightGO{ scene_.CreateObject("Spot light") };
    lightGO.MoveTo(1, 2, 1.5f);
    auto& light{ lightGO.CreateComponent<LightComponent>(Light{ Light::Type::Spot }, false, true) };

    auto floorGO{ scene_.CreateObject("Floor") };

    auto floorMeshMat{ MESH_MATERIAL_ADD(Shapes::Cube(), MaterialService::Instantiate("BlinnPhong", "Floor material")) };
    floorMeshMat->submeshes[0].material->SetUniformVal("diffuseColor", glm::vec4{ 1.0f, 1.0f, 1.0f, 1.0f });
    auto floorMat{ floorGO.CreateComponent<MeshComponent>(floorMeshMat) };
    floorGO.Scale(10, 0.2f, 10);
    floorGO.MoveTo(0, -1, 0);
    floorGO.CreateComponent<RigidBodyComponent>(RigidBodyComponent::Shape::Box, glm::vec3{ 10, 0.2f, 10 });

    auto sphere{ scene_.CreateObject("Sphere") };
    sphere.CreateComponent<MeshComponent>(MESH_MATERIAL_ADD(Shapes::Sphere(), MaterialService::Instantiate("Test material", "Sphere material")));
    sphere.MoveTo(3, 0, 3);
    sphere.CreateComponent<RigidBodyComponent>(RigidBodyComponent::Shape::Sphere);

    //RenderTarget renderTarget(512, 512, vk::Format::eR8G8B8A8Unorm);
    //auto cameraGO2{ scene_.CreateObject("Camera 2") };
    //cameraGO2.CreateComponent<CameraComponent>(Camera::Perspective(90, 100, 100, 0.1f, 100.0f), false, renderTarget);
    //cameraGO2.MoveTo(-1, 2, 3);
    //cameraGO2.LookAt(blackBoxGO);

    auto quadGO{ scene_.CreateObject("Plane") };
    //auto& quadMat{ quadGO.CreateComponent<MeshComponent>(Plane(), MaterialService::Instantiate("BlinnPhong")).material };

    auto quadMeshMat{ MESH_MATERIAL_ADD(Shapes::Plane(), MaterialService::Instantiate("Test material", "Plane material")) };
    quadMeshMat->submeshes[0].material->SetUniformVal("thisCouldBeSomeColor", glm::vec3{ 1.0f });
    auto quadMat{ quadGO.CreateComponent<MeshComponent>(quadMeshMat) };
    // TODO: Double buffering.
    //quadMat.SetTexture("diffuseTexture", cameraGO2.Component<CameraComponent>().renderTarget.ViewRef());
    //quadMat.SetUniformVal("hasDiffuseTexture", 1);
    //quadMat.Default().SetTexture("inTex", cameraGO2.Component<CameraComponent>().renderTarget.ViewRef());
    quadGO.MoveTo(0, 0, -3);

    auto an{ scene_.CreateObject("Anim") };
    an.CreateComponent<SkinnedMeshComponent>();
    an.CreateComponent<AnimatorComponent>();
    an.Scale(0.01f, 0.01f, 0.01f);

    auto part{ scene_.CreateObject("Particles") };
    auto& parts{ part.CreateComponent<ParticleComponent>() };
    part.Patch<ParticleComponent>([](auto& parts) {
        parts.color = Color{ 1.0f, 0.0f, 1.0f, 1.0f };
        parts.count = 1000;
        parts.size = glm::vec2{ 0.05f, 0.05f };
        parts.speed = glm::vec3(0, 1, 0);
        parts.startOffset = glm::vec3(0.5, 0, 0.5);
        parts.speedOffset = glm::vec3(1, 0.4, 1);
        parts.life = 0.5f;
        parts.lifeSpan = 0.5f;
        parts.speedOffset = glm::vec3(1, 0.4, 1);
        parts.emitCount = 1000;

        auto partTex{ assets::AssetService::LoadTexture(L"assets/textures/particle01_rgba.ktx") };
        auto partTexView{ TEXTURE_VIEW_ADD(partTex, TextureView::CreateSampled(*partTex)) };
        parts.texture = partTexView;
    });
    part.MoveTo(-1, 0, 1);
}

// TODO: Make common.
TextureViewRef LoadSampled(const std::filesystem::path& fileName) {
    auto texture{ assets::AssetService::LoadTexture(fileName, false) };
    return TEXTURE_VIEW_ADD(texture, gfx::TextureView::CreateSampled(*texture));
}

void Game::TerrainScene(ugine::core::GameObject& camera) {
    camera.MoveTo(0, 20, 0);
    camera.LookAt(glm::vec3{ 64, 0, 64 });

    auto terrainGO{ scene_.CreateObject("Terrain") };
    auto& terrain{ terrainGO.CreateComponent<TerrainComponent>() };
    auto& foliage{ terrainGO.CreateComponent<FoliageComponent>() };

    terrainGO.Patch<TerrainComponent>([&](auto& t) {
        t.heightMap = L"../../assets/terrain/heightmap_test.png";
        t.splat = LoadSampled(L"../../assets/terrain/bjbjidoogbz.png");
        t.layers = LoadSampled(L"../../assets/terrain/terrainLayers.ktx");
        t.size = glm::vec2{ 16, 16 };
    });

    auto foliageDesc{ FoliageService::LoadFromFile(L"../../assets/terrain/foliage_tree/trees.fol") };

    terrainGO.Patch<FoliageComponent>([&](auto& f) {
        f.layerCount = 1;
        f.layers[0].foliage = foliageDesc;
        f.layers[0].count = 100;
    });
}

void Game::LightScene(ugine::core::GameObject& camera) {
    auto const CNT_X{ 64 };
    auto const CNT_Y{ 1 };
    auto const CNT_Z{ 64 };

    auto floorMeshMat{ MESH_MATERIAL_ADD(Shapes::Cube(), MaterialService::Instantiate("BlinnPhong")) };
    floorMeshMat->submeshes[0].material->SetUniformVal("diffuseColor", glm::vec4{ 1.0f, 1.0f, 1.0f, 1.0f });

    auto floorGO{ scene_.CreateObject("Floor") };
    floorGO.CreateComponent<MeshComponent>(floorMeshMat);
    floorGO.Scale(500, 0.2f, 500);
    floorGO.MoveTo(0, 0, 0);
    floorGO.CreateComponent<RigidBodyComponent>(RigidBodyComponent::Shape::Box, glm::vec3{ 10, 0.2f, 10 });

    LightComponent l;
    l.flare = false;
    l.generatesShadows = false;
    l.isCsm = false;
    l.volumetric = false;
    l.light.intensity = 10.0f;
    l.light.range = 1.0f;
    l.light.type = Light::Type::Point;

    glm::vec3 distance{ 2, 2, 2 };

    auto lights{ scene_.CreateObject("Lights") };
    lights.CreateComponent<NativeScriptComponent>().Add(lights, script::NativeScripts::Create("Shaker"));

    for (int x = 0; x < CNT_X; ++x) {
        for (int y = 0; y < CNT_Y; ++y) {
            for (int z = 0; z < CNT_Z; ++z) {
                l.light.color = glm::vec3{ math::RandomFloat(), math::RandomFloat(), math::RandomFloat() };

                auto light{ scene_.CreateObject("Point light") };
                light.CreateComponent<LightComponent>(l);
                light.MoveTo(glm::vec3{ x, y, z } * distance);

                lights.AddChild(light);
            }
        }
    }

    camera.MoveTo(glm::vec3{ -15, 20, 16 });
    camera.LookAt(glm::vec3{ CNT_X, 0, CNT_Z });
}

ugine::gfx::TextureViewRef Game::LoadSkyBox(const std::filesystem::path& file) {
    auto texture{ assets::AssetService::LoadCubeTexture(file) };

    return gfx::TEXTURE_VIEW_ADD(texture, gfx::TextureView::CreateSampled(*texture, vk::ImageViewType::eCube, vk::ImageAspectFlagBits::eColor, 0, 6));
}

ugine::gfx::TextureViewRef Game::LoadSkyBox(const std::vector<std::filesystem::path>& files) {
    std::vector<std::filesystem::path> f{ files };
    assets::SortCubeMapFiles(f);
    auto texture{ assets::AssetService::LoadCubeTexture(f) };

    return gfx::TEXTURE_VIEW_ADD(texture, gfx::TextureView::CreateSampled(*texture, vk::ImageViewType::eCube, vk::ImageAspectFlagBits::eColor, 0, 6));
}
