﻿#include "GameLayer.h"
#include "Game.h"

GameLayer::GameLayer(Game& game)
    : game_(game) {}

void GameLayer::Start() {
    renderer_ = std::make_unique<ugine::gfx::Renderer>();
}

void GameLayer::End() {
    renderer_ = nullptr;
}

void GameLayer::Update(float deltaMS) {
    game_.Scene().Update();
}

void GameLayer::Compute() {
    game_.Scene().Compute();
}

void GameLayer::Render(ugine::gfx::GPUCommandList command) {
    game_.Scene().PreRender(command);
    // TODO: Make render system a system.
    renderer_->Render(game_.Scene(), command);
    game_.Scene().PostRender(command);
}

bool GameLayer::HandleEvent(const ugine::system::Event& event) {
    return false;
}

void GameLayer::SetRenderFlags(ugine::gfx::RenderFlags flags) {
    renderer_->SetRenderFlags(flags);
}
