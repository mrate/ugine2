#pragma once

#include <ugine/core/GameObject.h>

struct Rect {
    float x{};
    float y{};
    float width{};
    float height{};
};

struct GuiContext {
    inline static const vk::Format FORMAT{ vk::Format::eR8G8B8A8Unorm };
    inline static const vk::Format CAMERA_FORMAT{ vk::Format::eR32G32B32A32Sfloat };
    inline static const glm::vec3 PREFAB_OFFSET{ ugine::core::FORWARD * 2.0f };

    ugine::core::GameObject hoverGO;
    ugine::core::Scene* scene{};

    bool captureHover{};

    glm::vec4 selectionColor{ 1.0f, 1.0f, 1.0f, 1.0f };

    ugine::gfx::Pipeline outlinePipeline;
    ugine::gfx::Pipeline depthPipeline;
    ugine::gfx::Pipeline singleColorPipeline;
};

void ApplyMaterial(ugine::core::GameObject& go, const ugine::gfx::MaterialInstanceRef& mat);
void ApplyFoliage(ugine::core::GameObject& go, const ugine::gfx::FoliageRef& foliage);
void ApplyMesh(ugine::core::GameObject& go, const ugine::gfx::MeshRef& messh);

void SelectGO(GuiContext& context, ugine::core::GameObject go);
void DeselectGO(GuiContext& context);

ugine::core::GameObject SelectedGO(GuiContext& context);
void SetHoverGO(GuiContext& context, const ugine::core::GameObject& go);
void SetHoverCapture(GuiContext& context, bool capture);

ugine::core::GameObject GameObjectAt(GuiContext& context, const Rect& sceneRect, float x, float y);