﻿#include <iostream>

#include <ugine/core/CoreService.h>
#include <ugine/gfx/FrameStats.h>
#include <ugine/gfx/Gfx.h>
#include <ugine/gfx/MaterialService.h>
#include <ugine/gui/Gui.h>
#include <ugine/system/Platform.h>

#include <EngineBase.h>

#include "Game.h"
#include "GameLayer.h"
#include "GuiLayer.h"

using namespace ugine;
using namespace ugine::core;
using namespace ugine::gfx;
using namespace ugine::system;

class Demo : public ugine::common::EngineBase {
public:
    using BaseClass = ugine::common::EngineBase;

    Demo()
        : ugine::common::EngineBase(L"uGine2 demo!", 1600, 1200, false, true)
        , guiLayer_(game_)
        , gameLayer_(game_) {}

protected:
    void OnStart() override {
        InitGui();

        gameLayer_.Start();
        guiLayer_.Start(&Gui());
        game_.Start();
    }

    void OnEnd() override {
        game_.End();
        gameLayer_.End();
        guiLayer_.End();
    }

    void OnResize(uint32_t width, uint32_t height) override {
        BaseClass::OnResize(width, height);
    }

    void OnEvent(const Event& event) override {
        if (guiLayer_.HandleEvent(event)) {
            return;
        }

        gameLayer_.HandleEvent(event);
    }

    void OnUpdate(float deltaMS) override {
        float fps{};
        if (Fps(fps)) {
            char text[32];
            snprintf(text, sizeof(text) - 1, "uGine2 demo! (%.0f fps)", fps);
            SetWindowTitle(text);
        }

        guiLayer_.Update(deltaMS);
        gameLayer_.Update(deltaMS);

        gameLayer_.SetRenderFlags(guiLayer_.RenderFlags());
    }

    void OnRender() override {
        auto& device{ GraphicsService::Device() };

        // TODO:
        FrameStats::Instance().Clear();

        // Compute.
        gameLayer_.Compute();

        // Present.
        auto gfxCommand{ device.BeginCommandList() };

        // Render.
        gameLayer_.Render(gfxCommand);

        bool render{};
        auto camera{ game_.Scene().Camera() };

        //if (camera) {
        //    auto& cameraComp{ camera.Component<ugine::core::CameraComponent>() };
        //    if (cameraComp.renderTarget) {
        //        fullScreenMat_->Default().SetTexture("inputTexture", cameraComp.renderTarget.ViewRef());
        //        render = true;
        //    }

        //    //if (cameraComp.depthBuffer) {
        //    //    auto cameraDepth{ TextureViews::TextureRef(cameraComp.depthBuffer.ViewRef()) };
        //    //    auto depth{ TextureViews::TextureRef(DepthBuffer()) };

        //    //    cameraDepth->CopyTo(gfxCommand, *depth, vk::ImageLayout::eDepthStencilReadOnlyOptimal, vk::ImageLayout::eDepthStencilReadOnlyOptimal,
        //    //        vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil, vk::Filter::eNearest);
        //    //}
        //}

        {
            Color clearColor{ 0.0f, 0.0f, 0.0f, 1.0f };
            auto presentCmd{ device.BeginCommandList() };

            guiLayer_.PreRender(presentCmd);

            GPUDebugLabel label(presentCmd, "Present");

            device.PresentBegin(presentCmd, clearColor);
            device.SetViewport(presentCmd, Viewport());
            device.SetScissor(presentCmd, Scissor());

            guiLayer_.Render(presentCmd);

            device.PresentEnd(presentCmd, &game_.Scene().ComputeSemaphore());
        }
    }

private:
    Game game_;
    GuiLayer guiLayer_;
    GameLayer gameLayer_;
};

int main(int argc, char* argv[]) {
    try {
        Demo demo;
        demo.Run();
    } catch (const std::exception& ex) {
        std::cerr << "Error: " << ex.what() << std::endl;
        return 1;
    }

    return 0;
}
